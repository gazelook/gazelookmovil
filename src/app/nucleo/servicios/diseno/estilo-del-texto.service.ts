import {ColorDelTexto, EstilosDelTexto} from '@shared/diseno/enums/estilos-colores-general';
import {Injectable} from '@angular/core';
import {ConfiguracionTexto} from '@shared/diseno/modelos/texto.interface';
import {TamanoDeTextoConInterlineado} from '@shared/diseno/enums/estilos-tamano-general.enum';
import {PaddingIzqDerDelTexto} from '@shared/diseno/enums/estilos-padding-general';

/*
    Especificaiones generales,

    - Permite obtener la combinacion de estilos para un elemento de texto en base a la interfaz ConfiguracionTexto
    - El metodo obtenerEstilos, devuelve un json en base a los parametros de configuracion deseados
*/

@Injectable({providedIn: 'root'})
export class EstiloDelTextoServicio {

  public colorDelTexto = ColorDelTexto;
  public estilosDelTexto = EstilosDelTexto;
  public tamanoDeTexto = TamanoDeTextoConInterlineado;
  public tamanoDeTextoConInterlineado = TamanoDeTextoConInterlineado;
  public paddingIzqDerDelTexto = PaddingIzqDerDelTexto;


  constructor() {
  }

  // Devuelve un JSON de clases en base a la configuracion deseada
  obtenerEstilosTexto(configuracion: ConfiguracionTexto): {} {
    const clases = {};
    clases[configuracion.color.toString()] = true;
    clases[configuracion.estiloTexto.toString()] = true;
    clases['enMayusculas'] = configuracion.enMayusculas;
    if (configuracion.tamano) {
      clases[configuracion.tamano.toString()] = true;
    }
    if (configuracion.tamanoConInterlineado) {
      clases[configuracion.tamanoConInterlineado.toString()] = true;
    }
    if (configuracion.paddingIzqDerDelTexto) {
      clases[configuracion.paddingIzqDerDelTexto.toString()] = true;
    }
    if (configuracion.enCapitalize) {
      clases['enCapitalize'] = true;
    }
    if (configuracion.textoOverflow) {
      clases['textoOverflow'] = true;
    }
    return clases;
  }
}
