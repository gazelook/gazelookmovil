import { Injectable } from '@angular/core';
import { TipoMonedaNegocio } from 'dominio/logica-negocio/moneda.negocio';
import { ValorBase } from '@shared/diseno/modelos/moneda-picker.interface';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';


@Injectable({ providedIn: 'root' })
export class MonedaPickerService {

  constructor(
    private generadorId: GeneradorId,
    private variablesGlobales: VariablesGlobales,
    private tipoMonedaNegocio: TipoMonedaNegocio,
  ) {

  }

  // Array a texto
  convertirArrayAString(cadena: string[]) {
    let resultado = ''
    cadena.forEach((c: string) => {
      resultado += c
    })
    return resultado
  }

  // Formatear valorNeto de la cadena a cadena de texto con puntos 0.000.0...
  formatearValorNetoAStringConPuntos(
    valorNeto: string
  ): string {
    const cadena = valorNeto.split('')
    const cadenaVolteada = this.voltearCadenaDeTexto(cadena)
    // Asignar Ceros
    let contador = 0
    const cadenaConPuntos = []
    cadenaVolteada.forEach((c, i) => {
      cadenaConPuntos.push(c)
      contador += 1
      if (contador === 3 && i !== cadenaVolteada.length - 1) {
        cadenaConPuntos.push('.')
        contador = 0
      }
    })
    const resultado = this.voltearCadenaDeTexto(cadenaConPuntos)
    return this.convertirArrayAString(resultado)
  }

  voltearCadenaDeTexto(cadena: string[]): string[] {
    const cadenaVolteada = []
    for (let index = cadena.length - 1; index >= 0; index--) {
      cadenaVolteada.push(cadena[index])
    }
    return cadenaVolteada
  }

  // Devuelve la cadena de ceros sin punto, en formato array
  obtenerCadenaSoloCeros(cadena: string): Array<string> {
    let resultado = ''
    cadena.split('.').forEach(c => {
      resultado += c
    })
    return resultado.split('')
  }

  // Devuelve el valorBase para el valor estimado del proyecto en session sotorage o obtenido de la bd
  obtenerValorEstimadoDelProyecto(
    valorEstimado: number
  ): ValorBase {
    const data: ValorBase = {
      contador: 0,
      valorFormateado: '0',
      valorNeto: '0'
    }

    if (valorEstimado > 0) {
      const valor = valorEstimado.toString()
      data.contador = valor.length
      data.valorNeto = valor
      data.valorFormateado = valor
    }

    return data
  }

  obtenerValorAPagarDeLaSuscripcion(
    valorEstimado: number
  ): ValorBase {
    const data: ValorBase = {
      contador: 0,
      valorFormateado: this.variablesGlobales.valorFormateadoInicialParaSuscripcion,
      valorNeto: this.variablesGlobales.valorNetoInicialParaSuscripcion
    }

    if (valorEstimado > 0) {
      const valor = valorEstimado.toString()
      data.contador = valor.length
      data.valorNeto = this.reemplazarCerosEnValorNetoPorElValorEstimadoActualDelProyecto(
        valor,
        data.valorNeto
      )
      data.valorFormateado = this.formatearValorNetoAStringConPuntos(data.valorNeto)
    }

    return data
  }

  reemplazarCerosEnValorNetoPorElValorEstimadoActualDelProyecto(
    valorEstimado: string,
    valorNeto: string
  ): string {
    const aux: string[] = valorNeto.split('')
    valorEstimado.split('').forEach(c => {
      aux.push(c)
      aux.splice(0, 1)
    })
    const cadena: string = this.convertirArrayAString(aux)
    return cadena
  }

  obtenerTipoDeMonedaActual(
    moneda: CatalogoTipoMonedaModel
  ): ValorBase {
    const monedaTipo: ValorBase = {
      valorFormateado: this.variablesGlobales.placeholderMonedaPicker,
      placeholder: this.variablesGlobales.placeholderMonedaPicker,
      seleccionado: { codigo: '', nombre: '', auxiliar: '' }
    }

    if (moneda && ((moneda.codigo && moneda.codigo.length > 0) || (moneda.codNombre && moneda.codNombre.length > 0))) {
      const monedas = this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaDelLocalStorage()
      if (monedas && monedas.length > 0) {
        monedas.forEach(m => {
          if (m.codigo === moneda.codigo || m.auxiliar === moneda.codNombre) {
            monedaTipo.valorFormateado = m.auxiliar
            monedaTipo.seleccionado = m
          }
        })
      }
    }

    return monedaTipo
  }

}
