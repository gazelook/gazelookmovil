import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { AlbumParams } from 'dominio/modelo/parametros/album-parametros.interface';
import { AccionEntidad, CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';

@Injectable({ providedIn: 'root' })
export class AlbumService {

  constructor(
    private generadorId: GeneradorId,
    private perfilNegocio: PerfilNegocio
  ) { }

  definirAccionDelAmbumSegunUrl(url: string): AccionEntidad {
    if (url.indexOf('registro') >= 0) {
      return AccionEntidad.REGISTRO
    }

    if (url.indexOf('crear') >= 0) {
      return AccionEntidad.CREAR
    }

    if (url.indexOf('actualizar') >= 0) {
      return AccionEntidad.ACTUALIZAR
    }

    if (url.indexOf('visitar') >= 0) {
      return AccionEntidad.VISITAR
    }
  }

  validarParametrosDelAlbumSegunAccionEntidad(
    params: AlbumParams,
    urlParams: Params
  ): AlbumParams {
    params.estado = false
    const { entidad, titulo, codigo } = urlParams
    switch (params.accionEntidad) {
      case AccionEntidad.REGISTRO:
        if (
          entidad && entidad !== ':entidad' &&
          titulo && titulo !== ':titulo' &&
          codigo && codigo !== ':codigo'
        ) {
          params.estado = true
          params.entidad = entidad as CodigosCatalogoEntidad
          params.titulo = titulo
          params.codigo = codigo
        }
        break
      case AccionEntidad.CREAR:
        if (
          entidad && entidad !== ':entidad' &&
          titulo && titulo !== ':titulo'
        ) {
          params.estado = true
          params.entidad = entidad as CodigosCatalogoEntidad
          params.titulo = titulo
        }
        break
      case AccionEntidad.ACTUALIZAR:
        if (
          entidad && entidad !== ':entidad' &&
          titulo && titulo !== ':titulo'
        ) {
          params.estado = true
          params.entidad = entidad as CodigosCatalogoEntidad
          params.titulo = titulo
        }
        break
      case AccionEntidad.VISITAR:
        if (
          entidad && entidad !== ':entidad' &&
          titulo && titulo !== ':titulo'
        ) {
          params.estado = true
          params.entidad = entidad as CodigosCatalogoEntidad
          params.titulo = titulo
        }
        break
      default:
        break
    }
    return params
  }

  determinarTipoItemsPorDefectoParaAlbumGeneralSegunEntidad(
    entidad: CodigosCatalogoEntidad
  ): CodigosCatalogoArchivosPorDefecto {
    switch (entidad) {
      case CodigosCatalogoEntidad.PERFIL:
        return CodigosCatalogoArchivosPorDefecto.ALBUM_GENERAL
      case CodigosCatalogoEntidad.PROYECTO:
        return CodigosCatalogoArchivosPorDefecto.PROYECTOS
      case CodigosCatalogoEntidad.NOTICIA:
        return CodigosCatalogoArchivosPorDefecto.PROYECTOS
      default:
        return CodigosCatalogoArchivosPorDefecto.ALBUM_GENERAL
    }
  }

  obtenerMediaDeLaListaDeMedias(
    id: string,
    media: Array<MediaModel>
  ): number {
    let pos = -1
    media.forEach((item, i) => {
      if (item.id === id) {
        pos = i
      }
    })
    return pos
  }
}
