import { TestBed } from '@angular/core/testing';

import { VideoGrabacionService } from '@core/servicios/generales/video-grabacion/video-grabacion.service';

describe('VideoGrabacionService', () => {
  let service: VideoGrabacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoGrabacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
