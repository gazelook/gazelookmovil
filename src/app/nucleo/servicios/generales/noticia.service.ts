import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params } from '@angular/router';
import { InputKey, Inputs } from 'src/app/presentacion/proyectos/publicar/publicar.component';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { EstiloErrorInput } from '@shared/diseno/enums/estilos-colores-general';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { CodigosCatalogoTipoMedia } from '../remotos/codigos-catalogos/catalago-tipo-media.enum';
import { AccionEntidad } from '../remotos/codigos-catalogos/catalogo-entidad.enum';
import { ResumenDataMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { MonedaPickerService } from '@core/servicios/generales/moneda-picker.service';

@Injectable({ providedIn: 'root' })
export class NoticiaService {

  constructor(
    private formBuilder: FormBuilder,
    private generadorId: GeneradorId,
    private monedaPickerService: MonedaPickerService,
    private albumNegocio: AlbumNegocio
  ) {   }

  determinarAccionProyectoPorUrl(url: string): AccionEntidad {
    if (url.indexOf('publicar') >= 0) {
      return AccionEntidad.CREAR
    }

    if (url.indexOf('actualizar') >= 0) {
      return AccionEntidad.ACTUALIZAR
    }

    if (url.indexOf('visitar') >= 0) {
      return AccionEntidad.VISITAR
    }
  }

  validarParametrosSegunAccionEntidad(
    params: ProyectoParams,
    urlParams: Params
  ): ProyectoParams {
    params.estado = false
    // Validar parametros segun accion entidad
    if (params.accionEntidad) {
      switch (params.accionEntidad) {
        case AccionEntidad.CREAR:
          params.estado = true
          return params
        case AccionEntidad.ACTUALIZAR:
          if (urlParams.id) {
            params.estado = true
            params.id = urlParams.id
          }
          return params
        case AccionEntidad.VISITAR:
          if (urlParams.id) {
            params.estado = true
            params.id = urlParams.id
          }
          return params
        default: break;
      }
    }
    return params
  }

  inicializarControlesFormulario(
    noticia: NoticiaModel
  ): FormGroup {
    const registroForm: FormGroup = this.formBuilder.group({
      tituloCorto: [
        (noticia) ? noticia.tituloCorto : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z0-9 ]+$'),
          Validators.minLength(3)
        ],
      ],
      titulo: [
        (noticia) ? noticia.titulo : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],
      autor: [
        (noticia) ? noticia.autor : '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],
      descripcion: [
        (noticia) ? noticia.descripcion : '',
        [
          Validators.pattern('^[A-Za-z0-9-,. ]+$')
        ]
      ],
      ubicacion: [
        (noticia) ? noticia.direccion.descripcion : '',
        [
          Validators.pattern('^[A-Za-z0-9-,. ]+$')
        ]
      ]
    })

    return registroForm
  }

  configurarInputsDelFormularioConKey(
    proyectoForm: FormGroup,
    soloLectura: boolean = false
  ): Array<Inputs> {
    let inputsForm: Array<Inputs> = []

    if (proyectoForm) {
      // 0
      inputsForm.push({
        key: InputKey.TITULO_CORTO,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO_CENTRADO
          },
          ocultarMensajeError: true,
          placeholder: 'm4v15texto6',
          data: proyectoForm.controls.tituloCorto,
          contadorCaracteres: {
            mostrar: false,
            numeroMaximo: 25,
            contador: 0
          },
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
      // 1
      inputsForm.push({
        key: InputKey.TITULO,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO
          },
          ocultarMensajeError: true,
          placeholder: 'm4v15texto7',
          data: proyectoForm.controls.titulo,
          contadorCaracteres: {
            mostrar: false,
            numeroMaximo: 40,
            contador: 0
          },
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
      // 2
      inputsForm.push({
        key: InputKey.UBICACION,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO
          },
          ocultarMensajeError: true,
          placeholder: 'm4v15texto8',
          data: proyectoForm.controls.ubicacion,
          id: this.generadorId.generarIdConSemilla(),
          errorPersonalizado: '',
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
      // 3
      inputsForm.push({
        key: InputKey.AUTOR,
        input: {
          tipo: 'text',
          error: false,
          estilo: {
            estiloError: EstiloErrorInput.ROJO,
            estiloInput: EstiloInput.PROYECTO_MINUSCULA
          },
          ocultarMensajeError: true,
          placeholder: 'm4v15texto9',
          data: proyectoForm.controls.autor,
          soloLectura: soloLectura,
          bloquearCopy: false
        }
      })
    }
    return inputsForm
  }

  // Inicializar inputs del formulario
  configurarInputsDelFormulario(
    proyectoForm: FormGroup,
    soloLectura: boolean = false
  ): Array<InputCompartido> {
    let inputsForm: Array<InputCompartido> = []

    if (proyectoForm) {
      // 0
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.PROYECTO_CENTRADO
        },
        ocultarMensajeError: true,
        placeholder: 'm4v15texto6',
        data: proyectoForm.controls.tituloCorto,
        contadorCaracteres: {
          mostrar: false,
          numeroMaximo: 25,
          contador: 0
        },
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: soloLectura,
        bloquearCopy: false
      })
      // 1
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.PROYECTO
        },
        ocultarMensajeError: true,
        placeholder: 'm4v15texto7',
        data: proyectoForm.controls.titulo,
        contadorCaracteres: {
          mostrar: false,
          numeroMaximo: 40,
          contador: 0
        },
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: soloLectura,
        bloquearCopy: false
      })
      // 2
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.PROYECTO
        },
        ocultarMensajeError: true,
        placeholder: 'm4v15texto8',
        data: proyectoForm.controls.ubicacion,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: soloLectura,
        bloquearCopy: false
      })
      // 3
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.PROYECTO_MINUSCULA
        },
        ocultarMensajeError: true,
        placeholder: 'm4v15texto9',
        data: proyectoForm.controls.autor,
        soloLectura: soloLectura,
        bloquearCopy: false
      })
    }
    return inputsForm
  }

  determinarUrlImagenPortada(
    album: AlbumModel,
    imagenesDefecto: Array<ArchivoModel>
  ): {
    url: string,
    porDefecto: boolean
  } {
    let data = {
      url: '',
      porDefecto: false
    }

    const url = this.definirUrlPortada(album)

    if (!url) {
      const min = 0
      const max = imagenesDefecto.length
      if (imagenesDefecto.length > 0) {
        const pos = Math.floor(Math.random() * ((max - 1) - min + 1) + min)
        data.url = imagenesDefecto[pos].url
        data.porDefecto = true
      }
      return data
    }

    data.url = url
    return data
  }

  obtenerTextoDescripcionContador(
    noticia: NoticiaModel
  ) {
    let texto = '0000/1500'
    if (noticia) {
      const numero = (noticia.descripcion) ? noticia.descripcion.length : 0

      if (numero < 10) {
        return '000' + numero + '/1500'
      }

      if (numero >= 10 && numero < 100) {
        return '00' + numero + '/1500'
      }

      if (numero >= 100 && numero < 1000) {
        return '0' + numero + '/1500'
      }

      return numero + '/1500'
    }
    return '0000/1500'
  }

  validarCamposEnNoticia(
    noticia: NoticiaModel
  ) {
    let res = true
    // Titulo Corto
    if (!(noticia.tituloCorto && noticia.tituloCorto.length > 0)) {
      return false
    }
    // Titulo largo
    if (!(noticia.titulo && noticia.titulo.length > 0)) {
      return false
    }
    // Direccion
    if (!(noticia.direccion.descripcion && noticia.direccion.descripcion.length > 0)) {
      return false
    }
    // Perfil
    if (!(noticia.perfil && noticia.perfil._id && noticia.perfil._id.length > 0)) {
      return false
    }
    // Autor
    if (!(noticia.autor && noticia.autor.length > 0)) {
      return false
    }
    // Descripcion
    // if (!(noticia.descripcion && noticia.descripcion.length > 0)) {
    //     return false
    // }

    return true
  }

  obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(
    proyecto: ProyectoModel
  ): ResumenDataMonedaPicker {
    const valorEstimado = (proyecto && proyecto.valorEstimado) ? proyecto.valorEstimado : 0
    const moneda: CatalogoTipoMonedaModel = (proyecto && proyecto.moneda) ? proyecto.moneda : { codigo: '' }
    const data = {
      valorEstimado: this.monedaPickerService.obtenerValorEstimadoDelProyecto(valorEstimado),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda)
    }
    return data
  }

  obtenerSubtituloAppBarSegunAccionEntidadParaNoticia(
    params: ProyectoParams
  ): string {
    let subtitulo: string = ''
    switch (params.accionEntidad) {
      case AccionEntidad.CREAR:
        subtitulo = 'm4v15texto1'
        break
      case AccionEntidad.ACTUALIZAR:
        subtitulo = 'm4v16texto1'
        break
      case AccionEntidad.VISITAR:
        subtitulo = 'm4v15texto5' 
        break
      default: break;
    }
    return subtitulo    
  }

  definirUrlPortada(album: AlbumModel): string {
    try {


      if (
        album &&
        album.portada &&
        album.portada.catalogoMedia &&
        album.portada.catalogoMedia.codigo !== CodigosCatalogoTipoMedia.TIPO_LINK &&
        album.portada.principal
      ) {
        return album.portada.principal.url
      }

      if (album && album.portada && album.portada.miniatura) {
        return album.portada.miniatura.url
      }

      return undefined
    } catch (error) {
      return undefined
    }
  }
}
