import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { TamanoDeTextoConInterlineado, TamanoPortadaGaze } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { EspesorLineaItem, ColorFondoLinea, ColorDelTexto, EstilosDelTexto, ColorDeFondo } from '@shared/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { ConfiguracionLineaVerde, DataContacto } from '@shared/diseno/modelos/lista-contactos.interface';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';
import { ItemCircularCompartido, ItemRectangularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { UsoItemCircular, UsoItemRectangular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ColorDeBorde, ColorFondoAleatorio } from '@shared/diseno/enums/estilo-colores.enum';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { Injectable } from '@angular/core'
import { PaddingLineaVerdeContactos } from '@shared/diseno/enums/estilos-padding-general';

@Injectable({ providedIn: 'root' })
export class MetodosParaFotos {

    constructor(
        private generadorId: GeneradorId,
        private albumNegocio: AlbumNegocio
    ) {

    }

    configurarItemListaContacto(
        perfil: PerfilModel,
        asociacion: AsociacionModel,
        seleccionado: boolean = false
    ): ConfiguracionItemListaContactosCompartido {
        if (!perfil) {
            return null
        }
        const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
            CodigosCatalogoTipoAlbum.PERFIL,
            perfil.album
        )

        let usoItem: UsoItemCircular = UsoItemCircular.CIRCARITACONTACTODEFECTO
        let urlMedia: string = album.portada.principal.url
        
        if (
            album &&
            album.portada &&
            album.portada.principal &&
            !album.portada.principal.fileDefault
        ) {
            
            urlMedia = album.portada.principal.url
            usoItem = UsoItemCircular.CIRCONTACTO
        }

        return {
            id: (asociacion && asociacion !== null) ? asociacion.id : this.generadorId.generarIdConSemilla(),
            usoItem: UsoItemListaContacto.USO_CONTACTO,
            contacto: {
                nombreContacto: perfil.nombreContacto || '',
                nombreContactoTraducido: perfil.nombreContactoTraducido,
                nombre: '',
                estilosTextoSuperior: {
                    color: ColorDelTexto.TEXTOAZULBASE,
                    estiloTexto: EstilosDelTexto.BOLD,
                    enMayusculas: true,
                    tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
                },
                estilosTextoInferior: {
                    color: ColorDelTexto.TEXTONEGRO,
                    estiloTexto: EstilosDelTexto.REGULAR,
                    enMayusculas: true,
                    tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
                },
                idPerfil: perfil._id
            },
            configCirculoFoto: this.configurarItemCircular(
                urlMedia,
                (!seleccionado) ? ColorDeBorde.BORDER_ROJO : ColorDeBorde.BORDER_AZUL,
                this.obtenerColorFondoAleatorio(),
                false,
                usoItem,
                true
            ),
            mostrarX: {
                mostrar: false,
                color: true
            },
            configuracionLineaVerde: this.configurarLineaVerde(
                PaddingLineaVerdeContactos.PADDING_1542_267
            ),
            mostrarCorazon: seleccionado
        }
    }

    configurarItemCircularParaChat(
        perfil: PerfilModel,
    ): ItemCircularCompartido {
        if (!perfil) {
            return null
        }

       
     

        const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
            CodigosCatalogoTipoAlbum.PERFIL,
            perfil.album
        )
        let usoItem: UsoItemCircular = UsoItemCircular.CIRPERFILCARITACHAT
        let urlMedia: string = album.portada.principal.url
        if (
            album &&
            album.portada &&
            album.portada.principal &&
            !album.portada.principal.fileDefault
        ) {
            urlMedia = album.portada.principal.url
            usoItem = UsoItemCircular.CIRPERFILCHAT
        }



        return this.configurarItemCircular(
            urlMedia,
            ColorDeBorde.BORDER_ROJO,
            this.obtenerColorFondoAleatorio(),
            false,
            usoItem,
            true
        )
    }

    configurarItemCircular(
        urlMedia: string,
        colorBorde: ColorDeBorde,
        colorFondo: ColorDeFondo,
        mostrarCorazon: boolean,
        usoItemCirculo: UsoItemCircular,
        activarClick: boolean,
        id?: string,
    ): ItemCircularCompartido {

        const colorAUsar = (usoItemCirculo === UsoItemCircular.CIRCARITAPERFIL) ? this.obtenerColorFondoAleatorio() : colorFondo

        return {
            id: (id) ? id : '',
            idInterno: this.generadorId.generarIdConSemilla(),
            usoDelItem: usoItemCirculo,
            esVisitante: false,
            urlMedia: urlMedia,
            activarClick: false,
            activarDobleClick: false,
            activarLongPress: false,
            mostrarBoton: true,
            mostrarLoader: (urlMedia && urlMedia.length > 0),
            textoBoton: '',
            capaOpacidad: {
                mostrar: false
            },
            esBotonUpload: false,
            colorBorde: colorBorde,
            colorDeFondo: colorAUsar,
            mostrarCorazon: mostrarCorazon,
        }
    }

    obtenerColorFondoAleatorio() {
        const fondos = Object.keys(ColorFondoAleatorio).map((type) => {
            return ColorFondoAleatorio[type]
        })

        const fondoElegido = this.randomItem(fondos);
        return fondoElegido
    }

    randomItem(items: any) {
        return items[Math.floor(Math.random() * items.length)]
    }

    configurarLineaVerde(
        paddingLineaVerdeContactos: PaddingLineaVerdeContactos
    ): ConfiguracionLineaVerde {
        return {
            ancho: AnchoLineaItem.ANCHO6386,
            espesor: EspesorLineaItem.ESPESOR041,
            colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
            forzarAlFinal: false,
            paddingLineaVerde: paddingLineaVerdeContactos
        }
    }

    configurarItemRectangular(
        urlMedia: string,
        colorBorde: ColorDeBorde,
        colorFondo: ColorDeFondo,
        mostrarCorazon: boolean,
        usoItemCirculo: UsoItemRectangular,
        activarClick: boolean,
    ): ItemRectangularCompartido {
        return {
            id: '',
            idInterno: this.generadorId.generarIdConSemilla(),
            usoDelItem: usoItemCirculo,
            esVisitante: false,
            urlMedia: urlMedia,
            activarClick: false,
            activarDobleClick: false,
            activarLongPress: false,
            mostrarBoton: true,
            mostrarLoader: (urlMedia && urlMedia.length > 0),
            textoBoton: '',
            capaOpacidad: {
                mostrar: false
            },
            esBotonUpload: false,
            colorBorde: colorBorde,
            colorDeFondo: colorFondo,
            mostrarCorazon: mostrarCorazon,
        }
    }

    configurarPortada(
        tamano: TamanoPortadaGaze
    ): PortadaGazeCompartido {
        return {
            tamano: tamano,
            mostrarLoader: true
        }
    }

    configurarItemNoticia(
        noticia: NoticiaModel
    ): CongifuracionItemProyectosNoticias {
        const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(
            noticia.adjuntos
        )

        let urlMedia: string = ''
        if (
            album.portada &&
            album.portada.miniatura
        ) {
            urlMedia = album.portada.miniatura.url
        }

        if (
            album.portada &&
            album.portada.principal &&
            !album.portada.miniatura
        ) {
            urlMedia = album.portada.principal.url
        }

        return {
            id: noticia.id,
            colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
            colorDeFondo: ColorDeFondo.FONDO_BLANCO,
            fecha: {
                mostrar: true,
                configuracion: {
                    fecha: new Date(noticia.fechaCreacion),
                    formato: 'dd/MM/yyyy'
                }
            },
            etiqueta: {
                mostrar: false,
            },
            titulo: {
                mostrar: true,
                configuracion: {
                    textoBoton1: noticia.tituloCorto,
                    colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
                    colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
                }
            },
            urlMedia: urlMedia,
            usoItem: UsoItemProyectoNoticia.RECNOTICIA,
            loader: (
                album &&
                album.portada &&
                urlMedia.length > 0
            ) ? true : false,
            eventoTap: {
                activo: false
            },
            eventoDobleTap: {
                activo: false
            },
            eventoPress: {
                activo: false
            },
            actualizado: (noticia.actualizado)
        }
    }

    configurarItemProyecto(
        proyecto: ProyectoModel,
        resumen: boolean = false
    ): CongifuracionItemProyectosNoticias {
        const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
            CodigosCatalogoTipoAlbum.GENERAL,
            proyecto.adjuntos
        )

        let urlMedia: string = ''
        urlMedia = album.portada.principal.url        
        // if (
        //     album.portada &&
        //     album.portada.miniatura
        // ) {
        //     urlMedia = album.portada.miniatura.url
        // }

        // if (
        //     album.portada &&
        //     album.portada.principal &&
        //     !album.portada.miniatura
        // ) {
        //     urlMedia = album.portada.principal.url
        // }

        return {
            id: proyecto.id,
            colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
            colorDeFondo: ColorDeFondo.FONDO_BLANCO,
            fecha: {
                mostrar: true,
                configuracion: {
                    fecha: new Date(proyecto.fechaCreacion),
                    formato: 'dd/MM/yyyy'
                }
            },
            etiqueta: {
                mostrar: false,
            },
            titulo: {
                mostrar: true,
                configuracion: {
                    textoBoton1: proyecto.tituloCorto,
                    colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
                    colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
                }
            },
            urlMedia: urlMedia,
            usoItem: UsoItemProyectoNoticia.RECPROYECTO,
            loader: (
                album &&
                album.portada &&
                urlMedia.length > 0
            ) ? true : false,
            eventoTap: {
                activo: false
            },
            eventoDobleTap: {
                activo: false
            },
            eventoPress: {
                activo: false
            },
            resumen: resumen,
            actualizado: (proyecto.actualizado),
            // mostrarCorazon: true
        }
    }

    configurarItemListaContactoParaNotificacion(
        perfil: PerfilModel,
        asociacion: AsociacionModel,
        mensaje: string,
        evento: Function
    ): ConfiguracionItemListaContactosCompartido {
        if (!perfil) {
            return null
        }
        const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
            CodigosCatalogoTipoAlbum.PERFIL,
            perfil.album
        )

        let usoItem: UsoItemCircular = UsoItemCircular.CIRCARITACONTACTODEFECTO
        let urlMedia: string = album.portada.principal.url


        if (
            album &&
            album.portada &&
            album.portada.principal &&
            !album.portada.principal.fileDefault
        ) {
            urlMedia = album.portada.principal.url
            usoItem = UsoItemCircular.CIRCONTACTO
        }

        return {
            id: (asociacion && asociacion !== null) ? asociacion.id : this.generadorId.generarIdConSemilla(),
            usoItem: UsoItemListaContacto.USO_MENSAJE,
            mensaje: {
                contacto: {
                    nombreContacto: perfil.nombreContacto || '',
                    nombre: '',
                    estilosTextoSuperior: {
                        color: ColorDelTexto.TEXTOAZULBASE,
                        estiloTexto: EstilosDelTexto.BOLD,
                        enMayusculas: true,
                        tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
                    },
                    estilosTextoInferior: {
                        color: ColorDelTexto.TEXTONEGRO,
                        estiloTexto: EstilosDelTexto.REGULAR,
                        enMayusculas: false,
                        tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
                    },
                    idPerfil: perfil._id
                },
                mensaje: mensaje
            },
            configCirculoFoto: this.configurarItemCircular(
                urlMedia,
                ColorDeBorde.BORDER_AZUL,
                this.obtenerColorFondoAleatorio(),
                true,
                usoItem,
                true
            ),
            mostrarX: {
                mostrar: false,
                color: true
            },
            configuracionLineaVerde: this.configurarLineaVerde(
                PaddingLineaVerdeContactos.PADDING_1542_267
            ),
            mostrarCorazon: false,
            eventoCirculoNombre: evento
        }
    }

}
