import { Injectable, OnInit } from '@angular/core';
import { AngularFireAction, AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { BehaviorSubject, Observable, Subject, Subscription, throwError } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';

import { DataNotificaciones, Notificaciones } from '@core/servicios/generales/notificaciones/notificaciones.interface';

@Injectable({ providedIn: 'root' })
export class NotificacionesDePerfil implements Notificaciones {

  public notificaciones$: Observable<AngularFireAction<DataSnapshot>[]>;
  public obtenerNotificaciones$: BehaviorSubject<DataNotificaciones | null>;
  public subscripcionNotificaciones$: Subscription;
  public unsubscribe$: Subject<void>;
  public unsubscribeAction$: Observable<void>;

  constructor(
    private db: AngularFireDatabase
  ) {
    this.configurarQuerysNotificaciones();
    this.configurarAccionUnsubscribe();
  }


  configurarQuerysNotificaciones(): void {
    this.obtenerNotificaciones$ = new BehaviorSubject(null);
    this.notificaciones$ = this.obtenerNotificaciones$.pipe(
      switchMap(query => {

        return (!query || query === null) ?
          [] :
          this.db.list(
            'notificaciones/' + query.nivel + '/' + query.idPropietario,
            ref => ref.orderByChild('codEntidad').equalTo(query.codigoEntidad)
          ).snapshotChanges(['child_added']).pipe(
            takeUntil(this.unsubscribeAction$)
          );
      }
      ),
      catchError(error => throwError(error))
    );
  }

  configurarAccionUnsubscribe(): void {
    this.unsubscribe$ = new Subject();
    this.unsubscribeAction$ = this.unsubscribe$.asObservable();
  }

  desconectarDeEscuchaNotificaciones(): void {
    if (this.subscripcionNotificaciones$) {
      this.unsubscribe$.next();
      this.subscripcionNotificaciones$.unsubscribe();
    }

    this.subscripcionNotificaciones$ = undefined;
  }

}
