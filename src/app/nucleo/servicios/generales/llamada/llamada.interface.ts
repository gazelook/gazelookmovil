import { AngularFireAction } from "@angular/fire/database";
import { DataSnapshot } from "@angular/fire/database/interfaces";
import { BehaviorSubject, Observable, Subject, Subscription } from "rxjs";
import { PerfilModel } from "dominio/modelo/entidades/perfil.model";
import { CodigoCatalogoTipoLlamada, CodigosCatalogoEstadoLlamada, CodigosCatalogoEstatusLLamada } from "./llamada.enum";

// Estado del perfil
export interface EstadoPerfil {
  id?: string,
  perfil?: PerfilModel,
  estatus?: CatalogoEstatusPerfil,
  fecha?: any,
}

export interface CatalogoEstatusPerfil {
  codigo: string
}

export interface IndicadorAgora {
  channel?: number,
  key?: number
}

export interface IndicadorAsociacion extends IndicadorAgora {
  id?: string, // Id de la asociacion
  fecha?: any
}

export interface CanalActivo {
  idCanal?: string,
  asociacion?: string,
  canal?: number
}

export interface Llamada extends CanalActivo {
  id?: string,
  estatus?: CodigosCatalogoEstatusLLamada,
  estado?: CodigosCatalogoEstadoLlamada,
  tipo?: CodigoCatalogoTipoLlamada,
  host?: string, // idPerfil
  receptor?: string, // idPerfil
  eliminaoPor?: string[],
  fecha?: any,
  busqueda?: string
}

export interface CatalogoEstatusLLamada {
  codigo: string
}

export interface CatalogoEstadoLlamada {
  codigo: string
}

export interface OpcionesLlamada {
  nombreContacto: string,
  urlMedia: string,
  forzarImagen: boolean,
  estadoLLamada: string,
  llamadaMuteada: boolean,
  videoMuteado: boolean,
  mostrarError: boolean,
  mensajeError: string,
}

export interface FirebaseQuery<T> {
  respuesta$?: Observable<AngularFireAction<DataSnapshot>[]>
  ejecutar$?: BehaviorSubject<T | null>
  subscripcion$?: Subscription
  unsubscribe$?: Subject<void>
  desconectar$?: Observable<void>
}
