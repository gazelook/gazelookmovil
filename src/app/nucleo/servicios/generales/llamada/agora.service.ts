// import { Injectable, OnDestroy } from '@angular/core'
// import { AngularFireDatabase } from '@angular/fire/database';
// import { AgoraClient, ClientEvent, NgxAgoraService, Stream, StreamEvent } from 'ngx-agora';
// import { Subject, Subscription } from 'rxjs';
// import { environment } from 'src/environments/environment';
// import { VariablesGlobales } from '../variables-globales.service';
// import { CanalActivo, IndicadorAsociacion, Llamada } from './llamada.interface';

// @Injectable({ providedIn: 'root' })
// export class AgoraService implements OnDestroy {

//     private localStream: Stream
//     private client: AgoraClient
//     public estatusCliente: boolean

//     public appId: string
//     public userUid: string | number
//     public remoteCalls: string[]
//     public connected: boolean
//     public published: boolean

//     public subscripcionRegistroLlamada$: Subscription
//     public subscripcionErrorEnLlamada$: Subscription
//     public escuchaRegistrarLlamada$: Subject<any>
//     public escuchaErrorEnLlamada$: Subject<any>

//     constructor(
//         private agoraService: NgxAgoraService,
//         private variablesGlobales: VariablesGlobales,
//         private db: AngularFireDatabase
//     ) {
//         this.configurarServicio()
//     }

//     ngOnDestroy() {
//         this.desconectar()
//     }

//     configurarServicio() {
//         this.escuchaRegistrarLlamada$ = new Subject()
//         this.escuchaErrorEnLlamada$ = new Subject()

//         this.localStream = undefined
//         this.estatusCliente = false
//         this.connected = false
//         this.published = false
//         this.remoteCalls = []
//         this.appId = (environment as any).agora ? (environment as any).agora.appId : ''
//         this.configurarCliente()
//     }

//     reiniciarServicio() {
//         this.desconectar()
//         this.configurarServicio()
//     }

//     configurarCliente() {
//         this.client = this.agoraService.createClient({ mode: 'rtc', codec: 'h264' })
//         this.client.init(this.appId, () => this.estatusCliente = true, () => this.estatusCliente = false);
//         this.manejadoresDeEventos()
//     }

//     private manejadoresDeEventos(): void {
//         this.client.on(ClientEvent.LocalStreamPublished, evt => {
//             this.published = true
//             this.localStream.muteAudio()
//             this.localStream.muteVideo()
//             this.escuchaRegistrarLlamada$.next(true)
//         })

//         this.client.on(ClientEvent.Error, error => {
//             if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
//                 this.client.renewChannelKey(
//                     '',
//                     () => {},
//                     renewError => {}
//                 );
//             }
//         })

//         this.client.on(ClientEvent.RemoteStreamAdded, evt => {
//             const stream = evt.stream as Stream
//             this.client.subscribe(stream, { audio: true, video: true }, err => {
//             })
//         })

//         this.client.on(ClientEvent.RemoteStreamSubscribed, evt => {
//             const stream = evt.stream as Stream;
//             const id = this.getRemoteId(stream);
//             if (!this.remoteCalls.length) {
//                 this.remoteCalls.push(id);
//                 setTimeout(() => stream.play(id), 1000);
//             }
//         });

//         this.client.on(ClientEvent.RemoteStreamRemoved, evt => {
//             const stream = evt.stream as Stream;
//             if (stream) {
//                 stream.stop();
//                 this.remoteCalls = [];
//             }
//         });

//         this.client.on(ClientEvent.PeerLeave, evt => {
//             const stream = evt.stream as Stream;
//             if (stream) {
//                 stream.stop()
//                 stream.close()
//                 this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`)
//             }
//         });
//     }

//     start(
//         idElementoHTML: string,
//         canalActivo: CanalActivo,
//         video: boolean = false,
//         publicarStream: boolean = true
//     ): void {
//         this.localStream = this.agoraService.createStream({ audio: true, video: video, screen: false })
//         this.asignarManejadorDeEventosLocales()
//         this.init(idElementoHTML, publicarStream, canalActivo)
//     }

//     join(
//         canalActivo: CanalActivo,
//         publicarStream: boolean
//     ) {
//         this.client.join(null, canalActivo.canal.toString(), null, (uid) => {
//             this.userUid = uid
//             if (publicarStream) {
//                 this.publish()
//             }
//         })
//     }

//     publish(): void {
//         this.client.publish(this.localStream, err => {
//         })
//     }

//     unpublish(): void {
//         if (!this.published) {
//             return
//         }

//         this.client.unpublish(this.localStream, error => {})
//         this.published = false
//     }

//     leave(): void {
//         if (this.connected) {
//             this.client.leave(
//                 () => {
//                     this.connected = false;
//                     this.published = false;
//                     this.remoteCalls = [];
//                     this.localStream.stop()
//                     this.localStream.close()
//                 },
//                 err => {

//                 }
//             );
//         } else {
//             this.agoraService.AgoraRTC.Logger.warning('Local client is not connected to channel.');
//         }
//     }

//     protected init(
//         idElementoHTML: string,
//         publicarStream: boolean,
//         canalActivo: CanalActivo,
//     ): void {
//         this.localStream.init(
//             () => {
//                 // The user has granted access to the camera and mic.
//                 this.connected = true
//                 this.localStream.play(idElementoHTML)
//                 if (publicarStream){
//                     this.join(canalActivo, publicarStream)
//                 }
//             },
//             err => {
//                 this.escuchaErrorEnLlamada$.next(err)
//             }
//         );
//     }

//     private asignarManejadorDeEventosLocales(): void {
//         this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
//         })

//         // The user has denied access to the camera and mic.
//         this.localStream.on(StreamEvent.MediaAccessDenied, () => {

//         })
//     }

//     private getRemoteId(stream: Stream): string {
//         return `agora_remote-${stream.getId()}`;
//     }

//     estadoDelAudio(
//         mutear: boolean
//     ) {
//         if (mutear) {
//             this.localStream.muteAudio()
        
//             return
//         }

//         this.localStream.unmuteAudio()
//     }

//     estadoDelVideo(
//         mutear: boolean
//     ) {
//         if (mutear) {
//             this.localStream.muteVideo()
//             return
//         }

//         this.localStream.unmuteVideo()
//     }

//     cerrarStream() {
//         if (!this.localStream) {
//             return
//         }

//         if (this.localStream.isPlaying) {
//             this.localStream.stop()
//         }
//         this.localStream.close()
//     }

//     desconectar() {
//         if (this.subscripcionRegistroLlamada$) {
//             this.subscripcionRegistroLlamada$.unsubscribe()
//         }

//         if (this.subscripcionErrorEnLlamada$) {
//             this.subscripcionErrorEnLlamada$.unsubscribe()
//         }

//         this.subscripcionErrorEnLlamada$ = undefined
//         this.subscripcionRegistroLlamada$ = undefined
//         this.cerrarStream()
//     }
// }
