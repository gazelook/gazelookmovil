import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { BehaviorSubject, Subject, throwError } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { ColorDeBorde, ColorDeFondo } from '@shared/diseno/enums/estilo-colores.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ComentarioRespuesta, ConfiguracionComentario } from '@shared/diseno/modelos/comentario.interface';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ComentarioIntercambioFirebaseModel } from 'dominio/modelo/entidades/comentario-intercambio.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { IndicadorTotalComentarios } from 'src/app/presentacion/compra-intercambio/intercambio/publicar-intercambio/publicar-intercambio.component';
import { CodigosCatalogosEstadoComentarioIntercambio } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-comentario.enum';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { FirebaseQuery } from '@core/servicios/generales/llamada/llamada.interface';

@Injectable({ providedIn: 'root' })
export class ComentariosIntercambioFirebaseService implements OnDestroy {

  // Comentarios
  public limiteInicial: number
  public paginacionComentarios: number

  public historico: FirebaseQuery<QueryPorEstado>
  public comentarios: FirebaseQuery<string>
  public paginacion: FirebaseQuery<string>
  public totalComentarios: FirebaseQuery<QueryPorEstado>

  constructor(
    private db: AngularFireDatabase,
    private generadorId: GeneradorId,
    private albumNegocio: AlbumNegocio,
  ) {
    this.limiteInicial = 1
    this.paginacionComentarios = 30
    // Inicializar
    this.historico = {}
    this.comentarios = {}
    this.paginacion = {}
    this.totalComentarios = {}
    // Metodos
    this.configurarUnsubscribe()
    this.configurarQueryHitorico()
    this.configurarQueryComentarios()
    this.configurarQueryPaginacion()
    this.configurarQueryTotalComentarios()
  }

  ngOnDestroy(): void {
    this.desconectar(OrigenConexion.DE_TODO)
  }

  configurarUnsubscribe() {
    this.historico.unsubscribe$ = new Subject()
    this.historico.desconectar$ = this.historico.unsubscribe$.asObservable()

    this.comentarios.unsubscribe$ = new Subject()
    this.comentarios.desconectar$ = this.comentarios.unsubscribe$.asObservable()

    this.paginacion.unsubscribe$ = new Subject()
    this.paginacion.desconectar$ = this.comentarios.unsubscribe$.asObservable()

    this.totalComentarios.unsubscribe$ = new Subject()
    this.totalComentarios.desconectar$ = this.comentarios.unsubscribe$.asObservable()
  }

  desconectarDe(query: any): any {
    if (query.subscripcion$) {
      query.unsubscribe$.next()
      query.subscripcion$.unsubscribe()
    }

    query.subscripcion$ = undefined
    return query
  }

  desconectar(
    origen: OrigenConexion
  ) {
    switch (origen) {
      case OrigenConexion.HISTORICO:
        this.historico = this.desconectarDe(this.historico)
        break
      case OrigenConexion.COMENTARIOS:
        this.comentarios = this.desconectarDe(this.comentarios)
        break
      case OrigenConexion.PAGINACION:
        this.paginacion = this.desconectarDe(this.paginacion)
        break
      case OrigenConexion.TOTAL_COMENTARIOS:
        this.totalComentarios = this.desconectarDe(this.totalComentarios)
        break
      case OrigenConexion.DE_TODO:
        this.historico = this.desconectarDe(this.historico)
        this.comentarios = this.desconectarDe(this.comentarios)
        this.paginacion = this.desconectarDe(this.paginacion)
        this.totalComentarios = this.desconectarDe(this.totalComentarios)
        break
    }
  }

  configurarQueryHitorico() {
    this.historico.ejecutar$ = new BehaviorSubject(null)
    this.historico.respuesta$ = this.historico.ejecutar$.pipe(
      switchMap(data =>
        (data && data !== null) ?
          this.db.list(
            'comentariosIntercambio/' + data.idIntercambio,
            ref => ref.orderByChild('estado/codigo').equalTo(data.estado)
          ).snapshotChanges().pipe(
            takeUntil(this.historico.desconectar$)
          )
          : []
      ),
      catchError(error => throwError(error))
    )
  }

  configurarQueryComentarios() {
    this.comentarios.ejecutar$ = new BehaviorSubject(null)
    this.comentarios.respuesta$ = this.comentarios.ejecutar$.pipe(
      switchMap(idIntercambio =>
        (idIntercambio && idIntercambio !== null) ?
          this.db.list(
            'comentariosIntercambio/' + idIntercambio,
            ref => ref.orderByChild('fechaCreacionFirebase').limitToLast(this.limiteInicial)
          ).snapshotChanges().pipe(
            takeUntil(this.comentarios.desconectar$)
          )
          : []
      ),
      catchError(error => throwError(error))
    )
  }

  configurarQueryPaginacion() {
    this.paginacion.ejecutar$ = new BehaviorSubject(null)
    this.paginacion.respuesta$ = this.paginacion.ejecutar$.pipe(
      switchMap(idIntercambio =>
        (idIntercambio && idIntercambio !== null) ?
          this.db.list(
            'comentariosIntercambio/' + idIntercambio,
            ref => ref.orderByChild('fechaCreacionFirebase').limitToLast(this.paginacionComentarios)
          ).snapshotChanges().pipe(
            takeUntil(this.paginacion.desconectar$)
          )
          : []
      ),
      catchError(error => throwError(error))
    )
  }

  configurarQueryTotalComentarios() {
    this.totalComentarios.ejecutar$ = new BehaviorSubject(null)
    this.totalComentarios.respuesta$ = this.totalComentarios.ejecutar$.pipe(
      switchMap(data =>


        (data && data !== null) ?
          this.db.list(
            'comentariosIntercambio/' + data.idIntercambio,
            ref => ref.orderByChild('estado/codigo').equalTo(data.estado)
          ).snapshotChanges().pipe(
            takeUntil(this.totalComentarios.desconectar$)
          )
          : []

      ),
      catchError(error => throwError(error))
    )
  }

  // Metodos para configurar items de la lista
  ordenarComentariosDeFormaDescendente(
    listaComentariosFirebase: Array<ComentarioIntercambioFirebaseModel>
  ): Array<ComentarioIntercambioFirebaseModel> {
    const aux = listaComentariosFirebase.sort((a, b) => a.fechaCreacionFirebase - b.fechaCreacionFirebase)
    // return aux.reverse()
    return aux
  }

  configurarListaDeFechasComentarios(
    listaDeFechas: Date[],
    listaComentariosFirebase: Array<ComentarioIntercambioFirebaseModel>
  ): Date[] {
    listaComentariosFirebase.forEach(comentario => {
      const date: Date = new Date(comentario.fechaCreacionFirebase)
      let pos = -1

      listaDeFechas.forEach((fecha, i) => {
        if (this.compararFechas(date, fecha)) {
          pos = i
        }
      })

      if (pos < 0) {
        listaDeFechas.push(date)
      }
    })

    const aux = listaDeFechas.sort((a, b) => a.getTime() - b.getTime())
    // listaDeFechas = aux.reverse()
    listaDeFechas = aux
		return listaDeFechas
  }

  crearConfigurarDeLosComentarios(
    listaDeFechas: Date[],
    listaComentariosFirebase: Array<ComentarioIntercambioFirebaseModel>,
    listaConfiguracionComentarios: Array<ConfiguracionComentario>,
    idPropietarioProyecto: string,
    idPerfilSeleccionado: string,
    inglesSiempreActivoEnComentarios: boolean,
    eventoTap: Function,
    eventoTapPerfil: Function
  ): Array<ConfiguracionComentario> {
    let pos = 0
    listaConfiguracionComentarios = []
    listaDeFechas.forEach(fecha => {
      let idAnterior = ''
      let itemAnterior: ConfiguracionComentario = {}

      listaComentariosFirebase.forEach(comentario => {
        if (this.compararFechas(fecha, new Date(comentario.fechaCreacionFirebase))) {
          if (
            !comentario.coautor.coautor._id ||
            idAnterior !== comentario.coautor.coautor._id ||
            idAnterior.length === 0
          ) {
            if (idAnterior.length > 0) {
              pos += 1
            }

            idAnterior = comentario.coautor.coautor._id || 'no-existente'
            itemAnterior = {
              fecha: fecha,
              coautor: comentario.coautor.coautor,
              idInterno: this.generadorId.generarIdConSemilla(),
              itemCirculo: this.configurarItemCirculo(comentario.coautor.coautor),
              esPropietario: comentario.coautor.coautor._id === idPerfilSeleccionado,
              puedeBorrar: idPropietarioProyecto === idPerfilSeleccionado || idPerfilSeleccionado === comentario.coautor.coautor._id,
              inglesActivo: inglesSiempreActivoEnComentarios,
              comentarios: [
                {
                  idPerfilRespuesta: comentario.idPerfilRespuesta,
                  comentario: comentario,
                  itemCirculo: (comentario.idPerfilRespuesta && comentario.idPerfilRespuesta !== null) ? this.configurarItemCirculo(comentario.idPerfilRespuesta) : null,
                }
              ],
              couatorSeleccionado: false,
              eventoTap: eventoTap,
              eventoTapEnPerfil: eventoTapPerfil
            }

            listaConfiguracionComentarios[pos] = itemAnterior
          } else {
            listaConfiguracionComentarios[pos].comentarios.push({
              idPerfilRespuesta: comentario.idPerfilRespuesta,
              comentario: comentario,
              itemCirculo: (comentario.idPerfilRespuesta && comentario.idPerfilRespuesta !== null) ? this.configurarItemCirculo(comentario.idPerfilRespuesta) : null
            })

            listaConfiguracionComentarios[pos].comentarios = this.ordenarComentariosPorFechaEnItemConfiguracion(
              listaConfiguracionComentarios[pos].comentarios
            )

          }
        }
      })

      pos += 1
    })
    return listaConfiguracionComentarios
  }

  ordenarComentariosPorFechaEnItemConfiguracion(comentarios: Array<ComentarioRespuesta>) {
    // return comentarios.sort((a, b) => a.comentario.fechaCreacionFirebase - b.comentario.fechaCreacionFirebase).reverse()
		return comentarios.sort((a, b) => a.comentario.fechaCreacionFirebase - b.comentario.fechaCreacionFirebase)
  }

  compararFechas(a: Date, b: Date): boolean {
    if (
      a.getFullYear() === b.getFullYear() &&
      a.getMonth() === b.getMonth() &&
      a.getDate() === b.getDate()
    ) {
      return true
    }

    return false
  }

  obtenerUrlMediaParaItemCircular(
    perfil: PerfilModel
  ): { url: string, fileDefault: boolean } {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDelPerfil(CodigosCatalogoTipoAlbum.PERFIL, perfil)

    return (
      album &&
      album.portada &&
      album.portada.principal &&
      album.portada.principal.url
    ) ?
      {
        url: album.portada.principal.url,
        fileDefault: album.portada.principal.fileDefault
      } : {
        url: '',
        fileDefault: false
      }
  }

  configurarItemCirculo(
    perfil: PerfilModel
  ): ItemCircularCompartido {
    const data: { url: string, fileDefault: boolean } = this.obtenerUrlMediaParaItemCircular(perfil)

    return {
      id: perfil._id,
      idInterno: this.generadorId.generarIdConSemilla(),
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      esVisitante: true,
      mostrarBoton: false,
      mostrarLoader: data.url.length > 0,
      urlMedia: data.url,
      textoBoton: 'Upload Photos',
      usoDelItem: data.fileDefault ? UsoItemCircular.CIRCARITACONTACTODEFECTO : UsoItemCircular.CIRCONTACTO,
      mostrarCorazon: false,
      esBotonUpload: false,
      capaOpacidad: {
        mostrar: false
      },
      activarClick: false,
      activarDobleClick: false,
      activarLongPress: false,
    }
  }

  actualizarIndicadorDelTotalDeComentarios(
    idComentarioEliminado: string,
    indicadorTotalComentarios: IndicadorTotalComentarios
  ): IndicadorTotalComentarios {
    const index = indicadorTotalComentarios.comentarios.findIndex(e => e === idComentarioEliminado)

    if (index >= 0) {
      indicadorTotalComentarios.total -= 1
      indicadorTotalComentarios.comentarios.splice(index, 1)
    }

    return indicadorTotalComentarios
  }

  actualizarListaDeConfiguracionDeComentarios(
    idComentarioEliminado: string,
    listaDeFechas: Array<Date>,
    listaConfiguracionComentarios: Array<ConfiguracionComentario>
  ): {
    listaDeFechas: Array<Date>,
    listaConfiguracionComentarios: Array<ConfiguracionComentario>
  } {
    let posGrupo = -1

    listaConfiguracionComentarios.forEach((grupo, pos) => {
      const index = grupo.comentarios.findIndex(e => e.comentario.id === idComentarioEliminado)
      if (index >= 0) {
        grupo.comentarios.splice(index, 1)

        if (grupo.comentarios.length === 0) {
          posGrupo = pos
        }
      }
    })

    if (posGrupo >= 0) {
      const fechaEliminada = listaConfiguracionComentarios[posGrupo].fecha
      listaConfiguracionComentarios.splice(posGrupo, 1)

      let hayMasComentarios = false
      listaConfiguracionComentarios.forEach(configuracion => {
        if (this.compararFechas(fechaEliminada, configuracion.fecha)) {
          hayMasComentarios = true
        }
      })

      if (!hayMasComentarios) {
        let indexFecha = -1
        listaDeFechas.forEach((fecha, pos) => {
          if (this.compararFechas(fecha, fechaEliminada)) {
            indexFecha = pos
          }
        })

        if (indexFecha >= 0) {
          listaDeFechas.splice(indexFecha, 1)
        }
      }
    }

    return {
      listaDeFechas: listaDeFechas,
      listaConfiguracionComentarios: listaConfiguracionComentarios
    }

  }

}

export interface QueryPorEstado {
  idIntercambio: string,
  estado: CodigosCatalogosEstadoComentarioIntercambio
}

export enum OrigenConexion {
  HISTORICO = 'a',
  COMENTARIOS = 'b',
  PAGINACION = 'c',
  TOTAL_COMENTARIOS = 'd',
  DE_TODO = 'e'
}
