import { Subject } from 'rxjs';
import { CodigosCatalogoTipoMoneda } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-moneda.enum';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { Injectable } from '@angular/core'
@Injectable({ providedIn: 'root' })
export class VariablesGlobales {
	// Generales
	public mostrarMundo: boolean // Para ocultar o mostrar el mundo
	public semillaItemsId: number // Semilla para genera el id de los items
	// Valores base para valor estimado del proyecto
	public valorNetoInicialParaProyectos: string
	public valorFormateadoInicialParaProyectos: string
	public placeholderMonedaPicker: string
	// Valores base para monto a pagar por la suscripcion mas cuota extra
	public valorNetoInicialParaSuscripcion: string
	public valorFormateadoInicialParaSuscripcion: string
	// Monto base de la supscripcion y tipo de moneda
	public montoBaseSuspcripcion: number
	public tipoDeMonedaBase: CodigosCatalogoTipoMoneda
	public tamanoMaximoArchivos: number

	public mostrarAvisoIdiomas: boolean
	public mostrarGif: boolean
	public $cambioIdiomaGif: Subject<string>
	public mostrarCapaGif: boolean
	public $cambiarALandingPage: Subject<boolean>
	
	// Status Perfiles
	public cambiosDeEstadoEnElPerfil$: Subject<PerfilModel>
	public nuevoPerfilAnadido$: Subject<PerfilModel>

	constructor() {
		// Generales
		this.mostrarMundo = true
		this.semillaItemsId = 0
		// Valores base para valor estimado del proyecto
		this.valorNetoInicialParaProyectos = '0000000000'
		this.valorFormateadoInicialParaProyectos = '0.000.000.000'
		this.placeholderMonedaPicker = '€/$/...'
		this.valorNetoInicialParaSuscripcion = '0'
		this.valorFormateadoInicialParaSuscripcion = '0'
		this.montoBaseSuspcripcion = 18
		this.tipoDeMonedaBase = CodigosCatalogoTipoMoneda.EUR
		this.tamanoMaximoArchivos = 160000000
		// Test
		this.mostrarAvisoIdiomas = false
		this.mostrarGif = false
		this.$cambioIdiomaGif = new Subject<string>()
		this.mostrarCapaGif = false
		this.$cambiarALandingPage = new Subject<boolean>()
		// Cambio de estado en el perfil
		this.cambiosDeEstadoEnElPerfil$ = new Subject()
		this.nuevoPerfilAnadido$ = new Subject()
	}
}
