import { Injectable } from '@angular/core';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';

@Injectable({ providedIn: 'root' })
export class ParamsServiceLocal {

  constructor(
    private metodosSessionStorageService: MetodosSessionStorageService
  ) { }

  guardarParams(params: any) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.PARAMS, params)
  }

  obtenerParams(): any {
    this.metodosSessionStorageService.obtener(LlavesSessionStorage.PARAMS)
  }
}
