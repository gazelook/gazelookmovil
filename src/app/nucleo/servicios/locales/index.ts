import { LlavesSessionStorage } from './llaves/session-storage.enum';
import { LlavesLocalStorage } from './llaves/local-storage.enum';

export const servicesLocales: any[] = [
    LlavesLocalStorage,
    LlavesSessionStorage
];

export * from './llaves/session-storage.enum';
export * from './llaves/local-storage.enum';
