import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
@Injectable({
    providedIn: 'root'
})
export class MapaService {
    mapbox = (mapboxgl as typeof mapboxgl);
    map: mapboxgl.Map;
    style = `mapbox://styles/mapbox/streets-v11`;
    // Coordenadas de la localización donde queremos centrar el mapa
    // lat = -4.003151393940854;
    // lng = -79.20034025720358;
    zoom = 10;


    constructor() {
        // Asignamos el token desde las variables de entorno
        this.mapbox.accessToken = environment.mapBoxToken;
    }

    buildMap(latitude: number, longitude: number) {
        // const draw = new MapboxDraw({
        //     defaultMode: "draw_circle",
        //     userProperties: true,
        //     modes: {
        //         ...MapboxDraw.modes,
        //         draw_circle: CircleMode,
        //         drag_circle: DragCircleMode,
        //         direct_select: DirectMode,
        //         simple_select: SimpleSelectMode
        //     }
        // });
        // draw.changeMode('draw_circle', { initialRadiusInKm: 0.5 });

        this.map = new mapboxgl.Map({
            container: 'map',
            style: this.style,
            zoom: this.zoom,
            center: [-79.20373646827943, -3.9911000256111464]
        });
        this.map.addControl(new mapboxgl.NavigationControl());

        // this.map.addControl(new mapboxgl.GeolocateControl({
        //     positionOptions: {
        //         enableHighAccuracy: true
        //     },
        //     trackUserLocation: true
        // }));

        // this.map.addSource('my-data', {
        //     type: 'vector',
        //     url: 'mapbox://myusername.tilesetid'
        //   });
        // this.map = new mapboxgl.Map({
        //     container: 'map',
        //     style: 'mapbox://styles/mapbox/light-v10',
        //     zoom: 12,
        //     center: [-87.622088, 41.878781]
        // });
        // this.map.on('load', function () {


        //     // Add Mapillary sequence layer.
        //     // https://www.mapillary.com/developer/tiles-documentation/#sequence-layer
        //     this.map.addSource('mapillary', {
        //         'type': 'vector',
        //         'tiles': [
        //             'https://d25uarhxywzl1j.cloudfront.net/v0.1/{z}/{x}/{y}.mvt'
        //         ],
        //         'minzoom': 6,
        //         'maxzoom': 14
        //     });
        //     this.map.addLayer(
        //         {
        //             'id': 'mapillary',
        //             'type': 'line',
        //             'source': 'mapillary',
        //             'source-layer': 'mapillary-sequences',
        //             'layout': {
        //                 'line-cap': 'round',
        //                 'line-join': 'round'
        //             },
        //             'paint': {
        //                 'line-opacity': 0.6,
        //                 'line-color': 'rgb(53, 175, 109)',
        //                 'line-width': 2
        //             }
        //         },
        //         'waterway-label'
        //     );
        // });

        this.map.addControl(new mapboxgl.NavigationControl());
        // setTimeout(() => this.mapaCrearCirulo(), 3000);

        // let a = this.createGeoJSONCircle([-79.2037364682794, 3.9911000256111464], 0.5)


        // this.map.addSource("polygon", a)
        // this.map.addLayer({
        //     "id": "polygon",
        //     "type": "fill",
        //     "source": "polygon",
        //     "layout": {},
        //     "paint": {
        //         "fill-color": "blue",
        //         "fill-opacity": 0.6
        //     },

        // });



        // this.map.on('load', function(){
        //     this.map.addSource("polygon", a)



        // })

        // this.map.getSource('circle').setData(this.createGeoJSONCircle([-93.6248586, 41.58527859], 1).data);
        // let el = document.createElement('div');
        // el.className = 'marker';
        // el.style.backgroundImage = "url('https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/icons/icon-boton-cancelar.svg')"
        // el.style.backgroundRepeat = 'no-repeat'
        // el.style.backgroundSize = '100%';
        // el.style.backgroundSize = 'cover'
        // // el.style.width = '20%'
        // el.style.height = '25%'

        // let marker = new mapboxgl.Marker({
        //     draggable: true,
        //     // element: el,
        //     scale: 1

        // })
        //     .setLngLat([-79.20373646827943, -3.9911000256111464])
        //     .addTo(this.map);

        // function onDragEnd() {
        //     var lngLat = marker.getLngLat();
        //     // coordinates.style.display = 'block';
        //     // coordinates.innerHTML =
        //     //   'Longitude: ' + lngLat.lng + '<br />Latitude: ' + lngLat.lat;
        // }

        // marker.on('dragend', onDragEnd);
    }

    mapaCrearCirulo() {

        this.map.on('load', function () {
            // Add Mapillary sequence layer.
            // https://www.mapillary.com/developer/tiles-documentation/#sequence-layer
            this.map.addSource('mapillary', {
                'type': 'vector',
                'tiles': [
                    'https://d25uarhxywzl1j.cloudfront.net/v0.1/{z}/{x}/{y}.mvt'
                ],
                'minzoom': 6,
                'maxzoom': 14
            });
            this.map.addLayer(
                {
                    'id': 'mapillary',
                    'type': 'line',
                    'source': 'mapillary',
                    'source-layer': 'mapillary-sequences',
                    'layout': {
                        'line-cap': 'round',
                        'line-join': 'round'
                    },
                    'paint': {
                        'line-opacity': 0.6,
                        'line-color': 'rgb(53, 175, 109)',
                        'line-width': 2
                    }
                },
                'waterway-label'
            );
        });
        this.map.addControl(new mapboxgl.NavigationControl());
    }

    createGeoJSONCircle(center?, radiusInKm?, points?): mapboxgl.AnySourceData {
        if (!points) points = 64;

        var coords = {
            latitude: center[1],
            longitude: center[0]
        };

        var km = radiusInKm;

        var ret = [];
        var distanceX = km / (111.320 * Math.cos(coords.latitude * Math.PI / 180));
        var distanceY = km / 110.574;

        var theta, x, y;
        for (var i = 0; i < points; i++) {
            theta = (i / points) * (2 * Math.PI);
            x = distanceX * Math.cos(theta);
            y = distanceY * Math.sin(theta);

            ret.push([coords.longitude + x, coords.latitude + y]);
        }
        ret.push(ret[0]);


        return {
            "type": "geojson",
            "data": {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [ret]
                    }
                }]
            }
        };
    };
}
