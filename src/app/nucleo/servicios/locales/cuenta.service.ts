import { DocumentosUsuarioModel } from 'dominio/modelo/entidades/documentos-usuario.model';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { Injectable } from '@angular/core';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';

@Injectable({ providedIn: 'root' })
export class CuentaServiceLocal {

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService,
    private metodosSesionStorage: MetodosSessionStorageService,
  ) {

  }

  // Guardar usuario en el storage
  guardarUsuarioEnLocalStorage(usuario: UsuarioModel): void {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.USUARIO, usuario);
  }

  // OBtener usuario del storage
  obtenerUsuarioDelLocalStorage(): UsuarioModel {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.USUARIO);
  }

  removerUsuarioDelLocalStorage(): void {
    return this.metodosLocalStorageService.remover(LlavesLocalStorage.USUARIO);
  }

  // Guardar DNI sesion
  guardarIdFotoDniSessionStorage(documento: DocumentosUsuarioModel): void {
    return this.metodosSesionStorage.guardar(LlavesLocalStorage.FOTO_DNI, documento);
  }
  removerIdFotoDniSessionStorage(): void {
    return this.metodosSesionStorage.remover(LlavesLocalStorage.FOTO_DNI);
  }
  obtenerIdFotoDniSessionStorage(): any {
    return this.metodosSesionStorage.obtener(LlavesLocalStorage.FOTO_DNI);
  }

  // Guardar usuario en el storage
  guardarUsuarioEnSessionStorage(usuario: UsuarioModel): void {
    this.metodosSesionStorage.guardar(LlavesSessionStorage.USUARIO, usuario);
  }

  // OBtener usuario del storage
  obtenerUsuarioDelSessionStorage(): UsuarioModel {
    return this.metodosSesionStorage.obtener(LlavesSessionStorage.USUARIO);
  }

  removerUsuarioDelSessionStorage(): void {
    return this.metodosSesionStorage.remover(LlavesSessionStorage.USUARIO);
  }

  eliminarSessionStorage(): void {
    this.metodosSesionStorage.eliminarSessionStorage();
  }

  guardarFirebaseUIDEnLocalStorage(uid: string): void {
    this.metodosLocalStorageService.guardar(LlavesLocalStorage.FIREBASE_UID, uid);
  }

  obtenerFirebaseUIDEnLocalStorage(): string {
    return this.metodosLocalStorageService.obtener(LlavesLocalStorage.FIREBASE_UID);
  }
}
