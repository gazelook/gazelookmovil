export enum LlavesSessionStorage {
    USUARIO = 'usuario',
    PARAMS = 'params',
    ALBUM_ACTIVO = 'album_activo',
    PERFIL_ACTIVO = 'perfil_activo',
    PROYECTO_ACTIVO = 'proyecto_activo',
    INTERCAMBIO_ACTIVO = 'intercambio_activo',
    TIPO_PERFIL_ACTIVO = 'tipo_perfil_activo',
    HITORIAL_PERFILES = 'historial_perfiles',
    LLAMADA_ACTIVA = 'llamada_activa',
    MENU_ACTIVO = 'a',
    PAGINAS = 'b',
    PAGINAS_ACTIVAS = 'c',
    MENUS_YA_ABIERTOS = 'd',
    PREGUNTAS = 'e',
    PREGUNTAS_YA_ABIERTAS = 'f',
    ANIMACION_FINALIZADA = 'animFin'
}