import { Injectable } from '@angular/core';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';

@Injectable({ providedIn: 'root' })
export class NoticiaServiceLocal {

  constructor(
    private metodosSessionStorageService: MetodosSessionStorageService
  ) {   }

  guardarNoticiaActivaEnSessionStorage(noticia: NoticiaModel) {
    this.metodosSessionStorageService.guardar(LlavesLocalStorage.NOTICIA_ACTIVA, noticia)
  }

  obtenerNoticiaActiviaDelSessionStorage(): NoticiaModel {
    return this.metodosSessionStorageService.obtener(LlavesLocalStorage.NOTICIA_ACTIVA)
  }

  removerNoticiaActivaDelSessionStorage() {
    this.metodosSessionStorageService.remover(LlavesLocalStorage.NOTICIA_ACTIVA)
  }
}
