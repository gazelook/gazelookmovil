import { Injectable } from '@angular/core';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';

@Injectable({ providedIn: 'root' })
export class PerfilServiceLocal {

  constructor(
    private metodosLocalStorageService: MetodosLocalStorageService,
    private metodosSessionStorageService: MetodosSessionStorageService,
  ) {   }

  guardarPerfilActivoEnSessionStorage(perfil: PerfilModel) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.PERFIL_ACTIVO, perfil)
  }

  obtenerPerfilActivoDelSessionStorage(): PerfilModel {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.PERFIL_ACTIVO)
  }

  removerPerfilActivoDelSessionStorage() {
    this.metodosSessionStorageService.remover(LlavesSessionStorage.PERFIL_ACTIVO)
  }

  guardarTipoPerfilActivo(tipoPerfil: CatalogoTipoPerfilModel) {
    this.metodosSessionStorageService.guardar(LlavesSessionStorage.TIPO_PERFIL_ACTIVO, tipoPerfil)
  }

  obtenerTipoPerfilActivo(): CatalogoTipoPerfilModel {
    return this.metodosSessionStorageService.obtener(LlavesSessionStorage.TIPO_PERFIL_ACTIVO)
  }

  removerTipoPerfilActivoDelSessionStorage() {
    this.metodosSessionStorageService.remover(LlavesSessionStorage.TIPO_PERFIL_ACTIVO)
  }
}
