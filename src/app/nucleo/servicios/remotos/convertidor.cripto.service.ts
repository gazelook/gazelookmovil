import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APIGAZE} from '@core/servicios/remotos/rutas';
import {Observable} from 'rxjs';
import {RespuestaRemota} from '@core/util/respuesta';

@Injectable({providedIn: 'root'})
export class ConvertidorCriptoService {
  constructor(private http: HttpClient) {
  }


  public convertirMonedaACripto(amount: number, convertID: number, symbol: string): Observable<RespuestaRemota<number>> {
    const url = `${APIGAZE.BASE}conversion-coinmarketcap`;
    const data = {
      amount,
      convert_id: convertID,
      symbol
    };
    return this.http.post<any>(url, data);
  }
}
