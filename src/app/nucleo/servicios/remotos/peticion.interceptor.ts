import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError, switchMap, delay, debounceTime } from 'rxjs/operators';
import { LocalStorage } from '@core/servicios/locales/local-storage.service';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { Cuenta } from '@core/servicios/remotos/rutas/cuenta.enum';
import { environment } from 'src/environments/environment';

@Injectable()
export class PeticionInterceptor implements HttpInterceptor {
    peticion: Observable<HttpEvent<MediaDeviceInfo>>
    constructor(
        private localStorage: LocalStorage,
        private cuentaNegocio: CuentaNegocio
    ) {   }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<MediaDeviceInfo>> {
        const apiKey = environment.apiKey // Aqui se debe obtener el ApiKey

        if (apiKey) {
            //api key de autorizacion para consumo del api
            req = req.clone({
                headers: req.headers.set('apiKey', apiKey)
            });
        }


        if (!req.url.startsWith("http") || req.url == (APIGAZE.BASE.toString() + Cuenta.REFRESCAR_TOKEN.toString())) {
            return next.handle(req).pipe(map((event: HttpEvent<any>) => {
                return event;
            }, (err: HttpErrorResponse) => {
                const message = err.error.message;
                return throwError(message);
            }))
        }

        return this.cuentaNegocio.obtenerTokenAutenticacion()
        .pipe(
            switchMap((token) => {

                const idioma = this.localStorage.obtenerIdiomaLocal() // Se obtiene el idioma

                if (idioma) {
                    //Idioma seleccionado por el usuario
                    req = req.clone({
                        headers: req.headers.set('idioma', idioma.codNombre.toLowerCase())
                    });
                }

                //Se agrega el token
                if (token) {
                    //Token de autenticacion
                    req = req.clone({
                        headers: req.headers.set('Authorization', `Bearer ${token}`)
                    });
                }

                return next.handle(req).pipe(
                    map((event: HttpEvent<any>) => {
                        if (event instanceof HttpResponse) {
                            if (event.body) {
                                if (event.body.codigoEstado) {
                                    if (event.body.codigoEstado > 401) {
                                        throw new Error(event.body.respuesta.mensaje)
                                    }
                                    if (event.body.codigoEstado === 401) {
                                        this.cuentaNegocio.noAutorizado()
                                        throw new Error(event.body.respuesta.mensaje)
                                    }
                                }
                            }
                        }
                        return event;
                    }),
                    catchError((error: HttpErrorResponse) => {
                        if (error.status || error['codigoEstado'] > 399) {
                            //await this.internacionalizacionNegocio.obtenerTextoLlave('enviar')
                            if (error.status === 401 || error['codigoEstado'] === 401) {
                                return throwError("No tienes autorizacion");
                            } else {
                                if (error.status === 404 || error['codigoEstado'] === 404) {
                                    return throwError("text37");
                                } else {
                                    return throwError("text37");
                                }
                            }
                        }
                        return throwError(error)
                    }));
            }))
    }
}
