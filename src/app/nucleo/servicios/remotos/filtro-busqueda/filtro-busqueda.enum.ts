export enum FiltroGeneral {
    FECHA = 'fecha',
    ALFA = 'alfa',
}

export enum FiltroProyNoti {
    VOTO = 'voto',
}

export enum FiltroBusquedaProyectos {
    NUEVOS_PROYECTO = 'nuevos_proyectos',
    MENOS_VOTADOS = 'menos_votados',
    MAS_VOTADOS = 'mas_votados'
}