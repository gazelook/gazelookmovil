import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APIGAZE} from '@core/servicios/remotos/rutas';
import {Observable} from 'rxjs';
import {RespuestaRemota} from '@core/util/respuesta';

@Injectable({providedIn: 'root'})
export class LandingService {
  constructor(private http: HttpClient) {
  }


  public mostrarTextoLandingSegundaPagina(): Observable<RespuestaRemota<any>> {
    const url = `${APIGAZE.BASE}verificar-ingreso`;
  
    return this.http.get<any>(url);
  }
}
