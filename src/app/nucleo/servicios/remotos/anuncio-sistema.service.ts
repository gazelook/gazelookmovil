import { AnuncioSistemaEntity } from 'dominio/entidades/anuncio-sistema.entity';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RespuestaRemota } from '@core/util/respuesta';
import { AnuncioSistema } from '@core/servicios/remotos/rutas/anuncio-sistema.enum';

@Injectable({ providedIn: 'root' })
export class AnuncioSistemaServiceRemoto {

    constructor(
        private http: HttpClient
    ) {

    }

    obtenerAnunciosSistema(perfil: string,): Observable<HttpResponse<RespuestaRemota<Array<AnuncioSistemaEntity>>>> {
        return this.http.get<RespuestaRemota<AnuncioSistemaEntity[]>>(
            APIGAZE.BASE +
            AnuncioSistema.OBTENER
            + '/?'
            + 'limite=' + `100`
            + '&pagina=' + `1`
            + '&perfil=' + `${perfil}`,
            { observe: 'response' }
        )
    }

    obtenerAnunciosSistemaPublicitario(perfil: string,): Observable<HttpResponse<RespuestaRemota<Array<AnuncioSistemaEntity>>>> {
        return this.http.get<RespuestaRemota<AnuncioSistemaEntity[]>>(
            APIGAZE.BASE +
            AnuncioSistema.UNICO
            + '/?'
            + 'limite=' + `100`
            + '&pagina=' + `1`
            + '&perfil=' + `${perfil}`,
            { observe: 'response' }
        )
    }

}