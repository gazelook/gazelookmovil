import { Moneda } from '@core/servicios/remotos/rutas/moneda.enum';
import { CodigosCatalogoTipoMoneda } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-moneda.enum';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { CatalogoTipoMonedaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-moneda.entity';
import { RespuestaRemota } from '@core/util/respuesta';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Catalogo } from '@core/servicios/remotos/rutas/catalogos.enum';

@Injectable({ providedIn: 'root' })
export class TipoMonedaServiceRemoto {

    constructor(
        private http: HttpClient
    ) {   }

    obtenerCatalogoTipoMoneda() : Observable<RespuestaRemota<CatalogoTipoMonedaEntity[]>> {
        return this.http.get<RespuestaRemota<CatalogoTipoMonedaEntity[]>>(APIGAZE.BASE + Catalogo.CATALOGO_TIPO_MONEDA.toString())
    }

    convertirMontoEntreMonedas(
        monto: number,
        convertirDe: CodigosCatalogoTipoMoneda,
        convertirA: CodigosCatalogoTipoMoneda,
    ): Observable<RespuestaRemota<{ monto: string }>> {
        return this.http.post<RespuestaRemota<{ monto: string }>>(
            APIGAZE.BASE +
            Moneda.CONVERTIR_MONEDA.toString() + '/?' +
            'monto=' + monto +
            '&convertirDe=' + convertirDe +
            '&convertirA=' + convertirA,
            {}
        )
    } 

    convertirMontoEntreMonedasProyectos(
        monto: number,
        convertirDe: string,
        convertirA: string,
    ): Observable<RespuestaRemota<{ monto: string }>> {
        return this.http.post<RespuestaRemota<{ monto: string }>>(
            APIGAZE.BASE +
            Moneda.CONVERTIR_MONEDA.toString() + '/?' +
            'monto=' + monto +
            '&convertirDe=' + convertirDe +
            '&convertirA=' + convertirA,
            {}
        )
    } 

}
