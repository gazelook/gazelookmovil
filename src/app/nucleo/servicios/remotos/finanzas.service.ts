import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReporteFinanzasUsuarioEntity } from 'dominio/entidades/reporte-finanzas-usuario.entity';
import { Observable } from 'rxjs';
import { RespuestaRemota } from '@core/util/respuesta';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
import { Finanzas } from '@core/servicios/remotos/rutas/finanzas.enum';

@Injectable({ providedIn: 'root' })
export class FinanzasServiceRemoto {
  constructor(private http: HttpClient) {}

  obtenerAportaciones(
    idUsuario: string,
    fechaInicial: string,
    fechaFinal: string
  ): Observable<RespuestaRemota<any>> {
    return this.http.get<RespuestaRemota<any>>(
      APIGAZE.BASE +
        Finanzas.APORTACIONES +
        `/?idUsuario=${idUsuario}&fechaInicial=${fechaInicial}&fechaFinal=${fechaFinal}`
    );
  }

  obtenerDocumentosLegales(
    origen: string,
    fechaInicial: string,
    limite: number,
    pagina: number
  ): Observable<HttpResponse<RespuestaRemota<any>>> {
    if (fechaInicial) {
      return this.http.get<RespuestaRemota<any>>(
        APIGAZE.BASE +
          Finanzas.DOCUMENTOS_LEGALES +
          `?origen=${origen}&fechaInicial=${fechaInicial}&limite=${limite}&pagina=${pagina}`,
        { observe: 'response' }
      );
    } else {
      return this.http.get<RespuestaRemota<any>>(
        APIGAZE.BASE +
          Finanzas.DOCUMENTOS_LEGALES +
          `?origen=${origen}&limite=${limite}&pagina=${pagina}`,
        { observe: 'response' }
      );
    }
  }

  obtenerAportacionesGenerales(
    filtro: string,
    codigo: string,
    fechaInicial: string,
    fechaFinal: string
  ): Observable<RespuestaRemota<any>> {
    return this.http.get<RespuestaRemota<any>>(
      APIGAZE.BASE +
        Finanzas.APORTACIONES_GENERALES +
        `?filtro=${filtro}&fechaInicial=${fechaInicial}&fechaFinal=${fechaFinal}&campo=${codigo}`
    );
  }

  obtenerDatosInformeActualFinanzas(
    idUsuario: string,
    anio: string,
    mes?
  ): Observable<
    HttpResponse<RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>>
  > {
    if (mes) {
      return this.http.get<
        RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>
      >(
        APIGAZE.BASE +
          Finanzas.APORTACIONES +
          '/?' +
          'idUsuario=' +
          `${idUsuario}` +
          '&mes=' +
          `${mes}` +
          '&anio=' +
          `${anio}`,
        { observe: 'response' }
      );
    } else {
      return this.http.get<
        RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>
      >(
        APIGAZE.BASE +
          Finanzas.APORTACIONES +
          '/?' +
          'idUsuario=' +
          `${idUsuario}` +
          '&anio=' +
          `${anio}`,
        { observe: 'response' }
      );
    }
  }

  obtenerTarifasTotalesUsuario(
    fecha?: string,
    codigoPais?: string
  ): Observable<
    HttpResponse<RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>>
  > {
    if (fecha && codigoPais) {
      return this.http.get<
        RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>
      >(
        APIGAZE.BASE +
          Finanzas.APORTACIONES_GENERALES +
          '/?' +
          'filtro=pais' +
          '&campo=' +
          `${codigoPais}` +
          '&fecha=' +
          `${fecha}`,
        { observe: 'response' }
      );
    } else {
        if (fecha) {
          return this.http.get<
            RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>
          >(
            APIGAZE.BASE +
              Finanzas.APORTACIONES_GENERALES +
              '/?' +
              'filtro=mundial' +
              '&fecha=' +
              `${fecha}`,
            { observe: 'response' }
          );
        } 
      }
  }
}

