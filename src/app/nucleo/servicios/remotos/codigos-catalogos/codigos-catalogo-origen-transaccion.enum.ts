export enum CodigosCatalogoOrigenTransaccion {
    SUSCRIPCION_DEL_USUARIO = 'CATORI_1',
    GASTOS_OPERATIVOS = 'CATORI_2',
    DONACION = 'CATORI_3',
    GANACIA_PROYECTO = 'CATORI_4',
    ASIGNACION_DE_FONDOS = 'CATORI_5',
    CUOTA_EXTRA = 'CATORI_6',
    FONDO_RESERVADOS = 'CATORI_7',
    MONTO_SOBRANTE = 'CATORI_8',
    FONDOS_RESERVADOS_FISCALIZACION_PROYECTOS = 'CATORI_9',
    FONDOS_RESERVADOS_GAZELOOL = 'CATORI_10',
}


