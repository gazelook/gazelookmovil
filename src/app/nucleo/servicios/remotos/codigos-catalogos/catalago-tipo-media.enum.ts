export enum CodigosCatalogoTipoMedia {
    TIPO_LINK = 'CATMED_1',
    TIPO_MEDIA_COMPUESTO = 'CATMED_2',
    TIPO_MEDIA_SIMPLE = 'CATMED_3',
} 