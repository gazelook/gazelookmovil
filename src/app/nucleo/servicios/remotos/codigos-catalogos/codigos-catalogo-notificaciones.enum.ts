export enum CodigosCatalogoAccionNotificacion {
    VER_NOTIFICACION = 'ACCNOTFIR_1',
    NOTIFI_ELI_CUENTA = 'ACCNOTFIR_3',

}

export enum CodigosCatalogoEstadoNotificacion {
    ACTIVA = 'EST_241',
    ELIMINADA = 'EST_242'
}

export enum CodigosCatalogoTipoNotificacion {
    USUARIO = 'TIPNOTFIR_1',
    SISTEMA = 'TIPNOTFIR_2'
}