export enum CodigosCatalogoEntidad {
    NINGUNA = 'ENT_VACIO',
    USUARIO = 'ENT_1',
    PROYECTO = 'ENT_2',
    NOTICIA = 'ENT_3',
    PENSAMIENTO = 'ENT_4',
    TRANSACCION = 'ENT_6',
    PERFIL = 'ENT_8',
    CONTACTO = 'ENT_9',
    COMENTARIO = 'ENT_10',
    MEDIA = 'ENT_12',
    DISPOSITIVO = 'ENT_13',
    BENEFICIARIO = 'ENT_18',
    TELEFONO = 'ENT_20',
    DIRECCION = 'ENT_21',
    ASOCIACION = 'ENT_22',
    PARTICIPANTE_ASOCIACION = 'ENT_23',
    CONVERSACION = 'ENT_24',
    MENSAJE = 'ENT_25',
    VOTO_PROYECTO = 'ENT_31',
    PARTICIPANTE_PROYECTO = 'ENT_32',
    ESTRATEGIA = 'ENT_34',
    SUBSCRIPCION = 'ENT_38',
    ALBUM = 'ENT_42',
    CATALOGO_LOCALIDAD = 'ENT_46',
    TRADUCCION_NOTICIA = 'ENT_47',
    VOTO_NOTICIA = 'ENT_51',
    TRADUCCION_PROYECTO = 'ENT_55',
    INTERCAMBIO = 'ENT_100',
    COMENTARIO_INTERCAMBIO = 'ENT_115'
}

export enum AccionEntidad {
    REGISTRO = 'registro', // Cuando el usuario esta en el proceso de crear su cuenta
    CREAR = 'crear', // Cuando el usuario ya inicio sesion, y selecciona un perfil que no ha creado
    ACTUALIZAR = 'actualizar', // Cuando el usuario ya inicio sesion, y selecciona  un perfil que ya ha creado para actualizar,
    VISITAR = 'visitar', // 
}

export enum AccionAlbum {    
    VISITA = 'visita',
    CREAR = 'crear',
    ACTUALIZAR = 'actualizar',
}

