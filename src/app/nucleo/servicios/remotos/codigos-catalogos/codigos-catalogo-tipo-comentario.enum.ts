export enum CodigosCatatalogosTipoComentario {
    TIPO_FORO = 'CATIPCOM_1',
    TIPO_NORMAL = 'CATIPCOM_2',
    TIPO_ESTRATEGIA = 'CATIPCOM_3'
}

export enum CodigosCatalogosEstadoComentario {
    ACTIVA = 'EST_105',
    ELIMINADO = 'EST_106',
    HISTORICO = 'EST_107'
}

export enum CodigosCatalogosEstadoComentarioIntercambio {
    ACTIVA = 'EST_257',
    ELIMINADO = 'EST_258',
    HISTORICO = 'EST_259'
}