export enum Noticia {
    NOTICIAS_PERFIL = 'noticia-perfil',
    CREAR_NOTICIA = 'noticia',
    NOTICIA_UNICA = 'noticia-unica',
    ACTUALIZAR_NOTICIA_UNICA = 'noticia-unica',
    ELIMINAR_NOTICIA_UNICA = 'noticia-unica',
    VOTO_NOTICIA_UNICO = 'voto-noticia-unica',
    NOTICIAS_RECIENTES_PAG = 'noticias-recientes-pag',
    BUSCAR_NOTICIAS_POR_TITULO = 'buscar-noticias',
    TODAS_NOTICIAS= 'todas-noticias',
}