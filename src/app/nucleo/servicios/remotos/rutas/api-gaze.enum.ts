import { environment } from 'src/environments/environment';
export class APIGAZE {
    static BASE: string = environment.apiUrl;
    static BASE_MANTENIMIENTO: string = environment.apiMantenimiento;
    // static BASE_SOCKET: string = environment.apiSocket
    static API_KEY: string = environment.apiKey;
    static API_PAYMENTEZ = environment.apiPaymentez;
}

