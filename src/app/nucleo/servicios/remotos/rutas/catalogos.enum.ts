export enum Catalogo {
  IDIOMAS = 'catalogos/catalogo-idiomas',
  PAIS = 'catalogos/catalogo-pais',
  BUSCAR_LOCALIDAD = 'catalogos/buscar-localidad',
  METODOS_PAGOS = 'catalogos/catalogo-metodo-pago',
  TIPO_PERFIL = 'catalogos/tipo-perfiles',
  MEDIA = 'catalogos/catalogo-tipo-media',
  CATALOGO_TIPO_MONEDA = 'catalogos/catalogo-tipo-moneda',
}
