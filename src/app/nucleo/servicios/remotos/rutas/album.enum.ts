export enum Album {
    ACTUALIZAR_ALBUM = 'album/actualizar',
    AGREGAR_MEDIA_AL_ALBUM = 'album/agregar-media',
    ELIMINAR_MEDIA_DEL_ALBUM = 'album/eliminar-media',
    ACTUALIZAR_MEDIA_DEL_ALBUM = 'album/actualizar-media',
    AGREGAR_ALBUM_A_ENTIDAD = 'album/crear-album-entidad',
    ACTUALIZAR_PARAMETROS_DEL_ALBUM_EN_ENTIDAD = 'album/actualizar-propiedades'
}