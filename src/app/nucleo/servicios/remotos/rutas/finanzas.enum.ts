export enum Finanzas {
    APORTACIONES = 'aportaciones',
    DOCUMENTOS_LEGALES = 'documentos-legales',
    APORTACIONES_GENERALES = 'aportaciones-filtro'
  
}


export enum TipoAportaciones {
    MUNDIALES = 'mundial',
    PAIS = 'pais',
    LOCALIDAD = 'localidad'
}