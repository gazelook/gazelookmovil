export enum SocketCanal {
    MENSAJE = 'mensaje',
    UNIRSE_CONV = 'unirseConversacion',
    SALIR_CONV = 'salirConversacion',
    LEIDO_POR = 'leidoPor', 
    NOTIFICACION = 'notificacion',
    ELIMINAR_MENSAJE = 'eliminarMensaje',
    TRANSFERENCIA_PROYECTO = 'transferenciaEntidad'
}