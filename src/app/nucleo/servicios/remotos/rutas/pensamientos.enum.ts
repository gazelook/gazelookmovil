export enum Pensamiento {
    PENSAMIENTO = "pensamiento",   
    PENSAMIENTO_PRIVADO = "pensamiento/privado",
    PENSAMIENTO_PUBLICO = "pensamiento/publico",
    CARGAR_PENSAMIENTOS_PUBLICO = "pensamiento/misPensamientos/publicos",
    CARGAR_PENSAMIENTOS_PRIVADO = "pensamiento/misPensamientos/privados",
}