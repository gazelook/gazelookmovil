export enum ParticipanteAsociacion {
    // CONTACTOS_PERFIL = "contactos-perfil",
    CAMBIAR_ESTADO = "cambiar-estado-participante-asociacion-tipo",
    PARTICIPANTE_ASO_TIPO = 'obtener-participante-asociacion-tipo',
    CREAR_ASOCIACION = 'asociacion',
    OBTENER_PARTICIPANTE_ASO = 'obtener-participante-asociacion-tipo/perfiles-de-asociacion'
}