import {Injectable} from '@angular/core';
import {Params} from '@angular/router';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';

import {CodigosCatatalogosEstadoProyecto} from '@core/servicios/remotos/codigos-catalogos/codigos-estado-proyecto.enum';
import {CodigosCatalogoTipoProyecto} from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';

import {FiltroBusquedaProyectos} from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import {VotoProyectoEntity} from 'dominio/entidades/votoProyecto.entity';
import {Proyecto} from '@core/servicios/remotos/rutas/proyecto.enum';
import {ProyectoEntity} from 'dominio/entidades/proyecto.entity';
import {RespuestaRemota} from '@core/util/respuesta';

import {APIGAZE} from '@core/servicios/remotos/rutas/api-gaze.enum';
import {PagoEntity} from 'dominio/entidades';
import {DonacionProyectosModel} from 'dominio/modelo/entidades';

@Injectable({providedIn: 'root'})
export class ProyectoServiceRemoto {

  constructor(
    private http: HttpClient
  ) {
  }

  crearProyecto(proyecto: ProyectoEntity): Observable<RespuestaRemota<ProyectoEntity>> {

    return this.http.post<RespuestaRemota<ProyectoEntity>>(APIGAZE.BASE + Proyecto.CREAR_PROYECTO, proyecto);
  }

  obtenerInformacionDelProyecto(idProyecto: string, idPerfil: string, estado?: string): Observable<RespuestaRemota<ProyectoEntity>> {
    const params: Params = estado
      ? {idProyecto, idPerfil, estado, original: 'true'}
      : {idProyecto, idPerfil, original: 'true'};
    const url = `${APIGAZE.BASE}${Proyecto.PROYECTO_UNICO}`;
    return this.http.get<RespuestaRemota<ProyectoEntity>>(url, {params});
  }

  actualizarProyecto(proyecto: ProyectoEntity): Observable<RespuestaRemota<ProyectoEntity>> {
    return this.http.put<RespuestaRemota<ProyectoEntity>>(APIGAZE.BASE + Proyecto.ACTUALIZAR_PROYECTO, proyecto);
  }

  obtenerProyectosPerfil(idPerfil: string, limite: number, pagina: number, traducir: boolean)
    : Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {
    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(
      APIGAZE.BASE + Proyecto.PROYECTOS_PERFIL +
      `/?perfil=${idPerfil}&limite=${limite}&pagina=${pagina}&traducir=${traducir}`
      , {observe: 'response'});
  }

  obtenerProyectosRecomendados(
    perfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {
    const filtroNuevos = 'nuevos_proyectos';
    const estado = CodigosCatatalogosEstadoProyecto.PROYECTO_ACTIVO;
    const params: Params = {limite, pagina, filtro: filtroNuevos, tipo, perfil, estado};
    const url = `${APIGAZE.BASE}${Proyecto.PROYECTOS_RECOMENDADOS}`;
    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(url, {
      observe: 'response',
      params
    });
  }

  obtenerProyectosForos(
    perfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {
    const masVotados = 'mas_votados';
    const estado = CodigosCatatalogosEstadoProyecto.PROYECTO_FORO;
    const params: Params = {limite, pagina, filtro: masVotados, tipo, perfil, estado};
    const url = `${APIGAZE.BASE}${Proyecto.PROYECTOS_FOROS}`;
    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(url, {
      observe: 'response',
      params
    });
  }

  obtenerProyectosEsperaFinanciamiento(
    perfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {
    const masVotados = 'mas_votados';
    const params: Params = {limite, pagina, filtro: masVotados, tipo, perfil};
    const url = `${APIGAZE.BASE}${Proyecto.PROYECTOS_ESPERA_FINANCIAMIENTO}`;
    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(url, {
      observe: 'response',
      params
    });
  }

  obtnerProyectosSeleccionados(
    perfil: string,
    limite: number,
    pagina: number,
    tipo: string,
    fecha?: string,
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {
    const params: Params = fecha
      ? {limite, pagina, fechaInicial: fecha, fechaFinal: fecha, tipo, perfil}
      : {limite, pagina, tipo, perfil};

    const url = `${APIGAZE.BASE}${Proyecto.PROYECTOS_SELECCIONADOS}`;
    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(url, {
      observe: 'response',
      params
    });
  }

  apoyarProyecto(voto: VotoProyectoEntity): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + Proyecto.VOTO_PROYECTO_UNICO, voto);
  }

  obtenerFechaMaximaParaActualizarValorEstimado(): Observable<RespuestaRemota<{ fecha: Date }>> {
    return this.http.get<RespuestaRemota<{ fecha: Date }>>(APIGAZE.BASE + Proyecto.VALOR_ESTIMADO);
  }

  eliminarProyecto(idProyecto: string, idPerfil: string): Observable<RespuestaRemota<string>> {
    return this.http.delete<RespuestaRemota<string>>(
      APIGAZE.BASE
      + Proyecto.ELIMINAR_PROYECTO
      + '/' + `${idPerfil}`
      + '/' + `${idProyecto}`
    );
  }

  obtenerFechaForo(
    inicio: boolean
  ): Observable<RespuestaRemota<any>> {
    if (inicio) {
      return this.http.get<RespuestaRemota<any>>(APIGAZE.BASE + Proyecto.INICIO_DEL_FORO);
    } else {
      return this.http.get<RespuestaRemota<any>>(APIGAZE.BASE + Proyecto.FIN_DEL_FORO);
    }
  }

  obtenerProyectosSinFriltroFechas(
    limite: number,
    pagina: number,
    perfil: string,
    tipo: CodigosCatalogoTipoProyecto,
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {

    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(
      APIGAZE.BASE +
      Proyecto.TODOS_PROYECTOS
      + '/?'
      + 'limite=' + `${limite}`
      + '&pagina=' + `${pagina}`
      + '&perfil=' + `${perfil}`
      + '&tipo=' + `${tipo.toString()}`,
      {observe: 'response'}
    );
  }

  buscarProyectosPorFiltro(
    limite: number,
    pagina: number,
    filtro: FiltroBusquedaProyectos,
    tipo: CodigosCatalogoTipoProyecto,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {

    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(
      APIGAZE.BASE +
      Proyecto.PROYECTOS_RECIENTES
      + '/?'
      + 'limite=' + `${limite}`
      + '&pagina=' + `${pagina}`
      + '&filtro=' + `${filtro.toString()}`
      + '&tipo=' + `${tipo.toString()}`
      + '&perfil=' + `${perfil}`
      + '&fechaInicial=' + `${fechaInicial}`
      + '&fechaFinal=' + `${fechaFinal}`,
      {observe: 'response'}
    );
  }

  buscarProyectosPorTitulo(
    titulo: string,
    limite: number,
    pagina: number
  ): Observable<HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>> {
    return this.http.get<RespuestaRemota<ProyectoEntity[]>>(
      APIGAZE.BASE +
      Proyecto.BUSCAR_PROYECTOS_POR_TITULO
      + '/?'
      + 'titulo=' + `${titulo}`
      + '&limite=' + `${limite}`
      + '&pagina=' + `${pagina}`,
      {observe: 'response'}
    );
  }

  confirmarTransferenciaDeProyecto(
    idProyecto: string,
    idPerfilNuevo: string,
    idPerfilPropietario: string
  ): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(
      APIGAZE.BASE +
      Proyecto.CONFIRMAR_TRANSFERENCIA_DE_PROYECTO,
      {
        idProyecto,
        idPerfilNuevo,
        idPerfilPropietario
      }
    );
  }

  donacionesProyectos(donacionProyecto: DonacionProyectosModel): Observable<RespuestaRemota<PagoEntity>> {
    return this.http.post<any>(APIGAZE.BASE + Proyecto.DONACIONES_PROYECTOS, donacionProyecto);
  }

  validarDonacionProyectoStripe(idTransaccion: string): Observable<RespuestaRemota<boolean>> {
    return this.http.post<any>(APIGAZE.BASE + Proyecto.VALIDAR_DONACION_PROYECTO_STRIPE, {idTransaccion});
  }

}
