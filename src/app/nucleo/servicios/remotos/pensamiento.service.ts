
import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Pensamiento } from '@core/servicios/remotos/rutas/pensamientos.enum';
import { RespuestaRemota } from '@core/util/respuesta';
import { PensamientoEntity } from "dominio/entidades/pensamiento.entity";
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum';
@Injectable({ providedIn: 'root' })
export class PensamientoService {
    constructor(private http: HttpClient) {  }

    obtenerPensamientoAleatorio(): Observable<RespuestaRemota<PensamientoEntity>> {
        return this.http.get<RespuestaRemota<PensamientoEntity>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO.toString());
    }

    obtenerPensamientos(idPerfil:string,esPublico:boolean):Observable<RespuestaRemota<Array<PensamientoEntity>>>{
        if(!esPublico){
            return this.http.get<RespuestaRemota<Array<PensamientoEntity>>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO_PRIVADO.toString()+`/${idPerfil}`);
        }
        return this.http.get<RespuestaRemota<Array<PensamientoEntity>>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO_PUBLICO.toString()+`/${idPerfil}`);
    }

    crearPensamiento(data:PensamientoEntity):Observable<RespuestaRemota<PensamientoEntity>>{
        return this.http.post<RespuestaRemota<PensamientoEntity>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO.toString(),data);
    }
    actualizarPensamiento(data:PensamientoEntity):Observable<RespuestaRemota<string>> {
        return this.http.put<RespuestaRemota<string>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO.toString(),data);

    }
    actualizarEstadoPensamiento(idPensamiento:string):Observable<RespuestaRemota<PensamientoEntity>>{
        return this.http.put<RespuestaRemota<PensamientoEntity>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO.toString()+`/${idPensamiento}`,{})
    }

    eliminarPensamiento(idPensamiento:string):Observable<RespuestaRemota<string>>{
        return this.http.delete<RespuestaRemota<string>>(APIGAZE.BASE + Pensamiento.PENSAMIENTO.toString()+`/${idPensamiento}`)
    }

    cargarMasPensamientos(
        perfil: string,
        limite: number,
        pagina: number,
        esPublico: boolean,
        traducir: boolean,
    ):Observable<HttpResponse<RespuestaRemota<Array<PensamientoEntity>>>>{
        let ruta = ''
        let query = ''

        ruta = (esPublico) ? Pensamiento.CARGAR_PENSAMIENTOS_PUBLICO.toString() : Pensamiento.CARGAR_PENSAMIENTOS_PRIVADO.toString()
        query = (traducir) ?
            APIGAZE.BASE +
            ruta +
            `/?perfil=${perfil}&limite=${limite}&pagina=${pagina}&traducir=${traducir}`
            :
            APIGAZE.BASE +
            ruta +
            `/?perfil=${perfil}&limite=${limite}&pagina=${pagina}`

        return this.http.get<RespuestaRemota<Array<PensamientoEntity>>>(
            query,
            {observe:'response'}
        )
    }
}

