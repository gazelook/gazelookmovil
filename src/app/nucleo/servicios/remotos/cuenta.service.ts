import { UsuarioModel } from './../../../dominio/modelo/entidades/usuario.model';
import {FormularioContactanos} from 'dominio/modelo/entidades/fomulario-contactanos.model';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {APIGAZE} from '@core/servicios/remotos/rutas/api-gaze.enum';
import {RespuestaRemota} from '@core/util/respuesta';
import {Cuenta} from '@core/servicios/remotos/rutas/cuenta.enum';
import {UsuarioEntity} from 'dominio/entidades/usuario.entity';
import {PagoEntity} from 'dominio/entidades/pago.entity';
import {TokenEntity} from 'dominio/entidades/token.entity';
import {IniciarSesionEntity} from 'dominio/entidades/iniciar-sesion.entity';
import {CorreoContacto} from 'src/app/presentacion/contacto/contacto.component';
import {DataCerrarSesion} from 'src/app/app.component';

import {FormularioContactanosEnum} from '@core/servicios/remotos/rutas/formulario-contactanos.enum';
import {CoinPaymentesResponse} from 'dominio/entidades/catalogos/coinpayments-rates.entity';

@Injectable({providedIn: 'root'})
export class CuentaServiceRemoto {
  constructor(
    private http: HttpClient
  ) {

  }

  // OBTENER el catalo
  iniciarSesion(datos: Object): Observable<RespuestaRemota<IniciarSesionEntity>> {
    return this.http.post<RespuestaRemota<IniciarSesionEntity>>(APIGAZE.BASE + Cuenta.INICIAR_SESION, datos);
  }

  crearCuenta(usuario: UsuarioEntity): Observable<RespuestaRemota<PagoEntity>> {
    return this.http.post<RespuestaRemota<PagoEntity>>(APIGAZE.BASE + Cuenta.CUENTA, usuario);
  }

  activarCuenta(data: any): Observable<RespuestaRemota<IniciarSesionEntity>> {
    return this.http.post<RespuestaRemota<IniciarSesionEntity>>(APIGAZE.BASE + Cuenta.VALIDAR_CUENTA, data);
  }

  crearCuentaCoinPayments(data: UsuarioEntity): Observable<RespuestaRemota<CoinPaymentesResponse>> {
    return this.http.post<RespuestaRemota<CoinPaymentesResponse>>(APIGAZE.BASE + Cuenta.CREAR_CUENTA_COINPAYMENTS, data);
  }

  crearCuentaPaymentez(data: any): Observable<any> {
    return this.http.post<RespuestaRemota<IniciarSesionEntity>>(APIGAZE.BASE + Cuenta.CREAR_CUENTA_PAYMENTEZ, data);
  }

  activarCuentaPaymentez(data: UsuarioModel): Observable<any> {
    return this.http.post<RespuestaRemota<IniciarSesionEntity>>(APIGAZE.BASE + Cuenta.VALIDAR_CUENTA_PAYMENTEZ, data);
  }

  refrescarToken(tokenRefrescar: string): Observable<RespuestaRemota<TokenEntity>> {
    return this.http.post<RespuestaRemota<TokenEntity>>(APIGAZE.BASE + Cuenta.REFRESCAR_TOKEN, {
      'tokenRefresh': tokenRefrescar
    });
  }

  validarEmailUnico(email: string): Observable<RespuestaRemota<string>> {
    return this.http.get<RespuestaRemota<string>>(APIGAZE.BASE + Cuenta.EMAIL_UNICO + '/' + email);
  }

  recuperarContrasena(email: string): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE_MANTENIMIENTO + Cuenta.RECUPERAR_CONTRASENIA + '?email=' + email, {});
  }

  enviarEmailDeContacto(correo: CorreoContacto): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE_MANTENIMIENTO + Cuenta.ENVIAR_EMAIL_DE_CONTACTO, correo);
  }

  solicitarInformacionUsuario(idUsuario: string): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + Cuenta.SOLICITAR_INFORMACION + '/' + idUsuario, {})

  }

  eliminarDatosUsuario(idUsuario: string): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + Cuenta.SOLICITAR_ELIMINAR_DATOS_USUARIO + '/' + `${idUsuario}`, {});
  }

  reenviarCorreoVerificacion(idUsuario: string): Observable<RespuestaRemota<any>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + Cuenta.REENVIO_EMAIL_VERI + '/' + `${idUsuario}`, {});
  }


  cerrarSessionEnElApi(data: DataCerrarSesion): Observable<RespuestaRemota<string>> {
    return this.http.get<RespuestaRemota<string>>(
      APIGAZE.BASE + Cuenta.CERRAR_SESION +
      '/?' +
      'idDispositivo=' + data.idDispositivo +
      '&idUsuario=' + data.idUsuario
    );
  }

  enviarFormularioContactanos(formulario: FormularioContactanos): Observable<RespuestaRemota<string>> {
    return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE_MANTENIMIENTO + FormularioContactanosEnum.ENVIAR_FORMULARIO_CONTACTANOS, formulario);
  }

}
