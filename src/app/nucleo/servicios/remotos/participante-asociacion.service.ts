import { AsociacionEntity } from 'dominio/entidades/asociacion.entity';
import { ParticipanteAsociacion } from '@core/servicios/remotos/rutas/participante-asociacion.enum';
import { ParticipanteAsociacionEntity } from 'dominio/entidades/participante-asociacion.entity';
import { APIGAZE } from '@core/servicios/remotos/rutas/api-gaze.enum'
import { RespuestaRemota } from '@core/util/respuesta'
import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class ParticipanteAsociacionServiceRemoto {

    constructor(
        private http: HttpClient,
    ) {   }

    obtenerParticipanteAsoTipo(
        perfil: string,
        limite: number,
        pagina: number,
        filtro: string,
        filtroOrder?: string): Observable<HttpResponse<RespuestaRemota<Array<ParticipanteAsociacionEntity>>>> {
        return this.http.get<RespuestaRemota<ParticipanteAsociacionEntity[]>>(APIGAZE.BASE + ParticipanteAsociacion.PARTICIPANTE_ASO_TIPO.toString()
            + '/?' + 'limite=' + `${limite}`
            + '&pagina=' + `${pagina}`
            + '&perfil=' + `${perfil}`
            + '&filtro=' + `${filtro}`
            + '&filtroOrden=' + `${filtroOrder}`
            , { observe: 'response' })
    }

    cambiarEstadoAsociacion(data: ParticipanteAsociacionEntity): Observable<RespuestaRemota<string>> {
        return this.http.put<RespuestaRemota<string>>(APIGAZE.BASE + ParticipanteAsociacion.CAMBIAR_ESTADO.toString(), data);
    }

    crearAsociacion(data: any): Observable<RespuestaRemota<string>> {
        return this.http.post<RespuestaRemota<string>>(APIGAZE.BASE + ParticipanteAsociacion.CREAR_ASOCIACION.toString(), data);
    }

    obtenerParticipantesAsociacion(
        idAsociacion: string,
        idPerfil: string,
    ): Observable<RespuestaRemota<AsociacionEntity>> {
        return this.http.get<RespuestaRemota<AsociacionEntity>>(
            APIGAZE.BASE + ParticipanteAsociacion.OBTENER_PARTICIPANTE_ASO.toString()
            + '/?'
            + 'idPerfil=' + `${idPerfil}`
            + '&idConversacion=' + `${idAsociacion}`
        )
    }
}
