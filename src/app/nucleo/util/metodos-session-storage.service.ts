import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MetodosSessionStorageService {
    // Guarda datos en el storage
    guardar(llave: string, valor: any): void {
        sessionStorage.setItem(llave, JSON.stringify(valor));
    }

    // Obtener datos
    obtener(llave: string): any {
        return JSON.parse(sessionStorage.getItem(llave));
    }

    // Remover
    remover(llave: string): void {
        sessionStorage.removeItem(llave);
    }

    eliminarSessionStorage(): void {
        sessionStorage.clear();
    }
}
