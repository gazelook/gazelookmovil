import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { RutasNoticias } from 'src/app/presentacion/noticias/rutas-noticias.enum';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';

@Component({
  selector: 'app-menu-publicar-proyecto-noticia',
  templateUrl: './menu-publicar-proyecto-noticia.component.html',
  styleUrls: ['./menu-publicar-proyecto-noticia.component.scss']
})
export class MenuPublicarProyectoNoticiaComponent implements OnInit {

  public sesionIniciada: boolean
  public perfilSeleccionado: PerfilModel

  public confAppBar: ConfiguracionAppbarCompartida
  public confLineaVerde: LineaCompartida
  public confLineaVerdeAll: LineaCompartida

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private _location: Location,
    private variablesGlobales: VariablesGlobales,
    private notificacionesUsuario: NotificacionesDeUsuario,
    // private llamadaFirebaseService: LlamadaFirebaseService
  ) {   }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true
    this.configurarEstadoSesion()
    this.configurarPerfilSeleccionado()
    if (this.perfilSeleccionado) {
      this.configurarAppBar()
      this.configurarLinea()
      this.notificacionesUsuario.validarEstadoDeLaSesion(false)
      // this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }




  configurarEstadoSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v5texto29'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        }
      },
    }
  }

  configurarLinea() {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }
  }

  irACrearProyectoSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
    let ruta = RutasLocales.MODULO_PROYECTOS.toString()
    let publicar = RutasProyectos.PUBLICAR.toString()
    publicar = publicar.replace(':codigoTipoProyecto', codigo)
    this.router.navigateByUrl(ruta + '/' + publicar)
  }

  irAInformacionUtilDeProyectos() {
    let ruta = RutasLocales.MODULO_PROYECTOS.toString()
    let informacion = RutasProyectos.INFORMACION_UTIL.toString()
    this.router.navigateByUrl(ruta + '/' + informacion)
  }

  irACrearNoticia() {
    let ruta = RutasLocales.MODULO_NOTICIAS.toString()
    let informacion = RutasNoticias.PUBLICAR
    this.router.navigateByUrl(ruta + '/' + informacion)
  }

  navegarSegunMenu(menu: number) {
    switch (menu) {
      case 0:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL)
        break
      case 1:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_PAIS)
        break
      case 2:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_LOCAL)
        break
      case 3:
        this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_RED)
        break
      case 4:
        this.irACrearNoticia()
        break
      case 5:
        this.irAInformacionUtilDeProyectos()
        break
    }
  }
}
