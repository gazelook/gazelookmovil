import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacionesDePerfil } from '@core/servicios/generales/notificaciones/notificaciones-perfil.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
import { DataNotificaciones } from '@core/servicios/generales/notificaciones/notificaciones.interface';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  ColorFondoItemMenu,
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import {
  TamanoColorDeFondo,
  TamanoItemMenu,
  TamanoLista,
  TamanoPortadaGaze,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { ItemMenuCompartido } from '@shared/diseno/modelos/item-menu.interface';
import { ConfiguracionLineaVerde } from '@shared/diseno/modelos/lista-contactos.interface';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import {
  ItemAccion,
  ItemMenuModel,
  ItemSubMenu,
} from 'dominio/modelo/entidades/item-menu.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { RutasGazing } from 'src/app/presentacion/gazing/rutas-gazing.enum';
import { RutasNoticias } from 'src/app/presentacion/noticias/rutas-noticias.enum';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { CatalogoIdiomaEntity } from '../../dominio/entidades/catalogos';
import { IdiomaNegocio } from '../../dominio/logica-negocio';
import { RutasPerfiles } from './../perfiles/rutas-perfiles.enum';

@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.scss'],
})
export class MenuPrincipalComponent implements OnInit, OnDestroy {
  public perfilSeleccionado: PerfilModel;
  public mostrarLinea: boolean;
  public notificacionesMensajes: any;
  public listaMenu: ItemMenuModel[];
  public itemSubMenu3Puntos: ItemSubMenu;
  public itemMenuMultipleAccion: ItemMenuModel;
  public sesionIniciada: boolean;
  public dataNotificacionesMensajes: DataNotificaciones;
  public idiomaSeleccionado: CatalogoIdiomaEntity;
  public dataLista: DatosLista;
  public confAppBar: ConfiguracionAppbarCompartida;
  public confLinea: ConfiguracionLineaVerde;
  public confPortada: PortadaGazeCompartido;
  public confDialogoLateral: DialogoCompartido;
  public confDialogoConstruccion: DialogoCompartido;
  constructor(
    // tslint:disable-next-line: variable-name
    private _location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private perfilNegocio: PerfilNegocio,
    private cuentaNegocio: CuentaNegocio,
    private variablesGlobales: VariablesGlobales,
    private db: AngularFireDatabase,
    private notificacionesPerfil: NotificacionesDePerfil,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private idiomaNegocio: IdiomaNegocio
  ) {
    this.mostrarLinea = false;
    this.sesionIniciada = false;
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true;

    this.prepararAppBar();
    this.obtenerIdioma();
    this.configurarDialogo();
    this.configurarPortada();
    this.prepararItemsMenu();
    this.prepararDataSubMenu3puntos();
    this.prepararDatosParaMenuMultipleAccion();
    this.configurarEstadoDeLaSesion();
    this.configurarDataLista();
    this.configurarLineas();
    this.configurarDialogos();

    if (this.sesionIniciada) {
      this.verificarPerfilSeleccionado();
      this.configurarDataNotificacionesMensajes();
      this.configurarEscuchaNotificacionesMensajes();
      this.obtenerNotificaciones();
    } else {
      this.confAppBar.gazeAppBar.subtituloDemo =
        this.obtenerTituloPrincipal(false);
    }
  }

  ngOnDestroy(): void {
    this.notificacionesPerfil.desconectarDeEscuchaNotificaciones();
    this.notificacionesUsuario.desconectarDeEscuchaNotificaciones();
    // this.llamadaFirebaseService.desconectarDeEscuchaLlamadas()
  }

  configurarEstadoDeLaSesion(): void {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
  }

  configurarDataLista(): void {
    this.dataLista = {
      cargando: false,
      reintentar: () => {},
      tamanoLista: TamanoLista.TIPO_MENU_PRINCIPAL,
    };
  }

  configurarPortada(): void {
    this.confPortada = {
      tamano: TamanoPortadaGaze.PORTADACOMPLETA,
      imagenAleatoriaRamdon: true,
    };
  }

  configurarLineas(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }

  configurarDialogo(): void {
    this.confDialogoLateral = {
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      completo: true,
      descripcion: 'm6v4texto11',
    };
  }

  verificarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();

    if (!this.perfilSeleccionado) {
      this._location.replaceState('/');
      this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      return;
    }

    this.confAppBar.gazeAppBar.subtituloNormal =
      this.obtenerTituloPrincipal(true);
    this.confAppBar.gazeAppBar.mostrarBotonXRoja = true;

    if (
      this.perfilSeleccionado &&
      this.perfilSeleccionado.tipoPerfil &&
      this.perfilSeleccionado.tipoPerfil.codigo &&
      this.perfilSeleccionado.tipoPerfil.codigo ===
        CodigosCatalogoTipoPerfil.GROUP
    ) {
      this.confAppBar.usoAppBar = UsoAppBar.USO_GAZE_BAR;
    }
  }

  async prepararAppBar(
    usoAppBar: UsoAppBar = UsoAppBar.USO_ELEGIR_PERFIL
  ): Promise<void> {
    this.confAppBar = {
      usoAppBar,
      accionAtras: () => {
        this._location.replaceState('/');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      },
      gazeAppBar: {
        tituloPrincipal: {
          mostrar: true,
          llaveTexto: 'm2v11texto1',
        },
        textoElegirPerfil: {
          mostrar: true,
          llaveTexto: 'm2v11texto2',
        },

        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        clickTituloPrincipal: () => {
          if (this.perfilSeleccionado) {
            this.router.navigateByUrl(
              RutasLocales.MODULO_PERFILES +
                '/' +
                RutasPerfiles.PERFIL.toString().replace(
                  ':id',
                  this.perfilSeleccionado._id
                )
            );
          }
        },
        clickElegirOtroPerfil: () => {
          this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
        },
      },
      sinColorPrincipal: true,
      conColorSecundario: true,
    };
  }

  obtenerTituloPrincipal(profileCreated: boolean): {
    mostrar: boolean;
    llaveTexto: string;
  } {
    if (profileCreated) {
      return {
        mostrar: true,
        llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre,
      };
    } else {
      return {
        mostrar: true,
        llaveTexto: 'demo',
      };
    }
  }

  async prepararItemsMenu(): Promise<void> {
    this.listaMenu = [
      // Pensamientos
      {
        id: MenuPrincipal.MIS_PENSAMIENTOS,
        titulo: ['m2v11texto7', 'm2v11texto8', 'm2v11texto9'],
        ruta: RutasLocales.MODULO_PENSAMIENTO,
        tipo: TipoMenu.ACCION,
      },
      // Contactos
      {
        id: MenuPrincipal.GAZING,
        titulo: ['m2v11texto10', 'm2v11texto11'],
        ruta:
          RutasLocales.GAZING.toString() +
          '/' +
          RutasGazing.CHAT_CONTACTOS.toString(),
        tipo: TipoMenu.ACCION,
        mostrarCorazon: false,
      },
      // Publicar proyectos y noticias
      {
        id: MenuPrincipal.PUBLICAR,
        titulo: ['m2v11texto12', 'm2v11texto13', 'm2v11texto14'],
        ruta: RutasLocales.MENU_PUBLICAR_PROYECTO_NOTICIA.toString(),
        tipo: TipoMenu.ACCION,
      },
      // Proyectos de usuarios
      {
        id: MenuPrincipal.PROYECTOS,
        titulo: ['m2v11texto15', 'm2v11texto16'],
        tipo: TipoMenu.ACCION,
        ruta:
          RutasLocales.MODULO_PROYECTOS +
          '/' +
          RutasProyectos.MENU_SELECCIONAR_TIPO_PROYECTOS,
      },
      // Noticias de usuarios
      {
        id: MenuPrincipal.NOTICIAS,
        titulo: ['m2v11texto17', 'm2v11texto18'],
        tipo: TipoMenu.ACCION,
        ruta:
          RutasLocales.MODULO_NOTICIAS + '/' + RutasNoticias.NOTICIAS_USUARIOS,
      },
      // Compra o intercambio
      {
        id: MenuPrincipal.COMPRAS,
        titulo: ['m2v11texto19', 'm2v11texto19.1'],
        tipo: TipoMenu.ACCION,
        ruta: RutasLocales.COMPRAS_INTERCAMBIOS,
      },
      // Finanzas
      {
        id: MenuPrincipal.FINANZAS,
        titulo: ['m2v11texto21'],
        tipo: TipoMenu.ACCION,
        ruta: RutasLocales.FINANZAS,
      },
      // Anuncios
      {
        id: MenuPrincipal.ANUNCIOS,
        titulo: ['m2v11texto22', 'm2v11texto23'],
        tipo: TipoMenu.ANUNCIOS,
        ruta: RutasLocales.ANUNCIOS_SISTEMA,
      },
    ];
  }
  configurarDialogos(): void {
    this.confDialogoConstruccion = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      descripcion: 'm7v4texto3',
    };
  }

  prepararItemMenu(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo:
        item.id === MenuPrincipal.ANUNCIOS
          ? ColorFondoItemMenu.TRANSPARENTE
          : ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo[0] ? item.titulo[0].toString() : null,
      texto2: item.titulo[1] ? item.titulo[1].toString() : null,
      texto3: item.titulo[2] ? item.titulo[2].toString() : null,
      tipoMenu: item.tipo,
      linea: {
        mostrar: true,
        configuracion: {
          ancho:
            item.id === MenuPrincipal.ANUNCIOS ||
            item.id === MenuPrincipal.FINANZAS
              ? AnchoLineaItem.ANCHO6028
              : AnchoLineaItem.ANCHO6382,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
          cajaGaze: item.id === MenuPrincipal.ANUNCIOS,
        },
      },
      gazeAnuncios: item.id === MenuPrincipal.ANUNCIOS,
      idInterno: item.id,
      onclick: () => this.navigationSubMenu(item.ruta),
      dobleClick: () => {},
    };
  }

  async prepararDataSubMenu3puntos(): Promise<void> {
    this.itemSubMenu3Puntos = {
      id: 'puntos',
      titulo: '',
      mostrarDescripcion: false,
      menusInternos: [
        {
          id: 'mya',
          titulo: ['m2v12texto1'],
          action: () => {
            this.router.navigateByUrl(RutasLocales.MI_CUENTA);
          },
        },
        {
          id: 'iv',
          titulo: ['m2v12texto2'],
          subTitulo: ['m2v12texto3'],
          action: () => {
            this.router.navigateByUrl(RutasLocales.PAGO_CUOTA_EXTRA);
          },
        },
        {
          id: 'he',
          titulo: ['m2v12texto4'],
          action: () => {
            this.router.navigateByUrl(RutasLocales.LANDING);
          },
        },
        // {
        // 	id: "faq",
        // 	titulo: ["m2v12texto5"],
        // 	action: () => {
        // 		this.router.navigateByUrl(RutasLocales.LANDING)
        // 	},
        // },
        // {
        // 	id: "our",
        // 	titulo: ["m2v12texto6"],
        // 	action: () => {
        // 		this.router.navigateByUrl(RutasLocales.NUETRAS_METAS)
        // 	},
        // },
        // {
        // 	id: "web",
        // 	titulo: ["WebSite"],
        // 	action: () => { },
        // }
      ],
    };
  }
 
  navigationSubMenu(ruta: RutasLocales): void {
    if (ruta === RutasLocales.COMPRAS_INTERCAMBIOS) {
      this.confDialogoConstruccion.mostrarDialogo = true;
      return;
    }
    if (ruta) {
      this.router.navigateByUrl(ruta.toString());
    } else {
      this.confDialogoLateral.mostrarDialogo = true;
    }
  }

  prepararItemSubMenu(item: ItemSubMenu): ItemMenuCompartido {
    return {
      id: '',
      submenus: item.menusInternos ? item.menusInternos : [],
      mostrarDescripcion: item.mostrarDescripcion,
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo,
      tipoMenu: TipoMenu.SUBMENU,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => this.mostrarDescripcion(item),
      dobleClick: () => {},
    };
  }

  mostrarDescripcion(item: any): void {
    const elemento: HTMLElement = document.getElementById(
      'flecha' + item.id
    ) as HTMLElement;
    if (item.mostrarDescripcion) {
      item.mostrarDescripcion = false;
      elemento.classList.remove('rotar-flecha');
    } else {
      item.mostrarDescripcion = true;
      elemento.classList.add('rotar-flecha');
    }
  }

  obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
  }

  prepararDatosParaMenuMultipleAccion(): void {
    this.itemMenuMultipleAccion = {
      id: 'multip',
      titulo: [
        {
          accion: () => {
            this.router.navigate([RutasLocales.CONTACTO]);
          },
          nombre: 'm2v11texto24',
          codigo: 'g',
        },
        {
          accion: () => {
            // tslint:disable-next-line: prefer-const
            let link = document.createElement('a');
            const nombre = 'politica-privacidad-gazelook';
            link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/terminos-condiciones-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
            link.download = nombre;
            link.target = '_blank';
            link.dispatchEvent(
              new MouseEvent('click', {
                view: window,
                bubbles: false,
                cancelable: true,
              })
            );
          },
          nombre: 'm2v11texto25',
          codigo: 'm',
        },
      ],
      tipo: TipoMenu.LEGAL,
    };
  }

  prepararItemsParaMenuMultipleAccion(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      mostrarDescripcion: false,
      tamano: TamanoItemMenu.ITEM_MENU_CONTENIDO, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      acciones: item.titulo as ItemAccion[],
      tipoMenu: TipoMenu.LEGAL,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => {},
      dobleClick: () => {},
    };
  }

  scroolEnContenedor(): void {
    const elemento = document.getElementById(
      'contenedorListaMenuPrincipal'
    ) as HTMLElement;
    if (!elemento) {
      return;
    }

    if (elemento.scrollTop < 30) {
      this.mostrarLinea = false;
      return;
    }

    if (elemento.scrollTop > 30) {
      this.mostrarLinea = true;
      return;
    }

    this.mostrarLinea = elemento.scrollTop >= 30;
  }

  configurarDataNotificacionesMensajes(): void {
    this.dataNotificacionesMensajes = {
      codigoEntidad: CodigosCatalogoEntidad.MENSAJE,
      idPropietario: this.perfilSeleccionado._id,
      nivel: CodigosCatalogoEntidad.PERFIL,
      leido: false,
      limite: 5,
    };
  }

  async obtenerNotificaciones(): Promise<void> {
    if (!this.sesionIniciada || !this.perfilSeleccionado) {
      this.notificacionesPerfil.desconectarDeEscuchaNotificaciones();
      this.notificacionesUsuario.desconectarDeEscuchaNotificaciones();
      return;
    }

    this.notificacionesUsuario.validarEstadoDeLaSesion(false);
    // this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
    this.notificacionesPerfil.obtenerNotificaciones$.next(
      this.dataNotificacionesMensajes
    );
    console.log('dataNotificacionesMensajes', this.dataNotificacionesMensajes);
    
  }

  async configurarEscuchaNotificacionesMensajes(): Promise<void> {
    (this.notificacionesPerfil.subscripcionNotificaciones$ =
      this.notificacionesPerfil.notificaciones$.subscribe(
        (data) => {
          console.log('data', data);
          
          let contador = 0;
          data.forEach((item) => {
            // tslint:disable-next-line: no-string-literal
            if (!item['leido']) {
              contador += 1;
            }
          });
          const index = this.listaMenu.findIndex(
            (e) => e.id === MenuPrincipal.GAZING
          );
          this.listaMenu[index].mostrarCorazon = index >= 0 && contador > 0;
        }
        // tslint:disable-next-line: no-unused-expression
      )),
      (error: any) => {
        this.notificacionesPerfil.desconectarDeEscuchaNotificaciones();
      };
  }
}

export enum MenuPrincipal {
  MIS_PENSAMIENTOS,
  MIS_CONTACTOS,
  PUBLICAR,
  PROYECTOS,
  NOTICIAS,
  COMPRAS,
  FINANZAS,
  ANUNCIOS,
  GAZING,
}
