import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import {
  ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ConfiguracionLineaVerde } from '@shared/diseno/modelos/lista-contactos.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';

@Component({
  selector: 'app-nuestras-metas',
  templateUrl: './nuestras-metas.component.html',
  styleUrls: ['./nuestras-metas.component.scss']
})
export class NuestrasMetasComponent implements OnInit {

  public perfilSeleccionado: PerfilModel
  public menuActivo: number

  public confAppBar: ConfiguracionAppbarCompartida
  public confLineaCompleta: ConfiguracionLineaVerde
  public confLineaMitad: ConfiguracionLineaVerde

  constructor(
    private perfilNegocio: PerfilNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private _location: Location,
    private generadorId: GeneradorId,
    private formBuilder: FormBuilder,
    private cuentaNegocio: CuentaNegocio,
    private router: Router
  ) {
    this.menuActivo = -1
  }

  ngOnInit(): void {
    this.configurarPerfilSeleccionado()
    if (this.perfilSeleccionado) {
      this.configurarAppBar()
      this.configurarLineas()
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      accionAtras: () => {
        this._location.back()
      },
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v13texto1'
        },
      }
    }
  }

  configurarLineas() {
    this.confLineaCompleta = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }

    this.confLineaMitad = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }
  }

  clickEnMenu(menuClickeado: number) {
    if (this.menuActivo === menuClickeado) {
      this.menuActivo = -1
      return
    }
    this.menuActivo = menuClickeado
    return
  }
}
