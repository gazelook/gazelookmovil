import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  ColorDeBorde,
  ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { ConfiguracionItemBuscadorProyectoNoticia } from '@shared/diseno/modelos/item-buscador-proyectos-noticias.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';

@Component({
  selector: 'app-menu-ver-proyectos',
  templateUrl: './menu-ver-proyectos.component.html',
  styleUrls: ['./menu-ver-proyectos.component.scss']
})
export class MenuVerProyectosComponent implements OnInit {

  public sesionIniciada: boolean
  public perfilSeleccionado: PerfilModel

  public listaProyectos: PaginacionModel<string>
  public puedeCargarMas: boolean

  public confAppBar: ConfiguracionAppbarCompartida
  public confLineaVerde: LineaCompartida
  public confLineaVerdeAll: LineaCompartida

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private _location: Location,
    private proyectoNegocio: ProyectoNegocio,
    private albumNegocio: AlbumNegocio
  ) {
    this.puedeCargarMas = true
  }

  ngOnInit(): void {
    this.configurarEstadoSesion()
    this.configurarPerfilSeleccionado()
    this.inicializarDataListaProyectos()
    if (this.perfilSeleccionado) {
      this.configurarAppBar()
      this.configurarLinea()
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }

  configurarEstadoSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {
        this.accionAtras()
      },
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v13texto2'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v1texto1',
            valorBusqueda: '',
          }
        }
      },
    }
  }

  configurarLinea() {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    }
  }

  inicializarDataListaProyectos() {
    this.listaProyectos = {
      anteriorPagina: false,
      paginaActual: 1,
      proximaPagina: true,
      totalDatos: 0,
      totalPaginas: 0,
      lista: [],
    }
  }

  irAVerProyectoSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
    let ruta = RutasLocales.MODULO_PROYECTOS.toString()
    let informacion = RutasProyectos.MENU_LISTA_PROYECTOS.toString()
    informacion = informacion.replace(':codigoTipoProyecto', codigo.toString())
    this.router.navigateByUrl(ruta + '/' + informacion)
  }

  irAInformacionUtilDeProyectos() {
    let ruta = RutasLocales.MODULO_PROYECTOS.toString()
    let informacion = RutasProyectos.INFORMACION_UTIL.toString()
    this.router.navigateByUrl(ruta + '/' + informacion)
  }

  navegarSegunMenu(menu: number) {
    switch (menu) {
      case 0:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL)
        break
      case 1:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_PAIS)
        break
      case 2:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_LOCAL)
        break
      case 3:
        this.irAVerProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_RED)
        break
      case 4:
        this.irAInformacionUtilDeProyectos()
        break
    }
  }

  async buscarProyectosPorTitulo(titulo: string) {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = false
      return
    }

    try {

      const dataPaginacion = await this.proyectoNegocio.buscarProyectosPorTitulo(
        titulo,
        10,
        this.listaProyectos.paginaActual
      ).toPromise()

      this.listaProyectos.totalDatos = dataPaginacion.totalDatos
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(proyecto => {
      })
      this.puedeCargarMas = true

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1
      }

    } catch (error) {
      this.puedeCargarMas = false
    }
  }

  configurarItemNoticiasProyectos(
    proyecto: ProyectoModel
  ): ConfiguracionItemBuscadorProyectoNoticia {
    return {
      codigoEntidad: CodigosCatalogoEntidad.PROYECTO,
      proyecto: proyecto,
      configuracionRectangulo: this.obtenerConfiguracionRectangulo(proyecto),
      eventoTap: (id: string) => {
        if (!id || id.length === 0) {
          return
        }

        const modulo = RutasLocales.MODULO_PROYECTOS.toString()
        let ruta = RutasProyectos.VISITAR.toString()
        ruta = ruta.replace(':id', id)
        this.router.navigateByUrl(modulo + '/' + ruta)
      }
    }
  }

  obtenerConfiguracionRectangulo(proyecto: ProyectoModel): CongifuracionItemProyectosNoticias {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    )

    return {
      usoVersionMini: true,
      id: proyecto.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: false,
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: false,
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: false,
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    }
  }

  accionAtras() {
    this._location.back()
  }
}
