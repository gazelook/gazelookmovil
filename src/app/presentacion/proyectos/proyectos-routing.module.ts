import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {HistoricoProyectoComponent} from 'src/app/presentacion/proyectos/historico-proyectos/historico-proyecto.component';
import {NuevosProyectosComponent} from 'src/app/presentacion/proyectos/nuevos-proyectos/nuevos-proyectos.component';
import {MenuListaProyectosComponent} from 'src/app/presentacion/proyectos/menu-lista-proyectos/menu-lista-proyectos.component';
import {MenuVerProyectosComponent} from 'src/app/presentacion/proyectos/menu-ver-proyectos/menu-ver-proyectos.component';
import {InformacionUtilComponent} from 'src/app/presentacion/proyectos/informacion-util/informacion-util.component';
import {ProyectosComponent} from 'src/app/presentacion/proyectos/proyectos.component';
import {RutasProyectos} from 'src/app/presentacion/proyectos/rutas-proyectos.enum';
import {PublicarComponent} from 'src/app/presentacion/proyectos/publicar/publicar.component';

import {ProyectosRecomendadosComponent} from 'src/app/presentacion/proyectos/proyectos-recomendados/proyectos-recomendados.component';
import {ProyectosForosComponent} from 'src/app/presentacion/proyectos/proyectos-foros/proyectos-foros.component';
import {
  ProyectosSeleccionadosComponent
} from 'src/app/presentacion/proyectos/proyectos-seleccionados/proyectos-seleccionados.component';
import {
  ProyectosEsperaFondosComponent
} from 'src/app/presentacion/proyectos/proyectos-espera-fondos/proyectos-espera-fondos.component';
import {DonacionProyectosComponent} from 'src/app/presentacion/proyectos/donacion-proyectos/donacion-proyectos.component';

const routes: Routes = [
  {
    path: '',
    component: ProyectosComponent,
    children: [
      {
        path: RutasProyectos.PUBLICAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasProyectos.ACTUALIZAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasProyectos.VISITAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasProyectos.INFORMACION_UTIL.toString(),
        component: InformacionUtilComponent
      },
      {
        path: RutasProyectos.MENU_SELECCIONAR_TIPO_PROYECTOS.toString(),
        component: MenuVerProyectosComponent
      },
      {
        path: RutasProyectos.MENU_LISTA_PROYECTOS.toString(),
        component: MenuListaProyectosComponent
      },
      {
        path: RutasProyectos.LISTA_NUEVOS_PROYECTOS.toString(),
        component: NuevosProyectosComponent
      },
      {
        path: RutasProyectos.LISTA_PROYECTOS_RECOMENDADOS.toString(),
        component: ProyectosRecomendadosComponent
      }, {
        path: RutasProyectos.LISTA_PROYECTOS_ESPERA_FONDOS.toString(),
        component: ProyectosEsperaFondosComponent
      },
      {
        path: RutasProyectos.LISTA_PROYECTOS_FOROS.toString(),
        component: ProyectosForosComponent
      }, {
        path: RutasProyectos.LISTA_PROYECTOS_SELECCIONADOS.toString(),
        component: ProyectosSeleccionadosComponent
      },
      {
        path: RutasProyectos.HISTORICO.toString(),
        component: HistoricoProyectoComponent
      },
      {
        path: RutasProyectos.DONACIONES_PROYECTOS.toString(),
        component: DonacionProyectosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectosRoutingModule {

}
