import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {Component, OnInit} from '@angular/core';
import { Location } from '@angular/common';

import {
  ConfiguracionAppbarCompartida,
  DatosLista,
  LineaCompartida,
} from '@shared/diseno/modelos';
import {
  AnchoLineaItem,
  ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  TamanoLista,
  UsoAppBar
} from '@shared/diseno/enums';
import {CatalogoMetodoPagoModel} from 'dominio/modelo/catalogos';
import {CodigosCatalogosMetodosDePago, CodigosEstadoMetodoPago} from '@core/servicios/remotos/codigos-catalogos';
import {PagoNegocio} from 'dominio/logica-negocio';

import {EstiloDelTextoServicio} from '@core/servicios/diseno';

@Component({
  selector: 'app-donacion-proyectos',
  templateUrl: './donacion-proyectos.component.html',
  styleUrls: ['./donacion-proyectos.component.scss']
})
export class DonacionProyectosComponent implements OnInit {


  public configuracionAppBar: ConfiguracionAppbarCompartida;
  public confLinea: LineaCompartida;
 
  public dataLista: DatosLista;
  public listaMetodoPago: CatalogoMetodoPagoModel[];
  public codigosCatalogos = CodigosCatalogosMetodosDePago;
  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public metodoPagoCriptomoneda: string = CodigosCatalogosMetodosDePago.CRIPTOMONEDA;

  constructor(
    private pagoNegocio: PagoNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio,
    private variablesGlobales: VariablesGlobales,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.configurarAppBar();
    this.configurarLinea();
    this.prepararLista();
    this.obtenerCatalogoMetodosPago();
    this.variablesGlobales.mostrarMundo = false;
  }

  // Métodos pago
  private obtenerCatalogoMetodosPago(): void {
    this.dataLista.cargando = true;
    this.pagoNegocio.obtenerCatalogoMetodoPago()
      .subscribe((resp: CatalogoMetodoPagoModel[]) => {
        this.dataLista.cargando = false;
        this.listaMetodoPago = resp;
      }, error => {
        this.dataLista.cargando = false;
        this.dataLista.error = error;
      });
  }

  private prepararLista(): void {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoMetodosPago,
      lista: this.listaMetodoPago,
      tamanoLista: TamanoLista.TIPO_PERFILES
    };
  }

  // Config AppBar
  private configurarAppBar(): void {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.accionAtras(),
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        nombrePerfil: {
          mostrar: false
        },
        mostrarDivBack: {
          icono: true,
          texto: false,
        },
        mostrarTextoHome: false,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v9texto1'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      }
    };
  }
  accionAtras(): void {
    this.location.back();
  }

//  conf linea verde
  private configurarLinea(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
      forzarAlFinal: false
    };
  }

}
