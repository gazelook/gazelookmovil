import {Component, OnInit, Input} from '@angular/core';

import {CatalogoMetodoPagoModel} from 'dominio/modelo/catalogos';
import {CodigosCatalogosMetodosDePago, CodigosEstadoMetodoPago} from '@core/servicios/remotos/codigos-catalogos';
import {IdiomaNegocio, PagoNegocio, TipoMonedaNegocio} from 'dominio/logica-negocio';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {MonedaPickerService} from '@core/servicios/generales';

@Component({
  selector: 'app-metodo-cripto',
  templateUrl: './metodo-cripto.component.html',
  styleUrls: ['./metodo-cripto.component.scss']
})
export class MetodoCriptoComponent implements OnInit {
  @Input() item: CatalogoMetodoPagoModel;

  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public codigosCatalogos = CodigosCatalogosMetodosDePago;


  constructor(
    private pagoNegocio: PagoNegocio,
    private idiomaNegocio: IdiomaNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private monedaPickerService: MonedaPickerService,
  ) {
  }

  ngOnInit(): void {
  }

  public navegarMetodoPagoCripto(metodoPago: CatalogoMetodoPagoModel): void {

  }


}
