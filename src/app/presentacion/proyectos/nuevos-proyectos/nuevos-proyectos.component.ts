import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorDeBorde,
  ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { NoticiaService } from '@core/servicios/generales/noticia.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { CodigosCatatalogosEstadoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-estado-proyecto.enum';
import { FiltroBusquedaProyectos } from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';

@Component({
  selector: 'app-nuevos-proyectos',
  templateUrl: './nuevos-proyectos.component.html',
  styleUrls: ['./nuevos-proyectos.component.scss']
})
export class NuevosProyectosComponent implements OnInit {

  @ViewChild('toast', { static: false }) toast: ToastComponent;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;
  public mostrarCapaNormal: boolean;
  public puedeCargarMas: boolean;
  // Parametros internos
  public filtroGeneral: FiltroBusquedaProyectos;
  public fechaInicial: Date;
  public fechaFinal: Date;
  public filtroActivoDeBusqueda: FiltroBusqueda;
  public perfilSeleccionado: PerfilModel;
  public idCapaCuerpo: string;
  public params: ProyectoParams;
  public currentDay: string;
  public listaProyectoModel: Array<NoticiaModel>;
  public listaProyectoModelFiltroOtro: Array<NoticiaModel>;
  public menosVotados: boolean;
  // Configuracion hijos
  public confToast: ConfiguracionToast;
  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaNoticias: LineaCompartida;

  public listaProyectos: PaginacionModel<CongifuracionItemProyectosNoticias>;
  public confResultadoBusqueda: Array<CongifuracionItemProyectosNoticias>;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private noticiaNegocio: NoticiaNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private translateService: TranslateService,
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.params = { estado: false };
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.fechaInicial = new Date('2021-01-02');
    this.fechaFinal = new Date();
    this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES;
    this.menosVotados = false;
    this.idCapaCuerpo = 'capa-cuerpo-intercambios';
    this.confResultadoBusqueda = [];

    this.listaProyectoModel = [];
    this.listaProyectoModelFiltroOtro = [];
    this.currentDay = new Date().toISOString().slice(0, 10);
  }

  ngOnInit(): void {

    this.variablesGlobales.mostrarMundo = false;
    this.inicializarPerfilSeleccionado();
    this.configurarParametros();
    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarToast();
      this.configurarAppBar();
      this.configurarLineas();
      this.ejecutarBusqueda();

    } else {
      this.perfilNegocio.validarEstadoDelPerfil(
        this.perfilSeleccionado,
        this.params.estado
      );
    }
  }

  obtenerFechaParaParametro(fecha: Date): string {
    const dia = (fecha.getDate() < 10) ? '0' + fecha.getDate() : fecha.getDate();
    const mes = (fecha.getMonth() + 1 < 10) ? '0' + (fecha.getMonth() + 1) : fecha.getMonth() + 1;
    return fecha.getFullYear() + '-' + mes + '-' + dia;
  }

  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarParametros(): void {
    const urlParams: Params = this.route.snapshot.params;
    if (urlParams.codigoTipoProyecto) {
      this.params.codigoTipoProyecto = urlParams.codigoTipoProyecto as CodigosCatalogoTipoProyecto;
      this.params.estado = true;
    }
    if (urlParams.menosVotado) {
      this.params.menosVotado = urlParams.menosVotado;
      this.params.estado = true;


      if (this.params.menosVotado === 'true') {
        this.filtroGeneral = FiltroBusquedaProyectos.MENOS_VOTADOS;
        this.menosVotados = true;
      } else {
        this.filtroGeneral = FiltroBusquedaProyectos.NUEVOS_PROYECTO;
        this.menosVotados = false;
      }

    }

  }

  configurarToast(): void {
    this.confToast = {
      mostrarToast: false, // True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    };
  }

  configurarAppBar(): void {

    let llaverTextoTipoProyecto: string;
    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_LOCAL) {
      llaverTextoTipoProyecto = 'm5v1texto2.2';
    }
    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL) {
      llaverTextoTipoProyecto = 'm5v1texto2';
    }
    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_PAIS) {
      llaverTextoTipoProyecto = 'm5v1texto2.1';
    }
    if (this.params.codigoTipoProyecto === CodigosCatalogoTipoProyecto.PROYECTO_RED) {
      llaverTextoTipoProyecto = 'm5v1texto2.3';
    }
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {
        this.accionAtras();
      },
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: llaverTextoTipoProyecto
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v3texto1',
            valorBusqueda: '',
          }
        }
      },
    };
  }

  configurarLineas(): void {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  inicializarListaProyectos(): void {
    this.listaProyectoModel = [];
    this.listaProyectos = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  accionAtras(): void {
    if (this.filtroActivoDeBusqueda !== FiltroBusqueda.RECIENTES) {
      this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES;
      this.ejecutarBusqueda();
      return;
    }

    this._location.back();
  }

  tapEnInputFecha(id: string): void {
    const elemento: HTMLElement = document.getElementById(id) as HTMLElement;
    if (elemento) {
      elemento.click();
    }
  }

  async obtenerProyectosSinFriltroFechas(
    perfil: string,
    tipo: CodigosCatalogoTipoProyecto,
  ): Promise<void> {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }

    try {
      this.mostrarCapaError = false;
      this.mostrarCapaLoader = (this.listaProyectos.lista.length === 0);
      const dataPaginacion = await this.proyectoNegocio.obtenerProyectosSinFriltroFechas(
        20,
        this.listaProyectos.paginaActual,
        perfil,
        tipo
      ).toPromise();
      this.listaProyectos.totalDatos = dataPaginacion.totalDatos;
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina;
      dataPaginacion.lista.forEach(proyecto => {
        this.listaProyectoModel.push(proyecto);
        this.listaProyectos.lista.push(this.configurarItemListaNoticias(proyecto));
      });
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = true;

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1;
      }
    } catch (error) {
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = false;
      this.mostrarCapaError = true;
    }
  }

  async obtenerProyectosPorFiltro(
    filtro: FiltroBusquedaProyectos,
    tipo: CodigosCatalogoTipoProyecto,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
  ): Promise<void> {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }

    try {

      this.mostrarCapaError = false;
      this.mostrarCapaLoader = (this.listaProyectos.lista.length === 0);
      const dataPaginacion = await this.proyectoNegocio.buscarProyectosPorFiltro(
        16,
        this.listaProyectos.paginaActual,
        filtro,
        tipo,
        perfil,
        fechaInicial,
        fechaFinal
      ).toPromise();

      this.listaProyectos.totalDatos = dataPaginacion.totalDatos;
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina;

      dataPaginacion.lista.forEach(proyecto => {
        this.listaProyectoModel.push(proyecto);
        this.listaProyectos.lista.push(this.configurarItemListaNoticias(proyecto));
      });
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = true;

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1;
      }
    } catch (error) {
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = false;
      this.mostrarCapaError = true;
    }
  }

  configurarItemListaNoticias(proyecto: ProyectoModel): CongifuracionItemProyectosNoticias {

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );

    return {
      id: proyecto.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (data.id) {
            const proyecto: ProyectoModel = this.obtenerProyectoDeLista(data.id);

            if (
              !this.perfilSeleccionado ||
              !proyecto
            ) {
              return;
            }

            if (
              proyecto &&
              this.perfilSeleccionado._id !== proyecto.perfil._id
            ) {
              const modulo = RutasLocales.MODULO_PROYECTOS.toString();
              let ruta = RutasProyectos.VISITAR.toString();
              ruta = ruta.replace(':id', data.id);
              this.router.navigateByUrl(modulo + '/' + ruta);
              return;
            }

            const modulo = RutasLocales.MODULO_PROYECTOS.toString();
            let ruta = RutasProyectos.ACTUALIZAR.toString();
            ruta = ruta.replace(':id', data.id);
            this.router.navigateByUrl(modulo + '/' + ruta);
          }
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    };
  }

  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectoModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectoModel[index];
    }

    return null;
  }

  scroolEnCapaCuerpo(): void {
    if (!this.puedeCargarMas) {
      return;
    }

    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;

      if (this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS || this.params.menosVotado === 'true') {
        this.obtenerProyectosPorFiltro(
          this.filtroGeneral,
          this.params.codigoTipoProyecto,
          this.perfilSeleccionado._id,
          this.obtenerFechaParaParametro(this.fechaInicial),
          this.obtenerFechaParaParametro(this.fechaFinal)
        );
        return;
      }


      this.obtenerProyectosSinFriltroFechas(
        this.perfilSeleccionado._id,
        this.params.codigoTipoProyecto,
      );
    }
  }

  validarFechasIngresadas(): boolean {
    if (this.fechaInicial.getTime() > this.fechaFinal.getTime()) {
      this.toast.abrirToast('text53');
      return false;
    }

    return true;
  }

  ejecutarBusqueda(): void {
    if (this.mostrarCapaLoader) {
      return;
    }
    if (
      this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS &&
      !this.validarFechasIngresadas()
    ) {
      this.filtroActivoDeBusqueda = FiltroBusqueda.RECIENTES;
      return;
    }


    this.inicializarListaProyectos();

    if (this.filtroActivoDeBusqueda === FiltroBusqueda.RANGO_FECHAS || this.params.menosVotado === 'true') {
      this.obtenerProyectosPorFiltro(
        this.filtroGeneral,
        this.params.codigoTipoProyecto,
        this.perfilSeleccionado._id,
        this.obtenerFechaParaParametro(this.fechaInicial),
        this.obtenerFechaParaParametro(this.fechaFinal)
      );
      return;
    }

    this.obtenerProyectosSinFriltroFechas(
      this.perfilSeleccionado._id,
      this.params.codigoTipoProyecto,
    );
  }

  cambioDeFecha(orden: number): void {
    if (orden === 0) {
      const fechaInicial = this.fechaInicial;
      this.fechaInicial = new Date(this.obtenerFechaSeleccionada(fechaInicial));
    }

    if (orden === 1) {
      const fechaFinal = this.fechaFinal;
      this.fechaFinal = new Date(this.obtenerFechaSeleccionada(fechaFinal));
    }
    this.filtroActivoDeBusqueda = FiltroBusqueda.RANGO_FECHAS;
    this.ejecutarBusqueda();
  }

  async buscarProyectosPorTitulo(titulo: string): Promise<void> {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }

    try {
      this.mostrarCapaLoader = (this.listaProyectos.lista.length === 0);
      const dataPaginacion = await this.proyectoNegocio.buscarProyectosPorTitulo(
        titulo,
        16,
        this.listaProyectos.paginaActual
      ).toPromise();

      this.listaProyectos.totalDatos = dataPaginacion.totalDatos;
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina;


      dataPaginacion.lista.forEach(proyecto => {
        this.listaProyectoModel.push(proyecto);
        this.listaProyectos.lista.push(this.configurarItemListaNoticias(proyecto));
      });
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = true;

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1;
      }
    } catch (error) {
      this.mostrarCapaLoader = false;
      this.puedeCargarMas = false;
      this.mostrarCapaError = true;
    }
  }

  obtenerFechaSeleccionada(fecha): string {
    // let date = new Date(fecha);
    // let anio = date.getFullYear().toString();
    // let mes_temporal = date.getMonth() + 1;
    // let dia_temporal = date.getDate() + 1
    // var mes = String(mes_temporal.toString()).padStart(2, '0');
    // let dia = String(dia_temporal.toString()).padStart(2, '0');

    // let fechaSeleccionada = anio + '/' + mes + '/' + dia;

    // return fechaSeleccionada
    const fechaAServer1 = fecha.replace('-', '/');
    const fechaAServer2 = fechaAServer1.replace('-', '/');
    return fechaAServer2;
  }

}

export enum FiltroBusqueda {
  RECIENTES = 'none',
  RANGO_FECHAS = 'fecha',
  MAS_VOTADAS = 'voto'
}




