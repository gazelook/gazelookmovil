import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';

@Component({
  selector: 'app-informacion-util',
  templateUrl: './informacion-util.component.html',
  styleUrls: ['./informacion-util.component.scss']
})
export class InformacionUtilComponent implements OnInit {

  public sesionIniciada: boolean
  public perfilSeleccionado: PerfilModel
  public confAppBar: ConfiguracionAppbarCompartida

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private _location: Location
  ) {  }

  ngOnInit(): void {
    this.configurarEstadoSesion()
    this.configurarPerfilSeleccionado()
    if (this.perfilSeleccionado) {
      this.configurarAppBar()
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }

  configurarEstadoSesion() {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  configurarAppBar() {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v14texto1'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        }
      },
    }
  }
}
