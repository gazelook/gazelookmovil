import { Component, OnInit } from '@angular/core';

// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';

@Component({
    selector: 'app-proyectos',
    templateUrl: './proyectos.component.html',
    styleUrls: ['./proyectos.component.scss']
})

export class ProyectosComponent implements OnInit {

    constructor(
        private notificacionesUsuario: NotificacionesDeUsuario,
        // private llamadaFirebaseService: LlamadaFirebaseService
    ) {
        
    }

    ngOnInit(): void {        
        this.notificacionesUsuario.validarEstadoDeLaSesion(false)
        // this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
    }
}