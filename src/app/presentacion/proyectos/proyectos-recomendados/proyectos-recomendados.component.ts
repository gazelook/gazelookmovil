import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {PaginacionModel} from 'dominio/modelo';
import {
  ConfiguracionAppbarCompartida,
  CongifuracionItemProyectos,
  CongifuracionItemProyectosNoticias,
  LineaCompartida
} from '@shared/diseno/modelos';
import {AlbumModel, PerfilModel, ProyectoModel} from 'dominio/modelo/entidades';
import {AlbumNegocio, PerfilNegocio, ProyectoNegocio} from 'dominio/logica-negocio';
import {ProyectoParams} from 'dominio/modelo/parametros';
import {
  CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil,
  CodigosCatalogoTipoProyecto
} from '@core/servicios/remotos/codigos-catalogos';
import {
  AnchoLineaItem,
  arrayPosotionCenterProject,
  arrayPosotionCenterRigthProject,
  arrayPosotionEndProject,
  arrayPosotionStartProject,
  ColorDeBorde,
  ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  UsoAppBar,
  UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {Location} from '@angular/common';
import {RutasLocales} from '@env/src/app/rutas-locales.enum';
import {RutasProyectos} from '@env/src/app/presentacion/proyectos/rutas-proyectos.enum';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {VariablesGlobales} from '@core/servicios/generales';


@Component({
  selector: 'app-proyectos-recomendados',
  templateUrl: './proyectos-recomendados.component.html',
  styleUrls: ['./proyectos-recomendados.component.scss']
})
export class ProyectosRecomendadosComponent implements OnInit {

// Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public puedeCargarMas: boolean;
  public mensajeCapaError: string;

  public perfilSeleccionado: PerfilModel;

  public listaProyectosRecomendados: PaginacionModel<CongifuracionItemProyectos>;
  public listaProyectosRecomendadosModel: ProyectoModel[] = [];
  public params: ProyectoParams;

  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaNoticias: LineaCompartida;
  public idCapaCuerpo: string;


  private limite = 12;

  constructor(
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.variablesGlobales.mostrarMundo = true;
    this.params = {estado: false};

    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';

    this.idCapaCuerpo = 'capa-cuerpo-intercambios';
  }

  ngOnInit(): void {

    this.inicializarPerfilSeleccionado();
    this.configurarParametros();
    this.configurarAppBar();

    this.inicializarListaProyectos();

    this.obtenerProyectosRecomendados();
    this.configurarLineas();
  }


  obtenerProyectosRecomendados(): void {

    if (!this.listaProyectosRecomendados.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = (this.listaProyectosRecomendados.lista.length === 0);


    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;

    this.proyectoNegocio.obtenerProyectosRecomendados(
      id,
      this.limite,
      this.listaProyectosRecomendados.paginaActual,
      tipo
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosRecomendados.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosRecomendados.paginaActual += 1;
          this.listaProyectosRecomendadosModel = respuesta.lista;
          this.listaProyectosRecomendadosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosRecomendados.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
        },
        (error) => {
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
  }

  configurarItemProyectos(proyecto: ProyectoModel, contador: number): CongifuracionItemProyectos {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );
    return {
      positionStart: arrayPosotionStartProject.includes(contador),
      positionEnd: arrayPosotionEndProject.includes(contador),
      positionCenter: arrayPosotionCenterProject.includes(contador),
      positionCenterRigth: arrayPosotionCenterRigthProject.includes(contador),
      numeroProyecto: contador,
      id: proyecto.id,
      totalVotos: proyecto.totalVotos,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          console.log('evento');
          
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (!data || !this.perfilSeleccionado) {
            return;
          }
          const proyectoLista: ProyectoModel = this.obtenerProyectoDeLista(data.id);

          const ruta = (proyectoLista) && this.perfilSeleccionado._id === proyectoLista.perfil._id
            ? RutasProyectos.ACTUALIZAR.toString().replace(':id', data.id)
            : RutasProyectos.VISITAR.toString().replace(':id', data.id);

          const modulo = RutasLocales.MODULO_PROYECTOS.toString();
          const url = `${modulo}/${ruta}`;
          this.router.navigateByUrl(url).then();
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      },
    };
  }

  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectosRecomendadosModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectosRecomendadosModel[index];
    }

    return null;
  }

  inicializarListaProyectos(): void {
    this.listaProyectosRecomendadosModel = [];
    this.listaProyectosRecomendados = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  scroolEnCapaCuerpo(): void {
    if (!this.puedeCargarMas) {
      return;
    }
    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;
      this.obtenerProyectosRecomendados();
    }
  }

  configurarParametros(): void {
    this.activatedRoute.paramMap.subscribe(
      params => this.params.codigoTipoProyecto = params.get('codigoTipoProyecto') as CodigosCatalogoTipoProyecto
    );
  }

  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarAppBar(): void {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.accionAtras(),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm5v4texto2'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,

          }
        }
      },
    };
  }

  configurarLineas(): void {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  accionAtras(): void {
    this.location.back();
  }
}
