import { MonedaPickerService } from '@core/servicios/generales';
import { VariablesGlobales } from '@core/servicios/generales';
import { Location } from '@angular/common';
import {
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AngularFireAction, AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoTipoMedia } from '@core/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { CatalogoTipoMensaje } from '@core/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { CodigosCatatalogosEstadoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-estado-proyecto.enum';
import { TranslateService } from '@ngx-translate/core';
import { BuscadorModalComponent } from '@shared/componentes/buscador-modal/buscador-modal.component';
import {
  ColorTextoBoton,
  TipoBoton,
} from '@shared/componentes/button/button.component';
import { SelectorComponent } from '@shared/componentes/selector/selector.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AccionesBuscadorLocalidadModal } from '@shared/diseno/enums/acciones-general.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  TamanoLista,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';
import { ConfiguracionComentario } from '@shared/diseno/modelos/comentario.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { InfoAccionSelector } from '@shared/diseno/modelos/info-accion-selector.interface';
import { InfoAccionBuscadorLocalidades } from '@shared/diseno/modelos/info-acciones-buscador-localidades.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import {
  ComentarioFirebaseEntity,
  ComentarioFirebaseEntityMapperService,
} from 'dominio/entidades/comentario.entity';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import {
  ColorDeBorde,
  ColorDeFondo,
  ColorDelTexto,
  ColorFondoLinea,
  EspesorDelBorde,
  EspesorLineaItem,
  EstilosDelTexto,
} from '@shared/diseno/enums';
import {
  ComentariosFirebaseService,
  OrigenConexion,
} from '@core/servicios/generales/comentarios-firebase.service';
import {
  RutasAlbumAudios,
  RutasAlbumGeneral,
  RutasAlbumLinks,
} from '../../album/rutas-albums.enum';
import { RutasProyectos } from '../rutas-proyectos.enum';
import { ColorIconoBoton } from '@shared/componentes';
import { PortadaExpandidaComponent } from '@shared/componentes';
import { AccionesSelector } from '@shared/diseno/enums';
import { TipoIconoBarraInferior } from '@shared/diseno/enums';
import { BarraBusqueda } from '@shared/diseno/modelos';
import { ConfiguracionBuscador, ModoBusqueda } from '@shared/diseno/modelos';
import { ItemDialogoHorizontal } from '@shared/diseno/modelos';
import { ConfiguracionDone } from '@shared/diseno/modelos';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos';
import { LineaCompartida } from '@shared/diseno/modelos';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos';
import { ResumenDataMonedaPicker } from '@shared/diseno/modelos';
import {
  BloquePortada,
  ColoresBloquePortada,
  ConfiguracionPortadaExandida,
  SombraBloque,
  TipoBloqueBortada,
} from '@shared/diseno/modelos';
import { ConfiguracionVotarEntidad } from '@shared/diseno/modelos';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos';
import { ComentarioNegocio } from 'dominio/logica-negocio';
import { IdiomaNegocio } from 'dominio/logica-negocio';
import { TipoMonedaNegocio } from 'dominio/logica-negocio';
import { ParticipanteAsociacionNegocio } from 'dominio/logica-negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio';
import { UbicacionNegocio } from 'dominio/logica-negocio';
import { AlbumModel } from 'dominio/modelo/entidades';
import {
  ComentarioFirebaseModel,
  ComentarioModel,
} from 'dominio/modelo/entidades';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades';
import { VotoProyectoModel } from 'dominio/modelo/entidades';
import { PaginacionModel } from 'dominio/modelo';
import { ProyectoParams } from 'dominio/modelo/parametros';
import { GeneradorId } from '@core/servicios/generales';
import { MetodosParaFotos } from '@core/servicios/generales';
import { ProyectoService } from '@core/servicios/generales';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos';
import { CodigoEstadoParticipanteAsociacion } from '@core/servicios/remotos/codigos-catalogos';
import {
  CodigosCatalogosEstadoComentario,
  CodigosCatatalogosTipoComentario,
} from '@core/servicios/remotos/codigos-catalogos';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos';
import { CodigosCatalogosTipoRol } from '@core/servicios/remotos/codigos-catalogos';
import { FiltroGeneral } from '@core/servicios/remotos/filtro-busqueda';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';
import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { RutasLocales } from './../../../rutas-locales.enum';
import { ChatMetodosCompartidosService } from './../../gazing/chat-metodos-comunes.service';

@Component({
  selector: 'app-publicar',
  templateUrl: './publicar.component.html',
  styleUrls: ['./publicar.component.scss'],
})
export class PublicarComponent implements OnInit, OnDestroy {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  @ViewChild('portadaExpandida', { static: false })
  portadaExpandida: PortadaExpandidaComponent;
  @ViewChild('selectorPaises', { static: false })
  selectorPaises: SelectorComponent;
  @ViewChild('buscadorLocalidades', { static: false })
  buscadorLocalidades: BuscadorModalComponent;

  // Utils
  public util = FuncionesCompartidas;
  public AccionEntidadEnum = AccionEntidad;
  public CodigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum;
  public InputKeyEnum = InputKey;
  public imagenesDefecto: Array<ArchivoModel>;
  public portadaUrl: string;
  public codigosCatalogoTipoProyecto = CodigosCatalogoTipoProyecto;
  public comentarioAEliminar: ComentarioModel;
  public idCapaFormulario: string;
  public puedeCargarMas: boolean;
  public puedeHacerScroolAlFinal: boolean;
  public fechaMaximaUpdate: Date;
  public paraCompartir: boolean;
  public idPerfilCoautorParaResponder: string;
  public idInternoParaResponder: string;
  public indicadorTransferenciaActiva: boolean;
  public origenValidacionCambios: OrigenValidacionDeCambios;
  public mostrarBarraInferior: boolean;
  public idiomaSeleccionado: CatalogoIdiomaEntity;
  // Traducciones
  private tituloOriginal: string;
  private tituloCortoOriginal: string;
  private descripcionOriginal: string;
  private direccionOriginal: string;
  private esOriginal: boolean;

  // Parametros de la url
  public params: ProyectoParams;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;
  public mostrarCapaNormal: boolean;

  // Parametros internos
  public catalogoTipoMoneda: Array<CatalogoTipoMonedaModel>;
  public perfilSeleccionado: PerfilModel;
  public proyecto: ProyectoModel;
  public proyectoForm: FormGroup;
  public inputsForm: Array<Inputs>;
  public maxDescripcion: number;
  public inputProyectoCompleto: string;
  public listaDeFechas: Date[];
  public formatoFecha: string;
  public listaContactos: PaginacionModel<ParticipanteAsociacionModel>;
  public listaContactosSeleccionados: Array<string>;
  public listaContactosOriginal: Array<ConfiguracionItemListaContactosCompartido>;
  public listaContactosBuscador: Array<ConfiguracionItemListaContactosCompartido>;
  public listaResultadosLocalidades: PaginacionModel<ItemSelector>;
  public inglesSiempreActivoEnComentarios: boolean;

  public infoPropietarioProyecto: ConfiguracionItemListaContactosCompartido;

  // TEST comentarios
  public listaComentariosFirebase: Array<ComentarioFirebaseModel>;
  public listaConfiguracionComentarios: Array<ConfiguracionComentario>;
  public indicadorTotalComentarios: IndicadorTotalComentarios;
  public coautores: Array<PerfilModel>;
  // tslint:disable-next-line: ban-types
  public eventoTapComentario: Function;
  // tslint:disable-next-line: ban-types
  public eventoTapPerfilComentario: Function;

  // Configuraciones
  public confToast: ConfiguracionToast;
  public confAppbar: ConfiguracionAppbarCompartida;
  public confPortadaExpandida: ConfiguracionPortadaExandida;
  public confSelector: ConfiguracionSelector;
  public confBuscador: ConfiguracionBuscadorModal;
  public confMonedaPicker: ConfiguracionMonedaPicker;
  public confMonedaPickerMontoFaltante: ConfiguracionMonedaPicker;
  public confMonedaPickerConvertir: ConfiguracionMonedaPicker;
  public confBotonHistorico: BotonCompartido;
  public confBotonCompartir: BotonCompartido;
  public confBotonFullProject: BotonCompartido;
  public confBotonLinks: BotonCompartido;
  public confBotonPublish: BotonCompartido;
  public confBotonTransferir: BotonCompartido;
  public confBotonEliminar: BotonCompartido;
  public confLineaComentarios: LineaCompartida;
  public confBarraInferior: ConfiguracionBarraInferiorInline;
  public confDialogoEliminarProyecto: DialogoCompartido;
  public confDialogoEliminarComentario: DialogoCompartido;
  public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta;
  public confVotarEntidad: ConfiguracionVotarEntidad;
  public confListaContactoCompartido: ConfiguracionListaContactoCompartido;
  public confBuscadorProyectos: ConfiguracionBuscador;
  public confDialogoFullProyecto: DialogoCompartido;
  public confDone: ConfiguracionDone;
  public confDialogoSalida: DialogoCompartido;
  public confDialogoEliminarPDF: DialogoCompartido;
  public confDialogoConfirmarTransferencia: DialogoCompartido;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public proyectoService: ProyectoService,
    public comenFireService: ComentariosFirebaseService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private perfilNegocio: PerfilNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private albumNegocio: AlbumNegocio,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private mediaNegocio: MediaNegocio,
    private translateService: TranslateService,
    private ubicacionNegocio: UbicacionNegocio,
    private comentarioNegocio: ComentarioNegocio,
    private generadorId: GeneradorId,
    private participanteAsociacionNegocio: ParticipanteAsociacionNegocio,
    private chatMetodosCompartidosService: ChatMetodosCompartidosService,
    private metodosParaFotos: MetodosParaFotos,
    private db: AngularFireDatabase,
    private metodosSessionStorageService: MetodosSessionStorageService,
    private comentarioFirebaseEntityMapperService: ComentarioFirebaseEntityMapperService,
    private idiomaNegocio: IdiomaNegocio,
    private variablesGlobales: VariablesGlobales,
    private monedaPickerService: MonedaPickerService
  ) {
    this.listaContactosSeleccionados = [];
    this.listaContactosBuscador = [];
    this.listaContactosOriginal = [];
    this.idCapaFormulario =
      'capa_formulario_' + this.generadorId.generarIdConSemilla();
    this.puedeCargarMas = true;
    this.puedeHacerScroolAlFinal = false;
    this.imagenesDefecto = [];
    this.portadaUrl = '';
    this.catalogoTipoMoneda = [];
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.mensajeCapaError = '';
    this.inputsForm = [];
    this.maxDescripcion = 1000;
    this.inputProyectoCompleto = 'input-file-pdf';
    this.params = { estado: false };
    this.listaDeFechas = [];
    this.formatoFecha = 'dd/MM/yyyy';
    this.paraCompartir = true;
    this.idPerfilCoautorParaResponder = '';
    this.idInternoParaResponder = '';
    this.indicadorTransferenciaActiva = false;
    this.esOriginal = false;
    this.inglesSiempreActivoEnComentarios = false;

    this.listaComentariosFirebase = [];
    this.listaConfiguracionComentarios = [];
    this.coautores = [];
    this.mostrarBarraInferior = false;
  }

  ngOnInit(): void {
    this.configurarParametrosDeLaUrl();
    this.inicializarPerfilSeleccionado();
    this.inicializarDataListaContactos();
    this.inicializarFechaMaximaUpdate();
    this.configurarIndicadorTotalDeComentarios();
    this.configurarDone();

    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarToast();
      this.configurarAppBar(-1);
      this.configurarLineas();
      this.configurarDialogoEliminarPDF();
      this.configurarDialogoEliminarProyecto();
      this.configurarDialogoEliminarComentario();
      this.configurarDialologConfirmarTransferencia();
      this.configurarBarraInferiorInline();
      this.configurarImagenPantallaCompleta();
      this.configurarListaContactoCompartido();
      this.configurarDialogoFullProyecto();
      this.configurarDialogoConfirmarSalida();

      this.inicializarDataTipoMoneda();
      this.inicializarDataDeLaEntidad();

      window.onbeforeunload = () =>
        this.validarInformacionDelProyectoAntesDeRecargarOCambiarDePaginaHaciaDelante(
          this.params.accionEntidad !== AccionEntidad.CREAR
        );
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(
        this.perfilSeleccionado,
        this.params.estado
      );
    }
  }

  ngOnDestroy(): void {
    this.comenFireService.desconectar(OrigenConexion.COMENTARIOS);
  }

  // Escucha para el boton de back del navegador
  @HostListener('window:popstate', ['$event'])
  onPopState(event: any): void {
    this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
  }

  accionAtras(): void {
    if (this.esOriginal) {
      this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto = 'm4v1texto2';
      this.proyecto.tituloCorto = this.tituloCortoOriginal;
      this.proyecto.titulo = this.tituloOriginal;
      this.proyecto.descripcion = this.descripcionOriginal;
      this.proyecto.direccion.descripcion = this.direccionOriginal;
      this.proyectoForm =
        this.proyectoService.inicializarControlesFormularioAccionActualizar(
          this.proyecto
        );
      this.inputsForm =
        this.proyectoService.configurarInputsDelFormularioConKey(
          this.proyectoForm,
          true
        );
      this.esOriginal = false;
      return;
    }

    if (
      this.params.accionEntidad !== AccionEntidad.REGISTRO &&
      this.confListaContactoCompartido &&
      this.confListaContactoCompartido.mostrar
    ) {
      this.confListaContactoCompartido.mostrar = false;
      this.confListaContactoCompartido.botonAccion.mostrarDialogo = false;
      this.confListaContactoCompartido.listaContactos.lista = [];
      this.listaContactosBuscador = [];
      this.listaContactosOriginal = [];

      this.inicializarDataListaContactos();
      this.configurarListaContactoCompartido();
      this.cambiarEstadoAppBarParaListaDeContactos(false);
      return;
    }

    if (
      (this.params.accionEntidad === AccionEntidad.ACTUALIZAR ||
        this.params.accionEntidad === AccionEntidad.CREAR) &&
      this.proyecto &&
      this.proyecto.perfil &&
      this.proyecto.perfil._id === this.perfilSeleccionado._id &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_BACK;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    this.navegarAlBack();
  }

  validarInformacionDelProyectoAntesDeRecargarOCambiarDePaginaHaciaDelante(
    eliminarData: boolean = false
  ): void {
    this.proyecto = this.proyectoNegocio.asignarValoresDeLosCamposAlProyecto(
      this.params,
      this.proyecto,
      this.confSelector.seleccionado,
      this.confMonedaPicker.inputCantidadMoneda.valor,
      this.confMonedaPicker.selectorTipoMoneda.seleccionado,
      this.proyectoForm,
      eliminarData
    );
  }

  // Parametros de url
  configurarParametrosDeLaUrl(): void {
    this.params.accionEntidad =
      this.proyectoService.determinarAccionProyectoPorUrl(this.router.url);
    if (this.params.accionEntidad) {
      const urlParams: Params = this.route.snapshot.params;
      this.params = this.proyectoService.validarParametrosSegunAccionEntidad(
        this.params,
        urlParams
      );
      if (this.params.accionEntidad !== AccionEntidad.CREAR) {
        this.mostrarBarraInferior = true;
      } else {
        this.mostrarBarraInferior = false;
      }
    }
  }

  // Inicializar perfil seleccionado
  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
  }

  inicializarDataListaContactos(): void {
    this.listaContactos = {
      anteriorPagina: false,
      paginaActual: 1,
      proximaPagina: true,
      totalDatos: 0,
      totalPaginas: 0,
      lista: [],
    };
  }

  async inicializarFechaMaximaUpdate(): Promise<void> {
    try {
      const data: { fecha: Date } = await this.proyectoNegocio
        .obtenerFechaMaximaParaActualizarValorEstimado()
        .toPromise();
      this.fechaMaximaUpdate = data.fecha;
    } catch (error) {
      this.fechaMaximaUpdate = new Date();
    }
  }

  configurarIndicadorTotalDeComentarios(
    total: number = 0,
    comentarios: Array<string> = []
  ): void {
    this.indicadorTotalComentarios = {
      total,
      comentarios,
    };
  }

  // Inicializar data de la entidad
  inicializarDataDeLaEntidad(): void {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.inicializarContenidoParaAccionCrear();
        break;
      case AccionEntidad.ACTUALIZAR:
        this.inicializarDataParaAccionActualizar();
        break;
      case AccionEntidad.VISITAR:
        this.inicializarDataParaAccionVisitar();
        break;
      default:
        break;
    }
  }

  inicializarContenidoParaAccionCrear(): void {
    this.proyecto = this.proyectoNegocio.validarProyectoActivoSegunAccionCrear(
      this.params.codigoTipoProyecto,
      this.perfilSeleccionado
    );

    // Utils
    this.inicializarControlesSegunAccion();
    this.inicializarInputs();
    this.inicializarDataTipoMoneda();
    // Componentes hijos
    this.configurarPortadaExpandida();
    this.configurarSelector();
    // this.configurarBuscador()
    this.configurarMonedaPicker();
    this.configurarBotones();
    this.inicializarImagenesPorDefecto();
  }

  async inicializarDataParaAccionActualizar(): Promise<void> {
    try {
      this.mostrarCapaLoader = true;

      this.proyecto = await this.proyectoNegocio
        .obtenerInformacionDelProyecto(
          this.params.id,
          this.perfilSeleccionado._id
        )
        .toPromise();

      this.mostrarBarraInferior = true;

      this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
      this.proyectoNegocio.guardarProyectoActivoEnSessionStorage(this.proyecto);

      if (!this.proyecto.id || !this.proyecto.tipo) {
        this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
        throw new Error('text31');
      }

      if (
        this.perfilSeleccionado &&
        this.proyecto.perfil &&
        this.perfilSeleccionado._id !== this.proyecto.perfil._id
      ) {
        this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
        throw new Error('text31');
      }

      // Utils
      this.inicializarControlesSegunAccion();
      this.inicializarInputs();
      // Componentes hijos
      this.configurarPortadaExpandida();
      this.inicializarImagenesPorDefecto();
      this.configurarSelector();
      this.configurarBuscador();
      this.configurarMonedaPicker();
      this.configurarBotones();
      this.configurarEstadoTransferenciaActiva();

      this.configurarEventosComentario();
      this.configurarEscuchaTotalDeComentarios();
      this.obtenerTotalDeComentarios();
      this.configurarEscuchaListaDeComentarios();
      this.obtenerComentarios();

      this.mostrarCapaLoader = false;
      if (this.confBarraInferior) {
        this.confBarraInferior.desactivarBarra =
          this.configurarEstadoDeLaBarraInferior();
      }
    } catch (error) {
      this.mensajeCapaError = 'text31';
      this.mostrarCapaLoader = false;
      this.mostrarCapaError = true;
    }
  }

  async inicializarDataParaAccionVisitar(): Promise<void> {
    try {
      this.mostrarCapaLoader = true;
      this.proyecto = await this.proyectoNegocio
        .obtenerInformacionDelProyecto(
          this.params.id,
          this.perfilSeleccionado._id
        )
        .toPromise();

      if (
        this.proyecto.estado.codigo ===
        CodigosCatatalogosEstadoProyecto.PROYECTO_PRE_ESTRATEGIA
      ) {
        this.mostrarBarraInferior = false;
      } else {
        this.mostrarBarraInferior = true;
      }

      this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
      this.proyectoNegocio.guardarProyectoActivoEnSessionStorage(this.proyecto);

      if (!this.proyecto.id || !this.proyecto.tipo) {
        this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
        throw new Error('text31');
      }

      // Utils
      this.configurarAppBar(-1);
      this.inicializarControlesSegunAccion();
      this.inicializarInputs();
      // Componentes hijos
      this.configurarPortadaExpandidaParaVisita();
      this.inicializarImagenesPorDefecto();
      this.configurarSelector();
      this.configurarBuscador();
      this.configurarMonedaPicker(true);
      this.configurarMonedaPickerMontoFaltante(true);
      this.configurarMonedaPickerConvertir();
      this.configurarBotones();
      this.configurarVotarEntidad();

      this.configurarEventosComentario();
      this.configurarEscuchaTotalDeComentarios();
      this.obtenerTotalDeComentarios();
      this.configurarEscuchaListaDeComentarios();
      this.obtenerComentarios();

      this.infoPropietarioProyecto = this.configuracionPropietarioProyecto();

      this.mostrarCapaLoader = false;
      if (this.confBarraInferior) {
        this.confBarraInferior.desactivarBarra =
          this.configurarEstadoDeLaBarraInferior();
      }

      if (
        this.proyecto.tituloCortoOriginal &&
        this.proyecto.tituloOriginal &&
        this.proyecto.descripcionOriginal
      ) {
        this.confAppbar.searchBarAppBar.idiomaOriginal.mostrarOriginal = true;
      }

      this.confMonedaPickerMontoFaltante.inputCantidadMoneda.valor.valorNeto =
        this.proyecto.montoFaltante
          ? this.proyecto.montoFaltante.toString()
          : '0';
    } catch (error) {
      this.mensajeCapaError = 'text31';
      this.mostrarCapaLoader = false;
      this.mostrarCapaError = true;
    }
  }

  // Obtener la lista de imagenes por defecto
  async inicializarImagenesPorDefecto(): Promise<void> {
    try {
      const archivos = this.mediaNegocio.obtenerArchivosDefaultPorTipo(
        CodigosCatalogoArchivosPorDefecto.PROYECTOS
      );

      if (!archivos || archivos === null) {
        throw new Error('');
      }

      this.imagenesDefecto = archivos;
      this.definirImagenDeLaPortadaExpandida();
    } catch (error) {
      this.imagenesDefecto = [];
      this.definirImagenDeLaPortadaExpandida();
    }
  }

  // Setear imagen por defecto
  definirImagenDeLaPortadaExpandida(): void {
    const data = this.proyectoService.determinarUrlImagenPortada(
      this.proyecto,
      this.imagenesDefecto
    );

    this.confPortadaExpandida.urlMedia = data.url;
    this.confPortadaExpandida.mostrarLoader =
      this.confPortadaExpandida.urlMedia.length > 0;

    this.confPortadaExpandida.bloques.forEach((bloque, pos) => {
      if (pos === 0) {
        bloque.colorFondo = data.porDefecto
          ? ColoresBloquePortada.AZUL_FUERTE
          : ColoresBloquePortada.AZUL_FUERTE_CON_OPACIDAD;
      }

      if (pos === 1) {
        bloque.colorFondo = data.porDefecto
          ? ColoresBloquePortada.AZUL_DEBIL
          : ColoresBloquePortada.AZUL_DEBIL_CON_OPACIDAD;
      }
    });
  }

  // Inicializar controles del formulario
  inicializarControlesSegunAccion(): void {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.proyectoForm =
          this.proyectoService.inicializarControlesFormularioAccionCrear(
            this.proyecto
          );
        break;
      case AccionEntidad.ACTUALIZAR:
        this.proyectoForm =
          this.proyectoService.inicializarControlesFormularioAccionActualizar(
            this.proyecto
          );
        break;
      case AccionEntidad.VISITAR:
        this.proyectoForm =
          this.proyectoService.inicializarControlesFormularioAccionActualizar(
            this.proyecto
          );
        break;
      default:
        break;
    }
  }

  // Inicializar los inputs
  async inicializarInputs(): Promise<void> {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.inputsForm =
          this.proyectoService.configurarInputsDelFormularioConKey(
            this.proyectoForm
          );
        break;
      case AccionEntidad.ACTUALIZAR:
        this.inputsForm =
          this.proyectoService.configurarInputsDelFormularioConKey(
            this.proyectoForm
          );
        break;
      case AccionEntidad.VISITAR:
        this.inputsForm =
          this.proyectoService.configurarInputsDelFormularioConKey(
            this.proyectoForm,
            true
          );
        break;
      default:
        break;
    }
  }

  inicializarDataTipoMoneda(): void {
    this.tipoMonedaNegocio
      .obtenerCatalogoTipoMonedaParaElegibles()
      .subscribe((elegibles) => {
        this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
      });
  }

  // Configuracion del appbar
  async configurarAppBar(
    estatusSubTitulo: number,
    buscador?: BarraBusqueda
  ): Promise<void> {
    let subtitulo: string =
      this.proyectoService.obtenerSubtituloAppBarSegunAccionEntidadParaProyecto(
        this.params
      );

    if (estatusSubTitulo === 1) {
      subtitulo = 'm4v2texto2';
    }

    if (estatusSubTitulo === 2) {
      subtitulo = 'm4v17texto1';
    }

    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {
        this.accionAtras();
      },
      eventoHome: () => {
        if (
          (this.params.accionEntidad === AccionEntidad.ACTUALIZAR ||
            this.params.accionEntidad === AccionEntidad.CREAR) &&
          this.proyecto &&
          this.proyecto.perfil &&
          this.proyecto.perfil._id === this.perfilSeleccionado._id &&
          this.validarSiExistenCambios()
        ) {
          this.origenValidacionCambios = OrigenValidacionDeCambios.BOTON_HOME;
          this.configurarDialogoConfirmarSalida(true);
          return;
        }

        this.navegarAlHome();
      },
      searchBarAppBar: {
        tamanoColorFondo:
          estatusSubTitulo === 3
            ? TamanoColorDeFondo.TAMANO6920
            : TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: subtitulo,
        },
        buscador: buscador
          ? buscador
          : {
              mostrar: false,
              configuracion: {
                disable: true,
              },
            },
        idiomaOriginal: {
          mostrarOriginal: false,
          llaveTexto: 'm4v1texto2',
          clickMostrarOriginal: () => {
            if (this.esOriginal) {
              this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto =
                'm4v1texto2';
              this.proyecto.tituloCorto = this.tituloCortoOriginal;
              this.proyecto.titulo = this.tituloOriginal;
              this.proyecto.descripcion = this.descripcionOriginal;
              this.proyecto.direccion.descripcion = this.direccionOriginal;
              this.proyectoForm =
                this.proyectoService.inicializarControlesFormularioAccionActualizar(
                  this.proyecto
                );
              this.inputsForm =
                this.proyectoService.configurarInputsDelFormularioConKey(
                  this.proyectoForm,
                  true
                );
              this.esOriginal = false;

              return;
            }

            if (!this.esOriginal) {
              this.esOriginal = true;
              this.tituloOriginal = this.proyecto.titulo;
              this.tituloCortoOriginal = this.proyecto.tituloCorto;
              this.descripcionOriginal = this.proyecto.descripcion;
              this.direccionOriginal = this.proyecto.direccion.descripcion;

              if (
                this.proyecto.tituloCortoOriginal &&
                this.proyecto.tituloOriginal &&
                this.proyecto.descripcionOriginal
                //  && this.proyecto.direccion.descripcionOriginal
              ) {
                this.proyecto.tituloCorto = this.proyecto?.tituloCortoOriginal;
                this.proyecto.titulo = this.proyecto?.tituloOriginal;
                this.proyecto.descripcion = this.proyecto?.descripcionOriginal;
                this.proyecto.direccion.descripcion =
                  this.proyecto.direccion?.descripcionOriginal;
              }

              this.proyectoForm =
                this.proyectoService.inicializarControlesFormularioAccionActualizar(
                  this.proyecto
                );
              this.inputsForm =
                this.proyectoService.configurarInputsDelFormularioConKey(
                  this.proyectoForm,
                  true
                );
              this.confAppbar.searchBarAppBar.idiomaOriginal.llaveTexto =
                'm4v1texto2.1';
              return;
            }
          },
        },
      },
    };
  }

  // Configuracion de la portada expandida
  configurarPortadaExpandida(): void {
    this.confPortadaExpandida = {
      urlMedia: '',
      mostrarLoader: false,
      bloques: [
        {
          tipo: TipoBloqueBortada.BOTON_PROYECTO,
          colorFondo: ColoresBloquePortada.AZUL_FUERTE,
          conSombra: SombraBloque.SOMBRA_NEGRA,
          llaveTexto: 'm4v5texto2',
          eventoTap: () => this.irAlAlbumGeneral(),
        },
        {
          tipo: TipoBloqueBortada.INFO_PROYECTO,
          colorFondo: ColoresBloquePortada.AZUL_DEBIL,
          llaveTexto: 'm4v5texto3',
        },
      ],
    };
  }

  configurarPortadaExpandidaParaVisita(): void {
    this.confPortadaExpandida = {
      urlMedia: '',
      mostrarLoader: false,
      bloques: [],
      eventoTapPortada: {
        activarEvento: false,
        evento: () => {
          this.irAlAlbumGeneral();
        },
      },
      eventoDobleTapPortada: {
        activarEvento: true,
        evento: () => {
          this.validarAccionDobleTapEnPortadaExpandida();
        },
      },
    };

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      this.proyecto.adjuntos
    );

    if (!album || (album && album.media && album.media.length === 0)) {
      const bloque: BloquePortada = {
        colorFondo: ColoresBloquePortada.AZUL_DEBIL,
        llaveTexto: 'm4v1texto5',
        tipo: TipoBloqueBortada.NO_ADDED_INFO,
      };
      this.confPortadaExpandida.bloques.push(bloque);
    }

    if (this.proyecto.actualizado) {
      this.confPortadaExpandida.bloques.push({
        tipo: TipoBloqueBortada.ACTUALUZADO_INFO,
        llaveTexto: 'm3v3texto10',
        colorFondo: ColoresBloquePortada.AMARILLO_BASE,
      });
    }
  }

  async configurarSelector(): Promise<void> {
    const item: ItemSelector =
      this.proyectoService.obtenerPaisSeleccionadoEnElProyecto(this.proyecto);

    this.confSelector = {
      tituloSelector: 'text61',
      mostrarModal: false,
      inputPreview: {
        mostrar: true,
        input: {
          valor: item.nombre,
          placeholder: 'Country:',
          textoBold: true,
          textoEnMayusculas: true,
        },
      },
      seleccionado: item,
      elegibles: [],
      cargando: {
        mostrar: false,
      },
      error: {
        mostrarError: false,
        contenido: '',
        tamanoCompleto: false,
      },
      quitarMarginAbajo: true,
      evento: (data: InfoAccionSelector) => this.eventoEnSelector(data),
    };

    this.confSelector.inputPreview.input.placeholder =
      await this.translateService.get('m4v5texto10').toPromise();
  }

  async configurarBuscador(): Promise<void> {
    const item: ItemSelector =
      this.proyectoService.obtenerLocalidadSeleccionadaEnElProyecto(
        this.proyecto
      );
    const pais: ItemSelector =
      this.proyectoService.obtenerPaisSeleccionadoEnElProyecto(this.proyecto);

    this.confBuscador = {
      seleccionado: item,
      inputPreview: {
        mostrar: true,
        input: {
          placeholder: 'Postal Code:',
          valor: item.nombre.length > 0 ? item.nombre : '',
          auxiliar: item.auxiliar,
          textoBold: true,
          textoEnMayusculas: true,
        },
        quitarMarginAbajo: true,
      },
      mostrarModal: false,
      inputBuscador: {
        valor: '',
        placeholder: 'Find your location',
      },
      resultado: {
        mostrarElegibles: false,
        mostrarCargando: false,
        error: {
          mostrarError: false,
          contenido: '',
          tamanoCompleto: false,
        },
        items: [],
      },
      pais,
      evento: (data: InfoAccionBuscadorLocalidades) =>
        this.eventoEnBuscador(data),
    };

    this.confBuscador.inputPreview.input.placeholder =
      await this.translateService.get('m4v5texto11').toPromise();
  }

  obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    // tslint:disable-next-line: radix
    const valorEstimado = 0;
    const moneda: CatalogoTipoMonedaModel = {
      codigo: '',
      codNombre: '',
    };
    const data = {
      valorEstimado: {
        contador: 0,
        valorFormateado:
          this.variablesGlobales.valorFormateadoInicialParaSuscripcion,
        valorNeto: this.variablesGlobales.valorNetoInicialParaSuscripcion,
      },
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda),
    };
    return data;
  }

  configurarMonedaPicker(soloLectura: boolean = false): void {
    const dataMoneda: ResumenDataMonedaPicker =
      this.proyectoService.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(
        this.proyecto
      );
    this.confMonedaPicker = {
      inputCantidadMoneda: {
        valor: dataMoneda.valorEstimado,
        colorFondo: ColorDeFondo.FONDO_AZUL_CLARO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        soloLectura,
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: this.params.accionEntidad !== AccionEntidad.VISITAR,
          llaveTexto: 'm4v5texto14',
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO,
          },
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) =>
          this.eventoEnSelectorDeTipoMoneda(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: '',
        },
        soloLectura,
      },
    };
  }

  configurarMonedaPickerMontoFaltante(soloLectura: boolean = false): void {
    const dataMoneda: ResumenDataMonedaPicker =
      this.proyectoService.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(
        this.proyecto
      );
    this.confMonedaPickerMontoFaltante = {
      inputCantidadMoneda: {
        valor: dataMoneda.valorEstimado,
        colorFondo: ColorDeFondo.FONDO_AZUL_CLARO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        soloLectura,
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: this.params.accionEntidad !== AccionEntidad.VISITAR,
          llaveTexto: 'm4v5texto14',
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO,
          },
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) =>
          this.eventoEnSelectorDeTipoMoneda(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: '',
        },
        soloLectura,
      },
    };
  }

  configurarMonedaPickerConvertir(): void {
    const dataMoneda: ResumenDataMonedaPicker =
      this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();
    this.confMonedaPickerConvertir = {
      inputCantidadMoneda: {
        valor: dataMoneda.valorEstimado,
        colorFondo: ColorDeFondo.FONDO_AZUL_CLARO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        soloLectura: true,
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm4v5texto14',
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO,
          },
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) =>
          this.eventoEnSelectorDeTipoMonedaConvertir(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: '',
        },
        soloLectura: false,
      },
    };
  }

  async configurarBotones(): Promise<void> {
    // Boton historico
    this.confBotonHistorico = {
      text: 'm4v5texto18',
      colorTexto: ColorTextoBoton.CELESTE,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.irAlHistoricoDelProyecto();
      },
    };
    // Boton compartir
    this.confBotonCompartir = {
      text: 'm4v5texto19',
      colorTexto: ColorTextoBoton.AMARRILLO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.paraCompartir = true;
        this.validarAccionCompartirProyecto();
      },
    };
    // Boton articulo o proyecto completo
    this.confBotonFullProject = {
      text: 'm4v5texto25',
      colorTexto: ColorTextoBoton.ROJO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.eventoBotonFullProject(),
    };
    // Boton link
    this.confBotonLinks = {
      text: 'm4v5texto28',
      colorTexto: ColorTextoBoton.VERDE,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.irAlAlbumDeLinks();
      },
    };
    // Boton submit
    this.confBotonPublish = {
      text: 'm4v5texto29',
      colorTexto: ColorTextoBoton.AMARRILLO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.eventoEnBotonPublish(),
    };
    // Boton Transferir
    this.confBotonTransferir = {
      text: 'm4v6texto30',
      colorTexto: ColorTextoBoton.AZUL,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.eventoEnBotonTransferir(),
    };
    // Boton Eliminar
    this.confBotonEliminar = {
      text: 'm4v6texto31',
      colorTexto: ColorTextoBoton.ROJO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.eventoEnBotonEliminar(),
    };
  }

  configurarToast(): void {
    this.confToast = {
      mostrarToast: false, // True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    };
  }

  configurarLineas(): void {
    this.confLineaComentarios = {
      ancho: AnchoLineaItem.ANCHO100,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

  configurarEstadoDeLaBarraInferior(): boolean {
    if (this.mostrarCapaError || this.mostrarCapaLoader) {
      return true;
    }

    if (this.proyecto && this.proyecto.estado && this.proyecto.estado.codigo) {
      switch (this.proyecto.estado.codigo as CodigosCatatalogosEstadoProyecto) {
        case CodigosCatatalogosEstadoProyecto.PROYECTO_ACTIVO:
          return false;
        case CodigosCatatalogosEstadoProyecto.PROYECTO_EN_ESTRATEGIA:
          return false;
        case CodigosCatatalogosEstadoProyecto.PROYECTO_FORO:
          return false;
        case CodigosCatatalogosEstadoProyecto.PROYECTO_FORO_SEGUNDA_ESTAPA:
          return false;
        default:
          return true;
      }
    }

    return true;
  }

  configurarBarraInferiorInline(): void {
    this.confBarraInferior = {
      desactivarBarra: this.configurarEstadoDeLaBarraInferior(),
      capaColorFondo: {
        mostrar: true,
        anchoCapa: TamanoColorDeFondo.TAMANO100,
        colorDeFondo: ColorDeFondo.FONDO_TRANSPARENCIA_BASE,
      },
      estatusError: (error: string) => {},
      placeholder: {
        mostrar: true,
        texto: 'm3v10texto15',
        configuracion: {
          color: ColorDelTexto.TEXTOROJOBASE,
          enMayusculas: true,
          estiloTexto: EstilosDelTexto.REGULAR,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2,
        },
      },
      iconoTexto: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_TEXTO,
          eventoTap: (dataApiArchivo: SubirArchivoData) => {
            if (
              dataApiArchivo.descripcion &&
              dataApiArchivo.descripcion.length > 0
            ) {
              this.enviarComentario(dataApiArchivo);
            }
          },
        },
        capa: {
          mostrar: false,
        },
      },
      iconoAudio: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_AUDIO,
          eventoTap: (dataApiArchivo: SubirArchivoData) =>
            this.enviarComentario(dataApiArchivo, false),
        },
        capa: {
          siempreActiva: false,
          grabadora: {
            noPedirTituloGrabacion: true,
            usarLoader: true,
            grabando: false,
            duracionActual: 0,
            tiempoMaximo: 300,
            factorAumentoLinea: 100 / 300, // 100% del ancho divido para tiempoMaximo
          },
          clickParar: true,
        },
      },
      botonSend: {
        mostrar: true,
        evento: (dataApiArchivo: SubirArchivoData) => {
          this.enviarComentario(dataApiArchivo);
        },
      },
    };
  }

  configurarImagenPantallaCompleta(): void {
    this.confImagenPantallaCompleta = {
      mostrar: false,
      mostrarLoader: true,
      urlMedia: '',
      eventoBotonCerrar: () => {
        this.confImagenPantallaCompleta.mostrar = false;
        this.confImagenPantallaCompleta.mostrarLoader = true;
        this.confImagenPantallaCompleta.urlMedia = '';
      },
      eventoBotonDescargar: () => {
        const link = document.createElement('a');
        link.href = this.confImagenPantallaCompleta.urlMedia;
        link.target = '_blank';
        link.dispatchEvent(
          new MouseEvent('click', {
            view: window,
            bubbles: false,
            cancelable: true,
          })
        );
      },
    };
  }

  configurarListaContactoCompartido(): void {
    this.confListaContactoCompartido = {
      mostrar: false,
      llaveSubtitulo: this.paraCompartir ? 'm4v2texto4' : 'm4v17texto3',
      listaContactos: {
        lista: [],
        cargarMas: () => {
          if (this.listaContactos.proximaPagina) {
            this.listaContactos.paginaActual += 1;
            this.obtenerContactos();
          }
        },
        reintentar: () => {
          this.inicializarDataListaContactos();
          this.obtenerContactos();
        },
        cargando: false,
        tamanoLista: TamanoLista.LISTA_CONTACTOS,
      },
      botonAccion: {
        mostrarDialogo: false,
        mostrarDialogoLibre: true,
        tipo: TipoDialogo.MULTIPLE_ACCION_HORIZONTAL_INFORMACION,
        completo: false,
        accionesDialogoHorizontal: [
          {
            accion: {
              ejecutar: () => {
                if (this.paraCompartir) {
                  this.enviarProyectoComoMensaje(
                    CatalogoTipoMensaje.COMPARTIR_PROYECTO
                  );
                  return;
                }

                if (!this.paraCompartir) {
                  this.confDialogoConfirmarTransferencia.mostrarDialogo = true;
                }
              },
              enProgreso: false,
              tipoBoton: TipoBoton.ICON_COLOR,
              colorIcono: ColorIconoBoton.ROJO,
            },
            descripcion: this.paraCompartir ? 'm4v2texto6' : 'm4v17texto1',
            estilosTexto: {
              color: ColorDelTexto.TEXTOROJOBASE,
              estiloTexto: EstilosDelTexto.BOLD,
              enMayusculas: true,
              tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_IGUAL,
            },
          },
        ],
      },
      contactoSeleccionado: (idAsociacion: string) => {
        const posEnSeleccionado: number =
          this.listaContactosSeleccionados.findIndex((e) => e === idAsociacion);
        const posEnListaContactos: number =
          this.confListaContactoCompartido.listaContactos.lista.findIndex(
            (e) => e.id === idAsociacion
          );
        const contacto =
          this.confListaContactoCompartido.listaContactos.lista[
            posEnListaContactos
          ];

        if (posEnSeleccionado >= 0) {
          this.listaContactosSeleccionados.splice(posEnSeleccionado, 1);
          if (posEnListaContactos >= 0) {
            contacto.configCirculoFoto.mostrarCorazon = false;
            contacto.configCirculoFoto.colorBorde = ColorDeBorde.BORDER_ROJO;
            this.confListaContactoCompartido.listaContactos.lista[
              posEnListaContactos
            ] = contacto;
          }

          this.confListaContactoCompartido.botonAccion.mostrarDialogo =
            this.listaContactosSeleccionados.length > 0;
          return;
        }

        if (posEnSeleccionado <= 0) {
          if (
            !this.paraCompartir &&
            this.listaContactosSeleccionados.length === 1
          ) {
            return;
          }

          this.listaContactosSeleccionados.push(idAsociacion);
          if (posEnListaContactos >= 0) {
            contacto.configCirculoFoto.mostrarCorazon = true;
            contacto.configCirculoFoto.colorBorde = ColorDeBorde.BORDER_AZUL;
            this.confListaContactoCompartido.listaContactos.lista[
              posEnListaContactos
            ] = contacto;
          }

          this.confListaContactoCompartido.botonAccion.mostrarDialogo =
            this.listaContactosSeleccionados.length > 0;
          return;
        }
      },
    };
  }

  configurarDialogoFullProyecto(
    mostrar: boolean = false,
    botones: Array<ItemDialogoHorizontal> = []
  ): void {
    this.confDialogoFullProyecto = {
      mostrarDialogo: mostrar,
      completo: true,
      tipo: TipoDialogo.MULTIPLE_ACCION_HORIZONTAL_INFORMACION,
      accionesDialogoHorizontal: botones,
    };
  }

  configurarDialogoEliminarProyecto(): void {
    this.confDialogoEliminarProyecto = {
      mostrarDialogo: false,
      completo: true,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: 'm4v6texto38',
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminarProyecto.mostrarDialogo = false;
            this.eliminarProyecto();
          },
        },
        {
          text: 'm3v9texto3',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminarProyecto.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  async configurarDialogoEliminarComentario(): Promise<void> {
    this.confDialogoEliminarComentario = {
      mostrarDialogo: false,
      completo: true,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: 'ERASE MESSAGE?',
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          enProgreso: false,
          ejecutar: () => {
            this.eliminarComentario();
          },
        },
        {
          text: 'm3v9texto3',
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminarComentario.mostrarDialogo = false;
          },
        },
      ],
    };
    const texto = await this.translateService.get('m4v6texto36').toPromise();
    const texto2 = await this.translateService.get('m4v6texto37').toPromise();

    this.confDialogoEliminarComentario.descripcion = texto + '\n' + texto2;
  }

  configurarVotarEntidad(): void {
    const title = !this.proyecto.voto
      ? ['m4v1texto19', 'm4v1texto20']
      : this.proyecto.montoFaltante
      ? ['m5v8texto5']
      : ['m4v1texto22', 'm4v1texto23'];

    this.confVotarEntidad = {
      id: this.proyecto.id || '',
      entidad: CodigosCatalogoEntidad.PROYECTO,
      voto: this.proyecto.voto,
      bloqueTitulo: {
        coloDeFondo: ColorDeFondo.FONDO_CELESTE_CON_OPACIDAD,
        llavesTexto: title,
      },
      bloqueBoton: {
        llaveTexto: 'm4v1texto21',
        activarEventoTap: !this.proyecto.voto || !!this.proyecto.montoFaltante,
        eventoTap: () => {
          // this.donacionProyectos();
          if (this.proyecto.montoFaltante) {
            this.donacionProyectos();
          } else {
            this.apoyarProyecto().then();
          }
        },
      },
      apoyarProyecto: !!this.proyecto.montoFaltante,
    };
  }

  donacionProyectos(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    const componente = RutasProyectos.DONACIONES_PROYECTOS.toString().replace(
      ':codigoTipoProyecto',
      this.proyecto.id
    );
    const url = `${ruta}/${componente}`;

    this.router.navigateByUrl(url).then();
  }

  configurarListaPaginacionLocalidades(): void {
    this.listaResultadosLocalidades = {
      lista: [],
      paginaActual: 1,
      totalDatos: 0,
      proximaPagina: true,
    };
  }

  configurarDone(): void {
    this.confDone = {
      mostrarDone: false,
      intervalo: 4000,
      mostrarLoader: false,
    };
  }

  configurarEstadoTransferenciaActiva(): void {
    if (!this.proyecto || !this.proyecto.id || !(this.proyecto.id.length > 0)) {
      return;
    }

    this.db.database
      .ref('transferencias-activas/' + this.proyecto.id)
      .get()
      .then((data) => {
        if (data.exists) {
          const aux = data.val();
          this.indicadorTransferenciaActiva = aux.activa;
        }
      })
      .catch((error) => {
        this.indicadorTransferenciaActiva = false;
      });
  }

  configurarDialogoConfirmarSalida(mostrarDialogo: boolean = false): void {
    this.confDialogoSalida = {
      mostrarDialogo,
      descripcion: 'm2v3texto21',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoSalida.mostrarDialogo = false;
            this.eventoEnBotonPublish(false);
          },
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoSalida.mostrarDialogo = false;
            this.validarAccionDespuesDeCambiosSegunElOrigen(false);
          },
        },
      ],
    };
  }

  // Click en input pais
  async abrirSelectorPaises(): Promise<void> {
    try {
      this.confSelector.cargando.mostrar = true;
      this.confSelector.mostrarModal = true;
      const items: ItemSelector[] = await this.ubicacionNegocio
        .obtenerCatalogoPaisesParaSelector()
        .toPromise();
      if (!items) {
        throw new Error('');
      }

      this.ubicacionNegocio.guardarPaisesDelSelectorEnLocalStorage(items);
      this.confSelector.elegibles = items;
      this.confSelector.cargando.mostrar = false;

      if (this.confSelector.elegibles.length === 0) {
        this.confSelector.cargando.mostrar = false;
        this.confSelector.error.contenido = 'text31';
        this.confSelector.error.mostrarError = true;
      }
    } catch (error) {
      this.confSelector.elegibles = [];
      this.confSelector.cargando.mostrar = false;
      this.confSelector.error.contenido = 'text31';
      this.confSelector.error.mostrarError = true;
    }
  }

  // Click en input localidades
  abrirBuscadorLocalidades(): void {
    if (!this.confBuscador.pais || this.confBuscador.pais.codigo.length === 0) {
      this.toast.abrirToast('text54');
      return;
    }

    this.confBuscador.mostrarModal = true;
  }

  mostrarErrorEnBuscador(
    contenido: string,
    mostrar: boolean,
    tamanoCompleto: boolean = false
  ): void {
    this.confBuscador.resultado.error.contenido = contenido;
    this.confBuscador.resultado.error.tamanoCompleto = tamanoCompleto;
    this.confBuscador.resultado.error.mostrarError = mostrar;
    this.confBuscador.resultado.mostrarCargando = false;
  }

  reiniciarBuscador(): void {
    this.confBuscador.mostrarModal = false;
    this.confBuscador.inputBuscador.valor = '';
    this.confBuscador.resultado.items = [];
    this.confBuscador.resultado.mostrarElegibles = false;
    this.confBuscador.resultado.mostrarCargando = false;
    this.confBuscador.resultado.error.contenido = '';
    this.confBuscador.resultado.error.tamanoCompleto = false;
    this.confBuscador.resultado.error.mostrarError = false;
  }

  async buscarLocalidades(
    pais: string,
    query: string,
    reiniciarLista: boolean = true
  ): Promise<void> {
    try {
      this.mostrarErrorEnBuscador('', false);
      this.confBuscador.resultado.mostrarCargando = reiniciarLista;
      this.confBuscador.resultado.mostrarElegibles = !reiniciarLista;
      this.confBuscador.resultado.puedeCargarMas = false;
      this.confBuscador.resultado.mostrarCargandoPequeno = !reiniciarLista;

      if (reiniciarLista) {
        this.confBuscador.resultado.items = [];
        this.configurarListaPaginacionLocalidades();
      }

      const localidades: PaginacionModel<ItemSelector> =
        await this.ubicacionNegocio
          .buscarLocalidadesPorNombrePaisConPaginacion(
            25,
            this.listaResultadosLocalidades.paginaActual,
            pais,
            query
          )
          .toPromise();

      if (!localidades) {
        throw new Error();
      }

      localidades.lista.forEach((item) => {
        this.listaResultadosLocalidades.lista.push(item);
      });
      this.listaResultadosLocalidades.proximaPagina = localidades.proximaPagina;

      this.confBuscador.resultado.items = this.listaResultadosLocalidades.lista;
      this.confBuscador.resultado.mostrarElegibles = true;
      this.confBuscador.resultado.puedeCargarMas = true;
      this.confBuscador.resultado.mostrarCargando = false;
      this.confBuscador.resultado.mostrarCargandoPequeno = false;
    } catch (error) {
      this.mostrarErrorEnBuscador('text31', true);
    }
  }

  eventoEnSelector(data: InfoAccionSelector): void {
    switch (data.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorPaises();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        // // Buscador
        // this.confBuscador.pais = data.informacion
        // this.confBuscador.seleccionado.codigo = ''
        // this.confBuscador.seleccionado.nombre = ''
        // this.confBuscador.inputPreview.input.valor = ''
        // Selector
        this.confSelector.seleccionado = data.informacion;
        this.confSelector.mostrarModal = false;
        this.confSelector.inputPreview.input.valor = data.informacion.nombre;
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        break;
      case AccionesSelector.BUSCAR_PAIS_POR_QUERY:
        break;
      default:
        break;
    }
  }

  eventoEnBuscador(data: InfoAccionBuscadorLocalidades): void {
    switch (data.accion) {
      case AccionesBuscadorLocalidadModal.ABRIR_BUSCADOR:
        this.confBuscador.resultado.items = [];
        this.configurarListaPaginacionLocalidades();
        this.abrirBuscadorLocalidades();
        break;
      case AccionesBuscadorLocalidadModal.REALIZAR_BUSQUEDA:
        this.buscarLocalidades(data.informacion.pais, data.informacion.query);
        break;
      case AccionesBuscadorLocalidadModal.CARGAR_MAS_RESULTADOS:
        if (!this.listaResultadosLocalidades.proximaPagina) {
          return;
        }

        this.listaResultadosLocalidades.paginaActual += 1;
        this.buscarLocalidades(
          data.informacion.pais,
          data.informacion.query,
          false
        );
        break;
      case AccionesBuscadorLocalidadModal.SELECCIONAR_ITEM:
        if (!data.informacion || !data.informacion.item) {
          break;
        }

        this.confBuscador.seleccionado = data.informacion.item;
        this.confBuscador.inputPreview.input.valor =
          this.confBuscador.seleccionado.nombre;
        this.confBuscador.inputPreview.input.auxiliar =
          this.confBuscador.seleccionado?.auxiliar;
        this.reiniciarBuscador();
        break;
      case AccionesBuscadorLocalidadModal.CERRAR_BUSCADOR:
        this.reiniciarBuscador();
        break;
      default:
        break;
    }
  }

  // Metodo para reintentar, en caso de error al obtener la informacion
  reintentar(): void {
    this.mostrarCapaError = false;
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        break;
      case AccionEntidad.ACTUALIZAR:
        this.inicializarDataParaAccionActualizar();
        break;
      case AccionEntidad.VISITAR:
        this.inicializarDataParaAccionVisitar();
        break;
      default:
        break;
    }
  }

  irAlAlbumGeneral(validarCambios: boolean = true): void {
    const album: AlbumModel = this.albumNegocio.validarAlbumEnProyectoActivo(
      CodigosCatalogoTipoAlbum.GENERAL
    );

    if (
      !album ||
      (this.params.accionEntidad === AccionEntidad.VISITAR &&
        album.estado &&
        album.estado.codigo &&
        album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR)
    ) {
      return;
    }

    if (
      this.proyecto.adjuntos.findIndex(
        (e) => e.tipo.codigo === album.tipo.codigo
      ) < 0
    ) {
      this.proyecto.adjuntos.push(album);
    }

    if (
      validarCambios &&
      this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios =
        OrigenValidacionDeCambios.BOTON_ALBUM_GENERAL;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      this.validarInformacionDelProyectoAntesDeRecargarOCambiarDePaginaHaciaDelante();
    }

    // Se define los parametros del album
    let titulo =
      this.proyecto.tituloCorto.length > 0
        ? this.proyecto.tituloCorto
        : 'm3v10texto7';
    // titulo = titulo.replace(/[^a-zA-Z 0-9.]+/g, ' ')
    titulo = titulo.replace(/[&\/\\#,+()$~%.'":*?<>{}]_|#|-|@|<>/g, '');
    let ruta = '';
    if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
      if (!album._id) {
        ruta =
          RutasLocales.MODULO_ALBUM.toString() +
          '/' +
          RutasAlbumGeneral.CREAR.toString();
        ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
        ruta = ruta.replace(':titulo', titulo);
      } else {
        ruta =
          RutasLocales.MODULO_ALBUM.toString() +
          '/' +
          RutasAlbumGeneral.ACTUALIZAR.toString();
        ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
        ruta = ruta.replace(':titulo', titulo);
      }
    } else {
      ruta =
        RutasLocales.MODULO_ALBUM.toString() +
        '/' +
        RutasAlbumGeneral.VISITAR.toString();
      ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
      ruta = ruta.replace(':titulo', titulo);
    }

    this.router.navigateByUrl(ruta);
  }

  irAlAlbumAudios(validarCambios: boolean = true): void {
    // Se valida el album
    const album: AlbumModel = this.albumNegocio.validarAlbumEnProyectoActivo(
      CodigosCatalogoTipoAlbum.AUDIOS
    );

    if (
      !album ||
      (this.params.accionEntidad === AccionEntidad.VISITAR &&
        album.estado &&
        album.estado.codigo &&
        album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR)
    ) {
      return;
    }

    if (
      this.proyecto.adjuntos.findIndex(
        (e) => e.tipo.codigo === album.tipo.codigo
      ) < 0
    ) {
      this.proyecto.adjuntos.push(album);
    }

    if (
      validarCambios &&
      this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios =
        OrigenValidacionDeCambios.BOTON_ALBUM_AUDIOS;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      this.validarInformacionDelProyectoAntesDeRecargarOCambiarDePaginaHaciaDelante();
    }

    let titulo =
      this.proyecto.tituloCorto.length > 0
        ? this.proyecto.tituloCorto
        : 'm3v10texto7';
    titulo = titulo.replace(/[^a-zA-Z 0-9.]+/g, ' ');

    let ruta = '';
    if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
      if (!album._id) {
        ruta =
          RutasLocales.MODULO_ALBUM.toString() +
          '/' +
          RutasAlbumAudios.CREAR.toString();
        ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
        ruta = ruta.replace(':titulo', titulo);
      } else {
        ruta =
          RutasLocales.MODULO_ALBUM.toString() +
          '/' +
          RutasAlbumAudios.ACTUALIZAR.toString();
        ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
        ruta = ruta.replace(':titulo', titulo);
      }
    } else {
      ruta =
        RutasLocales.MODULO_ALBUM.toString() +
        '/' +
        RutasAlbumAudios.VISITAR.toString();
      ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
      ruta = ruta.replace(':titulo', titulo);
    }

    this.router.navigateByUrl(ruta);
  }

  irAlAlbumDeLinks(validarCambios: boolean = true): void {
    const album: AlbumModel = this.albumNegocio.validarAlbumEnProyectoActivo(
      CodigosCatalogoTipoAlbum.LINK
    );

    if (
      !album ||
      (this.params.accionEntidad === AccionEntidad.VISITAR &&
        album.estado &&
        album.estado.codigo &&
        album.estado.codigo === CodigosCatalogoEstadoAlbum.SIN_CREAR)
    ) {
      return;
    }

    if (
      this.proyecto.adjuntos.findIndex(
        (e) => e.tipo.codigo === album.tipo.codigo
      ) < 0
    ) {
      this.proyecto.adjuntos.push(album);
    }

    if (
      validarCambios &&
      this.params.accionEntidad === AccionEntidad.ACTUALIZAR &&
      this.validarSiExistenCambios()
    ) {
      this.origenValidacionCambios =
        OrigenValidacionDeCambios.BOTON_ALBUM_LINKS;
      this.configurarDialogoConfirmarSalida(true);
      return;
    }

    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      this.validarInformacionDelProyectoAntesDeRecargarOCambiarDePaginaHaciaDelante();
    }

    // Se define los parametros del album
    let titulo =
      this.proyecto.tituloCorto.length > 0
        ? this.proyecto.tituloCorto
        : 'm3v10texto7';
    let ruta = '';
    titulo = titulo.replace(/[^a-zA-Z 0-9.]+/g, ' ');
    if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
      if (!album._id) {
        ruta =
          RutasLocales.MODULO_ALBUM.toString() +
          '/' +
          RutasAlbumLinks.CREAR.toString();
        ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
        ruta = ruta.replace(':titulo', titulo);
      } else {
        ruta =
          RutasLocales.MODULO_ALBUM.toString() +
          '/' +
          RutasAlbumLinks.ACTUALIZAR.toString();
        ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
        ruta = ruta.replace(':titulo', titulo);
      }
    } else {
      ruta =
        RutasLocales.MODULO_ALBUM.toString() +
        '/' +
        RutasAlbumLinks.VISITAR.toString();
      ruta = ruta.replace(':entidad', CodigosCatalogoEntidad.PROYECTO);
      ruta = ruta.replace(':titulo', titulo);
    }

    this.router.navigateByUrl(ruta);
  }

  // Inicializar data catalogo moneda
  async inicializarDataCatalogoMoneda(): Promise<void> {
    this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(
      (elegibles) => {
        this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
        this.confMonedaPicker.selectorTipoMoneda.elegibles = elegibles;
        this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
        this.confMonedaPicker.selectorTipoMoneda.error.mostrar = false;
      },
      (error) => {
        this.confMonedaPicker.selectorTipoMoneda.elegibles = [];
        this.confMonedaPicker.selectorTipoMoneda.error.llaveTexto = 'text31';
        this.confMonedaPicker.selectorTipoMoneda.error.mostrar = true;
        this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
      }
    );
  }

  async inicializarDataCatalogoMonedaConvertir(): Promise<void> {
    this.confMonedaPickerConvertir.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(
      (elegibles) => {
        this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
        this.confMonedaPickerConvertir.selectorTipoMoneda.elegibles = elegibles;
        this.confMonedaPickerConvertir.selectorTipoMoneda.mostrarLoader = false;
        this.confMonedaPickerConvertir.selectorTipoMoneda.error.mostrar = false;
      },
      (error) => {
        this.confMonedaPickerConvertir.selectorTipoMoneda.elegibles = [];
        this.confMonedaPickerConvertir.selectorTipoMoneda.error.llaveTexto =
          'text31';
        this.confMonedaPickerConvertir.selectorTipoMoneda.error.mostrar = true;
        this.confMonedaPickerConvertir.selectorTipoMoneda.mostrarLoader = false;
      }
    );
  }

  // Abrir Selector
  abrirSelectorMoneda(): void {
    this.confMonedaPicker.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMoneda();
  }

  // Abrir Selector
  abrirSelectorMonedaConvertir(): void {
    this.confMonedaPickerConvertir.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMonedaConvertir();
  }

  seleccionarTipoMoneda(item: ItemSelector): void {
    // Ocultar selector
    this.confMonedaPicker.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPicker.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPicker.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado =
      item.auxiliar || '';
  }

  async seleccionarTipoMonedaConvertir(item: ItemSelector): Promise<void> {
    // Ocultar selector
    this.confMonedaPickerConvertir.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPickerConvertir.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPickerConvertir.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPickerConvertir.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado =
      item.auxiliar || '';
    // tslint:disable-next-line: no-string-literal
    const cuotaMinima = Math['round10'](
      await this.tipoMonedaNegocio
        .convertirMontoEntreMonedasProyectos(
          this.proyecto.valorEstimado,
          this.proyecto.moneda.codNombre,
          item.auxiliar
        )
        .toPromise(),
      -2
    );

    this.confMonedaPickerConvertir.inputCantidadMoneda.valor.valorNeto =
      cuotaMinima.toString();
  }

  // Metodos del selector de moneda
  eventoEnSelectorDeTipoMoneda(accion: InfoAccionSelector): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMoneda();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMoneda(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMoneda();
        break;
      default:
        break;
    }
  }

  eventoEnSelectorDeTipoMonedaConvertir(accion: InfoAccionSelector): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMonedaConvertir();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMonedaConvertir(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMonedaConvertir();
        break;
      default:
        break;
    }
  }

  async irAlHistoricoDelProyecto(): Promise<void> {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        const texto = await this.translateService.get('text30').toPromise();
        this.toast.abrirToast(texto);
        break;
      case AccionEntidad.ACTUALIZAR:
        this.router.navigateByUrl(
          RutasLocales.MODULO_PROYECTOS.toString() +
            '/' +
            RutasProyectos.HISTORICO.toString().replace(
              ':id',
              this.proyecto.id.toString()
            )
        );
        break;
      case AccionEntidad.VISITAR:
        this.router.navigateByUrl(
          RutasLocales.MODULO_PROYECTOS.toString() +
            '/' +
            RutasProyectos.HISTORICO.toString().replace(
              ':id',
              this.proyecto.id.toString()
            )
        );
        break;
      default:
        break;
    }
  }

  async validarAccionCompartirProyecto(): Promise<void> {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        const texto = await this.translateService.get('text30').toPromise();
        this.toast.abrirToast(texto);
        break;
      case AccionEntidad.ACTUALIZAR:
        this.obtenerContactos(true);
        break;
      case AccionEntidad.VISITAR:
        this.obtenerContactos(true);
        break;
      default:
        break;
    }
  }

  abrirSelectorArchivo(id: string): void {
    const elemento = document.getElementById(id) as HTMLElement;
    if (elemento) {
      elemento.click();
    }
  }

  async agregarMediaAlProyecto(files: FileList): Promise<void> {
    try {
      this.confDialogoFullProyecto.mostrarDialogo = false;
      this.confBotonFullProject.enProgreso = true;

      if (files.length <= 0) {
        throw new Error('');
      }

      const file = files[0];
      if (file && file.type !== 'application/pdf') {
        this.confBotonFullProject.enProgreso = false;
        this.toast.abrirToast('m4v5texto24');
        return;
      }

      const dataApiArchivo: SubirArchivoData = {
        archivo: files[0],
        formato: 'application/pdf',
        catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
        descripcion: '',
        relacionAspecto: '1:1',
      };

      if (!this.proyecto.medias) {
        this.proyecto.medias = [];
      }

      const media: MediaModel = await this.mediaNegocio
        .subirMedia(dataApiArchivo)
        .toPromise();

      if (!media) {
        throw new Error('');
      }

      this.proyecto.medias[0] = media;
      this.confBotonFullProject.enProgreso = false;

      const elemento = document.getElementById(
        this.inputProyectoCompleto
      ) as HTMLInputElement;
      if (elemento) {
        elemento.value = '';
      }
    } catch (error) {
      this.confBotonFullProject.enProgreso = false;
      this.toast.abrirToast('text37');
    }
  }

  async eventoEnBotonPublish(validarCambios: boolean = true): Promise<void> {

    if (validarCambios && !this.validarSiExistenCambios()) {
      if (this.params.accionEntidad === AccionEntidad.ACTUALIZAR) {
        this.confDone.mostrarDone = true;
        setTimeout(() => {
          this.location.back();
        }, 1500);

        return;
      }
      return;
    }

    this.validarInformacionDelProyectoAntesDeRecargarOCambiarDePaginaHaciaDelante();
    const estado: boolean = this.proyectoService.validarCamposEnProyecto(
      this.proyecto
    );

    if (!estado) {
      this.toast.abrirToast('text4');
      // if (this.params.accionEntidad === AccionEntidad.CREAR) {
      // 	this.proyectoNegocio.crearObjetoVacioDeProyecto(
      // 		this.params.codigoTipoProyecto,
      // 		this.perfilSeleccionado
      // 	)
      // }
      return;
    }

    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.publicarProyecto();
        break;
      case AccionEntidad.ACTUALIZAR:
        this.actualizarProyecto();
        break;
      case AccionEntidad.VISITAR:
        break;
      default:
        break;
    }
  }

  eventoEnBotonTransferir(): void {
    if (this.params.accionEntidad !== AccionEntidad.ACTUALIZAR) {
      return;
    }

    if (this.indicadorTransferenciaActiva) {
      return;
    }

    this.paraCompartir = false;
    this.obtenerContactos(true);
  }

  eventoEnBotonEliminar(): void {
    this.confDialogoEliminarProyecto.mostrarDialogo = true;
  }

  async eliminarProyecto(): Promise<void> {
    if (this.params.accionEntidad !== AccionEntidad.ACTUALIZAR) {
      return;
    }

    try {
      const estatus: string = await this.proyectoNegocio
        .eliminarProyecto(this.proyecto.id, this.proyecto.perfil._id)
        .toPromise();

      this.confDone.mostrarDone = true;
      setTimeout(() => {
        this.navegarAlBack();
      }, 1500);
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async publicarProyecto(): Promise<void> {
    try {
      this.confBotonPublish.enProgreso = true;
      const proyecto = await this.proyectoNegocio
        .crearProyecto(this.proyecto)
        .toPromise();
      this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
      this.toast.cerrarToast();
      this.validarAccionDespuesDeCambiosSegunElOrigen();
      this.confBotonPublish.enProgreso = false;
    } catch (error) {
      this.toast.abrirToast('text36');
      this.proyectoNegocio.crearObjetoVacioDeProyecto(
        this.proyecto.tipo.codigo as CodigosCatalogoTipoProyecto,
        this.perfilSeleccionado
      );
      this.confBotonPublish.enProgreso = false;
    }
  }

  async actualizarProyecto(): Promise<void> {
    // return
    try {
      this.confBotonPublish.enProgreso = true;
      const proyecto = await this.proyectoNegocio
        .actualizarProyecto({ ...this.proyecto })
        .toPromise();

      this.toast.cerrarToast();
      this.validarAccionDespuesDeCambiosSegunElOrigen();
      this.confBotonPublish.enProgreso = false;
    } catch (error) {
      this.confBotonPublish.enProgreso = false;
      this.toast.abrirToast('text36');
    }
  }

  validarAccionDespuesDeCambiosSegunElOrigen(
    mostrarDone: boolean = true
  ): void {
    if (mostrarDone) {
      this.confDone.mostrarDone = mostrarDone;
      setTimeout(() => {
        this.ejecutarAccionDespuesDelCambio();
      }, 1500);
      return;
    }

    this.ejecutarAccionDespuesDelCambio();
  }

  ejecutarAccionDespuesDelCambio(): void {
    switch (this.origenValidacionCambios) {
      case OrigenValidacionDeCambios.BOTON_BACK:
        this.navegarAlBack();
        break;
      case OrigenValidacionDeCambios.BOTON_HOME:
        this.navegarAlHome();
        break;
      case OrigenValidacionDeCambios.BOTON_ALBUM_GENERAL:
        this.irAlAlbumGeneral(false);
        break;
      case OrigenValidacionDeCambios.BOTON_ALBUM_AUDIOS:
        this.irAlAlbumAudios(false);
        break;
      case OrigenValidacionDeCambios.BOTON_ALBUM_LINKS:
        this.irAlAlbumDeLinks(false);
        break;
      default:
        this.navegarAlBack();
        break;
    }
  }

  async enviarComentario(
    dataApiArchivo: SubirArchivoData,
    texto: boolean = true
  ): Promise<void> {
    try {
      const puedeComentar = this.validarSiElUsuarioPuedeComentar();
      if (puedeComentar) {
        const comentario: ComentarioModel = {
          importante: false,
          proyecto: {
            id: this.proyecto.id,
          },
          coautor: {
            coautor: {
              _id: this.perfilSeleccionado._id,
            },
          },
        };

        if (
          this.idPerfilCoautorParaResponder &&
          this.idPerfilCoautorParaResponder.length > 0
        ) {
          comentario.idPerfilRespuesta = {
            _id: this.idPerfilCoautorParaResponder,
          };
        }

        if (!texto) {
          const media: MediaModel = await this.mediaNegocio
            .subirMedia(dataApiArchivo)
            .toPromise();
          comentario.adjuntos = [];
          comentario.adjuntos.push({
            id: media.id,
          });
        }

        if (texto) {
          comentario.traducciones = [
            {
              texto: dataApiArchivo.descripcion,
            },
          ];
        }

        const comentarioCreado: ComentarioModel = await this.comentarioNegocio
          .crearComentario(comentario)
          .toPromise();

        if (!texto) {
          this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader =
            false;
          this.confBarraInferior.iconoAudio.capa.mostrar = false;
        }

        this.idPerfilCoautorParaResponder = '';
      }
    } catch (error) {
      if (!texto) {
        this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader = false;
        this.confBarraInferior.iconoAudio.capa.mostrar = false;
      }
      this.toast.abrirToast('text37');
    }
  }

  validarRolDelUsuarioEnElProyecto(
    rolparaValidar: CodigosCatalogosTipoRol
  ): boolean {
    let estado = false;
    if (this.proyecto && this.proyecto.participantes) {
      this.proyecto.participantes.forEach((item) => {
        if (
          item &&
          item.coautor &&
          item.coautor._id &&
          item.coautor._id === this.perfilSeleccionado._id &&
          item.roles
        ) {
          item.roles.forEach((rol) => {
            if (rol.rol.codigo === rolparaValidar) {
              estado = true;
            }
          });
        }
      });
    }
    return estado;
  }

  validarSiElUsuarioPuedeComentar(): boolean {
    switch (this.proyecto.estado.codigo as CodigosCatatalogosEstadoProyecto) {
      case CodigosCatatalogosEstadoProyecto.PROYECTO_ACTIVO:
        return true;
      case CodigosCatatalogosEstadoProyecto.PROYECTO_EN_ESTRATEGIA:
        return (
          this.validarRolDelUsuarioEnElProyecto(
            CodigosCatalogosTipoRol.ESTRATEGA
          ) ||
          this.validarRolDelUsuarioEnElProyecto(
            CodigosCatalogosTipoRol.PROPIETARIO
          )
        );
      case CodigosCatatalogosEstadoProyecto.PROYECTO_FORO:
        return (
          this.validarRolDelUsuarioEnElProyecto(
            CodigosCatalogosTipoRol.COAUTOR
          ) ||
          this.validarRolDelUsuarioEnElProyecto(
            CodigosCatalogosTipoRol.PROPIETARIO
          )
        );
      case CodigosCatatalogosEstadoProyecto.PROYECTO_FORO_SEGUNDA_ESTAPA:
        return (
          this.validarRolDelUsuarioEnElProyecto(
            CodigosCatalogosTipoRol.COAUTOR
          ) ||
          this.validarRolDelUsuarioEnElProyecto(
            CodigosCatalogosTipoRol.PROPIETARIO
          )
        );
      default:
        return false;
    }
  }

  determinarTipoDelComentario(): CodigosCatatalogosTipoComentario {
    if (this.proyecto.estado) {
      switch (this.proyecto.estado.codigo as CodigosCatatalogosEstadoProyecto) {
        case CodigosCatatalogosEstadoProyecto.PROYECTO_PRE_ESTRATEGIA:
          return CodigosCatatalogosTipoComentario.TIPO_ESTRATEGIA;
        case CodigosCatatalogosEstadoProyecto.PROYECTO_EN_ESTRATEGIA:
          return CodigosCatatalogosTipoComentario.TIPO_ESTRATEGIA;
        case CodigosCatatalogosEstadoProyecto.PROYECTO_FORO:
          return CodigosCatatalogosTipoComentario.TIPO_FORO;
        case CodigosCatatalogosEstadoProyecto.PROYECTO_FORO_SEGUNDA_ESTAPA:
          return CodigosCatatalogosTipoComentario.TIPO_FORO;
        default:
          return CodigosCatatalogosTipoComentario.TIPO_NORMAL;
      }
    }
  }

  async eliminarComentario(): Promise<void> {
    if (!this.comentarioAEliminar) {
      return;
    }

    try {
      this.confDialogoEliminarComentario.mostrarDialogo = false;

      let estado: string;
      let seFueAHistorico = false;

      if (this.proyecto.perfil._id === this.perfilSeleccionado._id) {
        estado = await this.comentarioNegocio
          .eliminarComentario(
            this.proyecto.id,
            this.comentarioAEliminar.coautor.id,
            this.comentarioAEliminar.id
          )
          .toPromise();
        seFueAHistorico = true;
      }

      if (
        !seFueAHistorico &&
        this.perfilSeleccionado._id ===
          this.comentarioAEliminar.coautor.coautor._id
      ) {
        estado = await this.comentarioNegocio
          .eliminarMiComentario(
            this.comentarioAEliminar.id,
            this.comentarioAEliminar.coautor.id
          )
          .toPromise();
      }

      if (!estado) {
        throw new Error('text37');
      }

      const querys = {};
      const path =
        this.params.id + '/' + this.comentarioAEliminar.id + '/estado';
      querys[path] = {
        codigo: seFueAHistorico
          ? CodigosCatalogosEstadoComentario.HISTORICO
          : CodigosCatalogosEstadoComentario.ELIMINADO,
      };

      this.db.database
        .ref('comentarios')
        .update(querys)
        .then(
          () => {
            const index = this.listaComentariosFirebase.findIndex(
              (e) => e.id === this.comentarioAEliminar.id
            );
            this.eliminarComentariosDeListas(
              this.comentarioAEliminar.id,
              index
            );
            this.comentarioAEliminar = undefined;
          },
          (error) => {
            this.toast.abrirToast('text37');
          }
        );
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async scroolEnCapaFormulario(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    const elemento: HTMLElement = document.getElementById(
      this.idCapaFormulario
    ) as HTMLElement;
    if (
      !elemento ||
      !this.puedeCargarMas ||
      this.indicadorTotalComentarios.total ===
        this.listaComentariosFirebase.length
    ) {
      // this.puedeCargarMas = true
      return;
    }

    if (
      elemento.offsetHeight + elemento.scrollTop >=
      elemento.scrollHeight - 15
    ) {
      this.puedeCargarMas = false;
      this.comenFireService.paginacionComentarios += 30;
      this.obtenerListaDeComentariosEnPaginacion();
      this.obtenerComentariosPaginacion();
    }
  }

  validarAccionDobleTapEnPortadaExpandida(): void {
    if (this.proyecto && this.proyecto.adjuntos) {
      const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        this.proyecto.adjuntos
      );

      if (!album) {
        return;
      }

      this.irAlAlbumGeneral();
      // if (album.media && album.media.length === 1) {
      // 	if (!this.confImagenPantallaCompleta) {
      // 		this.toast.abrirToast('text37')
      // 		return
      // 	}

      // 	this.confImagenPantallaCompleta.urlMedia = this.confPortadaExpandida.urlMedia
      // 	this.confImagenPantallaCompleta.mostrarLoader = true
      // 	this.confImagenPantallaCompleta.mostrar = true
      // } else {
      // 	this.irAlAlbumGeneral()
      // }
    }
  }

  async apoyarProyecto(): Promise<void> {
    try {
      const votoModel: VotoProyectoModel = {
        perfil: {
          _id: this.perfilSeleccionado._id,
        },
        proyecto: {
          id: this.proyecto.id,
        },
        descripcion: 'Voto',
      };
      const estatus: string = await this.proyectoNegocio
        .apoyarProyecto(votoModel)
        .toPromise();
      this.proyecto.voto = true;
      this.configurarVotarEntidad();
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async eventoBotonFullProject(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.VISITAR) {
      this.eventoRevisarPDFFullProject();
      return;
    }

    if (
      this.proyecto &&
      this.proyecto.medias &&
      this.proyecto.medias.length === 0
    ) {
      this.abrirSelectorArchivo(this.inputProyectoCompleto);
      return;
    }

    let botones: Array<ItemDialogoHorizontal> =
      await this.configurarBotonReviewPDF([]);
    botones = await this.configurarBotonCambiarPDF(botones);
    botones = await this.configurarBotonEliminarPDF(botones);

    this.configurarDialogoFullProyecto(true, botones);
  }

  async configurarBotonReviewPDF(
    botones: Array<ItemDialogoHorizontal>
  ): Promise<ItemDialogoHorizontal[]> {
    const texto = await this.translateService.get('m4v6texto21').toPromise();
    const texto2 = await this.translateService.get('m4v6texto22').toPromise();

    const descripcion = texto + '\n' + texto2;
    if (
      this.proyecto &&
      this.proyecto.medias &&
      this.proyecto.medias.length > 0
    ) {
      botones.push({
        descripcion,
        accion: {
          tipoBoton: TipoBoton.ICON_COLOR,
          colorIcono: ColorIconoBoton.AZUL,
          enProgreso: false,
          ejecutar: () => this.eventoRevisarPDFFullProject(),
        },
        estilosTexto: {
          color: ColorDelTexto.TEXTOAZULBASE,
          enMayusculas: true,
          estiloTexto: EstilosDelTexto.BOLD,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1,
        },
      });
    }

    return botones;
  }

  async configurarBotonCambiarPDF(
    botones: Array<ItemDialogoHorizontal>
  ): Promise<ItemDialogoHorizontal[]> {
    const texto = await this.translateService.get('m4v6texto23').toPromise();
    const texto2 = await this.translateService.get('m4v6texto24').toPromise();

    const descripcion = texto + '\n' + texto2;
    botones.push({
      descripcion,
      accion: {
        tipoBoton: TipoBoton.ICON_COLOR,
        colorIcono: ColorIconoBoton.AMARILLO,
        enProgreso: false,
        ejecutar: () => {
          this.abrirSelectorArchivo(this.inputProyectoCompleto);
        },
      },
      estilosTexto: {
        color: ColorDelTexto.TEXTOAZULBASE,
        enMayusculas: true,
        estiloTexto: EstilosDelTexto.BOLD,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1,
      },
    });

    return botones;
  }

  async configurarBotonEliminarPDF(
    botones: Array<ItemDialogoHorizontal>
  ): Promise<ItemDialogoHorizontal[]> {
    const texto = await this.translateService.get('m4v6texto24.1').toPromise();
    const texto2 = await this.translateService.get('m4v6texto24.2').toPromise();

    const descripcion = texto + '\n' + texto2;
    botones.push({
      descripcion,
      accion: {
        tipoBoton: TipoBoton.ICON_COLOR,
        colorIcono: ColorIconoBoton.ROJO,
        enProgreso: false,
        ejecutar: () => {
          this.confDialogoEliminarPDF.mostrarDialogo = true;
        },
      },
      estilosTexto: {
        color: ColorDelTexto.TEXTOAZULBASE,
        enMayusculas: true,
        estiloTexto: EstilosDelTexto.BOLD,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1,
      },
    });

    return botones;
  }

  eventoRevisarPDFFullProject(): void {
    if (
      this.proyecto &&
      this.proyecto.medias &&
      this.proyecto.medias.length > 0
    ) {
      const media: MediaModel = this.proyecto.medias[0];

      if (media && media.principal && media.principal.url) {
        window.open(media.principal.url);
      }
    }
  }

  determinarSiHayAlbumSegunTipo(codigo: CodigosCatalogoTipoAlbum): boolean {
    if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
      return true;
    }

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      codigo,
      this.proyecto.adjuntos
    );

    if (album) {
      return true;
    }

    return false;
  }

  cambiarEstadoAppBarParaListaDeContactos(paraBuscar: boolean = true): void {
    if (paraBuscar) {
      const buscador: BarraBusqueda = {
        mostrar: true,
        configuracion: {
          disable: false,
          modoBusqueda: ModoBusqueda.BUSQUEDA_LOCAL,
          buscar: (reiniciar: boolean) => {
            if (reiniciar) {
              this.listaContactosBuscador = [];
              this.confListaContactoCompartido.listaContactos.lista =
                this.listaContactosOriginal;
              return;
            }

            const buscar: string =
              this.confAppbar.searchBarAppBar.buscador.configuracion
                .valorBusqueda;
            if (buscar.length > 0) {
              this.buscarContactos(buscar);
            }
          },
          entidad: CodigosCatalogoEntidad.CONTACTO,
          cerrarBuscador: () => {
            this.cerrarBuscador();
          },
          placeholder: 'm4v2texto1',
          valorBusqueda: '',
        },
      };
      this.configurarAppBar(this.paraCompartir ? 1 : 2, buscador);
    } else {
      this.configurarAppBar(-1);
    }
  }

  async obtenerContactos(primeraVez?: boolean): Promise<void> {
    if (primeraVez) {
      this.configurarListaContactoCompartido();
    }

    this.cambiarEstadoAppBarParaListaDeContactos();
    this.confListaContactoCompartido.mostrar = true;

    try {
      if (!this.listaContactos.proximaPagina) {
        return;
      }

      this.confListaContactoCompartido.listaContactos.cargando = true;
      const contactos: PaginacionModel<ParticipanteAsociacionModel> =
        await this.participanteAsociacionNegocio
          .obtenerParticipanteAsoTipo(
            this.perfilSeleccionado._id,
            15,
            this.listaContactos.paginaActual,
            CodigoEstadoParticipanteAsociacion.CONTACTO,
            FiltroGeneral.ALFA
          )
          .toPromise();

      this.listaContactos.proximaPagina = contactos.proximaPagina;
      contactos.lista.forEach((item) => {
        this.listaContactos.lista.push(item);
        this.confListaContactoCompartido.listaContactos.lista.push(
          this.metodosParaFotos.configurarItemListaContacto(
            item.contactoDe,
            item.asociacion
          )
        );
      });

      this.confListaContactoCompartido.listaContactos.cargando = false;
    } catch (error) {
      this.confListaContactoCompartido.botonAccion.mostrarDialogo = false;
      this.confListaContactoCompartido.listaContactos.cargando = false;
      this.confListaContactoCompartido.listaContactos.error = 'text37';
    }
  }

  enviarProyectoComoMensaje(tipoMensaje: CatalogoTipoMensaje): void {
    if (this.listaContactosSeleccionados.length <= 0) {
      this.toast.abrirToast('text49');
      return;
    }

    const querys = {};
    this.listaContactosSeleccionados.forEach((idAsociacion) => {
      const idMensaje = this.db.database
        .ref('mensajes/' + idAsociacion)
        .push().key;
      const mensajeModel =
        this.chatMetodosCompartidosService.crearObjetoMensajeParaProyecto(
          idMensaje,
          idAsociacion,
          tipoMensaje,
          {
            id: '',
            perfil: { _id: this.perfilSeleccionado._id },
          },
          this.proyecto
        );

      const path = idAsociacion + '/' + idMensaje;
      querys[path] = mensajeModel;
    });

    const indicador = {};
    indicador[this.proyecto.id] = {
      activa: tipoMensaje === CatalogoTipoMensaje.TRANSFERIR_PROYECTO,
    };

    this.confListaContactoCompartido.mostrar = false;
    this.confListaContactoCompartido.botonAccion.mostrarDialogo = false;

    this.db.database
      .ref('mensajes')
      .update(querys)
      .then((a) => {
        return this.db.database.ref('transferencias-activas').update(indicador);
      })
      .then((b) => {
        this.reiniciarListaContactos();
        this.toast.cerrarToast();
        this.confDone.mostrarDone = true;
        this.indicadorTransferenciaActiva = true;
        setTimeout(() => {
          this.confDone.mostrarDone = false;
        }, 1500);
      })
      .catch((error) => {
        this.toast.abrirToast('text37');
      });
  }

  reiniciarListaContactos(): void {
    this.confListaContactoCompartido.mostrar = false;
    this.confListaContactoCompartido.listaContactos.lista = [];
    this.listaContactosSeleccionados = [];
    this.inicializarDataListaContactos();
    this.configurarAppBar(-1);
  }

  buscarContactos(buscar: string): void {
    if (!(this.confListaContactoCompartido.listaContactos.lista.length > 0)) {
      return;
    }

    this.listaContactosBuscador = [];
    if (this.listaContactosOriginal.length > 0) {
      this.confListaContactoCompartido.listaContactos.lista =
        this.listaContactosOriginal;
    }

    const contactos: Array<ConfiguracionItemListaContactosCompartido> =
      this.confListaContactoCompartido.listaContactos.lista;

    contactos.forEach((item) => {
      if (
        item &&
        item.contacto &&
        item.contacto.nombreContacto &&
        item.contacto.nombreContacto.toLowerCase().includes(buscar)
      ) {
        this.listaContactosBuscador.push(item);
      }
    });

    this.listaContactosOriginal =
      this.confListaContactoCompartido.listaContactos.lista;
    this.confListaContactoCompartido.listaContactos.lista =
      this.listaContactosBuscador;
  }

  cerrarBuscador(): void {
    this.listaContactosBuscador = [];

    if (this.listaContactosOriginal.length > 0) {
      this.confListaContactoCompartido.listaContactos.lista =
        this.listaContactosOriginal;
    }

    if (this.confListaContactoCompartido.listaContactos.lista.length === 0) {
      this.inicializarDataListaContactos();
      this.obtenerContactos(true);
    }
  }

  validarSiExistenCambios(): boolean {
    const proyectoAux: ProyectoModel =
      this.proyectoNegocio.asignarValoresDeLosCamposAlProyectoParaValidarCambios(
        { ...this.proyecto },
        this.confSelector.seleccionado,
        this.confMonedaPicker.inputCantidadMoneda.valor,
        this.confMonedaPicker.selectorTipoMoneda.seleccionado,
        this.proyectoForm
      );
    const resultado = this.proyectoNegocio.validarSiExistenCambiosEnElProyecto(
      proyectoAux,
      this.params.accionEntidad
    );

    return resultado;
  }

  navegarAlHome(): void {
    const perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
    this.metodosSessionStorageService.eliminarSessionStorage();
    this.location.replaceState('/');
    this.router.navigateByUrl(
      perfilSeleccionado
        ? RutasLocales.MENU_PRINCIPAL
        : RutasLocales.MENU_SELECCION_PERFILES
    );
  }

  navegarAlBack(): void {
    this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
    this.location.back();
  }

  intercambioEnInglesActivo(): void {
    this.inglesSiempreActivoEnComentarios =
      !this.inglesSiempreActivoEnComentarios;

    this.listaConfiguracionComentarios.forEach((comentario) => {
      comentario.inglesActivo = this.inglesSiempreActivoEnComentarios;
    });
  }

  obtenerImgIdiomaComentario(): {} {
    const clases = {};
    if (!this.inglesSiempreActivoEnComentarios) {
      clases['boton-circular'] = true;
      return clases;
    }
    if (this.inglesSiempreActivoEnComentarios) {
      clases[ICONO_IDIOMA[this.idiomaSeleccionado.codNombre]] = true;
      return clases;
    }
  }

  configurarEventosComentario(): void {
    this.eventoTapComentario = (comentario: ComentarioModel) => {
      this.comentarioAEliminar = comentario;
      this.confDialogoEliminarComentario.mostrarDialogo = true;
    };

    this.eventoTapPerfilComentario = (
      idPerfilCoautor: string,
      idInterno: string
    ) => {
      if (idPerfilCoautor === this.perfilSeleccionado._id) {
        return;
      }

      const index = this.listaConfiguracionComentarios.findIndex(
        (e) => e.coautor._id === idPerfilCoautor && e.idInterno === idInterno
      );

      this.listaConfiguracionComentarios.forEach((item) => {
        item.couatorSeleccionado = false;
      });

      if (index < 0 || idPerfilCoautor.length === 0 || idInterno.length === 0) {
        return;
      }

      if (
        idPerfilCoautor === this.idPerfilCoautorParaResponder &&
        idInterno === this.idInternoParaResponder
      ) {
        this.listaConfiguracionComentarios[index].couatorSeleccionado = false;
        this.idPerfilCoautorParaResponder = '';
        this.idInternoParaResponder = '';
        return;
      }

      if (
        idInterno !== this.idInternoParaResponder &&
        (idPerfilCoautor !== this.idPerfilCoautorParaResponder ||
          idPerfilCoautor === this.idPerfilCoautorParaResponder)
      ) {
        this.listaConfiguracionComentarios[index].couatorSeleccionado = true;
        this.idPerfilCoautorParaResponder = idPerfilCoautor;
        this.idInternoParaResponder = idInterno;
        return;
      }
    };
  }

  async obtenerTotalDeComentarios(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.totalComentarios.ejecutar$.next({
      idProyecto: this.params.id,
      estado: CodigosCatalogosEstadoComentario.ACTIVA,
    });
  }

  async configurarEscuchaTotalDeComentarios(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.totalComentarios.subscripcion$ =
      this.comenFireService.totalComentarios.respuesta$.subscribe(
        (data) => {
          this.comenFireService.desconectar(OrigenConexion.TOTAL_COMENTARIOS);

          if (data) {
            this.configurarIndicadorTotalDeComentarios(
              data.length,
              data.map((c) => c.key)
            );
          }
        },
        (error) => {
          this.configurarIndicadorTotalDeComentarios();
        }
      );
  }

  async obtenerComentarios(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.puedeCargarMas = false;
    this.comenFireService.comentarios.ejecutar$.next(this.params.id);
  }

  async configurarEscuchaListaDeComentarios(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.comentarios.subscripcion$ =
      this.comenFireService.comentarios.respuesta$.subscribe(
        (data) => {
          if (!data || data.length === 0) {
            this.puedeCargarMas = true;
            return;
          }

          this.configurarListaDeComentarios(data);
          this.puedeCargarMas = true;
        },
        (error) => {
          this.listaDeFechas = [];
          this.listaComentariosFirebase = [];
          this.listaConfiguracionComentarios = [];
        }
      );
  }

  async configurarListaDeComentarios(
    actions: AngularFireAction<DataSnapshot>[]
  ): Promise<void> {
    try {
      const data = actions.reverse();
      data.forEach((item) => {
        const comentario: ComentarioFirebaseModel =
          this.comentarioFirebaseEntityMapperService.transform(
            item.payload.val() as ComentarioFirebaseEntity
          );

        const index = this.listaComentariosFirebase.findIndex(
          (e) => e.id === comentario.id
        );
        if (
          comentario.estado.codigo === CodigosCatalogosEstadoComentario.ACTIVA
        ) {
          if (index < 0) {
            this.listaComentariosFirebase.push(comentario);
          }

          const indexDos = this.indicadorTotalComentarios.comentarios.findIndex(
            (e) => e === comentario.id
          );
          if (indexDos < 0) {
            this.indicadorTotalComentarios.total += 1;
            this.indicadorTotalComentarios.comentarios.unshift(comentario.id);
          }
        } else {
          this.eliminarComentariosDeListas(comentario.id, index);
        }
      });

      this.listaComentariosFirebase =
        this.comenFireService.ordenarComentariosDeFormaDescendente(
          this.listaComentariosFirebase
        );

      this.listaDeFechas =
        this.comenFireService.configurarListaDeFechasComentarios(
          this.listaDeFechas,
          this.listaComentariosFirebase
        );

      this.listaConfiguracionComentarios =
        this.comenFireService.crearConfigurarDeLosComentarios(
          this.listaDeFechas,
          this.listaComentariosFirebase,
          this.listaConfiguracionComentarios,
          this.proyecto.perfil._id,
          this.perfilSeleccionado._id,
          this.inglesSiempreActivoEnComentarios,
          this.eventoTapComentario,
          this.eventoTapPerfilComentario
        );
    } catch (error) {}
  }

  async validarInformacionDeLosCoautores(): Promise<void> {
    try {
      this.listaComentariosFirebase.forEach(async (comentario) => {
        const indexUno = this.coautores.findIndex(
          (e) => e._id === comentario.coautor.coautor._id
        );

        if (indexUno >= 0) {
          const coautor = this.coautores[indexUno];
          comentario.coautor.coautor.nombreContacto = coautor.nombreContacto;
          comentario.coautor.coautor.album = coautor.album;
        } else {
          const coautor = await this.perfilNegocio
            .obtenerResumenPerfilParaCoautorProyecto(
              comentario.coautor.coautor._id
            )
            .toPromise();

          if (!coautor) {
            throw new Error('');
          }

          this.coautores.push(coautor);
          comentario.coautor.coautor.nombreContacto = coautor.nombreContacto;
          comentario.coautor.coautor.album = coautor.album;
        }
      });
    } catch (error) {
      this.coautores = [];
    }
  }

  eliminarComentariosDeListas(idComentario: string, index: number = -1): void {
    if (index >= 0) {
      this.listaComentariosFirebase.splice(index, 1);
    }

    // Actualizar indicador del total
    this.indicadorTotalComentarios =
      this.comenFireService.actualizarIndicadorDelTotalDeComentarios(
        idComentario,
        this.indicadorTotalComentarios
      );

    // Actualizar lista de configuraciones
    const listas =
      this.comenFireService.actualizarListaDeConfiguracionDeComentarios(
        idComentario,
        this.listaDeFechas,
        this.listaConfiguracionComentarios
      );

    this.listaDeFechas = listas.listaDeFechas;
    this.listaConfiguracionComentarios = listas.listaConfiguracionComentarios;
  }

  async obtenerComentariosPaginacion(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.paginacion.ejecutar$.next(this.params.id);
  }

  async obtenerListaDeComentariosEnPaginacion(): Promise<void> {
    if (this.params.accionEntidad === AccionEntidad.CREAR) {
      return;
    }

    this.comenFireService.paginacion.subscripcion$ =
      this.comenFireService.paginacion.respuesta$.subscribe(
        (data) => {
          this.comenFireService.desconectar(OrigenConexion.PAGINACION);
          if (!data || data.length === 0) {
            this.puedeCargarMas = true;
            return;
          }

          this.configurarListaDeComentarios(data);
          this.puedeCargarMas = true;
        },
        (error) => {
          this.puedeCargarMas = true;
          this.comenFireService.paginacionComentarios -= 30;
        }
      );
  }

  trackByFn(index: number, item: ConfiguracionComentario): string {
    return item.idInterno;
  }

  validarSiHayMediasEnElAlbum(): boolean {
    try {
      const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
        CodigosCatalogoTipoAlbum.GENERAL,
        this.proyecto.adjuntos
      );

      if (!album) {
        throw new Error('');
      }

      return album.media.length > 0;
    } catch (error) {
      return false;
    }
  }

  obtenerInputSegunKey(key: InputKey): InputCompartido {
    const index = this.inputsForm.findIndex((e) => e.key === key);

    if (index < 0) {
      return undefined;
    }

    return this.inputsForm[index].input;
  }

  async configurarDialogoEliminarPDF(): Promise<void> {
    this.confDialogoEliminarPDF = {
      mostrarDialogo: false,
      descripcion: 'm3v11texto7',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm2v13texto9',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.proyecto.medias = [];
            this.confDialogoEliminarPDF.mostrarDialogo = false;
            this.confDialogoFullProyecto.mostrarDialogo = false;
          },
        },
        {
          text: 'm2v13texto10',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoEliminarPDF.mostrarDialogo = false;
            this.confDialogoFullProyecto.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  async configurarDialologConfirmarTransferencia(): Promise<void> {
    this.confDialogoConfirmarTransferencia = {
      mostrarDialogo: false,
      descripcion: 'm4v17texto6',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm4v17texto7',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoConfirmarTransferencia.mostrarDialogo = false;
            this.enviarProyectoComoMensaje(
              CatalogoTipoMensaje.TRANSFERIR_PROYECTO
            );
          },
        },
        {
          text: 'm4v17texto8',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.confDialogoConfirmarTransferencia.mostrarDialogo = false;
          },
        },
      ],
    };
  }

  configuracionPropietarioProyecto(): ConfiguracionItemListaContactosCompartido {
    let usoCirculo: UsoItemCircular;
    let urlMedia: string;

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.PERFIL,
      this.proyecto.perfil.album
    );

    if (
      !album ||
      (album.portada &&
        album.portada.principal &&
        album.portada.principal.fileDefault)
    ) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      urlMedia = album.portada.principal.url;
    } else {
      usoCirculo = UsoItemCircular.CIRCONTACTO;
      urlMedia = album.portada.principal.url;
    }

    return {
      id: this.proyecto.perfil._id || '',
      usoItem: UsoItemListaContacto.USO_CONTACTO,
      contacto: {
        nombreContacto: this.proyecto.perfil.nombreContacto,
        nombreContactoTraducido: this.proyecto.perfil.nombreContactoTraducido,
        nombre: this.proyecto.perfil.nombre,
        estilosTextoSuperior: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        estilosTextoInferior: {
          color: ColorDelTexto.TEXTONEGRO,
          estiloTexto: EstilosDelTexto.REGULAR,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        idPerfil: this.proyecto.perfil._id || '',
      },
      configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
        urlMedia,
        ColorDeBorde.BORDER_ROJO,
        this.metodosParaFotos.obtenerColorFondoAleatorio(),
        false,
        usoCirculo,
        true
      ),
      mostrarX: {
        mostrar: false,
        color: true,
      },
      eventoCirculoNombre: () => {
        // this.abrirChatValidandoNotificaciones(idAsociacion)
      },
    };
  }
}

export interface Inputs {
  key: InputKey;
  input: InputCompartido;
}

export enum InputKey {
  TITULO_CORTO = 'titulo-corto',
  TITULO = 'titulo',
  LOCALIDAD = 'descripcion-direccion',
  UBICACION = 'ubicacion',
  AUTOR = 'autor',
}

export interface IndicadorTotalComentarios {
  total: number;
  comentarios: Array<string>;
}

export enum OrigenValidacionDeCambios {
  BOTON_BACK = 'back',
  BOTON_HOME = 'home',
  BOTON_ALBUM_GENERAL = 'album-general',
  BOTON_ALBUM_AUDIOS = 'album-audios',
  BOTON_ALBUM_LINKS = 'album-links',
}

const ICONO_IDIOMA = {
  en: 'boton-circular-en',
  de: 'boton-circular-de',
  fr: 'boton-circular-fr',
  es: 'boton-circular-es',
  it: 'boton-circular-it',
  pt: 'boton-circular-pt',
};
