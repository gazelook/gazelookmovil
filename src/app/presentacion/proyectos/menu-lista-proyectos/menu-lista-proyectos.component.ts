import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { ConfiguracionContadorDias } from '@shared/diseno/modelos/contador-dias.interface';
import { ConfiguracionItemBuscadorProyectoNoticia } from '@shared/diseno/modelos/item-buscador-proyectos-noticias.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorDeBorde,
  ColorDeFondo,
  ColorDelTexto,
  ColorFondoLinea,
  EspesorLineaItem,
  EstilosDelTexto,
} from '@shared/diseno/enums/estilos-colores-general';
import { ContadorDiasComponent } from '@shared/componentes/contador-dias/contador-dias.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { PaddingIzqDerDelTexto } from '@shared/diseno/enums/estilos-padding-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ConfiguracionDialogoInline } from '@shared/diseno/modelos/dialogo-inline.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';

@Component({
  selector: 'app-menu-lista-proyectos',
  templateUrl: './menu-lista-proyectos.component.html',
  styleUrls: ['./menu-lista-proyectos.component.scss'],
})
export class MenuListaProyectosComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  @ViewChild('contadorInicioForo', { static: false })
  contadorInicioForo: ContadorDiasComponent;
  @ViewChild('contadorFinalForo', { static: false })
  contadorFinalForo: ContadorDiasComponent;

  // Utils
  public MenuListaProyectosEnum = MenuListaProyectos;

  // Parametros internos
  public sesionIniciada: boolean;
  public perfilSeleccionado: PerfilModel;
  public diaEnSegundos: number;
  public horaEnSegundos: number;
  public minutoEnSegundos: number;
  public params: ProyectoParams;
  public listaProyectos: PaginacionModel<string>;
  public puedeCargarMas: boolean;

  // Configuracion hijos
  public confAppBar: ConfiguracionAppbarCompartida;
  public confLineaVerde: LineaCompartida;
  public confLineaVerdeAll: LineaCompartida;
  public confContadorDiasForoInicio: ConfiguracionContadorDias;
  public confContadorDiasForoFinal: ConfiguracionContadorDias;
  public confToast: ConfiguracionToast;
  public confDialogoProyectosEnEsperaDeFondos: ConfiguracionDialogoInline;
  public confDialogoProyectosEnEstrategia: ConfiguracionDialogoInline;
  public confDialogoProyectosEnEjecucion: ConfiguracionDialogoInline;
  public confDialogoProyectosEjecutados: ConfiguracionDialogoInline;
  public confDialogoProyectosMenosVotados: ConfiguracionDialogoInline;
  public confDialogoTiempoForoAunNoInicia: ConfiguracionDialogoInline;
  public confDialogoTiempoForoAunNoInicia2: ConfiguracionDialogoInline;

  public confDialogoProyectosRecomendados: ConfiguracionDialogoInline;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private route: ActivatedRoute,
    private proyectoNegocio: ProyectoNegocio,
    private location: Location,
    private albumNegocio: AlbumNegocio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.params = { estado: false };
    this.diaEnSegundos = 86400;
    this.horaEnSegundos = 3600;
    this.minutoEnSegundos = 60;
    this.puedeCargarMas = true;
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true;
    this.configurarEstadoSesion();
    this.configurarPerfilSeleccionado();
    this.configurarParametros();
    if (this.params.estado && this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarLinea();
      this.configurarToast();
      this.configurarContadoresDias();
      this.configurarDialogos();
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(
        this.perfilSeleccionado,
        this.params.estado
      );
    }
  }

  configurarEstadoSesion(): void {
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
  }

  configurarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarParametros(): void {
    const urlParams: Params = this.route.snapshot.params;
    if (urlParams.codigoTipoProyecto) {
      this.params.codigoTipoProyecto =
        urlParams.codigoTipoProyecto as CodigosCatalogoTipoProyecto;
      this.params.estado = true;
    }
  }

  configurarAppBar(): void {
    let llaverTextoTipoProyecto: string;
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_LOCAL
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2.2';
    }
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2';
    }
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_PAIS
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2.1';
    }
    if (
      this.params.codigoTipoProyecto ===
      CodigosCatalogoTipoProyecto.PROYECTO_RED
    ) {
      llaverTextoTipoProyecto = 'm5v1texto2.3';
    }
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {
        this.accionAtras();
      },
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil
              .codigo as CodigosCatalogoTipoPerfil
          ),
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: llaverTextoTipoProyecto,
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v1texto1',
            valorBusqueda: '',
          },
        },
      },
    };
  }

  configurarLinea(): void {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };

    this.confLineaVerdeAll = {
      ancho: AnchoLineaItem.ANCHO6920,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }

  configurarToast(): void {
    this.confToast = {
      mostrarToast: false, // True para mostrar
      mostrarLoader: false, // true para mostrar cargando en el toast
      cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
    };
  }

  configurarContadoresDias(): void {
    this.confContadorDiasForoInicio = this.configurarContadorDias(
      false,
      'texto836'
    );
    this.confContadorDiasForoFinal = this.configurarContadorDias(
      false,
      'texto842'
    );
  }

  configurarContadorDias(
    mostrar: boolean = false,
    titulo: string = '',
    fechaInicial: Date = new Date(),
    fechaFinal: Date = new Date()
  ): ConfiguracionContadorDias {
    return {
      mostrar,
      mostrarLoader: true,
      titulo,
      fechaInicial,
      fechaFinal,
    };
  }

  configurarDialogos(): void {
    // ProyectosRecomendanos
    this.confDialogoProyectosRecomendados = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };
    this.confDialogoProyectosRecomendados.descripcion.push(
      {
        texto: 'm5v2texto13.1',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto13.2',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos seleccionados
    this.confDialogoProyectosEnEsperaDeFondos = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEnEsperaDeFondos.descripcion.push(
      {
        texto: 'm5v2texto14',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto15',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos en estrategia
    this.confDialogoProyectosEnEstrategia = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEnEstrategia.descripcion.push(
      {
        texto: 'm5v2texto16',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto17',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos en ejecucion
    this.confDialogoProyectosEnEjecucion = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEnEjecucion.descripcion.push(
      {
        texto: 'm5v2texto18',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto19',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // Proyectos ejecutados
    this.confDialogoProyectosEjecutados = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosEjecutados.descripcion.push(
      {
        texto: 'm5v2texto20',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto21',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // PROYECTOS MENOS VOTADOS
    this.confDialogoProyectosMenosVotados = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoProyectosMenosVotados.descripcion.push(
      {
        texto: 'm5v2texto18',
        estilo: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      },
      {
        texto: 'm5v2texto19',
        estilo: {
          color: ColorDelTexto.TEXTOROJOBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
          paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
        },
      }
    );

    // TIEMPO FORO AUN NO INICIA
    this.confDialogoTiempoForoAunNoInicia = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoTiempoForoAunNoInicia.descripcion.push({
      texto: 'm5v2texto13.3',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });

    // TIEMPO FORO AUN NO INICIA2
    this.confDialogoTiempoForoAunNoInicia2 = {
      noMostrar: true,
      descripcion: [],
      listaBotones: [],
      sinMargenEnDescripcion: true,
      anchoParaProyectos: true,
    };

    this.confDialogoTiempoForoAunNoInicia2.descripcion.push({
      texto: 'm5v2texto13.4',
      estilo: {
        color: ColorDelTexto.TEXTOAZULBASE,
        estiloTexto: EstilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: TamanoDeTextoConInterlineado.L10_I1,
        paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0,
      },
    });
  }

  inicializarDataListaProyectos(): void {
    this.listaProyectos = {
      anteriorPagina: false,
      paginaActual: 1,
      proximaPagina: true,
      totalDatos: 0,
      totalPaginas: 0,
      lista: [],
    };
  }

  navegarSegunMenu(menuTipo: MenuListaProyectos): void {
    switch (menuTipo) {
      case MenuListaProyectos.NUEVOS_PROYECTOS:
        this.verNuevosProyectos();
        break;
      case MenuListaProyectos.PROYECTOS_RECOMENDADOS_POR_ADMIN:
        this.mostrarProyectosRecomendados();
        // this.reiniciarDialogos();
        this.confDialogoProyectosRecomendados.noMostrar = false;
        break;
      case MenuListaProyectos.FORO_DE_LOS_PROYECTOS:
        // this.reiniciarDialogos();
        // this.abrirContadorParaForos();
        this.mostrarProyectosForos();
        break;
      case MenuListaProyectos.PROYECTOS_SELECCIONADOS:
        this.mostrarProyectosSeleccionados();
        // this.reiniciarDialogos();
        // this.abrirContadorParaForos(false);
        // this.confDialogoTiempoForoAunNoInicia2.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_EN_ESPERA_DE_FONDOS:
        // this.reiniciarDialogos();
        // this.abrirContadorParaForos(false);
        this.mostrarProyectosEsperaFondos();

        break;
      case MenuListaProyectos.PROYECTOS_EN_ESTRATEGIA:
        this.reiniciarDialogos();
        this.confDialogoProyectosEnEstrategia.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_EN_EJECUCION:
        this.reiniciarDialogos();
        this.confDialogoProyectosEnEjecucion.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_EJECUTADOS:
        this.reiniciarDialogos();
        this.confDialogoProyectosEjecutados.noMostrar = false;
        break;
      case MenuListaProyectos.PROYECTOS_MENOS_VOTADOS:
        // this.reiniciarDialogos()

        this.verMenosVotadoProyectos();
        this.confDialogoProyectosMenosVotados.noMostrar = false;
        break;
      default:
        break;
    }
  }

  async abrirContadorParaForos(inicio: boolean = true): Promise<void> {
    try {
      const fechaString = await this.proyectoNegocio
        .obtenerFechaForo(inicio)
        .toPromise();

      if (fechaString) {
        const date: Date = new Date(fechaString.fecha);
        let fechaForo = new Date(date);
        let fechaActual = new Date();

        if (inicio) {
          if (fechaForo > fechaActual) {
            this.confDialogoTiempoForoAunNoInicia.noMostrar = false;

            this.confContadorDiasForoInicio = this.configurarContadorDias(
              true,
              'm5v2texto2',
              new Date(),
              date
            );
            this.confContadorDiasForoInicio = this.calcularTiempo(
              this.confContadorDiasForoInicio
            );
            this.confContadorDiasForoInicio.mostrarLoader = false;
            return;
          }
          if (fechaForo <= fechaActual) {
            console.log('aca');

            this.mostrarProyectosForos();
            return;
          }
        }

        if (!inicio) {
          if (fechaForo > fechaActual) {
            this.confContadorDiasForoFinal = this.configurarContadorDias(
              true,
              'm5v2texto8',
              new Date(),
              date
            );
            this.confContadorDiasForoFinal = this.calcularTiempo(
              this.confContadorDiasForoFinal
            );
            this.confContadorDiasForoFinal.mostrarLoader = false;
            this.confDialogoProyectosEnEsperaDeFondos.noMostrar = false;
            return;
          }

          if (fechaForo <= fechaActual) {
            this.mostrarProyectosSeleccionados();
            this.mostrarProyectosEsperaFondos();
            return;
          }
        }
      }
    } catch (error) {
      this.toast.abrirToast('text36');
    }
  }

  calcularTiempo(
    configuracion: ConfiguracionContadorDias
  ): ConfiguracionContadorDias {
    if (
      configuracion.fechaFinal.getTime() < configuracion.fechaInicial.getTime()
    ) {
      configuracion.dias = 0;
      configuracion.horas = 0;
      configuracion.minutos = 0;
      configuracion.segundos = 0;
      return configuracion;
    }

    let totalSegundos =
      Math.abs(
        configuracion.fechaFinal.valueOf() -
          configuracion.fechaInicial.valueOf()
      ) / 1000;

    // Total de dias
    const dias = Math.floor(totalSegundos / this.diaEnSegundos);
    totalSegundos -= dias * this.diaEnSegundos;

    // Total de horas
    const horas = Math.floor(totalSegundos / this.horaEnSegundos) % 24;
    totalSegundos -= horas * this.horaEnSegundos;

    // Total de minutos
    const minutos = Math.floor(totalSegundos / this.minutoEnSegundos) % 60;
    totalSegundos -= minutos * this.minutoEnSegundos;

    // Total de segundos
    configuracion.dias = dias;
    configuracion.horas = horas;
    configuracion.minutos = minutos;
    configuracion.segundos = Math.round(totalSegundos);
    return configuracion;
  }

  accionAtras(): void {
    // if (
    // 	(this.confContadorDiasForoInicio && this.confContadorDiasForoInicio.mostrar) ||
    // 	(this.confContadorDiasForoFinal && this.confContadorDiasForoFinal.mostrar) ||
    // 	(!this.confDialogoProyectosEnEsperaDeFondos.noMostrar) ||
    // 	(!this.confDialogoProyectosEnEstrategia.noMostrar) ||
    // 	(!this.confDialogoProyectosEnEjecucion.noMostrar) ||
    // 	(!this.confDialogoProyectosEjecutados.noMostrar)
    // ) {
    // 	this.reiniciarDialogos()
    // 	return
    // }

    this.location.back();
  }

  verNuevosProyectos(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    let componente = RutasProyectos.LISTA_NUEVOS_PROYECTOS.toString();
    componente = componente.replace(
      ':codigoTipoProyecto',
      this.params.codigoTipoProyecto
    );
    componente = componente.replace(':menosVotado', 'false');
    this.router.navigateByUrl(ruta + '/' + componente).then();
  }

  mostrarProyectosRecomendados(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    const componente =
      RutasProyectos.LISTA_PROYECTOS_RECOMENDADOS.toString().replace(
        ':codigoTipoProyecto',
        this.params.codigoTipoProyecto
      );
    const url = `${ruta}/${componente}`;
    this.router.navigateByUrl(url).then();
  }

  mostrarProyectosForos(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    const componente = RutasProyectos.LISTA_PROYECTOS_FOROS.toString().replace(
      ':codigoTipoProyecto',
      this.params.codigoTipoProyecto
    );
    const url = `${ruta}/${componente}`;
    this.router.navigateByUrl(url).then();
  }

  mostrarProyectosEsperaFondos(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    const componente =
      RutasProyectos.LISTA_PROYECTOS_ESPERA_FONDOS.toString().replace(
        ':codigoTipoProyecto',
        this.params.codigoTipoProyecto
      );
    const url = `${ruta}/${componente}`;
    this.router.navigateByUrl(url).then();
  }

  mostrarProyectosSeleccionados(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    const componente =
      RutasProyectos.LISTA_PROYECTOS_SELECCIONADOS.toString().replace(
        ':codigoTipoProyecto',
        this.params.codigoTipoProyecto
      );
    const url = `${ruta}/${componente}`;
    this.router.navigateByUrl(url).then();
  }

  verMenosVotadoProyectos(): void {
    const ruta = RutasLocales.MODULO_PROYECTOS.toString();
    let componente = RutasProyectos.LISTA_NUEVOS_PROYECTOS.toString();
    componente = componente.replace(
      ':codigoTipoProyecto',
      this.params.codigoTipoProyecto
    );
    componente = componente.replace(':menosVotado', 'true');

    this.router.navigateByUrl(ruta + '/' + componente).then();
  }

  async buscarProyectosPorTitulo(titulo: string): Promise<void> {
    if (!this.listaProyectos.proximaPagina) {
      this.puedeCargarMas = false;
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = false
      return;
    }

    try {
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = (
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.lista.length === 0
      // )
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargandoPequeno = (
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.lista.length > 0
      // )

      const dataPaginacion = await this.proyectoNegocio
        .buscarProyectosPorTitulo(titulo, 10, this.listaProyectos.paginaActual)
        .toPromise();

      this.listaProyectos.totalDatos = dataPaginacion.totalDatos;
      this.listaProyectos.proximaPagina = dataPaginacion.proximaPagina;

      dataPaginacion.lista.forEach((proyecto) => {
        // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.lista.push(
        // 	this.configurarItemNoticiasProyectos(proyecto)
        // )
      });
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = false
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargandoPequeno = false
      this.puedeCargarMas = true;

      if (this.listaProyectos.proximaPagina) {
        this.listaProyectos.paginaActual += 1;
      }
    } catch (error) {
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = false
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargandoPequeno = false
      // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.error = 'text37'
      this.puedeCargarMas = false;
    }
  }

  configurarItemNoticiasProyectos(
    proyecto: ProyectoModel
  ): ConfiguracionItemBuscadorProyectoNoticia {
    return {
      codigoEntidad: CodigosCatalogoEntidad.PROYECTO,
      proyecto,
      configuracionRectangulo: this.obtenerConfiguracionRectangulo(proyecto),
      eventoTap: (id: string) => {
        if (!id || id.length === 0) {
          return;
        }

        const modulo = RutasLocales.MODULO_PROYECTOS.toString();
        let ruta = RutasProyectos.VISITAR.toString();
        ruta = ruta.replace(':id', id);
        this.router.navigateByUrl(modulo + '/' + ruta).then();
      },
    };
  }

  obtenerConfiguracionRectangulo(
    proyecto: ProyectoModel
  ): CongifuracionItemProyectosNoticias {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );

    return {
      usoVersionMini: true,
      id: proyecto.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: false,
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: false,
      },
      urlMedia:
        album && album.portada
          ? album.portada.principal.url || album.portada.miniatura.url || ''
          : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        ((album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url))
      ),
      eventoTap: {
        activo: false,
      },
      eventoDobleTap: {
        activo: false,
      },
      eventoPress: {
        activo: false,
      },
    };
  }

  cerrarBuscador(): void {
    this.confAppBar.searchBarAppBar.buscador.configuracion.valorBusqueda = '';
    // this.confAppBar.searchBarAppBar.buscador.configuracion.mostrarResultado = false
    // this.confAppBar.searchBarAppBar.buscador.configuracion.mostrarIconoLupa = true
    // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargando = false
    // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.cargandoPequeno = false
    // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.lista = []
    // this.confAppBar.searchBarAppBar.buscador.configuracion.configuracionLista.error = null
    this.inicializarDataListaProyectos();
  }

  reiniciarDialogos(): void {
    this.confContadorDiasForoFinal = this.configurarContadorDias(false);
    this.confContadorDiasForoInicio = this.configurarContadorDias(false);
    this.confDialogoProyectosEjecutados.noMostrar = true;
    this.confDialogoProyectosEnEjecucion.noMostrar = true;
    this.confDialogoProyectosEnEsperaDeFondos.noMostrar = true;
    this.confDialogoProyectosEnEstrategia.noMostrar = true;
    this.confDialogoProyectosMenosVotados.noMostrar = true;
    this.confDialogoProyectosRecomendados.noMostrar = true;
    this.confDialogoTiempoForoAunNoInicia.noMostrar = true;
    this.confDialogoTiempoForoAunNoInicia2.noMostrar = true;
  }
}

export enum MenuListaProyectos {
  NUEVOS_PROYECTOS = '0',
  PROYECTOS_RECOMENDADOS_POR_ADMIN = '1',
  FORO_DE_LOS_PROYECTOS = '2',
  PROYECTOS_SELECCIONADOS = '3',
  PROYECTOS_EN_ESPERA_DE_FONDOS = '4',
  PROYECTOS_EN_ESTRATEGIA = '5',
  PROYECTOS_EN_EJECUCION = '6',
  PROYECTOS_EJECUTADOS = '7',
  PROYECTOS_MENOS_VOTADOS = '8',
}
