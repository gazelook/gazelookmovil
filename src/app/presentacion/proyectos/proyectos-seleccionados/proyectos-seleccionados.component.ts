import {Component, OnInit} from '@angular/core';
import {AlbumModel, PerfilModel, ProyectoModel} from 'dominio/modelo/entidades';
import {PaginacionModel} from 'dominio/modelo';
import {
  ConfiguracionAppbarCompartida,
  CongifuracionItemProyectos,
  CongifuracionItemProyectosNoticias,
  LineaCompartida,
  ModoBusqueda
} from '@shared/diseno/modelos';
import {ProyectoParams} from 'dominio/modelo/parametros';
import {AlbumNegocio, PerfilNegocio, ProyectoNegocio} from 'dominio/logica-negocio';
import {ActivatedRoute, Router} from '@env/node_modules/@angular/router';
import {Location} from '@env/node_modules/@angular/common';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {VariablesGlobales} from '@core/servicios/generales';
import {
  AnchoLineaItem,
  arrayPosotionCenterProject,
  arrayPosotionCenterRigthProject,
  arrayPosotionEndProject,
  arrayPosotionStartProject,
  ColorDeBorde,
  ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem,
  TamanoColorDeFondo,
  UsoAppBar,
  UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {
  CodigosCatalogoEntidad,
  CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil,
  CodigosCatalogoTipoProyecto
} from '@core/servicios/remotos/codigos-catalogos';
import {RutasProyectos} from '@env/src/app/presentacion/proyectos/rutas-proyectos.enum';
import {RutasLocales} from '@env/src/app/rutas-locales.enum';


@Component({
  selector: 'app-proyectos-seleccionados',
  templateUrl: './proyectos-seleccionados.component.html',
  styleUrls: ['./proyectos-seleccionados.component.scss']
})
export class ProyectosSeleccionadosComponent implements OnInit {

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public puedeCargarMas: boolean;
  public mensajeCapaError: string;

  public perfilSeleccionado: PerfilModel;

  public listaProyectosSeleccionados: PaginacionModel<CongifuracionItemProyectos>;
  public listaProyectosSeleccionadosModel: ProyectoModel[] = [];

  public params: ProyectoParams;

  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaVerde: LineaCompartida;
  public idCapaCuerpo: string;

  public fecha: Date;
  public fechaFiltro: string;

  private limite = 12;

  constructor(
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.variablesGlobales.mostrarMundo = true;
    this.params = {estado: false};

    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.listaProyectosSeleccionadosModel = [];

    this.idCapaCuerpo = 'capa-cuerpo-intercambios';
  }

  ngOnInit(): void {
    this.inicializarPerfilSeleccionado();
    this.configurarParametros();
    this.configurarAppBar();
    this.inicializarListaProyectos();
    this.obtenerProyectosSeleccionados();
    this.configurarLineas();
    this.fecha = new Date();

  }

  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  obtenerProyectosSeleccionados(): void {

    if (!this.listaProyectosSeleccionados.proximaPagina) {
      this.puedeCargarMas = true;
      return;
    }
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = (this.listaProyectosSeleccionados.lista.length === 0);

    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;

    this.proyectoNegocio.obtenerProyectosSeleccionados(
      id,
      this.limite,
      this.listaProyectosSeleccionados.paginaActual,
      tipo,
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosSeleccionados.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosSeleccionadosModel = respuesta.lista;
          this.listaProyectosSeleccionadosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosSeleccionados.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosSeleccionados.paginaActual += 1;
        },
        (error) => {
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
  }

  obtenerProyectosSeleccionadosFecha(): void {
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = true;
    this.listaProyectosSeleccionados.lista = [];
    this.listaProyectosSeleccionadosModel = [];
    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;

    this.proyectoNegocio.obtenerProyectosSeleccionados(
      id,
      this.limite,
      this.listaProyectosSeleccionados.paginaActual = 1,
      tipo,
      this.fechaFiltro
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosSeleccionados.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosSeleccionadosModel = respuesta.lista;
          this.listaProyectosSeleccionadosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosSeleccionados.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosSeleccionados.paginaActual += 1;
        },
        (error) => {
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
  }

  configurarItemProyectos(proyecto: ProyectoModel, contador: number): CongifuracionItemProyectos {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );
    return {
      positionStart: arrayPosotionStartProject.includes(contador),
      positionEnd: arrayPosotionEndProject.includes(contador),
      positionCenter: arrayPosotionCenterProject.includes(contador),
      positionCenterRigth: arrayPosotionCenterRigthProject.includes(contador),
      numeroProyecto: contador,
      id: proyecto.id,
      totalVotos: proyecto.totalVotos,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (!data || !this.perfilSeleccionado) {
            return;
          }
          const proyectoLista: ProyectoModel = this.obtenerProyectoDeLista(data.id);

          const ruta = (proyectoLista) && this.perfilSeleccionado._id === proyectoLista.perfil._id
            ? RutasProyectos.ACTUALIZAR.toString().replace(':id', data.id)
            : RutasProyectos.VISITAR.toString().replace(':id', data.id);

          const modulo = RutasLocales.MODULO_PROYECTOS.toString();
          const url = `${modulo}/${ruta}`;
          console.log('URL', url);
          
          this.router.navigateByUrl(url).then();
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      },
    };
  }


  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectosSeleccionadosModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectosSeleccionadosModel[index];
    }

    return null;
  }

  scroolEnCapaCuerpo(): void {
    if (!this.puedeCargarMas) {
      return;
    }
    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;
      this.obtenerProyectosSeleccionados();
    }
  }

  inicializarListaProyectos(): void {
    this.listaProyectosSeleccionadosModel = [];
    this.listaProyectosSeleccionados = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }

  configurarParametros(): void {
    this.activatedRoute.paramMap.subscribe(
      params => this.params.codigoTipoProyecto = params.get('codigoTipoProyecto') as CodigosCatalogoTipoProyecto
    );
  }

  cambioDeFecha(): void {
    this.fechaFiltro = new Date(this.fecha).toISOString().substring(0, 10);
    this.obtenerProyectosSeleccionadosFecha();
  }

  configurarAppBar(): void {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.accionAtras(),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v15texto11'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v3texto1',
            valorBusqueda: '',
          }
        }
      },
    };
  }

  accionAtras(): void {
    this.location.back();
  }

  configurarLineas(): void {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6028,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    };
  }

}
