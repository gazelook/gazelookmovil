import {Component, OnInit} from '@angular/core';
import {
  BotonCompartido,
  ConfiguracionAppbarCompartida,
  CongifuracionItemProyectos,
  CongifuracionItemProyectosNoticias,
  LineaCompartida,
  ModoBusqueda
} from '@shared/diseno/modelos';
import {AlbumModel, PerfilModel, ProyectoModel} from 'dominio/modelo/entidades';
import {AlbumNegocio, PerfilNegocio, ProyectoNegocio} from 'dominio/logica-negocio';
import {ActivatedRoute, Router} from '@env/node_modules/@angular/router';
import {Location} from '@env/node_modules/@angular/common';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {VariablesGlobales} from '@core/servicios/generales';
import {ProyectoParams} from 'dominio/modelo/parametros';
import {PaginacionModel} from 'dominio/modelo';
import {
  CodigosCatalogoEntidad,
  CodigosCatalogoTipoAlbum,
  CodigosCatalogoTipoPerfil,
  CodigosCatalogoTipoProyecto
} from '@core/servicios/remotos/codigos-catalogos';
import {
  arrayPosotionCenterProject,
  arrayPosotionCenterRigthProject,
  arrayPosotionEndProject,
  arrayPosotionStartProject,
  ColorDeBorde,
  ColorDeFondo,
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
  UsoAppBar,
  UsoItemProyectoNoticia
} from '@shared/diseno/enums';
import {RutasProyectos} from '@env/src/app/presentacion/proyectos/rutas-proyectos.enum';
import {RutasLocales} from '@env/src/app/rutas-locales.enum';
import {ColorTextoBoton, TipoBoton} from '@shared/componentes';


@Component({
  selector: 'app-proyectos-foros',
  templateUrl: './proyectos-foros.component.html',
  styleUrls: ['./proyectos-foros.component.scss']
})
export class ProyectosForosComponent implements OnInit {

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public puedeCargarMas: boolean;
  public mensajeCapaError: string;

  public perfilSeleccionado: PerfilModel;

  public listaProyectosForos: PaginacionModel<CongifuracionItemProyectos>;
  public listaProyectosForosModel: ProyectoModel[] = [];

  public params: ProyectoParams;

  public confAppbar: ConfiguracionAppbarCompartida;
  public confLineaNoticias: LineaCompartida;
  public idCapaCuerpo: string;

  private limite = 12;
  public configBotonProyectos: BotonCompartido;

  constructor(
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    private proyectoNegocio: ProyectoNegocio,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private variablesGlobales: VariablesGlobales
  ) {
    this.variablesGlobales.mostrarMundo = true;
    this.params = {estado: false};

    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.listaProyectosForosModel = [];

    this.idCapaCuerpo = 'capa-cuerpo-intercambios';
  }

  ngOnInit(): void {
    this.inicializarPerfilSeleccionado();
    this.configurarBoton();
    this.configurarParametros();
    this.configurarAppBar();
    this.inicializarListaProyectos();
    this.obtenerProyectosForos();

  }


  obtenerProyectosForos(): void {

    if (!this.listaProyectosForos.proximaPagina) {
      this.puedeCargarMas = true;

      return;
    }
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = (this.listaProyectosForos.lista.length === 0);

    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;
    this.configBotonProyectos.enProgreso = true;

    this.proyectoNegocio.obtenerProyectosForos(
      id,
      this.limite,
      this.listaProyectosForos.paginaActual,
      tipo
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosForos.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosForosModel = respuesta.lista;
          this.listaProyectosForosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosForos.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosForos.paginaActual += 1;
        },
        (error) => {
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
    this.configBotonProyectos.enProgreso = false;
  }

  actualizarProyectosForos(): void {
    this.mostrarCapaError = false;
    this.mostrarCapaLoader = true;
    this.listaProyectosForos.lista = [];
    this.listaProyectosForosModel = [];

    const tipo = this.params.codigoTipoProyecto;
    const id = this.perfilSeleccionado._id;
    this.configBotonProyectos.enProgreso = true;

    this.proyectoNegocio.obtenerProyectosForos(
      id,
      this.limite,
      this.listaProyectosForos.paginaActual = 1,
      tipo
    )
      .subscribe(
        (respuesta) => {
          this.listaProyectosForos.proximaPagina = respuesta.proximaPagina;
          this.listaProyectosForosModel = respuesta.lista;
          this.listaProyectosForosModel.forEach((proyecto, i) => {
            const contador = i + 1;
            this.listaProyectosForos.lista.push(this.configurarItemProyectos(proyecto, contador));
          });
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = true;
          this.listaProyectosForos.paginaActual += 1;
        },
        (error) => {
          this.configBotonProyectos.enProgreso = false;
          this.mostrarCapaLoader = false;
          this.puedeCargarMas = false;
          this.mostrarCapaError = true;
        }
      );
    this.configBotonProyectos.enProgreso = false;
  }

  configurarItemProyectos(proyecto: ProyectoModel, contador: number): CongifuracionItemProyectos {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    );
    return {
      positionStart: arrayPosotionStartProject.includes(contador),
      positionEnd: arrayPosotionEndProject.includes(contador),
      positionCenter: arrayPosotionCenterProject.includes(contador),
      positionCenterRigth: arrayPosotionCenterRigthProject.includes(contador),
      numeroProyecto: contador,
      id: proyecto.id,
      totalVotos: proyecto.totalVotos,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      actualizado: proyecto.actualizado,
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: !!(
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ),
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
          this.proyectoNegocio.removerProyectoActivoDelSessionStorage();
          if (!data || !this.perfilSeleccionado) {
            return;
          }
          const proyectoLista: ProyectoModel = this.obtenerProyectoDeLista(data.id);

          const ruta = (proyectoLista) && this.perfilSeleccionado._id === proyectoLista.perfil._id
            ? RutasProyectos.ACTUALIZAR.toString().replace(':id', data.id)
            : RutasProyectos.VISITAR.toString().replace(':id', data.id);

          const modulo = RutasLocales.MODULO_PROYECTOS.toString();
          const url = `${modulo}/${ruta}`;
          this.router.navigateByUrl(url).then();
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      },
    };
  }

  configurarBoton(): void {
    this.configBotonProyectos = {
      text: 'm5v5texto4',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.VERDE,
      enProgreso: false,
      ejecutar: () => this.actualizarProyectosForos(),
    };
  }

  obtenerProyectoDeLista(id: string): ProyectoModel {
    const index: number = this.listaProyectosForosModel.findIndex(e => e.id === id);

    if (index >= 0) {
      return this.listaProyectosForosModel[index];
    }

    return null;
  }

  scroolEnCapaCuerpo(): void {
    if (!this.puedeCargarMas) {
      return;
    }
    const elemento: HTMLElement = document.getElementById(this.idCapaCuerpo) as HTMLElement;
    if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 5.22) {
      this.puedeCargarMas = false;
      this.obtenerProyectosForos();
    }
  }

  inicializarListaProyectos(): void {
    this.listaProyectosForosModel = [];
    this.listaProyectosForos = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    };
  }


  inicializarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarParametros(): void {
    this.activatedRoute.paramMap.subscribe(
      params => this.params.codigoTipoProyecto = params.get('codigoTipoProyecto') as CodigosCatalogoTipoProyecto
    );
  }

  configurarAppBar(): void {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.accionAtras(),
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil
          )
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v15texto8'
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            entidad: CodigosCatalogoEntidad.PROYECTO,
            placeholder: 'm5v3texto1',
            valorBusqueda: '',
          }
        }
      },
    };
  }

  accionAtras(): void {
    this.location.back();
  }

}
