import { DonacionProyectosComponent } from './donacion-proyectos/donacion-proyectos.component';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProyectosRoutingModule} from 'src/app/presentacion/proyectos/proyectos-routing.module';
import {PublicarComponent} from 'src/app/presentacion/proyectos/publicar/publicar.component';
import {TranslateModule} from '@ngx-translate/core';
import {ProyectosComponent} from 'src/app/presentacion/proyectos/proyectos.component';
import {CompartidoModule} from '@shared/compartido.module';
import {InformacionUtilComponent} from 'src/app/presentacion/proyectos/informacion-util/informacion-util.component';
import {MenuVerProyectosComponent} from 'src/app/presentacion/proyectos/menu-ver-proyectos/menu-ver-proyectos.component';
import {MenuListaProyectosComponent} from 'src/app/presentacion/proyectos/menu-lista-proyectos/menu-lista-proyectos.component';
import {NuevosProyectosComponent} from 'src/app/presentacion/proyectos/nuevos-proyectos/nuevos-proyectos.component';
import {HistoricoProyectoComponent} from 'src/app/presentacion/proyectos/historico-proyectos/historico-proyecto.component';
import {ProyectosRecomendadosComponent} from './proyectos-recomendados/proyectos-recomendados.component';
import {ProyectosForosComponent} from './proyectos-foros/proyectos-foros.component';
import {ProyectosSeleccionadosComponent} from './proyectos-seleccionados/proyectos-seleccionados.component';
import {ProyectosEsperaFondosComponent} from './proyectos-espera-fondos/proyectos-espera-fondos.component';
import {MetodoCriptoComponent} from './donacion-proyectos/metodos-pago/metodo-cripto/metodo-cripto.component';
import {MetodoStripeComponent} from './donacion-proyectos/metodos-pago/metodo-stripe/metodo-stripe.component';
import {MetodoPaymentezComponent} from './donacion-proyectos/metodos-pago/metodo-paymentez/metodo-paymentez.component';
import {NgxStripeModule} from '@env/node_modules/ngx-stripe';


@NgModule({
  declarations: [
    ProyectosComponent,
    PublicarComponent,
    InformacionUtilComponent,
    MenuVerProyectosComponent,
    MenuListaProyectosComponent,
    NuevosProyectosComponent,
    HistoricoProyectoComponent,
    ProyectosRecomendadosComponent,
    ProyectosForosComponent,
    ProyectosSeleccionadosComponent,
    ProyectosEsperaFondosComponent,
    DonacionProyectosComponent,
    MetodoCriptoComponent,
    MetodoStripeComponent,
    MetodoPaymentezComponent,
  ],
    imports: [
        FormsModule,
        TranslateModule,
        CommonModule,
        ProyectosRoutingModule,
        CompartidoModule,
        NgxStripeModule,
    ],
  exports: [
    FormsModule,
    TranslateModule,
    CompartidoModule,
    MetodoStripeComponent,
    MetodoPaymentezComponent,
    MetodoCriptoComponent,
  ]
})
export class ProyectosModule {
}
