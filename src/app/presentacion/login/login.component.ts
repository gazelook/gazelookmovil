import { Location } from '@angular/common';
import {
  Component,
  HostListener,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { LlavesLocalStorage } from '@core/servicios/locales/llaves/local-storage.enum';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';
import { CodigosCatalogoIdioma } from '@core/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { MetodosLocalStorageService } from '@core/util/metodos-local-storage.service';
import { TranslateService } from '@ngx-translate/core';
import {
  ColorTextoBoton,
  TipoBoton
} from '@shared/componentes/button/button.component';
import { PensamientoCompartidoComponent } from '@shared/componentes/pensamiento/pensamiento-compartido.component';
import { PortadaGazeComponent } from '@shared/componentes/portada-gaze/portada-gaze.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem,
  EstiloErrorInput
} from '@shared/diseno/enums/estilos-colores-general';
import {
  TamanoDeTextoConInterlineado,
  TamanoLista,
  TamanoPortadaGaze
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import {
  EstiloItemPensamiento,
  TipoPensamiento
} from '@shared/diseno/enums/tipo-pensamiento.enum';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionLineaVerde } from '@shared/diseno/modelos/lista-contactos.interface';
import { PensamientoCompartido } from '@shared/diseno/modelos/pensamiento';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { IdiomaNegocio } from 'dominio/logica-negocio/idioma.negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import {CodigosCatalogoIdiomaCod} from '@core/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  loginForm: FormGroup;
  pensamientoCompartido: PensamientoCompartido;
  inputEmail: InputCompartido;
  inputContrasena: InputCompartido;
  botonCompartido: BotonCompartido;
  botonSubscribirse: BotonCompartido;
  botonSubmit: BotonCompartido;
  dataListaAleatorio: DatosLista;
  configuracionToast: ConfiguracionToast;
  configuracionLineaVerde: ConfiguracionLineaVerde;
  configuracionLineaVerdeCompleta: ConfiguracionLineaVerde;
  @ViewChild('container', { read: ViewContainerRef })
  container: ViewContainerRef;
  @ViewChild(PensamientoCompartidoComponent)
  pensamiento: PensamientoCompartidoComponent;
  @ViewChild('portadaGazeComponent') portadaGazeComponent: PortadaGazeComponent;
  components = [];
  configuracionPortada: PortadaGazeCompartido;

  public mostrarBanda: boolean;
  public confDialogoRecuperarContrasena: ConfiguracionRecuperarContrasena;
  public confBotonAvisoIdiomas: BotonCompartido;
  quote: {};

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private formBuilder: FormBuilder,
    public variablesGlobales: VariablesGlobales,
    private metodosLocalStorageService: MetodosLocalStorageService,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private router: Router,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private idiomaNegocio: IdiomaNegocio,
    private translateService: TranslateService,
    public auth: AngularFireAuth,
    private generadorId: GeneradorId
  ) {
    this.mostrarBanda = false;

    this.quote = {
      1: {
        price: 0.00043015718597491534,
        last_updated: '2021-12-13T16:50:02.000Z',
      },
    };
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.iniciarSesion();
    }
  }

  ngOnInit(): void {
    this.detectarDispositivo();
    this.configurarVariblesGlobales();
    this.iniciarElementos();
    this.configurarPortada();
    this.validarAvisoIdiomasEnLocalStorage();
    this.configurarBotones();
    this.validarIdiomaSeleccionado();
    this.inicializarDialogoRecuperarContrasena();
    this.configurarLineaVerde();
    this.verificarBanda();
    this.configurarListaPensamientoAleatorio();


    for (let key in this.quote) {
      let llave = this.quote[key];

      for (let key2 in llave) {
        let llave2 = llave[key2];
        break;
      }
    }
  }

  configurarPortada() {
    this.configuracionPortada = {
      tamano: TamanoPortadaGaze.PORTADACOMPLETA,
      espacioDerecha: false,
    };
  }

  configurarListaPensamientoAleatorio() {
    this.dataListaAleatorio = {
      tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
      lista: [],
      cargarMas: () => {},
    };
  }

  detectarDispositivo() {
    if (
      !/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      document.location.href = 'https://gazelook.com';
    }
  }

  configurarVariblesGlobales() {
    this.variablesGlobales.mostrarMundo = true;
  }

  configurarLineaVerde() {
    this.configuracionLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6386,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      forzarAlFinal: false,
    };
    this.configuracionLineaVerdeCompleta = {
      ancho: AnchoLineaItem.ANCHO100,
      espesor: EspesorLineaItem.ESPESOR041,
      colorFondo: ColorFondoLinea.FONDOLINEACELESTE,
      forzarAlFinal: false,
    };
  }

  verificarBanda() {
    if (this.variablesGlobales.mostrarAvisoIdiomas === false) {
      this.mostrarBanda = true;
    }
  }

  validarTipoEmail(data: { id: string; texto: string }) {
    let validar_email =
      /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;
    if (data.texto === '') {
      this.asignarMensajeDeErrorEnInput(data.id, 'text1');
      return;
    }
    if (!validar_email.test(data.texto)) {
      this.asignarMensajeDeErrorEnInput(data.id, 'text7');
      return;
    }
  }

  asignarMensajeDeErrorEnInput(id: string, mensaje: string): void {
    this.inputEmail.errorPersonalizado = mensaje;
  }

  //Inicia todos los componentes
  async iniciarElementos() {
    // "^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$"
    let emailPattern =
      '^(?=.*[A-Za-z])(?=.*d)(?=.*[@$!%*#?&])[A-Za-zd@$!%*#?&]{8,}$';
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      contrasena: ['', [Validators.required]],
    });

    this.configuracionToast = {
      cerrarClickOutside: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: '',
    };

    this.inputEmail = {
      id: this.generadorId.generarIdConSemilla(),
      tipo: 'text',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.AMARILLO,
        estiloInput: EstiloInput.LOGIN,
      },
      placeholder: '',
      data: this.loginForm.controls.email,
      bloquearCopy: false,
      validarCampo: {
        validar: true,
        validador: (data: any) => {
          this.validarTipoEmail(data);
        },
      },
    };

    this.inputContrasena = {
      id: this.generadorId.generarIdConSemilla(),
      tipo: 'password',
      error: false,
      estilo: {
        estiloError: EstiloErrorInput.AMARILLO,
        estiloInput: EstiloInput.LOGIN,
      },
      placeholder: '',
      data: this.loginForm.controls.contrasena,
      contrasena: true,
      bloquearCopy: false,
    };

    this.pensamientoCompartido = {
      tipoPensamiento: TipoPensamiento.PENSAMIENTO_ALEATORIO,
      tituloPensamiento: 'm1v2texto17',
      esLista: false,
      configuracionItem: {
        estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
      },
    };
  }

  async configurarBotones() {
    this.botonSubscribirse = {
      text: 'texto11',
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.CELESTE,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this._location.replaceState('/');
        this.router.navigateByUrl(RutasLocales.MENU_PERFILES.toString());
      },
    };

    this.botonCompartido = {
      text: 'm1v2texto10',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.enviarLanding(),
    };

    this.botonSubmit = {
      text: 'm1v2texto16',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.CELESTEMAIN,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.iniciarSesion(),
    };

    this.confBotonAvisoIdiomas = {
      text: 'm1v2texto6',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.variablesGlobales.mostrarAvisoIdiomas = false;
        this.mostrarBanda = true;
        this.metodosLocalStorageService.guardar(
          LlavesLocalStorage.AVISO_IDIOMAS,
          this.variablesGlobales.mostrarAvisoIdiomas ? 1 : 0
        );
      },
    };
  }

  async validarIdiomaSeleccionado() {
    let idiomasArray = ['en', 'it', 'es', 'fr', 'de', 'pt'];
    let idiomaNavegador = window.navigator.language.substring(0, 2);
    let existe = idiomasArray.includes(idiomaNavegador);
    let idioma: CatalogoIdiomaEntity;
    idioma = {};
    if (existe) {
      let idiomaPrincipal = CodigosCatalogoIdiomaCod[idiomaNavegador];

      idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
        CodigosCatalogoIdioma[idiomaPrincipal]
      );
    } else {
      idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
        CodigosCatalogoIdioma.INGLES
      );
    }
    const idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (!idiomaSeleccionado && idioma) {
      
      this.idiomaNegocio.guardarIdiomaSeleccionado(idioma);
      this.internacionalizacionNegocio.usarIidoma(idioma.codNombre);
    }
  }

  enviarLanding() {
    // this.variablesGlobales.mostrarGif = true
    // setTimeout(() => {
    // 	this.variablesGlobales.mostrarGif = false
    // 	// this.router.navigateByUrl(RutasLocales.LANDING.toString())
    // }, 12000)
    sessionStorage.setItem(
      LlavesSessionStorage.ANIMACION_FINALIZADA,
      '0'
    );
    this.router.navigateByUrl(RutasLocales.LANDING.toString());
    this.variablesGlobales.mostrarGif = false;
  }

  async iniciarSesion() {
    try {
      this.botonSubmit.enProgreso = true;

      if (!this.loginForm.valid) {
        this.botonSubmit.enProgreso = false;
        this.botonSubmit.enProgreso = false;
        this.inputEmail.error = true;
        this.inputContrasena.error = true;
        return;
      }

      const res: UsuarioModel = await this.cuentaNegocio
        .iniciarSesion(
          this.loginForm.value.email.trim().toLowerCase(),
          this.loginForm.value.contrasena.trim()
        )
        .toPromise();

      if (!res) {
        throw new Error('');
      }

      const dataFirebase = await this.auth.signInAnonymously();
      if (!dataFirebase || !dataFirebase.user || !dataFirebase.user.uid) {
        this.cuentaNegocio.cerrarSession();
        throw new Error('');
      }

      this.cuentaNegocio.guardarFirebaseUIDEnLocalStorage(
        dataFirebase.user.uid
      );

      this.loginForm.reset();
      this.navegarmenuSeleccionarPerfiles();
    } catch (error) {
      this.botonSubmit.enProgreso = false;
      this.toast.abrirToast('text7');
    }
  }

  navegarmenuSeleccionarPerfiles() {
    this._location.replaceState('/');
    this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES, {
      replaceUrl: true,
    });
  }

  //Cuando el usuario cambie de idioma los elemento cambia de idioma
  async cambiarIdioma() {
    if (this.pensamiento) {
      this.pensamiento.obtenerPensamientoAleatorio();
    }
  }

  inicializarDialogoRecuperarContrasena(mostrarDialogo: boolean = false) {
    this.confDialogoRecuperarContrasena = {
      mostrar: mostrarDialogo,
      email: '',
      mostrarStatus: false,
      status: '',
      mostrarLoader: false,
      mostrarError: false,
      error: '',
    };
  }

  async recuperarContrasena() {
    if (
      !this.confDialogoRecuperarContrasena.email ||
      !(this.confDialogoRecuperarContrasena.email.length > 0) ||
      !(this.confDialogoRecuperarContrasena.email.indexOf('@') >= 0) ||
      !(this.confDialogoRecuperarContrasena.email.indexOf('.') >= 0)
    ) {
      this.confDialogoRecuperarContrasena.mostrarLoader = false;
      this.confDialogoRecuperarContrasena.mostrarError = true;
      this.confDialogoRecuperarContrasena.error = 'text5';
      return;
    }

    try {
      this.confDialogoRecuperarContrasena.mostrarError = false;
      this.confDialogoRecuperarContrasena.mostrarLoader = true;
      const estatus: string = await this.cuentaNegocio
        .recuperarContrasena(
          this.confDialogoRecuperarContrasena.email.trim().toLowerCase()
        )
        .toPromise();

      if (!estatus) {
        throw new Error('');
      }

      this.confDialogoRecuperarContrasena.mostrarLoader = false;
      this.confDialogoRecuperarContrasena.mostrarStatus = true;
      this.confDialogoRecuperarContrasena.status = 'text65';
    } catch (error) {
      this.confDialogoRecuperarContrasena.mostrarLoader = false;
      this.confDialogoRecuperarContrasena.mostrarError = true;
      this.confDialogoRecuperarContrasena.error = 'text37';
    }
  }

  eventoTapEnModal(target: any) {
    target.classList.forEach((clase: any) => {
      if (
        clase === 'modal-recuperar-contrasena' &&
        !this.confDialogoRecuperarContrasena.mostrarLoader
      ) {
        this.inicializarDialogoRecuperarContrasena(false);
      }
    });
  }

  validarAvisoIdiomasEnLocalStorage() {
    const estado = this.metodosLocalStorageService.obtener(
      LlavesLocalStorage.AVISO_IDIOMAS
    );
    // 1 => true, 0 => false
    if (estado === 0) {
      this.variablesGlobales.mostrarAvisoIdiomas = false;
    }

    this.metodosLocalStorageService.guardar(
      LlavesLocalStorage.AVISO_IDIOMAS,
      this.variablesGlobales.mostrarAvisoIdiomas ? 1 : 0
    );
  }
}

export interface ConfiguracionRecuperarContrasena {
  mostrar?: boolean;
  email?: string;
  mostrarLoader?: boolean;
  mostrarError?: boolean;
  error?: string;
  mostrarStatus?: boolean;
  status?: string;
}
