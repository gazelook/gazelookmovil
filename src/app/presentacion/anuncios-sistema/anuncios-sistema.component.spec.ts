import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnunciosSistemaComponent } from './anuncios-sistema.component';

describe('AnunciosSistemaComponent', () => {
  let component: AnunciosSistemaComponent;
  let fixture: ComponentFixture<AnunciosSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnunciosSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnunciosSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
