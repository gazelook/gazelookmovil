import { CodigosCatalogoTipoAnuncioSistema } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-estilo-anuncio-sistema.enum';
import { CodigosCatalogoTipoAnuncio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-anuncio.enum';
import { AnuncioSistemaModel } from 'dominio/modelo/entidades/anuncio-sistema.model';
import { AnuncioSistemaNegocio } from 'dominio/logica-negocio/anuncio-sistema.negocio';
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { AnuncioPublicitarioCompartido } from '@shared/diseno/modelos/anuncio-publicitario.interface';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { Location } from '@angular/common';

@Component({
	selector: 'app-anuncios-sistema',
	templateUrl: './anuncios-sistema.component.html',
	styleUrls: ['./anuncios-sistema.component.scss']
})
export class AnunciosSistemaComponent implements OnInit {

	public sesionIniciada: boolean
	public perfilSeleccionado: PerfilModel

	public confAppBar: ConfiguracionAppbarCompartida
	public configuracionAnuncioPublicitario: Array<AnuncioPublicitarioCompartido>
	public listaAnuncio: Array<AnuncioSistemaModel>

	public confLineaVerde: LineaCompartida
	public codigoCatalogoTipoAnuncio = CodigosCatalogoTipoAnuncio
	public codigosCatalogoTipoAnuncioSistema = CodigosCatalogoTipoAnuncioSistema

	constructor(
		private cuentaNegocio: CuentaNegocio,
		private perfilNegocio: PerfilNegocio,
		private router: Router,
		private _location: Location,
		private variablesGlobales: VariablesGlobales,
		private anuncioSistemaNegocio: AnuncioSistemaNegocio,
	) {
		this.listaAnuncio = [];
		this.configuracionAnuncioPublicitario = [];
	}

	ngOnInit(): void {
		this.variablesGlobales.mostrarMundo = false
		this.configurarEstadoSesion()
		this.configurarPerfilSeleccionado()
		if (this.perfilSeleccionado) {
			this.configurarAppBar()
			this.configurarLinea()
			this.obtenerAnuncios()
		} else {
			this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
		}
	}

	configurarEstadoSesion() {
		this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
	}

	configurarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
				},
				mostrarTextoHome: true,
				mostrarDivBack: {
					icono: true,
					texto: true,
				},
				mostrarLineaVerde: true,
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm4v5texto29'
				},
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true,
					}
				}
			},
		}
	}

	configurarLinea() {
		this.confLineaVerde = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR089,
		}
	}

	configurarAnuncioSistema(anuncio: AnuncioSistemaModel, espacio: string): AnuncioPublicitarioCompartido {
		let colorFondo = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.FONDO).color;
		let colorTexto = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.LETRA).color;
		let tamanioTexto = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.TAMANIO).tamanioLetra;

		let formatoColor = colorFondo;
		let degradado1: string = ''
		let degradado2: string = ''
		let tipoDegradado: string = ''


		if (anuncio.tipo.codigo === 'TIPANU_01') {
			let colorFondo2 = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.FONDO && e.color.toLowerCase() !== colorFondo.toLowerCase()).color
			formatoColor = 'linear-gradient(90deg, ' + formatoColor + ', ' + colorFondo2 + ')';
			degradado1 = colorFondo
			degradado2 = colorFondo2
			if (degradado1 === "#0C1327" && degradado2 === "#D86A6A") {
				tipoDegradado = 'url(#SVGID_1_)'
			}
			if (degradado1 === "#336699" && degradado2 === "#011348") {
				tipoDegradado = 'url(#SVGID_2_)'
			}
			if (degradado1 === "#011348" && degradado2 === "#E89A63") {
				tipoDegradado = 'url(#SVGID_3_)'
			}


		}

		return {
			titulo: anuncio?.titulo,
			descripcion: anuncio.descripcion,
			fecha: anuncio.fechaInicio,
			tipo: anuncio.tipo,
			colorFondo: colorFondo ? colorFondo : (anuncio.tipo.codigo == 'TIPANU_01' ? '#ff8b8b' : anuncio.tipo.codigo == 'TIPANU_02' ? '#082434' : anuncio.tipo.codigo == 'TIPANU_03' ? '#082434' : 'black'),
			colorTexto: colorTexto ? colorTexto : (anuncio.tipo.codigo == 'TIPANU_01' ? 'white' : anuncio.tipo.codigo == 'TIPANU_02' ? '#9fc5ee' : anuncio.tipo.codigo == 'TIPANU_03' ? 'white' : '#bbb7b7'),
			tamanioLetra: tamanioTexto ? tamanioTexto : 'L4_I1',
			espacio: espacio,
			formatoColor: formatoColor,
			degradado1: degradado1,
			degragado2: degradado2,
			tipoDegradado: tipoDegradado
		}
	}

	obtenerEspacioAnuncio(anuncioActual: AnuncioSistemaModel, anuncioSiguiente: AnuncioSistemaModel) {
		if (anuncioActual.tipo.codigo == 'TIPANU_01') { // ADVERTISEMENT
			return ESPACIO_ANUNCIOS_PUBLICITARIO[anuncioSiguiente.tipo.codigo];
		} else if (anuncioActual.tipo.codigo == 'TIPANU_02' || anuncioActual.tipo.codigo == 'TIPANU_04') { // INFORMATION
			return ESPACIO_ANUNCIOS_INFO_FANANZA[anuncioSiguiente.tipo.codigo];
		} else if (anuncioActual.tipo.codigo == 'TIPANU_03') { // REFLEXTION
			return ESPACIO_ANUNCIOS_REFLEXIVO[anuncioSiguiente.tipo.codigo];
		}
	}

	async obtenerAnuncios() {
		this.listaAnuncio = await this.anuncioSistemaNegocio.obtenerAnunciosSistema(this.perfilSeleccionado._id).toPromise();
		for (let i = 0; i < this.listaAnuncio.length; i++) {
			const anuncio = this.listaAnuncio[i];
			let espacio = '0vw';

			if (i < this.listaAnuncio.length - 1) {
				espacio = this.obtenerEspacioAnuncio(anuncio, this.listaAnuncio[i + 1]);
			}

			this.configuracionAnuncioPublicitario.push(this.configurarAnuncioSistema(anuncio, espacio));

		}
	}

}

const ESPACIO_ANUNCIOS_PUBLICITARIO = {
	'TIPANU_01': '7vw',
	'TIPANU_02': '7vw',
	'TIPANU_03': '7vw',
	'TIPANU_04': '7vw',
}

const ESPACIO_ANUNCIOS_INFO_FANANZA = {
	'TIPANU_01': '7vw',
	'TIPANU_02': '7vw',
	'TIPANU_03': '7vw',
	'TIPANU_04': '7vw',
}

const ESPACIO_ANUNCIOS_REFLEXIVO = {
	'TIPANU_01': '7vw',
	'TIPANU_02': '7vw',
	'TIPANU_03': '7vw',
	'TIPANU_04': '7vw',
}
