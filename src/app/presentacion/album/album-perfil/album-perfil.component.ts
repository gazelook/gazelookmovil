import { Location } from '@angular/common';
import { AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { AlbumService } from '@core/servicios/generales/album.service';
import { CamaraService } from '@core/servicios/generales/camara.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoMedia } from '@core/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { AccionAlbum, CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoArchivo } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-archivo.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { ConvertidorArchivos } from '@core/util/caster-archivo.service';
import { TranslateService } from '@ngx-translate/core';
import { AppbarComponent } from '@shared/componentes/appbar/appbar.component';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { CamaraComponent } from '@shared/componentes/camara/camara.component';
import { CropperComponent } from '@shared/componentes/cropper/cropper.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AccionesItemCircularRectangular } from '@shared/diseno/enums/acciones-general.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
	ColorDeBorde,
	ColorDeFondo, ColorFondoLinea,
	EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ColorCapaOpacidadItem } from '@shared/diseno/enums/item-cir-rec-capa-opacidad.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { ConfiguracionCamara, ConfiguracionCropper } from '@shared/diseno/modelos/foto-editor.interface';
import { InfoAccionCirRec } from '@shared/diseno/modelos/info-accion-cir-rec.interface';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { AlbumParams } from 'dominio/modelo/parametros/album-parametros.interface';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AccionEntidad } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';

@Component({
	selector: 'app-album-perfil',
	templateUrl: './album-perfil.component.html',
	styleUrls: ['./album-perfil.component.scss']
})
export class AlbumPerfilComponent implements OnInit, AfterViewInit, OnDestroy {
	@ViewChild('appbar', { static: false }) appbar: AppbarComponent
	@ViewChild('camara', { static: false }) camara: CamaraComponent
	@ViewChild('cropper', { static: false }) cropper: CropperComponent
	@ViewChild('toast', { static: false }) toast: ToastComponent

	// Utils
	public accionAlbumEnum = AccionAlbum
	public codigosTipoPerfil = CodigosCatalogoTipoPerfil
	public eventoEnitemFuncion: Function
	public observable: any
	public AccionEntidadEnum = AccionEntidad

	// Parametros de url
	public params: AlbumParams

	// Parametros internos
	public haySesionIniciada: boolean
	public perfilSeleccionado: PerfilModel
	public album: AlbumModel // Album en uso
	public cantidadMaximaFotos: number
	public listaMediaAlbum: Array<MediaModel> // Para almacenar las medias a devolver en caso de crear
	public itemsAlbum: Array<ItemCircularCompartido> // Array de items compartido
	public itemsAlbumPorDefecto: Array<ItemCircularCompartido> // Array de items compartido - fotos por defecto
	public cantidadItemsPorDefecto: number // Numero total de items por defecto a mostrar
	public confBotonSubmit: BotonCompartido
	// Configuraciones
	public confCamara: ConfiguracionCamara // Configuracion camara
	public confCropper: ConfiguracionCropper // Configuracion del cropper de imagenes
	public confToast: ConfiguracionToast // Configuracion del toast
	public confAppBar: ConfiguracionAppbarCompartida // Configuracion appbar compartida
	public confPortada: ItemCircularCompartido // Portada del album
	public confBotonUpload: ItemCircularCompartido // Boton siempre visible de upload photos
	public confLinea: LineaCompartida // Linea compartida
	public confDone: ConfiguracionDone

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private variablesGlobales: VariablesGlobales,
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private mediaNegocio: MediaNegocio,
		private convertidorArchivos: ConvertidorArchivos,
		private generadorId: GeneradorId,
		private perfilNegocio: PerfilNegocio,
		private albumNegocio: AlbumNegocio,
		private _location: Location,
		private albumService: AlbumService,
		private cuentaNegocio: CuentaNegocio,
		private translateService: TranslateService,
		private camaraService: CamaraService
	) {
		this.haySesionIniciada = false
		this.params = { estado: false }
		this.cantidadMaximaFotos = 6
		this.listaMediaAlbum = []
		this.itemsAlbum = []
		this.itemsAlbumPorDefecto = []
		this.cantidadItemsPorDefecto = 5
		this.eventoEnitemFuncion = (data: InfoAccionCirRec) => {	
			this.eventoEnItem(data)
		}
	}

	ngOnInit(): void {
		this.variablesGlobales.mostrarMundo = false
		this.configurarParametrosDeUrl()
		this.validarSessionIniciada()
		this.inicializarPerfilSeleccionado()
		this.configurarBoton()
		this.configurarEscuchaCamaraService()
		if (this.params.estado && ((this.haySesionIniciada
      && this.perfilSeleccionado) || (!this.haySesionIniciada))) {
			this.inicializarDataAlbum()
			this.configurarToast()
			this.configurarAppBar()
			this.configurarPortada()
			this.configurarLinea()
			this.confgurarBotonUploadPhotos()
			this.configurarItemsAlbumPorDefecto()
			this.configurarItemsAlbum()
			this.configurarDone()

			// En caso la pagina sea recargada, se guarda el estado del album en el session sotarage
			window.onbeforeunload = () => this.guardarAlbumAntesDeSalirReload()
			return
		}

		if (this.haySesionIniciada) {
			this.perfilNegocio.validarEstadoDelPerfil(
				this.perfilSeleccionado,
				this.params.estado
			)
		} else {
			this._location.back()
		}
	}

	configurarEscuchaCamaraService() {
		this.camaraService.suscripcionFotoCortada$ = this.camaraService.
    escuchaFotoCortada$.subscribe(imagen => {
			this.subirArchivoAlServidor(imagen)
		})
	}
	ngAfterViewInit(): void {  }

	ngOnDestroy(): void { }


	// Escucha para el boton de back del navegador
	@HostListener('window:popstate', ['$event'])
	onPopState(event: any) {
		this.albumNegocio.removerAlbumSegunElEstado(
			this.params,
			this.album,
			CodigosCatalogoEstadoAlbum.SIN_CREAR
		)
	}

	async configurarBoton() {
		this.confBotonSubmit = {
			text: 'm2v3texto20',
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			colorTexto: ColorTextoBoton.AMARRILLO,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {
				this.accionAtrasAppBarBack()
			}
		}
	}

	configurarParametrosDeUrl() {
		const urlParams: Params = this.route.snapshot.params
		this.params.accionEntidad = this.albumService.definirAccionDelAmbumSegunUrl(this.router.url)

		if (this.params.accionEntidad) {
			this.params = this.albumService.validarParametrosDelAlbumSegunAccionEntidad(this.params, urlParams)
		}
	}

	// Inicializar perfil seleccionado
	inicializarPerfilSeleccionado() {
		this.perfilSeleccionado = (this.haySesionIniciada) ?
			this.perfilNegocio.obtenerPerfilSeleccionado() :
			this.perfilNegocio.obtenerPerfilActivoDelSessionStorage()
	}

	validarSessionIniciada() {
		this.haySesionIniciada = this.cuentaNegocio.sesionIniciada()
	}

	inicializarDataAlbum() {
		// Validar la entidad a la que pertenece el album
		this.album = this.albumNegocio.obtenerAlbumActivoDelSessionStorage()

		// Si el album no existe, se ejecuta el back
		if (!this.album) {
			// this._location.back()
		}
	}

	configurarItemsAlbum() {
		// Items para el album
		if (this.album) {
			this.album.media.forEach(media => {
				this.itemsAlbum.push({
					id: media.id,
					idInterno: this.generadorId.generarIdConSemilla(),
					usoDelItem: UsoItemCircular.CIRALBUM,
					esVisitante: false,
					urlMedia: media.principal.url,
					activarClick: false,
					activarDobleClick: true,
					activarLongPress: true,
					mostrarBoton: false,
					mostrarLoader: true,
					fotoPredeterminadaRamdon: this.determinarOrigenRamdomDeLaPortada(),
					textoBoton: 'Click to upload',
					capaOpacidad: {
						mostrar: false
					},
					eventoEnItem: this.eventoEnitemFuncion, 
					esBotonUpload: false,
					colorBorde: ColorDeBorde.BORDER_ROJO,
					colorDeFondo: ColorDeFondo.FONDO_BLANCO,
					mostrarCorazon: false,
				})
			})
			// Definir la portada
			if (this.album.portada && this.album.portada.principal &&
        this.album.portada.principal.url.length > 0) {
				const portada: MediaModel = this.album.portada
				this.confPortada.mostrarLoader = true
				this.confPortada.id = portada.id
				this.confPortada.urlMedia = portada.principal.url
				this.confPortada.mostrarBoton = false
			}
		}
	}

	configurarDone() {
		this.confDone = {
			intervalo: 4000,
			mostrarDone: false,
			mostrarLoader: false
		}
	}

	determinarOrigenRamdomDeLaPortada() {
		let origen = false
		if (this.params.entidad === CodigosCatalogoEntidad.PERFIL) {
			if (this.perfilSeleccionado.tipoPerfil.codigo === CodigosCatalogoTipoPerfil.PLAYFUL) {
				origen = true
			}
		}
		return origen
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false,
			mostrarLoader: false,
			cerrarClickOutside: false,
			texto: ''
		}
	}

	configurarAppBar() {
		const dataAppBar = this.albumNegocio.determinarTextosAppBarSegunEntidad(
			this.params.entidad,
			this.perfilSeleccionado,
			CodigosCatalogoTipoAlbum.GENERAL
		)

		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true
					}
				},
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
				},
				mostrarDivBack: {
					icono: true,
					texto: this.haySesionIniciada ? true : false,
				},
				mostrarTextoHome: this.haySesionIniciada ? true : false,
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm2v14texto1'
				},
				mostrarLineaVerde: true,
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
			},
			accionAtras: () => {
				this.accionAtrasAppBarBack(false)
			}
		}
	}

	async configurarPortada() {
		this.confPortada = {
			id: '',
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemCircular.CIRPERFIL,
			esVisitante: true,
			urlMedia: '',
			activarClick: false,
			activarDobleClick: false,
			activarLongPress: false,
			mostrarBoton: true,
			mostrarLoader: false,
			textoBoton: ' ',
			eventoEnItem: this.eventoEnitemFuncion,
			capaOpacidad: {
				mostrar: false
			},
			esBotonUpload: false,
			colorBorde: ColorDeBorde.BORDER_ROJO,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
		}

		if (this.perfilSeleccionado.tipoPerfil.codigo !== CodigosCatalogoTipoPerfil.PLAYFUL) {
			const texto = await this.translateService.get('m2v17texto2').toPromise()
			const texto2 = await this.translateService.get('m2v17texto3').toPromise()
			this.confPortada.textoBoton = texto + '\n' + texto2
		}
	}

	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR041,
			forzarAlFinal: false
		}
	}

	async confgurarBotonUploadPhotos() {
		this.confBotonUpload = {
			id: 'botonUpload',
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemCircular.CIRALBUM,
			esVisitante: false,
			urlMedia: '',
			activarClick: true,
			activarDobleClick: false,
			activarLongPress: false,
			mostrarBoton: true,
			mostrarLoader: false,
			textoBoton: 'Click to upload',
			eventoEnItem: this.eventoEnitemFuncion,
			capaOpacidad: {
				mostrar: false
			},
			esBotonUpload: true,
			colorBorde: ColorDeBorde.BORDER_ROJO,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
		}

		const texto: string = await this.translateService.get('m2v14texto7').toPromise()
		const texto2: string = await this.translateService.get('m2v14texto8').toPromise()
		const texto3: string = await this.translateService.get('m2v14texto9').toPromise()
		this.confBotonUpload.textoBoton = texto + '\n' + texto2 + '\n' + texto3
	}

	validarDivisorGrilla(pos: number) {
		if (pos === 0 || pos % 2 === 0) {
			return false
		}

		return true
	}

	obtenerColorCapaOpacidad(pos: number) {
		// Opacidad C
		if (pos === 0 || pos === 3) {
			return ColorCapaOpacidadItem.CAPA_OPACIDAD_C
		}
		// Opacidad B
		if (pos === 1 || pos === 2) {
			return ColorCapaOpacidadItem.CAPA_OPACIDAD_B
		}
		// Opacidad A
		return ColorCapaOpacidadItem.CAPA_OPACIDAD_A
	}

	configurarItemsAlbumPorDefecto() {
		let pos = 0
		while (pos < this.cantidadItemsPorDefecto) {
			// Llenar item
			this.itemsAlbumPorDefecto.push(
				{
					id: 'itemFotoDefecto_' + pos,
					idInterno: this.generadorId.generarIdConSemilla(),
					usoDelItem: UsoItemCircular.CIRALBUM,
					esVisitante: true,
					urlMedia: '',
					activarClick: false,
					activarDobleClick: false,
					activarLongPress: false,
					mostrarBoton: false,
					mostrarLoader: true,
					textoBoton: 'Click to upload',
					capaOpacidad: {
						mostrar: true,
						colorOpacidad: this.obtenerColorCapaOpacidad(pos)
					},
					esBotonUpload: false,
					colorBorde: ColorDeBorde.BORDER_ROJO,
					colorDeFondo: ColorDeFondo.FONDO_TRANSPARENTE,
					mostrarCorazon: false,
				}
			)
			pos += 1
		}

		pos = 0
		this.mediaNegocio.obtenerListaArchivosDefault().subscribe(data => {
			// Validar si existen datos
			data.forEach((item) => {
				if (item.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.ALBUM_PERFIL) {
					this.itemsAlbumPorDefecto[pos].urlMedia = item.url
					pos += 1
				}
			})

			if (pos === 0) {
				// Ocultar loader cuando no existe datos
				this.itemsAlbumPorDefecto.forEach(item => {
					item.mostrarLoader = false
				})
			}
		}, error => {
			this.itemsAlbumPorDefecto = []
		})
	}

	subirArchivoAlServidor(event: ImageCroppedEvent) {
		const imagen = this.convertidorArchivos.dataURItoBlob(event.base64)
		const idItem: string = this.generadorId.generarIdConSemilla()
		this.itemsAlbum.push({
			id: idItem,
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: UsoItemCircular.CIRALBUM,
			esVisitante: false,
			urlMedia: '',
			activarClick: false,
			activarDobleClick: true,
			activarLongPress: true,
			mostrarBoton: false,
			mostrarLoader: true,
			fotoPredeterminadaRamdon: this.determinarOrigenRamdomDeLaPortada(),
			textoBoton: 'Click to upload',
			capaOpacidad: {
				mostrar: false
			},
			eventoEnItem: this.eventoEnitemFuncion,
			esBotonUpload: false,
			colorBorde: ColorDeBorde.BORDER_ROJO,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			mostrarCorazon: false,
		})

		const data: SubirArchivoData = {
			archivo: imagen,
			formato: 'image/jpeg',
			relacionAspecto: '1:1',
			descripcion: '',
			catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
		}

		this.subirMedia(data, idItem)
	}

	async subirMedia(
		data: SubirArchivoData,
		idItem: string
	) {
		try {
			const media: MediaModel = await this.albumNegocio.subirMedia(
        data, this.params, this.album)
			const pos = this.obtenerPosicionPorIdItem(idItem)
			if (media) {
				this.album.media.push(media)
				// Actualizar data en vista
				if (pos >= 0) {
					this.itemsAlbum[pos].id = media.id
					this.itemsAlbum[pos].urlMedia = media.principal.url
				}
			} else {
				this.itemsAlbum.splice(pos, 1)
				this.toast.abrirToast('text37')
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	borrarMediaDelLocal(data: InfoAccionCirRec) {
		let pos = -1
		this.itemsAlbum.forEach((item, i) => {
			if (item.id === data.informacion.id) {
				pos = i
			}
		})
		// Validar si el item de la portada fue borrado
		if (this.itemsAlbum[pos].id === this.confPortada.id) {
			this.confPortada.id = ''
			this.confPortada.urlMedia = ''
			this.confPortada.mostrarLoader = false
			this.confPortada.mostrarBoton = true

			// Actualizar el album
			this.album.portada.id = ''
			this.album.portada.principal.id = ''
			this.album.portada.principal.url = ''
		}
		this.album.media.splice(pos, 1)
		this.itemsAlbum.splice(pos, 1)
	}

	async borrarMediaDelApi(data: InfoAccionCirRec) {
		if (!(data.informacion.id && data.informacion.id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.borrarMedia(
        data.informacion.id, this.params, this.album)


			if (!status) {
				throw new Error('')
			}

			this.borrarMediaDelLocal(data)

			if (this.album.media.length === 0) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this.albumNegocio.removerAlbumActivoDelSessionStorage()
					this._location.back()
				})
			}
		} catch (error) {

			this.toast.abrirToast('text37')
		}
	}

	validarAccionEntidadParaBorrarItemDelAlbum(data: InfoAccionCirRec) {
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				this.borrarMediaDelLocal(data)
				break
			case AccionEntidad.ACTUALIZAR:
				this.borrarMediaDelApi(data)
				break
			default: break;
		}
	}

	determinarAccionParaPortada(data: InfoAccionCirRec): string {
		if (this.confPortada.id !== data.informacion.id) {
			return 'asignar'
		}

		if (this.confPortada.id === data.informacion.id) {
			return 'remover'
		}

		return null
	}

	establecerPortadaEnLocal(data: InfoAccionCirRec, accion: string) {
		switch (accion) {
			case 'asignar':
				this.confPortada.mostrarBoton = false
				this.confPortada.id = data.informacion.id
				this.confPortada.mostrarLoader = true
				this.confPortada.urlMedia = data.informacion.urlMedia
				// Portada en item album
				const portadaMedia: MediaModel = this.obtenerMediaPorIdItem(
          this.confPortada.id)

				// Definir tipo a la portada
				portadaMedia.principal.tipo = {
					codigo: CodigosCatalogoTipoArchivo.IMAGEN
				}
				// Setear portada en el album activo
				this.album.portada = {
					...portadaMedia,
					catalogoMedia: {
						codigo: CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO
					},
				}
				break
			case 'remover':
				this.confPortada.id = ''
				this.confPortada.urlMedia = ''
				this.confPortada.mostrarLoader = false
				this.confPortada.mostrarBoton = true
				// Setear portada en el album activo
				this.album.portada = {}
				break
			default: break;
		}
	}

	async asignarPortada(data: InfoAccionCirRec, accion: string) {
		if (!(data.informacion && data.informacion.id &&
        data.informacion.id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.asignarPortada(
        data.informacion.id, this.params, this.album)
			if (status) {
				this.establecerPortadaEnLocal(data, accion)
			} else {
				this.toast.abrirToast('text37')
			}

		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}




	async removerPortada(data: InfoAccionCirRec, accion: string) {
		if (!(data.informacion && data.informacion.id &&
      data.informacion.id.length > 0)) {
			return
		}

		try {
			const status: boolean = await this.albumNegocio.removerPortada(
        data.informacion.id, this.params, this.album)

			if (status) {
				this.establecerPortadaEnLocal(data, accion)
			} else {
				this.toast.abrirToast('text37')
			}
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	establecerPortadaEnElApi(
		data: InfoAccionCirRec,
		accion: string
	) {
		switch (accion) {
			case 'asignar':
				this.asignarPortada(data, accion)
				break
			case 'remover':
				this.removerPortada(data, accion)
				break
			default: break;
		}
	}

	validarAccioParaEstablecerPortada(data: InfoAccionCirRec) {
		const accion: string = this.determinarAccionParaPortada(data)
		switch (this.params.accionEntidad) {
			case AccionEntidad.CREAR:
				if (data.informacion && data.informacion.id &&
          data.informacion.id.length > 0) {
					this.establecerPortadaEnLocal(data, accion)
				}
				break
			case AccionEntidad.ACTUALIZAR:
				this.establecerPortadaEnElApi(data, accion)
				break
			default: break;
		}
	}

	// Evento en items
	eventoEnItem(data: InfoAccionCirRec) {
		// Tomar Foto
		if (data.accion === AccionesItemCircularRectangular.TOMAR_FOTO) {

			if (this.itemsAlbum.length >= this.cantidadMaximaFotos) {
				this.toast.abrirToast('text63')
				return
			}

			this.camaraService.reiniciarCamara()
			this.camaraService.cambiarEstadoCamara(true, true)
			return
		}

		// Subir archivo
		if (data.accion === AccionesItemCircularRectangular.SUBIR_ARCHIVO) {
			if (this.itemsAlbum.length < this.cantidadMaximaFotos) {
				this.camaraService.configurarCropper(true, 'imageFile', data.informacion.archivo[0])
			} else {
				this.toast.abrirToast('text63')
			}
			return
		}

		// Borrar item
		if (data.accion === AccionesItemCircularRectangular.BORRAR_ITEM) {
			this.validarAccionEntidadParaBorrarItemDelAlbum(data)
			return
		}

		// Establecer foto por defecto
		if (data.accion === AccionesItemCircularRectangular.ESTABLECER_ITEM_PREDETERMINADO) {
			this.validarAccioParaEstablecerPortada(data)
			return
		}
	}

	obtenerPosicionPorIdItem(id: string) {
		let pos = -1
		this.itemsAlbum.forEach((item, i) => {
			if (item.id === id) {
				pos = i
			}
		})
		return pos
	}

	obtenerMediaPorIdItem(id: string): MediaModel {
		let media = null
		this.album.media.forEach((item, i) => {
			if (item.id === id) {
				media = item
			}
		})
		return media
	}

	accionAtrasAppBarBack(
		esDelSubmit: boolean = true
	) {
		this.guardarAlbumAntesDeSalir(esDelSubmit, true)
	}

	async guardarAlbumAntesDeSalir(
		esDelSubmit: boolean,
		destruirAlbum: boolean = false
	) {
		try {
			let enviarCrearAlbum = false
			const idEntidad: string = this.albumNegocio.
      obtenerIdDeLaEntidadSegunCodigo(this.params.entidad)

			if (
				this.params.accionEntidad === AccionEntidad.CREAR &&
				idEntidad &&
				idEntidad.length > 0 &&
				this.album.media.length > 0
			) {
				enviarCrearAlbum = true
			}

			if (enviarCrearAlbum) {

				const albumCreado: AlbumModel = await this.albumNegocio.
        agregarAlbumEnEntidad(
					idEntidad,
					this.params.entidad,
					this.album
				).toPromise()

				if (!albumCreado) {
					throw new Error('')
				}

				this.album._id = albumCreado._id
			}

			this.album = this.albumNegocio.actualizarEstadoDelAlbum(
        this.album, CodigosCatalogoEstadoAlbum.CREADO)
			this.albumNegocio.validarActualizacionDelAlbumSegunParams(
        this.album, this.params, destruirAlbum)
			this.toast.cerrarToast()

			if (esDelSubmit) {
				this.confDone.mostrarDone = true
				setTimeout(() => {
					this._location.back()
				}, 600)
			}

			if (!esDelSubmit) {
				this._location.back()
			}
		} catch (error) {
			this.toast.abrirToast('text36')
		}
	}

	guardarAlbumAntesDeSalirReload(
		destruirAlbum: boolean = false
	) {
		this.albumNegocio.validarActualizacionDelAlbumSegunParams(
      this.album, this.params, destruirAlbum)
	}
}
