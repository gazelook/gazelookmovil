import { Component, OnInit } from '@angular/core'
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service'
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service'

@Component({
	selector: 'app-album-general',
	templateUrl: './album.component.html',
	styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

	constructor(
		private notificacionesUsuario: NotificacionesDeUsuario,
		// private llamadaFirebaseService: LlamadaFirebaseService
	) { 	}

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
		// this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
	}
}