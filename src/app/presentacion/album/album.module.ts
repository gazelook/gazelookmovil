import { AlbumComponent } from 'src/app/presentacion/album/album.component';
import { AlbumGeneralComponent } from 'src/app/presentacion/album/album-general/album-general.component';
import { AlbumRoutingModule } from 'src/app/presentacion/album/album-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from '@shared/compartido.module';
import { FormsModule } from '@angular/forms';
import { AlbumAudiosComponent } from 'src/app/presentacion/album/album-audios/album-audios.component';
import { AlbumLinksComponent } from 'src/app/presentacion/album/album-links/album-links.component';
import { AlbumPerfilComponent } from 'src/app/presentacion/album/album-perfil/album-perfil.component';


@NgModule({
  declarations: [
    AlbumComponent,
    AlbumGeneralComponent,
    AlbumAudiosComponent,
    AlbumLinksComponent,
    AlbumPerfilComponent,
  ],
  imports: [
    TranslateModule,
    CommonModule,
    AlbumRoutingModule,
    CompartidoModule,
    FormsModule
  ],
  exports: [
    TranslateModule,
    CompartidoModule,
    FormsModule
  ]
})
export class AlbumModule { }
