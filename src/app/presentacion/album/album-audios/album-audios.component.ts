import { Location } from '@angular/common';
import { AfterViewChecked, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { DireccionDelReproductor } from '@shared/diseno/enums/audio-reproductor.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoIconoBarraInferior } from '@shared/diseno/enums/tipo-icono.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { AlbumService } from '@core/servicios/generales/album.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import {
  ColorDeFondo, ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ConfiguracionAudioReproductor } from '@shared/diseno/modelos/audio-reproductor.interface';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { ConfiguracionModalTituloAudio } from '@shared/diseno/modelos/modal-titulo-audio.interface';
import { AlbumParams } from 'dominio/modelo/parametros/album-parametros.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';

@Component({
  selector: 'app-album-audios',
  templateUrl: './album-audios.component.html',
  styleUrls: ['./album-audios.component.scss']
})
export class AlbumAudiosComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild('toast', { static: false }) toast: ToastComponent

  // Params
  public params: AlbumParams

  // Utils
  public activarScrollAlFinal: boolean
  public AccionEntidadEnum = AccionEntidad

  // Parametros internos
  public perfilSeleccionado: PerfilModel
  public album: AlbumModel
  public dataApiArchivo: SubirArchivoData
  public idMediaActivo: string

  // Configuraciones
  public confAppBar: ConfiguracionAppbarCompartida
  public confToast: ConfiguracionToast
  public confLineaBlanca: LineaCompartida
  public confLineaVerde: LineaCompartida
  public confBarraInferior: ConfiguracionBarraInferiorInline
  public listaConfAudioReproductor: Array<ConfiguracionAudioReproductor>
  public confModalTituloAudio: ConfiguracionModalTituloAudio
  public confBotonSubmit: BotonCompartido
  public confDone: ConfiguracionDone

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private albumService: AlbumService,
    private albumNegocio: AlbumNegocio,
    private mediaNegocio: MediaNegocio,
    private perfilNegocio: PerfilNegocio,
    private generadorId: GeneradorId,
    private translateService: TranslateService,
  ) {
    this.params = { estado: false }
    this.listaConfAudioReproductor = []
    this.idMediaActivo = ''
    this.activarScrollAlFinal = false
  }

  ngOnInit(): void {
    this.configurarParametrosDeUrl()
    this.inicializarPerfilSeleccionado()
    if (this.params.estado && this.perfilSeleccionado) {
      this.inicializarDataAlbum()
      this.configurarMediasDelAlbum()
      this.configurarToast()
      this.configurarAppBar()
      this.configurarLineas()
      this.configurarBarraInferior()
      this.configurarModalTituloAudio()
      this.configurarBoton()
      this.configurarDone()
      // En caso la pagina sea recargada, se guarda el estado del album en el session storage sotarage
      window.onbeforeunload = () => this.guardarAlbumAntesDeSalirReload()
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(
        this.perfilSeleccionado,
        this.params.estado
      )
    }
  }

  ngOnDestroy(): void {

  }

  // Escucha para el boton de back del navegador
  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    this.albumNegocio.removerAlbumSegunElEstado(
      this.params,
      this.album,
      CodigosCatalogoEstadoAlbum.SIN_CREAR
    )
  }

  ngAfterViewChecked(): void {
    this.moverScrollDelAListaDeAudiosAlFinal()
  }

  // Parametros de url
  configurarParametrosDeUrl() {
    const urlParams: Params = this.route.snapshot.params
    this.params.accionEntidad = this.albumService.definirAccionDelAmbumSegunUrl(this.router.url)
    if (this.params.accionEntidad) {
      this.params = this.albumService.validarParametrosDelAlbumSegunAccionEntidad(this.params, urlParams)
    }
  }

  // Inicializar perfil seleccionado
  inicializarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
  }

  inicializarDataAlbum() {
    // Validar la entidad a la que pertenece el album
    this.album = this.albumNegocio.obtenerAlbumActivoDelSessionStorage()
    // Si el album no existe, se ejecuta el back
    if (!this.album) {
      this._location.back()
    }
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    }
  }

  configurarAppBar() {
    // Determinar textos del appbar segun entidad
    const dataAppBar = this.albumNegocio.determinarTextosAppBarSegunEntidad(
      this.params.entidad,
      this.perfilSeleccionado,
      CodigosCatalogoTipoAlbum.AUDIOS
    )

    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        nombrePerfil: {
          mostrar: true,
          llaveTexto: dataAppBar.nombrePerfil
        },
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarTextoHome: dataAppBar.mostrarTextoHome,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm4v7texto1'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      },
      accionAtras: () => this.accionAtrasAppBarBack(false)
    }
  }

  configurarLineas() {
    this.confLineaBlanca = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEACELESTE,
      espesor: EspesorLineaItem.ESPESOR012,
      forzarAlFinal: false
    }

    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR012,
      forzarAlFinal: false
    }
  }

  configurarMediasDelAlbum() {
    if (this.album) {
      this.album.media.forEach(media => {
        const item: ConfiguracionAudioReproductor = {
          idInterno: 'audio_' + this.generadorId.generarIdConSemilla(),
          mostrarTitulo: true,
          colorDeFondo: ColorDeFondo.FONDO_AMARILLO_CLARO,
          direccion: DireccionDelReproductor.HACIA_LA_DERECHA,
          reproduciendo: false,
          mostrarLoader: true,
          media: media,
          eventoDobleTap: (id: string) => this.eventoDobleTapEnReproductor(id),
          eventoPress: (id: string) => this.validarAccionEntidadParaBorrarItemDelAlbum(id),
        }
        this.listaConfAudioReproductor.push(item)
      })
    }
  }

  async configurarModalTituloAudio() {
    this.confModalTituloAudio = {
      mostrar: false,
      desactivarCerrarClickAfuera: false,
      textoTitulo: {
        valor: '',
        placeholder: '',
        contadorCaracteres: {
          mostrar: false,
          maximo: 75,
          contador: 0
        },
      },
      evento: (descripcion: string) => {
        this.validarAccionEntidadParaActualizarDescripcion(descripcion)
      }
    }

    this.confModalTituloAudio.textoTitulo.placeholder = await this.translateService.get('m4v7texto6').toPromise()
  }

  async configurarBoton() {
    this.confBotonSubmit = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.accionAtrasAppBarBack()
      }
    }
  }

  configurarDone() {
    this.confDone = {
      intervalo: 4000,
      mostrarDone: false,
      mostrarLoader: false
    }
  }

  capturarErrorBarraInferior(error: string) {
  }

  configurarBarraInferior() {
    this.confBarraInferior = {
      desactivarBarra: false,
      capaColorFondo: {
        mostrar: true,
        anchoCapa: TamanoColorDeFondo.TAMANO100,
        // colorDeFondo: ColorDeFondo.FONDO_AMARILLO_CLARO
      },
      estatusError: (error: string) => this.capturarErrorBarraInferior(error),
      iconoAudio: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_AUDIO,
          eventoTap: (dataApiArchivo: SubirArchivoData) => this.subirMediaAlApi(dataApiArchivo),
        },
        capa: {
          siempreActiva: true,
          grabadora: {
            usarLoader: true,
            grabando: false,
            duracionActual: 0,
            tiempoMaximo: 600,
            factorAumentoLinea: 100 / 600, // 100% del ancho divido para tiempoMaximo
          }
        }
      },
    }
  }

  subirMediaAlAlbumLocal(media: MediaModel) {
    const item: ConfiguracionAudioReproductor = {
      media: media,
      idInterno: 'audio_' + this.generadorId.generarIdConSemilla(),
      mostrarTitulo: true,
      colorDeFondo: ColorDeFondo.FONDO_AMARILLO_CLARO,
      direccion: DireccionDelReproductor.HACIA_LA_DERECHA,
      reproduciendo: false,
      mostrarLoader: true,
      eventoDobleTap: (id: string) => this.eventoDobleTapEnReproductor(id),
      eventoPress: (id: string) => this.validarAccionEntidadParaBorrarItemDelAlbum(id),
    }

    this.album.media.push(media)
    this.listaConfAudioReproductor.push(item)
    this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader = false
    this.activarScrollAlFinal = true
  }

  eventoTapEnReproductor() {  }

  eventoDobleTapEnReproductor(id: string) {
    if (this.params.accionEntidad === AccionEntidad.VISITAR) {
      return
    }

    this.idMediaActivo = id
    const pos = this.albumService.obtenerMediaDeLaListaDeMedias(id, this.album.media)
    if (pos >= 0 && this.idMediaActivo.length > 0) {
      const media: MediaModel = this.album.media[pos]
      this.confModalTituloAudio.textoTitulo.valor = media.descripcion
      this.confModalTituloAudio.mostrar = true
    }
  }

  borrarMediaDelLocal(id: string) {
    let pos = -1
    this.album.media.forEach((item, i) => {
      if (item.id === id) {
        pos = i
      }
    })

    if (pos >= 0) {
      this.album.media.splice(pos, 1)
    }

    pos = -1
    this.listaConfAudioReproductor.forEach((conf, j) => {
      if (conf.media.id === id) {
        pos = j
      }
    })

    if (pos >= 0) {
      this.listaConfAudioReproductor.splice(pos, 1)
    }
  }

  validarAccionEntidadParaBorrarItemDelAlbum(id: string) {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.borrarMediaDelLocal(id)
        break
      case AccionEntidad.ACTUALIZAR:
        this.borrarMediaDelApi(id)
        break
      default: break;
    }
  }

  validarAccionEntidadParaActualizarDescripcion(descripcion: string) {
    if (!(this.idMediaActivo.length > 0)) {
      return
    }

    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.actualizarDescripcionEnLaMediaLocal(descripcion)
        break
      case AccionEntidad.ACTUALIZAR:
        this.actualizarDescripcionEnLaMediaDelApi(descripcion)
        break
      default: break;
    }
  }

  actualizarDescripcionEnLaMediaLocal(descripcion: string) {
    const pos = this.albumService.obtenerMediaDeLaListaDeMedias(this.idMediaActivo, this.album.media)
    if (pos >= 0) {
      const media: MediaModel = this.album.media[pos]
      media.descripcion = descripcion
      this.album.media[pos] = media

      this.listaConfAudioReproductor.forEach(item => {
        if (item.media.id === this.idMediaActivo) {
          item.media.descripcion = descripcion
        }
      })

      this.idMediaActivo = ''
      this.confModalTituloAudio.mostrar = false
    }
  }

  moverScrollDelAListaDeAudiosAlFinal() {
    if (this.activarScrollAlFinal) {
      const elemento: HTMLElement = document.getElementById('listaDeAudios') as HTMLElement
      elemento.scrollTop = elemento.scrollHeight
      this.activarScrollAlFinal = false
    }
  }

  async subirMediaAlApi(data: SubirArchivoData) {
    try {
      const media: MediaModel = await this.albumNegocio.subirMedia(data, this.params, this.album)

      if (media) {
        this.subirMediaAlAlbumLocal(media)
      } else {
        this.toast.abrirToast('text33')
        this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader = false
      }
    } catch (error) {
      this.toast.abrirToast('text33')
      this.confBarraInferior.iconoAudio.capa.grabadora.mostrarLoader = false
    }
  }

  async borrarMediaDelApi(id: string) {
    if (!(id && id.length > 0)) {
      return
    }

    try {
      const status: boolean = await this.albumNegocio.borrarMedia(id, this.params, this.album)

      if (!status) {
        throw new Error('')
      }

      this.borrarMediaDelLocal(id)

      if (this.album.media.length === 0) {
        this.confDone.mostrarDone = true
        setTimeout(() => {
          this.albumNegocio.removerAlbumActivoDelSessionStorage()
          this._location.back()
        })
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  async actualizarDescripcionEnLaMediaDelApi(descripcion: string) {
    try {
      const status: boolean = await this.albumNegocio.actualizarDescripcionDeLaMedia(
        this.idMediaActivo,
        descripcion,
        this.params,
        this.album
      )

      if (status) {
        this.actualizarDescripcionEnLaMediaLocal(descripcion)
      } else {
        this.toast.abrirToast('text37')
      }
    } catch (error) {

      this.toast.abrirToast('text37')
    }
  }

  accionAtrasAppBarBack(
    esDelSubmit: boolean = true
  ) {
    this.guardarAlbumAntesDeSalir(esDelSubmit, true)
  }

  async guardarAlbumAntesDeSalir(
    esDelSubmit: boolean,
    destruirAlbum: boolean = false
  ) {
    try {
      let enviarCrearAlbum = false
      const idEntidad: string = this.albumNegocio.obtenerIdDeLaEntidadSegunCodigo(this.params.entidad)
      if (
        this.params.accionEntidad === AccionEntidad.CREAR &&
        idEntidad &&
        idEntidad.length > 0 &&
        this.album.media.length > 0
      ) {
        enviarCrearAlbum = true
      }

      if (enviarCrearAlbum) {

        const albumCreado: AlbumModel = await this.albumNegocio.agregarAlbumEnEntidad(
          idEntidad,
          this.params.entidad,
          this.album
        ).toPromise()

        if (!albumCreado) {
          throw new Error('')
        }

        this.album._id = albumCreado._id
      }

      this.album = this.albumNegocio.actualizarEstadoDelAlbum(this.album, CodigosCatalogoEstadoAlbum.CREADO)
      this.albumNegocio.validarActualizacionDelAlbumSegunParams(this.album, this.params, destruirAlbum)
      this.toast.cerrarToast()

      if (esDelSubmit) {
        this.confDone.mostrarDone = true
        setTimeout(() => {
          this._location.back()
        }, 600)
      }

      if (!esDelSubmit) {
        this._location.back()
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  guardarAlbumAntesDeSalirReload(
    destruirAlbum: boolean = false
  ) {
    this.albumNegocio.validarActualizacionDelAlbumSegunParams(this.album, this.params, destruirAlbum)
  }
}
