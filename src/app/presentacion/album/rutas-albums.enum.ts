export enum RutasAlbumPerfil {
    REGISTRO = 'perfil/registro/:entidad:/:titulo/:codigo/',
    CREAR = 'perfil/crear/:entidad/:titulo',
    ACTUALIZAR = 'perfil/actualizar/:entidad/:titulo',
    VISITAR = 'perfil/visitar/:entidad/:titulo',
}

export enum RutasAlbumGeneral {
    REGISTRO = 'general/registro/:entidad:/:titulo/:codigo/',
    CREAR = 'general/crear/:entidad/:titulo',
    ACTUALIZAR = 'general/actualizar/:entidad/:titulo',
    VISITAR = 'general/visitar/:entidad/:titulo',
}

export enum RutasAlbumAudios {
    CREAR = 'audios/crear/:entidad/:titulo',
    ACTUALIZAR = 'audios/actualizar/:entidad/:titulo',
    VISITAR = 'audios/visitar/:entidad/:titulo',
}

export enum RutasAlbumLinks {
    CREAR = 'links/crear/:entidad/:titulo',
    ACTUALIZAR = 'links/actualizar/:entidad/:titulo',
    VISITAR = 'links/visitar/:entidad/:titulo',
}

