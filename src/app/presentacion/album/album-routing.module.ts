import { RestriccionRutas } from '@core/servicios/generales/canActivate/resticcionRutas.service';
import { RutasAlbumAudios, RutasAlbumGeneral, RutasAlbumLinks, RutasAlbumPerfil } from 'src/app/presentacion/album/rutas-albums.enum';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumComponent } from './album.component';
import { AlbumGeneralComponent } from 'src/app/presentacion/album/album-general/album-general.component';
import { AlbumAudiosComponent } from 'src/app/presentacion/album/album-audios/album-audios.component';
import { AlbumLinksComponent } from 'src/app/presentacion/album/album-links/album-links.component';
import { AlbumPerfilComponent } from 'src/app/presentacion/album/album-perfil/album-perfil.component';

const routes: Routes = [
  {
    path: '',
    component: AlbumComponent,
    children: [
      {
        path: RutasAlbumPerfil.REGISTRO.toString(),
        component: AlbumPerfilComponent
      },
      {
        path: RutasAlbumPerfil.CREAR.toString(),
        component: AlbumPerfilComponent
      },
      {
        path: RutasAlbumPerfil.ACTUALIZAR.toString(),
        component: AlbumPerfilComponent
      },
      {
        path: RutasAlbumPerfil.VISITAR.toString(),
        component: AlbumPerfilComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumGeneral.REGISTRO.toString(),
        component: AlbumGeneralComponent
      },
      {
        path: RutasAlbumGeneral.CREAR.toString(),
        component: AlbumGeneralComponent
      },
      {
        path: RutasAlbumGeneral.ACTUALIZAR.toString(),
        component: AlbumGeneralComponent
      },
      {
        path: RutasAlbumGeneral.VISITAR.toString(),
        component: AlbumGeneralComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumAudios.CREAR.toString(),
        component: AlbumAudiosComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumAudios.ACTUALIZAR.toString(),
        component: AlbumAudiosComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumAudios.VISITAR.toString(),
        component: AlbumAudiosComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumLinks.CREAR.toString(),
        component: AlbumLinksComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumLinks.ACTUALIZAR.toString(),
        component: AlbumLinksComponent,
        canActivate: [ RestriccionRutas ]
      },
      {
        path: RutasAlbumLinks.VISITAR.toString(),
        component: AlbumLinksComponent,
        canActivate: [ RestriccionRutas ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule {

}
