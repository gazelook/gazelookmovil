import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { DiametroDelLoader } from '@shared/componentes/cargando/cargando.component';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { ColorIconoBoton } from '@shared/componentes/button/button.component';
import { ConfiguracionItemLink } from '@shared/diseno/modelos/item-link.interface';
import { Location } from '@angular/common';
import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  ColorTextoBoton,
  TipoBoton,
} from '@shared/componentes/button/button.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { AlbumParams } from 'dominio/modelo/parametros/album-parametros.interface';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { AlbumService } from '@core/servicios/generales/album.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoMedia } from '@core/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';

@Component({
  selector: 'app-album-links',
  templateUrl: './album-links.component.html',
  styleUrls: ['./album-links.component.scss'],
})
export class AlbumLinksComponent implements OnInit, OnDestroy {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  // Params
  public params: AlbumParams;
  // Utils
  public AccionEntidadEnum = AccionEntidad;
  public DiametroDelLoaderEnum = DiametroDelLoader;
  public codigosCatalogoEntidad = CodigosCatalogoEntidad;

  // Parametros internos
  public perfilSeleccionado: PerfilModel;
  public album: AlbumModel;
  public dataApiArchivo: SubirArchivoData;
  public mediaActivo: MediaModel;
  public listaDeLinks: Array<ConfiguracionItemLink>;

  // Configuraciones
  public confAppBar: ConfiguracionAppbarCompartida;
  public confToast: ConfiguracionToast;
  public confLineaVerde: LineaCompartida;
  public confBotonReady: BotonCompartido;
  public confBotonSubmit: BotonCompartido;
  public confDone: ConfiguracionDone;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public variablesGlobales: VariablesGlobales,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private albumService: AlbumService,
    private albumNegocio: AlbumNegocio,
    private mediaNegocio: MediaNegocio,
    private perfilNegocio: PerfilNegocio,
    private generadorId: GeneradorId,
    private translateService: TranslateService
  ) {
    this.params = { estado: false };
    this.mediaActivo = {
      descripcion: '',
      miniatura: {},
      principal: {
        url: '',
      },
      id: '',
    };
    this.listaDeLinks = [];
  }

  ngOnInit(): void {
    console.log('estoy aca');

    this.variablesGlobales.mostrarMundo = false;
    this.configurarParametrosDeUrl();
    this.inicializarPerfilSeleccionado();
    if (this.params.estado && this.perfilSeleccionado) {
      this.inicializarDataAlbum();
      this.configurarMediasDelAlbum();
      this.configurarToast();
      this.configurarAppBar();
      this.configurarLineas();
      this.configurarBotones();
      this.configurarDone();

      // En caso la pagina sea recargada, se guarda el estado del album en el session storage sotarage
      window.onbeforeunload = () => this.guardarAlbumAntesDeSalirReload();
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(
        this.perfilSeleccionado,
        this.params.estado
      );
    }
  }

  ngOnDestroy(): void {}

  // Escucha para el boton de back del navegador
  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    this.albumNegocio.removerAlbumSegunElEstado(
      this.params,
      this.album,
      CodigosCatalogoEstadoAlbum.SIN_CREAR
    );
  }

  // Parametros de url
  configurarParametrosDeUrl() {
    const urlParams: Params = this.route.snapshot.params;
    this.params.accionEntidad = this.albumService.definirAccionDelAmbumSegunUrl(
      this.router.url
    );
    if (this.params.accionEntidad) {
      this.params =
        this.albumService.validarParametrosDelAlbumSegunAccionEntidad(
          this.params,
          urlParams
        );
    }
  }

  // Inicializar perfil seleccionado
  inicializarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  inicializarDataAlbum() {
    // Validar la entidad a la que pertenece el album
    this.album = this.albumNegocio.obtenerAlbumActivoDelSessionStorage();

    // Si el album no existe, se ejecuta el back
    if (!this.album) {
      this._location.back();
    }
  }

  configurarMediasDelAlbum() {
    if (this.album) {
      this.album.media.forEach((media) => {
        const item: ConfiguracionItemLink = {
          entidad: this.params.entidad,
          esPortada: this.album.portada && this.album.portada.id === media.id,
          media: media,
          albumPredeterminado: this.album.predeterminado,
          eventoTap: (id: string, url: string) => this.abrirUrlLink(id, url),
          eventoDobleTap: (id: string) =>
            this.validarAccionEntidadParaDefinirPortada(id),
          eventoPress: (id: string) => {
            this.modalEliminarLink(id);
          },
        };

        this.listaDeLinks.push(item);
      });

      this.ordernarListaDeLinks();
    }
  }

  modalEliminarLink(id: string) {
    this.validarAccionEntidadParaEliminarMedia(id);
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    };
  }

  configurarAppBar() {
    // Determinar textos del appbar segun entidad
    const dataAppBar = this.albumNegocio.determinarTextosAppBarSegunEntidad(
      this.params.entidad,
      this.perfilSeleccionado,
      CodigosCatalogoTipoAlbum.LINK
    );

    if (this.params.accionEntidad === AccionEntidad.VISITAR) {
      dataAppBar.subtitulo = 'm4v8texto10';
    }

    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          },
        },
        nombrePerfil: {
          mostrar: true,
          llaveTexto: dataAppBar.nombrePerfil,
        },
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarTextoHome: dataAppBar.mostrarTextoHome,
        subtitulo: {
          mostrar: true,
          llaveTexto: dataAppBar.subtitulo,
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      },
      accionAtras: () => this.accionAtrasAppBarBack(false),
    };
  }

  configurarLineas() {
    this.confLineaVerde = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR012,
      forzarAlFinal: false,
    };
  }

  configurarBotones() {
    this.confBotonReady = {
      tipoBoton: TipoBoton.ICON_COLOR,
      colorIcono: ColorIconoBoton.AZUL,
      enProgreso: false,
      ejecutar: () => {
        if (this.mediaActivo.principal.url.length > 0) {
          this.definirDataParaSubirMedia();
        }
      },
    };

    this.confBotonSubmit = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.accionAtrasAppBarBack();
      },
    };
  }

  configurarDone() {
    this.confDone = {
      intervalo: 4000,
      mostrarDone: false,
      mostrarLoader: false,
    };
  }

  obtenerEstadoModalDataLink() {
    if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
      return true;
    }

    return false;
  }

  reiniciarData(
    enProgreso: boolean,
    descripcion: string = '',
    enlace: string = ''
  ) {
    this.confBotonReady.enProgreso = enProgreso;
    this.mediaActivo.descripcion = descripcion;
    this.mediaActivo.principal.url = enlace;
  }

  abrirUrlLink(id: string, url: string) {
    if (!(url && url.length > 0)) {
      this.toast.abrirToast('text34');
      return;
    }

    const index = this.listaDeLinks.findIndex((e) => e.media.id === id);
    if (index >= 0 && this.params.accionEntidad === AccionEntidad.VISITAR) {
      this.listaDeLinks[index].fueVisitado = true;
    }

    window.open(url);
    return;
  }

  subirMediaAlAlbumLocal(media: MediaModel) {
    const item: ConfiguracionItemLink = {
      entidad: this.params.entidad,
      media: media,
      esPortada:
        this.album.media.length === 0 &&
        (this.params.entidad === CodigosCatalogoEntidad.NOTICIA ||
          this.params.entidad === CodigosCatalogoEntidad.PROYECTO),
      albumPredeterminado: this.album.predeterminado,
      eventoTap: (id: string, url: string) => this.abrirUrlLink(id, url),
      eventoDobleTap: (id: string) =>
        this.validarAccionEntidadParaDefinirPortada(id),
      eventoPress: (id: string) => {
        this.validarAccionEntidadParaEliminarMedia(id);
      },
    };

    if (
      (this.params.entidad === CodigosCatalogoEntidad.NOTICIA ||
        this.params.entidad === CodigosCatalogoEntidad.PROYECTO) &&
      this.album.media.length === 1 &&
      this.album.portada &&
      this.album.portada.id
    ) {
      this.validarAccionEntidadParaDefinirPortada(this.album.portada.id);
    }

    this.album.media.push(media);

    if (item.esPortada) {
      this.album.portada = media;
    }

    this.listaDeLinks.push(item);
    this.ordernarListaDeLinks();
    this.reiniciarData(false);
  }

  definirDataParaSubirMedia() {
    const data: SubirArchivoData = {
      catalogoMedia: CodigosCatalogoTipoMedia.TIPO_LINK,
      descripcion: this.mediaActivo.descripcion,
      enlace: this.mediaActivo.principal.url.trim(),
    };

    this.subirMediaAlApi(data);
  }

  establecerPortadaEnLocal(id: string, accion: string) {
    switch (accion) {
      case 'asignar':
        const pos = this.albumService.obtenerMediaDeLaListaDeMedias(
          id,
          this.album.media
        );
        if (pos < 0) {
          return;
        }

        const media: MediaModel = this.album.media[pos];

        this.album.portada = {
          ...media,
          catalogoMedia: {
            codigo: CodigosCatalogoTipoMedia.TIPO_LINK,
          },
        };

        this.listaDeLinks.forEach((link) => {
          if (link.media.id === id) {
            link.esPortada = true;
          } else {
            link.esPortada = false;
          }
        });

        this.ordernarListaDeLinks();
        break;
      case 'remover':
        this.album.portada = {};
        this.listaDeLinks.forEach((link) => {
          link.esPortada = false;
        });
        this.ordernarListaDeLinks();
        break;
      default:
        break;
    }
  }

  determinarAccionParaPortada(id: string): string {
    if (!this.album.portada || this.album.portada.id !== id) {
      return 'asignar';
    }

    if (this.album.portada && this.album.portada.id === id) {
      return 'remover';
    }

    return null;
  }

  validarAccionEntidadParaDefinirPortada(id: string) {
    const accion: string = this.determinarAccionParaPortada(id);
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.establecerPortadaEnLocal(id, accion);
        break;
      case AccionEntidad.ACTUALIZAR:
        this.establecerPortadaEnElApi(id, accion);
        break;
      default:
        break;
    }
  }

  establecerPortadaEnElApi(id: string, accion: string) {
    switch (accion) {
      case 'asignar':
        this.asignarPortada(id, accion);
        break;
      case 'remover':
        this.removerPortada(id, accion);
        break;
      default:
        break;
    }
  }

  borrarMediaDelLocal(id: string) {
    const pos = this.albumService.obtenerMediaDeLaListaDeMedias(
      id,
      this.album.media
    );
    if (pos < 0) {
      return;
    }
    const media: MediaModel = this.album.media[pos];
    this.album.media.splice(pos, 1);

    if (this.album.portada && this.album.portada.id === media.id) {
      this.album.portada = {};
    }

    let posLink = -1;
    this.listaDeLinks.forEach((link, i) => {
      if (link.media.id === id) {
        posLink = i;
      }
    });

    if (posLink >= 0) {
      this.listaDeLinks.splice(posLink, 1);
    }

    if (
      (this.params.entidad === CodigosCatalogoEntidad.NOTICIA ||
        this.params.entidad === CodigosCatalogoEntidad.PROYECTO) &&
      this.album.media.length === 1
    ) {
      this.validarAccionEntidadParaDefinirPortada(this.album.media[0].id);
    }
  }

  validarAccionEntidadParaEliminarMedia(id: string) {
    switch (this.params.accionEntidad) {
      case AccionEntidad.CREAR:
        this.borrarMediaDelLocal(id);
        break;
      case AccionEntidad.ACTUALIZAR:
        this.borrarMediaDelApi(id);
        break;
      default:
        break;
    }
  }

  async subirMediaAlApi(data: SubirArchivoData) {
    try {
      this.confBotonReady.enProgreso = true;
      const media: MediaModel = await this.albumNegocio.subirMedia(
        data,
        this.params,
        this.album
      );


      if (!media) {
		  	// throw new Error('')
        this.toast.abrirToast('LINK_NO_VALIDO');

        this.confBotonReady.enProgreso = false;
        return;
      }

      let a = this.subirMediaAlAlbumLocal(media);

      this.confBotonReady.enProgreso = false;
    } catch (error) {

      this.confBotonReady.enProgreso = false;
      this.toast.abrirToast('text37');
    }
  }

  async borrarMediaDelApi(id: string) {
    if (!(id && id.length > 0)) {
      return;
    }

    try {
      const status: boolean = await this.albumNegocio.borrarMedia(
        id,
        this.params,
        this.album
      );

      if (!status) {
        throw new Error('');
      }

      this.borrarMediaDelLocal(id);

      if (this.album.media.length === 0) {
        this.confDone.mostrarDone = true;
        setTimeout(() => {
          this.albumNegocio.removerAlbumActivoDelSessionStorage();
          this._location.back();
        });
      }
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async asignarPortada(id: string, accion: string) {
    if (!(id && id.length > 0)) {
      return;
    }

    try {
      const status: boolean = await this.albumNegocio.asignarPortada(
        id,
        this.params,
        this.album
      );
      if (!status) {
        throw new Error('');
      }

      this.establecerPortadaEnLocal(id, accion);
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  async removerPortada(id: string, accion: string) {
    if (!(id && id.length > 0)) {
      return;
    }

    try {
      const status: boolean = await this.albumNegocio.removerPortada(
        id,
        this.params,
        this.album
      );

      if (status) {
        this.establecerPortadaEnLocal(id, accion);
      } else {
        this.toast.abrirToast('text37');
      }
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  accionAtrasAppBarBack(esDelSubmit: boolean = true) {
    this.guardarAlbumAntesDeSalir(esDelSubmit, true);
  }

  async guardarAlbumAntesDeSalir(
    esDelSubmit: boolean,
    destruirAlbum: boolean = false
  ) {
    try {
      let enviarCrearAlbum = false;
      const idEntidad: string =
        this.albumNegocio.obtenerIdDeLaEntidadSegunCodigo(this.params.entidad);

      if (
        this.params.accionEntidad === AccionEntidad.CREAR &&
        idEntidad &&
        idEntidad.length > 0 &&
        this.album.media.length > 0
      ) {
        enviarCrearAlbum = true;
      }

      if (enviarCrearAlbum) {
        const albumCreado: AlbumModel = await this.albumNegocio
          .agregarAlbumEnEntidad(idEntidad, this.params.entidad, this.album)
          .toPromise();

        if (!albumCreado) {
          throw new Error('');
        }

        this.album._id = albumCreado._id;
      }

      // Actualizar estado predeterminado del album
      if (
        this.params.entidad === CodigosCatalogoEntidad.NOTICIA &&
        this.params.accionEntidad === AccionEntidad.ACTUALIZAR
      ) {
        await this.albumNegocio
          .actualizarParametrosDelAlbum(idEntidad, this.params.entidad, {
            _id: this.album._id,
            predeterminado: this.album.predeterminado,
          })
          .toPromise();
      }

      this.album = this.albumNegocio.actualizarEstadoDelAlbum(
        this.album,
        CodigosCatalogoEstadoAlbum.CREADO
      );
      this.albumNegocio.validarActualizacionDelAlbumSegunParams(
        this.album,
        this.params,
        destruirAlbum
      );
      this.toast.cerrarToast();

      if (esDelSubmit) {
        this.confDone.mostrarDone = true;
        setTimeout(() => {
          this._location.back();
        }, 600);
      }

      if (!esDelSubmit) {
        this._location.back();
      }
    } catch (error) {
      this.toast.abrirToast('text36');
    }
  }

  guardarAlbumAntesDeSalirReload(destruirAlbum: boolean = false) {
    this.albumNegocio.validarActualizacionDelAlbumSegunParams(
      this.album,
      this.params,
      destruirAlbum
    );
  }

  ordernarListaDeLinks() {
    const index = this.listaDeLinks.findIndex(
      (e) => e.esPortada && e.esPortada === true
    );
    if (index >= 0) {
      const portada = this.listaDeLinks[index];
      this.listaDeLinks.splice(index, 1);
      this.listaDeLinks.unshift(portada);
    }
    if (this.listaDeLinks.length === 1) {
      this.listaDeLinks[0].esPortada = true;
    }
  }
}
