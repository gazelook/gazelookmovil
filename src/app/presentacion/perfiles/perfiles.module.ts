import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from '@shared/compartido.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilesRoutingModule } from 'src/app/presentacion/perfiles/perfiles-routing.module';
import { PerfilesComponent } from 'src/app/presentacion/perfiles/perfiles.component';
import { PerfilComponent } from 'src/app/presentacion/perfiles/perfil/perfil.component';


@NgModule({
	declarations: [ PerfilesComponent, PerfilComponent ],
	imports: [
		CommonModule,
		PerfilesRoutingModule,
		CompartidoModule,
		TranslateModule, 
		FormsModule
	],
	exports: [
		TranslateModule
	]
})
export class PerfilesModule { }
