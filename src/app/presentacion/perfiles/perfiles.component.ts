import { Component, OnInit } from '@angular/core';
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';

@Component({
	selector: 'app-perfiles',
	templateUrl: './perfiles.component.html',
	styleUrls: ['./perfiles.component.scss']
})
export class PerfilesComponent implements OnInit {

	constructor(
		private notificacionesUsuario: NotificacionesDeUsuario,
		// private llamadaFirebaseService: LlamadaFirebaseService
	) {   }

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
		// this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
	}

}
