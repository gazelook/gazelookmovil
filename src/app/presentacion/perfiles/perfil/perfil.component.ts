import { CodigosCatalogoIdioma } from '@core/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { IdiomaNegocio } from 'dominio/logica-negocio/idioma.negocio';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { ChatMetodosCompartidosService } from 'src/app/presentacion/gazing/chat-metodos-comunes.service';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { RutasPerfiles } from 'src/app/presentacion/perfiles/rutas-perfiles.enum';
import { PensamientoNegocio } from 'dominio/logica-negocio/pensamiento.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';
import { RutasNoticias } from 'src/app/presentacion/noticias/rutas-noticias.enum';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { AlturaResumenPerfil } from '@shared/diseno/enums/altura-resumen-perfil.enum';
import { ConfiguracionResumenPerfil } from '@shared/diseno/modelos/resumen-perfil.interface';
import { FiltroGeneral } from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import { CodigoEstadoParticipanteAsociacion, TipoParticipanteAsociacion } from '@core/servicios/remotos/codigos-catalogos/codigo-estado-partic-aso.enum';
import { DiametroDelLoader } from '@shared/componentes/cargando/cargando.component';
import { RutasAlbumGeneral } from 'src/app/presentacion/album/rutas-albums.enum';
import { RutasGazing } from 'src/app/presentacion/gazing/rutas-gazing.enum';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
import { TipoPensamiento, EstiloItemPensamiento } from '@shared/diseno/enums/tipo-pensamiento.enum';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { PensamientoCompartido } from '@shared/diseno/modelos/pensamiento';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { CongifuracionTituloRectangulo } from '@shared/diseno/modelos/titulo-rectangulo.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { UsoItemRectangular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ColorDeBorde, ColorDeFondo } from '@shared/diseno/enums/estilos-colores-general';
import { ItemCircularCompartido, ItemRectangularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { MetodosParaFotos } from '@core/servicios/generales/metodos-para-fotos.service';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoPortadaGaze, TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, RouterEvent, NavigationEnd, NavigationStart, NavigationError } from '@angular/router';
import { PerfilesParams } from 'dominio/modelo/parametros/perfil-parametros.interface';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ParticipanteAsociacionNegocio } from 'dominio/logica-negocio/participante-asociacion.negocio';
import { DataNotificaciones, NotificacionFirebaseModel, NotificacionPorCodigoEntidad, ProyectoNotificacion } from '@core/servicios/generales/notificaciones/notificaciones.interface';
import { NotificacionesDePerfilSinFiltro } from '@core/servicios/generales/notificaciones/notificaciones-perfil-sin-filtro.service';

@Component({
	selector: 'app-perfil',
	templateUrl: './perfil.component.html',
	styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit, OnDestroy {
	@ViewChild('toast', { static: false }) toast: ToastComponent

	// Utils
	public mostrarLoaoder: boolean
	public mostrarError: boolean
	public contenidoError: string
	public CodigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum
	public ModoDelPerfilEnum = ModoDelPerfil
	public DiametroDelLoaderEnum = DiametroDelLoader
	public idCapaListaContactos: string
	public idCapaListaNoticias: string
	public idCapaListaProyectos: string
	public idiomaSeleccionado: CatalogoIdiomaEntity
	public origenFlechaBack: boolean
	public tipoPerfilVisitar: string
	public controlRutas: any
	public proyectosNotificaciones: Array<ProyectoNotificacion>

	// Parametros internos
	public params: PerfilesParams
	public perfilSeleccionado: PerfilModel
	public perfil: PerfilModel
	public historialPerfiles: Array<HistorialPerfil>
	public historialPerfilActual: HistorialPerfil
	public nombreDeUsuario: string
	public loaderBotonesEstadoAsociacion: boolean
	public dataNotificacion: DataNotificaciones
	public notificaciones: Array<NotificacionPorCodigoEntidad>

	// Configuraciones hijos
	public confAppbar: ConfiguracionAppbarCompartida
	public confAlbumPerfil: ItemCircularCompartido
	public confAlbumGeneral: ItemRectangularCompartido
	public confPortada: PortadaGazeCompartido
	public confBotonMisContactos: BotonCompartido
	public confAvisoSinContactos: CongifuracionTituloRectangulo
	public confAvisoSinNoticias: CongifuracionItemProyectosNoticias
	public confAvisoSinProyectos: CongifuracionItemProyectosNoticias
	public confListaContactos: PaginacionModel<ItemCircularCompartido>
	public confListaNoticias: PaginacionModel<CongifuracionItemProyectosNoticias>
	public confListaProyectos: PaginacionModel<CongifuracionItemProyectosNoticias>
	public confDataLista: DatosLista
	public confPensamientoCompartido: PensamientoCompartido
	public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta
	public confResumenPerfil: ConfiguracionResumenPerfil
	public confToast: ConfiguracionToast
	public confListaPensamientos: PaginacionModel<PensamientoModel>
	public confBotonesEstadoAsociacion: Array<BotonCompartido>
	public confDone: ConfiguracionDone

	constructor(
		public variablesGlobales: VariablesGlobales,
		public estilosDelTextoServicio: EstiloDelTextoServicio,
		private router: Router,
		private route: ActivatedRoute,
		private _location: Location,
		private perfilNegocio: PerfilNegocio,
		private metodosParaFotos: MetodosParaFotos,
		private albumNegocio: AlbumNegocio,
		public participanteAsoNegocio: ParticipanteAsociacionNegocio,
		private noticiaNegocio: NoticiaNegocio,
		private proyectoNegocio: ProyectoNegocio,
		private pensamientoNegocio: PensamientoNegocio,
		private chatMetodosCompartidosService: ChatMetodosCompartidosService,
		private notificacionesService: NotificacionesDePerfilSinFiltro,
		private idiomaNegocio: IdiomaNegocio,
	) {
		this.params = { estado: false, esPropietario: false }
		this.idCapaListaContactos = 'capa_perfil_contactos'
		this.idCapaListaNoticias = 'capa_perfil_noticias'
		this.idCapaListaProyectos = 'capa_perfil_proyectos'
		this.mostrarLoaoder = true
		this.mostrarError = false
		this.contenidoError = ''
		this.historialPerfiles = []
		this.origenFlechaBack = false
		this.nombreDeUsuario = ''
		this.tipoPerfilVisitar = ''
		this.confBotonesEstadoAsociacion = []
		this.loaderBotonesEstadoAsociacion = false
		this.proyectosNotificaciones = []
		this.notificaciones = []
	}

	async ngOnInit() {
		this.variablesGlobales.mostrarMundo = false
		this.validarNavegacion()
		this.configurarPerfilSeleccionado()
		this.configurarParametrosUrl()
		this.configurarAppBar()
		this.configurarAlbumPerfil()
		this.configurarAlbumGeneral()
		this.configurarPortada()
		this.configurarBotones()
		this.configurarAvisosDelPerfil()
		this.configurarListaContactos()
		this.configurarListaNoticias()
		this.configurarListaProyectos()
		this.configurarListaPensamientos()
		this.configurarImagenPantallaCompleta()
		this.configurarHistorialDelPerfilActual()
		this.configurarResumenPerfil()
		this.configurarToast()
		this.configurarDone()

		if (this.params.estado && this.perfilSeleccionado) {
			await this.inicializarDataDeLaEntidad()

			window.onbeforeunload = () => this.notificacionesService.desconectarDeEscuchaNotificaciones()
		} else {
			this.perfilNegocio.validarEstadoDelPerfil(
				this.perfilSeleccionado,
				this.params.estado
			)
		}
	}

	ngOnDestroy(): void {
		this.notificacionesService.desconectarDeEscuchaNotificaciones()
	}

	@HostListener('window:popstate', ['$event'])
	onPopState(event: any) {
		this.origenFlechaBack = true
		this.removerHistorialActualDelEnSession()
	}

	validarNavegacion() {
		let currentUrl = this.router.url;
		this.router.events.subscribe((event) => {
			if (event instanceof NavigationStart) {
			}

			if (
				event instanceof NavigationEnd &&
				currentUrl.includes('perfiles/ver') &&
				this.origenFlechaBack
			) {
				window.location.reload()
			}

			if (event instanceof NavigationError) {

			}
		});
	}

	configurarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
		this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado()

	}

	configurarParametrosUrl() {
		const { id } = this.route.snapshot.params

		if (!id || !this.perfilSeleccionado) {
			return
		}

		this.params.id = id
		this.params.esPropietario = (this.perfilSeleccionado._id === id)
		this.params.estado = true


	}

	configurarAppBar() {

		this.confAppbar = {
			accionAtras: () => this.accionAtrasAppBar(),
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				mostrarDivBack: {
					icono: true,
					texto: true
				},
				mostrarLineaVerde: true,
				mostrarTextoHome: true,
				mostrarBotonXRoja: false,
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre
				},
				buscador: {
					mostrar: true,
					configuracion: {
						disable: false,
						placeholder: 'm3v2texto1',
						modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
						entidad: CodigosCatalogoEntidad.PERFIL
					}
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: (this.params.esPropietario) ? 'm3v2texto2' : 'm3v3texto2'
				},

			}

		}
	}

	accionAtrasAppBar() {
		if (this.loaderBotonesEstadoAsociacion) {
			return
		}

		if (
			this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_CONTACTOS &&
			!this.confListaContactos.cargando
		) {
			this.configurarListaContactos()
			this.configurarContactosInvitacionesDelPerfil()
			this.historialPerfilActual.modoDelPerfil = ModoDelPerfil.DEFAULT
			this.actualizarHitorialDelPerfilActual()
			return
		}

		if (
			this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_NOTICIAS &&
			!this.confListaNoticias.cargando
		) {
			this.configurarListaNoticias()
			this.configurarNoticiasDelPerfil()
			this.historialPerfilActual.modoDelPerfil = ModoDelPerfil.DEFAULT
			this.actualizarHitorialDelPerfilActual()
			return
		}

		if (
			this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_PROYECTOS &&
			!this.confListaProyectos.cargando
		) {
			this.configurarListaProyectos()
			this.configurarProyectosDelPerfil()
			this.historialPerfilActual.modoDelPerfil = ModoDelPerfil.DEFAULT
			this.actualizarHitorialDelPerfilActual()
			return
		}

		this.origenFlechaBack = true
		this.removerHistorialActualDelEnSession()
		this._location.back()
	}

	configurarAlbumPerfil(
		urlMedia: string = '',
		usoItemCircular: UsoItemCircular = UsoItemCircular.CIRCARITAPERFIL
	) {
		this.confAlbumPerfil = this.metodosParaFotos.configurarItemCircular(
			urlMedia,
			ColorDeBorde.BORDER_ROJO,
			ColorDeFondo.FONDO_BLANCO,
			false,
			usoItemCircular,
			false
		)
	}

	configurarAlbumGeneral(
		urlMedia: string = '',
		usoItemCircular: UsoItemRectangular = UsoItemRectangular.RECPERFIL
	) {
		this.confAlbumGeneral = this.metodosParaFotos.configurarItemRectangular(
			urlMedia,
			ColorDeBorde.BORDER_BLANCO,
			ColorDeFondo.FONDO_BLANCO,
			false,
			usoItemCircular,
			false
		)
	}

	configurarPortada() {
		this.confPortada = this.metodosParaFotos.configurarPortada(TamanoPortadaGaze.PORTADACORTADA)
	}

	configurarBotones() {
		this.confBotonMisContactos = {
			text: 'm3v2texto5',
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
			colorTexto: ColorTextoBoton.VERDE,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			ejecutar: () => {
				this.perfilNegocio.removerPerfilActivoDelSessionStorage()
				const ruta = RutasLocales.GAZING.toString()
				const rutaAux = RutasGazing.CHAT_CONTACTOS.toString()
				this.router.navigateByUrl(ruta + '/' + rutaAux)
			}
		}
	}

	configurarAvisosDelPerfil() {
		this.confAvisoSinContactos = {
			colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
			colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
			textoBoton1: 'm3v1texto9',
			textoBoton2: 'm3v1texto10'
		}

		this.confAvisoSinNoticias = {
			id: '',
			titulo: {
				mostrar: true,
				configuracion: {
					textoBoton1: 'm3v1texto12',
					textoBoton2: 'm3v1texto13',
					colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
				},
			},
			colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
			etiqueta: { mostrar: false },
			fecha: {
				mostrar: false
			},
			loader: true,
			urlMedia: this.chatMetodosCompartidosService.obtenerUrlRamdon(),
			colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
			eventoTap: {
				activo: true,
				evento: (id: string) => {
					// if (this.params.esPropietario) {
					// 	let ruta = RutasLocales.MODULO_NOTICIAS.toString()
					// 	let informacion = RutasNoticias.PUBLICAR
					// 	this.router.navigateByUrl(ruta + '/' + informacion)
					// }

				},
			},
			eventoPress: { activo: false },
			eventoDobleTap: { activo: false },
			usoItem: UsoItemProyectoNoticia.RECNOTICIA,
		}

		this.confAvisoSinProyectos = {
			titulo: {
				mostrar: true,
				configuracion: {
					textoBoton1: 'm3v1texto15',
					textoBoton2: 'm3v1texto16',
					colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
				},
			},
			colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
			etiqueta: { mostrar: false },
			fecha: {
				mostrar: false,

			},
			loader: true,
			urlMedia: this.chatMetodosCompartidosService.obtenerUrlRamdon(),
			colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
			eventoTap: {
				activo: true,
				evento: (id: string) => {
					// if (this.params.esPropietario) {
					// 	let ruta = RutasLocales.MENU_PUBLICAR_PROYECTO_NOTICIA.toString()
					// 	let informacion = RutasNoticias.PUBLICAR
					// 	this.router.navigateByUrl(ruta)
					// }

				},
			},
			eventoPress: { activo: false },
			eventoDobleTap: { activo: false },
			id: '',
			usoItem: UsoItemProyectoNoticia.RECPROYECTO,
			actualizado: false,
		};
	}

	configurarListaContactos() {
		this.confListaContactos = {
			paginaActual: 0,
			proximaPagina: true,
			lista: [],
			totalDatos: 0,
			cargando: false
		}
	}

	configurarListaNoticias() {
		this.confListaNoticias = {
			paginaActual: 0,
			proximaPagina: true,
			lista: [],
			totalDatos: 0,
			cargando: false
		}
	}

	configurarListaProyectos() {
		this.confListaProyectos = {
			paginaActual: 0,
			proximaPagina: true,
			lista: [],
			totalDatos: 0,
			cargando: false
		}
	}

	configurarListaPensamientos() {
		this.confListaPensamientos = {
			lista: [],
			cargando: false,
			paginaActual: 1,
			proximaPagina: true
		}

		this.confDataLista = {
			tamanoLista: TamanoLista.LISTA_PENSAMIENTO_PERFIL,
			lista: [],
			cargarMas: () => {
				if (!this.confListaPensamientos.cargando) {
					this.configurarPensamientosDelPerfil()
				}
			}
		};

		this.confPensamientoCompartido = {
			tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
			tituloPensamiento: '',
			esLista: true,
			configuracionItem: { estilo: EstiloItemPensamiento.ITEM_ALEATORIO },
		}
	}

	configurarImagenPantallaCompleta() {
		this.confImagenPantallaCompleta = {
			urlMedia: '',
			mostrar: false,
			mostrarLoader: true,
			eventoBotonCerrar: () => {
				this.confImagenPantallaCompleta.mostrar = false
				this.confImagenPantallaCompleta.mostrarLoader = true
				this.confImagenPantallaCompleta.urlMedia = ''
			},
			eventoBotonDescargar: () => {

				let link = document.createElement('a');
				link.href = this.confImagenPantallaCompleta.urlMedia;
				link.target = '_blank'
				link.dispatchEvent(new MouseEvent('click', {
					view: window,
					bubbles: false,
					cancelable: true
				}));

			}
		}
	}

	configurarHistorialDelPerfilActual(
		modoDelPerfil: ModoDelPerfil = ModoDelPerfil.DEFAULT,
		perfilActivo: string = '',
		perfilANavegar: Array<string> = []
	) {
		this.historialPerfilActual = {
			modoDelPerfil: modoDelPerfil,
			perfilActivo: perfilActivo,
			perfilANavegar: perfilANavegar
		}
	}

	configurarResumenPerfil(
		idContacto: string = '',
		mostrarLoader: boolean = false,
		mostrarResumen: boolean = false,
		titulo: string = '',
		urlMedia: string = '',
		usoDelItem: UsoItemCircular = UsoItemCircular.CIRCONTACTO,
		pensamientos: PensamientoModel[] = [],
		sonContactos: boolean = false
	) {
		this.confResumenPerfil = {
			loader: mostrarLoader,
			mostrar: mostrarResumen,
			error: false,
			titulo: titulo,
			alturaModal: AlturaResumenPerfil.VISTA_BUSCAR_CONTACTOS_100,
			configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
				urlMedia,
				ColorDeBorde.BORDER_AMARILLO,
				ColorDeFondo.FONDO_BLANCO,
				false,
				usoDelItem,
				false
			),
			configuracionPensamiento: {
				tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
				esLista: true,
				subtitulo: true,
				configuracionItem: {
					estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
				},
			},
			confDataListaPensamientos: {
				tamanoLista: TamanoLista.LISTA_PENSAMIENTO_RESUMEN_MODAL_PERFIL,
				lista: pensamientos,
			},
			botones: [
				{
					text: 'm3v5texto2',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.AMARRILLO,
					ejecutar: () => this.irAOtroPerfil(idContacto),
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				},
				{
					text: 'm3v5texto4',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.ROJO,
					ejecutar: () => {
						this.configurarResumenPerfil()
					},
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				},
			],
			reintentar: (idPerfil: string) => {
				this.abrirResumenPerfil(idContacto)
			},
			sonContactos: sonContactos
		}
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false,
			mostrarLoader: false,
			cerrarClickOutside: false,
			texto: '',
			intervalo: 5,
			bloquearPantalla: false,
		}
	}

	configurarDone() {
		this.confDone = {
			mostrarDone: false,
			intervalo: 4000,
			mostrarLoader: false
		}
	}

	async inicializarDataDeLaEntidad() {
		try {
			this.mostrarLoaoder = true
			const idPerfilOtro = (this.params.esPropietario) ? '' : this.perfilSeleccionado._id
			this.perfil = await this.perfilNegocio.obtenerInformacionDelPerfil(this.params.id, idPerfilOtro).toPromise()
			this.perfilNegocio.guardarPerfilActivoEnSessionStorage(this.perfil)

			if (!this.perfil) {
				throw new Error('')
			}

			// Inicializar componentes hijos
			this.nombreDeUsuario = (this.perfil.nombreContacto.length > 7) ? this.perfil.nombreContacto.substring(0, 7) + '...' : this.perfil.nombreContacto
			this.configurarContactosInvitacionesDelPerfil()
			this.configurarNoticiasDelPerfil()
			this.configurarProyectosDelPerfil()
			this.configurarAlbumDePerfil()
			this.configurarAlbumDeGeneral()
			this.configurarPensamientosDelPerfil()
			this.configurarBotonesEstadoAsociacion()
			this.configurarNotificaciones()

			this.historialPerfilActual.perfilActivo = this.params.id
			this.validarHistorialDelPerfilEnMemoria()



			switch (this.idiomaSeleccionado.codNombre) {
				case CodigosCatalogoIdioma.ESPANOL:
					this.tipoPerfilVisitar = TIPO_PERFIL_ES[this.perfil.tipoPerfil.codigo]
					break;
				case CodigosCatalogoIdioma.INGLES:
					this.tipoPerfilVisitar = TIPO_PERFIL_EN[this.perfil.tipoPerfil.codigo]
					break;
				case CodigosCatalogoIdioma.FRANCES:
					this.tipoPerfilVisitar = TIPO_PERFIL_FR[this.perfil.tipoPerfil.codigo]
					break;
				case CodigosCatalogoIdioma.ALEMAN:
					this.tipoPerfilVisitar = TIPO_PERFIL_DE[this.perfil.tipoPerfil.codigo]
					break;
				case CodigosCatalogoIdioma.ITALIANO:
					this.tipoPerfilVisitar = TIPO_PERFIL_IT[this.perfil.tipoPerfil.codigo]
					break;
				case CodigosCatalogoIdioma.PORTUGUES:
					this.tipoPerfilVisitar = TIPO_PERFIL_PO[this.perfil.tipoPerfil.codigo]
					break;
			}

			this.mostrarLoaoder = false


		} catch (error) {
			this.mostrarLoaoder = false
			this.mostrarError = true
			this.contenidoError = 'text37'
		}
	}

	configurarContactosInvitacionesDelPerfil() {
		if (!this.perfil.asociaciones || this.perfil.asociaciones.length === 0) {
			return
		}

		try {
			this.perfil.asociaciones.forEach(asociacion => {
				const participante = asociacion.participantes[0]
				const perfilAux = participante.perfil

				if (
					// this.params.esPropietario ||
					(
						this.historialPerfilActual.perfilANavegar.indexOf(perfilAux._id) < 0 &&
						this.perfilSeleccionado._id !== perfilAux._id
					)
				) {
					const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
						CodigosCatalogoTipoAlbum.PERFIL,
						perfilAux.album
					)

					if (!album || album === null) {
						throw new Error('')
					}

					const urlMedia = album.portada.principal.url
					const usoItemCircular: UsoItemCircular = (album.portada.principal.fileDefault) ?
						UsoItemCircular.CIRCARITACONTACTODEFECTO :
						UsoItemCircular.CIRCONTACTO

					this.confListaContactos.lista.push(
						this.metodosParaFotos.configurarItemCircular(
							urlMedia,
							ColorDeBorde.BORDER_ROJO,
							ColorDeFondo.FONDO_BLANCO,
							false,
							usoItemCircular,
							false,
							perfilAux._id,
						)
					)
				}
			})
		} catch (error) {
			this.configurarListaContactos()
		}
	}

	configurarNoticiasDelPerfil() {
		if (!this.perfil || this.perfil.noticias.length === 0) {
			return
		}

		try {
			this.perfil.noticias.forEach(noticia => {
				this.confListaNoticias.lista.push(
					this.metodosParaFotos.configurarItemNoticia(noticia)
				)
			})
		} catch (error) {
			this.configurarListaNoticias()
		}
	}

	configurarProyectosDelPerfil() {
		if (!this.perfil || this.perfil.proyectos.length === 0) {
			return
		}

		try {
			this.perfil.proyectos.forEach(proyecto => {
				const configuracion = this.metodosParaFotos.configurarItemProyecto(proyecto)
				const index = this.proyectosNotificaciones.findIndex(e => e.proyecto._id === proyecto.id)
				configuracion.mostrarCorazon = (index >= 0)

				this.confListaProyectos.lista.push(configuracion)
			})
		} catch (error) {
			this.configurarListaProyectos()
		}
	}

	configurarAlbumDePerfil() {
		if (!this.perfil || !this.perfil.album || this.perfil.album.length === 0) {
			return
		}

		try {
			const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.PERFIL,
				this.perfil.album
			)

			if (!album || album === null) {
				throw new Error('')
			}

			const urlMedia = album.portada.principal.url
			const usoItemCircular: UsoItemCircular = (album.portada.principal.fileDefault) ? UsoItemCircular.CIRCARITAPERFIL : UsoItemCircular.CIRPERFIL

			this.configurarAlbumPerfil(
				urlMedia,
				usoItemCircular
			)
		} catch (error) {
			this.configurarAlbumPerfil()
		}
	}

	configurarAlbumDeGeneral() {
		if (!this.perfil || !this.perfil.album || this.perfil.album.length === 0) {
			return
		}

		try {
			const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.GENERAL,
				this.perfil.album
			)

			if (!album || album === null) {
				throw new Error('')
			}

			const urlMedia = album.portada.principal.url
			const usoItemCircular: UsoItemRectangular = UsoItemRectangular.RECPERFIL

			this.configurarAlbumGeneral(
				urlMedia,
				usoItemCircular
			)
		} catch (error) {
			this.configurarAlbumPerfil()
		}
	}

	async configurarPensamientosDelPerfil() {
		try {
			if (!this.confListaPensamientos.proximaPagina) {
				return
			}

			this.confListaPensamientos.cargando = true
			const query = (!this.params.esPropietario) ?
				this.pensamientoNegocio.cargarMasPensamientosPaginacion(
					this.perfil._id,
					this.confListaPensamientos.paginaActual,
					true,
					12,
					true
				)
				:
				this.pensamientoNegocio.cargarMasPensamientosPaginacion(
					this.perfil._id,
					this.confListaPensamientos.paginaActual,
					true,
					12,
				)

			const pensamientos = await query.toPromise()

			this.confListaPensamientos.proximaPagina = pensamientos.proximaPagina
			if (this.confListaPensamientos.proximaPagina) {
				this.confListaPensamientos.paginaActual += 1
			}

			pensamientos.lista.forEach(pensamiento => {
				this.confDataLista.lista.push(pensamiento)
			})
			this.confListaPensamientos.cargando = false
		} catch (error) {
			this.configurarListaPensamientos()
		}
	}

	configurarBotonesEstadoAsociacion() {
		if (this.params.esPropietario) {
			return
		}

		try {
			const participante = this.perfil.participanteAsociacion
			this.confBotonesEstadoAsociacion = []
			if (!participante) {
				// Enviar invitacion
				this.confBotonesEstadoAsociacion.push({
					text: 'm3v6texto4',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.AMARRILLO,
					ejecutar: () => this.enviarInvitacion(),
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				})
				return
			}

			if (participante.estado.codigo === CodigoEstadoParticipanteAsociacion.INVITANTE) {
				// Cancelar invitacion
				this.confBotonesEstadoAsociacion.push({
					text: 'm3v6texto5',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.ROJO,
					ejecutar: () => this.cambiarEstadoAsociacion(CodigoEstadoParticipanteAsociacion.CANCELADA),
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				})
				return
			}

			if (participante.estado.codigo === CodigoEstadoParticipanteAsociacion.INVITADO) {
				// Aceptar invitacion
				this.confBotonesEstadoAsociacion.push({
					text: 'm3v3texto13',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.AMARRILLO,
					ejecutar: () => this.cambiarEstadoAsociacion(CodigoEstadoParticipanteAsociacion.CONTACTO),
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				})
				// Declinar invitacion
				this.confBotonesEstadoAsociacion.push({
					text: 'm3v3texto14',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.ROJO,
					ejecutar: () => this.cambiarEstadoAsociacion(CodigoEstadoParticipanteAsociacion.RECHAZADA),
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				})
				return
			}
		} catch (error) {
			this.confBotonesEstadoAsociacion = []
		}
	}

	eventoTapAlbums(
		tipoAlbum: CodigosCatalogoTipoAlbum
	) {
		try {
			if (this.params.esPropietario) {
				const data = this.perfilNegocio.obtenerCatalogoTipoPerfil()
				const tipoPerfil = this.perfilNegocio.obtenerTipoPerfilSegunCodigo(
					this.perfil.tipoPerfil.codigo
				)

				if (!tipoPerfil) {
					throw new Error('')
				}

				this.perfilNegocio.removerPerfilActivoDelSessionStorage()
				this.perfilNegocio.guardarTipoPerfilActivo(tipoPerfil)
				const ruta = RutasLocales.ACTUALIZAR_PERFIL.toString().replace(':id', this.perfil._id)
				this.router.navigateByUrl(ruta)
				return
			}

			const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				tipoAlbum,
				this.perfil.album
			)

			if (!album || album === null) {
				throw new Error('')
			}

			this.confImagenPantallaCompleta.urlMedia = album.portada.principal.url
			this.confImagenPantallaCompleta.mostrarLoader = true
			this.confImagenPantallaCompleta.mostrar = true
		} catch (error) {
		}
	}

	eventoDobleTapAlbums() {
		try {
			if (this.params.esPropietario) {
				return
			}

			const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.GENERAL,
				this.perfil.album
			)

			if (!album || album === null || !album.media || album.media.length === 0) {
				throw new Error('')
			}

			this.albumNegocio.guardarAlbumActivoEnSessionStorage(album)
			let nombreContacto = this.perfil.nombreContacto
			if (this.perfil.nombreContactoTraducido) {
				nombreContacto = nombreContacto + ' ' + '|' + ' ' + this.perfil.nombreContactoTraducido
			}
			const ruta = RutasLocales.MODULO_ALBUM.toString()
			let rutaAux = RutasAlbumGeneral.VISITAR.toString()
			rutaAux = rutaAux.replace(':entidad', CodigosCatalogoEntidad.PERFIL)
			rutaAux = rutaAux.replace(':titulo', nombreContacto)

			this.router.navigateByUrl(ruta + '/' + rutaAux)
		} catch (error) {
		}
	}

	eventoTapContacto(idContacto: string) {
		try {
			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_CONTACTOS) {
				this.abrirResumenPerfil(idContacto)
				return
			}

			if (this.perfil.asociaciones.length <= 4) {
				this.abrirResumenPerfil(idContacto)
				return
			}

			this.historialPerfilActual.modoDelPerfil = ModoDelPerfil.AMPLIAR_CONTACTOS
			this.confListaContactos.lista = []
			this.obtenerContactosConPaginacion((this.params.esPropietario) ? CodigoEstadoParticipanteAsociacion.INVITADO : CodigoEstadoParticipanteAsociacion.CONTACTO)
		} catch (error) {
		}
	}

	async eventoDobleTapContacto(idContacto: string) {
		if (!this.params.esPropietario) {
			return
		}

		try {
			const pos = this.confListaContactos.lista.findIndex(e => e.id === idContacto)

			if (pos < 0) {
				throw new Error('')
			}

			this.confListaContactos.lista[pos].mostrarLoader = true

			const perfilAux = await this.perfilNegocio.obtenerPerfilGeneral(idContacto, this.perfilSeleccionado._id).toPromise()

			const estatus = await this.participanteAsoNegocio.cambiarEstadoAsociacion(
				perfilAux.participanteAsociacion.asociacion._id,
				CodigoEstadoParticipanteAsociacion.RECHAZADA,
				perfilAux._id
			).toPromise()

			if (!estatus) {
				throw new Error('')
			}

			this.confListaContactos.lista.splice(pos, 1)
		} catch (error) {
			this.reiniciarLoaders()
			this.toast.abrirToast('text37')
		}
	}

	reiniciarLoaders() {
		this.confListaContactos.lista.forEach(e => {
			e.mostrarLoader = false
		})
	}

	async obtenerContactosConPaginacion(
		tipoAsociacion: CodigoEstadoParticipanteAsociacion
	) {
		try {
			if (!this.confListaContactos.proximaPagina) {
				return
			}

			this.confListaContactos.cargando = true
			this.confListaContactos.paginaActual += 1

			const data = await this.participanteAsoNegocio.obtenerParticipanteAsoTipo(
				this.perfil._id,
				20,
				this.confListaContactos.paginaActual,
				tipoAsociacion,
				FiltroGeneral.ALFA
			).toPromise()

			this.confListaContactos.proximaPagina = data.proximaPagina
			this.confListaContactos.totalDatos = data.totalDatos

			data.lista.forEach(participante => {
				const perfilAux = participante.contactoDe
				const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
					CodigosCatalogoTipoAlbum.PERFIL,
					perfilAux.album
				)

				if (!album || album === null) {
					throw new Error('')
				}

				if (
					this.historialPerfilActual.perfilANavegar.indexOf(perfilAux._id) < 0 &&
					this.perfilSeleccionado._id !== perfilAux._id
				) {
					const urlMedia = album.portada.principal.url
					const usoItemCircular: UsoItemCircular = (album.portada.principal.fileDefault) ?
						UsoItemCircular.CIRCARITACONTACTODEFECTO :
						UsoItemCircular.CIRCONTACTO

					// (this.historialPerfilActual.perfilANavegar.indexOf(perfilAux._id) >= 0) ? ColorDeBorde.BORDER_AMARILLO : ColorDeBorde.BORDER_ROJO,

					this.confListaContactos.lista.push(
						this.metodosParaFotos.configurarItemCircular(
							urlMedia,
							ColorDeBorde.BORDER_ROJO,
							ColorDeFondo.FONDO_BLANCO,
							false,
							usoItemCircular,
							false,
							perfilAux._id,
						)
					)
				}
			})

			this.confListaContactos.cargando = false
		} catch (error) {
			this.confListaContactos.cargando = false
			this.confListaContactos.proximaPagina = false
			this.toast.abrirToast('text31')
		}
	}

	async abrirResumenPerfil(idContacto: string) {
		try {
			if (!idContacto || idContacto.length === 0) {
				return
			}

			this.confResumenPerfil.mostrar = true
			this.confResumenPerfil.loader = true

			const perfilBasico = await this.perfilNegocio.obtenerPerfilBasico(idContacto, this.perfilSeleccionado._id).toPromise()

			const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.PERFIL,
				perfilBasico.album
			)

			if (!album || album === null) {
				return
			}

			const urlMedia = album.portada.principal.url
			const usoDelItem = (album.portada.principal.fileDefault) ? UsoItemCircular.CIRCARITACONTACTODEFECTO : UsoItemCircular.CIRCONTACTO

			let sonContactos: boolean = false
			if (perfilBasico.participanteAsociacion) {
				sonContactos = (perfilBasico.participanteAsociacion.estado.codigo === CodigoEstadoParticipanteAsociacion.CONTACTO)
			}

			this.configurarResumenPerfil(
				idContacto,
				true,
				true,
				perfilBasico.nombreContacto,
				urlMedia,
				usoDelItem,
				perfilBasico.pensamientos,
				sonContactos
			)

			this.confResumenPerfil.loader = false

		} catch (error) {
			this.configurarResumenPerfil()
		}
	}

	eventoTapNoticia(idNoticia: string) {


		try {
			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_NOTICIAS) {
				this.abrirNoticia(idNoticia)
				return
			}

			if (this.confListaNoticias.lista.length <= 2) {
				this.abrirNoticia(idNoticia)
				return
			}

			this.historialPerfilActual.modoDelPerfil = ModoDelPerfil.AMPLIAR_NOTICIAS
			this.confListaNoticias.lista = []
			this.obtenerNoticiasConPaginacion()
		} catch (error) {
		}
	}

	async obtenerNoticiasConPaginacion() {
		try {
			if (!this.confListaNoticias.proximaPagina) {
				return
			}

			this.confListaNoticias.cargando = true
			this.confListaNoticias.paginaActual += 1
			let perfil = this.perfilSeleccionado._id
			let traducir: boolean = false

			if (perfil === this.perfil._id) {
				traducir = false
			} else {
				traducir = true
			}
			const noticias = await this.noticiaNegocio.obtenerNoticiasMapeadasDelPerfil(
				this.perfil._id,
				10,
				this.confListaNoticias.paginaActual,
				traducir
			).toPromise()

			this.confListaNoticias.proximaPagina = noticias.proximaPagina

			noticias.lista.forEach(noticia => {
				this.confListaNoticias.lista.push(
					this.metodosParaFotos.configurarItemNoticia(noticia)
				)
			})

			this.confListaNoticias.cargando = false
		} catch (error) {
			this.confListaNoticias.cargando = false
			this.confListaNoticias.proximaPagina = false
			this.toast.abrirToast('text31')
		}
	}

	abrirNoticia(idNoticia: string) {
		try {
			this.actualizarHitorialDelPerfilActual()

			const ruta = RutasLocales.MODULO_NOTICIAS.toString()
			let rutaAux = RutasNoticias.VISITAR.toString()

			if (this.params.esPropietario) {
				rutaAux = RutasNoticias.ACTUALIZAR.toString()
			}

			rutaAux = rutaAux.replace(':id', idNoticia)
			this.router.navigateByUrl(ruta + '/' + rutaAux)
		} catch (error) {
		}
	}

	eventoTapProyecto(idProyecto: string) {
		try {
			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_PROYECTOS) {
				this.abrirProyecto(idProyecto)
				return
			}

			if (this.confListaProyectos.lista.length <= 2) {
				this.abrirProyecto(idProyecto)
				return
			}

			this.historialPerfilActual.modoDelPerfil = ModoDelPerfil.AMPLIAR_PROYECTOS
			this.confListaProyectos.lista = []
			this.obtenerProyectosConPaginacion()
		} catch (error) {
		}
	}

	async obtenerProyectosConPaginacion() {
		try {
			if (!this.confListaProyectos.proximaPagina) {
				return
			}

			this.confListaProyectos.cargando = true
			this.confListaProyectos.paginaActual += 1
			let perfil = this.perfilSeleccionado._id

			let traducir: boolean = false

			if (perfil === this.perfil._id) {
				traducir = false
			} else {
				traducir = true
			}

			const proyectos = await this.proyectoNegocio.obtenerProyectosMapeadosDelPerfil(
				this.perfil._id,
				10,
				this.confListaProyectos.paginaActual,
				traducir
			).toPromise()

			this.confListaProyectos.proximaPagina = proyectos.proximaPagina

			proyectos.lista.forEach(proyecto => {
				const configuracion = this.metodosParaFotos.configurarItemProyecto(proyecto, false)
				const index = this.proyectosNotificaciones.findIndex(e => e.proyecto._id === proyecto.id)
				configuracion.mostrarCorazon = (index >= 0)

				this.confListaProyectos.lista.push(configuracion)
			})

			this.confListaProyectos.cargando = false
		} catch (error) {
			this.confListaProyectos.cargando = false
			this.confListaProyectos.proximaPagina = false
			this.toast.abrirToast('text31')
		}
	}

	async abrirProyecto(idProyecto: string) {
		try {
			if (this.params.esPropietario) {
				this.toast.abrirToast('', true)
				await this.notificacionesService.actualizarEstadoNotificacionesProyecto(
					this.proyectosNotificaciones,
					this.dataNotificacion,
					idProyecto
				)
			}

			this.actualizarHitorialDelPerfilActual()

			const ruta = RutasLocales.MODULO_PROYECTOS.toString()
			let rutaAux = RutasProyectos.VISITAR.toString()

			if (this.params.esPropietario) {
				rutaAux = RutasProyectos.ACTUALIZAR.toString()
			}

			this.toast.cerrarToast()
			rutaAux = rutaAux.replace(':id', idProyecto)
			this.router.navigateByUrl(ruta + '/' + rutaAux)
		} catch (error) {
		}
	}

	detectarScroolEnListaContactos() {
		try {
			const elemento = document.getElementById(this.idCapaListaContactos) as HTMLElement
			if (!elemento) {
				return

			}

			if (elemento.scrollTop === 0) {
				return
			}

			if (
				elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight &&
				!this.confListaContactos.cargando
			) {
				if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_CONTACTOS) {
					this.obtenerContactosConPaginacion((this.params.esPropietario) ? CodigoEstadoParticipanteAsociacion.INVITADO : CodigoEstadoParticipanteAsociacion.CONTACTO)
					return
				}
			}
		} catch (error) {
		}
	}

	detectarScroolEnListaNoticias() {
		try {
			const elemento = document.getElementById(this.idCapaListaNoticias) as HTMLElement
			if (!elemento) {
				return

			}

			if (elemento.scrollTop === 0) {
				return
			}

			if (
				elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight &&
				!this.confListaNoticias.cargando
			) {
				if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_NOTICIAS) {
					this.obtenerNoticiasConPaginacion()
					return
				}
			}
		} catch (error) {
		}
	}

	detectarScroolEnListaProyectos() {
		try {
			const elemento = document.getElementById(this.idCapaListaProyectos) as HTMLElement
			if (!elemento) {
				return

			}

			if (elemento.scrollTop === 0) {
				return
			}

			if (
				elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 1.78 &&
				!this.confListaProyectos.cargando
			) {
				if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_PROYECTOS) {
					this.obtenerProyectosConPaginacion()
					return
				}
			}
		} catch (error) {
		}
	}

	irAOtroPerfil(idContacto: string) {
		if (!idContacto || idContacto.length === 0) {
			return
		}

		this.origenFlechaBack = true
		this.actualizarHitorialDelPerfilActual(idContacto)

		const ruta = RutasLocales.MODULO_PERFILES.toString()
		const rutaAux = RutasPerfiles.PERFIL.toString().replace(':id', idContacto)
		this.router.navigateByUrl(ruta + '/' + rutaAux)
	}

	async cambiarEstadoAsociacion(
		estado: CodigoEstadoParticipanteAsociacion = CodigoEstadoParticipanteAsociacion.RECHAZADA
	) {
		try {
			const participante = this.perfil.participanteAsociacion

			if (!participante) {
				throw new Error('')
			}

			this.loaderBotonesEstadoAsociacion = true

			const estatus = await this.participanteAsoNegocio.cambiarEstadoAsociacion(
				participante.asociacion._id,
				estado,
				this.perfilSeleccionado._id
			).toPromise()

			if (!estatus) {
				throw new Error('')
			}

			this.loaderBotonesEstadoAsociacion = false
			this.confDone.mostrarDone = true




			setTimeout(() => {
				// this._location.back()
				this.confDone.mostrarDone = false
				window.history.go(-2)
				// this.confDone.mostrarDone = false
				// this.router.navigateByUrl(
				// 	RutasLocales.MODULO_PERFILES
				// 	+ '/' +
				// 	RutasPerfiles.PERFIL.toString().replace(':id', this.perfil._id)
				// )

			}, 1500)
		} catch (error) {
			this.loaderBotonesEstadoAsociacion = false
			this.toast.abrirToast('text37')
		}
	}

	async enviarInvitacion() {
		try {
			this.loaderBotonesEstadoAsociacion = true

			const participantes = [
				{ perfil: { _id: this.perfilSeleccionado._id } },
				{ perfil: { _id: this.perfil._id } }
			]

			const estatus = await this.participanteAsoNegocio.crearAsociacion(
				'Asociacion de contacto',
				TipoParticipanteAsociacion.CONTACTO,
				participantes
			).toPromise()

			if (!estatus) {
				return
			}

			this.loaderBotonesEstadoAsociacion = false
			this.confDone.mostrarDone = true
			setTimeout(() => {
				// this._location.back()
				// this.confDone.mostrarDone = false
				this.confDone.mostrarDone = false
				window.history.go(-2)
				// this.router.navigateByUrl(
				// 	RutasLocales.MODULO_PERFILES
				// 	+ '/' +
				// 	RutasPerfiles.PERFIL.toString().replace(':id', this.perfil._id)
				// )
			}, 1500)
		} catch (error) {
			this.toast.abrirToast('text37')
		}
	}

	async validarHistorialDelPerfilEnMemoria() {
		try {
			this.historialPerfiles = this.obtenerHistorialDelSession()

			if (!this.historialPerfiles || this.historialPerfiles === null) {
				this.historialPerfiles = []
			}

			const index = this.historialPerfiles.findIndex(e => e.perfilActivo === this.historialPerfilActual.perfilActivo)

			if (index < 0) {
				this.historialPerfiles.push(this.historialPerfilActual)
				this.guardarHistorialEnSession(this.historialPerfiles)
				return
			}

			this.historialPerfilActual = this.historialPerfiles[index]

			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.DEFAULT) {
				return
			}

			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_CONTACTOS) {
				this.confListaContactos.lista = []
				this.mostrarLoaoder = false
				this.obtenerContactosConPaginacion((this.params.esPropietario) ? CodigoEstadoParticipanteAsociacion.INVITADO : CodigoEstadoParticipanteAsociacion.CONTACTO)
				return
			}

			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_NOTICIAS) {
				this.confListaNoticias.lista = []
				this.mostrarLoaoder = false
				this.obtenerNoticiasConPaginacion()
				return
			}

			if (this.historialPerfilActual.modoDelPerfil === ModoDelPerfil.AMPLIAR_PROYECTOS) {
				this.confListaProyectos.lista = []
				this.mostrarLoaoder = false
				this.obtenerProyectosConPaginacion()
				return
			}
		} catch (error) {
		}
	}

	actualizarHitorialDelPerfilActual(
		perfilANavegar: string = ''
	) {
		try {
			if (perfilANavegar.length > 0) {
				this.historialPerfilActual.perfilANavegar.push(perfilANavegar)
			}

			const index = this.historialPerfiles.findIndex(e => e.perfilActivo === this.historialPerfilActual.perfilActivo)
			if (index < 0) {
				return
			}

			this.historialPerfiles[index] = this.historialPerfilActual
			this.guardarHistorialEnSession(this.historialPerfiles)
		} catch (error) {
		}
	}

	removerHistorialActualDelEnSession() {
		try {
			this.perfilNegocio.removerPerfilActivoDelSessionStorage()
			const index = this.historialPerfiles.findIndex(e => e.perfilActivo === this.historialPerfilActual.perfilActivo)
			if (index < 0) {
				return
			}

			this.historialPerfiles.splice(index, 1)
			this.guardarHistorialEnSession(this.historialPerfiles)

			if (this.historialPerfiles.length === 0) {
				sessionStorage.removeItem(LlavesSessionStorage.HITORIAL_PERFILES)
			}
		} catch (error) {
		}
	}

	obtenerHistorialDelSession() {
		return JSON.parse(sessionStorage.getItem(LlavesSessionStorage.HITORIAL_PERFILES.toString()))
	}

	guardarHistorialEnSession(
		historialPerfiles: Array<HistorialPerfil>
	) {
		sessionStorage.setItem(LlavesSessionStorage.HITORIAL_PERFILES, JSON.stringify(historialPerfiles))
	}

	irAMisPensamientos() {
		if (!this.params.esPropietario) {
			return
		}

		if (
			this.perfil.pensamientos &&
			this.perfil.pensamientos.length === 0
		) {
			this.router.navigateByUrl(RutasLocales.MODULO_PENSAMIENTO)
		}

		this.router.navigateByUrl(RutasLocales.MODULO_PENSAMIENTO)
	}

	// Notificaciones

	configurarDataNotificaciones() {
		this.dataNotificacion = {
			idPropietario: this.perfilSeleccionado._id,
			nivel: CodigosCatalogoEntidad.PERFIL,
			codigoEntidad: CodigosCatalogoEntidad.NINGUNA,
			leido: false,
		}
	}

	async configurarEscuchaNotificaciones() {
		this.notificacionesService.notificaciones$.subscribe(data => { 
			const aux: Array<NotificacionFirebaseModel> = []

			data.forEach(e => {

				aux.push({
					id: e.key,
					...e.payload.val() as Object
				})
			})

			this.notificaciones = this.notificacionesService.filtrarNotificacionesPorCodigoEntidad(aux)

			// Validar que acciones ejecutar segun el codigo de la entidad y la accion de la notificacion
			this.notificaciones.forEach(grupo => {
				this.validarAccionSegunNotificaciones(grupo)
			})
		})
	}

	configurarNotificaciones() {

		if (!this.params.esPropietario) {
			this.notificacionesService.desconectarDeEscuchaNotificaciones()
			return
		}

		this.configurarDataNotificaciones()
		this.configurarEscuchaNotificaciones()

		this.notificacionesService.obtenerNotificaciones$.next(this.dataNotificacion)
	}


	// Notificaciones


	validarAccionSegunNotificaciones(
		grupo: NotificacionPorCodigoEntidad
	) {
		switch (grupo.codigoEntidad) {
			case CodigosCatalogoEntidad.ASOCIACION:
				this.notificacionesDeEntidadAsociacion(grupo.notificaciones)
				break
			case CodigosCatalogoEntidad.COMENTARIO:
				this.notificacionesDeEntidadProyecto(grupo.notificaciones)
				break
			default: break
		}
	}

	async notificacionesDeEntidadAsociacion(
		notificaciones: Array<NotificacionFirebaseModel>
	) {
		try {

			const asociciones = this.notificacionesService.convertirNotificacionesEnArrayDeAsociaciones(notificaciones)

			asociciones.forEach(a => {
				const participante = a.participantes[0]
				const index = this.confListaContactos.lista.findIndex(e => e.id === participante.perfil._id)


				if (
					participante.estado.codigo === CodigoEstadoParticipanteAsociacion.INVITANTE &&
					index < 0
				) {
					this.confListaContactos.lista.unshift(
						this.configurarItemParaListaContactos(
							participante.perfil
						)
					)
				}
			})

			this.notificacionesService.actualizarEstadoNotificaciones(notificaciones, this.dataNotificacion)
		} catch (error) {
		}
	}

	async notificacionesDeEntidadProyecto(
		notificaciones: Array<NotificacionFirebaseModel>
	) {
		try {
			const data = this.notificacionesService.convertirNotificacionesEnArrayDeProyectoNotificacion(notificaciones)

			data.forEach(d => {
				const index = this.proyectosNotificaciones.findIndex(e => e.proyecto._id === d.proyecto._id)
				if (index < 0) {
					this.proyectosNotificaciones.push(d)
					const indexDos = this.confListaProyectos.lista.findIndex(e => e.id === d.proyecto._id)

					if (indexDos >= 0) {
						this.confListaProyectos.lista[indexDos].mostrarCorazon = true
					}
				}
			})
		} catch (error) {
		}
	}

	configurarItemParaListaContactos(
		perfil: PerfilModel
	): ItemCircularCompartido {
		const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			CodigosCatalogoTipoAlbum.PERFIL,
			perfil.album
		)

		if (!album || album === null) {
			throw new Error('')
		}

		const urlMedia = album.portada.principal.url
		const usoItemCircular: UsoItemCircular = (album.portada.principal.fileDefault) ?
			UsoItemCircular.CIRCARITACONTACTODEFECTO :
			UsoItemCircular.CIRCONTACTO

		return this.metodosParaFotos.configurarItemCircular(
			urlMedia,
			ColorDeBorde.BORDER_ROJO,
			ColorDeFondo.FONDO_BLANCO,
			false,
			usoItemCircular,
			false,
			perfil._id,
		)
	}

}

export enum ModoDelPerfil {
	DEFAULT = 'default',
	AMPLIAR_CONTACTOS = 'amp_cnt',
	AMPLIAR_NOTICIAS = 'amp_not',
	AMPLIAR_PROYECTOS = 'amp_pro'
}

export interface HistorialPerfil {
	modoDelPerfil: ModoDelPerfil,
	perfilActivo: string,
	perfilANavegar: Array<string>
}



const TIPO_PERFIL_EN = {
	'TIPERFIL_1': 'CLASSIC PROFILE',
	'TIPERFIL_2': 'PLAYFUL PROFILE',
	'TIPERFIL_3': 'SUBSTITUTE PROFILE',
	'TIPERFIL_4': 'GROUP PROFILE',
}

const TIPO_PERFIL_ES = {
	'TIPERFIL_1': 'PERFIL CLÁSICO',
	'TIPERFIL_2': 'PERFIL LÚDICO',
	'TIPERFIL_3': 'PERFIL SUSTITUTO',
	'TIPERFIL_4': 'PERFIL DE GRUPO',
}

const TIPO_PERFIL_DE = {
	'TIPERFIL_1': 'KLASSISCHES PROFIL',
	'TIPERFIL_2': 'SPIELERISCHES PROFIL',
	'TIPERFIL_3': 'ERSATZ-PROFIL',
	'TIPERFIL_4': 'GRUPPEN-PROFIL',
}

const TIPO_PERFIL_FR = {
	'TIPERFIL_1': 'PROFIL CLASSIQUE',
	'TIPERFIL_2': 'PROFIL LUDIQUE',
	'TIPERFIL_3': 'PROFIL DE REMPLACEMENT',
	'TIPERFIL_4': 'PROFIL DE GROUPE',
}

const TIPO_PERFIL_IT = {
	'TIPERFIL_1': 'PROFILO CLASSICO',
	'TIPERFIL_2': 'PROFILO LUDICO',
	'TIPERFIL_3': 'PROFILO SOSTITUTO',
	'TIPERFIL_4': 'PROFILO DI GRUPPO',
}

const TIPO_PERFIL_PO = {
	'TIPERFIL_1': 'PERFIL CLÁSSICO',
	'TIPERFIL_2': 'PERFIL LÚDICO',
	'TIPERFIL_3': 'PERFIL SUBSTITUTO',
	'TIPERFIL_4': 'PERFIL DE GRUPO',
}