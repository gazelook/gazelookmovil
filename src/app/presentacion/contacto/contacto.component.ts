import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import {
  ColorFondoLinea,
  EspesorLineaItem,
  EstiloErrorInput
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent

  public perfilSeleccionado: PerfilModel
  public confLinea: LineaCompartida
  public contactoForm: FormGroup
  public inputsForm: Array<InputCompartido> = []
  public botonEnviar: BotonCompartido
  public mensaje: string
  public tema: string;
  public confAppBar: ConfiguracionAppbarCompartida
  public confToast: ConfiguracionToast
  public mensajeCorreo: boolean;
  public errorMsj: boolean;
  public contactUs: boolean
  public textoMensaje: string;
  public textoMensajeError: string;
  constructor(
    private perfilNegocio: PerfilNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private _location: Location,
    private generadorId: GeneradorId,
    private formBuilder: FormBuilder,
    private cuentaNegocio: CuentaNegocio,
    private variablesGlobales: VariablesGlobales,
    private translateService: TranslateService,
  ) {
    this.mensaje = ''
    this.tema = ''
    this.contactUs = true
    this.mensajeCorreo = false
    this.errorMsj = false
    this.textoMensaje = '';
    this.textoMensajeError = '';
  }
   
  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = true
    this.obtenerPerfil()
    if (this.perfilSeleccionado) {
      this.prepararAppBar()
      this.configuracionBotones()
      this.configurarToast()
      this.contactoForm = this.inicializarControlesFormularioAccionCrear()
      this.inputsForm = this.configurarInputsDelFormulario(this.contactoForm)
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }

  obtenerPerfil() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  prepararAppBar() {
    this.confAppBar = {
      accionAtras: () => {
        this._location.back()
      },
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v11texto1'
        },
      }
    }
  }

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false
    }
  }

  configuracionBotones() {
    this.botonEnviar = {
      text: 'm6v11texto10',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      ejecutar: () => {
        this.enviarCorreoDeContacto()
      },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };
  }

  inicializarControlesFormularioAccionCrear(proyecto?): FormGroup {
    const registroForm: FormGroup = this.formBuilder.group({
      contacName: ['',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z0-9 ]+$'),
          Validators.minLength(3)
        ],
      ],
      name: ['',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],
      email: ['',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(3)
        ]
      ],

    
      message: ['', Validators.required]
    })

    return registroForm
  }

  configurarInputsDelFormulario(proyectoForm: FormGroup): Array<InputCompartido> {
    let inputsForm: Array<InputCompartido> = []

    if (proyectoForm) {
      // 0
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        ocultarMensajeError: true,
        placeholder: 'm6v11texto5',
        data: proyectoForm.controls.contacName,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false 
      })
      // 1
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        ocultarMensajeError: true,
        placeholder: 'm6v11texto6',
        data: proyectoForm.controls.name,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      })
      // 2
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        ocultarMensajeError: true,
        placeholder: 'm6v11texto7',
        data: proyectoForm.controls.email,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      })
    
    }
    return inputsForm
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
    }
  }

  validarCampos() {
    const { contacName, name, email } = this.contactoForm.value
    const mensajeEnviar = this.mensaje

    if (!(contacName.length > 0)) {
      return false
    }

    if (!(name.length > 0)) {
      return false
    }

    if (!(email.length > 0)) {
      return false
    }
    if (!(this.tema.length > 0)) {
      return false
    }

    if (!(mensajeEnviar.length > 0)) {
      return false
    }

    return true
  }

  async enviarCorreoDeContacto() {
    this.botonEnviar.enProgreso = true
    this.textoMensaje = await this.translateService.
    get('m6v12texto13').toPromise();

    if (!this.validarCampos()) {
      this.toast.abrirToast('text4')
      this.botonEnviar.enProgreso = false
      return
    }

    const { contacName, name, email } = this.contactoForm.value
    const mensajeEnviar = this.mensaje

    const correo: CorreoContacto = {
      nombre: name,
      nombreContacto: contacName,
      email: email,
      tema: this.tema,
      mensaje: this.mensaje,
    }

    try {
      const status: string = await this.cuentaNegocio.enviarEmailDeContacto(correo).toPromise()

      if (!status) {
        throw new Error('')
      }
     
      this.botonEnviar.enProgreso = false
      this.mensajeCorreo = true;
      this.contactUs = false;
      this.contactoForm.reset()
      this.mensaje = ''
    } catch (error) {
      // this.toast.abrirToast('text37')
      this.errorMsj = true;
      this.botonEnviar.enProgreso = false
      this.textoMensajeError = await this.translateService.
        get('text4').toPromise();
    }
  }
}

export interface CorreoContacto {
  nombre?: string,
  nombreContacto?: string,
  email?: string,
  pais?: string,
  direccion?: string,
  tema?: string,
  mensaje?: string,
}
