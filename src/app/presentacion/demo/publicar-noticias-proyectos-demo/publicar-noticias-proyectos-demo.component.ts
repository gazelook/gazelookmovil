import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { RutasDemo } from './../rutas-demo.enum';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { Router } from '@angular/router';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { Component, OnInit } from '@angular/core';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
    ColorFondoLinea,
    EspesorLineaItem,
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { Location } from '@angular/common';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';

@Component({
	selector: 'app-publicar',
	templateUrl: './publicar-noticias-proyectos-demo.component.html',
	styleUrls: ['./publicar-noticias-proyectos-demo.component.scss']
})
export class PublicarNoticiasProyectosDemoComponent implements OnInit {

	public confAppBar: ConfiguracionAppbarCompartida
	public confLineaVerde: LineaCompartida
	public confLineaVerdeAll: LineaCompartida
	public confDialogoInicial: DialogoCompartido

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private cuentaNegocio: CuentaNegocio,
		private perfilNegocio: PerfilNegocio,
		private router: Router,
		private _location: Location
	) { }

	ngOnInit(): void {
		this.configurarAppBar()
		this.configurarLinea()
		this.configurarDialogo()
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => this.accionAtras(),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v10texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
			}
		}
	}

	accionAtras() {
		this._location.back()
	}

	configurarLinea() {
		this.confLineaVerde = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}

		this.confLineaVerdeAll = {
			ancho: AnchoLineaItem.ANCHO6920,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
	}

	configurarDialogo() {
		this.confDialogoInicial = {
			completo: true,
			tipo: TipoDialogo.INFO_VERTICAL,
			mostrarDialogo: false,
			descripcion: 'm1v10texto3', 
		}
	}

	irACrearProyectoSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
		let ruta = RutasLocales.MODULO_DEMO.toString()
		let publicar = RutasDemo.PUBLICAR_PROYECTO.toString()
		publicar = publicar.replace(':codigoTipoProyecto', codigo)
		this.router.navigateByUrl(ruta + '/' + publicar)
	}

	irAInformacionUtilDeProyectos() {
		let ruta = RutasLocales.MODULO_DEMO.toString()
		let informacion = RutasDemo.INFORMACION_UTIL.toString()
		this.router.navigateByUrl(ruta + '/' + informacion)
	}

	irACrearNoticia() {
		let ruta = RutasLocales.MODULO_DEMO.toString()
		let informacion = RutasDemo.PUBLICAR_NOTICIA.toString()
		this.router.navigateByUrl(ruta + '/' + informacion)
	}

	navegarSegunMenu(menu: number) {
		switch (menu) {
			case 0:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case 1:
				this.irACrearProyectoSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_PAIS)
				break
			case 2:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case 3:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case 4:
				this.irACrearNoticia()
				break
			case 5:
				this.irAInformacionUtilDeProyectos()
				break
		}
	}
}

