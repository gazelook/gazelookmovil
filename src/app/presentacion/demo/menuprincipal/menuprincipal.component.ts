import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { ColorFondoItemMenu } from '@shared/diseno/enums/estilos-colores-general';
import {
  TamanoColorDeFondo,
  TamanoItemMenu,
  TamanoLista,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { ItemMenuCompartido } from '@shared/diseno/modelos/item-menu.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import {
  ItemAccion,
  ItemMenuModel,
  ItemSubMenu,
} from '../../../dominio/modelo/entidades/item-menu.model';
import {
  ColorTextoBoton,
  TipoBoton,
} from '@shared/componentes/button/button.component';
import {
  TamanoDeTextoConInterlineado,
  TamanoPortadaGaze,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionLineaVerde } from '@shared/diseno/modelos/lista-contactos.interface';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';

@Component({
  selector: 'app-menuprincipal',
  templateUrl: './menuprincipal.component.html',
  styleUrls: ['./menuprincipal.component.scss'],
})
export class MenuprincipalComponent implements OnInit {
  configuracionAppBar: ConfiguracionAppbarCompartida;
  listaMenu: ItemMenuModel[];
  itemSubMenu3Puntos: ItemSubMenu;
  itemMenuMultipleAccion: ItemMenuModel;
  sesionIniciada = false;
  public confLinea: ConfiguracionLineaVerde;
  public mostrarLinea: boolean;
  dataLista: DatosLista = {
    cargando: false,
    reintentar: () => {},
    tamanoLista: TamanoLista.TIPO_MENU_PRINCIPAL,
  };
  perfilSeleccionado: PerfilModel;
  configuracionPortada: PortadaGazeCompartido;
  configDialogoLateral: DialogoCompartido;
  botonEnter: BotonCompartido;
  public confBoton: BotonCompartido;
  public confDialogoConstruccion: DialogoCompartido;

  constructor(
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private _location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private perfilNegocio: PerfilNegocio,
    private cuentaNegocio: CuentaNegocio,
    private variablesGlobales: VariablesGlobales,
    private location: Location
  ) {
    this.mostrarLinea = false;
    this.prepararItemsMenu();
    this.prepararDataSubMenu3puntos();
    this.prepararDatosParaMenuMultipleAccion();
    this.configurarBotonPrincipal();
    this.configurarDialogos();
  }

  ngOnInit(): void {
    this.configuracionPortada = { tamano: TamanoPortadaGaze.PORTADACOMPLETA };
    this.sesionIniciada = this.cuentaNegocio.sesionIniciada();
    this.variablesGlobales.mostrarMundo = true;
    this.cargarDatos();
    this.prepararAppBar();
    this.configurarLineas();
    this.configurarDialogoDemo();
    // this.configuracionAppBar.gazeAppBar.subtituloDemo =
    //   this.obtenerTituloPrincipal(false);
  }

  async configurarBotonPrincipal() {
    this.confBoton = {
      text: 'm1v4texto1',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        this.router.navigateByUrl(RutasLocales.MENU_PERFILES);
      },
    };

    //this.confBoton.text = await this.servicioIdiomas.obtenerTextoLlave(this.configuracion.demoAppbar.boton.llaveTexto ? this.configuracion.demoAppbar.boton.llaveTexto : 'undefined')
  }

  configurarLineas() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
    };
  }
  configurarDialogoDemo() {
    this.configDialogoLateral = {
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      completo: true,
      descripcion: 'm1v3texto3',
    };
  }
  cargarDatos() {
    this.botonEnter = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m1v1texto6'),
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.navegarMenuPrincipal(),
    };
  }
  navegarMenuPrincipal() {}

  async prepararAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      accionAtras: () => this.clickBotonAtras(),
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v4texto2',
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
      },
    };
  }

  configurarDialogos(): void {
    this.confDialogoConstruccion = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.INFO_VERTICAL,
      descripcion: 'm7v4texto3',
    };
  }

  obtenerTituloPrincipal(profileCreated: boolean) {
    if (profileCreated) {
      return {
        mostrar: true,
        llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre,
      };
    } else {
      return {
        mostrar: true,
        llaveTexto: 'm1v3texto2',
      };
    }
  }

  async prepararItemsMenu() {
    this.listaMenu = [
      {
        id: MenuPrincipal.MIS_PENSAMIENTOS,
        titulo: ['m1v3texto1'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.MENU_PERFILES.toString(),
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.MIS_PENSAMIENTOS,
        titulo: ['m2v11texto7', 'm2v11texto8', 'm2v11texto9'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.CREAR_PENSAMIENTO.toString(),
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.GAZING,
        titulo: ['m2v11texto10', 'm2v11texto11'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.MIS_CONTACTOS.toString(),
        tipo: TipoMenu.ACCION,
      },

      {
        id: MenuPrincipal.PUBLICAR,
        titulo: ['m2v11texto12', 'm2v11texto13', 'm2v11texto14'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.MENU_PUBLICAR_PROYECTOS_NOTICIAS.toString(),
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.PROYECTOS,
        titulo: ['m2v11texto15', 'm2v11texto16'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.MENU_LISTA_TIPO_PROYECTOS.toString(),
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.NOTICIAS,
        titulo: ['m2v11texto17', 'm2v11texto18'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.NOTICIAS_USUARIOS.toString(),
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.COMPRAS,
        titulo: ['m2v11texto19', 'm2v11texto19.1'],
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.COMPRAS_INTER.toString(),
        tipo: TipoMenu.ACCION,
      },
      {
        id: MenuPrincipal.FINANZAS,
        titulo: ['m2v11texto21'],
        tipo: TipoMenu.ACCION,
        ruta:
          RutasLocales.MODULO_DEMO.toString() +
          '/' +
          RutasDemo.FINANZAS.toString(),
      },
      {
        id: MenuPrincipal.ANUNCIOS,
        titulo: ['m2v11texto22', 'm2v11texto23'],
        tipo: TipoMenu.ANUNCIOS,
      },
    ];
  }

  prepararItemMenu(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo:
        item.id == MenuPrincipal.ANUNCIOS
          ? ColorFondoItemMenu.TRANSPARENTE
          : ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo[0] ? item.titulo[0].toString() : null,
      texto2: item.titulo[1] ? item.titulo[1].toString() : null,
      texto3: item.titulo[2] ? item.titulo[2].toString() : null,
      tipoMenu: item.tipo,
      linea: {
        mostrar: true,
        configuracion: {
          ancho:
            item.id == MenuPrincipal.ANUNCIOS ||
            item.id == MenuPrincipal.FINANZAS
              ? AnchoLineaItem.ANCHO6028
              : AnchoLineaItem.ANCHO6382,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
          cajaGaze: item.id == MenuPrincipal.ANUNCIOS,
        },
      },
      gazeAnuncios: item.id == MenuPrincipal.ANUNCIOS,
      idInterno: item.id,
      onclick: () => this.navigationSubMenu(item.ruta),
      dobleClick: () => {},
    };
  }

  async prepararDataSubMenu3puntos() {
    this.itemSubMenu3Puntos = {
      id: '',
      titulo: '',
      mostrarDescripcion: false,
      menusInternos: [
        {
          id: 'mya',
          titulo: ['m2v11texto24'],
          action: () => {},
        },
        {
          id: 'he',
          titulo: ['m2v11texto25'],
          action: () => {},
        },
        {
          id: 'faq',
          titulo: ['m2v11texto26'],
          action: () => {},
        },
        {
          id: 'our',
          titulo: ['m2v11texto27'],
          action: () => {},
        },
        {
          id: 'web',
          titulo: ['m2v11texto28'],
          action: () => {},
        },
      ],
    };
  }

  navigationSubMenu(ruta: RutasLocales) {
    if (
      ruta ===
      RutasLocales.MODULO_DEMO.toString() +
        '/' +
        RutasDemo.COMPRAS_INTER.toString()
    ) {
      this.confDialogoConstruccion.mostrarDialogo = true;

      return;
    }
    if (ruta === RutasLocales.GAZING) {
      let ruta = RutasLocales.GAZING.toString();
      // let misContactos = RutasGazing.LISTAR.toString()
      // this.router.navigateByUrl(ruta + '/' + misContactos)
    } else if (ruta) {
      this.router.navigateByUrl(ruta.toString());
    } else {
      this.configDialogoLateral.mostrarDialogo = true;
    }
  }

  prepararItemSubMenu(item: ItemSubMenu): ItemMenuCompartido {
    return {
      id: '',
      submenus: item.menusInternos ? item.menusInternos : [],
      mostrarDescripcion: item.mostrarDescripcion,
      tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      texto1: item.titulo,
      tipoMenu: TipoMenu.SUBMENU,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => {
        this.configDialogoLateral.mostrarDialogo = true;
      },
      dobleClick: () => {},
    };
  }

  mostrarDescripcion(item: any) {
    const elemento: HTMLElement = document.getElementById(
      'flecha' + item.id
    ) as HTMLElement;
    if (item.mostrarDescripcion) {
      item.mostrarDescripcion = false;
      elemento.classList.remove('rotar-flecha');
    } else {
      item.mostrarDescripcion = true;
      elemento.classList.add('rotar-flecha');
    }
  }

  scroolEnContenedor() {
    const elemento = document.getElementById(
      'contenedorListaMenuPrincipal'
    ) as HTMLElement;
    if (!elemento) {
      return;
    }

    if (elemento.scrollTop < 30) {
      this.mostrarLinea = false;
      return;
    }

    if (elemento.scrollTop > 30) {
      this.mostrarLinea = true;
      return;
    }

    this.mostrarLinea = elemento.scrollTop >= 30;
  }

  prepararDatosParaMenuMultipleAccion() {
    this.itemMenuMultipleAccion = {
      id: 'multip',
      titulo: [
        {
          accion: () => {
            this.configDialogoLateral.mostrarDialogo = true;
          },
          nombre: 'm2v11texto24',
          codigo: 'g',
        },
        {
          accion: () => {
            this.configDialogoLateral.mostrarDialogo = true;
          },
          nombre: 'm2v11texto25',
          codigo: 'm',
        },
        {
          accion: () => {
            this.configDialogoLateral.mostrarDialogo = true;
          },
          nombre: 'm2v11texto26',
          codigo: 'p',
        },
      ],
      tipo: TipoMenu.LEGAL,
    };
  }

  prepararItemsParaMenuMultipleAccion(item: ItemMenuModel): ItemMenuCompartido {
    return {
      id: '',
      mostrarDescripcion: false,
      tamano: TamanoItemMenu.ITEM_MENU_CONTENIDO, // Indica el tamano del item (altura)
      colorFondo: ColorFondoItemMenu.PREDETERMINADO,
      acciones: item.titulo as ItemAccion[],
      tipoMenu: TipoMenu.LEGAL,
      linea: {
        mostrar: true,
        configuracion: {
          ancho: AnchoLineaItem.ANCHO6920,
          espesor: EspesorLineaItem.ESPESOR071,
          colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
          forzarAlFinal: true,
        },
      },
      gazeAnuncios: false,
      idInterno: item.id,
      onclick: () => {},
      dobleClick: () => {},
    };
  }

  clickBotonAtras() {
    this.router.navigateByUrl(RutasLocales.LANDING.toString());
  }
  async cambiarIdioma() {
    this.botonEnter.text =
      await this.internacionalizacionNegocio.obtenerTextoLlave('m1v1texto6');
  }
}

export enum MenuPrincipal {
  MIS_PENSAMIENTOS,
  MIS_CONTACTOS,
  PUBLICAR,
  PROYECTOS,
  NOTICIAS,
  COMPRAS,
  FINANZAS,
  ANUNCIOS,
  GAZING,
}
