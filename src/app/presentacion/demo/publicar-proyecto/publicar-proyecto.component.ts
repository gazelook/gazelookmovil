import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { BuscadorModalComponent } from '@shared/componentes/buscador-modal/buscador-modal.component';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { SelectorComponent } from '@shared/componentes/selector/selector.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';
import { ConfiguracionComentario } from '@shared/diseno/modelos/comentario.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { InfoAccionSelector } from '@shared/diseno/modelos/info-accion-selector.interface';
import { InfoAccionBuscadorLocalidades } from '@shared/diseno/modelos/info-acciones-buscador-localidades.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import {
	ColorDeBorde,
	ColorDeFondo, ColorDelTexto, EspesorDelBorde, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { PortadaExpandidaComponent } from '@shared/componentes/portada-expandida/portada-expandida.component';
import { ConfiguracionBuscador } from '@shared/diseno/modelos/buscador.interface';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos/lista-contacto.interface';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';
import { ResumenDataMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import { ColoresBloquePortada, ConfiguracionPortadaExandida, SombraBloque, TipoBloqueBortada } from '@shared/diseno/modelos/portada-expandida.interface';
import { ConfiguracionVotarEntidad } from '@shared/diseno/modelos/votar-entidad.interface';
import { TipoMonedaNegocio } from 'dominio/logica-negocio/moneda.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { ProyectoService } from '@core/servicios/generales/proyecto.service';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';

@Component({
	selector: 'app-publicar-proyecto',
	templateUrl: './publicar-proyecto.component.html',
	styleUrls: ['./publicar-proyecto.component.scss']
})
export class PublicarProyectoComponent implements OnInit, AfterViewInit {
	@ViewChild('toast', { static: false }) toast: ToastComponent
	@ViewChild('portadaExpandida', { static: false }) portadaExpandida: PortadaExpandidaComponent
	@ViewChild('selectorPaises', { static: false }) selectorPaises: SelectorComponent
	@ViewChild('buscadorLocalidades', { static: false }) buscadorLocalidades: BuscadorModalComponent

	// Utils
	public util = FuncionesCompartidas
	public AccionEntidadEnum = AccionEntidad
	public CodigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum
	public imagenesDefecto: Array<ArchivoModel>
	public portadaUrl: string
	public codigosCatalogoTipoProyecto = CodigosCatalogoTipoProyecto
	public comentarioAEliminar: ComentarioModel
	public idCapaFormulario: string
	public puedeCargarMas: boolean
	public puedeHacerScroolAlFinal: boolean
	public $estatusMensajeCompartirTransferencia: Subject<any>
	public fechaMaximaUpdate: Date
	public paraCompartir: boolean
	public idPerfilCoautorParaResponder: string
	public idInternoParaResponder: string

	// Parametros de la url
	public params: ProyectoParams

	// Configuracion de capas
	public mostrarCapaLoader: boolean
	public mostrarCapaError: boolean
	public mensajeCapaError: string
	public mostrarCapaNormal: boolean

	// Parametros internos
	public catalogoTipoMoneda: Array<CatalogoTipoMonedaModel>
	public perfilSeleccionado: PerfilModel
	public proyecto: ProyectoModel
	public proyectoForm: FormGroup
	public inputsForm: Array<InputCompartido>
	public maxDescripcion: number
	public inputProyectoCompleto: string
	public listaDeFechas: Date[]
	public formatoFecha: string
	public listaComentarios: PaginacionModel<ConfiguracionComentario>
	public listaComentariosModel: ComentarioModel[]
	public listaContactos: PaginacionModel<ParticipanteAsociacionModel>
	public listaContactosSeleccionados: Array<string>
	public listaContactosOriginal: Array<ConfiguracionItemListaContactosCompartido>
	public listaContactosBuscador: Array<ConfiguracionItemListaContactosCompartido>

	// Configuraciones
	public confToast: ConfiguracionToast
	public confAppbar: ConfiguracionAppbarCompartida
	public confPortadaExpandida: ConfiguracionPortadaExandida
	public confSelector: ConfiguracionSelector
	public confBuscador: ConfiguracionBuscadorModal
	public confMonedaPicker: ConfiguracionMonedaPicker
	public confBotonHistorico: BotonCompartido
	public confBotonCompartir: BotonCompartido
	public confBotonFullProject: BotonCompartido
	public confBotonLinks: BotonCompartido
	public confBotonPublish: BotonCompartido
	public confBotonTransferir: BotonCompartido
	public confBotonEliminar: BotonCompartido
	public confLineaComentarios: LineaCompartida
	public confBarraInferior: ConfiguracionBarraInferiorInline
	public confDialogoEliminarProyecto: DialogoCompartido
	public confDialogoEliminarComentario: DialogoCompartido
	public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta
	public confVotarEntidad: ConfiguracionVotarEntidad
	public confListaContactoCompartido: ConfiguracionListaContactoCompartido
	public confBuscadorProyectos: ConfiguracionBuscador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		public proyectoService: ProyectoService,
		private _location: Location,
		private proyectoNegocio: ProyectoNegocio,
		private tipoMonedaNegocio: TipoMonedaNegocio,
		private mediaNegocio: MediaNegocio,
		private translateService: TranslateService,
		private generadorId: GeneradorId,
	) {
		this.listaContactosSeleccionados = []
		this.listaContactosBuscador = []
		this.listaContactosOriginal = []
		this.idCapaFormulario = 'capa_formulario_' + this.generadorId.generarIdConSemilla()
		this.puedeCargarMas = true
		this.puedeHacerScroolAlFinal = false
		this.imagenesDefecto = []
		this.listaComentariosModel = []
		this.portadaUrl = ''
		this.catalogoTipoMoneda = []
		this.mostrarCapaLoader = false
		this.mostrarCapaError = false
		this.mensajeCapaError = ''
		this.inputsForm = []
		this.maxDescripcion = 1000
		this.inputProyectoCompleto = 'input-file-pdf'
		this.params = { estado: false }
		this.listaDeFechas = []
		this.formatoFecha = 'dd/MM/yyyy'
		this.$estatusMensajeCompartirTransferencia = new Subject()
		this.paraCompartir = true
		this.idPerfilCoautorParaResponder = ''
		this.idInternoParaResponder = ''
	}

	ngOnInit(): void {
		this.inicializarContenidoParaAccionPublicarEnDemo()
		this.inicializarFechaMaximaUpdate()
		this.inicializarDataTipoMoneda()
		this.configurarAppBar()
		this.configurarDialogoEliminarProyecto()
		this.configurarDialogoEliminarComentario()
	}

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.inicializarImagenesPorDefecto()
		})
	}

	accionAtras() {
		this._location.back()
	}

	inicializarDataListaComentarios() {
		this.listaComentarios = {
			anteriorPagina: false,
			paginaActual: 1,
			proximaPagina: true,
			totalDatos: 0,
			totalPaginas: 0,
			lista: [],
		}
	}

	inicializarFechaMaximaUpdate() {
		this.fechaMaximaUpdate = new Date()
	}

	inicializarContenidoParaAccionPublicarEnDemo() {
		this.proyecto = this.proyectoNegocio.obtenerProyectoParaDemo()


		// Utils
		this.inicializarControlesSegunAccion()
		this.inicializarInputs()
		this.inicializarDataTipoMoneda()
		// Componentes hijos
		this.configurarPortadaExpandida()
		this.configurarSelector()
		this.configurarBuscador()
		this.configurarMonedaPicker()
		this.configurarBotones()
	}

	// Obtener la lista de imagenes por defecto
	inicializarImagenesPorDefecto() {
		this.mediaNegocio.obtenerListaArchivosDefault().subscribe(data => {
			data.forEach(item => {
				if (item.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.PROYECTOS) {
					this.imagenesDefecto.push(item)
				}
			})
			this.definirImagenDeLaPortadaExpandida()
		}, error => {
			this.imagenesDefecto = []
			this.definirImagenDeLaPortadaExpandida()
		})
	}

	// Setear imagen por defecto
	definirImagenDeLaPortadaExpandida() {
		const data = this.proyectoService.determinarUrlImagenPortada(this.proyecto, this.imagenesDefecto)
		this.confPortadaExpandida.urlMedia = data.url
		this.confPortadaExpandida.mostrarLoader = (this.confPortadaExpandida.urlMedia.length > 0)
	}

	// Inicializar controles del formulario
	inicializarControlesSegunAccion() {
		this.proyectoForm = this.proyectoService.inicializarControlesFormularioAccionCrear(this.proyecto)
	}

	// Inicializar los inputs
	inicializarInputs() {
		this.inputsForm = this.proyectoService.configurarInputsDelFormulario(this.proyectoForm)
	}

	inicializarDataTipoMoneda() {
		this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(elegibles => {
			this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles)
		})
	}

	configurarAppBar() {
		this.confAppbar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => this.accionAtras(),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v12texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				mostrarCasaHome: true
			},

		}
	}

	// Configuracion de la portada expandida
	configurarPortadaExpandida() {
		this.confPortadaExpandida = {
			urlMedia: '',
			mostrarLoader: false,
			bloques: [
				{
					tipo: TipoBloqueBortada.BOTON_PROYECTO,
					colorFondo: ColoresBloquePortada.AZUL_FUERTE,
					conSombra: SombraBloque.SOMBRA_NEGRA,
					llaveTexto: 'm4v5texto2',
					eventoTap: () => { }
				},
				{
					tipo: TipoBloqueBortada.INFO_PROYECTO,
					colorFondo: ColoresBloquePortada.AZUL_DEBIL,
					llaveTexto: 'm4v5texto3',
				}
			]
		}
	}

	configurarSelector() {
		const item: ItemSelector = this.proyectoService.obtenerPaisSeleccionadoEnElProyecto(this.proyecto)


		this.confSelector = {
			tituloSelector: 'text61',
			mostrarModal: false,
			inputPreview: {
				mostrar: true,
				input: {
					valor: '',
					placeholder: 'm4v5texto10',
					textoBold: true,
					textoEnMayusculas: true
				}
			},
			seleccionado: item,
			elegibles: [],
			cargando: {
				mostrar: false
			},
			error: {
				mostrarError: false,
				contenido: '',
				tamanoCompleto: false
			},
			quitarMarginAbajo: true,
			evento: (data: InfoAccionSelector) => this.eventoEnSelector(data)
		}
	}

	configurarBuscador() {
		const item: ItemSelector = this.proyectoService.obtenerLocalidadSeleccionadaEnElProyecto(this.proyecto)
		const pais: ItemSelector = this.proyectoService.obtenerPaisSeleccionadoEnElProyecto(this.proyecto)

		this.confBuscador = {
			seleccionado: item,
			inputPreview: {
				mostrar: true,
				input: {
					placeholder: 'm4v5texto11',
					valor: '',
					auxiliar: item.auxiliar,
					textoBold: true,
					textoEnMayusculas: true
				},
				quitarMarginAbajo: true
			},
			mostrarModal: false,
			inputBuscador: {
				valor: '',
				placeholder: 'Busca tu localidad',
			},
			resultado: {
				mostrarElegibles: false,
				mostrarCargando: false,
				error: {
					mostrarError: false,
					contenido: '',
					tamanoCompleto: false
				},
				items: []
			},
			pais: pais,
			evento: (data: InfoAccionBuscadorLocalidades) => this.eventoEnBuscador(data)
		}
	}

	configurarMonedaPicker(
		soloLectura: boolean = false
	) {
		const dataMoneda: ResumenDataMonedaPicker = this.proyectoService.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(this.proyecto)

		this.confMonedaPicker = {
			inputCantidadMoneda: {
				valor: dataMoneda.valorEstimado,
				colorFondo: ColorDeFondo.FONDO_AZUL_CLARO,
				colorTexto: ColorDelTexto.TEXTOAZULBASE,
				tamanoDelTexto: TamanoDeTextoConInterlineado.L2_IGUAL,
				estiloDelTexto: EstilosDelTexto.BOLD,
				soloLectura: soloLectura
			},
			selectorTipoMoneda: {
				titulo: {
					mostrar: this.params.accionEntidad !== AccionEntidad.VISITAR,
					llaveTexto: 'm4v5texto14'
				},
				inputTipoMoneda: {
					valor: dataMoneda.tipoMoneda,
					colorFondo: ColorDeFondo.FONDO_BLANCO,
					colorTexto: ColorDelTexto.TEXTOAZULBASE,
					tamanoDelTexto: TamanoDeTextoConInterlineado.L2_IGUAL,
					estiloDelTexto: EstilosDelTexto.BOLD,
					estiloBorde: {
						espesor: EspesorDelBorde.ESPESOR_018,
						color: ColorDeBorde.BORDER_NEGRO
					}
				},
				elegibles: [],
				seleccionado: dataMoneda.tipoMoneda.seleccionado,
				mostrarSelector: false,
				evento: (accion: InfoAccionSelector) => this.eventoEnSelectorDeTipoMoneda(accion),
				mostrarLoader: false,
				error: {
					mostrar: false,
					llaveTexto: ''
				},
				soloLectura: soloLectura
			}
		}
	}

	async configurarBotones() {
		// Boton historico
		this.confBotonHistorico = {
			text: 'm4v5texto18',
			colorTexto: ColorTextoBoton.CELESTE,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton compartir
		this.confBotonCompartir = {
			text: 'm4v5texto19',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton articulo o proyecto completo
		this.confBotonFullProject = {
			text: 'm4v5texto25',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton link
		this.confBotonLinks = {
			text: 'm4v5texto28',
			colorTexto: ColorTextoBoton.VERDE,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton submit
		this.confBotonPublish = {
			text: 'm4v5texto29',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton Transferir
		this.confBotonTransferir = {
			text: 'm4v6texto27',
			colorTexto: ColorTextoBoton.AZUL,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton Eliminar
		this.confBotonEliminar = {
			text: 'm4v6texto28',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}

		// Obtener y setear textos
		this.confBotonHistorico.text = await this.translateService.get('m4v5texto18').toPromise()
		this.confBotonCompartir.text = await this.translateService.get('m4v5texto19').toPromise()
		this.confBotonFullProject.text = await this.translateService.get('m4v5texto25').toPromise()
		this.confBotonLinks.text = await this.translateService.get('m4v5texto28').toPromise()
		this.confBotonPublish.text = await this.translateService.get('m4v5texto29').toPromise()
		this.confBotonTransferir.text = await this.translateService.get('m4v6texto27').toPromise()
		this.confBotonEliminar.text = await this.translateService.get('m4v6texto28').toPromise()
	}

	configurarDialogoEliminarProyecto() {

	}

	configurarDialogoEliminarComentario() {

	}



	configurarVotarEntidad() {

	}

	// Click en input pais
	abrirSelectorPaises() {

	}

	// Click en input localidades
	abrirBuscadorLocalidades() {

	}

	// Buscador localidades
	buscarLocalidades(pais: string, query: string) {

	}

	eventoEnSelector(data: InfoAccionSelector) {

	}

	eventoEnBuscador(data: InfoAccionBuscadorLocalidades) {

	}

	// Metodo para reintentar, en caso de error al obtener la informacion
	reintentar() {

	}

	// Navegar al album acorde a la accion del proyecto
	irAlAlbumGeneral() {

	}

	irAlAlbumAudios() {

	}

	irAlAlbumDeLinks() {

	}

	// Metodos del selector de moneda
	eventoEnSelectorDeTipoMoneda(accion: InfoAccionSelector) {

	}

	eventoEnBotonPublish() {

	}

	eventoEnBotonTransferir() {

	}

	eventoEnBotonEliminar() {

	}

	publicarProyecto() {

	}

	actualizarProyecto() {

	}

	scroolEnCapaFormulario() {

	}

	validarAccionDobleTapEnPortadaExpandida() {

	}

	eventoBotonFullProject() {

	}

	determinarSiHayAlbumSegunTipo(codigo: CodigosCatalogoTipoAlbum) {
		return true
	}

	cerrarBuscador() {
		this.listaContactosBuscador = []
	}

}
