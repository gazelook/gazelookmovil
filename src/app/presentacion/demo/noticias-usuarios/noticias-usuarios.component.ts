import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {
  ColorDeBorde, ColorDeFondo, ColorFondoLinea, EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { JsonDemoNegocio } from 'dominio/logica-negocio/json-demo.negocio';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { NoticiaService } from '@core/servicios/generales/noticia.service';


@Component({
  selector: 'app-noticias-usuarios',
  templateUrl: './noticias-usuarios.component.html',
  styleUrls: ['./noticias-usuarios.component.scss']
})
export class NoticiasUsuariosDemoComponent implements OnInit {
  // Configuracion de capas
  public mostrarCapaLoader: boolean
  public mostrarCapaError: boolean
  public mensajeCapaError: string
  public mostrarCapaNormal: boolean
  public puedeCargarMas: boolean

  // Parametros internos
  public fechaInicial: Date
  public fechaFinal: Date
  public filtroActivo: string
  public filtroPorTitulo: boolean
  public metodoBusqueda: number
  public idCapaCuerpo: string

  public listaNoticiaModel: Array<NoticiaModel>
  public listaNoticiasModelFiltroOtro: Array<NoticiaModel>

  // Configuracion hijos
  public confAppbar: ConfiguracionAppbarCompartida
  public confBotonRecientes: BotonCompartido
  public confBotonMasVotados: BotonCompartido
  public confLineaNoticias: LineaCompartida

  public listaNoticias: PaginacionModel<CongifuracionItemProyectosNoticias>
  public confResultadoBusqueda: Array<CongifuracionItemProyectosNoticias>
	public archivosLocalStorage: ArchivoModel[]
	public archivosContactos: ArchivoModel[]
  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public noticiaService: NoticiaService,
    private noticiaNegocio: NoticiaNegocio,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private translateService: TranslateService,
    private perfilNegocio: PerfilNegocio,
    private albumNegocio: AlbumNegocio,
    public jsonNegocio: JsonDemoNegocio,
    public variablesGlobales: VariablesGlobales,
    public mediaNegocio: MediaNegocio
  ) {
    this.mostrarCapaLoader = false
    this.mostrarCapaError = false
    this.puedeCargarMas = true
    this.mensajeCapaError = ''
    this.fechaInicial = new Date()
    this.fechaFinal = new Date()
    this.metodoBusqueda = 0
    this.filtroActivo = 'fecha'
    this.filtroPorTitulo = false
    this.idCapaCuerpo = 'capa-cuerpo-noticias'
    this.confResultadoBusqueda = []

    this.listaNoticiaModel = []
    this.listaNoticiasModelFiltroOtro = []
    this.archivosLocalStorage = []
		this.archivosContactos = []
  }

  ngOnInit(): void {
    this.configurarAppBar()
    this.configurarBotones()
    this.configurarLineas()
    this.inicializarListaNoticias()
    this.inicializarContenidoListaNoticias()
    this.variablesGlobales.mostrarMundo = false
  }

  async listarProyectoDemo() {
    this.jsonNegocio.obtenerNoticiasDemo().subscribe(data => {
      for (const notiDemo of data) {

        this.listaNoticias.lista.push(this.configurarItemListaNoticias(notiDemo))
      }
      this.archivosLocalStorage = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal()        
      this.archivosContactos =  this.archivosLocalStorage.filter(element => element.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.DEMO_LISTA_CONTACTOS) 
     


      let defauultNoti =  this.archivosLocalStorage.filter(element => element.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.PROYECTOS) 

      let noticias1 = this.archivosContactos.find(e => e.url.includes('3b10f5b7-1545-454e-84b8-99c41cdae115.jpg'))
      this.listaNoticias.lista[4].urlMedia = defauultNoti[5].url
      this.listaNoticias.lista[5].urlMedia = defauultNoti[7].url
      let noticia0 = this.archivosContactos.find(e => e.url.includes('0fab40da-0d18-4034-bf54-d7c081cbfc3d.jpg'))            		
		  this.listaNoticias.lista[0].urlMedia = noticia0.url

      let noticia1 = this.archivosContactos.find(e => e.url.includes('9b4f26ae-8ce9-46ff-a149-73378c1dd04a.jpg'))            		
		  this.listaNoticias.lista[1].urlMedia = noticia1.url

      let noticia2 = this.archivosContactos.find(e => e.url.includes('8270ee5e-d26a-4e46-ab17-79c6075c55dd.jpg'))            		
		  this.listaNoticias.lista[2].urlMedia = noticia2.url

      let noticia3 = this.archivosContactos.find(e => e.url.includes('61dadc45-c265-4dd2-8b73-c7c4a93912f2.jpg'))            		
		  this.listaNoticias.lista[6].urlMedia = noticia3.url

      let noticia4 = this.archivosContactos.find(e => e.url.includes('93f07d57-dc21-4518-be70-3d48d2b9631a.jpg'))            		
		  this.listaNoticias.lista[7].urlMedia = noticia4.url

      let noticia5 = this.archivosContactos.find(e => e.url.includes('e3d0b989-fb23-4ef5-9c49-1b628b0a8dce.jpg'))            		
		  this.listaNoticias.lista[8].urlMedia = noticia5.url

      let noticia6 = this.archivosContactos.find(e => e.url.includes('8f1ad699-03e9-4046-94e0-344b16876169.jpg'))            		
		  this.listaNoticias.lista[9].urlMedia = noticia6.url

      this.listaNoticias.lista[3].noDisponible = true;
      this.listaNoticias.lista[3].titulo.mostrar = false;
      this.listaNoticias.lista[3].fecha.mostrar = false;
      

     

    })
    // this.configNoDisponible();
  }

  async configNoDisponible() {
    this.listaNoticias.lista[3].noDisponible = true;
    this.listaNoticias.lista[3].titulo.mostrar = false;
    this.listaNoticias.lista[3].fecha.mostrar = false;
    this.listaNoticias.lista[3].titulo.configuracion.textoBoton1 = await this.translateService.get('m1v18texto9').toPromise();
    this.listaNoticias.lista[3].titulo.configuracion.textoBoton2 = await this.translateService.get('m1v18texto10').toPromise();;
    this.listaNoticias.lista[3].titulo.configuracion.textoBoton3 = await this.translateService.get('m1v18texto11').toPromise();;

    this.listaNoticias.lista[1].tipo =
      this.listaNoticias.lista[4].tipo =
      this.listaNoticias.lista[5].tipo =
      this.listaNoticias.lista[6].tipo =
      this.listaNoticias.lista[7].tipo =
      this.listaNoticias.lista[8].tipo = 'CATALB_3';
  }

  obtenerFechaParaParametro(fecha: Date) {
    return fecha.getFullYear() + '-' + (fecha.getMonth() + 1) + '-' + fecha.getDate()
  }

  configurarAppBar() {
    this.confAppbar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      accionAtras: () => this.accionAtras(),
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v18texto2'
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      }
    }
  }

  configurarBotones() {
    this.confBotonRecientes = {
      text: 'm4v12texto4',
      colorTexto: ColorTextoBoton.AMARRILLO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
      }
    }

    this.confBotonMasVotados = {
      text: 'm4v12texto5',
      colorTexto: ColorTextoBoton.ROJO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
      }
    }
  }

  configurarLineas() {
    this.confLineaNoticias = {
      ancho: AnchoLineaItem.ANCHO6382,
      espesor: EspesorLineaItem.ESPESOR071,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
    }
  }

  inicializarListaNoticias() {
    this.listaNoticias = {
      lista: [],
      proximaPagina: true,
      totalDatos: 0,
      paginaActual: 1,
    }
  }

  inicializarContenidoListaNoticias() {
    const fechaA: Date = new Date()
    const fechaB: Date = new Date()
    fechaA.setTime(fechaA.getTime() - ((24 * 60 * 60 * 1000) * 30))

    this.fechaInicial = fechaA
    this.fechaFinal = fechaB

    this.obtenerNoticiasPorFechaFiltro(
      this.obtenerFechaParaParametro(fechaA),
      this.obtenerFechaParaParametro(fechaB),
      '090909900909',
      this.filtroActivo
    )
  }

  cerrarBuscador() {
    if (this.filtroPorTitulo) {
      this.filtroPorTitulo = false
      this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda = ''
      this.inicializarListaNoticias()
      this.inicializarContenidoListaNoticias()
    }
  }

  accionAtras() {
    this.noticiaNegocio.removerNoticiaActivaDelSessionStorage()
    this._location.back()
  }

  tapEnInputFecha(id: string) {
    const elemento: HTMLElement = document.getElementById(id) as HTMLElement
    if (elemento) {
      elemento.click()
    }
  }

  async obtenerNoticiasPorFechaFiltro(
    fechaInicial: string,
    fechaFinal: string,
    perfil: string,
    filtro: string
  ) {
    this.listarProyectoDemo()
  }

  configurarItemListaNoticias(noticia: NoticiaModel): CongifuracionItemProyectosNoticias {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(noticia.adjuntos)
    return {
      id: noticia.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(noticia.fechaActualizacion),
          formato: 'dd/MM/yyyy'
        }
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: noticia.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: true,
        evento: (data: CongifuracionItemProyectosNoticias) => {
        }
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      },
    }
  }

  cambioDeFecha(orden: number) {
    if (orden === 0) {
      this.fechaInicial = new Date(this.fechaInicial)
    }

    if (orden === 1) {
      this.fechaFinal = new Date(this.fechaFinal)
    }
  }

  async buscarNoticiasPorTitulo(titulo: string) {
    if (!this.listaNoticias.proximaPagina) {
      this.puedeCargarMas = true
      return
    }

    try {
      this.mostrarCapaLoader = (this.listaNoticias.lista.length === 0)
      const dataPaginacion = await this.noticiaNegocio.buscarNoticiasPorTitulo(
        titulo,
        10,
        this.listaNoticias.paginaActual
      ).toPromise()

      this.listaNoticias.totalDatos = dataPaginacion.totalDatos
      this.listaNoticias.proximaPagina = dataPaginacion.proximaPagina

      dataPaginacion.lista.forEach(noticia => {
        this.listaNoticias.lista.push(this.configurarItemListaNoticias(noticia))
      })
      this.mostrarCapaLoader = false
      this.puedeCargarMas = true

      if (this.listaNoticias.proximaPagina) {
        this.listaNoticias.paginaActual += 1
      }
    } catch (error) {
      this.filtroPorTitulo = false
      this.mostrarCapaLoader = false
      this.mostrarCapaError = true
    }
  }
}
