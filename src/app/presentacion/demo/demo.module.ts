import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxCurrencyModule } from 'ngx-currency';
import { CompartidoModule } from '@shared/compartido.module';
import { ActualizarProyectoComponent } from 'src/app/presentacion/demo/actualizar-proyecto/actualizar-proyecto.component';
import { CompraIntermacioInicioDemoComponent } from 'src/app/presentacion/demo/compra-intermacio-inicio/compra-intermacio-inicio.component';
import { DemoRoutingModule } from 'src/app/presentacion/demo/demo-routing.module';
import { DemoComponent } from 'src/app/presentacion/demo/demo.component';
import { DetalleDemoPerfilComponent } from 'src/app/presentacion/demo/detalle-perfil/detalle-perfil.component';
import { FinanzasDemoComponent } from 'src/app/presentacion/demo/finanzas-demo/finanzas-demo.component';
import { InformacionUtilDemoComponent } from 'src/app/presentacion/demo/informacion-util/informacion-util.component';
import { ListaContactosDemoComponent } from 'src/app/presentacion/demo/lista-contactos/lista-contactos.component';
import { ListaProyectosMundialesDemoComponent } from 'src/app/presentacion/demo/lista-proyectos/lista-proyectos.component';
import { MenuListaProyectosDemoComponent } from 'src/app/presentacion/demo/menu-lista-proyectos/menu-lista-proyectos.component';
import { MenuListaTipoProyectoDemoComponent } from 'src/app/presentacion/demo/menu-lista-tipo-proyecto/menu-lista-tipo-proyecto.component';
import { MenuPerfilesDemoComponent } from 'src/app/presentacion/demo/menu-perfiles/menu-perfiles.component';
import { MenuprincipalComponent } from 'src/app/presentacion/demo/menuprincipal/menuprincipal.component';
import { NoticiasUsuariosDemoComponent } from 'src/app/presentacion/demo/noticias-usuarios/noticias-usuarios.component';
import { PensamientoDemoComponent } from 'src/app/presentacion/demo/pensamiento/pensamiento.component';
import { NoticiaPublicarComponent } from 'src/app/presentacion/demo/publicar-noticia/publicar-noticia.component';
import { PublicarNoticiasProyectosDemoComponent } from 'src/app/presentacion/demo/publicar-noticias-proyectos-demo/publicar-noticias-proyectos-demo.component';
import { PublicarProyectoComponent } from 'src/app/presentacion/demo/publicar-proyecto/publicar-proyecto.component';

@NgModule({
  declarations: [
    DemoComponent,
    MenuprincipalComponent,
    MenuPerfilesDemoComponent,
    ListaContactosDemoComponent,
    PensamientoDemoComponent,
    MenuListaTipoProyectoDemoComponent,
    InformacionUtilDemoComponent,
    MenuListaProyectosDemoComponent,
    ListaProyectosMundialesDemoComponent,
    NoticiasUsuariosDemoComponent,
    CompraIntermacioInicioDemoComponent,
    PublicarProyectoComponent,
    NoticiaPublicarComponent,
    PublicarNoticiasProyectosDemoComponent,
    ActualizarProyectoComponent,
    FinanzasDemoComponent,
    DetalleDemoPerfilComponent,
  ],
  imports: [
    NgxCurrencyModule,
    CommonModule,
    DemoRoutingModule,
    FormsModule,
    TranslateModule,
    CommonModule,
    CompartidoModule,
    DemoRoutingModule,
    FormsModule,
  ],
  exports: [
    NgxCurrencyModule,
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class DemoModule { }
