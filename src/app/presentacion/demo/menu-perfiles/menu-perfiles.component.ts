import { Location } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { ModalContenido } from '@shared/componentes/dialogo-contenido/dialogo-contenido.component';
import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';
import { ModalInferior } from '@shared/componentes/modal-inferior/modal-inferior.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { ColorFondoItemMenu } from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoItemMenu, TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ItemMenuCompartido } from '@shared/diseno/modelos/item-menu.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { InformacionModel } from 'dominio/modelo/informacion.model';
import { DialogoServicie } from '@core/servicios/diseno/dialogo.service';
import { CodigosCatalogosEstadoPerfiles } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-perfiles.enun';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
	ColorDelTexto, ColorFondoLinea,
	EspesorLineaItem,
	EstiloErrorInput, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';

@Component({
	selector: 'app-menu-perfiles',
	templateUrl: './menu-perfiles.component.html',
	styleUrls: ['./menu-perfiles.component.scss']
})
export class MenuPerfilesDemoComponent implements OnInit {
	configuracionAppBar: ConfiguracionAppbarCompartida;
	tipoPerfilSeleccionado: CatalogoTipoPerfilModel;
	listaTipoPerfil: CatalogoTipoPerfilModel[];
	idPerfilIncompatibleDialogo = "aviso-tipo-perfil";
	itemInformacion: InformacionModel;
	dataBoton: BotonCompartido;
	dataPerfilIncompatibleDialogo: ModalContenido;
	menorEdadForm: FormGroup;
	inputNombresResponsable: InputCompartido
	inputCorreoResponsable: InputCompartido
	dataModalTerminosCondiciones: ModalInferior
	dataLista: DatosLista
	itemSeleccionado: any


	constructor(
		private perfilNegocio: PerfilNegocio,
		private dialogoServicie: DialogoServicie,
		private router: Router,
		private internacionalizacionNegocio: InternacionalizacionNegocio,
		private formBuilder: FormBuilder,
		private cuentaNegocio: CuentaNegocio,
		private _location: Location,
	) {
		this.configurarBotonAceptar();
		this.prepararAppBar()
		this.prepararInfoTipoPerfiles();
		this.configurarDialogoContenido();
		this.iniciarFormMenorEdad();
		this.prepararModalTerminosCondiciones();
		this.preperarListaMenuTipoPerfil();
	}


	ngOnInit(): void {

		this.obtenerCatalogoTipoPerfil()
		this.verificarAceptacionTerminosCondiciones()
	}



	verificarAceptacionTerminosCondiciones() {
		this.dataModalTerminosCondiciones.abierto = this.cuentaNegocio.verificarAceptacionTerminosCondiciones();

	}

	obtenerCatalogoTipoPerfil() {
		this.perfilNegocio.obtenerCatalogoTipoPerfilConPerfil().subscribe((res: CatalogoTipoPerfilModel[]) => {


			this.listaTipoPerfil = res
			this.dataLista.cargando = false;
		}, error => {
			this.dataLista.error = error;
			this.dataLista.cargando = false;
		})
	}
	obtenerLlaveDescripcionSegunTipoPerfil(
        codigoPerfil: CodigosCatalogoTipoPerfil
    ) {
		switch (codigoPerfil) {
            case CodigosCatalogoTipoPerfil.CLASSIC:
                return ['m2v1texto8']
            case CodigosCatalogoTipoPerfil.PLAYFUL:
                return ['m2v1texto12']
            case CodigosCatalogoTipoPerfil.SUBSTITUTE:
                return [ 'm2v1texto16', 'm2v1texto17' ]
            case CodigosCatalogoTipoPerfil.GROUP:
                return ['m2v1texto21']
            default:
                return ''
        }
	}

	prepararItemTipoPerfil(tipoPerfil: CatalogoTipoPerfilModel): ItemMenuCompartido {

		const descripcion: any = this.obtenerLlaveDescripcionSegunTipoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
		const textos = this.obtenerLlavesTextosSegunCodigoPerfil(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
		const data = {
			id: '',
			tamano: TamanoItemMenu.ITEMMENUCREARPERFIL, // Indica el tamano del item (altura)
			colorFondo: this.obtenerColorPerfil(tipoPerfil.perfil),
			mostrarDescripcion: tipoPerfil.mostrarDescripcion ?? false,
			texto1: this.obtenerEstadoPerfil(tipoPerfil.perfil),
			texto2: textos.texto2,
			texto3: textos.texto3,
			texto4: 'm1v4texto6',

			tipoMenu: TipoMenu.GESTION_PROFILE_DEMO,
			descripcion: [],
			linea: {
				mostrar: true,
				configuracion: {
					ancho: AnchoLineaItem.ANCHO6386,
					espesor: EspesorLineaItem.ESPESOR071,
					colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
					forzarAlFinal: false
				}
			},
			gazeAnuncios: false,
			idInterno: tipoPerfil.codigo,
			onclick: () => this.mostrarDescripcion(tipoPerfil),
			dobleClick: () => this.gestionarPerfil(tipoPerfil)
		}

		descripcion.forEach((d: any) => {
			data.descripcion.push(
				{
					texto: this.internacionalizacionNegocio.obtenerTextoSincrono(d),
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
					color: ColorDelTexto.TEXTOBOTONBLANCO,
					estiloTexto: EstilosDelTexto.REGULAR,
					enMayusculas: true
				}
			)
		})

		

		return data


	}


	obtenerLlavesTextosSegunCodigoPerfil(
		codigoPerfil: CodigosCatalogoTipoPerfil
	) {
		const textos = {
			texto2: '',
			texto3: ''
		}
		switch (codigoPerfil) {
			case CodigosCatalogoTipoPerfil.CLASSIC:
				textos.texto2 = 'm1v5texto4'
				textos.texto3 = 'm1v5texto5'
				break
			case CodigosCatalogoTipoPerfil.PLAYFUL:
				textos.texto2 = 'm1v6texto7'
				textos.texto3 = 'm1v6texto8'
				break
			case CodigosCatalogoTipoPerfil.SUBSTITUTE:
				textos.texto2 = 'm1v5texto11'
				textos.texto3 = 'm1v5texto12'
				break
			case CodigosCatalogoTipoPerfil.GROUP:
				textos.texto2 = 'm1v5texto16'
				textos.texto3 = 'm1v5texto17'
				break
			default: break
		}

		return textos
	}


	prepararItemMiPerfil(): ItemMenuCompartido {
		return {
			id: '',
			tamano: TamanoItemMenu.ITEM_MENU_GENERAL, // Indica el tamano del item (altura)
			colorFondo: ColorFondoItemMenu.PREDETERMINADO,
			colorTexto: ColorDelTexto.TEXTOAMARILLOMEDIO,
			mostrarDescripcion: false,
			texto2: 'm1v4texto3',
			tipoMenu: TipoMenu.PROFILE,

			linea: {
				mostrar: true,
				configuracion: {
					ancho: AnchoLineaItem.ANCHO6382,
					espesor: EspesorLineaItem.ESPESOR071,
					colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
					forzarAlFinal: false
				}
			},
			gazeAnuncios: false,
			idInterno: '00000000',
			onclick: () => {
				this.router.navigate([RutasLocales.MODULO_DEMO + '/' + RutasDemo.MI_PERFIL]);
			},
			// onclick: () => this.mostrarDescripcion(tipoPerfil),
			// dobleClick: () => this.gestionarPerfil(tipoPerfil)
		};


	}


	gestionarPerfil(tipoPerfil: CatalogoTipoPerfilModel) {
		this.tipoPerfilSeleccionado = tipoPerfil;
		if (this.perfilNegocio.conflictoCrearPerfil(tipoPerfil, this.listaTipoPerfil)) {
			this.dataPerfilIncompatibleDialogo.abierto = true
		} else {
			this.navegarCrearPerfil(tipoPerfil);
		}
	}

	limpiarPerfiles(tipoPerfil: CatalogoTipoPerfilModel) {
		this.perfilNegocio.limpiarPerfiles(this.listaTipoPerfil);
		this.dialogoServicie.close(this.idPerfilIncompatibleDialogo)
		this.navegarCrearPerfil(tipoPerfil);
	}

	navegarCrearPerfil(tipoPerfil: CatalogoTipoPerfilModel) {
		// Definir perfil activo
		const perfilActivo: PerfilModel = this.perfilNegocio.obtenerPerfilDelUsuarioSegunTipo(tipoPerfil.codigo as CodigosCatalogoTipoPerfil)

		if (perfilActivo && perfilActivo !== null) {
			this.perfilNegocio.guardarPerfilActivoEnSessionStorage(perfilActivo)
			this.perfilNegocio.guardarTipoPerfilActivo(tipoPerfil)

			const registro = RutasLocales.REGISTRAR_PERFIL.toString()
			this.router.navigateByUrl(registro)
		} else {
			
		}
	}

	mostrarDescripcion(item: any) {
		if (this.itemSeleccionado === undefined) {
			this.itemSeleccionado = item
		}

		if (this.itemSeleccionado.codigo === item.codigo) {
			const elemento: HTMLElement = document.getElementById('flecha' + item.codigo) as HTMLElement;
			if (item.mostrarDescripcion) {
				item.mostrarDescripcion = false;
				this.itemSeleccionado.mostrarDescripcion = false;
				elemento.classList.remove("rotar-flecha")
			} else {
				item.mostrarDescripcion = true;
				this.itemSeleccionado.mostrarDescripcion = true;
				elemento.classList.add("rotar-flecha");
			}

		}

		if (this.itemSeleccionado.codigo !== item.codigo) {
			if (this.itemSeleccionado.mostrarDescripcion) {
				const elemento: HTMLElement = document.getElementById('flecha' + this.itemSeleccionado.codigo) as HTMLElement;
				if (this.itemSeleccionado.mostrarDescripcion) {
					this.itemSeleccionado.mostrarDescripcion = false;
					elemento.classList.remove("rotar-flecha")
				} else {
					this.itemSeleccionado.mostrarDescripcion = true;
					elemento.classList.add("rotar-flecha");
				}
			}

			const elemento2: HTMLElement = document.getElementById('flecha' + item.codigo) as HTMLElement;
			if (item.mostrarDescripcion) {
				item.mostrarDescripcion = false;
				elemento2.classList.remove("rotar-flecha")
			} else {
				item.mostrarDescripcion = true;
				elemento2.classList.add("rotar-flecha");
			}
			this.itemSeleccionado = item
		}





	}

	prepareItemInstrucciones(): ItemMenuCompartido {
		return {
			id: '',
			tamano: TamanoItemMenu.ITEMMENUCREARPERFIL, // Indica el tamano del item (altura)
			colorFondo: ColorFondoItemMenu.PREDETERMINADO, // El color de fondo que tendra el item
			mostrarDescripcion: false,
			tipoMenu: TipoMenu.INSTRUCCIONES,
			texto1: "texto235",
			texto2: "texto236",
			texto3: 'texto237',
			descripcion: null,
			linea: {
				mostrar: true,
				configuracion: {
					ancho: AnchoLineaItem.ANCHO6386,
					espesor: EspesorLineaItem.ESPESOR071,
					colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
					forzarAlFinal: false
				}
			},
			gazeAnuncios: false,
			idInterno: "",
			onclick: () => { },
			dobleClick: () => { }
		};
	}


	prepareItemInformacion(informacion: InformacionModel): ItemMenuCompartido {
		try {
			return {
				id: '',
				tamano: TamanoItemMenu.ITEMMENUCREARPERFIL, // Indica el tamano del item (altura)
				colorFondo: ColorFondoItemMenu.PREDETERMINADO, // El color de fondo que tendra el item
				mostrarDescripcion: informacion.mostrarDescripcion ?? false,
				tipoMenu: TipoMenu.ACCION,
				texto1: 'm1v6texto18',
				descripcion: [
					{
						texto: informacion.descripcion[0],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[1],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOAMARILLOMEDIO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[2],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[3],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[4],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[5],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[6],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},
					{
						texto: informacion.descripcion[7],
						tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I2,
						color: ColorDelTexto.TEXTOBLANCO,
						estiloTexto: EstilosDelTexto.REGULAR,
						enMayusculas: true
					},

				],
				linea: {
					mostrar: true,
					configuracion: {
						ancho: AnchoLineaItem.ANCHO6382,
						espesor: EspesorLineaItem.ESPESOR071,
						colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
						forzarAlFinal: false
					}
				},
				gazeAnuncios: false,
				idInterno: informacion.codigo,
				onclick: () => this.mostrarDescripcion(informacion),
				dobleClick: () => { }

			};
		} catch (error) {

		}

	}


	async prepararAppBar() {
		this.configuracionAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => this.volverAtras(),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v4texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
			}
		}
	}

	volverAtras() {

		this._location.back()
	}

	async prepararInfoTipoPerfiles() {
		this.itemInformacion = {
			codigo: "info",
			nombre: await this.internacionalizacionNegocio.obtenerTextoLlave('texto255'),
			descripcion: [
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto1'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto2'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto3'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto4'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto5'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto6'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto7'),
				await this.internacionalizacionNegocio.obtenerTextoLlave('m2v2texto8'), 
			],
		}
	}


	configurarBotonAceptar() {
		this.dataBoton = {
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			text: "ACEPTAR",
			ejecutar: () => { this.dataPerfilIncompatibleDialogo.abierto = false },
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO
		}
	}

	aceptarTerminosCondicionesMenorEdad() {
		if (this.menorEdadForm.value.nombreResposanble.length >= 1 || this.menorEdadForm.value.nombreResposanble.length >= 1) {
			if (this.menorEdadForm.valid) {
				this.dataModalTerminosCondiciones.abierto = false;
				this.cuentaNegocio.guardarAceptacionMenorEdad
					(
						this.menorEdadForm.value.correoResponsable,
						this.menorEdadForm.value.nombreResposanble,
						new Date()
					);
			}
			this.inputNombresResponsable.error = true;
			this.inputCorreoResponsable.error = true;
		} else {
			this.dataModalTerminosCondiciones.abierto = false;
			this.cuentaNegocio.aceptoTerminosCondiciones();
		}

	}

	configurarDialogoContenido() {
		this.dataPerfilIncompatibleDialogo = {
			titulo: "PERFIL INCOMPATIBLE",
			abierto: false,
			bloqueado: true,
			id: "perfil-incompatible"
		}
	}

	async iniciarFormMenorEdad() {
		this.menorEdadForm = this.formBuilder.group({
			//fechaNacimiento: ['', [Validators.required]],
			nombreResposanble: ['', [Validators.minLength(5)]],
			correoResponsable: ['', [Validators.email, Validators.minLength(3)]],
		});
		// this.inputFechaNacimiento = { tipo: 'date', error: false, estilo: { estiloError: EstiloErrorInput.ROJO, estiloInput: EstiloInput.DEFECTO }, placeholder: 'Tu fecha de nacimiento', data: this.menorEdadForm.controls.fechaNacimiento }
		this.inputNombresResponsable = {
			tipo: 'text',
			error: false,
			estilo: {
				estiloError: EstiloErrorInput.ROJO,
				estiloInput: EstiloInput.DEFECTO
			},
			placeholder: 'Nombres Responsable',
			data: this.menorEdadForm.controls.nombreResposanble,
			bloquearCopy: false
		}
		this.inputCorreoResponsable = {
			tipo: 'text',
			error: false,
			estilo: {
				estiloError: EstiloErrorInput.ROJO,
				estiloInput: EstiloInput.DEFECTO
			},
			placeholder: 'Correo Responsable',
			data: this.menorEdadForm.controls.correoResponsable,
			bloquearCopy: false
		}
	}

	prepararModalTerminosCondiciones() {
		this.dataModalTerminosCondiciones = {
			abierto: true,
			bloqueado: true,
			id: "modal-terms"
		}
	}

	obtenerEstadoPerfil(perfil: PerfilModel) {
		if (perfil) {
			// return "crear";//codigo temporal
			switch (perfil.estado.codigo) {
				case CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO || CodigosCatalogosEstadoPerfiles.PERFIL_CREADO:
					return "creado"
				case CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO:
					return "hibernar"
			}
		}
		return "crear";
	}

	obtenerColorPerfil(perfil: PerfilModel) {
		if (perfil) {
			// return ColorFondoItemMenu.PERFILHIBERNADO; //codigo temporal
			switch (perfil.estado.codigo) {
				case CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO || CodigosCatalogosEstadoPerfiles.PERFIL_CREADO:
					return ColorFondoItemMenu.PERFILCREADO
				case CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO:
					return ColorFondoItemMenu.PERFILHIBERNADO

			}
		}
		return ColorFondoItemMenu.PREDETERMINADO
	}

	preperarListaMenuTipoPerfil() {
		this.dataLista = {
			cargando: true,
			reintentar: this.obtenerCatalogoTipoPerfil,
			lista: this.listaTipoPerfil,
			tamanoLista: TamanoLista.TIPO_PERFILES
		}
	}

	// Escucha para el boton de back del navegador
	@HostListener('window:popstate', ['$event'])
	onPopState(event: any) {
		this.cuentaNegocio.limpiarTerminosCondiciones()
	}


}
