import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
	ColorFondoLinea,
	EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';

@Component({
	selector: 'app-menu-lista-tipo-proyecto',
	templateUrl: './menu-lista-tipo-proyecto.component.html',
	styleUrls: ['./menu-lista-tipo-proyecto.component.scss']
})
export class MenuListaTipoProyectoDemoComponent implements OnInit {

	public confAppBar: ConfiguracionAppbarCompartida
	public confLineaVerde: LineaCompartida
	public confLineaVerdeAll: LineaCompartida

	public configDialogoInicial: DialogoCompartido

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private cuentaNegocio: CuentaNegocio,
		private perfilNegocio: PerfilNegocio,
		private router: Router,
		private _location: Location,
	) {

	}

	ngOnInit(): void {
		this.configurarAppBar()
		this.configurarLinea()
		this.configurarDialogoDemo()
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => this.volverAtras(),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v14texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
			}
		}
	}

	volverAtras() {
		this._location.back()
	}

	configurarDialogoDemo() {
		this.configDialogoInicial = {
			completo: true,
			mostrarDialogo: false,
			tipo: TipoDialogo.INFO_VERTICAL,
			descripcion: 'm1v14texto3',
		}
	}

	configurarLinea() {
		this.confLineaVerde = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}

		this.confLineaVerdeAll = {
			ancho: AnchoLineaItem.ANCHO6920,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
	}

	irAListaProyectosSegunTipo(codigo: CodigosCatalogoTipoProyecto) {
		let ruta = RutasLocales.MODULO_DEMO.toString()
		let publicar = RutasDemo.MENU_LISTA_PROYECTOS.toString()
		this.router.navigateByUrl(ruta + '/' + publicar)
	}

	navegarSegunMenu(menu: number) {
		
		switch (menu) {
			case 0:
				this.irAListaProyectosSegunTipo(CodigosCatalogoTipoProyecto.PROYECTO_MUNDIAL)
				break
			case 1:
				this.configDialogoInicial.mostrarDialogo = true
				break
			case 2:
				this.configDialogoInicial.mostrarDialogo = true
				break
			case 3:
				this.configDialogoInicial.mostrarDialogo = true
				break
			case 4:
				// this.configDialogoInicial.mostrarDialogo = true
				let ruta = RutasLocales.MODULO_DEMO.toString()
				let publicar = RutasDemo.INFORMACION_UTIL.toString()
				this.router.navigateByUrl(ruta + '/' + publicar)
				break
		}
	}


}
