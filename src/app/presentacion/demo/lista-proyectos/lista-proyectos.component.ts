import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
  ColorDeBorde,
  ColorDeFondo
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ProyectoEntity } from 'dominio/entidades/proyecto.entity';
import { JsonDemoNegocio } from 'dominio/logica-negocio/json-demo.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';


@Component({
  selector: 'lista-proyectos',
  templateUrl: './lista-proyectos.component.html',
  styleUrls: ['./lista-proyectos.component.scss']
})
export class ListaProyectosMundialesDemoComponent implements OnInit {
  configuracionAppBar: ConfiguracionAppbarCompartida;
  configuracionItemProyecto: CongifuracionItemProyectosNoticias;
  configuracionItemProyectoTexto: CongifuracionItemProyectosNoticias;
  public archivosLocalStorage: ArchivoModel[];
  public archivosContactos: ArchivoModel[];

  public dataListaProyectos: Array<any>;
  public contador: number;
  private contar3: number;
  public lista: Array<{
    configuracion: CongifuracionItemProyectosNoticias,
    clases: any
  }>;
  constructor(
    private router: Router,
    private _location: Location,
    public jsonNegocio: JsonDemoNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public mediaNegocio: MediaNegocio,
  ) {
    this.dataListaProyectos = [];
    this.contador = 0;
    this.contar3 = 0;
    this.lista = [];
    this.archivosLocalStorage = [];
    this.archivosContactos = [];
  }



  ngOnInit(): void {
    this.obtenerArchivos();
    this.prepararAppBar();
    this.listarProyectosDemo();
    this.configurarItemProyectoTexto();
  }
  obtenerArchivos() {
    this.archivosLocalStorage = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal();
    this.archivosContactos = this.archivosLocalStorage.filter(element => element.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.DEMO_LISTA_CONTACTOS);
  }

  configurarItemProyectoTexto() {
    this.configuracionItemProyectoTexto = {
      textoGeneral: 'm1v16texto5',
      titulo: {
        mostrar: false,
        configuracion: {
          textoBoton1: '',
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: false,
        configuracion: {
          fecha: new Date(),
          formato: 'dd/MM/yyyy',
        },
      },
      loader: false,
      urlMedia: '',
      // urlMedia: 'https://images.ctfassets.net/hrltx12pl8hq/2StXTIF5oeiGpSJj8GMRc0/aa3192be223019fff3541692a99b920b/music.jpg?fit=fill&w=480&h=270',
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (proyect: CongifuracionItemProyectosNoticias) => {
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '00000000',
      usoItem: UsoItemProyectoNoticia.SOLO_TEXTO,
      actualizado: false,

    };
  }


  async prepararAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
      accionAtras: () => this.volverAtras(),
      demoAppbar: {
        mostrarLineaVerde: true,
        nombrePerfil: {
          mostrar: false,
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm1v16texto2'
        },
        tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
        mostrarCasaHome: true
      }
    };
  }

  volverAtras() {
    this._location.back();
  }

  configurarItemProyecto(infoProyecto: ProyectoEntity): CongifuracionItemProyectosNoticias {

    let capa: boolean = true;
    if (this.contar3 == 2) {
      capa = false;
    }
    return {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: infoProyecto.traducciones[0].tituloCorto,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: infoProyecto.fechaActualizacion,
          formato: 'dd/MM/yyyy',
        },
      },
      loader: false,
      urlMedia: infoProyecto.adjuntos[0].portada.principal.url,
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (proyect: CongifuracionItemProyectosNoticias) => {

          if (infoProyecto._id === '000000000000000000000000') {
            let ruta = RutasLocales.MODULO_DEMO.toString();
            let componente = RutasDemo.VISITAR_PROYECTO.toString();
            this.router.navigateByUrl(ruta + '/' + componente);
          }

        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: infoProyecto._id,
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      actualizado: infoProyecto.actualizado,
      capaDemo: capa
    };
  }
  configurarItemProyectoNoProyecto(infoItem: any): CongifuracionItemProyectosNoticias {
    return {
      textoGeneral: infoItem.textoInformativo,
      titulo: {
        mostrar: false,
        configuracion: {
          textoBoton1: '',
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: false,
        configuracion: {
          fecha: new Date(),
          formato: 'dd/MM/yyyy',
        },
      },
      loader: false,
      urlMedia: '',
      // urlMedia: 'https://images.ctfassets.net/hrltx12pl8hq/2StXTIF5oeiGpSJj8GMRc0/aa3192be223019fff3541692a99b920b/music.jpg?fit=fill&w=480&h=270',
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (proyect: CongifuracionItemProyectosNoticias) => {
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '00000000',
      usoItem: UsoItemProyectoNoticia.SOLO_TEXTO,
      actualizado: false,

    };
  }

  async listarProyectosDemo() {
    let proyectos = await this.jsonNegocio.obtenerProyectosDemo().toPromise();

    for (const proyec of proyectos) {
      this.contar3++;
      this.dataListaProyectos.push(this.configurarItemProyecto(proyec));
    }

    let proyecto0 = this.archivosContactos.find(e => e.url.includes('8b4dee88-dbdd-4965-9546-95deef9fa8bd.jpg'));
    this.dataListaProyectos[0].urlMedia = proyecto0.url;

    let proyecto1 = this.archivosContactos.find(e => e.url.includes('ff51bfce-8ac3-4d11-ae53-07e9cc63d01b.jpg'));
    this.dataListaProyectos[1].urlMedia = proyecto1.url;

    let proyecto2 = this.archivosContactos.find(e => e.url.includes('afb99de0-7e2e-473a-8447-428d52dbbddd.jpg'));
    this.dataListaProyectos[2].urlMedia = proyecto2.url;

    let proyecto3 = this.archivosContactos.find(e => e.url.includes('a4ab6c63-fbc1-4ee8-8819-919e7026eac0.jpg'));
    this.dataListaProyectos[3].urlMedia = proyecto3.url;

    let proyecto4 = this.archivosContactos.find(e => e.url.includes('9008c12d-3dab-4733-a018-3ab0886236b6.jpg'));
    this.dataListaProyectos[4].urlMedia = proyecto4.url;

    let proyecto5 = this.archivosContactos.find(e => e.url.includes('42814b05-3b88-460c-8e7d-2042e2f5bfac.jpg'));
    this.dataListaProyectos[5].urlMedia = proyecto5.url;

    let proyecto6 = this.archivosContactos.find(e => e.url.includes('d74e96bd-63d4-4763-8cf2-d375dff3939e.jpg'));
    this.dataListaProyectos[6].urlMedia = proyecto6.url;


    this.configurarClases();
  }


  clasesItems(cont: number) {

    let bool: boolean = false;
    let clasesJson = {};
    // clasesJson =
    // {
    // 	'item-proyecto': true,
    // 	'der-267': (this.contador === 2),
    // 	'izq-658': (this.contador === 3),
    // 	'der-658': (this.contador === 2)
    // }

    // this.contador += 1
    // if (this.contador === 4) {
    // 	this.contador = 0
    // }

    if (cont === 1 || cont === 5) {
      clasesJson =
      {
        'item-proyecto': true,
        'der-267': false,
        'izq-658': true,
        'der-658': false
      };
    }

    if (cont === 2 || cont === 6) {
      clasesJson =
      {
        'item-proyecto': true,
        'der-267': false,
        'izq-658': false,
        'der-658': true
      };
    }

    if (cont === 0 || cont === 3) {
      clasesJson =
      {
        'item-proyecto': true,
        'der-267': false,
        'izq-658': false,
        'der-658': false
      };
    }


    if (cont === 4) {
      clasesJson =
      {
        'item-proyecto': true,
        'der-267': true,
        'izq-658': false,
        'der-658': false
      };
    }

    return clasesJson;
  }

  configurarClases() {
    let cont = 0;
    for (const proyecto of this.dataListaProyectos) {
      this.lista.push({
        configuracion: proyecto,
        clases: this.clasesItems(cont),
      });
      cont++;
    }
  }




}
