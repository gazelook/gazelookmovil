import { JsonDemoNegocio } from 'dominio/logica-negocio/json-demo.negocio';
import { ConfiguracionBuscador } from '@shared/diseno/modelos/buscador.interface';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';
import { ConfiguracionItemListaContactosCompartido, ConfiguracionLineaVerde } from '@shared/diseno/modelos/lista-contactos.interface';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos/lista-contacto.interface';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { ConfiguracionVotarEntidad } from '@shared/diseno/modelos/votar-entidad.interface';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
import { Subject } from 'rxjs';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { ItemCircularCompartido, CapaOpacidad } from '@shared/diseno/modelos/item-cir-rec.interface';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { CodigosCatalogosTipoRol } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogos-tipo-rol.enum';
import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { ComentarioNegocio } from 'dominio/logica-negocio/comentario.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { TipoIconoBarraInferior } from '@shared/diseno/enums/tipo-icono.enum';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { ResumenDataMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { ProyectoService } from '@core/servicios/generales/proyecto.service';
import { PortadaExpandidaComponent } from '@shared/componentes/portada-expandida/portada-expandida.component';
import { ColoresBloquePortada, ConfiguracionPortadaExandida, SombraBloque, TipoBloqueBortada, BloquePortada } from '@shared/diseno/modelos/portada-expandida.interface';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { CodigosCatalogoTipoProyecto } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { FormGroup } from '@angular/forms';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';
import { ConfiguracionMonedaPicker } from '@shared/diseno/modelos/moneda-picker.interface';
import {
    EstilosDelTexto,
    ColorFondoLinea,
    EspesorLineaItem,
    ColorDelTexto,
    ColorDeBorde,
	ColorDeFondo,
	EspesorDelBorde
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { TranslateService } from '@ngx-translate/core';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { SelectorComponent } from '@shared/componentes/selector/selector.component';
import { BuscadorModalComponent } from '@shared/componentes/buscador-modal/buscador-modal.component';
import { InfoAccionSelector } from '@shared/diseno/modelos/info-accion-selector.interface';
import { InfoAccionBuscadorLocalidades } from '@shared/diseno/modelos/info-acciones-buscador-localidades.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { ConfiguracionComentario } from '@shared/diseno/modelos/comentario.interface';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';

@Component({
	selector: 'app-actualizar-proyecto',
	templateUrl: './actualizar-proyecto.component.html',
	styleUrls: ['./actualizar-proyecto.component.scss']
})
export class ActualizarProyectoComponent implements OnInit {
	@ViewChild('toast', { static: false }) toast: ToastComponent
	@ViewChild('portadaExpandida', { static: false }) portadaExpandida: PortadaExpandidaComponent
	@ViewChild('selectorPaises', { static: false }) selectorPaises: SelectorComponent
	@ViewChild('buscadorLocalidades', { static: false }) buscadorLocalidades: BuscadorModalComponent

	// Utils
	public util = FuncionesCompartidas
	public AccionEntidadEnum = AccionEntidad
	public CodigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum
	public imagenesDefecto: Array<ArchivoModel>
	public portadaUrl: string
	public codigosCatalogoTipoProyecto = CodigosCatalogoTipoProyecto
	public comentarioAEliminar: ComentarioModel
	public idCapaFormulario: string
	public puedeCargarMas: boolean
	public puedeHacerScroolAlFinal: boolean
	public $estatusMensajeCompartirTransferencia: Subject<any>
	public fechaMaximaUpdate: Date
	public paraCompartir: boolean
	public idPerfilCoautorParaResponder: string
	public idInternoParaResponder: string
	public archivosLocalStorage: ArchivoModel[]
	public archivosContactos: ArchivoModel[]

	// Parametros de la url
	public params: ProyectoParams

	// Configuracion de capas
	public mostrarCapaLoader: boolean
	public mostrarCapaError: boolean
	public mensajeCapaError: string
	public mostrarCapaNormal: boolean

	// Parametros internos
	public catalogoTipoMoneda: Array<CatalogoTipoMonedaModel>
	public perfilSeleccionado: PerfilModel
	public proyecto: ProyectoModel
	public proyectoForm: FormGroup
	public inputsForm: Array<InputCompartido>
	public maxDescripcion: number
	public inputProyectoCompleto: string
	public listaDeFechas: Date[]
	public formatoFecha: string
	public listaComentarios: PaginacionModel<ConfiguracionComentario>
	public listaComentariosModel: ComentarioModel[]
	public listaContactos: PaginacionModel<ParticipanteAsociacionModel>
	public listaContactosSeleccionados: Array<string>
	public listaContactosOriginal: Array<ConfiguracionItemListaContactosCompartido>
	public listaContactosBuscador: Array<ConfiguracionItemListaContactosCompartido>
	public textoAutor: string

	// Configuraciones
	public confToast: ConfiguracionToast
	public confAppbar: ConfiguracionAppbarCompartida
	public confPortadaExpandida: ConfiguracionPortadaExandida
	public confSelector: ConfiguracionSelector
	public confBuscador: ConfiguracionBuscadorModal
	public confMonedaPicker: ConfiguracionMonedaPicker
	public confBotonHistorico: BotonCompartido
	public confBotonCompartir: BotonCompartido
	public confBotonFullProject: BotonCompartido
	public confBotonLinks: BotonCompartido
	public confBotonPublish: BotonCompartido
	public confBotonTransferir: BotonCompartido
	public confBotonEliminar: BotonCompartido
	public confLineaComentarios: LineaCompartida
	public confBarraInferior: ConfiguracionBarraInferiorInline
	public confDialogoEliminarProyecto: DialogoCompartido
	public confDialogoEliminarComentario: DialogoCompartido
	public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta
	public confVotarEntidad: ConfiguracionVotarEntidad
	public confListaContactoCompartido: ConfiguracionListaContactoCompartido
	public confBuscadorProyectos: ConfiguracionBuscador

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		public proyectoService: ProyectoService,
		private _location: Location,
		private proyectoNegocio: ProyectoNegocio,
		private albumNegocio: AlbumNegocio,
		private mediaNegocio: MediaNegocio,
		private translateService: TranslateService,
		private comentarioNegocio: ComentarioNegocio,
		private generadorId: GeneradorId,		
		private jsonDemoNegocio: JsonDemoNegocio,
	) {
		this.archivosLocalStorage = []
		this.archivosContactos = []
		this.listaContactosSeleccionados = []
		this.listaContactosBuscador = []
		this.listaContactosOriginal = []
		this.idCapaFormulario = 'capa_formulario_' + this.generadorId.generarIdConSemilla()
		this.puedeCargarMas = true
		this.puedeHacerScroolAlFinal = false
		this.imagenesDefecto = []
		this.listaComentariosModel = []
		this.portadaUrl = ''
		this.catalogoTipoMoneda = []
		this.mostrarCapaLoader = false
		this.mostrarCapaError = false
		this.mensajeCapaError = ''
		this.inputsForm = []
		this.maxDescripcion = 1000
		this.inputProyectoCompleto = 'input-file-pdf'
		this.params = { estado: false, accionEntidad: AccionEntidad.VISITAR }
		this.listaDeFechas = []
		this.formatoFecha = 'dd/MM/yyyy'
		this.$estatusMensajeCompartirTransferencia = new Subject()
		this.paraCompartir = true
		this.idPerfilCoautorParaResponder = ''
		this.idInternoParaResponder = ''
		this.textoAutor = ''
	}

	ngOnInit(): void {
		this.obtenerArchivos()
		this.configurarTextoAutor()
		this.inicializarDataListaComentarios()
		this.inicializarFechaMaximaUpdate()
		this.inicializarDataDeLaEntidad()
		this.configurarToast()
		this.configurarAppBar()
		this.configurarLineas()
		this.configurarDialogoEliminarProyecto()
		this.configurarDialogoEliminarComentario()
		this.configurarBarraInferiorInline()
	}

	obtenerArchivos() {
        this.archivosLocalStorage = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal()        
        this.archivosContactos =  this.archivosLocalStorage.filter(element => element.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.DEMO_LISTA_CONTACTOS)   
    }
	ngAfterViewInit(): void {
		setTimeout(() => {
			this.inicializarImagenesPorDefecto()
		})
	}

	inicializarDataListaComentarios() {
		this.listaComentarios = {
			anteriorPagina: false,
			paginaActual: 1,
			proximaPagina: true,
			totalDatos: 0,
			totalPaginas: 0,
			lista: [],
		}
	}

	inicializarFechaMaximaUpdate() {
		this.fechaMaximaUpdate = new Date()
	}

	// Inicializar data de la entidad
	inicializarDataDeLaEntidad() {
		this.inicializarDataParaAccionVisitar()
	}

	async configurarTextoAutor() {
		this.textoAutor = await this.translateService.get('Nombre de contacto:').toPromise()
	}

	async inicializarDataParaAccionVisitar() {
		try {
			this.mostrarCapaLoader = true
			this.proyecto = await this.jsonDemoNegocio.obtenerProyectoDemo().toPromise()

			// Utils
			this.inicializarControlesSegunAccion()
			this.inicializarInputs()
			// Componentes hijos
			this.configurarPortadaExpandidaParaVisita()
			this.inicializarImagenesPorDefecto()
			this.configurarSelector()
			this.configurarBuscador()
			this.configurarMonedaPicker(true)
			this.configurarBotones()
			this.configurarVotarEntidad()
			this.obtenerComentarios()

			this.mostrarCapaLoader = false
		} catch (error) {
			this.mensajeCapaError = 'text31'
			this.mostrarCapaLoader = false
			this.mostrarCapaError = true
		}
	}

	// Obtener la lista de imagenes por defecto
	inicializarImagenesPorDefecto() {
		this.mediaNegocio.obtenerListaArchivosDefault().subscribe(data => {
			data.forEach(item => {
				if (item.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.PROYECTOS) {
					this.imagenesDefecto.push(item)
				}
			})
			this.definirImagenDeLaPortadaExpandida()
		}, error => {
			this.imagenesDefecto = []
			this.definirImagenDeLaPortadaExpandida()
		})
	}

	// Setear imagen por defecto
	definirImagenDeLaPortadaExpandida() {
		let proyecto0 = this.archivosContactos.find(e => e.url.includes('ff51bfce-8ac3-4d11-ae53-07e9cc63d01b.jpg'))            		

		// this.confPortadaExpandida.urlMedia = this.proyectoService.determinarUrlImagenPortada(this.proyecto, this.imagenesDefecto)
		this.confPortadaExpandida.urlMedia = proyecto0.url
		// this.confPortadaExpandida.mostrarLoader = (this.confPortadaExpandida.urlMedia.length > 0)
		this.confPortadaExpandida.mostrarLoader = false
	}

	// Inicializar controles del formulario
	inicializarControlesSegunAccion() {
		this.proyectoForm = this.proyectoService.inicializarControlesFormularioAccionActualizar(this.proyecto)
	}

	// Inicializar los inputs
	inicializarInputs() {
		this.inputsForm = this.proyectoService.configurarInputsDelFormulario(this.proyectoForm, true)
	}

	// Configuracion del appbar
	configurarAppBar() {
		this.confAppbar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => {
				this._location.back()
			},
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v17texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				mostrarCasaHome: true
			}
		}
	}

	configurarPortadaExpandidaParaVisita() {
		this.confPortadaExpandida = {
			urlMedia: '',
			mostrarLoader: false,
			bloques: [],
			eventoDobleTapPortada: {
				activarEvento: true,
				evento: () => {}
			}
		}

		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			CodigosCatalogoTipoAlbum.GENERAL,
			this.proyecto.adjuntos
		)

		if (
			!album ||
			(
				album &&
				album.media &&
				album.media.length === 0
			)
		) {
			const bloque: BloquePortada = {
				colorFondo: ColoresBloquePortada.AZUL_DEBIL,
				llaveTexto: 'm4v1texto5',
				tipo: TipoBloqueBortada.NO_ADDED_INFO
			}
			// this.confPortadaExpandida.bloques.push(bloque)
		}

		if (this.proyecto.actualizado) {
			this.confPortadaExpandida.bloques.push({
				tipo: TipoBloqueBortada.ACTUALUZADO_INFO,
				llaveTexto: 'm3v3texto10',
				colorFondo: ColoresBloquePortada.AMARILLO_BASE,
			})
		}
	}

	configurarSelector() {
		const item: ItemSelector = this.proyectoService.obtenerPaisSeleccionadoEnElProyecto(this.proyecto)

		this.confSelector = {
			tituloSelector: 'm4v5texto10',
			mostrarModal: false,
			inputPreview: {
				mostrar: true,
				input: {
					valor: '',
					placeholder: 'm4v5texto10',
					textoBold: true,
					textoEnMayusculas: true
				}
			},
			seleccionado: item,
			elegibles: [],
			cargando: {
				mostrar: false
			},
			error: {
				mostrarError: false,
				contenido: '',
				tamanoCompleto: false
			},
			quitarMarginAbajo: true,
			evento: (data: InfoAccionSelector) => {}
		}
	}

	configurarBuscador() {
		const item: ItemSelector = this.proyectoService.obtenerLocalidadSeleccionadaEnElProyecto(this.proyecto)
		const pais: ItemSelector = this.proyectoService.obtenerPaisSeleccionadoEnElProyecto(this.proyecto)

		this.confBuscador = {
			seleccionado: item,
			inputPreview: {
				mostrar: true,
				input: {
					placeholder: 'm4v5texto10',
					valor: '',
					auxiliar: item.auxiliar,
					textoBold: true,
					textoEnMayusculas: true
				},
				quitarMarginAbajo: true
			},
			mostrarModal: false,
			inputBuscador: {
				valor: '',
				placeholder: 'Busca tu localidad', 
			},
			resultado: {
				mostrarElegibles: false,
				mostrarCargando: false,
				error: {
					mostrarError: false,
					contenido: '',
					tamanoCompleto: false
				},
				items: []
			},
			pais: pais,
			evento: (data: InfoAccionBuscadorLocalidades) => {}
		}
	}

	configurarMonedaPicker(
		soloLectura: boolean = false
	) {
		const dataMoneda: ResumenDataMonedaPicker = this.proyectoService.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(this.proyecto)
		this.confMonedaPicker = {
			inputCantidadMoneda: {
				valor: dataMoneda.valorEstimado,
				colorFondo: ColorDeFondo.FONDO_AZUL_CLARO,
				colorTexto: ColorDelTexto.TEXTOAZULBASE,
				tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
				estiloDelTexto: EstilosDelTexto.BOLD,
				soloLectura: soloLectura
			},
			selectorTipoMoneda: {
				titulo: {
					mostrar: this.params.accionEntidad === AccionEntidad.CREAR,
					llaveTexto: 'texto1203'
				},
				inputTipoMoneda: {
					valor: dataMoneda.tipoMoneda,
					colorFondo: ColorDeFondo.FONDO_BLANCO,
					colorTexto: ColorDelTexto.TEXTOAZULBASE,
					tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
					estiloDelTexto: EstilosDelTexto.BOLD,
					estiloBorde: {
						espesor: EspesorDelBorde.ESPESOR_018,
						color: ColorDeBorde.BORDER_NEGRO
					}
				},
				elegibles: [],
				seleccionado: dataMoneda.tipoMoneda.seleccionado,
				mostrarSelector: false,
				evento: (accion: InfoAccionSelector) => {},
				mostrarLoader: false,
				error: {
					mostrar: false,
					llaveTexto: ''
				},
				soloLectura: soloLectura
			}
		}
	}

	async configurarBotones() {
		// Boton historico
		this.confBotonHistorico = {
			text: 'm4v5texto18',
			colorTexto: ColorTextoBoton.CELESTE,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}
		// Boton compartir
		this.confBotonCompartir = {
			text: 'm4v5texto19',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}
		// Boton articulo o proyecto completo
		this.confBotonFullProject = {
			text: 'm4v5texto25',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}
		// Boton link
		this.confBotonLinks = {
			text: 'm4v5texto28',
			colorTexto: ColorTextoBoton.VERDE,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}
		// Boton submit
		this.confBotonPublish = {
			text: 'texto675',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}
		// Boton Transferir
		this.confBotonTransferir = {
			text: 'm4v6texto27',
			colorTexto: ColorTextoBoton.AZUL,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}
		// Boton Eliminar
		this.confBotonEliminar = {
			text: 'm4v1texto27',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => {}
		}

		// Obtener y setear textos
		this.confBotonHistorico.text = await this.translateService.get('m4v5texto18').toPromise()
		this.confBotonCompartir.text = await this.translateService.get('m4v5texto19').toPromise()
		this.confBotonFullProject.text = await this.translateService.get('m4v5texto25').toPromise()
		this.confBotonLinks.text = await this.translateService.get('m4v5texto28').toPromise()
		this.confBotonPublish.text = await this.translateService.get('texto675').toPromise()
		this.confBotonTransferir.text = await this.translateService.get('m4v6texto27').toPromise()
		this.confBotonEliminar.text = await this.translateService.get('m4v6texto28').toPromise()
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false, //True para mostrar
			mostrarLoader: false, // true para mostrar cargando en el toast
			cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
		}
	}

	configurarLineas() {
		this.confLineaComentarios = {
			ancho: AnchoLineaItem.ANCHO6382,
			espesor: EspesorLineaItem.ESPESOR071,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
		}
	}

	configurarBarraInferiorInline() {
		this.confBarraInferior = {
			desactivarBarra: true,
			capaColorFondo: {
				mostrar: true,
				anchoCapa: TamanoColorDeFondo.TAMANO100,
				colorDeFondo: ColorDeFondo.FONDO_TRANSPARENCIA_BASE
			},
			estatusError: (error: string) => { },
			placeholder: {
				mostrar: true,
				texto: '',
				configuracion: {
					color: ColorDelTexto.TEXTOAZULBASE,
					enMayusculas: true,
					estiloTexto: EstilosDelTexto.BOLD,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L4_IGUAL
				}
			},
			iconoTexto: {
				icono: {
					mostrar: true,
					tipo: TipoIconoBarraInferior.ICONO_TEXTO,
					eventoTap: (dataApiArchivo: SubirArchivoData) => {
						if (dataApiArchivo.descripcion && dataApiArchivo.descripcion.length > 0) {
							
						}
					}
				},
				capa: {
					mostrar: false
				}
			},
			iconoAudio: {
				icono: {
					mostrar: true,
					tipo: TipoIconoBarraInferior.ICONO_AUDIO,
					eventoTap: (dataApiArchivo: SubirArchivoData) => {},
				},
				capa: {
					siempreActiva: false,
					grabadora: {
						noPedirTituloGrabacion: true,
						usarLoader: true,
						grabando: false,
						duracionActual: 0,
						tiempoMaximo: 300,
						factorAumentoLinea: 100 / 300, // 100% del ancho divido para tiempoMaximo
					}
				}
			},
			botonSend: {
				mostrar: true,
				evento: (dataApiArchivo: SubirArchivoData) => {
				}
			}
		}
	}

	configurarDialogoEliminarProyecto() {
		this.confDialogoEliminarProyecto = {
			mostrarDialogo: false,
			completo: true,
			tipo: TipoDialogo.CONFIRMACION,
			descripcion: 'm4v6texto26',
			listaAcciones: [
				{
					text: 'texto889',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.ROJO,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoEliminarProyecto.mostrarDialogo = false
					},
				},
				{
					text: 'texto436',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.AMARRILLO,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoEliminarProyecto.mostrarDialogo = false
					},
				}
			]
		}
	}

	configurarDialogoEliminarComentario() {
		this.confDialogoEliminarComentario = { 
			mostrarDialogo: false,
			completo: true, 
			tipo: TipoDialogo.CONFIRMACION,
			descripcion: 'm4v6texto32'+'m4v6texto33',
			listaAcciones: [
				{
					text: 'm3v9texto2',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.ROJO,
					enProgreso: false,
					ejecutar: () => {},
				},
				{
					text: 'm3v9texto3',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.AMARRILLO,
					enProgreso: false,
					ejecutar: () => {
						this.confDialogoEliminarComentario.mostrarDialogo = false
					},
				}
			]
		}
	}

	configurarVotarEntidad() {
		this.confVotarEntidad = {
			id: this.proyecto.id || '',
			entidad: CodigosCatalogoEntidad.PROYECTO,
			voto: this.proyecto.voto,
			bloqueTitulo: {
				coloDeFondo: ColorDeFondo.FONDO_CELESTE_CON_OPACIDAD,
				llavesTexto: (!this.proyecto.voto) ? [
					'm4v1texto19',
					'm4v1texto20',
				] : [
						'm4v1texto22',
						'm4v1texto23'
					]
			},
			bloqueBoton: {
				llaveTexto: 'm4v1texto21',
				activarEventoTap: !this.proyecto.voto,
				eventoTap: () => { }
			}
		}
	}

	eventoEnBotonEliminar() {
		this.confDialogoEliminarProyecto.mostrarDialogo = true
	}

	validarRolDelUsuarioEnElProyecto(rolparaValidar: CodigosCatalogosTipoRol): boolean {
		let estado: boolean = false
		if (this.proyecto && this.proyecto.participantes) {
			this.proyecto.participantes.forEach(item => {
				if (
					item &&
					item.coautor &&
					item.coautor._id &&
					item.coautor._id === this.perfilSeleccionado._id &&
					item.roles
				) {
					item.roles.forEach(rol => {
						if (rol.rol.codigo === rolparaValidar) {
							estado = true
						}
					})
				}
			})
		}
		return estado
	}

	obtenerUrlMediaParaItemCircular(perfil: PerfilModel): { url: string, fileDefault: boolean } {
		const album: AlbumModel = this.albumNegocio.obtenerAlbumDelPerfil(CodigosCatalogoTipoAlbum.PERFIL, perfil)
		return (
			album &&
			album.portada &&
			album.portada.principal &&
			album.portada.principal.url
		) ? {
				url: album.portada.principal.url,
				fileDefault: album.portada.principal.fileDefault
			} : { url: '', fileDefault: false }
	}

	configurarItemCirculo(perfil: PerfilModel): ItemCircularCompartido {
		const data: { url: string, fileDefault: boolean } = this.obtenerUrlMediaParaItemCircular(perfil)

		return {
			id: perfil._id,
			idInterno: this.generadorId.generarIdConSemilla(),
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			colorBorde: ColorDeBorde.BORDER_ROJO,
			esVisitante: true,
			mostrarBoton: false,
			mostrarLoader: data.url.length > 0,
			urlMedia: data.url,
			textoBoton: 'Upload Photos',
			usoDelItem: data.fileDefault ? UsoItemCircular.CIRCARITACONTACTODEFECTO : UsoItemCircular.CIRCONTACTO,
			mostrarCorazon: false,
			esBotonUpload: false,
			capaOpacidad: {
				mostrar: false
			},
			activarClick: false,
			activarDobleClick: false,
			activarLongPress: false,
		}
	}

	configurarComentarios(): void {
		let pos = 0
		this.listaComentarios.lista = []
		this.listaDeFechas.forEach(fecha => {
			let idAnterior = ''
			let itemAnterior: ConfiguracionComentario = {}
			this.listaComentariosModel.forEach(comentario => {
				if (this.compararFechas(fecha, new Date(comentario.fechaCreacion))) {
					if (!comentario.coautor.coautor._id || idAnterior !== comentario.coautor.coautor._id || idAnterior.length === 0) {
						if (idAnterior.length > 0) {
							pos += 1
						}

						idAnterior = comentario.coautor.coautor._id || 'no-existente'
						itemAnterior = {
							fecha: fecha,
							coautor: comentario.coautor.coautor,
							idInterno: this.generadorId.generarIdConSemilla(),
							itemCirculo: this.configurarItemCirculo(comentario.coautor.coautor),
							esPropietario: comentario.coautor.coautor._id === this.proyecto.perfil._id,
							puedeBorrar: false,
							comentarios: [
								{
									idPerfilRespuesta: comentario.idPerfilRespuesta,
									comentario: comentario,
									itemCirculo: (comentario.idPerfilRespuesta && comentario.idPerfilRespuesta !== null) ? this.configurarItemCirculo(comentario.idPerfilRespuesta) : null
								}
							],
							couatorSeleccionado: false,
							eventoTap: (comentario: ComentarioModel) => {
								this.comentarioAEliminar = comentario
								this.confDialogoEliminarComentario.mostrarDialogo = true
							},
							eventoTapEnPerfil: (
								idPerfilCoautor: string,
								idInterno: string
							) => {
								const index = this.listaComentarios.lista.findIndex(e => e.coautor._id === idPerfilCoautor && e.idInterno == idInterno)


								this.listaComentarios.lista.forEach(item => {
									item.couatorSeleccionado = false
								})

								if (index < 0 || idPerfilCoautor.length === 0 || idInterno.length === 0) {

									return
								}

								if (
									idPerfilCoautor === this.idPerfilCoautorParaResponder &&
									idInterno === this.idInternoParaResponder
								) {
									this.listaComentarios.lista[index].couatorSeleccionado = false
									this.idPerfilCoautorParaResponder = ''
									this.idInternoParaResponder = ''
									return
								}

								if (
									idInterno !== this.idInternoParaResponder &&
									(
										idPerfilCoautor !== this.idPerfilCoautorParaResponder ||
										idPerfilCoautor === this.idPerfilCoautorParaResponder
									)
								) {
									this.listaComentarios.lista[index].couatorSeleccionado = true
									this.idPerfilCoautorParaResponder = idPerfilCoautor
									this.idInternoParaResponder = idInterno
									return
								}
							}
						}
						this.listaComentarios.lista[pos] = itemAnterior
					} else {
						this.listaComentarios.lista[pos].comentarios.push({
							idPerfilRespuesta: comentario.idPerfilRespuesta,
							comentario: comentario,
							itemCirculo: (comentario.idPerfilRespuesta && comentario.idPerfilRespuesta !== null) ? this.configurarItemCirculo(comentario.idPerfilRespuesta) : null
						})
					}
				}
			})
			pos += 1
		})
	}

	compararFechas(a: Date, b: Date): boolean {
		if (
			a.getFullYear() === b.getFullYear() &&
			a.getMonth() === b.getMonth() &&
			a.getDate() === b.getDate()
		) {
			return true
		}

		return false
	}

	configurarListaDeFechas() {
		this.listaDeFechas = []
		this.listaComentariosModel.forEach(comentario => {
			const date: Date = new Date(comentario.fechaActualizacion)
			let pos = -1
			this.listaDeFechas.forEach((fecha, i) => {
				if (this.compararFechas(date, fecha)) {
					pos = i
				}
			})

			if (pos < 0) {
				this.listaDeFechas.push(date)
			}
		})
	}

	async obtenerComentarios() {
		try {
			const comentarios: ComentarioModel[] = await this.jsonDemoNegocio.obtenerComentarios().toPromise()
			
			this.listaComentariosModel = comentarios
			this.listaComentariosModel[0].fechaCreacionFirebase = 1623699809480
			this.listaComentariosModel[1].fechaCreacionFirebase = 1623699809480
		
			
			this.ordenarComentarios()
			this.configurarListaDeFechas()
			this.configurarComentarios()
		
			let index1 = this.archivosContactos.find(e => e.url.includes('8d20f732-5c12-4c63-9b30-f87e88a8ae38.jpg'))
			this.listaComentarios.lista[0].itemCirculo.urlMedia = index1.url    

			let index2 = this.archivosContactos.find(e => e.url.includes('bb148600-3053-45c0-b468-5b250884ff5c.jpg'))
			this.listaComentarios.lista[1].itemCirculo.urlMedia = index2.url  
			
			
		} catch (error) {
			this.inicializarDataListaComentarios()
		}
	}

	ordenarComentarios() {
		this.listaComentariosModel.sort((x, y) => {
			if (x.fechaCreacion > y.fechaCreacion) {
				return 1
			}

			if (x.fechaCreacion < y.fechaCreacion) {
				return -1
			}

			return 0
		})
	}

	determinarSiHayAlbumSegunTipo(codigo: CodigosCatalogoTipoAlbum) {
		if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
			return true
		}

		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			codigo,
			this.proyecto.adjuntos
		)

		if (album) {
			return true
		}

		return true
	}

	cerrarBuscador() {
		this.listaContactosBuscador = []
	}
}