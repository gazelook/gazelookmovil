import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { PortadaExpandidaComponent } from '@shared/componentes/portada-expandida/portada-expandida.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos/lista-contacto.interface';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';
import { ColoresBloquePortada, ConfiguracionPortadaExandida, SombraBloque, TipoBloqueBortada } from '@shared/diseno/modelos/portada-expandida.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { ConfiguracionVotarEntidad } from '@shared/diseno/modelos/votar-entidad.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ProyectoParams } from 'dominio/modelo/parametros/proyecto-parametros.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { NoticiaService } from '@core/servicios/generales/noticia.service';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';

@Component({
	selector: 'app-publicar-noticia',
	templateUrl: './publicar-noticia.component.html',
	styleUrls: ['./publicar-noticia.component.scss']
})
export class NoticiaPublicarComponent implements OnInit, AfterViewInit {
	@ViewChild('toast', { static: false }) toast: ToastComponent
	@ViewChild('portadaExpandida', { static: false }) portadaExpandida: PortadaExpandidaComponent

	public util = FuncionesCompartidas
	public AccionEntidadEnum = AccionEntidad
	public CodigosCatalogoTipoAlbumEnum = CodigosCatalogoTipoAlbum
	public imagenesDefecto: Array<ArchivoModel>
	public portadaUrl: string
	public idCapaFormulario: string

	// Parametros de la url
	public params: ProyectoParams

	// Configuracion de capas
	public mostrarCapaLoader: boolean
	public mostrarCapaError: boolean
	public mensajeCapaError: string
	public mostrarCapaNormal: boolean

	// Parametros internos
	public perfilSeleccionado: PerfilModel
	public noticia: NoticiaModel
	public noticiaForm: FormGroup
	public inputsForm: Array<InputCompartido>
	public paraCompartir: boolean
	public inputArticuloNoticia: string
	public idPerfilCoautorParaResponder: string
	public idInternoParaResponder: string
	public llaveTextoBotonesExtra: string
	public $estatusMensajeCompartirTransferencia: Subject<any>
	public listaContactos: PaginacionModel<ParticipanteAsociacionModel>
	public listaContactosSeleccionados: Array<string>
	public listaContactosOriginal: Array<ConfiguracionItemListaContactosCompartido>
	public listaContactosBuscador: Array<ConfiguracionItemListaContactosCompartido>

	// Configuraciones
	public confToast: ConfiguracionToast
	public confAppbar: ConfiguracionAppbarCompartida
	public confPortadaExpandida: ConfiguracionPortadaExandida
	public confBotonCompartir: BotonCompartido
	public confBotonArticulo: BotonCompartido
	public confBotonPublish: BotonCompartido
	public confBotonLinks: BotonCompartido
	public confBotonPhotos: BotonCompartido
	public confBotonEliminar: BotonCompartido
	public confVotarEntidad: ConfiguracionVotarEntidad
	public confBotonesExtras: BotonCompartido[]
	public confDialogoEliminarNoticia: DialogoCompartido
	public confListaContactoCompartido: ConfiguracionListaContactoCompartido
	public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		public noticiaService: NoticiaService,
		private noticiaNegocio: NoticiaNegocio,
		private _location: Location,
		private albumNegocio: AlbumNegocio,
		private mediaNegocio: MediaNegocio,
		private translateService: TranslateService,
	) {
		this.params = { estado: true }
		this.mostrarCapaLoader = false
		this.mostrarCapaError = false
		this.mensajeCapaError = ''
		this.inputArticuloNoticia = 'input-file-pdf'
		this.imagenesDefecto = []
		this.confBotonesExtras = []
		this.llaveTextoBotonesExtra = ''
		this.listaContactosSeleccionados = []
		this.listaContactosBuscador = []
		this.listaContactosOriginal = []
		this.$estatusMensajeCompartirTransferencia = new Subject()
	}

	ngOnInit(): void {
		this.inicializarDataDeLaEntidad()
		this.inicializarControlesSegunAccion()
		this.inicializarInputs()
		this.configurarPortadaExpandida()
		this.definirBotonesParaPortadaExpandida()
		this.configurarBotones()
		this.configurarBotonesExtras()
		this.configurarToast()
		this.configurarAppBar()
		this.configurarDialogoEliminarNoticia()
	}

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.inicializarImagenesPorDefecto()
		})
	}

	reintentar() {

	}

	async inicializarImagenesPorDefecto() {
		try {
			const data = await this.mediaNegocio.obtenerListaArchivosDefault().toPromise()
			data.forEach(item => {
				if (item.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.PROYECTOS) {
					this.imagenesDefecto.push(item)
				}
			})
			this.definirUrlMediaDeLaPortada()
		} catch (error) {
			this.imagenesDefecto = []
		}
	}

	definirUrlMediaDeLaPortada() {
		if (!this.noticia || !this.confPortadaExpandida) {
			return
		}

		const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(this.noticia.adjuntos)
		const data = this.noticiaService.determinarUrlImagenPortada(album, this.imagenesDefecto)
		this.confPortadaExpandida.urlMedia = data.url
	}

	inicializarDataDeLaEntidad() {
		this.noticia = this.noticiaNegocio.obtenerNoticiaParaDemo()
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false,
			mostrarLoader: false,
			cerrarClickOutside: false,
		}
	}

	configurarAppBar() {
		this.confAppbar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => this.accionAtras(),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v12texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				mostrarCasaHome: true
			}
		}
	}

	accionAtras() {
		this._location.back()
	}

	inicializarControlesSegunAccion() {
		this.noticiaForm = this.noticiaService.inicializarControlesFormulario(this.noticia)
	}

	inicializarInputs() {
		this.inputsForm = this.noticiaService.configurarInputsDelFormulario(this.noticiaForm)
	}

	configurarPortadaExpandida() {
		this.confPortadaExpandida = {
			urlMedia: '',
			mostrarLoader: true,
			bloques: [
				{
					tipo: TipoBloqueBortada.BOTON_NOTICIA,
					colorFondo: ColoresBloquePortada.AZUL_FUERTE,
					conSombra: SombraBloque.SOMBRA_NEGRA,
					llaveTexto: 'm4v15texto2',
				}
			],
			botones: []
		}
	}

	definirBotonesParaPortadaExpandida() {
		if (this.noticia) {
			const albumGeneral: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.GENERAL,
				this.noticia.adjuntos
			)
			const albumLinks: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.LINK,
				this.noticia.adjuntos
			)

			if (
				this.params.accionEntidad !== AccionEntidad.VISITAR &&
				!albumGeneral &&
				!albumLinks
			) {
				this.insertarBotonesEnPortadaExpandida(true, true)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(null, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}

			if (albumGeneral && albumGeneral.predeterminado) {
				this.insertarBotonesEnPortadaExpandida(true, false)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(albumGeneral, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}

			if (albumLinks && albumLinks.predeterminado) {
				this.insertarBotonesEnPortadaExpandida(false, true)
				if (this.confPortadaExpandida.urlMedia.length === 0) {
					const data = this.noticiaService.determinarUrlImagenPortada(null, this.imagenesDefecto)
					this.confPortadaExpandida.urlMedia = data.url
				}
			}
		}
	}

	insertarBotonesEnPortadaExpandida(
		usarBotonPhotos: boolean = false,
		usarBotonLinks: boolean = false
	) {
		this.confPortadaExpandida.botones = []
		if (usarBotonLinks) {
			this.confPortadaExpandida.botones.push({
				text: 'm4v15texto3',
				enProgreso: false,
				tipoBoton: TipoBoton.TEXTO,
				colorTexto: ColorTextoBoton.VERDE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				ejecutar: () => { }
			})
		}

		if (usarBotonPhotos) {
			this.confPortadaExpandida.botones.push({
				text: 'm4v15texto4',
				enProgreso: false,
				tipoBoton: TipoBoton.TEXTO,
				colorTexto: ColorTextoBoton.CELESTE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				ejecutar: () => { }
			})
		}
	}

	async configurarBotones() {
		// Boton compartir
		this.confBotonCompartir = {
			text: 'm4v15texto10',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton articulo o proyecto completo
		this.confBotonArticulo = {
			text: 'm4v15texto16',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton submit
		this.confBotonPublish = {
			text: 'm4v5texto29',
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}
		// Boton Eliminar
		this.confBotonEliminar = {
			text: 'm4v1texto29',
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => { }
		}

		// Obtener y setear textos
		this.confBotonCompartir.text = await this.translateService.get('m4v15texto10').toPromise()
		this.confBotonArticulo.text = await this.translateService.get('m4v15texto14').toPromise()
		this.confBotonPublish.text = await this.translateService.get('m4v5texto29').toPromise()
		this.confBotonEliminar.text = await this.translateService.get('m4v1texto29').toPromise()
	}

	async configurarBotonesExtras() {
		this.confBotonesExtras = []
		const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(this.noticia.adjuntos)

		if (!album) {
			return
		}


		if (album.tipo.codigo === CodigosCatalogoTipoAlbum.LINK) {

			if (this.params.accionEntidad === AccionEntidad.VISITAR) {
				const albumExtra: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
					CodigosCatalogoTipoAlbum.GENERAL,
					this.noticia.adjuntos
				)

				if (!albumExtra) {
					return
				}
			}

			this.confBotonesExtras.push({
				text: 'texto782',
				colorTexto: ColorTextoBoton.CELESTE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				tipoBoton: TipoBoton.TEXTO,
				enProgreso: false,
				ejecutar: () => { }
			})

			this.confBotonesExtras[0].text = await this.translateService.get('texto782').toPromise()
			this.llaveTextoBotonesExtra = 'texto803'
			return
		}

		if (album.tipo.codigo === CodigosCatalogoTipoAlbum.GENERAL) {

			if (this.params.accionEntidad === AccionEntidad.VISITAR) {
				const albumExtra: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
					CodigosCatalogoTipoAlbum.LINK,
					this.noticia.adjuntos
				)

				if (!albumExtra) {
					return
				}
			}

			this.confBotonesExtras.push({
				text: 'texto805',
				colorTexto: ColorTextoBoton.VERDE,
				tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
				tipoBoton: TipoBoton.TEXTO,
				enProgreso: false,
				ejecutar: () => { }
			})

			this.confBotonesExtras[0].text = await this.translateService.get('texto805').toPromise()
			this.llaveTextoBotonesExtra = 'texto804'
			return
		}
	}

	configurarDialogoEliminarNoticia() {
		this.confDialogoEliminarNoticia = {
			mostrarDialogo: false,
			completo: true,
			tipo: TipoDialogo.CONFIRMACION,
			descripcion: 'm4v16texto10',
			listaAcciones: [
				{
					text: 'm3v9texto2',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.ROJO,
					enProgreso: false,
					ejecutar: () => { },
				},
				{
					text: 'm3v9texto3',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
					tipoBoton: TipoBoton.TEXTO,
					colorTexto: ColorTextoBoton.AMARRILLO,
					enProgreso: false,
					ejecutar: () => { },
				}
			]
		}
	}

	determinarSiHayAlbumSegunTipo(codigo: CodigosCatalogoTipoAlbum) {
		if (this.params.accionEntidad !== AccionEntidad.VISITAR) {
			return true
		}

		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			codigo,
			this.noticia.adjuntos
		)

		if (album) {
			return true
		}

		return false
	}

	validarAlbumPredeterminado(tipo: CodigosCatalogoTipoAlbum) {
		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(tipo, this.noticia.adjuntos)

		if (!album) {
			return false
		}


		return !album.predeterminado
	}

}
