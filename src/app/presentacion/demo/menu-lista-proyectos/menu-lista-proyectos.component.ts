import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
    ColorFondoLinea,
    EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
	selector: 'app-menu-lista-proyectos',
	templateUrl: './menu-lista-proyectos.component.html',
	styleUrls: ['./menu-lista-proyectos.component.scss']
})
export class MenuListaProyectosDemoComponent implements OnInit {
	public MenuListaProyectosEnum = MenuListaProyectos

	// Configuracion hijos
	public confAppBar: ConfiguracionAppbarCompartida
	public confLineaVerde: LineaCompartida
	public confLineaVerdeAll: LineaCompartida
	public confDialogoInicial: DialogoCompartido

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private router: Router,
		private route: ActivatedRoute,
		private _location: Location
	) {   }

	ngOnInit(): void {
		this.configurarAppBar()
		this.configurarLinea()
		this.configurarDialogoDemo()
	}

	configurarDialogoDemo() {
		this.confDialogoInicial = {
			completo: true,
			mostrarDialogo: false,
			tipo: TipoDialogo.INFO_VERTICAL,
			descripcion: 'm1v15texto3',
		}
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_DEMO_APPBAR,
			accionAtras: () => this.volverAtras(),
			demoAppbar: {
				mostrarLineaVerde: true,
				nombrePerfil: {
					mostrar: false,
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm1v15texto2'
				},
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
				mostrarCasaHome: true
			}
		}
	}

	volverAtras() {
		this._location.back()
	}

	configurarLinea() {
		this.confLineaVerde = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
		this.confLineaVerdeAll = {
			ancho: AnchoLineaItem.ANCHO6920,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
		}
	}

	navegarSegunMenu(menuTipo: MenuListaProyectos) {
		switch (menuTipo) {
			case MenuListaProyectos.NUEVOS_PROYECTOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_RECOMENDADOS_POR_ADMIN:
				this.abrirListaProyectos()
				break
			case MenuListaProyectos.FORO_DE_LOS_PROYECTOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_SELECCIONADOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EN_ESPERA_DE_FONDOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EN_ESTRATEGIA:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EN_EJECUCION:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_EJECUTADOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			case MenuListaProyectos.PROYECTOS_MENOS_VOTADOS:
				this.confDialogoInicial.mostrarDialogo = true
				break
			default: break
		}
	}

	accionAtras() {
		this._location.back()
	}

	abrirListaProyectos() {
		let ruta = RutasLocales.MODULO_DEMO.toString()
		let componente = RutasDemo.LISTA_PROYECTOS.toString()
		this.router.navigateByUrl(ruta + '/' + componente)
	}
}

export enum MenuListaProyectos {
	NUEVOS_PROYECTOS = '0',
	PROYECTOS_RECOMENDADOS_POR_ADMIN = '1',
	FORO_DE_LOS_PROYECTOS = '2',
	PROYECTOS_SELECCIONADOS = '3',
	PROYECTOS_EN_ESPERA_DE_FONDOS = '4',
	PROYECTOS_EN_ESTRATEGIA = '5',
	PROYECTOS_EN_EJECUCION = '6',
	PROYECTOS_EJECUTADOS = '7',
	PROYECTOS_MENOS_VOTADOS = '8',
}