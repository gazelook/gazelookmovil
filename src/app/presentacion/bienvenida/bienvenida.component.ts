import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoDeTextoConInterlineado, TamanoPortadaGaze } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import {
	ColorFondoLinea,
	EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';
@Component({
	selector: 'app-bienvenida',
	templateUrl: './bienvenida.component.html',
	styleUrls: ['./bienvenida.component.scss']
})
export class BienvenidaComponent implements OnInit {
	configuracionLinea0: LineaCompartida
	configuracionLinea1: LineaCompartida
	configuracionLinea2: LineaCompartida
	botonEnter: BotonCompartido
	sesionIniciada: boolean

	public portadaGazeComponent: PortadaGazeCompartido

	constructor(
		private variablesGlobales: VariablesGlobales,
		private internacionalizacionNegocio: InternacionalizacionNegocio,
		private router: Router,
		private _location: Location,
		private cuentaNegocio: CuentaNegocio,
		public estiloTexto: EstiloDelTextoServicio,
	) {
		this.cargarDatos()
		this.sesionIniciada = false
	}

	ngOnInit(): void {
		this.detectarDispositivo()
		this.variablesGlobales.mostrarMundo = true
		this.portadaGazeComponent = { tamano: TamanoPortadaGaze.PORTADACOMPLETA, espacioDerecha: false }
		this.verificarSesion()
	}

	detectarDispositivo() {
		if (!(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
			document.location.href = 'https://gazelook.com';
		}
	}
	verificarSesion() {
		this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
	}
	cargarDatos() {
		this.configuracionLinea0 = {
			ancho: AnchoLineaItem.ANCHO100,
			espesor: EspesorLineaItem.ESPESOR071,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			forzarAlFinal: false
		}
		this.configuracionLinea1 = {
			ancho: AnchoLineaItem.ANCHO6382,
			espesor: EspesorLineaItem.ESPESOR071,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			forzarAlFinal: false
		}
		this.configuracionLinea2 = {
			ancho: AnchoLineaItem.ANCHO7416,
			espesor: EspesorLineaItem.ESPESOR071,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			forzarAlFinal: true
		}
		this.botonEnter = {
			text: this.internacionalizacionNegocio.obtenerTextoSincrono('m1v1texto6'),
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			colorTexto: ColorTextoBoton.AMARRILLO,
			tipoBoton: TipoBoton.TEXTO,
			enProgreso: false,
			ejecutar: () => this.navegarMenuPrincipal()
		}
	}
	navegarMenuPrincipal() {
		this.router.navigate([RutasLocales.MODULO_DEMO + '/' + RutasDemo.MENU_PRINCIPAL]);
	}

	ngAfterViewInit(): void {
		setTimeout(async () => {
			this.variablesGlobales.mostrarMundo = true
		})
	}
	// Eventos de click
	clickBotonAtras() {
		this._location.back();
	}
	async cambiarIdioma() {

		this.botonEnter.text = await this.internacionalizacionNegocio.obtenerTextoLlave('m1v1texto6')
	}
}
