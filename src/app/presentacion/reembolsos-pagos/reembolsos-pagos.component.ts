import { TamanoColorDeFondo } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-reembolsos-pagos',
  templateUrl: './reembolsos-pagos.component.html',
  styleUrls: ['./reembolsos-pagos.component.scss']
})
export class ReembolsosPagosComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  public headersTable = ['ID', 'MONTO', 'FECHA', 'ACCIONES'];

  public confAppBar: ConfiguracionAppbarCompartida;

  constructor(
    private location: Location
  ) { }

  ngOnInit(): void {
    this.configurarAppBar();
  }

  configurarAppBar(): void {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => this.onRegresar(),
      searchBarAppBar: {
        nombrePerfil: {
          mostrar: false,
          llaveTexto: ''
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarTextoHome: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'TRADUCIR TITULO'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      }
    };
  }



  onRegresar(): void {
    this.location.back();
  }

}
