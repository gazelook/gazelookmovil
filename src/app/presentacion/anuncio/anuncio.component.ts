import {LlavesSessionStorage} from '@core/servicios/locales/llaves/session-storage.enum';
import {Router} from '@angular/router';
import {Component, OnInit, AfterViewInit, HostListener} from '@angular/core';

@Component({
  selector: 'app-anuncio',
  templateUrl: './anuncio.component.html',
  styleUrls: ['./anuncio.component.scss']
})
export class AnuncioComponent implements OnInit, AfterViewInit {

  public mostrarCapas: Array<boolean>;
  public interval: any;
  public pos: number;
  public paginas: number;

  constructor(
    private router: Router
  ) {
    this.mostrarCapas = [];
    this.pos = 0;
    this.paginas = 0;
  }

  ngOnInit(): void {
    this.detectarDispositivo();
    this.validarPaginaDeDondeProviene();
    this.inicializarCapas();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.mostrarCapasMetodo();
    });
  }

  validarPaginaDeDondeProviene(): void {
    const a = sessionStorage.getItem(LlavesSessionStorage.PAGINAS.toString());

    if (a && a !== null) {
      // tslint:disable-next-line:radix
      this.paginas = parseInt(a);
    } else {
      sessionStorage.setItem(LlavesSessionStorage.PAGINAS.toString(), this.paginas.toString());
    }
  }

  detectarDispositivo(): void {
    if (!(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
      document.location.href = 'https://gazelook.com/web';
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    this.detectarDispositivo();
  }

  inicializarCapas(): void {
    for (let i = 0; i < 9; i++) {
      this.mostrarCapas[i] = false;
    }
  }

  mostrarCapasMetodo(): void {
    setTimeout(() => {
      this.mostrarCapas[this.pos] = true;
      this.pos += 1;
      setTimeout(() => {
        this.mostrarCapas[this.pos] = true;
        this.pos += 1;
        setTimeout(() => {
          this.mostrarCapas[this.pos] = true;
          this.pos += 1;
          setTimeout(() => {
            this.mostrarCapas[this.pos] = true;
            this.pos += 1;
            setTimeout(() => {
              this.mostrarCapas[this.pos] = true;
              this.pos += 1;
            }, 1900);
          }, 1600);
        }, 1400);
      }, 1200);
    }, 800);
  }

  retornar(): void {
    // this.router.navigateByUrl(RutasLocales.LANDING.toString())
  }
}
