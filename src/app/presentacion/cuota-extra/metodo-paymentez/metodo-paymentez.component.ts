import { CodigosCatalogoOrigenTransaccion } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-origen-transaccion.enum';

import { Component, OnInit, Input, ViewChild } from '@angular/core';

import {
  CuentaNegocio,
  IdiomaNegocio,
  PagoNegocio,
  ProyectoNegocio,
  TipoMonedaNegocio,
} from 'dominio/logica-negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno';
import { MonedaPickerService } from '@core/servicios/generales';
import {
  CatalogoMetodoPagoModel,
  CatalogoTipoMonedaModel,
  PagoFacturacionModelDonacionProyectos,
  PaymentezTransaccion,
} from 'dominio/modelo/catalogos';
import {
  CodigosCatalogosMetodosDePago,
  CodigosCatalogoTipoMoneda,
  CodigosCatalogoTipoProyecto,
  CodigosEstadoMetodoPago,
} from '@core/servicios/remotos/codigos-catalogos';

import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos';
import {
  BotonCompartido,
  ConfiguracionDone,
  ConfiguracionMonedaPicker,
  ConfiguracionToast,
  DialogoCompartido,
  InfoAccionSelector,
  ItemSelector,
  ResumenDataMonedaPicker,
  ValorBase,
} from '@shared/diseno/modelos';
import {
  ColorTextoBoton,
  ModalContenido,
  TipoBoton,
  ToastComponent,
} from '@shared/componentes';
import {
  AccionesSelector,
  COD_MONEDAS_PAYMENTEZ,
  ColorDeBorde,
  ColorDeFondo,
  ColorDelTexto,
  EspesorDelBorde,
  EstilosDelTexto,
  TamanoDeTextoConInterlineado,
} from '@shared/diseno/enums';
import { ActivatedRoute } from '@env/node_modules/@angular/router';
import { DonacionProyectosModel, UsuarioModel } from 'dominio/modelo/entidades';
import { ProyectoParams } from 'dominio/modelo/parametros';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/src/environments/environment';
import { Location } from '@angular/common';

declare var PaymentCheckout;

@Component({
  selector: 'app-metodo-paymentez',
  templateUrl: './metodo-paymentez.component.html',
  styleUrls: ['./metodo-paymentez.component.scss'],
})
export class MetodoPaymentezComponent implements OnInit {
  @Input() item: CatalogoMetodoPagoModel;

  @ViewChild('toast', { static: false }) toast: ToastComponent;
  public configuracionToast: ConfiguracionToast;

  private paymentCheckout;

  public idInputCantidadAPagarPaymentez: string;

  public confMonedaPickerPaymentez: ConfiguracionMonedaPicker;

  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public codigosCatalogos = CodigosCatalogosMetodosDePago;

  public tipoMonedaLocales: ItemSelector[] = [];

  public idiomaSeleccionado: CatalogoIdiomaEntity;
  private idiomaPaymentez: string;
  public terminosPagoPaymentez = false;
  public valorBaseAPagar: ValorBase;

  public montoPagarPaymentez: number;
  public monedaSeleccionadaPaymentez: string;

  private codigoPagoPaymentez: string;

  public confBotonMontoPagoPaymentez: BotonCompartido;
  public dataModalMontoPaymentezPago: ModalContenido;
  public dataModalPaymentez: ModalContenido;
  public configDialoPagoPaymentez: DialogoCompartido;
  public configDialoPagoPaymentezError: DialogoCompartido;

  public descripcionPagoContinuarPaymentez: string;
  public mostrarModalPagoContinuarPaymentez = false;
  public dataModalMensajePaymentezPago: ModalContenido;

  public mensajePaimentez: string;

  private usuario: UsuarioModel;

  public params: ProyectoParams;

  public confDone: ConfiguracionDone;

  constructor(
    private pagoNegocio: PagoNegocio,
    private idiomaNegocio: IdiomaNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private monedaPickerService: MonedaPickerService,
    private cuentaNegocio: CuentaNegocio,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private proyectoNegocio: ProyectoNegocio,
    private location: Location
  ) {
    this.idInputCantidadAPagarPaymentez = 'KJ21-0982jjs';
    this.params = { estado: false };
  }

  ngOnInit(): void {
    this.configurarParametros();
    this.obtenerIdioma();
    this.obtenerUsuarioCuentaNegocio();
    this.funcionRedondear();
    this.configurarBotonPaymentez();
    this.configurarMonedaPickerPaymentez();
    this.confDialogosMontoPagar();
    this.configurarDialogoContenido();
    this.confDialogosMontoPagar();
    this.configurarDone();
    this.configurarToast();
    this.configurarTextoPaymentz().then();

    this.paymentCheckout = new PaymentCheckout.modal({
      client_app_code: environment.clientAppCodePaymentez,
      client_app_key: environment.clientAppKeyPaymentez,
      locale: this.idiomaPaymentez,
      env_mode: environment.envModePaymentez,
      onOpen: () => {},
      onClose: () => {
        // this.configurarTextoPaymentz(true).then();
      },
      onResponse: (response: PaymentezTransaccion) => {
 
        const {
          transaction: { id, authorization_code, status },
        } = response;
        if (status === 'success') {
          this.confDone.mostrarDone = true;
          this.donarProyectosMetodoStripe(id, authorization_code);
        } else {
          this.toast.abrirToast('text37');
        }
      },
    });
  }

  public navegarMetodoPagoPaymentez(metodoPago: CatalogoMetodoPagoModel): void {
    metodoPago.codigo === CodigosCatalogosMetodosDePago.PAYMENTEZ
      ? (this.dataModalMontoPaymentezPago.abierto = true)
      : (this.dataModalMontoPaymentezPago.abierto = false);
    this.codigoPagoPaymentez = metodoPago.codigo;
  }

  public navegarMetodoPagoPaymentez2(
    metodoPago: CatalogoMetodoPagoModel
  ): void {
    metodoPago.codigo === CodigosCatalogosMetodosDePago.PAYMENTEZ2
      ? (this.dataModalMontoPaymentezPago.abierto = true)
      : (this.dataModalMontoPaymentezPago.abierto = false);
    this.codigoPagoPaymentez = metodoPago.codigo;
  }

  private configurarBotonPaymentez(): void {
    this.confBotonMontoPagoPaymentez = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        this.confirmarMontoPagoPaymentez().then();
      },
    };
  }

  private async confirmarMontoPagoPaymentez(): Promise<void> {
    const montoIngresadoPaymentez = parseFloat(
      this.confMonedaPickerPaymentez.inputCantidadMoneda.valor.valorFormateado
    );
    const codigoMonedaPaymentez =
      this.confMonedaPickerPaymentez.selectorTipoMoneda.seleccionado.codigo;
    const monedaSeleccionadaPaymentez =
      this.confMonedaPickerPaymentez.selectorTipoMoneda.seleccionado.auxiliar;

    this.dataModalMontoPaymentezPago.bloqueado = true;
    this.confBotonMontoPagoPaymentez.enProgreso = true;
    this.dataModalMontoPaymentezPago.error = false;
    this.dataModalMontoPaymentezPago.errorContenido = '';

    if (!(montoIngresadoPaymentez > 0)) {
      this.dataModalMontoPaymentezPago.bloqueado = false;
      this.confBotonMontoPagoPaymentez.enProgreso = false;
      this.dataModalMontoPaymentezPago.error = true;
      this.dataModalMontoPaymentezPago.errorContenido = 'text40';
      return;
    }

    if (!(codigoMonedaPaymentez && monedaSeleccionadaPaymentez)) {
      this.dataModalMontoPaymentezPago.bloqueado = false;
      this.confBotonMontoPagoPaymentez.enProgreso = false;
      this.dataModalMontoPaymentezPago.error = true;
      this.dataModalMontoPaymentezPago.errorContenido = 'text41';
      return;
    }
    this.monedaSeleccionadaPaymentez = monedaSeleccionadaPaymentez;
    try {
      this.montoPagarPaymentez = await this.convertirMonto(
        montoIngresadoPaymentez,
        this.monedaSeleccionadaPaymentez
      );

      if (!this.terminosPagoPaymentez) {
        this.dataModalMontoPaymentezPago.bloqueado = false;
        this.confBotonMontoPagoPaymentez.enProgreso = false;
        this.dataModalMontoPaymentezPago.error = true;
        this.dataModalMontoPaymentezPago.errorContenido = 'text101';
        return;
      }
      this.dataModalMontoPaymentezPago.abierto = false;
      this.dataModalMontoPaymentezPago.bloqueado = false;
      this.confBotonMontoPagoPaymentez.enProgreso = false;

      this.dataModalMensajePaymentezPago.abierto = true;
      this.dataModalMensajePaymentezPago.bloqueado = true;

      setTimeout(() => {
        this.dataModalMensajePaymentezPago.abierto = false;
        this.dataModalMensajePaymentezPago.bloqueado = false;
      }, 8000);
      setTimeout(() => {
        this.paymentCheckout.open({
          user_id: this.usuario.id,
          user_email: this.usuario.email,
          user_phone: '',
          order_description: 'Cuota Extra',
          order_amount: this.montoPagarPaymentez,
          order_vat: 0,
          order_reference: '#234323411',
          order_installments_type: -1,
          order_taxable_amount: 0,
          order_tax_percentage: 0,
        });
      }, 5000);
    } catch (error) {

      this.toast.abrirToast('text37');
    }
  }

  private donarProyectosMetodoStripe(
    idPago: string,
    autorizacionCodePaymentez: string
  ): void {
    this.toast.abrirToast('', true);
    this.dataModalMensajePaymentezPago.abierto = false;
    const datosPago: PagoFacturacionModelDonacionProyectos = {
      idPago,
      nombres: this.usuario.perfiles[0].nombre,
      email: this.usuario.email,
      telefono: this.usuario.perfiles[0].telefonos[0]?.numero || '09xxxxxxxx',
      direccion: this.usuario.perfiles[0].direcciones[0].pais.nombre,
      monto: this.montoPagarPaymentez,
    };
    // const donacionProyecto: DonacionProyectosModel = {
    //   idUsuario: this.usuario.id,
    //   idProyecto: this.params.codigoTipoProyecto,
    //   metodoPago: {
    //     codigo: this.codigoPagoPaymentez
    //   },
    //   transacciones: [
    //     {
    //       monto: this.montoPagarPaymentez,
    //       moneda: {
    //         codNombre: CodigosCatalogoTipoMoneda.USD,
    //       }
    //     }
    //   ],
    //   datosFacturacion: datosPago,
    //   monedaRegistro: {
    //     codNombre: this.monedaSeleccionadaPaymentez
    //   },
    //   email: this.usuario.email,
    //   autorizacionCodePaymentez
    // };

    const dataEnviar = {
      usuario: {
        _id: this.usuario.id,
      },
      metodoPago: {
        codigo: CodigosCatalogosMetodosDePago.PAYMENTEZ,
      },
      transacciones: [
        {
          monto: this.montoPagarPaymentez,
          moneda: {
            codNombre: CodigosCatalogoTipoMoneda.USD,
          },
        },
      ],
      datosFacturacion: {
        nombres: this.usuario.perfiles[0].nombre,
        email: this.usuario.email,
        telefono: this.usuario.perfiles[0].telefonos[0]?.numero,
        direccion: this.usuario.perfiles[0].direcciones[0].pais.nombre,
        monto: this.montoPagarPaymentez,
        idPago: idPago,
      },

      monedaRegistro: {
        codNombre: this.monedaSeleccionadaPaymentez,
      },
      email: this.usuario.email,
      autorizacionCodePaymentez: autorizacionCodePaymentez,
    };


    this.pagoNegocio.crearOrdenPagoExtra(dataEnviar).subscribe(
      (res) => {
        this.confDone.mostrarDone = true;
        setTimeout(() => {
          this.accionAtras();
        }, 3000);
        this.confBotonMontoPagoPaymentez.enProgreso = false;
      },
      (error) => {
        this.toast.abrirToast('text98');
        this.dataModalMensajePaymentezPago.error = true;
        this.dataModalMensajePaymentezPago.errorContenido = 'text98';
        this.confBotonMontoPagoPaymentez.enProgreso = false;
      }
    );
  }

  configurarDone(): void {
    this.confDone = {
      mostrarDone: false,
      intervalo: 4000,
      mostrarLoader: false,
    };
  }
  //  Modals
  private configurarDialogoContenido(): void {
    this.dataModalPaymentez = {
      titulo: 'text72',
      abierto: false,
      bloqueado: false,
      id: 'pagopaymentez',
    };
  }

  private confDialogosMontoPagar(): void {
    this.dataModalMontoPaymentezPago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'montopaymentez',
    };
    this.dataModalMensajePaymentezPago = {
      titulo: '',
      abierto: false,
      bloqueado: false,
      id: 'mensajepaymentez',
    };
  }

  private configurarMonedaPickerPaymentez(): void {
    const dataMoneda: ResumenDataMonedaPicker =
      this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();
    this.confMonedaPickerPaymentez = {
      inputCantidadMoneda: {
        valor: {
          valorFormateado: '0',
        },
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true,
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15',
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO,
          },
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) =>
          this.eventoEnSelectorDeTipoMonedaPaymentez(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: '',
        },
      },
    };
  }

  private eventoEnSelectorDeTipoMonedaPaymentez(
    accion: InfoAccionSelector
  ): void {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMonedaPaymentez();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMonedaPaymentez(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMonedaPaymentez();
        break;
      default:
        break;
    }
  }

  seleccionarTipoMonedaPaymentez(item: ItemSelector): void {
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarSelector = false;
    this.confMonedaPickerPaymentez.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = false;
    this.confMonedaPickerPaymentez.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado =
      item.auxiliar || '';
  }

  private abrirSelectorMonedaPaymentez(): void {
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMonedaPaymentez();
  }

  private inicializarDataCatalogoMonedaPaymentez(): void {
    this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(
      (elegibles) => {
        this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
        const tipoMonedaPaymentez = [];
        this.tipoMonedaLocales = elegibles;
        this.tipoMonedaLocales.map((item) => {
          if (COD_MONEDAS_PAYMENTEZ.includes(item.codigo)) {
            tipoMonedaPaymentez.push(item);
          }
        });
        this.confMonedaPickerPaymentez.selectorTipoMoneda.elegibles =
          tipoMonedaPaymentez;
        this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = false;
        this.confMonedaPickerPaymentez.selectorTipoMoneda.error.mostrar = false;
      },
      (error) => {
        this.confMonedaPickerPaymentez.selectorTipoMoneda.elegibles = [];
        this.confMonedaPickerPaymentez.selectorTipoMoneda.error.llaveTexto =
          'text31';
        this.confMonedaPickerPaymentez.selectorTipoMoneda.error.mostrar = true;
        this.confMonedaPickerPaymentez.selectorTipoMoneda.mostrarLoader = false;
      }
    );
  }

  public eventoModalPaymentez(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal-monto-paymentez') {
        if (!this.dataModalMontoPaymentezPago.bloqueado) {
          this.dataModalMontoPaymentezPago.abierto = false;
        }
      }
    });
  }

  private obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    const valorEstimado =
      this.valorBaseAPagar && this.valorBaseAPagar.valorNeto
        ? parseInt(this.valorBaseAPagar.valorNeto, 0)
        : 0;
    const moneda: CatalogoTipoMonedaModel = {
      codigo: this.valorBaseAPagar?.seleccionado?.codigo || '',
      codNombre: this.valorBaseAPagar?.seleccionado?.auxiliar || '',
    };

    return {
      valorEstimado:
        this.monedaPickerService.obtenerValorAPagarDeLaSuscripcion(
          valorEstimado
        ),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda),
    };
  }

  private obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
    if (this.idiomaSeleccionado) {
      this.idiomaPaymentez =
        this.idiomaSeleccionado?.codNombre === 'en' ||
        this.idiomaSeleccionado?.codNombre === 'es' ||
        this.idiomaSeleccionado?.codNombre === 'pt'
          ? this.idiomaSeleccionado?.codNombre
          : 'en';
    }
  }

  public irDocumentosLegales(): void {
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `https://d3ubht94yroq8c.cloudfront.net/condiciones-pago/condiciones-pago-${this.idiomaSeleccionado.codNombre.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(
      new MouseEvent('click', {
        view: window,
        bubbles: false,
        cancelable: true,
      })
    );
  }

  public checkValueTerminosPagoPaymentez(_: any): void {}

  public obtenerClasesInputCantidadMoneda(): any {
    const clases = {};
    clases['input-cantidad'] = true;
    clases[
      this.confMonedaPickerPaymentez.inputCantidadMoneda.colorFondo.toString()
    ] = true;
    clases[
      this.confMonedaPickerPaymentez.inputCantidadMoneda.colorTexto.toString()
    ] = true;
    clases[
      this.confMonedaPickerPaymentez.inputCantidadMoneda.estiloDelTexto.toString()
    ] = true;
    clases[
      this.confMonedaPickerPaymentez.inputCantidadMoneda.tamanoDelTexto.toString()
    ] = true;
    if (this.confMonedaPickerPaymentez.inputCantidadMoneda.estiloBorde) {
      clases[
        this.confMonedaPickerPaymentez.inputCantidadMoneda.estiloBorde.toString()
      ] = true;
    }
    return clases;
  }

  private async convertirMonto(
    monto: number,
    tipoMoneda: string
  ): Promise<number> {
    const montoIngresado = monto;
    if (
      (tipoMoneda as CodigosCatalogoTipoMoneda) ===
      CodigosCatalogoTipoMoneda.USD
    ) {
      return montoIngresado;
    } else {
      // tslint:disable-next-line: no-string-literal
      return Math['round10'](
        await this.tipoMonedaNegocio
          .convertirMontoEntreMonedas(
            montoIngresado,
            tipoMoneda as CodigosCatalogoTipoMoneda,
            CodigosCatalogoTipoMoneda.USD
          )
          .toPromise(),
        -2
      );
    }
  }

  configurarParametros(): void {
    this.activatedRoute.paramMap.subscribe(
      (params) =>
        (this.params.codigoTipoProyecto = params.get(
          'codigoTipoProyecto'
        ) as CodigosCatalogoTipoProyecto)
    );
  }

  obtenerUsuarioCuentaNegocio(): void {
    this.usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
  }

  async configurarTextoPaymentz(estado: boolean = false): Promise<void> {
    const texto = 'text107';
    const texto2 = 'text37';
    estado
      ? (this.mensajePaimentez = await this.translateService
          .get(texto2)
          .toPromise())
      : (this.mensajePaimentez = await this.translateService
          .get(texto)
          .toPromise());
  }

  private configurarToast(): void {
    this.configuracionToast = {
      cerrarClickOutside: false,
      bloquearPantalla: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: '',
    };
  }

  accionAtras(): void {
    this.location.back();
  }

  private funcionRedondear(): void {
    // tslint:disable-next-line: no-string-literal
    if (!Math['round10']) {
      // tslint:disable-next-line: no-string-literal
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp);
      };
    }
  }

  ajustarDecimales(type: any, value: any, exp: any): any {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? +value[1] - exp : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? +value[1] + exp : exp));
  }
}
