import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetodoPaymentezComponent } from './metodo-paymentez.component';

describe('MetodoPaymentezComponent', () => {
  let component: MetodoPaymentezComponent;
  let fixture: ComponentFixture<MetodoPaymentezComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetodoPaymentezComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MetodoPaymentezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
