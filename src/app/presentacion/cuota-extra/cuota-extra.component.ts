import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import {
  ColorFondoLinea,
  EspesorLineaItem,
} from '@shared/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
  CodigosCatalogosMetodosDePago,
  CodigosEstadoMetodoPago,
} from '@core/servicios/remotos/codigos-catalogos';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { MonedaPickerService } from '@core/servicios/generales/moneda-picker.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {
  TamanoColorDeFondo,
  TamanoLista,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { IdiomaNegocio } from 'dominio/logica-negocio/idioma.negocio';
import { TipoMonedaNegocio } from 'dominio/logica-negocio/moneda.negocio';
import { PagoNegocio } from 'dominio/logica-negocio/pago.negocio';
import { CatalogoMetodoPagoModel } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { StripeService } from 'ngx-stripe';

@Component({
  selector: 'app-cuota-extra',
  templateUrl: './cuota-extra.component.html',
  styleUrls: ['./cuota-extra.component.scss'],
})
export class CuotaExtraComponent implements OnInit {
  public confAppBar: ConfiguracionAppbarCompartida;
  idiomaSeleccionado: CatalogoIdiomaEntity;
  private usuario: UsuarioModel;
  public dataLista: DatosLista;
  public listaMetodoPago: CatalogoMetodoPagoModel[];
  public confLinea: LineaCompartida;
  public codigosCatalogos = CodigosCatalogosMetodosDePago;
  public codigosEstadoMetodoPago = CodigosEstadoMetodoPago;
  public metodoPagoCriptomoneda: string =
    CodigosCatalogosMetodosDePago.CRIPTOMONEDA;
  constructor(
    private cuentaNegocio: CuentaNegocio,
    private pagoNegocio: PagoNegocio,
    private location: Location,
    private variablesGlobales: VariablesGlobales,
    public estiloTexto: EstiloDelTextoServicio,
    private idiomaNegocio: IdiomaNegocio,
    public estiloTextoServicio: EstiloDelTextoServicio
  ) {}

  ngOnInit(): void {
    this.configurarAppBar();
    this.obtenerIdioma();
    this.prepararLista();
    this.configurarLinea();
    this.obtenerCatalogoMetodosPago();
    this.variablesGlobales.mostrarMundo = false;
  }

  // Métodos pago
  private obtenerCatalogoMetodosPago(): void {
    this.dataLista.cargando = true;
    this.pagoNegocio.obtenerCatalogoMetodoPago().subscribe(
      (resp: CatalogoMetodoPagoModel[]) => {
        this.dataLista.cargando = false;
        this.listaMetodoPago = resp;
      },
      (error) => {
        this.dataLista.cargando = false;
        this.dataLista.error = error;
      }
    );
  }

  //  conf linea verde
  private configurarLinea(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6386,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
      forzarAlFinal: false,
    };
  }

  private prepararLista(): void {
    this.dataLista = {
      cargando: true,
      reintentar: this.obtenerCatalogoMetodosPago,
      lista: this.listaMetodoPago,
      tamanoLista: TamanoLista.TIPO_PERFILES,
    };
  }

  obtenerIdioma(): void {
    this.idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();
  }

  async configurarAppBar(): Promise<void> {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {

        this.onRegresar();
      },
      searchBarAppBar: {
        nombrePerfil: {
          mostrar: false,
          llaveTexto: '',
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          },
        },
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarTextoHome: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm2v9texto16',
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      },
    };
  }

  onRegresar(): void {
    this.location.back();
  }
}
