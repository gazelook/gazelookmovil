import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetodoStripeComponent } from './metodo-stripe.component';

describe('MetodoStripeComponent', () => {
  let component: MetodoStripeComponent;
  let fixture: ComponentFixture<MetodoStripeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetodoStripeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MetodoStripeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
