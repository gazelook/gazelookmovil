// import { Location } from '@angular/common';
// import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
// import { ActivatedRoute, Router } from '@angular/router';
// import { TamanoColorDeFondo } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
// import { UsoAppBar } from 'src/app/compartido/diseno/enums/uso-appbar.enum';
// import { ConfiguracionAppbarCompartida } from 'src/app/compartido/diseno/modelos/appbar.interface';
// import { AlbumNegocio } from 'src/app/dominio/logica-negocio/album.negocio';
// import { ParticipanteAsociacionNegocio } from 'src/app/dominio/logica-negocio/participante-asociacion.negocio';
// import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
// import { AsociacionModel } from 'src/app/dominio/modelo/entidades/asociacion.model';
// import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
// import { EstiloDelTextoServicio } from 'src/app/nucleo/servicios/diseno/estilo-del-texto.service';
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
// import { AgoraService } from 'src/app/nucleo/servicios/generales/llamada/agora.service';
// import { CodigoCatalogoAccionLlamada, CodigoCatalogoTipoLlamada, CodigosCatalogoEstadoLlamada, CodigosCatalogoEstatusLLamada, CodigosCatalogoEstatusPerfil } from 'src/app/nucleo/servicios/generales/llamada/llamada.enum';
// import { CanalActivo, IndicadorAsociacion, Llamada, OpcionesLlamada } from 'src/app/nucleo/servicios/generales/llamada/llamada.interface';
// import { VariablesGlobales } from 'src/app/nucleo/servicios/generales/variables-globales.service';
// import { CodigosCatalogoTipoAlbum } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
// import { MetodosSessionStorageService } from 'src/app/nucleo/util/metodos-session-storage.service';
// import { LlavesSessionStorage } from 'src/app/nucleo/servicios/locales/llaves/session-storage.enum';

// @Component({
// 	selector: 'app-chat-llamada',
// 	templateUrl: './chat-llamada.component.html',
// 	styleUrls: ['./chat-llamada.component.scss']
// })
// export class ChatLlamadaComponent implements OnInit, OnDestroy {
	
// 	public CodigoCatalogoTipoLlamadaEnum = CodigoCatalogoTipoLlamada
// 	public CodigoCatalogoAccionLlamadaEmun = CodigoCatalogoAccionLlamada
// 	public CodigosCatalogoEstatusLLamadaEnum = CodigosCatalogoEstatusLLamada

// 	public params: ChatLlamadaParams
// 	public llamada: Llamada
// 	public perfilSeleccionado: PerfilModel
// 	public asociacion: AsociacionModel
// 	public opciones: OpcionesLlamada
// 	public notificacion: Notification
// 	public intervalo: any

// 	public canales: Array<CanalActivo>
// 	public canalActivo: CanalActivo

// 	public confAppbar: ConfiguracionAppbarCompartida

// 	constructor(
// 		public estiloDelTextoServicio: EstiloDelTextoServicio,
// 		public agoraService: AgoraService,
// 		private perfilNegocio: PerfilNegocio,
// 		private variablesGlobales: VariablesGlobales,
// 		private router: Router,
// 		private _location: Location,
// 		private route: ActivatedRoute,
// 		private participanteAsoNegocio: ParticipanteAsociacionNegocio,
// 		private llamadaFirebaseService: LlamadaFirebaseService,
// 		private albumNegocio: AlbumNegocio
// 	) {

// 	}

// 	ngOnInit(): void {
// 		this.variablesGlobales.mostrarMundo = false
// 		this.agoraService.reiniciarServicio()
// 		this.configurarParams()
// 		this.configurarPerfilSeleccionado()
// 		this.configurarOpcionesLlamada()

// 		console.warn('--------------------------------')
// 		console.warn(this.params)
// 		console.warn('--------------------------------')
// 		if (this.params.estado && this.perfilSeleccionado) {
// 			this.configurarAppBar()
// 			this.configurarEscuchaRegistrarLlamada()
// 			this.configurarEscuchaErrorEnLlamada()
// 			this.configurarInformacionDeLaLlamada()
// 			return
// 		}
	
// 		this.accionAtras()
// 	}

// 	ngOnDestroy(): void {
// 		this.desconectar()
// 	}

// 	configurarAppBar(
// 		nombreContacto: string = this.opciones.nombreContacto,
// 		estadoLLamada: string = this.opciones.estadoLLamada
// 	) {
//         this.confAppbar = {
//             accionAtras: () => this.accionAtras(),
//             usoAppBar: UsoAppBar.USO_CONFERENCIA_APPBAR_MINI,
//             searchBarAppBar: {
//                 mostrarDivBack: {
//                     icono: true,
//                     texto: false
//                 },
//                 mostrarLineaVerde: true,
//                 mostrarTextoHome: false,
//                 mostrarBotonXRoja: false,
//                 tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
//                 nombrePerfil: {
//                     mostrar: true,
//                     llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre
//                 },
//                 buscador: {
//                     mostrar: false,
//                     configuracion: {
//                         disable: true,
//                     }
//                 },
//                 subtitulo: {
//                     mostrar: true,
//                     llaveTexto: 'm3v10texto1'
//                 },
// 				nombreContacto: nombreContacto,
// 				estatusLlamada: estadoLLamada
//             }

//         }
//     }

// 	configurarPerfilSeleccionado() {
// 		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
// 	}

// 	configurarParams() {
// 		this.params = this.llamadaFirebaseService.params
		
// 		if (
// 			!this.params ||
// 			(this.params && !this.params.id) ||
// 			(this.params && !this.params.tipo) ||
// 			(this.params && !this.params.accion) ||
// 			(this.params && typeof this.params.host === undefined)
// 		) {
// 			this.params = { estado: false }
// 			return
// 		}

// 		this.params.estado = true
// 		this.llamadaFirebaseService.configurarParams(this.params)
// 	}

// 	configurarOpcionesLlamada() {
// 		this.opciones = {
// 			nombreContacto: 'm4v2texto5',
// 			urlMedia: 'https://d3ubht94yroq8c.cloudfront.net/general/db911358-0a34-4294-b803-9a1dee5e2a66.jpg',
// 			forzarImagen: true,
// 			estadoLLamada: 'CONECTANDO',
// 			llamadaMuteada: true,
// 			videoMuteado: true,
// 			mostrarError: false,
// 			mensajeError: ''
// 		}
// 	}

// 	async configurarInformacionDeLaLlamada() {
// 		try {
// 			this.llamada = this.llamadaFirebaseService.crearObjetoDeLlamada(
// 				(this.params.host) ? this.params.id : '',
// 				this.params.tipo,
// 				this.perfilSeleccionado._id
// 			)

// 			if (!this.params.host) {
// 				this.llamada = await this.llamadaFirebaseService.obtenerInformacionDeLaLLamada(this.params.id)
// 				this.canalActivo = {
// 					idCanal: this.llamada.idCanal,
// 					asociacion: this.llamada.asociacion,
// 					canal: this.llamada.canal,
// 				}
// 				this.notificarCambios()
// 			}

// 			console.error('+++++++++++++++++++++')
// 			console.warn(this.llamada)
// 			console.error('+++++++++++++++++++++')
// 			this.actualizarEstatusDeLaLLamada()
// 			this.configurarInformacionDeLaAsociacion()
// 		} catch (error) {
// 			console.error('Error al configurar la info de la llamada', error)
// 		}
// 	}

// 	actualizarEstatusDeLaLLamada() {
// 		this.opciones.estadoLLamada = this.llamadaFirebaseService.actualizarEstatusDeLaLLamada(this.llamada)
// 		this.configurarAppBar()
// 	}

// 	async configurarInformacionDeLaAsociacion() {
// 		try {
// 			console.error('[[[[[[[[[[[[[[[')
// 			console.warn(this.llamada)
// 			console.error('[[[[[[[[[[[[[[[')
// 			this.asociacion = await this.participanteAsoNegocio.obtenerParticipantesAsociacion(
// 				(this.params.host) ? this.params.id : this.llamada.asociacion,
// 				this.perfilSeleccionado._id,
// 			).toPromise()

// 			if (!this.asociacion) {
// 				throw new Error('No se pudo obtener la informacion de la asociacion');
// 			}

// 			const otroPerfil = this.llamadaFirebaseService.obtenerParticipantesDeAsociacion(
// 				this.perfilSeleccionado._id,
// 				this.asociacion,
// 				false
// 			).perfil

// 			if(!otroPerfil) {
// 				throw new Error('No de pudo obtener el otro perfil')
// 			}

// 			const album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
// 				CodigosCatalogoTipoAlbum.PERFIL,
// 				otroPerfil.album
// 			)

// 			this.opciones.nombreContacto = otroPerfil.nombreContacto
// 			this.opciones.urlMedia = this.albumNegocio.obtenerUrlMedaDeLaPortada(album)
// 			this.configurarAppBar()

// 			this.llamada.receptor = otroPerfil._id

// 			this.ejecutarSegunAccionDeLaLlamada()
// 		} catch (error) {
// 			this.agoraService.escuchaErrorEnLlamada$.next(error)
// 		}
// 	}

// 	async ejecutarSegunAccionDeLaLlamada() {
// 		try {
// 			if (this.params.accion === CodigoCatalogoAccionLlamada.LLAMAR) {
// 				this.configurarCanales()
// 				this.llamar()
// 				return
// 			}

// 			if (this.params.accion === CodigoCatalogoAccionLlamada.CONTESTAR) {
// 				this.agoraService.start(
// 					'agora_local',
// 					this.canalActivo,
// 					(this.params.tipo === CodigoCatalogoTipoLlamada.VIDEO),
// 					false
// 				)
// 				this.llamadaFirebaseService.mostrarNotificacion(this.perfilSeleccionado._id, this.asociacion)
// 				this.llamadaFirebaseService.cambiarEstadoAudioNotificacion(false)
// 				return
// 			}
			
// 		} catch (error) {
// 			console.error(error)
// 		}
// 	}

// 	async configurarCanales() {
// 		try {
// 			this.canales = await this.llamadaFirebaseService.obtenerCanalesActivos()

// 			console.warn(this.canales)

// 			const canalActivo = this.llamadaFirebaseService.crearCanalActivo(
// 				this.asociacion.id,
// 				this.canales
// 			)

// 			console.warn(canalActivo)

// 			this.llamadaFirebaseService.actualizarCanalActivo(canalActivo)

// 			this.llamada.canal = canalActivo.canal
// 			this.llamada.idCanal = canalActivo.idCanal
// 			this.canalActivo = canalActivo
// 		} catch (error) {
// 			this.agoraService.escuchaErrorEnLlamada$.next(error)
// 		}
// 	}

// 	async llamar() {
// 		try {
// 			console.error('+++++++++++++++++++++++++++++')
// 			console.error('No se ha llamado el metodo main')
// 			console.error('+++++++++++++++++++++++++++++')

// 			this.llamadaFirebaseService.cambiarEstadoDelPerfil(
// 				this.perfilSeleccionado,
// 				CodigosCatalogoEstatusPerfil.PERFIL_OCUPADO
// 			)

// 			const estadoOtroParticipante = await this.llamadaFirebaseService.obtenerEstadoDelOtroParticipante(
// 				this.perfilSeleccionado._id,
// 				this.asociacion
// 			)

// 			if (!estadoOtroParticipante) {
// 				throw new Error('Error al obtener el estado del otro participante')
// 			}

// 			if (estadoOtroParticipante.estatus.codigo === CodigosCatalogoEstatusPerfil.PERFIL_OCUPADO) {
// 				throw new Error('No se puede completar, el usuario esta en otra llamada')
// 			}
				
// 			console.error('0000000000000000000000000000000')
// 			console.warn('Antes de llamar')
// 			console.error('0000000000000000000000000000000')
// 			this.agoraService.start(
// 				'agora_local',
// 				this.canalActivo,
// 				(this.params.tipo === CodigoCatalogoTipoLlamada.VIDEO)
// 			)
// 		} catch (error) {
// 			console.error(error)
// 			this.agoraService.escuchaErrorEnLlamada$.next(error)
// 		}
// 	}

// 	configurarEscuchaRegistrarLlamada() {
// 		this.agoraService.subscripcionRegistroLlamada$ = this.agoraService.escuchaRegistrarLlamada$.subscribe(
// 			async data => {
// 				if (this.params.accion === CodigoCatalogoAccionLlamada.CONTESTAR) {
// 					this.cambiarEstadoAudioLlamada(false)
// 					this.cambiarEstadoVideoLlamada(false)
// 					this.opciones.forzarImagen = false
// 					return
// 				}

// 				console.error('+++++++++++++++++++++++++++++++++')
// 				console.warn('Debes registrar la llamada en accion llamar', data)
// 				console.error('+++++++++++++++++++++++++++++++++')
// 				this.llamada.estatus = CodigosCatalogoEstatusLLamada.LLAMANDO
// 				this.llamada.busqueda = this.llamada.receptor + ' -- ' + this.llamada.estatus
// 				this.llamadaFirebaseService.registrarLlamada(this.llamada)
// 				this.llamadaFirebaseService.cambiarEstadoAudioNotificacion(false)
// 				this.notificarCambios()
// 				this.actualizarEstatusDeLaLLamada()

// 				// Empezar a contar
// 				setTimeout(() => {
// 					if (this.llamada.estatus !== CodigosCatalogoEstatusLLamada.LLAMANDO) {
// 						return
// 					}
// 					console.error('--------------------')
// 					console.warn('Se procede a cortar por time out')
// 					console.error('--------------------')
// 					this.cortarLlamada()
// 				}, 25000)
// 			}
// 		)
// 	}

// 	configurarEscuchaErrorEnLlamada() {
// 		this.agoraService.subscripcionErrorEnLlamada$ = this.agoraService.escuchaErrorEnLlamada$.subscribe(
// 			data => {
// 				console.error('Error en la llamada', data)
// 				this.opciones.mensajeError = (data) ? data : 'text37'
// 				this.opciones.mostrarError = true
// 				this.llamadaFirebaseService.cambiarEstadoAudioNotificacion(true)
// 			}
// 		)
// 	}

// 	notificarCambios() {
// 		console.error('llamada en ntoificar', this.llamada)
// 		if (
// 			!this.llamada ||
// 			this.opciones.mostrarError
// 		) {
// 			this.desconectar()
// 			return
// 		}

// 		this.escucharCambiosEnLaLlamada()
// 		this.llamadaFirebaseService.llamadaActiva.ejecutar$.next(this.llamada.id)
// 	}

// 	escucharCambiosEnLaLlamada() {
// 		this.llamadaFirebaseService.llamadaActiva.subscripcion$ = this.llamadaFirebaseService.llamadaActiva.respuesta$.subscribe(
// 			data => {
// 				data.forEach(info => {
// 					console.warn(info.key)
// 					console.warn(info.payload.val())
// 					if (info.key === 'estatus') {
// 						this.llamada.estatus = info.payload.val()
// 					}

// 					if (info.key === 'busqueda') {
// 						this.llamada.busqueda = info.payload.val()
// 					}

// 					if (info.key === 'estado') {
// 						this.llamada.estado = info.payload.val()
// 					}
// 				})

// 				console.error('Llamada actualizada', this.llamada)
// 				this.actualizarEstatusDeLaLLamada()
				
// 				if (this.llamada.estatus === CodigosCatalogoEstatusLLamada.EN_CURSO) {
// 					this.llamadaFirebaseService.cambiarEstadoAudioNotificacion(true)
// 					this.cambiarEstadoAudioLlamada(false)
// 					this.cambiarEstadoVideoLlamada(false)
// 					this.opciones.forzarImagen = false
// 				}

// 				if (
// 					this.llamada.estatus === CodigosCatalogoEstatusLLamada.TERMINADA ||
// 					this.llamada.estatus === CodigosCatalogoEstatusLLamada.CANCELADA ||
// 					this.llamada.estatus === CodigosCatalogoEstatusLLamada.RECHAZADA
// 				) {
// 					console.warn('Me cortaron o algo, que paso')
// 					this.llamadaFirebaseService.cambiarEstadoAudioNotificacion(true)
// 					this.cortar()
// 				}
// 			},
// 			error => {
// 				console.error('Error al escuchar cambios en la llamada', error)
// 			}
// 		)
// 	}

// 	async contestarLlamada() {
// 		try {
// 			this.llamada.estatus = CodigosCatalogoEstatusLLamada.EN_CURSO
// 			const res = await this.llamadaFirebaseService.actualizarEstadoDeLaLlamada(this.llamada)

// 			if (!res) {
// 				throw new Error('Error al actualizar en firebase');
// 			}

// 			this.agoraService.join(
// 				this.canalActivo,
// 				true
// 			)
// 			this.actualizarEstatusDeLaLLamada()
// 			this.llamadaFirebaseService.cambiarEstadoAudioNotificacion(true)
// 			console.error('------------------------------------------------')
// 			console.warn('Ejecutado')
// 			console.error('------------------------------------------------')
// 			await this.llamadaFirebaseService.cambiarEstadoDelPerfil(
// 				this.perfilSeleccionado,
// 				CodigosCatalogoEstatusPerfil.PERFIL_OCUPADO
// 			)
// 		} catch (error) {
// 			this.agoraService.escuchaErrorEnLlamada$.next(error)
// 		}
// 	}

// 	async cortarLlamada() {
// 		try {
// 			if (this.llamada.estatus === CodigosCatalogoEstatusLLamada.LLAMANDO) {
// 				this.llamada.estatus = (this.params.host) ?
// 					CodigosCatalogoEstatusLLamada.CANCELADA :
// 					CodigosCatalogoEstatusLLamada.RECHAZADA
// 			}

// 			if (this.llamada.estatus === CodigosCatalogoEstatusLLamada.EN_CURSO) {
// 				this.llamada.estatus = CodigosCatalogoEstatusLLamada.TERMINADA
// 			}

// 			this.llamadaFirebaseService.actualizarCanalActivo(this.canalActivo, true)
// 			const res = await this.llamadaFirebaseService.actualizarEstadoDeLaLlamada(this.llamada)

// 			if (!res) {
// 				throw new Error('Error al actualizar en firebase');
// 			}

// 			this.cortar()
// 		} catch (error) {
// 			this.agoraService.escuchaErrorEnLlamada$.next(error)
// 		}
// 	}

// 	cortar() {
// 		this.desconectar()
// 		this.llamadaFirebaseService.cambiarEstadoDelPerfil(this.perfilSeleccionado, CodigosCatalogoEstatusPerfil.PERFIL_ACTIVO)
// 		this.actualizarEstatusDeLaLLamada()
// 		this.opciones.forzarImagen = true
// 	}

// 	accionAtras() {
// 		this._location.back()
// 	}

// 	cambiarEstadoAudioLlamada(
// 		mutear: boolean
// 	) {
// 		if (
// 			this.opciones.mostrarError ||
// 			this.llamada.estatus !== CodigosCatalogoEstatusLLamada.EN_CURSO
// 		) {
// 			return
// 		}

// 		this.opciones.llamadaMuteada = mutear
// 		this.agoraService.estadoDelAudio(this.opciones.llamadaMuteada)
// 	}

// 	cambiarEstadoVideoLlamada(
// 		mutear: boolean
// 	) {
// 		if (
// 			this.opciones.mostrarError ||
// 			this.llamada.estatus !== CodigosCatalogoEstatusLLamada.EN_CURSO ||
// 			this.llamada.tipo === CodigoCatalogoTipoLlamada.AUDIO
// 		) {
// 			return
// 		}

// 		this.opciones.videoMuteado = mutear
// 		this.agoraService.estadoDelVideo(this.opciones.videoMuteado)
// 	}

// 	desconectar() {
// 		this.agoraService.unpublish()
// 		this.agoraService.leave()
// 		this.agoraService.desconectar()
// 		this.llamadaFirebaseService.desconectarDeEscuchaLlamadas()
// 	}

// 	atras() {
// 		if (
// 			this.llamada.estatus === CodigosCatalogoEstatusLLamada.LLAMANDO ||
// 			this.llamada.estatus === CodigosCatalogoEstatusLLamada.EN_CURSO
// 		) {
// 			this.cortarLlamada()
// 		}

// 		this._location.back()
// 	}

// }

// export interface ChatLlamadaParams {
// 	estado: boolean,
// 	id?: string,
// 	tipo?: CodigoCatalogoTipoLlamada,
// 	accion?: CodigoCatalogoAccionLlamada,
// 	host?: boolean
// }

