import { ChatContactosComponent } from 'src/app/presentacion/gazing/chat-contactos/chat-contactos.component';
import { ChatGazeComponent } from 'src/app/presentacion/gazing/chat-gaze/chat-gaze.component';
import { RutasGazing } from 'src/app/presentacion/gazing/rutas-gazing.enum';
import { GazingComponent } from 'src/app/presentacion/gazing/gazing.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { ChatLlamadaComponent } from './chat-llamada/chat-llamada.component';


const routes: Routes = [
	{
		path: '',
		component: GazingComponent,
		children: [
			{
				path: RutasGazing.CHAT_GAZE.toString(),
				component: ChatGazeComponent
			},
			{
				path: RutasGazing.CHAT_CONTACTOS.toString(),
				component: ChatContactosComponent
			},
			// {
			// 	path: RutasGazing.CONFERENCIA.toString(),
			// 	component: ChatLlamadaComponent
			// }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class GazingRoutingModule {

}
