import { Location } from '@angular/common';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, SnapshotAction } from '@angular/fire/database';
import { ActivatedRoute, Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { CodigoCatalogoTipoLlamada } from '@core/servicios/generales/llamada/llamada.enum';
import { MetodosParaFotos } from '@core/servicios/generales/metodos-para-fotos.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoMedia } from '@core/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';
import { CatalogoEstatusMensaje, CatalogoTipoMensaje, EstadoMensaje } from '@core/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { CodigosCatalogoTipoArchivo } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-archivo.enum';
import { CodigoEstadoParticipanteAsociacion } from '@core/servicios/remotos/codigos-catalogos/codigo-estado-partic-aso.enum';
import { FiltroGeneral } from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import { BarraInferiorInlineComponent } from '@shared/componentes/barra-inferior-inline/barra-inferior-inline.component';
import { ColorIconoBoton, ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { ConfiguracionMensajeFirebase } from '@shared/componentes/mensaje-gaze/mensaje-gaze.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
    ColorDeBorde,
    ColorDeFondo, ColorDelTexto, ColorFondoLinea, EspesorLineaItem, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { PaddingLineaVerdeContactos } from '@shared/diseno/enums/estilos-padding-general';
import {
    TamanoColorDeFondo,
    TamanoDeTextoConInterlineado, TamanoLista
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { TipoIconoBarraInferior } from '@shared/diseno/enums/tipo-icono.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { BarraBusqueda, ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos/lista-contacto.interface';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';
import { ReproductorVideo } from '@shared/diseno/modelos/reproductor-video.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { ParticipanteAsociacionNegocio } from 'dominio/logica-negocio/participante-asociacion.negocio';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { ConversacionModel } from 'dominio/modelo/entidades/conversacion.model';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { MensajeModelFirebase } from 'dominio/modelo/entidades/mensaje.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ChatParams } from 'dominio/modelo/parametros/chat.interface';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import * as Firebase from 'firebase/app';
import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { ChatMetodosCompartidosService } from 'src/app/presentacion/gazing/chat-metodos-comunes.service';
import { RutasGazing } from 'src/app/presentacion/gazing/rutas-gazing.enum';
import { RutasNoticias } from 'src/app/presentacion/noticias/rutas-noticias.enum';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';

@Component({
    selector: 'app-chat-gaze',
    templateUrl: './chat-gaze.component.html',
    styleUrls: ['./chat-gaze.component.scss']
})
export class ChatGazeComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
    @ViewChild('toast', { static: false }) toast: ToastComponent
    @ViewChild('listaEnVista', { static: false }) listaEnVistaRef: ElementRef

    public estoySeleccionando: boolean
    public mostrarVideo: boolean

    public contador: number
    public linkDescargar: Array<string>
    public interval: any
    public mostrarReproducirVideo: boolean
    // Utils
    public asociacion: AsociacionModel
    public conversacion: ConversacionModel
    public CatalogoTipoMensajeEnum = CatalogoTipoMensaje
    public CatalogoEstatusMensajeEnum = CatalogoEstatusMensaje
    public UsoAppBarEnum = UsoAppBar
    public idListaMensajesEnVista: string
    public idMensajeBase: string
    public centrarDialogoReenvio: boolean
    public dataScrool: VariablesDelScrool
    public limitePaginacion: number

    // Parametros internos
    public params: ChatParams
    public perfilSeleccionado: PerfilModel
    public mostrarCargando: boolean
    public mostrarCargandoPequeno: boolean
    public mostrarError: boolean
    public contenidoError: string
    public fechaDeBusqueda: Date
    public buscarContacto: Function

    // Configuracion hijos
    public confAppbar: ConfiguracionAppbarCompartida
    public confBarraInferior: ConfiguracionBarraInferiorInline
    public confToast: ConfiguracionToast
    public confItemOtroUsuario: ConfiguracionItemListaContactosCompartido
    public confDialogoCompartido: DialogoCompartido
    public confListaContactoCompartido: ConfiguracionListaContactoCompartido
    public confImagenPantallaCompleta: ConfiguracionImagenPantallaCompleta

    public primeraConsulta: boolean
    public paraReenviar: boolean
    public mensajesObservador: any
    public mensajesObservadorDeCambios: any
    public listaMensajes: Array<MensajeModelFirebase>
    public listaConfiguracionMensajes: Array<ConfiguracionMensajeFirebase>
    public listaDeFechasDelChat: Array<Fecha>
    public listaMensajesSeleccionados: Array<MensajeModelFirebase>
    public indicadorNotificacionesInterno: MarcadorDeNotificacion
    public listaContactos: PaginacionModel<ParticipanteAsociacionModel>
    public listaAsociacionesSeleccionados: Array<string>
    public listaContactosParaBusqueda: Array<ConfiguracionItemListaContactosCompartido>
    public confItemCirculoOtroUsuario: ItemCircularCompartido
    public confLinea: LineaCompartida
    public confDialogoEliminarMensaje: DialogoCompartido

    // data scroll
    public listaScroll: VariablesDelScrool
    public detectorDeCambiosRef: ChangeDetectorRef
    public indicadoresScroll: IndicadoresDelScrool

    //reproductor de video

    public confReprodVideo: ReproductorVideo

    public barraInferiorInline: BarraInferiorInlineComponent

    public ocultarVideo: boolean

    constructor(
        public estilosDelTextoServicio: EstiloDelTextoServicio,
        public generadorId: GeneradorId,
        private router: Router,
        private route: ActivatedRoute,
        private _location: Location,
        private perfilNegocio: PerfilNegocio,
        private participanteAsoNegocio: ParticipanteAsociacionNegocio,
        private metodosParaFotos: MetodosParaFotos,
        private albumNegocio: AlbumNegocio,
        private db: AngularFireDatabase,
        private variablesGlobales: VariablesGlobales,
        private mediaNegocio: MediaNegocio,
        private chatMetodosCompartidosService: ChatMetodosCompartidosService,
        private changeDetectionRef: ChangeDetectorRef,
        private participanteAsociacionNegocio: ParticipanteAsociacionNegocio,
        private proyectoNegocio: ProyectoNegocio,
        // private llamadaFirebaseService: LlamadaFirebaseService,
        private ngZone: NgZone
    ) {
        this.idListaMensajesEnVista = 'listaMensajesDelChat'
        this.idMensajeBase = 'mensajeBaseID'
        this.centrarDialogoReenvio = true
        this.limitePaginacion = 50
        this.params = { estado: false }
        this.mostrarCargando = false
        this.mostrarError = false
        this.contenidoError = ''
        this.fechaDeBusqueda = new Date()
        this.mostrarVideo = false
        // TEST
        this.buscarContacto = (reiniciar: boolean) => this.buscarContactoParaReenviar(reiniciar)
        this.paraReenviar = true
        this.listaMensajes = []
        this.listaConfiguracionMensajes = []
        this.listaDeFechasDelChat = []
        this.listaMensajesSeleccionados = []
        this.primeraConsulta = true
        this.listaAsociacionesSeleccionados = []
        this.listaContactosParaBusqueda = []
        this.estoySeleccionando = false

        this.ocultarVideo = false
        this.mostrarReproducirVideo = false


        // Scrool
        this.detectorDeCambiosRef = changeDetectionRef
        this.indicadoresScroll = {}

        //Videollamada
    }

    ngOnInit(): void {
        this.variablesGlobales.mostrarMundo = false
        this.configurarParametrosUrl()
        this.configurarPerfilSeleccionado()
        this.configurarAppBar()
        this.configurarBarraInferior()
        this.configurarDialogoEliminarMensaje()
        this.configurarToast()
        this.configurarOtroUsuarioPorDefecto()
        this.configurarDialogoCompartido()
        this.inicializarDataDelScrool()
        this.configurarIndicadorDeNotificaciones()
        this.configurarListaContactosPaginacion()
        this.configurarListaContactoCompartido()
        this.configurarImagenPantallaCompleta()
        if (this.params.estado && this.perfilSeleccionado) {
            this.configurarDataDelOtroUsuario()
        } else {
            this.perfilNegocio.validarEstadoDelPerfil(
                this.perfilSeleccionado,
                this.params.estado
            )
        }
    }

    ngAfterViewInit(): void {

    }

    ngAfterViewChecked(): void {
        this.chatMetodosCompartidosService.validarScroolAlFinal(
            this.dataScrool,
            this.idListaMensajesEnVista
        )
    }

    ngOnDestroy(): void {
        if (this.mensajesObservador) {
            this.mensajesObservador.unsubscribe()
        }
    }

    configurarParametrosUrl() {
        const { id } = this.route.snapshot.params
        if (id) {
            this.params.id = id
            this.params.estado = true
        }
    }

    configurarPerfilSeleccionado() {
        this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
    }

    async configurarDialogoEliminarMensaje() {
        this.confDialogoEliminarMensaje = {
            mostrarDialogo: false,
            descripcion: 'm3v11texto7',
            tipo: TipoDialogo.CONFIRMACION,
            completo: true,
            listaAcciones: [
                {
                    text: 'm2v13texto9',
                    tipoBoton: TipoBoton.TEXTO,
                    colorTexto: ColorTextoBoton.ROJO,
                    tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
                    enProgreso: false,
                    ejecutar: () => {
                        this.confDialogoEliminarMensaje.mostrarDialogo = false
                        this.eliminarMensajes()
                    }
                },
                {
                    text: 'm2v13texto10',
                    tipoBoton: TipoBoton.TEXTO,
                    colorTexto: ColorTextoBoton.AMARRILLO,
                    tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
                    enProgreso: false,
                    ejecutar: () => {
                        this.confDialogoEliminarMensaje.mostrarDialogo = false
                    }
                }
            ]
        }
    }

    configurarAppBar(
        buscador: BarraBusqueda = null,
        usoAppBar: UsoAppBar = UsoAppBar.USO_SEARCHBAR_APPBAR_MINI
    ) {
        this.confAppbar = {
            accionAtras: () => this.accionAtrasAppBar(),
            usoAppBar: usoAppBar,
            searchBarAppBar: {
                mostrarDivBack: {
                    icono: true,
                    texto: true
                },
                mostrarLineaVerde: true,
                mostrarTextoHome: true,
                mostrarBotonXRoja: false,
                tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
                nombrePerfil: {
                    mostrar: true,
                    llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre
                },
                buscador: (buscador && buscador !== null) ? buscador : {
                    mostrar: false,
                    configuracion: {
                        disable: true,
                    }
                },
                subtitulo: {
                    mostrar: true,
                    llaveTexto: (buscador) ? 'm3v12texto2' : 'm3v10texto1'
                },

            }

        }
    }


    async calcularTiempoAudio(archivo: any) {
        let duration: number;
        var video = document.createElement('audio');
        video.src = URL.createObjectURL(archivo);
        video.preload = 'metadata';

        video.onloadedmetadata = async function () {
            window.URL.revokeObjectURL(video.src);
            duration = video.duration;
            return duration
        }

    }

    getDuration(file): Promise<number> {
        return new Promise((res) => {
            let duracion: number = 0
            var objectURL = URL.createObjectURL(file);
            var audio = new Audio(objectURL);

            audio.onloadedmetadata = function () {
                duracion = audio.duration
                res(duracion)
            }
        })
    }
    async configurarBarraInferior() {
        this.confBarraInferior = {
            desactivarBarra: this.mostrarCargando && this.mostrarError,
            capaColorFondo: {
                mostrar: true,
                anchoCapa: TamanoColorDeFondo.TAMANO100,
                colorDeFondo: ColorDeFondo.FONDO_TRANSPARENCIA_BASE
            },
            estatusError: (error: string) => {

            },

            iconoTexto: {
                icono: {
                    mostrar: true,
                    tipo: TipoIconoBarraInferior.ICONO_TEXTO,
                    eventoTap: (dataApiArchivo: SubirArchivoData) => {
                        this.enviarMensaje(
                            dataApiArchivo,
                            CatalogoTipoMensaje.TEXTO
                        )
                    }
                },
                capa: {
                    mostrar: false
                }
            },
            iconoArchivo: {
                icono: {
                    mostrar: true,
                    tipo: TipoIconoBarraInferior.ICONO_ARCHIVO,
                    eventoTap: async (dataApiArchivo: SubirArchivoData) => {
                        let catalogoMensaje: CatalogoTipoMensaje
                        if (dataApiArchivo.archivo[0]['size'] < this.variablesGlobales.tamanoMaximoArchivos) {
                            if (TIPOS_VIDEO[dataApiArchivo.archivo[0]['type']]) {
                                dataApiArchivo.catalogoMedia = CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO
                                dataApiArchivo.archivo = dataApiArchivo.archivo[0]
                                catalogoMensaje = CatalogoTipoMensaje.ARCHIVO
                                dataApiArchivo.nombreDelArchivo = dataApiArchivo.archivo.name

                            } else if (TIPOS_AUDIO[dataApiArchivo.archivo[0]['type']]) {
                                catalogoMensaje = CatalogoTipoMensaje.AUDIO
                                dataApiArchivo.archivo = dataApiArchivo.archivo[0]
                                dataApiArchivo.catalogoMedia = CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE
                                dataApiArchivo.descripcion = ''
                                dataApiArchivo.formato = dataApiArchivo.archivo.type
                                dataApiArchivo.nombreDelArchivo = dataApiArchivo.archivo.name

                                let duracionAudio = await this.getDuration(dataApiArchivo.archivo)


                                dataApiArchivo.duracion = duracionAudio.toFixed(2).toString()

                                if (dataApiArchivo.archivo.type === "audio/mpeg") {
                                    dataApiArchivo.formato = 'audio/mp3'
                                } else {
                                    dataApiArchivo.formato = dataApiArchivo.archivo.type
                                }
                            } else if (TIPOS_IMAGEN[dataApiArchivo.archivo[0]['type']]) {
                                if (!dataApiArchivo.nombreDelArchivo) {
                                    dataApiArchivo.archivo = dataApiArchivo.archivo[0]
                                    dataApiArchivo.nombreDelArchivo = dataApiArchivo.archivo.name
                                }
                                dataApiArchivo.descripcion = ''
                                dataApiArchivo.catalogoMedia = CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO
                                dataApiArchivo.relacionAspecto = '4:3'

                                dataApiArchivo.estado = 'EST_24'
                                catalogoMensaje = CatalogoTipoMensaje.IMAGEN

                            } else {
                                dataApiArchivo.catalogoMedia = CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE
                                dataApiArchivo.archivo = dataApiArchivo.archivo[0]
                                catalogoMensaje = CatalogoTipoMensaje.ARCHIVO
                                dataApiArchivo.nombreDelArchivo = dataApiArchivo.archivo.name
                            }
                            dataApiArchivo.estado = 'EST_24'
                            this.enviarMensaje(
                                dataApiArchivo,
                                catalogoMensaje
                            )
                        } else {
                            this.toast.abrirToast('text52')
                        }
                    }
                },
                capa: {
                    mostrar: false
                }
            },
            iconoCamara: {
                icono: {
                    mostrar: true,
                    tipo: TipoIconoBarraInferior.ICONO_CAMARA,
                    eventoTap: (dataApiArchivo: SubirArchivoData) => {

                        if (!dataApiArchivo.nombreDelArchivo) {
                            dataApiArchivo.archivo = dataApiArchivo.archivo[0]
                            dataApiArchivo.nombreDelArchivo = dataApiArchivo.archivo.name
                        }
                        dataApiArchivo.descripcion = ''
                        dataApiArchivo.catalogoMedia = CodigosCatalogoTipoMedia.TIPO_MEDIA_COMPUESTO
                        dataApiArchivo.relacionAspecto = '1:1'

                        dataApiArchivo.estado = 'EST_24'

                        this.enviarMensaje(
                            dataApiArchivo,
                            CatalogoTipoMensaje.IMAGEN
                        )
                    }
                },
                capa: {
                    mostrar: false
                }
            },
            iconoLlamada: {
                icono: {
                    mostrar: false,
                    tipo: TipoIconoBarraInferior.ICONO_LLAMADA,
                    eventoTap: () => this.eventoIcono(CodigoCatalogoTipoLlamada.AUDIO)
                },
                capa: {
                    mostrar: false
                }
            },
            iconoVideoLlamada: {
                icono: {
                    mostrar: false,
                    tipo: TipoIconoBarraInferior.ICONO_VIDEO_LLAMADA,
                    eventoTap: () => this.eventoIcono(CodigoCatalogoTipoLlamada.VIDEO)
                },
                capa: {
                    mostrar: false
                }
            },
            iconoAudio: {
                icono: {
                    mostrar: true,
                    tipo: TipoIconoBarraInferior.ICONO_AUDIO,
                    eventoTap: (dataApiArchivo: SubirArchivoData) => {
                        dataApiArchivo.nombreDelArchivo = new Date().getTime().toString() + '.' + dataApiArchivo.formato.split('/')[1]
                        dataApiArchivo.estado = 'EST_24'
                        this.enviarMensaje(
                            dataApiArchivo,
                            CatalogoTipoMensaje.AUDIO
                        )
                    },
                },
                capa: {
                    siempreActiva: false,
                    grabadora: {
                        noPedirTituloGrabacion: true,
                        usarLoader: false,
                        grabando: false,
                        duracionActual: 0,
                        tiempoMaximo: 300,
                        factorAumentoLinea: 100 / 300, // 100% del ancho divido para tiempoMaximo
                    },
                    clickParar: true
                }
            },
            botonSend: {
                mostrar: true,
                evento: (dataApiArchivo: SubirArchivoData) => {
                    this.enviarMensaje(
                        dataApiArchivo,
                        CatalogoTipoMensaje.TEXTO
                    )
                }
            }
        }
    }

    configurarToast() {
        this.confToast = {
            cerrarClickOutside: false,
            mostrarLoader: false,
            mostrarToast: false,
            texto: ""
        }
    }

    reintentar() {

    }

    configurarOtroUsuarioPorDefecto() {
        this.confItemOtroUsuario = {
            id: this.generadorId.generarIdConSemilla(),
            usoItem: UsoItemListaContacto.USO_CONTACTO,
            contacto: {
                nombreContacto: '',
                nombre: '',
                estilosTextoSuperior: {
                    color: ColorDelTexto.TEXTOAZULBASE,
                    estiloTexto: EstilosDelTexto.BOLD,
                    enMayusculas: true,
                    tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
                },
                estilosTextoInferior: {
                    color: ColorDelTexto.TEXTONEGRO,
                    estiloTexto: EstilosDelTexto.REGULAR,
                    enMayusculas: true,
                    tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1
                },
                idPerfil: ''

            },
            configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
                '',
                ColorDeBorde.BORDER_ROJO,
                this.metodosParaFotos.obtenerColorFondoAleatorio(),
                false,
                UsoItemCircular.CIRCARITACONTACTODEFECTO,
                true
            ),
            mostrarX: {
                mostrar: false,
                color: true
            },
            configuracionLineaVerde: this.metodosParaFotos.configurarLineaVerde(
                PaddingLineaVerdeContactos.PADDING_1542_267
            )
        }

        this.confItemCirculoOtroUsuario = this.metodosParaFotos.configurarItemCircular(
            '',
            ColorDeBorde.BORDER_ROJO,
            this.metodosParaFotos.obtenerColorFondoAleatorio(),
            false,
            UsoItemCircular.CIRPERFILCARITACHAT,
            true
        )

        this.confLinea = {
            ancho: AnchoLineaItem.ANCHO6954,
            espesor: EspesorLineaItem.ESPESOR041,
            colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
            forzarAlFinal: false,
        }

    }

    configurarDialogoCompartido() {
        this.confDialogoCompartido = {
            mostrarDialogo: true,
            mostrarDialogoLibre: true,
            tipo: TipoDialogo.MULTIPLE_ACCION_HORIZONTAL_INFORMACION,
            completo: false,
            accionesDialogoHorizontal: [
                {
                    accion: {
                        ejecutar: () => {
                            this.configurarListaContactosPaginacion()
                            this.obtenerContactos(true)
                        },
                        enProgreso: false,
                        tipoBoton: TipoBoton.ICON_COLOR,
                        colorIcono: ColorIconoBoton.ROJO,

                    },

                    descripcion: 'm3v11texto2',
                    estilosTexto: {
                        color: ColorDelTexto.TEXTOROJOBASE,
                        estiloTexto: EstilosDelTexto.BOLD,
                        enMayusculas: true,
                        tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1
                    }
                },
                {
                    accion: {
                        ejecutar: () => {
                            // this.eliminarMensajes()
                            this.confDialogoEliminarMensaje.mostrarDialogo = true
                        },
                        enProgreso: false,
                        tipoBoton: TipoBoton.ICON_COLOR,
                        colorIcono: ColorIconoBoton.AZUL
                    },
                    descripcion: 'm3v11texto3',
                    estilosTexto: {
                        color: ColorDelTexto.TEXTOAZULBASE,
                        estiloTexto: EstilosDelTexto.BOLD,
                        enMayusculas: true,
                        tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1
                    }
                },
                {
                    accion: {
                        ejecutar: () => {
                            this.descargarMensajes()
                        },
                        enProgreso: false,
                        tipoBoton: TipoBoton.ICON_COLOR,
                        colorIcono: ColorIconoBoton.AMARILLO
                    },
                    descripcion: 'm3v11texto3.1',
                    estilosTexto: {
                        color: ColorDelTexto.TEXTOAMARILLOBASE,
                        estiloTexto: EstilosDelTexto.BOLD,
                        enMayusculas: true,
                        tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_I1
                    }
                }
            ],
        }
    }

    inicializarDataDelScrool() {
        this.dataScrool = {
            scroolAlFinal: true,
            puedeCargarMas: true,
            mostrarLoaderPaginacion: false,
        }
    }

    configurarIndicadorDeNotificaciones() {
        this.indicadorNotificacionesInterno = {
            mostrar: false,
            idLeidos: [],
            idPorLeer: [],
            idEliminados: []
        }
    }

   

    configurarListaContactosPaginacion() {
        this.listaContactos = {
            anteriorPagina: false,
            paginaActual: 1,
            proximaPagina: true,
            totalDatos: 0,
            totalPaginas: 0,
            lista: [],
        }
    }

    configurarListaContactoCompartido() {
        this.confListaContactoCompartido = {
            llaveSubtitulo: 'm3v12texto4',
            listaContactos: {
                lista: [],
                cargarMas: () => {


                    if (this.listaContactos.proximaPagina) {
                        this.listaContactos.paginaActual += 1
                        this.obtenerContactos()
                    }
                },
                reintentar: () => {
                    this.configurarListaContactosPaginacion()
                    this.obtenerContactos()
                },
                cargando: false,
                tamanoLista: TamanoLista.LISTA_CONTACTOS
            },
            botonAccion: {
                mostrarDialogo: false,
                mostrarDialogoLibre: true,
                tipo: TipoDialogo.MULTIPLE_ACCION_HORIZONTAL_INFORMACION,
                completo: false,
                accionesDialogoHorizontal: [
                    {
                        accion: {
                            ejecutar: () => {
                                this.reenviarMensajes()
                            },
                            enProgreso: false,
                            tipoBoton: TipoBoton.ICON_COLOR,
                            colorIcono: ColorIconoBoton.ROJO,
                        },
                        descripcion: 'm3v12texto5',
                        estilosTexto: {
                            color: ColorDelTexto.TEXTOROJOBASE,
                            estiloTexto: EstilosDelTexto.BOLD,
                            enMayusculas: true,
                            tamanoConInterlineado: TamanoDeTextoConInterlineado.L3_IGUAL
                        }
                    },
                ],
            },
            contactoSeleccionado: (idAsociacion: string) => {

                const posEnSeleccionado: number = this.listaAsociacionesSeleccionados.findIndex(e => e === idAsociacion)
                const posEnListaContactos: number = this.confListaContactoCompartido.listaContactos.lista.findIndex(e => e.id === idAsociacion)
                const contacto = this.confListaContactoCompartido.listaContactos.lista[posEnListaContactos]

                if (posEnSeleccionado >= 0) {
                    this.listaAsociacionesSeleccionados.splice(posEnSeleccionado, 1)
                    if (posEnListaContactos >= 0) {
                        contacto.configCirculoFoto.mostrarCorazon = false
                        contacto.configCirculoFoto.colorBorde = ColorDeBorde.BORDER_ROJO
                        this.confListaContactoCompartido.listaContactos.lista[posEnListaContactos] = contacto
                    }

                    this.confListaContactoCompartido.botonAccion.mostrarDialogo = (this.listaAsociacionesSeleccionados.length > 0)
                    return
                }

                if (posEnSeleccionado < 0) {
                    this.listaAsociacionesSeleccionados.push(idAsociacion)

                    if (posEnListaContactos >= 0) {
                        contacto.configCirculoFoto.mostrarCorazon = true
                        contacto.configCirculoFoto.colorBorde = ColorDeBorde.BORDER_AZUL
                        this.confListaContactoCompartido.listaContactos.lista[posEnListaContactos] = contacto
                    }

                    this.confListaContactoCompartido.botonAccion.mostrarDialogo = (this.listaAsociacionesSeleccionados.length > 0)
                    return
                }
            }
        }
    }

    async configurarDataDelOtroUsuario() {
        try {
            const asociacion: AsociacionModel = await this.participanteAsoNegocio.obtenerParticipantesAsociacion(
                this.params.id,
                this.perfilSeleccionado._id,
            ).toPromise()

            this.asociacion = asociacion

            if (!asociacion || !asociacion.participantes) {
                throw new Error('')
            }

            const index = asociacion.participantes.findIndex(e => (e && e.perfil && e.perfil._id !== this.perfilSeleccionado._id))

            if (index < 0) {
                throw new Error('')
            }

            const otroUsuario: ParticipanteAsociacionModel = asociacion.participantes[index]

            if (!otroUsuario.perfil) {
                throw new Error('')
            }



            this.confItemCirculoOtroUsuario = this.metodosParaFotos.configurarItemCircularParaChat(otroUsuario.perfil)

            this.confItemOtroUsuario = this.metodosParaFotos.configurarItemListaContacto(
                otroUsuario.perfil,
                this.asociacion
            )

            // Listas
            this.configurarConversacion()
            this.configurarListaDeMensajesAnadidos()
        } catch (error) {
            this.configurarOtroUsuarioPorDefecto()
        }
    }

    async configurarConversacion() {
        try {
            const dataFirebase = await this.db.database.ref('conversaciones/' + this.params.id).get()

            if (!dataFirebase.exists()) {
                this.conversacion = this.chatMetodosCompartidosService.crearObjetoDeConversacion(this.asociacion)

                const res = await this.db.database.ref('conversaciones/' + this.params.id).set(this.conversacion)
                return
            }

            this.conversacion = dataFirebase.val() as ConversacionModel

        } catch (error) {
        }
    }

    async configurarListaDeMensajesAnadidos() {
        this.mensajesObservador = this.db.list(
            'mensajes/' + this.params.id,
            ref => ref.orderByChild('fechaCreacion').limitToLast(this.limitePaginacion)
        ).snapshotChanges(['child_added', 'child_changed']).subscribe(actions => {

            this.insertarTodosLosMensajesEnListas(actions)

            this.indicadorNotificacionesInterno.mostrar = this.chatMetodosCompartidosService.validarScroolAlFinalParaNotificaciones(
                this.idListaMensajesEnVista
            )

            if (this.primeraConsulta) {
                this.primeraConsulta = false
                this.actualizarMensajesNoLeidos()
            }
        })
    }

    cargarMasMensajes(
        limite: number,
        idMensaje: string
    ) {
        const observer = this.mensajesObservador = this.db.list(
            'mensajes/' + this.params.id,
            ref => ref.orderByChild('fechaCreacion').limitToLast(limite).endAt(idMensaje))
            .snapshotChanges(['child_added']).subscribe(actions => {

                this.insertarTodosLosMensajesEnListas(actions, false)
                this.detectorDeCambiosRef.detectChanges()

                this.indicadoresScroll = this.chatMetodosCompartidosService.obtenerDataScroll(
                    this.idListaMensajesEnVista,
                    this.indicadoresScroll,
                    false
                )

                this.chatMetodosCompartidosService.establecerPosicionDelScrollTop(
                    (this.indicadoresScroll.despues.height - this.indicadoresScroll.antes.height),
                    this.idListaMensajesEnVista
                )

                this.dataScrool.puedeCargarMas = true

                observer.unsubscribe()
            })
    }

    configurarImagenPantallaCompleta(
        mostrar: boolean = false,
        mostrarLoader: boolean = true,
        urlMedia: string = ''
    ) {
        this.confImagenPantallaCompleta = {
            mostrar: mostrar,
            mostrarLoader: mostrarLoader,
            urlMedia: urlMedia,
            eventoBotonCerrar: () => {
                this.confImagenPantallaCompleta.mostrar = false
                this.confImagenPantallaCompleta.mostrarLoader = true
                this.confImagenPantallaCompleta.urlMedia = ''
            },
            eventoBotonDescargar: () => {
                
                // let link = document.createElement('a');2
                // const nombre = mensaje.adjuntos[0].principal.filename.toString()
                // link.href = this.confImagenPantallaCompleta.urlMedia;
                // link.download = nombre;
                // link.dispatchEvent(new MouseEvent('click'));

                let link = document.createElement('a');
				link.href = this.confImagenPantallaCompleta.urlMedia;
				link.target = '_blank'
				link.dispatchEvent(new MouseEvent('click', {
					view: window,
					bubbles: false,
					cancelable: true
				}));



            }
        }
    }


    eventoTapMensajesGeneral() {


        if (
            this.listaMensajesSeleccionados.length > 0 ||
            this.confListaContactoCompartido.mostrar === true
        ) {
            // 
            this.listaMensajesSeleccionados = []
            this.cambiarEstadoSeleccionadoEnTodosLosItemsDeListaMensajes()

            this.configurarAppBar()
            this.configurarListaContactosPaginacion()
            this.confListaContactoCompartido.mostrar = false
            return
        }
        if (this.listaMensajesSeleccionados.length === 0) {
            this.estoySeleccionando = false
        }


    }
    eventoTapEnMensaje(mensaje: MensajeModelFirebase) {


        if (this.estoySeleccionando) {
            return
        }

        if (
            this.listaMensajesSeleccionados.length > 0 ||
            this.confListaContactoCompartido.mostrar === true
        ) {
            this.listaMensajesSeleccionados = []
            this.cambiarEstadoSeleccionadoEnTodosLosItemsDeListaMensajes()

            this.configurarAppBar()
            this.configurarListaContactosPaginacion()
            this.confListaContactoCompartido.mostrar = false
            return
        }

        switch (mensaje.tipo.codigo as CatalogoTipoMensaje) {
            case CatalogoTipoMensaje.IMAGEN:
                const urlMedia = this.chatMetodosCompartidosService.obtenerUrlMediaDeAdjuntos(mensaje)
                this.configurarImagenPantallaCompleta(
                    (urlMedia && urlMedia.length > 0),
                    (urlMedia && urlMedia.length > 0),
                    urlMedia
                )
                break
            case CatalogoTipoMensaje.ARCHIVO:

                const urlArchivo = this.chatMetodosCompartidosService.obtenerUrlMediaDeAdjuntos(mensaje)
                const fileName = this.chatMetodosCompartidosService.obtenerFilenameMediaDeAdjuntos(mensaje)

                if (mensaje.adjuntos[0].principal.tipo.codigo === CodigosCatalogoTipoArchivo.FILES) {

                    if (urlArchivo && urlArchivo.length > 0) {
                        let link = document.createElement('a');
                        const nombre = fileName
                        link.href = urlArchivo;
                        link.download = nombre;
                        link.target = '_blank'
                        link.dispatchEvent(new MouseEvent('click', {
                            view: window,
                            bubbles: false,
                            cancelable: true
                        }));
                        // window.open(urlArchivo, '_blank')

                        // // window.location.href = urlArchivo;
                        // // var file = new Blob([data], { type: 'application/pdf' });
                        // window.location.href = urlArchivo;
                        // let link = document.createElement('a');
                        // const nombre = filrName
                        // link.href = urlArchivo;
                        // link.download = nombre;
                        // link.target = '_blank'
                        // link.dispatchEvent(new MouseEvent('click', {
                        //     view: window,
                        //     bubbles: false,
                        //     cancelable: true
                        // }));
                    }
                } else if (mensaje.adjuntos[0].principal.tipo.codigo === CodigosCatalogoTipoArchivo.VIDEO) {
                    this.confReprodVideo = {
                        url: urlArchivo,
                        mostrar: false
                    }
                    this.mostrarReproducirVideo = true
                }
                break
            case CatalogoTipoMensaje.AUDIO:
                this.reproducirAudio(mensaje)
                break
            case CatalogoTipoMensaje.COMPARTIR_PROYECTO:
                this.irAProyecto(mensaje)
                break
            case CatalogoTipoMensaje.TRANSFERIR_PROYECTO:
                this.irAProyecto(mensaje)
                break
            case CatalogoTipoMensaje.COMPARTIR_NOTICIA:
                this.irANoticia(mensaje)
                break
            case CatalogoTipoMensaje.COMPARTIR_CONTACTO: break
            default: break;
        }
    }

    irAProyecto(
        mensaje: MensajeModelFirebase
    ) {
        const proyecto: ProyectoModel = mensaje.referenciaEntidadCompartida as ProyectoModel
        if (
            !proyecto ||
            !proyecto.id ||
            !proyecto.perfil ||
            !proyecto.perfil._id
        ) {
            return
        }

        // Propietario
        if (this.perfilSeleccionado._id !== proyecto.perfil._id) {
            const modulo = RutasLocales.MODULO_PROYECTOS.toString()
            let ruta = RutasProyectos.VISITAR.toString()
            ruta = ruta.replace(':id', proyecto.id)
            this.router.navigateByUrl(modulo + '/' + ruta)
            return
        }

        const modulo = RutasLocales.MODULO_PROYECTOS.toString()
        let ruta = RutasProyectos.ACTUALIZAR.toString()
        ruta = ruta.replace(':id', proyecto.id)
        this.router.navigateByUrl(modulo + '/' + ruta)
    }

    irANoticia(
        mensaje: MensajeModelFirebase
    ) {
        const noticia: NoticiaModel = mensaje.referenciaEntidadCompartida as NoticiaModel
        if (
            !noticia ||
            !noticia.id ||
            !noticia.perfil ||
            !noticia.perfil._id
        ) {
            return
        }

        // Propietario
        if (this.perfilSeleccionado._id !== noticia.perfil._id) {
            const modulo = RutasLocales.MODULO_NOTICIAS.toString()
            let ruta = RutasNoticias.VISITAR.toString()
            ruta = ruta.replace(':id', noticia.id)
            this.router.navigateByUrl(modulo + '/' + ruta)
            return
        }

        const modulo = RutasLocales.MODULO_NOTICIAS.toString()
        let ruta = RutasNoticias.ACTUALIZAR.toString()
        ruta = ruta.replace(':id', noticia.id)
        this.router.navigateByUrl(modulo + '/' + ruta)
    }

    reproducirAudio(
        mensaje: MensajeModelFirebase
    ) {

    }

    eventoDobleTapEnMensaje(mensaje: MensajeModelFirebase) {



        // if ((mensaje.tipo.codigo === CatalogoTipoMensaje.TRANSFERIR_PROYECTO) ||
        //     (mensaje.tipo.codigo === CatalogoTipoMensaje.COMPARTIR_PROYECTO) ||
        //     (mensaje.tipo.codigo === CatalogoTipoMensaje.COMPARTIR_CONTACTO) ||
        //     (mensaje.tipo.codigo === CatalogoTipoMensaje.COMPARTIR_NOTICIA)
        // ) {
        //     return
        // }

        // if ((mensaje.tipo.codigo === CatalogoTipoMensaje.TRANSFERIR_PROYECTO)) {
        //     return
        // }

        this.seleccionarMensaje({
            ...mensaje
        })
    }

    seleccionarMensaje(mensaje: MensajeModelFirebase) {
        const index = this.listaMensajesSeleccionados.findIndex(e => e.id === mensaje.id)

        if (index < 0) {
            this.listaMensajesSeleccionados.push(mensaje)
            this.cambiarEstadoSeleccionadoEnListaMensajes(mensaje, true)
            return
        }

        if (index >= 0) {
            this.listaMensajesSeleccionados.splice(index, 1)
            this.cambiarEstadoSeleccionadoEnListaMensajes(mensaje)
        }
    }

    cambiarEstadoSeleccionadoEnListaMensajes(
        mensaje: MensajeModelFirebase,
        seleccionado: boolean = false
    ) {
        this.listaDeFechasDelChat.forEach(grupo => {
            const indexDos = grupo.mensajes.findIndex(m => m.mensaje.id === mensaje.id)
            if (indexDos >= 0) {
                grupo.mensajes[indexDos].seleccionado = seleccionado
            }

            grupo.mensajes.forEach(a => {
                a.capaEncima = (this.listaMensajesSeleccionados.length > 0)
            })
        })
    }

    cambiarEstadoSeleccionadoEnTodosLosItemsDeListaMensajes() {
        this.listaDeFechasDelChat.forEach(grupo => {
            grupo.mensajes.forEach(mensaje => {
                mensaje.seleccionado = false
            })
        })
    }

    async buscarMensajesPorFecha() {

    }

    enviarMensaje(
        dataApiArchivo: SubirArchivoData,
        tipoMensaje: CatalogoTipoMensaje
    ) {
        const propietario: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion()
        const otroParticipante: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion(false)
        const idMensaje = this.db.database.ref('mensajes/' + this.params.id).push().key

        const conAdjuntos = (tipoMensaje !== CatalogoTipoMensaje.TEXTO)

        this.enviarMensajeAccion(
            dataApiArchivo,
            propietario,
            otroParticipante,
            tipoMensaje,
            idMensaje,
            conAdjuntos
        )
    }

    obtenerParticipantesDeAsociacion(
        propietario: boolean = true
    ): ParticipanteAsociacionModel {
        const index = this.asociacion.participantes.findIndex(e => (
            e &&
            e.perfil &&
            (
                (propietario && e.perfil._id === this.perfilSeleccionado._id) ||
                (!propietario && e.perfil._id !== this.perfilSeleccionado._id)
            )
        ))

        if (index >= 0) {
            return this.asociacion.participantes[index]
        } else {
            return null
        }
    }

    verMensajesNoLeidos() {
        const lista = document.getElementById(this.idListaMensajesEnVista) as HTMLElement

        if (!lista) {
            return
        }

        if (this.indicadorNotificacionesInterno.idPorLeer.length === 0) {
            lista.scrollTop = lista.scrollHeight + lista.offsetHeight
            return
        }

        const elemento = document.getElementById(
            this.idMensajeBase +
            this.indicadorNotificacionesInterno.idPorLeer[0].id
        ) as HTMLElement

        if (!elemento) {
            return
        }

        lista.scrollTop = elemento.offsetTop
        this.actualizarMensajesNoLeidos()

    }

    async actualizarMensajesNoLeidos() {
        const aux = {}
        const propietarioObj = this.obtenerParticipantePropietario()

        this.indicadorNotificacionesInterno.idPorLeer.forEach(item => {
            const index = this.listaMensajes.findIndex(e => e.id === item.id)
            if (index >= 0) {
                const mensaje = this.listaMensajes[index]
                const indexDos = mensaje.leidoPor.findIndex(e => e.perfil._id === propietarioObj.perfil._id)

                if (indexDos < 0) {
                    mensaje.leidoPor.push(propietarioObj)
                    const path = this.params.id + '/' + mensaje.id + '/leidoPor'
                    aux[path] = mensaje.leidoPor
                }
            }
        })

        if (Object.keys(aux).length > 0) {
            this.db.database.ref('mensajes').update(aux)
                .then(data => {
                    this.actualizarEstadoLeidoDeMensajesEnNotificaciones(propietarioObj)
                }).catch(error => {
                })
        }
    }

    buscarContactoParaReenviar(reiniciar: boolean) {


        if (reiniciar) {
            this.confListaContactoCompartido.listaContactos.lista = this.listaContactosParaBusqueda
            return
        }

        if (!(
            this.confAppbar &&
            this.confAppbar.searchBarAppBar &&
            this.confAppbar.searchBarAppBar.buscador &&
            this.confAppbar.searchBarAppBar.buscador.configuracion &&
            this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda &&
            this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda.length > 0
        )) {


            this.confListaContactoCompartido.listaContactos.lista = this.listaContactosParaBusqueda
            return
        }

        const buscar = this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda
        const listaContactosBuscador: Array<ConfiguracionItemListaContactosCompartido> = []
        this.listaContactosParaBusqueda = this.confListaContactoCompartido.listaContactos.lista
        this.confListaContactoCompartido.listaContactos.cargando = true

        const contactos: Array<ConfiguracionItemListaContactosCompartido> = this.confListaContactoCompartido.listaContactos.lista

        contactos.forEach(item => {
            if (
                item &&
                item.contacto &&
                item.contacto.nombreContacto &&
                item.contacto.nombreContacto.toLowerCase().includes(buscar)
            ) {
                listaContactosBuscador.push(item)
            }
        })

        this.confListaContactoCompartido.listaContactos.lista = listaContactosBuscador
        this.confListaContactoCompartido.listaContactos.cargando = false
    }

    async obtenerContactos(primeraVez?: boolean) {
        if (this.listaMensajesSeleccionados.length <= 0) {
            return
        }

        if (primeraVez) {
            this.configurarListaContactoCompartido()
            const confBuscador: BarraBusqueda = this.chatMetodosCompartidosService.configurarBuscadorParaBarraReenvio(
                this.buscarContacto
            )
            this.configurarAppBar(confBuscador, UsoAppBar.USO_SEARCHBAR_APPBAR)
            this.confListaContactoCompartido.mostrar = true
        }

        try {
            this.confListaContactoCompartido.listaContactos.cargando = true

            if (!this.listaContactos.proximaPagina) {
                return
            }

            const contactos: PaginacionModel<ParticipanteAsociacionModel> =
                await this.participanteAsociacionNegocio.obtenerParticipanteAsoTipo(
                    this.perfilSeleccionado._id,
                    15,
                    this.listaContactos.paginaActual,
                    CodigoEstadoParticipanteAsociacion.CONTACTO,
                    FiltroGeneral.ALFA
                ).toPromise()

            this.listaContactos.proximaPagina = contactos.proximaPagina


            contactos.lista.forEach(item => {
                this.listaContactos.lista.push(item)
                this.confListaContactoCompartido.listaContactos.lista.push(
                    this.metodosParaFotos.configurarItemListaContacto(
                        item.contactoDe,
                        item.asociacion
                    )
                )

            })

            this.confListaContactoCompartido.listaContactos.cargando = false

        } catch (error) {
            this.confListaContactoCompartido.botonAccion.mostrarDialogo = false
            this.confListaContactoCompartido.listaContactos.cargando = false
            this.confListaContactoCompartido.listaContactos.error = 'text37'
        }

    }

    async reenviarMensajes() {
        if (this.listaAsociacionesSeleccionados.length <= 0) {
            return
        }

        try {
            const querys = {}
            const querysUltimoMensaje = {}


            this.listaAsociacionesSeleccionados.forEach(async idAsociacion => {
                const refConversacion = this.db.database.ref('mensajes/' + idAsociacion)
                let ultimoMensaje = null
                this.listaMensajesSeleccionados.forEach((mensaje, pos) => {
                    const idMensaje = refConversacion.push().key
                    const path = idAsociacion + '/' + idMensaje
                    const mensajeRes = this.cambiarDatosMensajeReenviar(
                        mensaje,
                        idAsociacion,
                        idMensaje
                    )
                    querys[path] = mensajeRes

                    if (pos === this.listaMensajesSeleccionados.length - 1) {
                        ultimoMensaje = mensajeRes
                    }
                })

                const resultado = await this.validarSiExisteConversacion(idAsociacion)

                if (resultado && ultimoMensaje !== null) {
                    const pathDos = idAsociacion + '/ultimoMensaje'
                    querysUltimoMensaje[pathDos] = ultimoMensaje
                }
            })
            await this.db.database.ref('mensajes').update(querys)
            await this.db.database.ref('conversaciones').update(querysUltimoMensaje)
            this.reiniciarVariablesReenvio()
            this.toast.cerrarToast()
            window.location.reload()
        } catch (error) {
            console.log('error', error);
            
            this.toast.abrirToast('text37')
        }
    }

    cambiarDatosMensajeReenviar(
        mensaje: MensajeModelFirebase,
        idAsociacion: string,
        idMensaje: string
    ): MensajeModelFirebase {
        const propietario: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion()
        const mensajeRes: MensajeModelFirebase = {
            ...mensaje
        }
        mensajeRes.id = idMensaje
        mensajeRes.conversacion = {
            id: idAsociacion,
        }
        mensajeRes.fechaCreacion = Firebase.default.database.ServerValue.TIMESTAMP,
            mensajeRes.fechaActualizacion = Firebase.default.database.ServerValue.TIMESTAMP,
            mensajeRes.propietario = {
                id: propietario.id || '',
                perfil: {
                    _id: this.perfilSeleccionado._id,
                    nombreContacto: this.perfilSeleccionado.nombreContacto
                }
            }
        mensajeRes.entregadoA = [
            {
                id: propietario.id || '',
                perfil: {
                    _id: this.perfilSeleccionado._id,
                    nombreContacto: this.perfilSeleccionado.nombreContacto
                }
            }
        ]
        mensajeRes.eliminadoPor = []
        mensajeRes.leidoPor = [
            {
                id: propietario.id || '',
                perfil: {
                    _id: this.perfilSeleccionado._id,
                    nombreContacto: this.perfilSeleccionado.nombreContacto
                }
            }
        ]
        mensajeRes.listaIdConversacion = []
        mensajeRes.identificadorTemporal = new Date().getTime()
        return mensajeRes
    }

    reiniciarVariablesReenvio() {
        this.listaAsociacionesSeleccionados = []
        this.listaMensajesSeleccionados.forEach(mensaje => {
            const index = this.listaConfiguracionMensajes.findIndex(e => e.mensaje.id === mensaje.id)
            if (index >= 0) {
                this.listaConfiguracionMensajes[index].seleccionado = false
            }
        })
        this.listaMensajesSeleccionados = []
        this.confListaContactoCompartido.mostrar = false
        this.configurarListaContactosPaginacion()
    }

    reiniciarVariablesEliminacion() {
        this.listaAsociacionesSeleccionados = []
        this.listaMensajesSeleccionados.forEach(mensaje => {
            const index = this.listaConfiguracionMensajes.findIndex(e => e.mensaje.id === mensaje.id)
            if (index >= 0) {
                this.listaConfiguracionMensajes[index].seleccionado = false
            }
        })
        this.listaMensajesSeleccionados = []
    }

    descargarMensajes() {
        this.contador = 0

        this.interval = setInterval(() => {
            if (
                this.contador === this.listaMensajesSeleccionados.length
            ) {
                this.limpiar()
                return
            }
            if (
                this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.ARCHIVO ||
                this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.AUDIO ||
                this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.IMAGEN
            ) {
                this.descagarAdjunto(this.listaMensajesSeleccionados[this.contador])

            }
            if (this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.TEXTO) {
                this.descargarTexto(this.listaMensajesSeleccionados[this.contador])
            }

            if (this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.COMPARTIR_NOTICIA ||
                this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.COMPARTIR_PROYECTO ||
                this.listaMensajesSeleccionados[this.contador].tipo.codigo === CatalogoTipoMensaje.TRANSFERIR_PROYECTO
            ) {
                this.descagarImgCompartir(this.listaMensajesSeleccionados[this.contador])
            }
            this.contador++
        }, 1000)

    }

    limpiar() {
        clearInterval(this.interval);
        this.listaMensajesSeleccionados = []
        this.cambiarEstadoSeleccionadoEnTodosLosItemsDeListaMensajes()
        this.confListaContactoCompartido.mostrar = false
    }

    descagarImgCompartir(mensaje?: MensajeModelFirebase) {
        let link = document.createElement('a');
        let nombreFile = mensaje.referenciaEntidadCompartida.adjuntos[0].portada.principal.url.toString()
        const nombre = nombreFile.substr(50, nombreFile.length)
        link.href = mensaje.referenciaEntidadCompartida.adjuntos[0].portada.principal.url;
        link.download = nombre;
        link.dispatchEvent(new MouseEvent('click'));

    }

    descagarAdjunto(mensaje?: MensajeModelFirebase) {


        // let link = document.createElement('a');
        // const nombre = mensaje.adjuntos[0].principal.filename.toString()
        // link.href = mensaje.adjuntos[0].principal.url;
        // link.download = nombre;
        // link.dispatchEvent(new MouseEvent('click'));

        let link = document.createElement('a');
        const nombre = mensaje.adjuntos[0].principal.filename.toString()
        link.href = mensaje.adjuntos[0].principal.url;
        link.download = nombre;
        link.target = '_blank'
        link.dispatchEvent(new MouseEvent('click', {
            view: window,
            bubbles: false,
            cancelable: true
        }));

    }

    descargarTexto(mensaje?: MensajeModelFirebase) {


        let filename = 'texto.txt'
        let type = "text/plain;charset=utf-8";
        let a = document.createElement('a')
        let file = new Blob([mensaje.contenido], { type: type });
        a.href = URL.createObjectURL(file);
        a.download = filename
        a.dispatchEvent(new MouseEvent('click'));


    }

    eliminarMensajes() {
        if (this.listaMensajesSeleccionados.length <= 0) {
            return
        }


        const propietario: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion()

        const querys = {}
        this.listaMensajesSeleccionados.forEach(mensaje => {
            const mensajeObj = { ...mensaje }

            if (mensajeObj.propietario.perfil._id === this.perfilSeleccionado._id) {
                mensajeObj.estado.codigo = EstadoMensaje.ELIMINADO

                const path = this.params.id + '/' + mensajeObj.id + '/estado'
                querys[path] = mensajeObj.estado
            } else {
                if (!mensajeObj.eliminadoPor) {
                    mensajeObj.eliminadoPor = []
                }

                const index = mensajeObj.eliminadoPor.findIndex(e => e.perfil._id === propietario.perfil._id)

                if (index < 0) {
                    mensajeObj.eliminadoPor.push({
                        id: propietario.id || '',
                        perfil: {
                            _id: this.perfilSeleccionado._id
                        }
                    })

                    const path = this.params.id + '/' + mensajeObj.id + '/eliminadoPor'
                    querys[path] = mensajeObj.eliminadoPor
                }
            }
        })

        this.db.database.ref('mensajes').update(querys)
            .then(data => {
                this.listaMensajesSeleccionados.forEach(mensaje => {
                    this.indicadorNotificacionesInterno.idEliminados.push({
                        id: mensaje.id,
                        fechaCreacion: mensaje.fechaCreacion
                    })
                })
                this.reiniciarVariablesEliminacion()
                this.toast.cerrarToast()
            }).catch(error => {
            })
    }

    asignarErrorEnMensaje(
        mensaje: MensajeModelFirebase
    ) {
        this.listaDeFechasDelChat.forEach(fecha => {
            const index = fecha.mensajes.findIndex(e => e.mensaje.id === mensaje.id)
            if (index >= 0) {
                fecha.mensajes[index].mostrarCargando = false
                fecha.mensajes[index].contenidoError = 'text37'
            }
        })
    }

    accionAtrasAppBar() {

        if (this.mostrarReproducirVideo) {
            this.mostrarReproducirVideo = false
            return
        }

        if (
            this.listaMensajesSeleccionados.length > 0 ||
            this.confListaContactoCompartido.mostrar === true
        ) {
            this.listaMensajesSeleccionados = []
            this.cambiarEstadoSeleccionadoEnTodosLosItemsDeListaMensajes()

            this.configurarAppBar()
            this.configurarListaContactosPaginacion()
            this.confListaContactoCompartido.mostrar = false
            return
        }

        this._location.back()
    }

    async actualizarEstadoTransferenciaProyecto(
        idMensaje: string,
        estado: CatalogoEstatusMensaje,
    ) {
        try {
            const indexMensaje = this.listaMensajes.findIndex(e => e.id === idMensaje)
            const mensaje = this.listaMensajes[indexMensaje]
            if (indexMensaje < 0) {
                return
            }

            mensaje.estatus.codigo = estado
            const path = this.params.id + '/' + mensaje.id
            const querys = {}
            querys[path] = mensaje

            const indicador = {}
            indicador[mensaje.referenciaEntidadCompartida.id] = {
                activa: false
            }

            await this.db.database.ref('mensajes').update(querys)
            await this.db.database.ref('transferencias-activas').update(indicador)

            if (estado === CatalogoEstatusMensaje.ACEPTADO) {
                await this.db.database.ref(
                    'mensajes/' +
                    this.params.id +
                    '/' +
                    mensaje.id +
                    '/referenciaEntidadCompartida/perfil'
                ).set({
                    _id: this.perfilSeleccionado._id,
                    nombre: this.perfilSeleccionado.nombre,
                    nombreContacto: this.perfilSeleccionado.nombreContacto
                })

                this.confirmarTransferenciaAlApi(mensaje.referenciaEntidadCompartida.id)
            }
        } catch (error) {
            this.toast.abrirToast('error al actualizar estado en firebase')
        }
    }

    async confirmarTransferenciaAlApi(
        idEntidad: string
    ) {
        if (!idEntidad || idEntidad.length <= 0) {
            return
        }

        const propietario: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion()
        const otroParticipante: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion(false)

        try {
            const status = await this.proyectoNegocio.confirmarTransferenciaDeProyecto(
                idEntidad,
                propietario.perfil._id,
                otroParticipante.perfil._id
            ).toPromise()

            if (status !== 200 && status !== 201) {
                throw new Error('')
            }
        } catch (error) {
            this.toast.abrirToast('Error al confirmar la transferencia del proyecto')
        }
    }

    configurarBotonEstadoTransferenciaDelProyecto(
        mensaje: ConfiguracionMensajeFirebase,
        aceptar: boolean = true
    ): BotonCompartido {  
        return {
          
            text: (aceptar) ? 'm3v10texto9' : mensaje.esPropietario? 'm3v10texto10.1' : 'm3v10texto10',
            enProgreso: false,
            tipoBoton: TipoBoton.TEXTO,
            ejecutar: () => this.actualizarEstadoTransferenciaProyecto(
                mensaje.mensaje.id,
                (aceptar) ? CatalogoEstatusMensaje.ACEPTADO : CatalogoEstatusMensaje.RECHAZADO
            ),
            colorTexto: (aceptar) ? ColorTextoBoton.AMARRILLO : ColorTextoBoton.ROJO,
            tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL
        }
    }

    trackByFn(index: number, item: ConfiguracionMensajeFirebase) {
        return item.mensaje.id;
    }

    async enviarMensajeAccion(
        dataApiArchivo: SubirArchivoData,
        propietario: ParticipanteAsociacionModel,
        otroPropietario: ParticipanteAsociacionModel,
        tipoMensaje: CatalogoTipoMensaje,
        idMensaje: string,
        conAdjunto: boolean = false
    ) {

        const mensajeModel: MensajeModelFirebase = this.chatMetodosCompartidosService.crearObjetoMensajeAEnviar(
            idMensaje,
            dataApiArchivo,
            propietario,
            tipoMensaje,
            this.params.id,
            this.perfilSeleccionado._id
        )

        try {
            if (conAdjunto) {
                this.insertarSoloUnMensajeEnListas(mensajeModel)

                const media: MediaModel = await this.mediaNegocio.subirMedia(dataApiArchivo).toPromise()

                if (!media) {
                    throw new Error('')
                }

                mensajeModel.adjuntos.push(media)
            }

            this.db.database.ref('mensajes/' + this.params.id + '/' + mensajeModel.id).set(mensajeModel)
                .then(a => {
                    return this.db.database.ref('conversaciones/' + this.params.id + '/ultimoMensaje').set(mensajeModel)
                }).then(b => {
                    this.dataScrool.scroolAlFinal = true
                }).catch(error => {
                    if (error && conAdjunto) {
                        this.asignarErrorEnMensaje(mensajeModel)
                    }
                })
        } catch (error) {
            if (conAdjunto) {
                this.asignarErrorEnMensaje(mensajeModel)
            }
        }
    }

    insertarTodosLosMensajesEnListas(
        actions: SnapshotAction<unknown>[],
        insertarAlFinal: boolean = true,
    ) {
        actions.forEach(action => {
            const mensaje: MensajeModelFirebase = {
                id: action.key,
                ...action.payload.val() as Object
            }
            // Encontrar el id
            const index = this.listaMensajes.findIndex(e => e.id === mensaje.id)

            const estaEliminado = (mensaje.estado.codigo === EstadoMensaje.ELIMINADO)
            const eliminadoPor = (mensaje.eliminadoPor && mensaje.eliminadoPor.length > 0 && mensaje.eliminadoPor.findIndex(e => e.perfil._id === this.perfilSeleccionado._id) >= 0)

            const indexEliminadoLocal = this.indicadorNotificacionesInterno.idEliminados.findIndex(e => e.id === mensaje.id)

            // Si el mensaje es nuevo en la lista
            if (!estaEliminado && !eliminadoPor && indexEliminadoLocal < 0) {
                if (index < 0) {
                    if (insertarAlFinal) {
                        this.listaMensajes.push(mensaje)
                    } else {
                        this.listaMensajes.unshift(mensaje)
                    }
                } else {
                    if (mensaje.tipo.codigo === CatalogoTipoMensaje.TRANSFERIR_PROYECTO) {
                   
                    }

                    this.listaMensajes[index] = mensaje


                    const indexDos = this.listaConfiguracionMensajes.findIndex(e => e.mensaje.id === mensaje.id)
                    if (
                        this.listaConfiguracionMensajes[indexDos].mostrarCargando &&
                        (
                            mensaje.tipo.codigo === CatalogoTipoMensaje.ARCHIVO ||
                            mensaje.tipo.codigo === CatalogoTipoMensaje.AUDIO
                        )
                    ) {
                        this.listaConfiguracionMensajes[indexDos].mostrarCargando = false
                    }
                }

                this.chatMetodosCompartidosService.actualizarEstadoIndicadorNotificaciones(
                    this.indicadorNotificacionesInterno,
                    mensaje,
                    this.perfilSeleccionado._id
                )
            } else if (index >= 0) {
                const id = this.listaMensajes[index].id
                this.listaMensajes.splice(index, 1)

                const indexDos = this.listaConfiguracionMensajes.findIndex(e => e.mensaje.id === id)
                if (indexDos >= 0) {
                    this.listaConfiguracionMensajes.splice(indexDos, 1)
                }

                let indexTres = -1
                this.listaDeFechasDelChat.forEach((grupo, pos) => {
                    const indexCuatro = grupo.mensajes.findIndex(e => e.mensaje.id === id)
                    if (indexCuatro >= 0) {
                        grupo.mensajes.splice(indexCuatro, 1)
                    }

                    if (grupo.mensajes.length === 0) {
                        indexTres = pos
                    }
                })

                if (indexTres >= 0) {
                    this.listaDeFechasDelChat.splice(indexTres, 1)
                }
            }
        })

        // this.listaMensajes = this.chatMetodosCompartidosService.ordenarItemEnListaPorFechaDeFormaAscendente(
        //     this.listaMensajes
        // )

        this.listaConfiguracionMensajes = this.chatMetodosCompartidosService.crearListaDeConfiguraCionDeMensajes(
            this.listaMensajes,
            this.perfilSeleccionado,
            this.listaMensajesSeleccionados,
            this.listaConfiguracionMensajes
        )

        this.listaDeFechasDelChat = this.chatMetodosCompartidosService.crearListaDeFechasDesdeListaDeMensajes(
            this.listaMensajes,
            this.listaConfiguracionMensajes,
            this.listaDeFechasDelChat,
            insertarAlFinal
        )

        this.listaDeFechasDelChat = this.chatMetodosCompartidosService.ordenarItemsEnListaDeFechasDeFormaAscendente(
            this.listaDeFechasDelChat
        )
    }

    insertarSoloUnMensajeEnListas(
        mensajeOrg: MensajeModelFirebase
    ) {
        const fecha = new Date()
        const mensaje: MensajeModelFirebase = {
            ...mensajeOrg
        }

        mensaje.fechaCreacion = fecha
        mensaje.fechaActualizacion = fecha

        this.listaMensajes.push(mensaje)

        const configuracion = this.chatMetodosCompartidosService.crearConfiguraCionMensajeUnica(mensaje, this.perfilSeleccionado, false, true)
        this.listaConfiguracionMensajes.push(configuracion)

        let insertarNuevoGrupo = true

        if (this.listaDeFechasDelChat.length > 0) {
            const ultimaFecha = this.listaDeFechasDelChat[this.listaDeFechasDelChat.length - 1].date
            if (this.chatMetodosCompartidosService.compararFechas(ultimaFecha, fecha.getTime())) {
                this.listaDeFechasDelChat[this.listaDeFechasDelChat.length - 1].mensajes.push(configuracion)
                insertarNuevoGrupo = false
            }
        }

        if (insertarNuevoGrupo) {
            this.listaDeFechasDelChat.push({
                id: this.generadorId.generarIdConSemilla(),
                timestamp: fecha.getTime(),
                date: fecha,
                mensajes: [configuracion]
            })
        }

        this.dataScrool.scroolAlFinal = true
    }

    detectarScrollEnListaMensajes() {
        this.dataScrool.scroolAlFinal = false

        const elemento = document.getElementById(this.idListaMensajesEnVista) as HTMLElement
        if (!elemento) {
            return
        }

        if (elemento.scrollTop === 0 && this.dataScrool.puedeCargarMas) {
            this.indicadoresScroll = this.chatMetodosCompartidosService.obtenerDataScroll(
                this.idListaMensajesEnVista,
                this.indicadoresScroll,
            )

            this.limitePaginacion = this.limitePaginacion + 15
            this.dataScrool.puedeCargarMas = false

            this.cargarMasMensajes(
                this.limitePaginacion,
                this.listaMensajes[0].id
            )
        }

        // Mostrar notificaciones
        if (elemento.scrollTop >= 0 && elemento.scrollTop < (elemento.scrollHeight - elemento.clientHeight)) {
            this.indicadorNotificacionesInterno.mostrar = true
        }

        // Scrool al final
        if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 2.49) {
            this.indicadorNotificacionesInterno.mostrar = false

            if (this.indicadorNotificacionesInterno.idPorLeer.length > 0) {
                this.actualizarMensajesNoLeidos()
            }
        }
    }

    actualizarEstadoLeidoDeMensajesEnNotificaciones(
        lector: ParticipanteAsociacionModel
    ) {
        this.indicadorNotificacionesInterno.idPorLeer.forEach(item => {
            const indexLeido = this.indicadorNotificacionesInterno.idLeidos.findIndex(e => e.id === item.id)
            if (indexLeido < 0) {
                this.indicadorNotificacionesInterno.idLeidos.push(item)
            }

            const index = this.listaMensajes.findIndex(e => e.id === item.id)
            if (index >= 0) {
                this.listaMensajes[index].leidoPor.push(lector)
            }
        })

        this.indicadorNotificacionesInterno.idPorLeer = []
        this.indicadorNotificacionesInterno.idLeidos = this.chatMetodosCompartidosService.ordenarItemEnListaDeNotificaciones(
            this.indicadorNotificacionesInterno.idLeidos
        )

        this.changeDetectionRef.detectChanges()
    }

    obtenerParticipantePropietario() {
        const propietario: ParticipanteAsociacionModel = this.obtenerParticipantesDeAsociacion()
        return {
            id: propietario.id || '',
            perfil: {
                _id: this.perfilSeleccionado._id
            }
        }
    }

    async validarSiExisteConversacion(
        idAsociacion: string
    ) {
        try {
            const dataFirebase = await this.db.database.ref('conversaciones/' + idAsociacion).get()

            if (dataFirebase.exists()) {
                return true
            }

            const index = this.listaContactos.lista.findIndex(e => e.asociacion.id === idAsociacion)

            if (index < 0) {
                return false
            }

            const propietario = this.obtenerParticipantesDeAsociacion()
            const otroParticipante = this.listaContactos.lista[index].contactoDe
            const asociacion = this.listaContactos.lista[index].asociacion

            const conversacion = this.chatMetodosCompartidosService.crearObjetoDeConversacionParaReenvio(
                propietario,
                otroParticipante,
                asociacion
            )

            const res = await this.db.database.ref('conversaciones/' + idAsociacion).set(conversacion)
            return true
        } catch (error) {
            return false
        }
    }


    eventoIcono(
        tipo: CodigoCatalogoTipoLlamada
    ) {
        // this.llamadaFirebaseService.configurarParams({
        //     estado: false,
        //     id: this.asociacion.id,
        //     tipo: tipo,
        //     accion: CodigoCatalogoAccionLlamada.LLAMAR,
        //     host: true
        // })
        this.router.navigateByUrl(RutasLocales.GAZING + '/' + RutasGazing.CONFERENCIA)
    }
}

export interface Fecha {
    id: string,
    timestamp: number,
    date: Date,
    mensajes: Array<ConfiguracionMensajeFirebase>
}

export interface NotificacionInterno {
    id: string,
    fechaCreacion: any
}

export interface VariablesDelScrool {
    scroolAlFinal: boolean,
    puedeCargarMas: boolean,
    mostrarLoaderPaginacion: boolean,
}

export interface IndicadoresDelScrool {
    antes?: {
        height: number,
        offset: number,
    },
    despues?: {
        height: number,
        offset: number,
    }
}

export interface MarcadorDeNotificacion {
    mostrar: boolean,
    idLeidos: Array<NotificacionInterno>,
    idPorLeer: Array<NotificacionInterno>,
    idEliminados: Array<NotificacionInterno>
}

const TIPOS_VIDEO = {
    'video/mp4': true,
    'video/mkv': true,
    'video/mov': true,
    'video/avi': true,
    // 'video/webm': true,

}
const TIPOS_AUDIO = {
    'audio/acc3': true,
    'audio/mp3': true,
    'audio/mp4': true,
    'audio/ogg': true,
    'audio/webm': true,
    'audio/wav': true,
    'audio/mpeg': true,
}

const TIPOS_IMAGEN = {
    'image/jpeg': true,
    'image/jpg': true,
    'image/png': true,
}