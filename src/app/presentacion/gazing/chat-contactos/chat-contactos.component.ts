import { RutasPerfiles } from 'src/app/presentacion/perfiles/rutas-perfiles.enum';
import { RutasGazing } from 'src/app/presentacion/gazing/rutas-gazing.enum';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import {
  TipoBoton,
  ColorTextoBoton,
} from '@shared/componentes/button/button.component';
import { TranslateService } from '@ngx-translate/core';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { FiltroGeneral } from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import { CodigoEstadoParticipanteAsociacion } from '@core/servicios/remotos/codigos-catalogos/codigo-estado-partic-aso.enum';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import { MetodosParaFotos } from '@core/servicios/generales/metodos-para-fotos.service';
import {
  ConfiguracionItemListaContactosCompartido,
  DataContacto,
} from '@shared/diseno/modelos/lista-contactos.interface';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ParticipanteAsociacionNegocio } from 'dominio/logica-negocio/participante-asociacion.negocio';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import {
  EstilosDelTexto,
  ColorDelTexto,
  ColorDeBorde,
  ColorDeFondo,
} from '@shared/diseno/enums/estilos-colores-general';
import { PaddingLineaVerdeContactos } from '@shared/diseno/enums/estilos-padding-general';
import {
  TamanoColorDeFondo,
  TamanoDeTextoConInterlineado,
} from '@shared/diseno/enums/estilos-tamano-general.enum';
import {
  AngularFireDatabase,
  AngularFireDatabaseModule,
} from '@angular/fire/database';
import { MensajeModelFirebase } from 'dominio/modelo/entidades/mensaje.model';
import { CatalogoTipoMensaje } from '@core/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { NotificacionesDePerfil } from '@core/servicios/generales/notificaciones/notificaciones-perfil.service';
import {
  DataNotificaciones,
  NotificacionFirebaseModel,
} from '@core/servicios/generales/notificaciones/notificaciones.interface';
@Component({
  selector: 'app-chat-contactos',
  templateUrl: './chat-contactos.component.html',
  styleUrls: ['./chat-contactos.component.scss'],
})
export class ChatContactosComponent implements OnInit, OnDestroy {
  @ViewChild('toast', { static: false }) toast: ToastComponent;

  // Utils
  public idCapaContactos: string;
  public puedeCargarMas: boolean;
  public idAsociacionActivo: string;

  // Parametros internos
  public perfilSeleccionado: PerfilModel;
  public contadorNotificaciones: number;
  public listaContactos: PaginacionModel<ConfiguracionItemListaContactosCompartido>;
  public mostrarCargando: boolean;
  public mostrarCargandoPequeno: boolean;
  public mostrarError: boolean;
  public contenidoError: string;

  public notificaciones: Array<NotificacionFirebaseModel>;
  public notificacionesTratadas: Array<string>;
  public dataNotificaciones: DataNotificaciones;
  public confListaNotificaciones: ConfiguracionListaNotificaciones;

  // Configuraciones componentes hijos
  public confAppBar: ConfiguracionAppbarCompartida;
  public confItemNotificaciones: ConfiguracionItemListaContactosCompartido;
  public configDialoEliminarContacto: DialogoCompartido;
  public confToast: ConfiguracionToast;

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    public variablesGoblales: VariablesGlobales,
    private participanteAsoNegocio: ParticipanteAsociacionNegocio,
    private perfilNegocio: PerfilNegocio,
    private router: Router,
    private _location: Location,
    private metodosParaFotos: MetodosParaFotos,
    private albumNegocio: AlbumNegocio,
    private translateService: TranslateService,
    private notificacionService: NotificacionesDePerfil,
    private db: AngularFireDatabase
  ) {
    this.contadorNotificaciones = 0;
    this.mostrarCargandoPequeno = false;
    this.mostrarCargando = false;
    this.mostrarError = false;
    this.contenidoError = '';
    this.idCapaContactos = 'lista-contactos-capa';
    this.puedeCargarMas = true;
    this.idAsociacionActivo = '';

    this.notificaciones = [];
    this.notificacionesTratadas = [];
  }

  ngOnInit(): void {
    this.configurarPerfilSeleccionado();
    this.configurarDialogoEliminar();
    this.configurarToast();
    this.configurarListaNotificaciones();

    if (this.perfilSeleccionado) {
      this.configurarAppBar();
      this.configurarItemNotificaciones();
      this.configurarListas();
      this.obtenerContactos();
      // Notificaciones
      this.configurarDataNotificaciones();
      this.configurarEscuchaNotificaciones();
      this.notificacionService.obtenerNotificaciones$.next(
        this.dataNotificaciones
      );
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado);
    }
  }

  ngOnDestroy(): void {
    this.notificacionService.desconectarDeEscuchaNotificaciones();
  }

  configurarPerfilSeleccionado() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarAppBar() {
    this.confAppBar = {
      accionAtras: () => {
        if (this.confListaNotificaciones.mostrar) {
          this.confListaNotificaciones.mostrar = false;
          return;
        }

        this._location.back();
      },
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre,
        },
        buscador: {
          mostrar: true,
          configuracion: {
            disable: false,
            modoBusqueda: ModoBusqueda.BUSQUEDA_COMPONENTE,
            placeholder: 'm3v7texto1',
            valorBusqueda: '',
            entidad: CodigosCatalogoEntidad.CONTACTO,
          },
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm3v7texto2',
        },
      },
    };
  }

  configurarToast() {
    this.confToast = {
      cerrarClickOutside: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: '',
    };
  }

  configurarListaNotificaciones() {
    this.confListaNotificaciones = {
      mostrar: false,
      lista: [],
      contador: 0,
    };
  }

  async configurarDialogoEliminar() {
    this.configDialoEliminarContacto = {
      mostrarDialogo: false,
      descripcion: '',
      tipo: TipoDialogo.CONFIRMACION,
      completo: true,
      listaAcciones: [
        {
          text: 'm3v9texto2',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.ROJO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoEliminarContacto.mostrarDialogo = false;
            this.eliminarContacto();
          },
        },
        {
          text: 'm3v9texto3',
          tipoBoton: TipoBoton.TEXTO,
          colorTexto: ColorTextoBoton.AMARRILLO,
          tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
          enProgreso: false,
          ejecutar: () => {
            this.configDialoEliminarContacto.mostrarDialogo = false;
            this.idAsociacionActivo = '';
          },
        },
      ],
    };

    this.configDialoEliminarContacto.descripcion = await this.translateService
      .get('m3v9texto1')
      .toPromise();
  }

  configurarItemNotificaciones() {
    this.confItemNotificaciones = {
      id: '',
      usoItem: UsoItemListaContacto.USO_MENSAJE,
      mostrarTextoInfo: true,
      mensaje: {
        contacto: {
          nombreContacto: 'm3v7texto3',
          estilosTextoSuperior: {
            color: ColorDelTexto.TEXTOAZULBASE,
            estiloTexto: EstilosDelTexto.BOLD,
            enMayusculas: true,
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          },
          estilosTextoInferior: {
            color: ColorDelTexto.TEXTOROJOBASE,
            estiloTexto: EstilosDelTexto.REGULAR,
            enMayusculas: true,
            tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
          },
          idPerfil: '',
        },
        contadorMensajes: this.confListaNotificaciones.contador,
      },
      configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
        'https://d2p41ymqiu27wq.cloudfront.net/file-default/c47e9e33-4104-4109-97f1-c08ceadf5268.png',
        ColorDeBorde.BORDER_AZUL,
        this.metodosParaFotos.obtenerColorFondoAleatorio(),
        true,
        UsoItemCircular.CIREXCLAMACION,
        true
      ),
      mostrarX: {
        mostrar: false,
      },
      eventoCirculoNombre: () => {
        if (this.confListaNotificaciones.lista.length <= 0) {
          this.confListaNotificaciones.mostrar = false;
          return;
        }

        this.confListaNotificaciones.mostrar =
          !this.confListaNotificaciones.mostrar;
      },
    };
  }

  configurarListas(a: boolean = true, b: boolean = true) {
    if (a) {
      this.listaContactos = {
        lista: [],
        paginaActual: 1,
        proximaPagina: true,
        totalDatos: 0,
      };
    }

    if (b) {
    }
  }

  reintentar() {
    this.configurarListas(true, false);
    this.obtenerContactos();
  }

  configuracionContactos(
    contacto: ParticipanteAsociacionModel
  ): ConfiguracionItemListaContactosCompartido {
    let usoCirculo: UsoItemCircular;
    let urlMedia: string;

    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.PERFIL,
      contacto.contactoDe?.album
    );

    if (
      !album ||
      (album.portada &&
        album.portada.principal &&
        album.portada.principal.fileDefault)
    ) {
      usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO;
      urlMedia = contacto.contactoDe.album[0].portada.principal.url;
    } else {
      usoCirculo = UsoItemCircular.CIRCONTACTO;
      urlMedia = album.portada.principal.url;
    }

    return {
      id: contacto.asociacion.id,
      usoItem: UsoItemListaContacto.USO_CONTACTO,
      contacto: {
        nombreContacto: contacto.contactoDe.nombreContacto,
        nombreContactoTraducido: contacto.contactoDe.nombreContactoTraducido,
        nombre: contacto.contactoDe.nombre,
        estilosTextoSuperior: {
          color: ColorDelTexto.TEXTOAZULBASE,
          estiloTexto: EstilosDelTexto.BOLD,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        estilosTextoInferior: {
          color: ColorDelTexto.TEXTONEGRO,
          estiloTexto: EstilosDelTexto.REGULAR,
          enMayusculas: true,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
        },
        idPerfil: contacto.contactoDe._id || '',
      },
      configCirculoFoto: this.metodosParaFotos.configurarItemCircular(
        urlMedia,
        ColorDeBorde.BORDER_ROJO,
        this.metodosParaFotos.obtenerColorFondoAleatorio(),
        false,
        usoCirculo,
        true
      ),
      mostrarX: {
        mostrar: true,
        color: true,
      },
      configuracionLineaVerde: this.metodosParaFotos.configurarLineaVerde(
        PaddingLineaVerdeContactos.PADDING_1542_267
      ),
      eventoCirculoNombre: (idAsociacion: string) => {
        this.abrirChatValidandoNotificaciones(idAsociacion);
      },
      eventoDobleTap: (idAsociacion: string, contacto: DataContacto) => {
        if (
          !contacto ||
          !contacto.idPerfil ||
          !(contacto.idPerfil.length > 0)
        ) {
          return;
        }

        const ruta = RutasLocales.MODULO_PERFILES.toString();
        const aux = RutasPerfiles.PERFIL.toString().replace(
          ':id',
          contacto.idPerfil
        );
        this.router.navigateByUrl(ruta + '/' + aux);
      },
      eventoIconoX: (idAsociacion: string) => {
        this.idAsociacionActivo = idAsociacion;
        this.configDialoEliminarContacto.mostrarDialogo = true;
      },
    };
  }

  async obtenerContactos() {
    try {
      if (!this.listaContactos.proximaPagina) {
        return;
      }

      this.mostrarCargando = this.listaContactos.lista.length === 0;
      this.mostrarCargandoPequeno = this.listaContactos.lista.length > 0;
      this.mostrarError = false;
      const contactos: PaginacionModel<ParticipanteAsociacionModel> =
        await this.participanteAsoNegocio
          .obtenerParticipanteAsoTipo(
            this.perfilSeleccionado._id,
            12,
            this.listaContactos.paginaActual,
            CodigoEstadoParticipanteAsociacion.CONTACTO,
            FiltroGeneral.ALFA
          )
          .toPromise();
      this.listaContactos.proximaPagina = contactos.proximaPagina;
      this.listaContactos.totalDatos = contactos.totalDatos;

      if (this.listaContactos.proximaPagina) {
        this.listaContactos.paginaActual += 1;
      }

      contactos.lista.forEach((contacto) => {
        this.listaContactos.lista.push(this.configuracionContactos(contacto));
      });
      console.log('this.listaContactos' ,this.listaContactos.lista.length);
      

      this.mostrarCargando = false;
      this.mostrarCargandoPequeno = false;
      this.mostrarError = false;
      this.puedeCargarMas = true;
    } catch (error) {
      this.puedeCargarMas = true;
      this.mostrarCargando = false;
      this.mostrarError = true;
      this.contenidoError = 'text31';
    }
  }

  eventoDeScroll() {
    const elemento: HTMLElement = document.getElementById(
      this.idCapaContactos
    ) as HTMLElement;
    if (
      elemento.offsetHeight + elemento.scrollTop >=
      elemento.scrollHeight - 5.22
    ) {
      if (this.puedeCargarMas) {
        this.puedeCargarMas = false;
        this.obtenerContactos();
      }
    }
  }

  async eliminarContacto() {
    try {
      if (this.idAsociacionActivo.length <= 0) {
        return;
      }

      const status: string = await this.participanteAsoNegocio
        .cambiarEstadoAsociacion(
          this.idAsociacionActivo,
          CodigoEstadoParticipanteAsociacion.ELIMINADO,
          this.perfilSeleccionado._id
        )
        .toPromise();

      if (!status) {
        throw new Error('');
      }

      const index = this.listaContactos.lista.findIndex(
        (e) => e.id === this.idAsociacionActivo
      );
      if (index >= 0) {
        this.listaContactos.lista.splice(index, 1);
      }

      this.toast.abrirToast('ELIMINACION_CORRECTA');
      this.idAsociacionActivo = '';
    } catch (error) {
      this.toast.abrirToast('text37');
    }
  }

  configurarDataNotificaciones() {
    this.dataNotificaciones = {
      codigoEntidad: CodigosCatalogoEntidad.MENSAJE,
      nivel: CodigosCatalogoEntidad.PERFIL,
      idPropietario: this.perfilSeleccionado._id,
      leido: false,
      limite: 15,
    };
  }

  async tratarNotificacion(notificacion: NotificacionFirebaseModel) {
    try {

      const mensaje: MensajeModelFirebase =
        notificacion.data as MensajeModelFirebase;
      const indexEnTratados = this.notificacionesTratadas.findIndex(
        (e) => e === notificacion.id
      );

      if (notificacion.leido) {
        return;
      }

      if (indexEnTratados >= 0) {
        return;
      }

      this.notificacionesTratadas.push(notificacion.id);

      const asociacion: AsociacionModel = await this.participanteAsoNegocio
        .obtenerParticipantesAsociacion(
          mensaje.conversacion.id,
          this.perfilSeleccionado._id
        )
        .toPromise();

      const indexUno = asociacion.participantes.findIndex(
        (e) => e.perfil._id === mensaje.propietario.perfil._id
      );


	  if (this.perfilSeleccionado._id === mensaje.propietario.perfil._id) {
		  return
	  }
    
      const perfil =
        indexUno >= 0
          ? asociacion.participantes[indexUno].perfil
          : mensaje.propietario.perfil;

      const notificacionItem: ItemNotificacion = {
        id: notificacion.id,
        idAsociacion: mensaje.conversacion.id,
        data: notificacion,
        item: this.metodosParaFotos.configurarItemListaContactoParaNotificacion(
          perfil,
          asociacion,
          this.obtenerContenidoDelMensaje(mensaje),
          (idAsociacion: string) => {
            this.abrirChatValidandoNotificaciones(idAsociacion);
          }
        ),
        notificaciones: [notificacion.id],
      };

      const indexNotiPrevia = this.confListaNotificaciones.lista.findIndex(
        (e) => e.idAsociacion === mensaje.conversacion.id
      );

      if (indexNotiPrevia < 0) {
        this.confListaNotificaciones.lista.unshift(notificacionItem);
      }

      if (indexNotiPrevia >= 0) {
        if (
          this.confListaNotificaciones.lista[
            indexNotiPrevia
          ].notificaciones.findIndex((e) => e === notificacion.id) < 0
        ) {
          this.confListaNotificaciones.lista[
            indexNotiPrevia
          ].notificaciones.push(notificacion.id);
        }

        const mensajeAntiguo = this.confListaNotificaciones.lista[
          indexNotiPrevia
        ].data.data as MensajeModelFirebase;
        const res = mensaje.fechaCreacion > mensajeAntiguo.fechaCreacion;
        if (res) {
          this.confListaNotificaciones.lista[
            indexNotiPrevia
          ].item.mensaje.mensaje = notificacionItem.item.mensaje.mensaje;
        }
      }

      this.confListaNotificaciones.contador =
        this.confListaNotificaciones.lista.length;
      this.confItemNotificaciones.mensaje.contadorMensajes =
        this.confListaNotificaciones.contador;
    } catch (error) {}
  }

  async configurarEscuchaNotificaciones() {
    this.notificacionService.subscripcionNotificaciones$ =
      this.notificacionService.notificaciones$.subscribe(
        (data) => {
          data.forEach((a) => {
            this.notificaciones.push({
              id: a.key,
              ...(a.payload.val() as Object),
            });
          });

          const aux = this.notificaciones.sort(
            (a, b) => a.fechaCreacion - b.fechaCreacion
          );
          this.notificaciones = aux.reverse();

          this.notificaciones.forEach((n) => {
            this.tratarNotificacion(n);
          });
        },
        (error) => {
          this.notificacionService.desconectarDeEscuchaNotificaciones();
        }
      );
  }

  obtenerContenidoDelMensaje(mensaje: MensajeModelFirebase) {
    switch (mensaje.tipo.codigo as CatalogoTipoMensaje) {
      case CatalogoTipoMensaje.TEXTO:
        return mensaje.contenido;
      default:
        return 'Ha enviado un archivo adjunto';
    }
  }

  async abrirChatValidandoNotificaciones(idAsociacion: string) {
    try {
      // Actualizar las notificaciones
      this.toast.abrirToast('', true);
      const index = this.confListaNotificaciones.lista.findIndex(
        (e) => e.idAsociacion === idAsociacion
      );

      if (index < 0) {
        this.irAlChat(idAsociacion);
        return;
      }

      const notificacion = this.confListaNotificaciones.lista[index];

      const querys = {};

      notificacion.notificaciones.forEach((idNotificacion) => {
        const path = idNotificacion;
        querys[path] = null;
      });

      const ref =
        'notificaciones/' +
        CodigosCatalogoEntidad.PERFIL +
        '/' +
        this.perfilSeleccionado._id;

      if (Object.keys(querys).length > 0) {
        await this.db.database.ref(ref).update(querys);
      }

      this.irAlChat(idAsociacion);
    } catch (error) {
      this.irAlChat(idAsociacion);
    }
  }

  irAlChat(idAsociacion: string) {
    if (!(idAsociacion.length > 0)) {
      return;
    }

    const ruta = RutasLocales.GAZING.toString();
    const chat = RutasGazing.CHAT_GAZE.toString().replace(':id', idAsociacion);
    this.router.navigateByUrl(ruta + '/' + chat);
  }
}

export interface ConfiguracionListaNotificaciones {
  contador: number;
  mostrar: boolean;
  lista: Array<ItemNotificacion>;
}

export interface ItemNotificacion {
  id: string;
  idAsociacion: string;
  data: {
    accion?: string;
    codEntidad?: CodigosCatalogoEntidad;
    data?: any;
    estado?: string;
    idEntidad?: string;
    leido?: boolean;
  };
  item: ConfiguracionItemListaContactosCompartido;
  notificaciones: Array<string>;
}
