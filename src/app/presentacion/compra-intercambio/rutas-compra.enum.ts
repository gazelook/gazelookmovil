export enum RutasCompra {
    COMPRA = 'compra',
    INTERCAMBIO = 'intercambio',
    INICIO = 'inicio',
    PUBLICAR = 'intercambio/publicar/:codigoTipoIntercambio',
    ACTUALIZAR = 'intercambio/actualizar/:id',
    VISITAR = 'intercambio/visitar/:id',
    LISTA_MIS_INTERCAMBIO = 'intercambio/lista-mis-intercambio/:codigoTipoIntercambio',
    BUSQUEDA_INTERCAMBIO_TIPO = 'intercambio/lista-intercambio/:codigoTipoIntercambio',
    
}