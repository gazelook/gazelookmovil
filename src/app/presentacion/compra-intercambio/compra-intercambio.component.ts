import { Component, OnInit } from '@angular/core';
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
@Component({
	selector: 'app-compra-intercambio',
	templateUrl: './compra-intercambio.component.html',
	styleUrls: ['./compra-intercambio.component.scss']
})
export class CompraIntercambioComponent implements OnInit {

	constructor(
		private notificacionesUsuario: NotificacionesDeUsuario,
		// private llamadaFirebaseService: LlamadaFirebaseService
	) {   }

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
		// this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
	}
}
