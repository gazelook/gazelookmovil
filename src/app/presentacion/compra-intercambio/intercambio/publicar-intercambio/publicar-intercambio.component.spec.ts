import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicarIntercambioComponent } from './publicar-intercambio.component';

describe('PublicarIntercambioComponent', () => {
  let component: PublicarIntercambioComponent;
  let fixture: ComponentFixture<PublicarIntercambioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicarIntercambioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicarIntercambioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
