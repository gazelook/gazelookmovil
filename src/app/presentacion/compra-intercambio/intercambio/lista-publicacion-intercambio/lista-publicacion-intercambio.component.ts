import { UsoItemIntercambio }
from '@shared/diseno/enums/uso-item-intercambio.enum';
import { ColorDeBorde, ColorDeFondo }
from '@shared/diseno/enums/estilos-colores-general';
import { CodigosCatalogoTipoAlbum }
from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { AlbumNegocio }
from 'dominio/logica-negocio/album.negocio';
import { AlbumModel }
from 'dominio/modelo/entidades/album.model';
import { ConfiguracionItemIntercambio }
from '@shared/diseno/modelos/item-intercambio.interface';
import { RutasCompra }
from 'src/app/presentacion/compra-intercambio/rutas-compra.enum';
import { RutasLocales }
from 'src/app/rutas-locales.enum';
import { CodigosCatalogoEntidad }
from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { ConfiguracionItemListaIntercambio }
from '@shared/diseno/modelos/item-lista-intercambio.interface';
import { IntercambioModel }
from 'dominio/modelo/entidades/intercambio.model';
import { IntercambioNegocio }
from 'dominio/logica-negocio/intercambio.negocio';
import { PaginacionModel }
from 'dominio/modelo/paginacion-model';
import { VariablesGlobales }
from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil }
from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { TamanoColorDeFondo }
from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar }
from '@shared/diseno/enums/uso-appbar.enum';
import { CodigosCatalogoTipoIntercambio }
from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { PerfilNegocio }
from 'dominio/logica-negocio/perfil.negocio';
import { CuentaNegocio }
from 'dominio/logica-negocio/cuenta.negocio';
import { EstiloDelTextoServicio }
from '@core/servicios/diseno/estilo-del-texto.service';
import { IntercambioParams }
from 'dominio/modelo/parametros/intercambio-params.interface';
import { PerfilModel }
from 'dominio/modelo/entidades/perfil.model';
import { ConfiguracionAppbarCompartida }
from '@shared/diseno/modelos/appbar.interface';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'app-lista-publicacion-intercambio',
    templateUrl: './lista-publicacion-intercambio.component.html',
    styleUrls: ['./lista-publicacion-intercambio.component.scss']
})
export class ListaPublicacionIntercambioComponent implements OnInit {


    // Parametros internos
    public sesionIniciada: boolean
    public perfilSeleccionado: PerfilModel
    public params: IntercambioParams
    // Configuracion hijos
    public confAppBar: ConfiguracionAppbarCompartida


    //listas
    public paginacionMisIntercambios: PaginacionModel<ConfiguracionItemListaIntercambio>
    public confCrearIntercambio: ConfiguracionItemListaIntercambio
    //interno
    public mostrarLoader: boolean
    public mostrarError: boolean
    public mensajeError: string
    public mostrarNoHayItems: boolean
    public puedeCargarMas: boolean
    public mostrarCargandoPequeno: boolean


    constructor(
        public estiloDelTextoServicio: EstiloDelTextoServicio,
        private cuentaNegocio: CuentaNegocio,
        private perfilNegocio: PerfilNegocio,
        private router: Router,
        private route: ActivatedRoute,
        private _location: Location,
        private variablesGlobales: VariablesGlobales,
        private intercambioNegocio: IntercambioNegocio,
        private albumNegocio: AlbumNegocio,

    ) {

        this.params = { estado: false }
        this.mostrarLoader = false
        this.mostrarError = false
        this.mensajeError = ''
        this.puedeCargarMas = true
        this.mostrarNoHayItems = false
        this.mostrarCargandoPequeno = false
    }

    ngOnInit(): void {

        this.variablesGlobales.mostrarMundo = false
        this.configurarEstadoSesion()
        this.configurarPerfilSeleccionado()
        this.configurarParametros()
        this.configurarLista()
        if (this.params.estado && this.perfilSeleccionado) {
            this.configurarAppBar()
            this.listarMisIntercambios()

        } else {
        }
    }


    configurarEstadoSesion() {
        this.sesionIniciada = this.cuentaNegocio.sesionIniciada()
    }

    configurarPerfilSeleccionado() {
        this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
    }

    configurarParametros() {
        const urlParams: Params = this.route.snapshot.params

        if (urlParams.codigoTipoIntercambio) {
            this.params.codigoTipoIntercambio = urlParams.codigoTipoIntercambio as
            CodigosCatalogoTipoIntercambio
            this.params.estado = true
        }
    }

    configurarLista() {
        this.paginacionMisIntercambios = {
            lista: [],
            paginaActual: 1,
            proximaPagina: true,
            totalDatos: 0,
        }
    }

    configurarAppBar() {


        let llaverTextoTipoIntercambio: string
        if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.ALOJAMIENTO) {
            llaverTextoTipoIntercambio = 'm5v1texto2.2'
        }
        if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.CULTURAL) {
            llaverTextoTipoIntercambio = 'm5v1texto2'
        }
        if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.HABILIDADES) {
            llaverTextoTipoIntercambio = 'm5v1texto2.1'
        }
        if (this.params.codigoTipoIntercambio === CodigosCatalogoTipoIntercambio.HABILIDADES) {
            llaverTextoTipoIntercambio = 'm5v1texto2.3'
        }
        this.confAppBar = {
            accionAtras: () => {
                this._location.back()
            },
            usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
            searchBarAppBar: {
                mostrarDivBack: {
                    icono: true,
                    texto: true
                },
                mostrarLineaVerde: true,
                mostrarTextoHome: true,
                mostrarBotonXRoja: false,
                tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
                nombrePerfil: {
                    mostrar: true,
                    llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
                      this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
                },
                buscador: {
                    mostrar: false,
                    configuracion: {
                        disable: true,

                    }
                },
                subtitulo: {
                    mostrar: true,
                    llaveTexto: llaverTextoTipoIntercambio
                },

            }

        }

    }

    async listarMisIntercambios() {
        if (!this.paginacionMisIntercambios.proximaPagina) {
            this.puedeCargarMas = false
            this.mostrarLoader = false
            this.mostrarCargandoPequeno = false
            return
        }

        try {
            this.mostrarNoHayItems = false
            this.mostrarLoader = (this.paginacionMisIntercambios.lista.length === 0)
            this.mostrarCargandoPequeno = (this.paginacionMisIntercambios.lista.length > 0)

            const dataPaginacion = await this.intercambioNegocio.buscarMisIntercambiosTipo(
                this.params.codigoTipoIntercambio,
                this.perfilSeleccionado._id,
                10,
                this.paginacionMisIntercambios.paginaActual
            ).toPromise()

            this.paginacionMisIntercambios.totalDatos = dataPaginacion.totalDatos
            this.paginacionMisIntercambios.proximaPagina = dataPaginacion.proximaPagina


            dataPaginacion.lista.forEach(intercambio => {
                const index: number = this.paginacionMisIntercambios.lista.findIndex(
                  e => e.intercambio.id === intercambio.id)
                if (index < 0) {
                    this.paginacionMisIntercambios.lista.push(
                        this.configurarItemIntercambios(intercambio)


                    )
                }

            })

            this.mostrarLoader = false
            this.mostrarCargandoPequeno = false
            this.puedeCargarMas = true
            this.mostrarNoHayItems = (this.paginacionMisIntercambios.lista.length === 0)

            if (this.paginacionMisIntercambios.proximaPagina) {
                this.paginacionMisIntercambios.paginaActual += 1
            }
        } catch (error) {
            this.mostrarLoader = false
            this.mostrarCargandoPequeno = false
            this.puedeCargarMas = false
        }
    }

    configurarItemIntercambios(
        intercambio: IntercambioModel
    ): ConfiguracionItemListaIntercambio {
        return {
            codigoEntidad: CodigosCatalogoEntidad.INTERCAMBIO,
            intercambio: intercambio,
            configuracionRectangulo: this.obtenerConfiguracionRectangulo(
              intercambio),
            eventoTap: (id: string) => {
                if (!id || id.length === 0) {
                    return
                }

                const modulo = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
                let ruta = RutasCompra.ACTUALIZAR.toString()
                ruta = ruta.replace(':id', id)
                this.router.navigateByUrl(modulo + '/' + ruta)
                return
            }
        }
    }

    configurarITemCrearIntercambio() {
        this.confCrearIntercambio = {
            codigoEntidad: CodigosCatalogoEntidad.INTERCAMBIO,
            configuracionRectangulo: {
                usoVersionMini: true,
                id: '',
                colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
                colorDeFondo: ColorDeFondo.FONDO_BLANCO,
                fecha: {
                    mostrar: false,
                },
                etiqueta: {
                    mostrar: false,
                },
                titulo: {
                    mostrar: false,
                },
                urlMedia: '',
                usoItem: UsoItemIntercambio.SOLO_TEXTO,
                loader: true,
                eventoTap: {
                    activo: false,
                },
                eventoDobleTap: {
                    activo: false
                },
                eventoPress: {
                    activo: false
                }
            },
            eventoTap: () => {

            }
        }
    }

    obtenerConfiguracionRectangulo(
        entidad: any,
        predeterminado: boolean = false
    ): ConfiguracionItemIntercambio {
        let album: AlbumModel

        if (predeterminado) {
            album = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(
              entidad.adjuntos)
        }

        if (!album) {
            album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
                CodigosCatalogoTipoAlbum.GENERAL,
                entidad.adjuntos
            )
        }

        const urlMedia = this.obtenerUrlMedia(album)

        return {
            usoVersionMini: true,
            id: entidad.id,
            colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
            colorDeFondo: ColorDeFondo.FONDO_BLANCO,
            fecha: {
                mostrar: false,
            },
            etiqueta: {
                mostrar: false,
            },
            titulo: {
                mostrar: false,
            },
            urlMedia: urlMedia,
            usoItem: UsoItemIntercambio.REC_INTERC,
            loader: (urlMedia.length > 0),
            eventoTap: {
                activo: false,
            },
            eventoDobleTap: {
                activo: false
            },
            eventoPress: {
                activo: false
            }
        }
    }

    obtenerUrlMedia(
        album: AlbumModel
    ): string {
        try {

            if (!album.portada) {
                throw new Error('')
            }

            if (album.portada.miniatura) {
                return album.portada.miniatura.url
            }

            if (album.portada.principal) {
                return album.portada.principal.url
            }

            return ''
        } catch (error) {
            return ''
        }
    }

}
