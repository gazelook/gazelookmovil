import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPublicacionIntercambioComponent } from './lista-publicacion-intercambio.component';

describe('ListaPublicacionIntercambioComponent', () => {
  let component: ListaPublicacionIntercambioComponent;
  let fixture: ComponentFixture<ListaPublicacionIntercambioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPublicacionIntercambioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPublicacionIntercambioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
