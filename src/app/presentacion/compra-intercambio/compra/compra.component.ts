import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import {
  ColorDelTexto, ColorFondoLinea,
  EspesorLineaItem, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.scss']
})
export class CompraComponent implements OnInit {
  configuracionAppBar: ConfiguracionAppbarCompartida;
  perfilSeleccionado: PerfilModel;
  confLinea: LineaCompartida;

  //botones
  botonCompra: BotonCompartido
  botonIntercambio: BotonCompartido

  botonBuscarCultural: BotonCompartido;
  botonPublicarCultural: BotonCompartido;

  constructor(
    private perfilNegocio: PerfilNegocio,
    public estiloTexto: EstiloDelTextoServicio,
    private _location: Location,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private variablesGlobales: VariablesGlobales
  ) { }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = false
    this.obtenerPerfil()

    if (this.perfilSeleccionado) {
      this.prepararAppBar()
      this.estiloTituloPrincipal()
      this.configuracionBotones()
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }

  obtenerPerfil() {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  prepararAppBar() {
    this.configuracionAppBar = {
      accionAtras: () => {
        this._location.back()
      },
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        mostrarDivBack: {
          icono: true,
          texto: true
        },
        mostrarLineaVerde: true,
        mostrarTextoHome: true,
        mostrarBotonXRoja: false,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,

          }
        },
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v10texto1'
        },
      }
    }
  }

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false
    }
  }

  estiloTituloPrincipal() {
    return this.estiloTexto.obtenerEstilosTexto({
      color: ColorDelTexto.TEXTOBLANCO,
      estiloTexto: EstilosDelTexto.BOLD,
      enMayusculas: true,
      tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I7
    })
  }

  async configuracionBotones() {
    this.botonBuscarCultural = {
      text: await this.internacionalizacionNegocio.obtenerTextoLlave('m6v5texto5'),
      tamanoTexto: TamanoDeTextoConInterlineado.L3_I1,
      colorTexto: ColorTextoBoton.AMARRILLO,
      ejecutar: () => { },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };

    this.botonPublicarCultural = {
      text: await this.internacionalizacionNegocio.obtenerTextoLlave('m6v5texto6'),
      tamanoTexto: TamanoDeTextoConInterlineado.L3_I1,
      colorTexto: ColorTextoBoton.AMARRILLO,
      ejecutar: () => { },
      enProgreso: false,
      tipoBoton: TipoBoton.TEXTO,
    };
  }
}
