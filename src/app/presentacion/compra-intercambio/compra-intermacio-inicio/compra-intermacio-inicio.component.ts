import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { RutasCompra } from 'src/app/presentacion/compra-intercambio/rutas-compra.enum';



@Component({
	selector: 'app-compra-intermacio-inicio',
	templateUrl: './compra-intermacio-inicio.component.html',
	styleUrls: ['./compra-intermacio-inicio.component.scss']
})
export class CompraIntermacioInicioComponent implements OnInit {

	public confAppBar: ConfiguracionAppbarCompartida;
	public botonCompra: BotonCompartido
	public botonIntercambio: BotonCompartido
	public confLinea: LineaCompartida
	public perfilSeleccionado: PerfilModel
	public configDialogoLateral: DialogoCompartido


	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private perfilNegocio: PerfilNegocio,
		private router: Router,
		private internacionalizacionNegocio: InternacionalizacionNegocio,
		private _location: Location,
		private translateService: TranslateService,
		private variablesGlobales: VariablesGlobales
	) {

	}

	ngOnInit(): void {
		this.variablesGlobales.mostrarMundo = true
		this.configurarPerfilSeleccionado()
		this.variablesGlobales.mostrarMundo = false
		if (this.perfilSeleccionado) {
			this.configurarAppBar()
			this.configuracionBotones()
			this.configurarLinea()
			this.configurarDialogoDemo()
		} else {
			this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
		}
	}

	configurarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	configurarAppBar() {
		this.confAppBar = {
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			accionAtras: () => {
				this.accionAtras()
			},
			searchBarAppBar: {
				tamanoColorFondo: TamanoColorDeFondo.TAMANO6920,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
				},
				mostrarTextoHome: true,
				mostrarDivBack: {
					icono: true,
					texto: true,
				},
				mostrarLineaVerde: true,
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm6v4texto1'
				},
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true,
					}
				}
			},
		}
	}

	configurarDialogoDemo() {
		this.configDialogoLateral = {
			mostrarDialogo: true,
			tipo: TipoDialogo.INFO_VERTICAL,
			completo: true,
			descripcion: 'm6v4texto11',
		}
	}

	// Configurar linea verde
	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6028,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR012,
			forzarAlFinal: false
		}
	}

	async configuracionBotones() {
		this.botonIntercambio = {
			text: 'm6v4texto9',
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			colorTexto: ColorTextoBoton.CELESTE,
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
			ejecutar: () => {
				const ruta = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
				const intercambio = RutasCompra.INTERCAMBIO.toString()
				this.router.navigateByUrl(ruta + '/' + intercambio)
			},
		};

		this.botonCompra = {
			text: 'm6v4texto10',
			tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
			colorTexto: ColorTextoBoton.AMARRILLO,
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
			ejecutar: () => {
				const ruta = RutasLocales.COMPRAS_INTERCAMBIOS.toString()
				const compra = RutasCompra.COMPRA.toString()
				this.router.navigateByUrl(ruta + '/' + compra)
			},
		};
	}

	accionAtras() {
		this._location.back()
	}
}
