import { IntercambioComponent } from 'src/app/presentacion/compra-intercambio/intercambio/intercambio.component';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompraIntercambioRoutingModule } from 'src/app/presentacion/compra-intercambio/compra-intercambio-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from '@shared/compartido.module';
import { CompraIntermacioInicioComponent } from 'src/app/presentacion/compra-intercambio/compra-intermacio-inicio/compra-intermacio-inicio.component';
import { CompraComponent } from 'src/app/presentacion/compra-intercambio/compra/compra.component';
import { CompraIntercambioComponent } from 'src/app/presentacion/compra-intercambio/compra-intercambio.component';
import { BuscadorIntercambioComponent } from 'src/app/presentacion/compra-intercambio/intercambio/buscador-intercambio/buscador-intercambio.component';
import { PublicarIntercambioComponent } from 'src/app/presentacion/compra-intercambio/intercambio/publicar-intercambio/publicar-intercambio.component';
import { ListaPublicacionIntercambioComponent } from 'src/app/presentacion/compra-intercambio/intercambio/lista-publicacion-intercambio/lista-publicacion-intercambio.component';


@NgModule({
  declarations: [
    CompraIntercambioComponent,
    CompraIntermacioInicioComponent,
    CompraComponent,
    BuscadorIntercambioComponent,
    PublicarIntercambioComponent,
    IntercambioComponent,
    ListaPublicacionIntercambioComponent
  ],
  imports: [
    CommonModule,
    CompartidoModule,
    CompraIntercambioRoutingModule,
    TranslateModule,
    FormsModule,
    CompartidoModule
  ],
  exports: [
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class CompraIntercambioModule { }
