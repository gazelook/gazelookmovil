import { LandingNegocio } from './../../dominio/logica-negocio/landing.negocio';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { CodigosCatalogoIdioma } from '@core/servicios/remotos/codigos-catalogos/catalogo-idioma.enum';
import { IdiomaNegocio } from 'dominio/logica-negocio/idioma.negocio';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { LlavesSessionStorage } from '@core/servicios/locales/llaves/session-storage.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  ElementRef,
  ViewChild,
  VERSION,
} from '@angular/core';
import { Location } from '@angular/common';
import { FormularioContactanos } from 'dominio/modelo/entidades/fomulario-contactanos.model';

import { ColorTextoBoton } from 'src/app/compartido/componentes/button/button.component';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from 'src/app/compartido/diseno/modelos/boton.interface';
import { TipoBoton } from '@shared/componentes';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit, AfterViewInit {
  name = 'Angular ' + VERSION.major;
  @ViewChild('myNameElem') myNameElem: ElementRef;

  public enContactos: boolean;
  public mensajeCorreo: boolean;
  public errorMsj: boolean;
  public contactUs: boolean;
  public idiomas: string;
  public paginas: number;
  public menu: number;
  public leermas: number;
  public mostrarlineaverde: boolean;
  public menusyaabiertos: Array<number>;
  public preguntaAbierta: number;
  public paginaAnterior: number;
  public nombre: string;
  public email: string;
  public pais: string;
  public direccion: string;
  public tema: string;
  public mensaje: string;
  public mostrarIdiomas: boolean;
  public confToast: ConfiguracionToast;

  public configBotonPagoExtra: BotonCompartido;

  //variable para boton PWA
  public promptEvent;

  public texto1: string;
  public texto2: string;
  public texto3: string;
  public textoRotativo: string;
  public contador: number;
  public contador2: number;
  public contador3: number;
  public interval: any;
  public interval2: any;
  public interval8: any;
  public tiempoIntervalo: number;
  public tiempoIntervalo2: number;
  public tiempoIntervalo8: number;
  

  public posiciones: Array<{
    id: string;
    pos: number;
    elemento: HTMLElement;
    altura: number;
  }>;
  public textoMensaje: string;
  public textoMensajeError: string;
  public mostrarTexto1: number;

  //textos
  public landingCambiosUrgentes: string;
  public landingCambiosUrgentes2: string;
  public landingCambiosUrgentes3: string;
  public landingCambiosUrgentes4: string;
  public landingCambiosUrgentes5: string;
  public aText = new Array(
    'There are only 10 types of people in the world:',
    "Those who understand binary, and those who don't"
  );

  public iSpeed = 100; // time delay of print out

  public iIndex = 0; // start printing array at this posision

  public iArrLength = this.aText[0].length; // the length of the text array

  public iScrollAt = 20; // start scrolling up at this many lines

  public iTextPos = 0; // initialise text position

  public sContents = ''; // initialise contents variable

  public iRow; // initialise current row

  public aparecer2: boolean;
  public timeOutAparecer2: any;

  public cssTyping: boolean;
  public cssTyping2: boolean;

  public aparecer3: boolean;
  public timeOutAparecer3: any;

  public aparecer4: boolean;
  public timeOutAparecer4: any;

  public aparecer5: boolean;
  public timeOutAparecer5: any;

  public aparecer6: boolean;
  public timeOutAparecer6: any;

  public aparecer7: boolean;
  public timeOutAparecer7: any;

  public aparecer8: boolean;
  public timeOutAparecer8: any;

  public desaparecerMar: boolean;
  public timeOutDesaparecerMar: any;

  public mostrarVisitaDemo: boolean;
  public imagenCargada: boolean;
  public imagenCargada2: boolean;
  public imagenCargada3: boolean;

  constructor(
    public variablesGlobales: VariablesGlobales,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private translateService: TranslateService,
    private cuentaNegocio: CuentaNegocio,
    private idiomaNegocio: IdiomaNegocio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private router: Router,
    private _location: Location,
    private _elemRef: ElementRef,
    private landingNegocio: LandingNegocio
  ) {
    this.textoMensaje = '';
    this.textoMensajeError = '';
    this.mensajeCorreo = false;
    this.contactUs = true;
    this.enContactos = false;
    this.idiomas = 'en';
    this.paginas = 0;
    this.menu = -1;
    this.leermas = -1;
    this.mostrarlineaverde = false;
    this.mostrarIdiomas = true;
    this.preguntaAbierta = -1;
    this.menusyaabiertos = [];
    this.posiciones = [];

    this.nombre = '';
    this.email = '';
    this.pais = '';
    this.direccion = '';
    this.tema = '';
    this.mensaje = '';
    this.paginaAnterior = 0;
    this.errorMsj = false;
    this.texto1 = '';
    this.texto2 = '';
    this.texto3 = '';
    this.textoRotativo = '';
    this.contador = 1;
    this.contador2 = 1;
    this.contador3 = 1;
    this.tiempoIntervalo = 13000;
    this.tiempoIntervalo2 = 12000;
    this.tiempoIntervalo8 = 20000;
    this.mostrarTexto1 = 1;
    this.aparecer2 = false;
    this.aparecer3 = false;
    this.aparecer4 = false;
    this.aparecer5 = false;
    this.aparecer6 = false;
    this.aparecer7 = false;
    this.aparecer8 = false;
    this.desaparecerMar = false;
    this.mostrarVisitaDemo = false;
    this.imagenCargada = true;
    this.imagenCargada2 = true;
    this.imagenCargada3 = true;
    this.cssTyping = true;
    this.cssTyping2 = true;
  }

  ngOnInit(): void {
    this.configurarImgCarga();
    this.verificarPagina();
   

    this.configurarVariablesGlobales();
    this.validarRutaDeContactos();
    this.detectarDispositivo();
    this.obtenerTextosRotativosVerde();
    this.obtenerTextoSegundaPagina();
    
 
    this.configurarVariablesDesdeLocalStorage();
    this.validarIdiomaInicial();

    this.configurarToast();
    this.configurarBoton();
    this.detectarDispositivo();
    this.textoAEscribir();

    if (this.paginas !== 0) {
      this.mostrarIdiomas = false;
    }

    this.configurarImgCargaMarcos();
  }

  verificarPagina() {
    let a = sessionStorage.getItem(LlavesSessionStorage.PAGINAS);
    console.log('AAAAAAAAAA', a);
    if (a === '1') {
      clearInterval(this.interval2);
      this.contador2 = 1;
      this.intervalo2();
    }

    if (a === '0') {
      this.configurarImgCargaMarcos();
    }


    let animFin = sessionStorage.getItem(LlavesSessionStorage.ANIMACION_FINALIZADA);

    if (animFin !== '1') {
      this.intervalo();
      this.intervaloAparecer();;
    }
    if (animFin === '1') {
      console.log('ya la visite');
      
      this.aparecer2 = true;
      this.aparecer3 = true;
      this.aparecer4 = true;
      this.aparecer5 = true;
      this.aparecer6 = true;
      this.aparecer7 = true;
      this.aparecer8 = true;
      this.cssTyping = false
      this.cssTyping2 = false;

      this.intervalo8()
    }

 

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.variablesGlobales.mostrarCapaGif = false;
      this.variablesGlobales.mostrarGif = false;
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.detectarDispositivo();
  }

  configurarVariablesGlobales() {
    this.variablesGlobales.mostrarMundo = false;
  }

  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(event: Event) {
    event.preventDefault();
    this.promptEvent = event;
  }

  public instalarPWA() {
    this.promptEvent.prompt();
  }

  public shouldInstall(): boolean {
    return !this.isRunningStandalone() && this.promptEvent;
  }

  public isRunningStandalone(): boolean {
    return window.matchMedia('(display-mode: standalone)').matches;
  }

  async obtenerTextosRotativosVerde() {
    this.texto1 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingTextoRotativo1'
    );
    this.texto2 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingTextoRotativo2'
    );
    this.texto3 = await this.internacionalizacionNegocio.obtenerTextoLlave(
      'landingTextoRotativo3'
    );
    this.textoRotativo = this.texto1;
  }

  async obtenerTextoSegundaPagina() {
    let a = await this.landingNegocio
      .mostrarTextoLandingSegundaPagina()
      .toPromise();
    // this.mostrarTexto1 = a
  }

  async textoAEscribir() {
    this.landingCambiosUrgentes =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes'
      );
    this.landingCambiosUrgentes2 =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes2'
      );
    this.landingCambiosUrgentes3 =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes3'
      );
    this.landingCambiosUrgentes4 =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes4'
      );
      this.landingCambiosUrgentes5 =
      await this.internacionalizacionNegocio.obtenerTextoLlave(
        'landingCambiosUrgentes5'
      );
  }

  imgenCargada(event): void {
    if (!event && !event.target) {
      return;
    }
    const x = event.target.x;
    const y = event.target.y;
    if (x === 0 && y === 0) {
      const width = event.target.width;
      const height = event.target.height;
      const portrait = height > width;
    }
    this.imagenCargada = false;
  }

  imgenCargada2(event): void {
    if (!event && !event.target) {
      return;
    }
    const x = event.target.x;
    const y = event.target.y;
    if (x === 0 && y === 0) {
      const width = event.target.width;
      const height = event.target.height;
      const portrait = height > width;
    }
    this.imagenCargada2 = false;
  }

  imgenCargada3(event): void {
    if (!event && !event.target) {
      return;
    }
    const x = event.target.x;
    const y = event.target.y;
    if (x === 0 && y === 0) {
      const width = event.target.width;
      const height = event.target.height;
      const portrait = height > width;
    }
    this.imagenCargada3 = false;
  }

  getValue() {}

  intervalo8() {


    this.interval8 = setInterval(() => {
      if (this.contador3 === 1) {
        this.tiempoIntervalo8 = 12000;
        this.contador3 = 2;
        clearInterval(this.interval8);
        this.intervalo8();
        return;
      }

      if (this.contador3 === 2) {
        this.tiempoIntervalo8 = 12000;
        this.contador3 = 1;
        clearInterval(this.interval8);
        this.intervalo8();
        return;
      }
    }, this.tiempoIntervalo8);


  }

  intervalo() {
    this.interval = setInterval(() => {
      if (this.contador === 1) {
        this.textoRotativo = this.texto2;
        this.tiempoIntervalo = 13000;
        this.contador = 2;
        clearInterval(this.interval);
        this.intervalo();
        return;
      }

      if (this.contador === 2) {
        this.textoRotativo = this.texto3;
        this.tiempoIntervalo = 13000;
        this.contador = 3;
        clearInterval(this.interval);
        this.intervalo();
        return;
      }

      if (this.contador === 3) {
        this.textoRotativo = this.texto1;
        this.tiempoIntervalo = 43000;
        this.contador = 1;
        clearInterval(this.interval);
        this.intervalo();
        return;
      }
    }, this.tiempoIntervalo);
  }

  intervalo2() {
    this.interval2 = setInterval(() => {
      if (this.contador2 === 1) {
        this.tiempoIntervalo2 = 12000;

        this.contador2 = 2;
        clearInterval(this.interval2);
        this.intervalo2();
        return;
      }

      // if (this.contador2 === 2) {
      //   this.tiempoIntervalo2 = 9000;
      //   this.contador2 = 3;
      //   clearInterval(this.interval2);
      //   this.intervalo2();
      //   return;
      // }

      if (this.contador2 === 2) {
        this.tiempoIntervalo2 = 12000;
        this.contador2 = 1;
        clearInterval(this.interval2);
        this.intervalo2();
        return;
      }
    }, this.tiempoIntervalo2);
  }

  intervaloAparecer() {
    this.timeOutAparecer2 = setTimeout(() => {
      this.aparecer2 = true;
      this.cssTyping = false;
    }, 15000);

    this.timeOutAparecer3 = setTimeout(() => {
      this.aparecer3 = true;
    }, 18000);

    // this.timeOutAparecer4 = setTimeout(() => {
    //   this.aparecer4 = true

    // }, 12000)

    this.timeOutDesaparecerMar = setTimeout(() => {
      this.aparecer4 = false;
      this.mostrarVisitaDemo = true;
    }, 22000);

    this.timeOutAparecer5 = setTimeout(() => {
      this.aparecer5 = true;
    }, 19000);

    this.timeOutAparecer6 = setTimeout(() => {
      this.contador3 = 2;
      this.intervalo8();
      this.aparecer6 = true;
      this.cssTyping2 = false;
    }, 29000);

    this.timeOutAparecer7 = setTimeout(() => {
      this.aparecer7 = true;
      sessionStorage.setItem(
        LlavesSessionStorage.ANIMACION_FINALIZADA,
        '1'
      );
    }, 30500);

    this.timeOutAparecer8 = setTimeout(() => {
      this.aparecer8 = true;
    }, 30500);
  }

  configurarImgCargaMarcos() {
    // CLAVO //
    let elementoImgClavo: HTMLImageElement;
    elementoImgClavo = document.createElement('img');
    elementoImgClavo.src = '../.././../assets/recursos/clavo_pared_1.svg';
    elementoImgClavo.onload = () => {
      const elemento = document.getElementById('clavo-pared') as HTMLElement;

      elemento.style.backgroundImage = 'url(' + elementoImgClavo.src + ')';
      elemento.style.backgroundRepeat = 'no-repeat';
      elemento.style.width = '35vw';
      elemento.style.height = '30vw';
    };

    // CORDON IZQ //
    let elementoImgCordIzq: HTMLImageElement;
    elementoImgCordIzq = document.createElement('img');
    elementoImgCordIzq.src = '../.././../assets/recursos/cordon_cuadro_1.svg';
    elementoImgCordIzq.onload = () => {
      const elementoCordIzq = document.getElementById(
        'cordon-izquierdo'
      ) as HTMLElement;

      elementoCordIzq.style.backgroundImage =
        'url(' + elementoImgCordIzq.src + ')';
      elementoCordIzq.style.backgroundRepeat = 'no-repeat';
      elementoCordIzq.style.backgroundSize = '100% 100%';
      elementoCordIzq.style.width = '70vw';
      elementoCordIzq.style.height = '41vw';
    };

    // CORDON DER //
    let elementoImgCordDer: HTMLImageElement;
    elementoImgCordDer = document.createElement('img');
    elementoImgCordDer.src = '../.././../assets/recursos/cordon_cuadro_1.svg';
    elementoImgCordDer.onload = () => {
      const elementoCordDer = document.getElementById(
        'cordon-derecho'
      ) as HTMLElement;

      elementoCordDer.style.backgroundImage =
        'url(' + elementoImgCordDer.src + ')';
      elementoCordDer.style.backgroundRepeat = 'no-repeat';
      elementoCordDer.style.width = '57vw';
      elementoCordDer.style.height = '48vw';
    };

    // CLAVO //
    let elementoImgMarco: HTMLImageElement;
    elementoImgMarco = document.createElement('img');
    elementoImgMarco.src = '../.././../assets/recursos/marco_verde_1.png';
    elementoImgMarco.onload = () => {
      const elementoMarco = document.getElementById(
        'marco-madera'
      ) as HTMLElement;

      elementoMarco.style.backgroundImage = 'url(' + elementoImgMarco.src + ')';
      elementoMarco.style.backgroundRepeat = 'no-repeat';
      elementoMarco.style.width = '80vw';
      elementoMarco.style.height = '85vw';
      elementoMarco.style.backgroundSize = '100% 100%';
      elementoMarco.style.marginLeft = '10vw';
      elementoMarco.style.transform = 'rotate(356deg)';
      elementoMarco.style.backgroundColor = '#8f5c84';
    };
  }
  configurarImgCarga() {
    let elementoImg: HTMLImageElement;

    elementoImg = document.createElement('img');

    if (this.paginas === 0) {
      let elemento = document.getElementById(
        'contenedor-landing'
      ) as HTMLElement;
      elemento.style.backgroundImage = null;
    }
    if (this.paginas === 1) {
      elementoImg.src =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/fondo-2-web.jpg';
      elementoImg.onload = () => {
        let elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
        elemento.style.backgroundSize = 'cover';
      };
    }
    if (this.paginas === 2) {
      elementoImg.src =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/fondo-3-web.jpg';
      elementoImg.onload = () => {
        let elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
        elemento.style.backgroundSize = 'cover';
      };
    }
    if (this.paginas === 3) {
      elementoImg.src =
        'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/fondo-web-4.jpg';
      elementoImg.onload = () => {
        let elemento = document.getElementById(
          'contenedor-landing'
        ) as HTMLElement;
        elemento.style.backgroundImage = 'url(' + elementoImg.src + ')';
        elemento.style.backgroundSize = 'cover';
      };
    }
  }

  //cambio de michel y andre solo ingles y espanol
  async validarIdiomaInicial() {
    let idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
      CodigosCatalogoIdioma.INGLES
    );
    const idiomaSeleccionado = this.idiomaNegocio.obtenerIdiomaSeleccionado();

    if (idiomaSeleccionado && idiomaSeleccionado !== null) {
      idioma = idiomaSeleccionado;
    }

    this.idiomas = idioma.codNombre.toLowerCase();
    this.internacionalizacionNegocio.usarIidoma(
      idioma.codNombre.toLocaleLowerCase()
    );
    this.idiomaNegocio.guardarIdiomaSeleccionado(idioma);
    this.idiomaNegocio.eliminarVarablesStorage();
  }

  detectarDispositivo() {
    if (
      !/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      document.location.href = 'https://gazelook.com/web';
    }
  }

  validarRutaDeContactos() {
    if (this.router.url.indexOf(RutasLocales.FORM_CONTACTO.toString()) >= 0) {
      this.enContactos = true;
      this.eventoclick(4);
    }
  }

  // como hacer click en paginas diferentes para MENU
  eventoclick(pagina: number) {
    clearInterval(this.interval2);
    this.intervalo2();
    if (pagina === 0) {
      this.mostrarIdiomas = true;
      this.configurarImgCargaMarcos();
    }

    if (pagina === 1) {
      clearInterval(this.interval2);
      this.contador2 = 1;
      this.intervalo2();
    }

    this.paginaAnterior = this.paginas;
    if (pagina !== 0) {
      this.mostrarIdiomas = false;
    }

    if (pagina === 4) {
      this.contactUs = true;
      this.mensajeCorreo = false;
      this.nombre = '';
      this.email = '';
      this.pais = '';
      this.direccion = '';
      this.tema = '';
      this.mensaje = '';
      this.errorMsj = false;
    }

    this.paginas = pagina;

    this.menu = -1;
    this.leermas = -1;
    this.menusyaabiertos = [];
    this.configurarImgCarga();
    // update local storage
    sessionStorage.setItem(
      LlavesSessionStorage.PAGINAS,
      this.paginas.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENU_ACTIVO,
      this.menu.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS,
      JSON.stringify(this.menusyaabiertos)
    );
  }

  eventomenu(menu: number) {
    this.preguntaAbierta = -1;
    const index = this.menusyaabiertos.findIndex((e) => e === menu);
    if (index < 0) {
      this.menusyaabiertos.push(menu);
    }
    if (menu < 15) {
      this.validarScroll(this.menu, menu, menu < 9 ? 0 : 9, menu < 9 ? 9 : 14);
    }

    if (this.menu === menu) {
      this.menu = -1;
    } else {
      this.menu = menu;
    }

    // update local storage
    sessionStorage.setItem(
      LlavesSessionStorage.MENU_ACTIVO,
      this.menu.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS,
      JSON.stringify(this.menusyaabiertos)
    );
  }

  validarScroll(viejo: number, nuevo: number, inicio: number, final: number) {
    let body: HTMLElement = document.getElementById('bodylinea') as HTMLElement;
    const posicionesNuevas: Array<{
      id: string;
      pos: number;
      elemento: HTMLElement;
      altura: number;
    }> = [];

    let aux = inicio;
    let idBase = 'menuinterno_';

    while (aux < final) {
      const elemento = document.getElementById(idBase + aux) as HTMLElement;

      posicionesNuevas.push({
        id: idBase + aux,
        pos: elemento.offsetTop,
        elemento: elemento,
        altura: elemento.clientHeight,
      });
      aux++;
    }

    if (viejo === -1) {
      this.posiciones = posicionesNuevas;
    }

    if (inicio < 9) {
      if (nuevo == 0) {
        this.posiciones = posicionesNuevas;
        body.scrollTop = this.posiciones[nuevo].pos - 115;
      } else {
        if (nuevo > viejo) {
          if (nuevo === viejo + 1) {
            body.scrollTop = posicionesNuevas[nuevo - 1].pos - 50;
          } else {
            body.scrollTop = this.posiciones[nuevo - 1].pos - 50;
          }
        } else {
          body.scrollTop = posicionesNuevas[nuevo - 1].pos - 50;
        }
      }
    } else if (inicio >= 9 && inicio < 14) {
      if (nuevo == 9) {
        this.posiciones = posicionesNuevas;
        body.scrollTop = this.posiciones[nuevo - 9].pos - 115;
      } else {
        if (nuevo > viejo) {
          if (nuevo === viejo + 1) {
            body.scrollTop = posicionesNuevas[nuevo - 10].pos - 120;
          } else {
            body.scrollTop = this.posiciones[nuevo - 10].pos - 120;
          }
        } else {
          body.scrollTop = posicionesNuevas[nuevo - 10].pos - 120;
        }
      }
    } else if (inicio >= 15 && inicio < 19) {
      if (nuevo == 15) {
        this.posiciones = posicionesNuevas;
        body.scrollTop = this.posiciones[nuevo - 14].pos - 115;
      } else {
        if (nuevo > viejo) {
          if (nuevo === viejo + 1) {
            body.scrollTop = posicionesNuevas[nuevo - 15].pos - 120;
          } else {
            body.scrollTop = this.posiciones[nuevo - 15].pos - 120;
          }
        } else {
          body.scrollTop = posicionesNuevas[nuevo - 15].pos - 120;
        }
      }
    }
  }

  eventoleermas(leermas: number) {
    if (this.leermas === leermas) {
      this.leermas = -1;
    } else {
      this.leermas = leermas;
    }
  }

  iralagranja(limpiarEstado: boolean = false) {
    if (limpiarEstado) {
      sessionStorage.setItem(LlavesSessionStorage.PAGINAS, '0');
      sessionStorage.setItem(LlavesSessionStorage.MENU_ACTIVO, '-1');
      sessionStorage.setItem(
        LlavesSessionStorage.MENUS_YA_ABIERTOS,
        JSON.stringify([])
      );
    }
    this.router.navigateByUrl(RutasLocales.FINCA.toString());
  }

  irDocumentosLegales(limpiarEstado: boolean = false): void {
    // if (limpiarEstado) {
    //     sessionStorage.setItem(LlavesSessionStorage.PAGINAS, '0');
    //     sessionStorage.setItem(LlavesSessionStorage.MENU_ACTIVO, '-1');
    //     sessionStorage.setItem(LlavesSessionStorage.MENUS_YA_ABIERTOS, JSON.stringify([]));
    // }
    // // this.router.navigateByUrl(RutasLocales.DOC_LEGALES);
    const link = document.createElement('a');
    const nombre = 'politica-privacidad-gazelook';
    link.href = `http://d3ubht94yroq8c.cloudfront.net/politicas-terminos/terminos-condiciones-${this.idiomas.toUpperCase()}.pdf`;
    link.download = nombre;
    link.target = '_blank';
    link.dispatchEvent(
      new MouseEvent('click', {
        view: window,
        bubbles: false,
        cancelable: true,
      })
    );
  }

  mostrarLineaVerde(evento: any) {
    let linea: HTMLElement = document.getElementById(
      'bodylinea'
    ) as HTMLElement;
    if (linea.scrollTop === 0) {
      this.mostrarlineaverde = false;
      this.mostrarIdiomas = true;
    } else {
      this.mostrarlineaverde = true;
      this.mostrarIdiomas = false;
    }
  }

  //cambio de michel y andre solo ingles y espanol
  async cambiarIdioma(codIdioma: string) {
    // if (codIdioma !== 'en' && codIdioma !== 'es') {
    // 	return
    // }

    let idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
      codIdioma as CodigosCatalogoIdioma
    );
    if (!idioma || idioma === null) {
      idioma = await this.idiomaNegocio.obtenerIdiomaSegunCodigo(
        CodigosCatalogoIdioma.ESPANOL
      );
    }

    this.internacionalizacionNegocio.usarIidoma(
      idioma.codNombre.toLocaleLowerCase()
    );
    this.idiomaNegocio.guardarIdiomaSeleccionado(idioma);
    this.idiomaNegocio.eliminarVarablesStorage();

    this.idiomas = idioma.codNombre.toLowerCase();
    this.paginas = 0;
    this.menu = -1;
    this.leermas = -1;
    this.menusyaabiertos = [];

    // update local storage
    sessionStorage.setItem(
      LlavesSessionStorage.PAGINAS,
      this.paginas.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENU_ACTIVO,
      this.menu.toString()
    );
    sessionStorage.setItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS,
      JSON.stringify(this.menusyaabiertos)
    );
    // clearInterval(this.interval);
    this.obtenerTextosRotativosVerde();
    this.textoAEscribir();

    this.configurarImgCargaMarcos();
  }

  cambiarpreguntaAbierta(pre: number) {
    if (this.preguntaAbierta === pre) {
      this.preguntaAbierta = -1;
    } else {
      this.preguntaAbierta = pre;
    }
  }
  validarCampos() {
    if (!(this.nombre.length > 0)) {
      return false;
    }

    if (!(this.email.length > 0)) {
      return false;
    }

    if (!(this.pais.length > 0)) {
      return false;
    }

    if (!(this.direccion.length > 0)) {
      return false;
    }

    if (!(this.tema.length > 0)) {
      return false;
    }

    if (!(this.mensaje.length > 0)) {
      return false;
    }

    return true;
  }

  configurarVariablesDesdeLocalStorage() {
    // update local storage
    const a = sessionStorage.getItem(LlavesSessionStorage.PAGINAS.toString());
    const b = sessionStorage.getItem(
      LlavesSessionStorage.MENU_ACTIVO.toString()
    );
    const c = sessionStorage.getItem(
      LlavesSessionStorage.MENUS_YA_ABIERTOS.toString()
    );

    if (a && a !== null) {
      this.paginas = parseInt(a);
    } else {
      sessionStorage.setItem(
        LlavesSessionStorage.PAGINAS.toString(),
        this.paginas.toString()
      );
    }

    if (b && b !== null) {
      this.menu = parseInt(b);
    } else {
      sessionStorage.setItem(
        LlavesSessionStorage.MENU_ACTIVO.toString(),
        this.menu.toString()
      );
    }

    if (c && c !== null) {
      this.menusyaabiertos = JSON.parse(c);
    } else {
      sessionStorage.setItem(
        LlavesSessionStorage.MENUS_YA_ABIERTOS.toString(),
        JSON.stringify(this.menusyaabiertos)
      );
    }
  }

  iralanuncio() {
    // this.router.navigateByUrl(RutasLocales.ANUNCIO.toString())
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
    };
  }

  async enviarmensaje() {
    if (!this.validarCampos()) {
      return;
    }

    const correo: CorreoContacto = {
      nombre: this.nombre,
      email: this.email,
      pais: this.pais,
      direccion: this.direccion,
      tema: this.tema,
      mensaje: this.mensaje,
    };

    try {
      const status: string = await this.cuentaNegocio
        .enviarEmailDeContacto(correo)
        .toPromise();
      if (!status) {
        throw new Error('');
      }

      this.nombre = '';
      this.email = '';
      this.pais = '';
      this.direccion = '';
      this.tema = '';
      this.mensaje = '';
    } catch (error) {}
  }

  irAlDemo() {
    this._location.replaceState('');
    this.router.navigate([
      RutasLocales.MODULO_DEMO + '/' + RutasDemo.MENU_PRINCIPAL,
    ]);
  }

  async configurarBoton(): Promise<void> {
    this.configBotonPagoExtra = {
      text: 'landingEnviar',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.CELESTE,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.enviarFormularioContactanos();
      },
    };
  }

  async enviarFormularioContactanos(): Promise<void> {
    this.textoMensaje = await this.translateService
      .get('m6v12texto13')
      .toPromise();

    const elemento: HTMLElement = document.getElementById(
      'adGoogle'
    ) as HTMLElement;
    elemento.click();
    if (this.nombre && this.email && this.tema && this.mensaje) {
      let formulario: FormularioContactanos;
      formulario = {};
      formulario.nombre = this.nombre;
      formulario.email = this.email;
      formulario.tema = this.tema;
      formulario.mensaje = this.mensaje;
      if (this.pais !== '' && this.direccion !== '') {
        formulario.pais = this.pais;
        formulario.direccion = this.direccion;
      }
      this.errorMsj = false;
      this.configBotonPagoExtra.enProgreso = true;
      this.cuentaNegocio
        .enviarFormularioContactanos(formulario)
        .subscribe((res) => {
          if (res === 200) {
            this.contactUs = false;
            this.mensajeCorreo = true;
          } else {
          }
          this.configBotonPagoExtra.enProgreso = false;
        });
    } else {
      this.errorMsj = true;
      this.configBotonPagoExtra.enProgreso = false;
      this.textoMensajeError = await this.translateService
        .get('text4')
        .toPromise();
    }
  }
}
export interface NoPreguntes {
  preguntas: string[];
  respuestas: string[];
}

export interface CorreoContacto {
  nombre?: string;
  nombreContacto?: string;
  email?: string;
  pais?: string;
  direccion?: string;
  tema?: string;
  mensaje?: string;
}
