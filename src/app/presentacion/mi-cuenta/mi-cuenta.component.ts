import { RutasLocales } from 'src/app/rutas-locales.enum';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { EstiloInput } from '@shared/diseno/enums/estilo-input.enum';
import { EstiloErrorInput } from '@shared/diseno/enums/estilos-colores-general';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import {
  ColorFondoLinea,
  EspesorLineaItem
} from '@shared/diseno/enums/estilos-colores-general';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { InputCompartido } from '@shared/diseno/modelos/input.interface';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { RegistroService } from '@core/servicios/generales/registro.service';
import { CatalogoEstadoUsuario } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-usuario.enum';

@Component({
  selector: 'app-mi-cuenta',
  templateUrl: './mi-cuenta.component.html',
  styleUrls: ['./mi-cuenta.component.scss']
})
export class MiCuentaComponent implements OnInit {

  @ViewChild('toast', { static: false }) toast: ToastComponent;

  public confAppBar: ConfiguracionAppbarCompartida;
  public perfilSeleccionado: PerfilModel;
  private usuarioActivo: UsuarioModel;
  public solicitudInfo: boolean;
  public infoSolicitudInfo: string;
  public solicitudEliminacion: boolean;
  public infoSolicitudEliminacion: string;
  public solicitudReenvioCorreo: boolean;
  public infoSolicitudReenvioCorreo: string;

  public registroForm: FormGroup;

  public confLinea: LineaCompartida;
  public inputsForm: Array<InputCompartido>;
  public botonSubmit: BotonCompartido;
  public confBotonInfoCuentas: BotonCompartido;
  public confBotonCorreoVerifi: BotonCompartido;
  public confBotonEliminarCuenta: BotonCompartido;

  public accionCuenta: number;
  public validadorNombreContacto: boolean;
  public validadorEmail: boolean;
  public validadorFechaNacimiento: boolean;
  public confSelector: ConfiguracionSelector; // Configuracion del selector
  public confBuscador: ConfiguracionBuscadorModal; // Configuracion buscador localidades
  public confDone: ConfiguracionDone;
  public confToast: ConfiguracionToast;
  public listaResultadosLocalidades: PaginacionModel<ItemSelector>;

  constructor(
    private translateService: TranslateService,
    private perfilNegocio: PerfilNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    // tslint:disable-next-line: variable-name
    private _location: Location,
    private router: Router,
    private registroService: RegistroService,
    private cuentaNegocio: CuentaNegocio,
    private notificacionesUsuario: NotificacionesDeUsuario,
    private generadorId: GeneradorId,
  ) {
    this.accionCuenta = 0;
    this.inputsForm = [];
    this.solicitudInfo = false;
    this.infoSolicitudInfo = '';
    this.solicitudEliminacion = false;
    this.infoSolicitudEliminacion = '';
    this.solicitudReenvioCorreo = false;
    this.infoSolicitudReenvioCorreo = '';
    this.validadorFechaNacimiento = false;
  }

  ngOnInit(): void {
    this.configurarPerfilSeleccionado();
    this.configurarToast();
    this.configurarDone();
    this.configurarUsuarioActivo();
    this.configurarBoton();

    if (this.perfilSeleccionado) {
      this.configurarAppBar();
      this.notificacionesUsuario.validarEstadoDeLaSesion(false);
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado);
    }
  }

  configurarUsuarioActivo(): void {
    this.usuarioActivo = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
  }

  configurarPerfilSeleccionado(): void {
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
  }

  configurarAppBar(): void {
    this.confAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      accionAtras: () => {
        this.accionAtras();
      },
      searchBarAppBar: {
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
        nombrePerfil: {
          mostrar: true,
          llaveTexto:
            this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
        },
        mostrarTextoHome: true,
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarLineaVerde: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm11v1texto0'
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true,
          }
        }
      },
    };
  }

  configurarToast(): void {
    this.confToast = {
      // texto: 'Prueba de toas',
      texto: '',
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: true,
      intervalo: 3,
      // bloquearPantalla: true
    };
  }

  configurarDone(): void {
    this.confDone = {
      mostrarDone: false,
      mostrarLoader: false,
      intervalo: 3
    };
  }

  accionAtras(): void {
    this._location.back();
  }

  configurarLinea(): void {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false
    };
  }


  seleccionarAccion(accion: number): void {
    if (this.accionCuenta === accion) {
      this.accionCuenta = 0;
      return;
    }

    switch (accion) {
      case AccionesCuenta.CAMBIAR_DATOS_CUENTA:
        this.accionCuenta = 1;
        this.iniciarDatos();
        this.configurarInputs();
        break;
      case AccionesCuenta.SOLICITAR_INFO_CUENTA:
        this.accionCuenta = 2;
        break;
      case AccionesCuenta.REENVIO_CORREO_VERI:
        this.accionCuenta = 3;
        break;
      case AccionesCuenta.ELIMINAR_CUENTA:
        this.accionCuenta = 4;
        break;
      case AccionesCuenta.REEMBOLSOS:
        this.accionCuenta = 5;
        this.router.navigateByUrl(RutasLocales.REEMBOLSOS_PAGOS);
        break;
      default:
        break;
    }
  }

  iniciarDatos(): void {
    this.registroForm = this.registroService.inicializarControlesDelFormulario(this.perfilSeleccionado, false);
  }
  async configurarBoton(): Promise<void> {
    this.botonSubmit = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.submitActualizar();
      }
    };

    this.confBotonInfoCuentas = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.solicitarInfoCuentas();
      }
    };

    this.confBotonCorreoVerifi = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.solicitarCorreoVeri();
      }
    };

    this.confBotonEliminarCuenta = {
      text: 'm2v3texto20',
      tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => {
        this.solicitarEliminarCuenta();
      }
    };
  }

  solicitarInfoCuentas(): void {
    this.cuentaNegocio.solicitarInformacionUsuario(this.usuarioActivo.id).subscribe(data => {
      if (data === 201) {
        this.solicitudInfo = true;
        this.infoSolicitudInfo = 'm11v1texto7';
      } else {
        this.infoSolicitudInfo = 'text37';
      }
      this.toast.cerrarToast();
    }, error => {
      this.toast.abrirToast('text37');
    });
  }

  solicitarCorreoVeri(): void {
    if (this.usuarioActivo.estado.codigo === CatalogoEstadoUsuario.ACTIVA) {
      this.solicitudReenvioCorreo = true;
      this.infoSolicitudReenvioCorreo = 'm11v1texto9.1';
      return;
    } else {
      this.cuentaNegocio.reenviarCorreoVerificacion(this.usuarioActivo.id).subscribe(data => {
        if (data.datos) {
          this.solicitudReenvioCorreo = true;
          this.infoSolicitudReenvioCorreo = 'm11v1texto9';
        } else {
          this.infoSolicitudReenvioCorreo = data.mensaje;
        }
        this.toast.cerrarToast();
      }, error => {
        this.toast.abrirToast('text37');
      });
    }
  }

  solicitarEliminarCuenta(): void {
    this.cuentaNegocio.eliminarDatosUsuario(this.usuarioActivo.id).subscribe(data => {
      if (data.datos) {
        this.solicitudEliminacion = true;
        this.infoSolicitudEliminacion = 'm11v1texto11';
      } else {
        this.infoSolicitudEliminacion = data.mensaje;
      }
      this.toast.cerrarToast();
    }, error => {
      this.toast.abrirToast('text37');
    });
  }

  configurarInputsDelFormulario(
    registroForm: FormGroup
  ): Array<InputCompartido> {
    // tslint:disable-next-line: prefer-const
    let inputsForm: Array<InputCompartido> = [];
    if (registroForm) {

      // 0
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: 'Name:',
        data: registroForm.controls.nombre,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      });
      // 1
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: 'Apellidos:',
        data: registroForm.controls.apellido,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      });
      // 2
      inputsForm.push({
        tipo: 'email',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: 'E-mail:',
        data: registroForm.controls.email,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        bloquearCopy: false
      });



      // 3
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: '',
        data: registroForm.controls.fechaNacimiento,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        esInputParaFecha: true,
        bloquearCopy: false
      });

      // 4
      inputsForm.push({
        tipo: 'text',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: '',
        data: registroForm.controls.anteriorContrasena,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        contrasena: false,
        bloquearCopy: true
      });

      // 5
      inputsForm.push({
        tipo: 'password',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: '',
        data: registroForm.controls.nuevaContrasena,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        contrasena: true,
        bloquearCopy: true
      });
      // 6
      inputsForm.push({
        tipo: 'password',
        error: false,
        estilo: {
          estiloError: EstiloErrorInput.ROJO,
          estiloInput: EstiloInput.MI_CUENTA
        },
        placeholder: '',
        data: registroForm.controls.confirmarNuevaContrasena,
        id: this.generadorId.generarIdConSemilla(),
        errorPersonalizado: '',
        soloLectura: false,
        contrasena: true,
        bloquearCopy: true
      });


    }
    return inputsForm;
  }
  async configurarInputs(): Promise<void> {
    this.inputsForm = this.configurarInputsDelFormulario(this.registroForm);
    if (this.inputsForm.length > 0) {
      this.inputsForm[0].placeholder = await this.translateService.get('m2v3texto10').toPromise();
      this.inputsForm[1].placeholder = await this.translateService.get('m2v3texto11').toPromise();
      this.inputsForm[2].placeholder = await this.translateService.get('m2v3texto12').toPromise();
      // this.inputsForm[3].placeholder = await this.translateService.get('').toPromise()
      // this.inputsForm[4].placeholder = await this.translateService.get('').toPromise()
      // this.inputsForm[5].placeholder = await this.translateService.get('').toPromise()
      // this.inputsForm[6].placeholder = await this.translateService.get('').toPromise()
      const usuario: UsuarioModel = this.perfilSeleccionado;
      if (
        usuario &&
        usuario.perfiles &&
        usuario.perfiles.length === 0
      ) {
        this.inputsForm[1].soloLectura = false;
        this.inputsForm[2].soloLectura = false;
        this.inputsForm[3].soloLectura = false;
        this.inputsForm[7].soloLectura = false;
        this.inputsForm[8].soloLectura = false;
        this.inputsForm[9].soloLectura = false;
        this.inputsForm[10].soloLectura = false;
      }
      this.inputsForm.forEach((input, pos) => {
        if (pos === 2) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarEmailUnico(data, false)
          };

        }
        if (pos === 3) {
          input.validarCampo = {
            validar: true,
            validador: (data: any) => this.validarFechaNacimiento(data, false)
          };

        }

      });
    }
  }

  // tslint:disable-next-line: variable-name
  calcularEdad(fecha_nacimiento: string | number | Date): number {
    // tslint:disable-next-line: prefer-const
    let hoy = new Date();
    // tslint:disable-next-line: prefer-const
    let cumpleanos = new Date(fecha_nacimiento);
    let edad = hoy.getFullYear() - cumpleanos.getFullYear();
    const m = hoy.getMonth() - cumpleanos.getMonth();
    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      edad--;
    }
    return edad;
  }

  async validarFechaNacimiento(
    data: { id: string, texto: string },
    forzarSubmit: boolean = true
  ): Promise<boolean> {
    try {

      if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
        if (forzarSubmit) {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
        }
        return;
      }


      const edad = this.calcularEdad(data.texto);

      if (edad >= 18) {
        return true;

      } else {
        this.asignarMensajeDeErrorEnInput(data.id, 'text84');
        return false;
      }


    } catch (error) {

      this.validadorFechaNacimiento = false;
      this.botonSubmit.enProgreso = false;
      const errorMensaje = (error && error.message && error.message.length > 0) ? error.message : 'text84';
      this.asignarMensajeDeErrorEnInput(data.id, errorMensaje);
      return false;
    }

  }
  async validarEmailUnico(
    data: { id: string, texto: string },
    forzarSubmit: boolean = true
  ): Promise<void> {
    if (data.texto === this.usuarioActivo.email) {
      return;
    }

    try {
      if (!data || !data.id || !data.texto || !(data.texto.length > 0)) {
        if (forzarSubmit) {
          this.botonSubmit.enProgreso = false;
          this.toast.abrirToast('text4');
        }
        return;
      }

      const estado: string = await this.cuentaNegocio.validarEmailUnico(data.texto).toPromise();

      if (!estado) {
        throw new Error('');
      }

      this.validadorEmail = true;

      if (this.validadorEmail && this.validadorNombreContacto && forzarSubmit) {
        // tslint:disable-next-line: no-unused-expression
        this.submitActualizar;
      }
    } catch (error) {
      this.validadorEmail = false;
      this.botonSubmit.enProgreso = false;
      const errorMensaje = (error && error.message && error.message.length > 0) ? error.message : 'text6';
      this.asignarMensajeDeErrorEnInput(data.id, errorMensaje);
    }
  }

  asignarMensajeDeErrorEnInput(id: string, mensaje: string): void {
    if (!(this.inputsForm.length > 0)) {
      return;
    }
    this.inputsForm.forEach(item => {
      if (item.id === id) {
        item.errorPersonalizado = mensaje;
      }
    });
  }

  async submitActualizar(): Promise<void> {
    try {
      this.botonSubmit.enProgreso = true;
      let errorEnCampos = true;
      if (this.registroForm.valid) {
        errorEnCampos = false;
      }
      if (!this.validadorFechaNacimiento) {
        const validFechaNac = await this.validarFechaNacimiento(
          {
            id: this.inputsForm[3].id,
            texto: this.inputsForm[3].data.value
          }
        );


        if (!validFechaNac) {

          this.botonSubmit.enProgreso = false;
          return;

        }
      }

      if (
        (
          // tslint:disable-next-line: no-string-literal
          this.registroForm.value['anteriorContrasena'] === ''
          // tslint:disable-next-line: no-string-literal
          && this.registroForm.value['confirmarNuevaContrasena'] === ''
          // tslint:disable-next-line: no-string-literal
          && this.registroForm.value['nuevaContrasena'] === ''
        )
        ||
        (
          this.registroForm.value['anteriorContrasena'] !== ''
          && this.registroForm.value['confirmarNuevaContrasena'] !== ''
          && this.registroForm.value['nuevaContrasena'] !== ''
        )
      ) {
        errorEnCampos = false;
      }
      if (
        (
          this.registroForm.value['anteriorContrasena'] !== ''
          && this.registroForm.value['confirmarNuevaContrasena'] === ''
          && this.registroForm.value['nuevaContrasena'] === ''
        )
        ||
        (
          this.registroForm.value['anteriorContrasena'] === ''
          && this.registroForm.value['confirmarNuevaContrasena'] !== ''
          && this.registroForm.value['nuevaContrasena'] === ''
        )
        ||
        (
          this.registroForm.value['anteriorContrasena'] === ''
          && this.registroForm.value['confirmarNuevaContrasena'] === ''
          && this.registroForm.value['nuevaContrasena'] !== ''
        )

      ) {
        errorEnCampos = true;
      }
      if (errorEnCampos) {
        this.botonSubmit.enProgreso = false;
        this.toast.abrirToast('text4');
        return;
      }

      if (this.registroForm.value['nuevaContrasena'] !== this.registroForm.value['confirmarNuevaContrasena']) {
        this.toast.abrirToast('text88');
        return;
      }


      const nombreCompleto = this.registroForm.value['nombre'] + '-' + this.registroForm.value['apellido'];
      this.registroForm.value['nombre'] = nombreCompleto;

      let usuario: UsuarioModel;
      usuario = {};

      if (this.registroForm.value['nombre']) {
        usuario.nombre = this.registroForm.value['nombre'];
      }
      if (this.registroForm.value['email'] && this.registroForm.value['email'] !== this.usuarioActivo.email) {
        usuario.email = this.registroForm.value['email'];
      }
      if (this.registroForm.value['anteriorContrasena']) {
        usuario.anteriorContrasena = this.registroForm.value['anteriorContrasena'];
      }

      if (this.registroForm.value['nuevaContrasena']) {
        usuario.nuevaContrasena = this.registroForm.value['nuevaContrasena'];
      }
      if (this.registroForm.value['fechaNacimiento']) {
        usuario.fechaNacimiento = this.registroForm.value['fechaNacimiento'];
      }
      usuario.idDispositivo = this.usuarioActivo.dispositivos[0].id;
      const usuarioActualizado = await this.perfilNegocio.actualizarDatosUsuario(usuario, this.usuarioActivo.id).toPromise();
      if (!usuarioActualizado) {
        throw new Error('');
      }

      this.actualizarEnLocalStorage(usuario);
      this.toast.cerrarToast();
      this.botonSubmit.enProgreso = false;
      setTimeout(() => {
        this.confDone.mostrarDone = false;
        window.location.reload();
      }, 1500);
    } catch (error) {
      this.botonSubmit.enProgreso = false;
      this.toast.abrirToast('text37');
    }
  }

  actualizarEnLocalStorage(usuario: UsuarioModel): void {
    if (usuario.nombre) {
      const perfil: PerfilModel = this.perfilNegocio.obtenerPerfilSeleccionado();
      perfil.nombre = usuario.nombre;
      this.perfilNegocio.actualizarPerfilSeleccionado(perfil);
    }

    if (usuario.fechaNacimiento) {
      const usuarioModel: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage();
      usuarioModel.fechaNacimiento = usuario.fechaNacimiento;
      this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuarioModel);
    }
  }
}

export enum AccionesCuenta {
  CAMBIAR_DATOS_CUENTA = 1,
  SOLICITAR_INFO_CUENTA = 2,
  REENVIO_CORREO_VERI = 3,
  ELIMINAR_CUENTA = 4,
  REEMBOLSOS = 5,
}


