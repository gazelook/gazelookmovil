import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { PensamientoRoutingModule } from "src/app/presentacion/pensamiento/pensamiento-routing.module";
import { CrearPensamientoComponent } from "src/app/presentacion/pensamiento/crear-pensamiento/crear-pensamiento.component";
import { PensamientoComponent } from 'src/app/presentacion/pensamiento/pensamiento.component';
import { CompartidoModule } from '@shared/compartido.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PensamientoComponent,
    CrearPensamientoComponent,
  ],
  imports: [    
    TranslateModule,
    ReactiveFormsModule,
    CommonModule,
    CompartidoModule,
    PensamientoRoutingModule,    
  ],
  exports:[ 
    TranslateModule   
  ],
  providers:[    
  ]
})
export class PensamientoModule { }
