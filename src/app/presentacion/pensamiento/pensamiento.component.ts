import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { PensamientoCompartidoComponent } from '@shared/componentes/pensamiento/pensamiento-compartido.component';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ColorDeFondo, ColorDelTexto, EstilosDelTexto } from '@shared/diseno/enums/estilos-colores-general';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { TipoIconoBarraInferior } from '@shared/diseno/enums/tipo-icono.enum';
import { EstiloItemPensamiento, TipoPensamiento } from '@shared/diseno/enums/tipo-pensamiento.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ConfiguracionBarraInferiorInline } from '@shared/diseno/modelos/barra-inferior-inline.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { ItemPensamiento, PensamientoCompartido } from '@shared/diseno/modelos/pensamiento';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PensamientoNegocio } from 'dominio/logica-negocio/pensamiento.negocio';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
// import { LlamadaFirebaseService } from '@core/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
import { ButtonComponent } from '@shared/componentes/button/button.component';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';

@Component({
  selector: 'app-pensamiento',
  templateUrl: './pensamiento.component.html',
  styleUrls: ['./pensamiento.component.scss']
})
export class PensamientoComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent
  @ViewChild('pensamientoCompartidoComponent', { static: false }) pensamientoCompartidoComponent: PensamientoCompartidoComponent

  public TipoDataPensamientosEnum = TipoDataPensamientos

  public mostrarTitulosPublico: Boolean
  public mostrarTitulosPrivado: Boolean
  public mostrarTituloGeneral: Boolean
  public mostrarInfoPublico: Boolean
  public mostrarInfoPrivado: Boolean

  public perfilSeleccionado: PerfilModel
  public listaPensamientosPublico: PaginacionModel<PensamientoModel>
  public listaPensamientosPrivado: PaginacionModel<PensamientoModel>
  public tipoDataPensamientoActivo: TipoDataPensamientos
  public desactivarBarraInferior: boolean
  public pensamientoActivo: ItemPensamiento

  public configuracionAppBar: ConfiguracionAppbarCompartida
  public confDataListaPensamientosPublicos: DatosLista
  public confDataListaPensamientosPrivados: DatosLista
  public confDialogoEliminarPensamientos: DialogoCompartido
  public confBarraInferior: ConfiguracionBarraInferiorInline
  public confPensamientoCompartido: PensamientoCompartido
  public confBotonPensamientosPublicos: BotonCompartido
  public confBotonPensamientosPrivados: BotonCompartido
  public configuracionToast: ConfiguracionToast
  public botonCrearPensamiento: BotonCompartido

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
    private pensamientoNegocio: PensamientoNegocio,
    private perfilNegocio: PerfilNegocio,
    private variablesGlobales: VariablesGlobales,
    private _location: Location,
    private router: Router,
    private notificacionesUsuario: NotificacionesDeUsuario,
    // private llamadaFirebaseService: LlamadaFirebaseService
  ) {
    this.tipoDataPensamientoActivo = TipoDataPensamientos.DEFAULT
    this.desactivarBarraInferior = true
    this.mostrarInfoPublico = false
    this.mostrarInfoPrivado = false
  }

  ngOnInit(): void {
    this.variablesGlobales.mostrarMundo = false
    this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
    this.configurarTitulo()
    this.configurarAppBar()
    this.configurarBarraInferiorInline()
    this.configurarBotones()
    this.configurarListas()
    this.confDialogoEliminarPensamiento()
    this.configurarToast()

    if (this.perfilSeleccionado) {
      this.inicializarData()
      this.notificacionesUsuario.validarEstadoDeLaSesion(false)
      // this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
    } else {
      this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
    }
  }

  configurarAppBar() {
    this.configuracionAppBar = {
      usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
      searchBarAppBar: {
        nombrePerfil: {
          mostrar: true,
          llaveTexto: this.perfilSeleccionado.tipoPerfil.nombre || ''
        },
        buscador: {
          mostrar: false,
          configuracion: {
            disable: true
          }
        },
        mostrarDivBack: {
          icono: true,
          texto: true,
        },
        mostrarTextoHome: true,
        subtitulo: {
          mostrar: true,
          llaveTexto: 'm6v1texto1'
        },
        mostrarLineaVerde: true,
        tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
      }
    }
  }

  configurarListas() {
    this.confDataListaPensamientosPublicos = {
      tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
      lista: [],
      cargarMas: () => this.obtenerPensamientosPublicos()
    }

    this.confDataListaPensamientosPrivados = {
      tamanoLista: TamanoLista.TIPO_PENSAMIENTO_GESTIONAR,
      lista: [],
      cargarMas: () => this.obtenerPensamientosPrivados()
    }

    this.listaPensamientosPublico = {
      proximaPagina: true,
      lista: [],
      paginaActual: 1,
      totalDatos: 0
    }

    this.listaPensamientosPrivado = {
      proximaPagina: true,
      lista: [],
      paginaActual: 1,
      totalDatos: 0
    }
  }

  confDialogoEliminarPensamiento() {
    this.confDialogoEliminarPensamientos = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: 'm6v2texto10',
      listaAcciones: [
        ButtonComponent.crearBotonAfirmativo(() => {
          this.confDialogoEliminarPensamientos.mostrarDialogo = false
          this.eliminarPensamiento(this.pensamientoActivo)
        }),
        ButtonComponent.crearBotonNegativo(() => {
          this.confDialogoEliminarPensamientos.mostrarDialogo = false
        })
      ]
    }
  }

  configurarBarraInferiorInline() {
    this.confBarraInferior = {
      desactivarBarra: this.desactivarBarraInferior,
      capaColorFondo: {
        mostrar: true,
        anchoCapa: TamanoColorDeFondo.TAMANO100,
        colorDeFondo: ColorDeFondo.FONDO_TRANSPARENCIA_BASE
      },
      estatusError: (error: string) => { },
      placeholder: {
        mostrar: true,
        texto: 'm6v1texto6',
        configuracion: {
          color: ColorDelTexto.TEXTOROJOBASE,
          enMayusculas: true,
          estiloTexto: EstilosDelTexto.REGULAR,
          tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I2
        }
      },
      iconoTexto: {
        icono: {
          mostrar: true,
          tipo: TipoIconoBarraInferior.ICONO_TEXTO,
          eventoTap: (dataApiArchivo: SubirArchivoData) => {
            if (!(dataApiArchivo.descripcion && dataApiArchivo.descripcion.length > 0)) {
              return
            }
            if (this.pensamientoActivo && this.pensamientoActivo !== null) {
              this.actualizarTextoPensamiento(dataApiArchivo.descripcion)
              return
            }

            this.publicarPensamiento(dataApiArchivo.descripcion)
          }
        },
        capa: {
          mostrar: false,
          contador: {
            mostrar: true,
            maximo: 230
          }
        }
      },
      botonSend: {
        mostrar: true,
        evento: (dataApiArchivo: SubirArchivoData) => {
          if (!(dataApiArchivo.descripcion && dataApiArchivo.descripcion.length > 0)) {
            return
          }

          if (this.pensamientoActivo && this.pensamientoActivo !== null) {
            this.actualizarTextoPensamiento(dataApiArchivo.descripcion)
            return
          }

          this.publicarPensamiento(dataApiArchivo.descripcion)
        }
      },
    }
  }


  configurarBotones() {
    this.confBotonPensamientosPublicos = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m6v2texto4'),
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: (param: any) => {
        this.configurarTitulo(false, true, false)
        this.reiniciarPensamientoActivo()
        this.inicializarData(TipoDataPensamientos.PUBLICOS, true)
      }
    }

    this.confBotonPensamientosPrivados = {
      text: this.internacionalizacionNegocio.obtenerTextoSincrono('m6v2texto5'),
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.ROJO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: (param: any) => {
        this.configurarTitulo(false, false, true)
        this.reiniciarPensamientoActivo()
        this.inicializarData(TipoDataPensamientos.PRIVADOS, true)
      }
    }
  }

  inicializarData(
    tipoData: TipoDataPensamientos = TipoDataPensamientos.DEFAULT,
    reiniciarListas: boolean = false
  ) {
    this.tipoDataPensamientoActivo = tipoData

    if (reiniciarListas) {
      this.configurarListas()
    }

    switch (tipoData) {
      case TipoDataPensamientos.DEFAULT:

        this.desactivarBarraInferior = true
        this.configurarListaPensamientos()
        break
      case TipoDataPensamientos.PUBLICOS:
        this.confBarraInferior.desactivarBarra = false
        this.configurarListaPensamientos(
          true,
          true,
          TipoPensamiento.PENSAMIENTO_PUBLICO_CREACION,
          EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
          true,
          true
        )
        if (this.pensamientoCompartidoComponent) {
          this.pensamientoCompartidoComponent.divPensamiento = 'divPensamientoFormaAmarilla'
        }
        break
      case TipoDataPensamientos.PRIVADOS:
        this.confBarraInferior.desactivarBarra = false
        this.configurarListaPensamientos(
          true,
          true,
          TipoPensamiento.PENSAMIENTO_PRIVADO_CREACION,
          EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
          true,
          true
        )
        if (this.pensamientoCompartidoComponent) {
          this.pensamientoCompartidoComponent.divPensamiento = 'divPensamientoFormaAmarilla'
        }
        break
      default:
        this.configurarListaPensamientos()
        break
    }
  }

  configurarListaPensamientos(
    esLista: boolean = false,
    subtitulo: boolean = false,
    tipoPensamiento: TipoPensamiento = TipoPensamiento.PENSAMIENTO_SIN_SELECCIONAR,
    estiloItemPensamiento: EstiloItemPensamiento = EstiloItemPensamiento.ITEM_CREAR_PENSAMIENTO,
    presentarX: boolean = false,
    cargarData: boolean = false
  ) {
    this.confPensamientoCompartido = {
      esLista: esLista,
      tipoPensamiento: tipoPensamiento,
      configuracionItem: {
        estilo: estiloItemPensamiento,
        presentarX: presentarX
      },
      subtitulo: subtitulo
    }

    if (
      cargarData &&
      this.tipoDataPensamientoActivo === TipoDataPensamientos.PUBLICOS
    ) {
      this.obtenerPensamientosPublicos()
      return
    }

    if (
      cargarData &&
      this.tipoDataPensamientoActivo === TipoDataPensamientos.PRIVADOS
    ) {
      this.obtenerPensamientosPrivados()
      return
    }
  }

  configurarToast() {
    this.configuracionToast = {
      cerrarClickOutside: false,
      mostrarLoader: false,
      mostrarToast: false,
      texto: ""
    }
  }

  async obtenerPensamientosPublicos() {
    try {
      if (!this.listaPensamientosPublico.proximaPagina) {
        return
      }

      const pensamientos: PaginacionModel<PensamientoModel> = await this.pensamientoNegocio.cargarMasPensamientos(
        this.perfilSeleccionado._id,
        15,
        this.listaPensamientosPublico.paginaActual,
        true
      ).toPromise()

      this.listaPensamientosPublico.proximaPagina = pensamientos.proximaPagina

      pensamientos.lista.forEach(pensamiento => {
        this.confDataListaPensamientosPublicos.lista.push(pensamiento)
      })

      if (this.listaPensamientosPublico.proximaPagina) {
        this.listaPensamientosPublico.paginaActual += 1
      }
    } catch (error) {
      this.listaPensamientosPublico.lista = []
      this.confDataListaPensamientosPublicos.lista = []
    }
  }

  async obtenerPensamientosPrivados() {
    try {
      if (!this.listaPensamientosPrivado.proximaPagina) {
        return
      }

      const pensamientos: PaginacionModel<PensamientoModel> = await this.pensamientoNegocio.cargarMasPensamientos(
        this.perfilSeleccionado._id,
        15,
        this.listaPensamientosPrivado.paginaActual,
        false
      ).toPromise()

      this.listaPensamientosPrivado.proximaPagina = pensamientos.proximaPagina

      pensamientos.lista.forEach(pensamiento => {
        this.confDataListaPensamientosPrivados.lista.push(pensamiento)
      })

      if (this.listaPensamientosPrivado.proximaPagina) {
        this.listaPensamientosPrivado.paginaActual += 1
      }
    } catch (error) {
      this.listaPensamientosPrivado.lista = []
      this.confDataListaPensamientosPrivados.lista = []
    }
  }

  reiniciarPensamientoActivo() {
    this.pensamientoActivo = null
    this.confBarraInferior.iconoTexto.capa.mostrar = false
    this.confBarraInferior.iconoTexto.capa.informacion = ''
  }

  unClick(itemPensamiento: ItemPensamiento) {
    this.reiniciarPensamientoActivo()

    if (
      itemPensamiento &&
      itemPensamiento.pensamiento.id &&
      itemPensamiento.pensamiento.id.length > 0
    ) {
      this.pensamientoActivo = itemPensamiento
      this.confDialogoEliminarPensamientos.mostrarDialogo = true
    }
  }

  dobleClick(itemPensamiento: ItemPensamiento) {
    this.reiniciarPensamientoActivo()

    if (
      itemPensamiento &&
      itemPensamiento.pensamiento.id &&
      itemPensamiento.pensamiento.id.length > 0 &&
      itemPensamiento.pensamiento.texto &&
      itemPensamiento.pensamiento.texto.length > 0
    ) {
      this.pensamientoActivo = itemPensamiento
      this.confBarraInferior.iconoTexto.capa.mostrar = true
      this.confBarraInferior.iconoTexto.capa.informacion = itemPensamiento.pensamiento.texto
    }
  }

  clickLargo(itemPensamiento: ItemPensamiento) {
    this.reiniciarPensamientoActivo()
    this.actualizarEstadoPensamiento(itemPensamiento)
  }

  async actualizarEstadoPensamiento(itemPensamiento: ItemPensamiento) {
    try {
      this.toast.abrirToast('', true)
      const estatus = this.pensamientoNegocio.actualizarEstadoPensamiento(itemPensamiento.pensamiento.id).toPromise()
      this.removerPensamientoDeListas(itemPensamiento)
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  async eliminarPensamiento(itemPensamiento: ItemPensamiento) {
    try {
      this.toast.abrirToast('', true)
      const estatus = await this.pensamientoNegocio.eliminarPensamiento(itemPensamiento.pensamiento.id).toPromise()
      this.removerPensamientoDeListas(itemPensamiento)
      this.pensamientoActivo = null
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }
  
  removerPensamientoDeListas(itemPensamiento: ItemPensamiento) {
    if (this.tipoDataPensamientoActivo === TipoDataPensamientos.PUBLICOS) {
      const index = this.confDataListaPensamientosPublicos.lista.findIndex(e => e.id === itemPensamiento.pensamiento.id)
      if (index >= 0) {
        this.confDataListaPensamientosPublicos.lista.splice(index, 1)
      }
      this.toast.cerrarToast()
      return
    }

    if (this.tipoDataPensamientoActivo === TipoDataPensamientos.PRIVADOS) {
      const index = this.confDataListaPensamientosPrivados.lista.findIndex(e => e.id === itemPensamiento.pensamiento.id)
      if (index >= 0) {
        this.confDataListaPensamientosPrivados.lista.splice(index, 1)
      }
      this.toast.cerrarToast()
      return
    }
  }

  async actualizarTextoPensamiento(texto: string) {
    try {
      this.toast.abrirToast('', true)
      const estatus = await this.pensamientoNegocio.actualizarPensamiento(
        this.pensamientoActivo.pensamiento.id,
        texto
      ).toPromise()

      this.pensamientoActivo.pensamiento.texto = texto
      this.pensamientoActivo.pensamiento.fechaActualizacion = new Date()

      if (this.tipoDataPensamientoActivo === TipoDataPensamientos.PUBLICOS) {
        const index = this.confDataListaPensamientosPublicos.lista.findIndex(e => e.id === this.pensamientoActivo.pensamiento.id)
        if (index >= 0) {
          this.confDataListaPensamientosPublicos.lista.splice(index, 1)
          this.confDataListaPensamientosPublicos.lista.unshift(this.pensamientoActivo.pensamiento)
        }
        this.pensamientoActivo = null
        this.toast.cerrarToast()
        return
      }

      if (this.tipoDataPensamientoActivo === TipoDataPensamientos.PRIVADOS) {
        const index = this.confDataListaPensamientosPrivados.lista.findIndex(e => e.id === this.pensamientoActivo.pensamiento.id)
        if (index >= 0) {
          this.confDataListaPensamientosPrivados.lista.splice(index, 1)
          this.confDataListaPensamientosPrivados.lista.unshift(this.pensamientoActivo.pensamiento)
        }
        this.pensamientoActivo = null
        this.toast.cerrarToast()
        return
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  async publicarPensamiento(
    texto: string
  ) {
    try {
      this.toast.abrirToast('', true)
      const estado = (this.tipoDataPensamientoActivo !== TipoDataPensamientos.DEFAULT && this.tipoDataPensamientoActivo === TipoDataPensamientos.PUBLICOS)

      const data = await this.pensamientoNegocio.crearPensamiento(
        this.perfilSeleccionado._id,
        estado,
        texto
      ).toPromise()

      const pensamiento: PensamientoModel = { ...data }

      if (this.tipoDataPensamientoActivo === TipoDataPensamientos.PUBLICOS) {
        this.confDataListaPensamientosPublicos.lista.unshift(pensamiento)
        this.toast.cerrarToast()
        return
      }

      if (this.tipoDataPensamientoActivo === TipoDataPensamientos.PRIVADOS) {
        this.confDataListaPensamientosPrivados.lista.unshift(pensamiento)
        this.toast.cerrarToast()
        return
      }
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  configurarTitulo(
    general: boolean = true,
    publicos: boolean = false,
    privados: boolean = false
  ) {
    this.mostrarTituloGeneral = general
    this.mostrarInfoPublico = publicos
    this.mostrarInfoPrivado = privados
  }
}

export enum TipoDataPensamientos {
  DEFAULT = 'default',
  PRIVADOS = 'privados',
  PUBLICOS = 'publicos'
}
