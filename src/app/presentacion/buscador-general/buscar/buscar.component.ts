import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { CodigoEstadoParticipanteAsociacion } from '@core/servicios/remotos/codigos-catalogos/codigo-estado-partic-aso.enum';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { AlturaResumenPerfil } from '@shared/diseno/enums/altura-resumen-perfil.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import {
	ColorDeBorde,
	ColorDeFondo, ColorDelTexto, ColorFondoAleatorio, ColorFondoLinea,
	EspesorLineaItem, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { PaddingLineaVerdeContactos } from '@shared/diseno/enums/estilos-padding-general';
import { TamanoColorDeFondo, TamanoDeTextoConInterlineado, TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';
import { EstiloItemPensamiento, TipoPensamiento } from '@shared/diseno/enums/tipo-pensamiento.enum';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { ConfiguracionItemBuscadorProyectoNoticia } from '@shared/diseno/modelos/item-buscador-proyectos-noticias.interface';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ConfiguracionItemListaContactosCompartido, ConfiguracionLineaVerde, DataContacto } from '@shared/diseno/modelos/lista-contactos.interface';
import { ConfiguracionResumenPerfil } from '@shared/diseno/modelos/resumen-perfil.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { AlbumNegocio } from 'src/app/dominio/logica-negocio/album.negocio';
import { NoticiaNegocio } from 'src/app/dominio/logica-negocio/noticia.negocio';
import { ProyectoNegocio } from 'src/app/dominio/logica-negocio/proyecto.negocio';
import { AlbumModel } from 'src/app/dominio/modelo/entidades/album.model';
import { RutasNoticias } from 'src/app/presentacion/noticias/rutas-noticias.enum';
import { RutasPerfiles } from 'src/app/presentacion/perfiles/rutas-perfiles.enum';
import { RutasProyectos } from 'src/app/presentacion/proyectos/rutas-proyectos.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { RutasGazing } from './../../gazing/rutas-gazing.enum';

@Component({
	selector: 'app-buscar',
	templateUrl: './buscar.component.html',
	styleUrls: ['./buscar.component.scss']
})
export class BuscarComponent implements OnInit {

	public CodigosCatalogoEntidadEnum = CodigosCatalogoEntidad
	public codigoEntidad: CodigosCatalogoEntidad
	public perfilSeleccionado: PerfilModel

	// Listas
	public paginacionProyectos: PaginacionModel<ConfiguracionItemBuscadorProyectoNoticia>
	public paginacionNoticias: PaginacionModel<ConfiguracionItemBuscadorProyectoNoticia>
	public paginacionContactos: PaginacionModel<ConfiguracionItemListaContactosCompartido>
	public paginacionPerfiles: PaginacionModel<ConfiguracionItemListaContactosCompartido>
	public listaPensamientos: DatosLista

	// Interno
	public mostrarLoader: boolean
	public mostrarCargandoPequeno: boolean
	public mostrarError: boolean
	public mensajeError: string
	public puedeCargarMas: boolean
	public idCapaCuerpo: string
	public idPerfilActivo: string
	public idAsociacionActivo: string
	public mostrarNoHayItems: boolean


	// Configuraciones
	public confAppbar: ConfiguracionAppbarCompartida
	public confResumenPerfil: ConfiguracionResumenPerfil

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private router: Router,
		private _location: Location,
		private perfilNegocio: PerfilNegocio,
		private proyectoNegocio: ProyectoNegocio,
		private albumNegocio: AlbumNegocio,
		private noticiaNegocio: NoticiaNegocio,
		private generadorId: GeneradorId,
		private variablesGlobales: VariablesGlobales
	) {
		this.mostrarLoader = false
		this.mostrarError = false
		this.mensajeError = ''
		this.puedeCargarMas = true
		this.mostrarCargandoPequeno = false
		this.idCapaCuerpo = 'listaConScroll'
		this.idPerfilActivo = ''
		this.idAsociacionActivo = ''
		this.mostrarNoHayItems = false

	}

	ngOnInit(): void {
		this.configurarEntidad()
		this.variablesGlobales.mostrarMundo = false
		this.configurarPerfilSeleccionado()
		this.configurarListaPensamientos()
		this.configurarResumenPerfil(false, false, false, '', '', '', UsoItemCircular.CIRCONTACTO)
		if (this.perfilSeleccionado) {
			this.configurarAppbar()
			this.configurarListas()
		} else {
			this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
		}
	}

	configurarEntidad() {
		const ruta = this.router.url

		if (ruta.indexOf('proyectos') >= 0) {
			this.codigoEntidad = CodigosCatalogoEntidad.PROYECTO
			return
		}

		if (ruta.indexOf('noticias') >= 0) {
			this.codigoEntidad = CodigosCatalogoEntidad.NOTICIA
			return
		}

		if (ruta.indexOf('contactos') >= 0) {
			this.codigoEntidad = CodigosCatalogoEntidad.CONTACTO
			return
		}

		if (ruta.indexOf('usuarios') >= 0) {
			this.codigoEntidad = CodigosCatalogoEntidad.PERFIL
			return
		}
	}

	configurarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	configurarAppbar() {
		this.confAppbar = {
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			accionAtras: () => {
				this._location.back()
			},
			searchBarAppBar: {
				mostrarDivBack: {
					icono: true,
					texto: true
				},
				mostrarTextoHome: true,
				mostrarLineaVerde: true,
				tamanoColorFondo: this.obtenerTamanoColorDeFondo(),
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(
            this.perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil)
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm3v8texto2'
				},
				buscador: {
					mostrar: true,
					configuracion: {
						disable: false,
						entidad: this.codigoEntidad,
						modoBusqueda: ModoBusqueda.BUSQUEDA_LOCAL,
						valorBusqueda: '',
						placeholder: this.obtenerLlavePlaceholder(),
						focusEnInput: true,
						buscar: () => {
							const query = this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda

							this.buscar(query)
						}
					}
				}
			}
		}
	}

	configurarResumenPerfil(
		mostrar: boolean,
		error: boolean,
		loader: boolean,
		titulo: string,
		idPerfil: string,
		urlMedia: string,
		usoItem: UsoItemCircular,
		sonContactos: boolean = false
	) {
		this.idPerfilActivo = idPerfil
		this.confResumenPerfil = {
			titulo: titulo,
			alturaModal: AlturaResumenPerfil.VISTA_BUSCAR_CONTACTOS_100,
			configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
				idPerfil,
				urlMedia,
				ColorDeBorde.BORDER_AMARILLO,
				ColorDeFondo.FONDO_BLANCO,
				false,
				usoItem,
				false
			),
			configuracionPensamiento: {
				tipoPensamiento: TipoPensamiento.PENSAMIENTO_PERFIL,
				esLista: true,
				subtitulo: true,
				configuracionItem: {
					estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
				},
			},
			confDataListaPensamientos: this.listaPensamientos,
			botones: [
				{
					text: 'm9v2texto2',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.AMARRILLO,
					ejecutar: () => {
						this.router.navigateByUrl(
							RutasLocales.MODULO_PERFILES +
							'/' +
							RutasPerfiles.PERFIL.toString().replace(':id', this.idPerfilActivo)
						)
					},
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				},
				{
					text: 'm9v2texto3',
					tamanoTexto: TamanoDeTextoConInterlineado.L4_I1,
					colorTexto: ColorTextoBoton.ROJO,
					ejecutar: () => {
						this.confResumenPerfil.mostrar = false
						this.idPerfilActivo = ''
					},
					enProgreso: false,
					tipoBoton: TipoBoton.TEXTO,
				},
			],
			reintentar: (id: string) => {
				this.obtenerPerfilBasico(id);
			},
			sonContactos: sonContactos,
			mostrar: mostrar,
			error: error,
			loader: loader,
		}
	}

	configurarListaPensamientos() {
		this.listaPensamientos = {
			tamanoLista: TamanoLista.LISTA_PENSAMIENTO_RESUMEN_MODAL_PERFIL,
			lista: [],
		}
	}

	obtenerLlavePlaceholder() {
		switch (this.codigoEntidad) {
			case CodigosCatalogoEntidad.PERFIL:
				return 'm3v6texto1'
			case CodigosCatalogoEntidad.PROYECTO:
				return 'm5v1texto1'
			case CodigosCatalogoEntidad.NOTICIA:
				return 'm4v3texto1'
			case CodigosCatalogoEntidad.CONTACTO:
				return 'm3v8texto1'
			default:
				return ''
		}
	}

	obtenerTamanoColorDeFondo(): TamanoColorDeFondo {
		switch (this.codigoEntidad) {
			case CodigosCatalogoEntidad.PERFIL:
				return TamanoColorDeFondo.TAMANO6920
			case CodigosCatalogoEntidad.PROYECTO:
				return TamanoColorDeFondo.TAMANO6920
			case CodigosCatalogoEntidad.NOTICIA:
				return TamanoColorDeFondo.TAMANO6920
			case CodigosCatalogoEntidad.CONTACTO:
				return TamanoColorDeFondo.TAMANO100
			default:
				return TamanoColorDeFondo.TAMANO100
		}
	}

	configurarListas() {
		switch (this.codigoEntidad) {
			case CodigosCatalogoEntidad.PERFIL:
				this.paginacionPerfiles = {
					lista: [],
					paginaActual: 1,
					proximaPagina: true,
					totalDatos: 0,
				}
				break
			case CodigosCatalogoEntidad.PROYECTO:
				this.paginacionProyectos = {
					lista: [],
					paginaActual: 1,
					proximaPagina: true,
					totalDatos: 0,
				}
				break
			case CodigosCatalogoEntidad.NOTICIA:
				this.paginacionNoticias = {
					lista: [],
					paginaActual: 1,
					proximaPagina: true,
					totalDatos: 0,
				}
				break
			case CodigosCatalogoEntidad.CONTACTO:
				this.paginacionContactos = {
					lista: [],
					paginaActual: 1,
					proximaPagina: true,
					totalDatos: 0,
				}
				break
			default:
				break
		}
	}

	buscar(
		query: string,
		configurarListas: boolean = true
	) {

		if (this.mostrarLoader || this.mostrarCargandoPequeno) {
			return
		}

		if (configurarListas) {
			this.configurarListas()
		}

		query = query.trim()

		switch (this.codigoEntidad) {
			case CodigosCatalogoEntidad.PERFIL:
				this.buscarUsuarios(query)
				break
			case CodigosCatalogoEntidad.PROYECTO:
				this.buscarProyectos(query)
				break
			case CodigosCatalogoEntidad.NOTICIA:
				this.buscarNoticias(query)
				break
			case CodigosCatalogoEntidad.CONTACTO:
				this.buscarContactos(query)
				break
			default:
				break
		}
	}

	async buscarProyectos(query: string) {
		if (!this.paginacionProyectos.proximaPagina) {
			this.puedeCargarMas = false
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			return
		}

		try {
			this.mostrarNoHayItems = false
			this.mostrarLoader = (this.paginacionProyectos.lista.length === 0)
			this.mostrarCargandoPequeno = (this.paginacionProyectos.lista.length > 0)

			const dataPaginacion = await this.proyectoNegocio.buscarProyectosPorTitulo(
				query,
				10,
				this.paginacionProyectos.paginaActual
			).toPromise()

			this.paginacionProyectos.totalDatos = dataPaginacion.totalDatos
			this.paginacionProyectos.proximaPagina = dataPaginacion.proximaPagina

			dataPaginacion.lista.forEach(proyecto => {
				const index: number = this.paginacionProyectos.lista.findIndex(e => e.proyecto.id === proyecto.id)
				if (index < 0) {
					this.paginacionProyectos.lista.push(
						this.configurarItemProyectos(proyecto)
					)
				}

			})
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = true
			this.mostrarNoHayItems = (this.paginacionProyectos.lista.length === 0)


			if (this.paginacionProyectos.proximaPagina) {
				this.paginacionProyectos.paginaActual += 1
			}
		} catch (error) {
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = false
		}
	}

	configurarItemProyectos(
		proyecto: ProyectoModel
	): ConfiguracionItemBuscadorProyectoNoticia {
		return {
			codigoEntidad: CodigosCatalogoEntidad.PROYECTO,
			proyecto: proyecto,
			configuracionRectangulo: this.obtenerConfiguracionRectangulo(proyecto),
			eventoTap: (id: string) => {
				if (!id || id.length === 0) {
					return
				}

				const modulo = RutasLocales.MODULO_PROYECTOS.toString()
				let ruta = RutasProyectos.VISITAR.toString()
				ruta = ruta.replace(':id', id)
				this.router.navigateByUrl(modulo + '/' + ruta)
				return
			}
		}
	}

	configurarItemNoticias(
		noticia: NoticiaModel
	): ConfiguracionItemBuscadorProyectoNoticia {
		return {
			codigoEntidad: CodigosCatalogoEntidad.NOTICIA,
			noticia: noticia,
			configuracionRectangulo: this.obtenerConfiguracionRectangulo(noticia, true),
			eventoTap: (id: string) => {
				if (!id || id.length === 0) {
					return
				}

				const modulo = RutasLocales.MODULO_NOTICIAS.toString()
				let ruta = RutasNoticias.VISITAR.toString()
				ruta = ruta.replace(':id', id)
				this.router.navigateByUrl(modulo + '/' + ruta)
				return
			}
		}
	}

	obtenerConfiguracionRectangulo(
		entidad: any,
		predeterminado: boolean = false
	): CongifuracionItemProyectosNoticias {
		let album: AlbumModel

		if (predeterminado) {
			album = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(entidad.adjuntos)
		}

		if (!album) {
			album = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.GENERAL,
				entidad.adjuntos
			)
		}

		const urlMedia = this.obtenerUrlMedia(album)

		return {
			usoVersionMini: true,
			id: entidad.id,
			colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
			colorDeFondo: ColorDeFondo.FONDO_BLANCO,
			fecha: {
				mostrar: false,
			},
			etiqueta: {
				mostrar: false,
			},
			titulo: {
				mostrar: false,
			},
			urlMedia: urlMedia,
			usoItem: UsoItemProyectoNoticia.RECPROYECTO,
			loader: (urlMedia.length > 0),
			eventoTap: {
				activo: false,
			},
			eventoDobleTap: {
				activo: false
			},
			eventoPress: {
				activo: false
			}
		}
	}

	scroolEnCapaCuerpo() {
		if (!this.puedeCargarMas) {
			return
		}

		const elemento: HTMLElement = document.getElementById(
      this.idCapaCuerpo) as HTMLElement
		if (elemento.offsetHeight + elemento.scrollTop >= elemento.scrollHeight - 3.16) {
			this.puedeCargarMas = false
			const query: string = this.confAppbar.searchBarAppBar.buscador.configuracion.valorBusqueda
			this.buscar(query, false)
		}
	}

	async buscarNoticias(query: string) {
		if (!this.paginacionNoticias.proximaPagina) {
			this.puedeCargarMas = false
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			return
		}

		try {
			this.mostrarNoHayItems = false
			this.mostrarLoader = (this.paginacionNoticias.lista.length === 0)
			this.mostrarCargandoPequeno = (this.paginacionNoticias.lista.length > 0)

			const dataPaginacion = await this.noticiaNegocio.buscarNoticiasPorTitulo(
				query,
				10,
				this.paginacionNoticias.paginaActual
			).toPromise()

			this.paginacionNoticias.totalDatos = dataPaginacion.totalDatos
			this.paginacionNoticias.proximaPagina = dataPaginacion.proximaPagina

			dataPaginacion.lista.forEach(noticia => {
				this.paginacionNoticias.lista.push(
					this.configurarItemNoticias(noticia)
				)
			})

			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = true
			this.mostrarNoHayItems = (this.paginacionNoticias.lista.length === 0)

			if (this.paginacionNoticias.proximaPagina) {
				this.paginacionNoticias.paginaActual += 1
			}
		} catch (error) {
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = false
		}
	}

	async buscarUsuarios(query: string) {
		
		if (!this.paginacionPerfiles.proximaPagina) {
			this.puedeCargarMas = false
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			return
		}

		try {
			this.mostrarNoHayItems = false
			this.mostrarLoader = (this.paginacionPerfiles.lista.length === 0)
			this.mostrarCargandoPequeno = (this.paginacionPerfiles.lista.length > 0)

			const dataPaginacion = await this.perfilNegocio.buscarPerfilesPorNombre(
				query,
				this.perfilSeleccionado._id,
				20,
				this.paginacionPerfiles.paginaActual
			).toPromise()
			
			this.paginacionPerfiles.totalDatos = dataPaginacion.totalDatos
			this.paginacionPerfiles.proximaPagina = dataPaginacion.proximaPagina

			dataPaginacion.lista.forEach(perfil => {
				this.paginacionPerfiles.lista.push(
					this.configuracionPerfil(perfil)
				)
			})

			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = true
			this.mostrarNoHayItems = (this.paginacionPerfiles.lista.length === 0)

			if (this.paginacionPerfiles.proximaPagina) {
				this.paginacionPerfiles.paginaActual += 1
			}
		} catch (error) {
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = false
		}
	}

	configuracionPerfil(
		perfil: PerfilModel,
		colorDelTextoSuperior: ColorDelTexto = ColorDelTexto.TEXTOBLANCO,
		colorDelTextoInferior: ColorDelTexto = ColorDelTexto.TEXTOBLANCO,
		usarCorazon: boolean = true,
		irAlChat: boolean = false
	): ConfiguracionItemListaContactosCompartido {
		
		let usoCirculo: UsoItemCircular = UsoItemCircular.CIRCONTACTO
		let corazon: boolean = false
		let urlMedia: string = ''

		const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
			CodigosCatalogoTipoAlbum.PERFIL,
			perfil.album
		)


		if (
			album &&
			album.portada &&
			album.portada.principal &&
			album.portada.principal.fileDefault
		) {
			usoCirculo = UsoItemCircular.CIRCARITACONTACTODEFECTO
			urlMedia = album.portada.principal.url
		} else {
			urlMedia = album.portada.principal.url
		}

		if (
			perfil &&
			perfil.asociaciones &&
			perfil.asociaciones.length > 0 &&
			usarCorazon
		) {
			corazon = true
		}

		return {
			id: perfil._id,
			usoItem: UsoItemListaContacto.USO_CONTACTO,
			contacto: {
				nombreContacto: perfil.nombreContacto,
				nombreContactoTraducido: perfil.nombreContactoTraducido,
				// nombre: perfil.nombre,
				contacto: false,
				estilosTextoSuperior: {
					color: colorDelTextoSuperior,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
				},
				estilosTextoInferior: {
					color: colorDelTextoInferior,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
				},
				idPerfil: perfil._id,
			},
			configCirculoFoto: this.configurarGeneralFotoPerfilContactos(
				perfil._id,
				urlMedia,
				ColorDeBorde.BORDER_ROJO,
				this.obtenerColorFondoAleatorio(),
				false,
				usoCirculo,
				false
			),
			mostrarX: {
				mostrar: false,
				color: true,
			},
			mostrarCorazon: corazon,
			configuracionLineaVerde: this.configurarLineaVerde(
				PaddingLineaVerdeContactos.PADDING_1542_267
			),
			eventoCirculoNombre: (idPerfil: string) => {
				if (!irAlChat) {
					this.obtenerPerfilBasico(idPerfil)
				}

				if (irAlChat) {
					this.idAsociacionActivo = perfil.asociaciones[0]._id
					this.router.navigateByUrl(
						RutasLocales.GAZING + '/' +
            RutasGazing.CHAT_GAZE.toString().replace(':id', this.idAsociacionActivo),
					);
				}
			},
			eventoDobleTap: (idAsociacion: string, contacto: DataContacto) => {
				if (
					!contacto ||
					!contacto.idPerfil ||
					!(contacto.idPerfil.length > 0)
				) {
					return
				}

				const ruta = RutasLocales.MODULO_PERFILES.toString()
				const aux = RutasPerfiles.PERFIL.toString().replace(':id', contacto.idPerfil)
				this.router.navigateByUrl(ruta + '/' + aux)
			}
		};
	}

	obtenerColorFondoAleatorio() {
		const fondosAleatorios = Object.keys(ColorFondoAleatorio).map(function (
			type
		) {
			return ColorFondoAleatorio[type]
		});
		const fondoAleatorio = this.randomItem(fondosAleatorios);
		return fondoAleatorio;
	}

	randomItem(items: any) {
		return items[Math.floor(Math.random() * items.length)];
	}

	configurarGeneralFotoPerfilContactos(
		idPerfil: string,
		urlMedia: string,
		colorBorde: any,
		colorFondo: any,
		mostrarCorazon: boolean,
		usoItemCirculo: UsoItemCircular,
		activarClick: boolean
	): ItemCircularCompartido {
		return {
			id: idPerfil,
			idInterno: this.generadorId.generarIdConSemilla(),
			usoDelItem: usoItemCirculo,
			esVisitante: true,
			urlMedia: urlMedia,
			activarClick: false,
			activarDobleClick: false,
			activarLongPress: false,
			mostrarBoton: false,
			mostrarLoader: (urlMedia.length > 0),
			textoBoton: '',
			capaOpacidad: {
				mostrar: false,
			},
			esBotonUpload: false,
			colorBorde: colorBorde,
			colorDeFondo: colorFondo,
			mostrarCorazon: mostrarCorazon,
		}
	}

	configurarLineaVerde(
		paddingLineaVerdeContactos?: PaddingLineaVerdeContactos
	): ConfiguracionLineaVerde {
		return {
			ancho: AnchoLineaItem.ANCHO6386,
			espesor: EspesorLineaItem.ESPESOR041,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			forzarAlFinal: false,
			paddingLineaVerde: paddingLineaVerdeContactos,
		};
	}

	async obtenerPerfilBasico(idPerfil: string) {
		if (idPerfil.length <= 0) {
			return
		}

		try {

			this.confResumenPerfil.loader = true
			this.confResumenPerfil.mostrar = true
			this.confResumenPerfil.confDataListaPensamientos.lista = []

			const perfil: PerfilModel = await this.perfilNegocio.obtenerPerfilBasico(
        idPerfil, this.perfilSeleccionado._id).toPromise()


			if (!perfil) {
				throw new Error('')
			}

			this.confResumenPerfil.confDataListaPensamientos.lista = perfil.pensamientos
			const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
				CodigosCatalogoTipoAlbum.PERFIL,
				perfil.album
			)

			let usoItem: UsoItemCircular = UsoItemCircular.CIRCONTACTO
			let urlMedia: string = ''
			let titulo: string = ''

			if (
				album &&
				album.portada &&
				album.portada.principal &&
				album.portada.principal.fileDefault
			) {
				usoItem = UsoItemCircular.CIRCARITACONTACTODEFECTO
				urlMedia = album.portada.principal.url
			} else {
				urlMedia = album.portada.principal.url
			}

			// titulo = perfil.nombre + '(' + perfil.nombreContacto + ')'
			titulo = perfil.nombreContacto

			let sonContactos: boolean = false
			if (perfil.participanteAsociacion) {
				sonContactos = (perfil.participanteAsociacion.estado.codigo ===
          CodigoEstadoParticipanteAsociacion.CONTACTO)
			}

			this.configurarResumenPerfil(
				true,
				false,
				false,
				titulo,
				perfil._id,
				urlMedia,
				usoItem,
				sonContactos
			)
		} catch (error) {
			this.configurarResumenPerfil(
				true,
				error,
				false,
				'',
				idPerfil,
				'',
				UsoItemCircular.CIRCONTACTO,

			)
		}
	}

	async buscarContactos(query: string) {
		if (!this.paginacionContactos.proximaPagina) {
			this.puedeCargarMas = false
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			return
		}

		try {
			this.mostrarNoHayItems = false
			this.mostrarLoader = (this.paginacionContactos.lista.length === 0)
			this.mostrarCargandoPequeno = (this.paginacionContactos.lista.length > 0)

			const dataPaginacion = await this.perfilNegocio.buscarContactosPorNombre(
				query,
				this.perfilSeleccionado._id,
				10,
				this.paginacionContactos.paginaActual
			).toPromise()

			this.paginacionContactos.totalDatos = dataPaginacion.totalDatos
			this.paginacionContactos.proximaPagina = dataPaginacion.proximaPagina

			dataPaginacion.lista.forEach(perfil => {
				if (perfil.asociaciones.length > 0) {
					this.paginacionContactos.lista.push(
						this.configuracionPerfil(
							perfil,
							ColorDelTexto.TEXTOAZULBASE,
							ColorDelTexto.TEXTONEGRO,
							false,
							true
						)
					)
				}
			})

			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = true
			this.mostrarNoHayItems = (this.paginacionContactos.lista.length === 0)

			if (this.paginacionContactos.proximaPagina) {
				this.paginacionContactos.paginaActual += 1
			}
		} catch (error) {
			this.mostrarLoader = false
			this.mostrarCargandoPequeno = false
			this.puedeCargarMas = false
		}
	}

	obtenerUrlMedia(
		album: AlbumModel
	): string {
		try {

			if (!album.portada) {
				throw new Error('')
			}

			if (album.portada.miniatura) {
				return album.portada.miniatura.url
			}

			if (album.portada.principal) {
				return album.portada.principal.url
			}

			return ''
		} catch (error) {
			return ''
		}
	}

}
