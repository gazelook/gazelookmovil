import { BuscarComponent } from 'src/app/presentacion/buscador-general/buscar/buscar.component';
import { RutasBuscadorGeneral } from 'src/app/presentacion/buscador-general/rutas-buscador-general.enum';
import { BuscadorGeneralComponent } from 'src/app/presentacion/buscador-general/buscador-general.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		component: BuscadorGeneralComponent,
		children: [
			{
				path: RutasBuscadorGeneral.BUSCAR_CONTACTOS.toString(),
				component: BuscarComponent
			},
			{
				path: RutasBuscadorGeneral.BUSCAR_NOTICIAS.toString(),
				component: BuscarComponent
			},
			{
				path: RutasBuscadorGeneral.BUSCAR_PERFILES.toString(),
				component: BuscarComponent
			},
			{
				path: RutasBuscadorGeneral.BUSCAR_PROYECTOS.toString(),
				component: BuscarComponent
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class BuscadorGeneralRoutingModule { }
