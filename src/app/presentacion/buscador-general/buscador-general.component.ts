import { Component, OnInit } from '@angular/core';
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from 'src/app/nucleo/servicios/generales/notificaciones/notificaciones-usuario.service';

@Component({
	selector: 'app-buscador-general',
	templateUrl: './buscador-general.component.html',
	styleUrls: ['./buscador-general.component.scss']
})
export class BuscadorGeneralComponent implements OnInit {

	constructor(
		private notificacionesUsuario: NotificacionesDeUsuario,
		// private llamadaFirebaseService: LlamadaFirebaseService
	) { 	}

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
		// this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
	}
}
