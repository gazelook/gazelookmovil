export enum RutasBuscadorGeneral {
    BUSCAR_PROYECTOS = 'proyectos',
    BUSCAR_NOTICIAS = 'noticias',
    BUSCAR_CONTACTOS = 'contactos',
    BUSCAR_PERFILES = 'usuarios',
}