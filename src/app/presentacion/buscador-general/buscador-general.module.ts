import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CompartidoModule } from '@shared/compartido.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuscadorGeneralRoutingModule } from 'src/app/presentacion/buscador-general/buscador-general-routing.module';
import { BuscadorGeneralComponent } from 'src/app/presentacion/buscador-general/buscador-general.component';
import { BuscarComponent } from 'src/app/presentacion/buscador-general/buscar/buscar.component';


@NgModule({
	declarations: [
		BuscadorGeneralComponent,
		BuscarComponent
	],
	imports: [
		CommonModule,
		BuscadorGeneralRoutingModule,
		CompartidoModule,
		TranslateModule,
		FormsModule,
	],
	exports: [
		CompartidoModule,
		TranslateModule,
		FormsModule,
	]
})
export class BuscadorGeneralModule { }
