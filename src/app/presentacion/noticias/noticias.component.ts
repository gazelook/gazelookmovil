import { Component, OnInit } from '@angular/core';
// import { LlamadaFirebaseService } from 'src/app/nucleo/servicios/generales/llamada/llamada-firebase.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';

@Component({
	selector: 'app-noticias',
	templateUrl: './noticias.component.html',
	styleUrls: ['./noticias.component.scss']
})
export class NoticiasComponent implements OnInit {

	constructor(
		private notificacionesUsuario: NotificacionesDeUsuario,
		// private llamadaFirebaseService: LlamadaFirebaseService
	) {   }

	ngOnInit(): void {
		this.notificacionesUsuario.validarEstadoDeLaSesion(false)
		// this.llamadaFirebaseService.suscribir(CodigosCatalogoEstatusLLamada.LLAMANDO)
	}
}
