import { CompartidoModule } from '@shared/compartido.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoticiasRoutingModule } from 'src/app/presentacion/noticias/noticias-routing.module';
import { NoticiasComponent } from 'src/app/presentacion/noticias/noticias.component';
import { PublicarComponent } from 'src/app/presentacion/noticias/publicar/publicar.component';
import { NoticiasUsuariosComponent } from 'src/app/presentacion/noticias/noticias-usuarios/noticias-usuarios.component';


@NgModule({
  declarations: [
    NoticiasComponent,
    PublicarComponent,
    NoticiasUsuariosComponent
  ],
  imports: [
    FormsModule,
    TranslateModule,
    CommonModule,
    CompartidoModule,
    NoticiasRoutingModule
  ],
  exports: [
    FormsModule,
    TranslateModule,
    CompartidoModule
  ]
})
export class NoticiasModule { }
