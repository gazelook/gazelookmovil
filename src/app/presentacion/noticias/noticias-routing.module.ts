import { NoticiasUsuariosComponent } from 'src/app/presentacion/noticias/noticias-usuarios/noticias-usuarios.component';
import { PublicarComponent } from 'src/app/presentacion/noticias/publicar/publicar.component';
import { RutasNoticias } from 'src/app/presentacion/noticias/rutas-noticias.enum';
import { NoticiasComponent } from 'src/app/presentacion/noticias/noticias.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: NoticiasComponent,
    children: [
      {
        path: RutasNoticias.PUBLICAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasNoticias.ACTUALIZAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasNoticias.VISITAR.toString(),
        component: PublicarComponent
      },
      {
        path: RutasNoticias.NOTICIAS_USUARIOS.toString(),
        component: NoticiasUsuariosComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoticiasRoutingModule { }
