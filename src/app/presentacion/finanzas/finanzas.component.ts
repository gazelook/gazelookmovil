import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import {
	EspesorLineaItem,
	ColorFondoLinea,
	ColorDelTexto,
	EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { Location } from '@angular/common';
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';

import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ColorTextoBoton, TipoBoton } from '@shared/componentes/button/button.component';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
// import { LlamadaFirebaseService } from '@core/servicios/generales/llamada/llamada-firebase.service';
import { ConfiguracionDialogoInline } from '@shared/diseno/modelos/dialogo-inline.interface';
import { PaddingIzqDerDelTexto } from '@shared/diseno/enums/estilos-padding-general';

@Component({
	selector: 'app-finanzas',
	templateUrl: './finanzas.component.html',
	styleUrls: ['./finanzas.component.scss']
})
export class FinanzasComponent implements OnInit {
	@ViewChild('toast', { static: false }) toast: ToastComponent;


	configuracionAppBar: ConfiguracionAppbarCompartida;
	perfilSeleccionado: PerfilModel;
	confLinea: LineaCompartida;

	//botones
	botonCompra: BotonCompartido
	botonIntercambio: BotonCompartido

	botonBuscarCultural: BotonCompartido;
	botonPublicarCultural: BotonCompartido;

	// lista
	mostrarCuotaAnual: boolean;
	mostrarCuotaUsuario: boolean;
	mostrarReportes: boolean;

	// Dialogos
	public confDialogoSinIngresos: ConfiguracionDialogoInline
	public confDialogoAlianzas: ConfiguracionDialogoInline
	public confDialogoProyectoSeleccionado: ConfiguracionDialogoInline
	public confDialogoAdicionalProSelec: ConfiguracionDialogoInline
	public confToast: ConfiguracionToast;


	constructor(
		private perfilNegocio: PerfilNegocio,
		private _location: Location,
		private variablesGlobales: VariablesGlobales,
		private notificacionesUsuario: NotificacionesDeUsuario,
		// private llamadaFirebaseService: LlamadaFirebaseService,
		public estilosDelTextoServicio: EstiloDelTextoServicio

	) {
		this.variablesGlobales.mostrarMundo = false
		this.mostrarCuotaAnual = false;
		this.mostrarCuotaUsuario = false;
		this.mostrarReportes = false;
	}

	ngOnInit(): void {
		this.variablesGlobales.mostrarMundo = false
		this.obtenerPerfil()
		this.configurarToast()
		this.configurarDialogos()
		if (this.perfilSeleccionado) {
			this.prepararAppBar()
			this.configuracionBotones()
			this.notificacionesUsuario.validarEstadoDeLaSesion(false)
		} else {
			this.perfilNegocio.validarEstadoDelPerfil(this.perfilSeleccionado)
		}
	}

	obtenerPerfil() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado();
	}

	configurarToast() {
		this.confToast = {
			mostrarToast: false, //True para mostrar
			mostrarLoader: false, // true para mostrar cargando en el toast
			cerrarClickOutside: false, // falso para que el click en cualquier parte no cierre el toast
		};
	}

	prepararAppBar() {
		this.configuracionAppBar = {
			accionAtras: () => {
				this._location.back()
			},
			usoAppBar: UsoAppBar.USO_SEARCHBAR_APPBAR,
			searchBarAppBar: {
				mostrarDivBack: {
					icono: true,
					texto: true
				},
				mostrarLineaVerde: true,
				mostrarTextoHome: true,
				mostrarBotonXRoja: false,
				tamanoColorFondo: TamanoColorDeFondo.TAMANO100,
				nombrePerfil: {
					mostrar: true,
					llaveTexto: this.perfilSeleccionado?.tipoPerfil.nombre
				},
				buscador: {
					mostrar: false,
					configuracion: {
						disable: true,

					}
				},
				subtitulo: {
					mostrar: true,
					llaveTexto: 'm7v1texto1'
				},

			}

		}

	}

	// Configurar linea verde
	configurarLinea() {
		this.confLinea = {
			ancho: AnchoLineaItem.ANCHO6382,
			colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
			espesor: EspesorLineaItem.ESPESOR071,
			forzarAlFinal: false
		}
	}



	configuracionBotones() {
		this.botonBuscarCultural = {
			text: 'm6v5texto5',
			tamanoTexto: TamanoDeTextoConInterlineado.L3_I2,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => {
				// this.router.navigateByUrl(RutasLocales.COMPRAS_INTERCAMBIOS+ '/'+ RutasCompra.INTERCAMBIO)
			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

		this.botonPublicarCultural = {
			text: 'm6v5texto6', 
			tamanoTexto: TamanoDeTextoConInterlineado.L3_I2,
			colorTexto: ColorTextoBoton.AMARRILLO,
			ejecutar: () => {
				// this.router.navigateByUrl(RutasLocales.COMPRAS_INTERCAMBIOS+ '/'+ RutasCompra.COMPRA)

			},
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO,
		};

	}


	abrirCuotaAnual() {
		this.reiniciarDialogos()
		if (this.mostrarCuotaAnual) {
			this.mostrarCuotaAnual = !this.mostrarCuotaAnual
			return
		}

		this.reiniciarFee()
		this.mostrarCuotaAnual = !this.mostrarCuotaAnual


	}

	abrirCuotaUsuario() {
		this.reiniciarDialogos()
		if (this.mostrarCuotaUsuario) {
			this.mostrarCuotaUsuario = !this.mostrarCuotaUsuario
			return
		}
		this.reiniciarFee()
		this.mostrarCuotaUsuario = !this.mostrarCuotaUsuario
	}

	abrirReportes() {
		this.toast.abrirToast('text81')
		// this.reiniciarDialogos()
		// if (this.mostrarReportes) {
		// 	this.mostrarReportes = !this.mostrarReportes
		// 	return
		// }
		// this.reiniciarFee()
		// this.mostrarReportes = !this.mostrarReportes;

	}


	abrirAnunciosFinanzas() {
		this.reiniciarFee()
		if (!this.confDialogoSinIngresos.noMostrar) {
			this.confDialogoSinIngresos.noMostrar = true

			return
		}
		if (this.confDialogoSinIngresos.noMostrar) {
			this.confDialogoSinIngresos.noMostrar = false

			this.confDialogoProyectoSeleccionado.noMostrar = true
			this.confDialogoAdicionalProSelec.noMostrar = true
			this.confDialogoAlianzas.noMostrar = true

			return
		}

	}
	abrirGazeAlianzas() {

		this.reiniciarFee()

		if (!this.confDialogoAlianzas.noMostrar) {
			this.confDialogoAlianzas.noMostrar = true
			return
		}
		if (this.confDialogoAlianzas.noMostrar) {

			this.confDialogoSinIngresos.noMostrar = true
			this.confDialogoProyectoSeleccionado.noMostrar = true
			this.confDialogoAdicionalProSelec.noMostrar = true
			this.confDialogoAlianzas.noMostrar = false
			return
		}




	}

	abrirFundsProject() {
		this.reiniciarFee()
		if (!this.confDialogoProyectoSeleccionado.noMostrar) {
			this.confDialogoProyectoSeleccionado.noMostrar = true

			return
		}
		if (this.confDialogoProyectoSeleccionado.noMostrar) {
			this.confDialogoProyectoSeleccionado.noMostrar = true

			this.confDialogoProyectoSeleccionado.noMostrar = false
			this.confDialogoAdicionalProSelec.noMostrar = true
			this.confDialogoAlianzas.noMostrar = true

			return
		}
	}

	abrirAdditionalExpenses() {
		this.reiniciarFee()
		if (!this.confDialogoAdicionalProSelec.noMostrar) {
			this.confDialogoAdicionalProSelec.noMostrar = true

			return
		}
		if (this.confDialogoAdicionalProSelec.noMostrar) {
			this.confDialogoAdicionalProSelec.noMostrar = true

			this.confDialogoProyectoSeleccionado.noMostrar = true
			this.confDialogoAdicionalProSelec.noMostrar = false
			this.confDialogoAlianzas.noMostrar = true

			return
		}
	} 

	configurarDialogos() {
		// Proyectos seleccionados
		this.confDialogoSinIngresos = {
			noMostrar: true,
			descripcion: [],
			listaBotones: [],
			sinMargenEnDescripcion: true,
			anchoParaProyectos: true
		}

		this.confDialogoSinIngresos.descripcion.push(
			{
				texto: 'm7v4texto3',
				estilo: {
					color: ColorDelTexto.TEXTOROJOBASE,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
					paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0
				}
			},
			{
				texto: 'm7v1texto20',
				estilo: {
					color: ColorDelTexto.TEXTOAZULBASE,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
					paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0
				}
			},

		)
		this.confDialogoAlianzas = {
			noMostrar: true,
			descripcion: [],
			listaBotones: [],
			sinMargenEnDescripcion: true,
			anchoParaProyectos: true
		}

		this.confDialogoAlianzas.descripcion.push(
			{
				texto: 'm7v1texto18',
				estilo: {
					color: ColorDelTexto.TEXTOAZULBASE,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
					paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0
				}
			},

		)

		this.confDialogoProyectoSeleccionado = {
			noMostrar: true,
			descripcion: [],
			listaBotones: [],
			sinMargenEnDescripcion: true,
			anchoParaProyectos: true
		}

		this.confDialogoProyectoSeleccionado.descripcion.push(
			{
				texto: 'm7v1texto19',
				estilo: {
					color: ColorDelTexto.TEXTOAZULBASE,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
					paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0
				}
			},

		)
		this.confDialogoAdicionalProSelec = {
			noMostrar: true,
			descripcion: [],
			listaBotones: [],
			sinMargenEnDescripcion: true,
			anchoParaProyectos: true
		}

		this.confDialogoAdicionalProSelec.descripcion.push(
			{
				texto: 'm7v1texto19',
				estilo: {
					color: ColorDelTexto.TEXTOAZULBASE,
					estiloTexto: EstilosDelTexto.BOLD,
					enMayusculas: true,
					tamanoConInterlineado: TamanoDeTextoConInterlineado.L2_I1,
					paddingIzqDerDelTexto: PaddingIzqDerDelTexto.PADDING_0
				}
			},

		)
	}

	reiniciarDialogos() {
		this.confDialogoSinIngresos.noMostrar = true
		this.confDialogoAlianzas.noMostrar = true
		this.confDialogoProyectoSeleccionado.noMostrar = true
		this.confDialogoAdicionalProSelec.noMostrar = true
	}
	reiniciarFee() {
		this.mostrarCuotaAnual = false;
		this.mostrarCuotaUsuario = false;
		this.mostrarReportes = false;
	}
}
