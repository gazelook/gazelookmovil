import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanzasRoutingModule } from 'src/app/presentacion/finanzas/finanzas-routing.module';
import { FinanzasComponent } from 'src/app/presentacion/finanzas/finanzas.component';
import { CompartidoModule } from '@shared/compartido.module';
import { TranslateModule } from '@ngx-translate/core';
import { CuotaAnualComponent } from 'src/app/presentacion/finanzas/cuota-anual/cuota-anual.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CuotaUsuarioComponent } from 'src/app/presentacion/finanzas/cuota-usuario/cuota-usuario.component';
import { TrustAccountComponent } from 'src/app/presentacion/finanzas/trust-account/trust-account.component';
import { NgxCurrencyModule } from 'ngx-currency';


@NgModule({
  declarations: [FinanzasComponent, CuotaAnualComponent, CuotaUsuarioComponent, TrustAccountComponent],
  imports: [
    NgxCurrencyModule,
    CommonModule,
    FinanzasRoutingModule,
    CompartidoModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    TranslateModule,
    NgxCurrencyModule
  ]
})
export class FinanzasModule { }
