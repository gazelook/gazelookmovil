import { AccionesSelector } from './../../../compartido/diseno/enums/acciones-general.enum';
import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ColorTextoBoton,
  TipoBoton,
} from 'src/app/compartido/componentes/button/button.component';
import { ToastComponent } from 'src/app/compartido/componentes/toast/toast.component';

import {
  EstilosDelTexto,
  ColorDelTexto,
  ColorDeBorde,
  ColorDeFondo,
  EspesorDelBorde
} from '../../../compartido/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from 'src/app/compartido/diseno/enums/estilos-tamano-general.enum';
import { BotonCompartido } from 'src/app/compartido/diseno/modelos/boton.interface';
import { ItemSelector } from 'src/app/compartido/diseno/modelos/elegible.interface';
import { InfoAccionSelector } from 'src/app/compartido/diseno/modelos/info-accion-selector.interface';
import {
  ConfiguracionMonedaPicker,
  ResumenDataMonedaPicker,
  ValorBase,
} from 'src/app/compartido/diseno/modelos/moneda-picker.interface';
import { ConfiguracionToast } from 'src/app/compartido/diseno/modelos/toast.interface';
import { FinanzasNegocio } from 'src/app/dominio/logica-negocio/finanzas.negocio';
import { InternacionalizacionNegocio } from 'src/app/dominio/logica-negocio/internacionalizacion.negocio';
import { TipoMonedaNegocio } from 'src/app/dominio/logica-negocio/moneda.negocio';
import { PerfilNegocio } from 'src/app/dominio/logica-negocio/perfil.negocio';
import { CatalogoTipoMonedaModel } from 'src/app/dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { PerfilModel } from 'src/app/dominio/modelo/entidades/perfil.model';
import { EstiloDelTextoServicio } from 'src/app/nucleo/servicios/diseno/estilo-del-texto.service';
import { MonedaPickerService } from 'src/app/nucleo/servicios/generales/moneda-picker.service';
import { CatalogoOrigenDocumentos } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalogo-origen-documentos.enum';

@Component({
  selector: 'app-trust-account',
  templateUrl: './trust-account.component.html',
  styleUrls: ['./trust-account.component.scss'],
})
export class TrustAccountComponent implements OnInit {
  @ViewChild('toast', { static: false }) toast: ToastComponent;
  // Parametros internos
  public fechaInicial: Date;
  public fechaFinal: Date;
  public filtroActivo: string;
  public filtroPorTitulo: boolean;
  public metodoBusqueda: number;
  public perfilSeleccionado: PerfilModel;
  public idCapaCuerpo: string;

  // Configuracion de capas
  public mostrarCapaLoader: boolean;
  public mostrarCapaError: boolean;
  public mensajeCapaError: string;
  public mostrarCapaNormal: boolean;
  public puedeCargarMas: boolean;
  public email: string;
  public miPerfil: PerfilModel;
  public valorBaseAPagar: ValorBase;

  public confMonedaPicker: ConfiguracionMonedaPicker;
  botonReporteSimple: BotonCompartido;
  botonReporteFecha: BotonCompartido;
  public confToast: ConfiguracionToast;

  constructor(
    private perfilNegocio: PerfilNegocio,
    private FinanzasNegocio: FinanzasNegocio,
    private tipoMonedaNegocio: TipoMonedaNegocio,
    private monedaPickerService: MonedaPickerService,
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    private internacionalizacionNegocio: InternacionalizacionNegocio
  ) {
    this.mostrarCapaLoader = false;
    this.mostrarCapaError = false;
    this.puedeCargarMas = true;
    this.mensajeCapaError = '';
    this.fechaInicial = new Date();
    this.fechaFinal = new Date();
  }

  ngOnInit(): void {
    this.configurarMonedaPicker();
    this.configurarBotones();
    this.configurarToast();
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    };
  }


  configurarMonedaPicker() {
    const dataMoneda: ResumenDataMonedaPicker = this.obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto();

    this.confMonedaPicker = {
      inputCantidadMoneda: {
        valor: dataMoneda.valorEstimado,
        colorFondo: ColorDeFondo.FONDO_BLANCO,
        colorTexto: ColorDelTexto.TEXTOAZULBASE,
        tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
        estiloDelTexto: EstilosDelTexto.BOLD,
        ocultarInput: true,
      },
      selectorTipoMoneda: {
        titulo: {
          mostrar: false,
          llaveTexto: 'm2v9texto15',
        },
        inputTipoMoneda: {
          valor: dataMoneda.tipoMoneda,
          colorFondo: ColorDeFondo.FONDO_BLANCO,
          colorTexto: ColorDelTexto.TEXTOAZULBASE,
          tamanoDelTexto: TamanoDeTextoConInterlineado.L3_IGUAL,
          estiloDelTexto: EstilosDelTexto.BOLD,
          estiloBorde: {
            espesor: EspesorDelBorde.ESPESOR_018,
            color: ColorDeBorde.BORDER_NEGRO,
          },
        },
        elegibles: [],
        seleccionado: dataMoneda.tipoMoneda.seleccionado,
        mostrarSelector: false,
        evento: (accion: InfoAccionSelector) =>
          this.eventoEnSelectorDeTipoMoneda(accion),
        mostrarLoader: false,
        error: {
          mostrar: false,
          llaveTexto: '',
        },
      },
    };
  }

  async configurarBotones() {
    this.botonReporteSimple = {
      text: await this.internacionalizacionNegocio.obtenerTextoLlave('m7v6texto5'), //
      tamanoTexto: TamanoDeTextoConInterlineado.L6_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.obtenerReporte(),
    };

    this.botonReporteFecha = {
      text: await this.internacionalizacionNegocio.obtenerTextoLlave('m7v7texto8'),
      tamanoTexto: TamanoDeTextoConInterlineado.L6_IGUAL,
      colorTexto: ColorTextoBoton.CELESTE,
      tipoBoton: TipoBoton.TEXTO,
      enProgreso: false,
      ejecutar: () => this.obtenerReporte(this.fechaInicial),
    };
  }

  // Inicializar data catalogo moneda
  async inicializarDataCatalogoMoneda() {
    this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = true;
    this.tipoMonedaNegocio.obtenerCatalogoTipoMonedaParaElegibles().subscribe(
      (elegibles) => {
        this.tipoMonedaNegocio.validarcatalogoMonedaEnLocalStorage(elegibles);
        this.confMonedaPicker.selectorTipoMoneda.elegibles = elegibles;
        this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
        this.confMonedaPicker.selectorTipoMoneda.error.mostrar = false;
      },
      (error) => {
        this.confMonedaPicker.selectorTipoMoneda.elegibles = [];
        this.confMonedaPicker.selectorTipoMoneda.error.llaveTexto = 'text31';
        this.confMonedaPicker.selectorTipoMoneda.error.mostrar = true;
        this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
      }
    );
  }

  // Abrir Selector
  abrirSelectorMoneda() {
    this.confMonedaPicker.selectorTipoMoneda.mostrarSelector = true;
    this.inicializarDataCatalogoMoneda();
  }

  seleccionarTipoMoneda(item: ItemSelector) {
    // Ocultar selector
    this.confMonedaPicker.selectorTipoMoneda.mostrarSelector = false;
    // Seleccionar el item
    this.confMonedaPicker.selectorTipoMoneda.seleccionado = item;
    this.confMonedaPicker.selectorTipoMoneda.mostrarLoader = false;
    // Mostrar preview
    this.confMonedaPicker.selectorTipoMoneda.inputTipoMoneda.valor.valorFormateado =
      item.auxiliar || '';
  }

  // Metodos del selector de moneda
  eventoEnSelectorDeTipoMoneda(accion: InfoAccionSelector) {
    switch (accion.accion) {
      case AccionesSelector.ABRIR_SELECTOR:
        this.abrirSelectorMoneda();
        break;
      case AccionesSelector.SELECCIONAR_ITEM:
        this.seleccionarTipoMoneda(accion.informacion);
        break;
      case AccionesSelector.REINTERTAR_CONTENIDO:
        this.abrirSelectorMoneda();
        break;
      default:
        break;
    }
  }

  obtenerValorEstimadoJuntoConElTipoDeMonedaDelProyecto(): ResumenDataMonedaPicker {
    const valorEstimado =
      this.valorBaseAPagar && this.valorBaseAPagar.valorNeto
        ? parseInt(this.valorBaseAPagar.valorNeto)
        : 0;
    const moneda: CatalogoTipoMonedaModel = {
      codigo: this.valorBaseAPagar?.seleccionado?.codigo || '',
      codNombre: this.valorBaseAPagar?.seleccionado?.auxiliar || '',
    };

    const data = {
      valorEstimado: this.monedaPickerService.obtenerValorAPagarDeLaSuscripcion(
        valorEstimado
      ),
      tipoMoneda: this.monedaPickerService.obtenerTipoDeMonedaActual(moneda),
    };
    return data;
  }

  obtenerClasesInputCantidadMoneda(): any {
    const clases = {};
    clases['input-cantidad'] = true;
    clases[
      this.confMonedaPicker.inputCantidadMoneda.colorFondo.toString()
    ] = true;
    clases[
      this.confMonedaPicker.inputCantidadMoneda.colorTexto.toString()
    ] = true;
    clases[
      this.confMonedaPicker.inputCantidadMoneda.estiloDelTexto.toString()
    ] = true;
    clases[
      this.confMonedaPicker.inputCantidadMoneda.tamanoDelTexto.toString()
    ] = true;

    if (this.confMonedaPicker.inputCantidadMoneda.estiloBorde) {
      clases[
        this.confMonedaPicker.inputCantidadMoneda.estiloBorde.toString()
      ] = true;
    }

    return clases;
  }

  validarFechasIngresadas() {
    if (this.fechaInicial.getTime() > this.fechaFinal.getTime()) {
      return false;
    }

    return true;
  }

  cambioDeFecha(orden: number) {
    if (orden === 0) { }

    if (orden === 1) { }
  }

  obtenerReporte(fecha?: Date) {
    let fechaInicial = '';
    let origen = CatalogoOrigenDocumentos.CUENTA_BANCARIA;
    if (fecha) {
      this.toast.abrirToast('text31')
     
    } else {
      this.toast.abrirToast('text31')


    }
  }
  async descargarDocumento(data) {
    if (data.lista) {

      let linkPDF = document.createElement('a');
      const nombre = `${data.lista[0].adjuntos[0].traducciones[0].descripcion}.pdf`;

      linkPDF.href = data.lista[0].adjuntos[0].principal.url;
      linkPDF.download = nombre;
      linkPDF.dispatchEvent(new MouseEvent('click'));

    } else {
      this.toast.abrirToast(await this.internacionalizacionNegocio.obtenerTextoLlave('NO_EXISTE_COINCIDENCIAS'));
    }
  }
}
