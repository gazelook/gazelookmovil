import { CommonModule } from '@angular/common';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  BrowserModule,
  HammerModule,
  HAMMER_GESTURE_CONFIG,
} from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
// =====Modulos traducciones
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// import { NgxAgoraModule } from 'ngx-agora';
import { NgxCurrencyModule } from 'ngx-currency';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxStripeModule } from 'ngx-stripe';
import { WebcamModule } from 'ngx-webcam';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from 'src/app/app-routing.module';
// DIRECTIVAS
// COMPONENTES
import { AppComponent } from 'src/app/app.component';
import { CompartidoModule } from '@shared/compartido.module';
import { RestriccionRutas } from '@core/servicios/generales/canActivate/resticcionRutas.service';
import { RutasInicioSession } from '@core/servicios/generales/canActivate/rutas-inicio-session.service';
import { DetectorGestos } from '@core/servicios/generales/detector-gestos.service';
// SERVICIOS
// import { ApiService } from '@core/servicios/locales/api'
import { HandleError } from '@core/servicios/locales/handleError.service';
import { IdiomaService } from '@core/servicios/remotos/idioma.service';
import { PeticionInterceptor } from '@core/servicios/remotos/peticion.interceptor';
import { AnuncioComponent } from 'src/app/presentacion/anuncio/anuncio.component';
import { AnunciosSistemaComponent } from 'src/app/presentacion/anuncios-sistema/anuncios-sistema.component';
import { BienvenidaComponent } from 'src/app/presentacion/bienvenida/bienvenida.component';
import { ContactoComponent } from 'src/app/presentacion/contacto/contacto.component';
import { FincaComponent } from 'src/app/presentacion/finca/finca.component';
import { LandingComponent } from 'src/app/presentacion/landing/landing.component';
// Test
import { LoginComponent } from 'src/app/presentacion/login/login.component';
import { MenuPerfilesComponent } from 'src/app/presentacion/menu-perfiles/menu-perfiles.component';
import { MenuPrincipalComponent } from 'src/app/presentacion/menu-principal/menu-principal.component';
import { MenuPublicarProyectoNoticiaComponent } from 'src/app/presentacion/menu-publicar-proyecto-noticia/menu-publicar-proyecto-noticia.component';
import { MenuSeleccionPerfilesComponent } from 'src/app/presentacion/menu-seleccion-perfiles/menu-seleccion-perfiles.component';
import { MetodoPagoComponent } from 'src/app/presentacion/metodo-pago/metodo-pago.component';
import { MiCuentaComponent } from 'src/app/presentacion/mi-cuenta/mi-cuenta.component';
import { NuestrasMetasComponent } from 'src/app/presentacion/nuestras-metas/nuestras-metas.component';
import { RegistroComponent } from 'src/app/presentacion/registro/registro.component';
import { DocLegalesComponent } from './presentacion/doc-legales/doc-legales.component';
import { CuotaExtraComponent } from './presentacion/cuota-extra/cuota-extra.component';
import { ReembolsosPagosComponent } from './presentacion/reembolsos-pagos/reembolsos-pagos.component';
import { MetodosPagoComponent } from './presentacion/metodos-pago/metodos-pago.component';
import { MetodoStripeComponent } from './presentacion/cuota-extra/metodo-stripe/metodo-stripe.component';
import { MetodoPaymentezComponent } from './presentacion/cuota-extra/metodo-paymentez/metodo-paymentez.component';

// Traducciones
export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  // return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    MetodoPagoComponent,
    MenuPerfilesComponent,
    BienvenidaComponent,
    MenuPrincipalComponent,
    MenuSeleccionPerfilesComponent,
    ContactoComponent,
    MenuPublicarProyectoNoticiaComponent,
    MiCuentaComponent,
    NuestrasMetasComponent,
    LandingComponent,
    FincaComponent,
    AnuncioComponent,
    AnunciosSistemaComponent,
    DocLegalesComponent,
    CuotaExtraComponent,
    ReembolsosPagosComponent,
    MetodosPagoComponent,
    MetodoStripeComponent,
    MetodoPaymentezComponent,
  ],
  imports: [
    NgxCurrencyModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    CompartidoModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
      // extend:true
    }),

    HammerModule,
    WebcamModule,
    ImageCropperModule,
    NgxStripeModule.forRoot(environment.pkStripe),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    // NgxAgoraModule.forRoot({ AppID: environment.agora.appId }),
    AngularFireModule.initializeApp(environment.firebase),
  ],
  exports: [CompartidoModule, TranslateModule, NgxCurrencyModule],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: DetectorGestos },
    { provide: HTTP_INTERCEPTORS, useClass: PeticionInterceptor, multi: true },
    // { provide: LocationStrategy, useClass: PathLocationStrategy },
    HandleError,
    IdiomaService,
    RestriccionRutas,
    RutasInicioSession,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
