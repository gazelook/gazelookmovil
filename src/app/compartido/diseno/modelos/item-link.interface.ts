import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
export interface ConfiguracionItemLink {
    entidad: CodigosCatalogoEntidad,
    fueVisitado?: boolean,
    esPortada?: boolean,
    media?: MediaModel,
    eventoTap?: Function,
    eventoDobleTap?: Function,
    eventoPress?: Function,
    albumPredeterminado?: boolean
}
