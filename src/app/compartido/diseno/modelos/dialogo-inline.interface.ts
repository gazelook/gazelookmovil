import { ConfiguracionTexto } from '@shared/diseno/modelos/texto.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface'
export interface ConfiguracionDialogoInline {
    noMostrar?: boolean,
    sinMargenEnDescripcion?: boolean,
    anchoParaProyectos?: boolean,
    descripcion: Array<{
        texto: string,
        estilo: ConfiguracionTexto
    }>,
    listaBotones: Array<BotonCompartido>,
}
