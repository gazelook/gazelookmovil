import {
  ColorDeBorde,
  ColorDeFondo, ColorDelTexto, EspesorDelBorde, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';

export interface TextoMoneda {
  mostrar: boolean,
  llaveTexto?: string,
}

export interface BordeInputMoneda {
  espesor: EspesorDelBorde,
  color: ColorDeBorde
}

export interface ValorBase {
  contador?: number, // Indica cuanto digitos han sido ingresados por el usuario
  valorNeto?: string, // Valor sin puntos, al inicio sera solo ceros
  valorFormateado: string, // Valor con puntos y demas
  placeholder?: string,
  seleccionado?: ItemSelector,
}

export interface InputMoneda {
  valor: ValorBase,
  colorFondo: ColorDeFondo,
  colorTexto: ColorDelTexto,
  estiloDelTexto: EstilosDelTexto,
  tamanoDelTexto: TamanoDeTextoConInterlineado,
  estiloBorde?: BordeInputMoneda,
  evento?: Function,
  ocultarInput?: boolean,
  soloLectura?: boolean,
}

export interface SelectorTipoMoneda {
  titulo: TextoMoneda,
  inputTipoMoneda: InputMoneda,
  elegibles: Array<ItemSelector>,
  seleccionado: ItemSelector,
  mostrarSelector: boolean,
  evento?: Function,
  mostrarLoader: boolean,
  error: {
    mostrar: boolean,
    llaveTexto: string
  },
  soloLectura?: boolean
}

export interface ConfiguracionMonedaPicker {
  inputCantidadMoneda: InputMoneda,
  selectorTipoMoneda: SelectorTipoMoneda,
}

export interface ResumenDataMonedaPicker {
  valorEstimado: ValorBase,
  tipoMoneda: ValorBase
}
