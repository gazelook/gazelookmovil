import {CongifuracionItemProyectosNoticias} from '@shared/diseno/modelos/item-proyectos-noticias.interface';


export interface CongifuracionItemProyectos extends CongifuracionItemProyectosNoticias {
  totalVotos?: number;
  positionStart?: boolean;
  positionEnd?: boolean;
  positionCenter?: boolean;
  positionCenterRigth?: boolean;
  numeroProyecto?: number;
  montoEntregado?: number;
  montoFaltante?: number;
}
