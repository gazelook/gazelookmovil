import { MensajeError } from "@shared/diseno/modelos/error.interface";
export interface ConfiguracionCamaraVideo {
    mostrarModal?: boolean,
    mostrarLoader?: boolean,
    mensajeError?: MensajeError,
    usarCroopper?: boolean,
    videoRealizado?: Function,
    cerrarCamara?:  Function
}
