import { PerfilEntity } from 'dominio/entidades/perfil.entity';

export interface ConfiguracionListaNoticias {
  cargando?: boolean,
  id?: string,
  titulo?: string;
  subtitulo?: string,
  esMiPerfil?: boolean,
  perfilSeleccionado?: PerfilEntity,
  resumen?: boolean,
  ultimaPagina?: boolean;
  evento?: Function,
  eventoScroll?: Function,
  noticias?: Array<any>
}

