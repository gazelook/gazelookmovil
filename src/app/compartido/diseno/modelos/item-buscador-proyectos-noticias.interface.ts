import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum'

export interface ConfiguracionItemBuscadorProyectoNoticia {
    codigoEntidad?: CodigosCatalogoEntidad,
    noticia?: NoticiaModel,
    proyecto?: ProyectoModel,
    configuracionRectangulo?: CongifuracionItemProyectosNoticias,
    eventoTap?: Function
}