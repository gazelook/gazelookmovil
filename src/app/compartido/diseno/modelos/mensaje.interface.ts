import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { CatalogoEntidadModel } from 'dominio/modelo/catalogos/catalogo-entidad.model';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { TipoLlamada } from '@shared/diseno/enums/llamada-agora.enum';
import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { MediaEntity } from 'dominio/entidades/media.entity';
import { TipoMensaje } from '@shared/diseno/enums/mensaje.enum';
export interface ConfiguracionMensajeCompartido {
    id?: string
    identificadorTemporal?: number
    contenido?: string
    propietario?: ParticipanteAsociacionModel
    fechaCreacion?: Date
    enviado?: boolean,
    reenvio?: boolean,
    eventoTap?: Function
    eventoDobleTap?: Function
    eventoPress?: Function
    loader?: boolean
    mostrar?: boolean
    error?: boolean
    tipoMensaje?: string
    estatusMensaje?: string
    referenciaEntidadCompartida?: any
    entidadCompartida?: CatalogoEntidadModel
} 
export interface ConfiguracionMensaje extends ConfiguracionMensajeCompartido {
    adjuntos?: Array<MediaEntity>,
    listaConfiguracionImagenes?: Array<ImagenMensajeCompartido>
    listaConfiguracionAudios?: Array<AudioMensajeCompartido>
    listaConfiguracionDocumentos?: Array<DocumentoMensajeCompartido>
    listaConfiguracionMensajeCompartidoProyecto?: Array<MensajeEntidadCompartido>
    listaConfiguracionMensajeCompartidoNoticia?: Array<MensajeEntidadCompartido>
    
}
export interface AudioMensajeCompartido extends ConfiguracionMensajeCompartido {
    adjunto: MediaModel
}

export interface DocumentoMensajeCompartido extends ConfiguracionMensajeCompartido {
    adjunto: MediaModel
}

export interface ImagenMensajeCompartido extends ConfiguracionMensajeCompartido {
    adjunto: MediaModel
    mostrarLoader?: boolean
}

export interface TextoMensajeCompartido extends ConfiguracionMensajeCompartido {

}

export interface MensajeEntidadCompartido extends ConfiguracionMensajeCompartido {
    configuracionEntidad: CongifuracionItemProyectosNoticias,
    listaAcciones?: Array<BotonCompartido>,
    infoAccion?: string,
}

export interface LlamadaMensajeCompartido extends ConfiguracionMensajeCompartido {
    tipoLlamada: TipoLlamada
}

export interface MensajeAccion {
    aceptarTransferPro?: boolean
    id?: string,
    url?: string,
    tipoMensaje?: TipoMensaje,

}