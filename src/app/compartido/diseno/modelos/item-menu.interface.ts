import { ColorDelTexto } from '@shared/diseno/enums/estilos-colores-general';
import { LineaDeTexto } from '@shared/diseno/modelos/linea-de-texto.interface'
import { TamanoItemMenu } from '../enums/estilos-tamano-general.enum'
import { ColorFondoItemMenu } from '@shared/diseno/enums/estilos-colores-general'
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface'
import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';
import { ItemMenuModel, ItemAccion } from 'dominio/modelo/entidades/item-menu.model';

// Configuracion del item menu
export interface ItemMenuCompartido {
    id: string, // Id del item
    tamano: TamanoItemMenu, // Indica el tamano del item (altura)
    colorFondo: ColorFondoItemMenu, // El color de fondo que tendra el item
    //lineasTexto: Array<LineaDeTexto>, // Todas las lineas de texto a ser mostradas en el item
    colorTexto?: ColorDelTexto
    linea: {
        mostrar: boolean, // Indica si se debe mostrar o no la linea
        configuracion?: LineaCompartida // Configuracion de la linea
    },
    gazeAnuncios?: boolean, // Indica si el item es para Gazelook Announcemente, lo hice como variable porque es un caso en particular
    idInterno?: string,
    mostrarDescripcion?: boolean

    onclick?: Function,
    dobleClick?: Function
    clickSostenido?: Function,

    tipoMenu: TipoMenu 
    texto1?: string,
    texto2?: string,
    texto3?: string,
    texto4?: string,
    texto5?: string,
    texto6?: string
    descripcion?: Array<LineaDeTexto> // Cuando se agrega la descripcion, se visualizara el icono de flecha y se omitira el click
    submenus?: ItemMenuModel[]
    acciones?: ItemAccion[],
    mostrarConrazon?: boolean
}

