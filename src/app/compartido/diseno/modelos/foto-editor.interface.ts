import { MensajeError } from "@shared/diseno/modelos/error.interface";
export interface ConfiguracionCamara {
    mostrarModal: boolean,
    mostrarLoader: boolean,
    mensajeError: MensajeError,
    usarCroopper?: boolean,
    fotoCapturada?: Function,
    fotoCapturadaDni?: Function,

    esDni?: boolean
}
export interface ConfiguracionCropper {
  mostrarModal: boolean,
  mostrarLoader: boolean,
  mostrarCaja: boolean,
  mensajeError: MensajeError,
  imageURL?: any,
  imageChangedEvent?: any,
  imageFile?: any,
  imageBase64?: string,
  imagenCortada?: Function,
}
