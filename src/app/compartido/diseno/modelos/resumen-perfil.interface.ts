import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { PensamientoCompartido } from '@shared/diseno/modelos/pensamiento';
import { AlturaResumenPerfil } from '@shared/diseno/enums/altura-resumen-perfil.enum';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';

export interface ConfiguracionResumenPerfil {
    configCirculoFoto?: ItemCircularCompartido,
    alturaModal: AlturaResumenPerfil,
    configuracionPensamiento?: PensamientoCompartido,
    titulo?: string,
    botones?: Array<BotonCompartido>,
    confDataListaPensamientos?: DatosLista,
    mostrar?: boolean,
    loader?: boolean
    error?: boolean,
    reintentar?: Function,
    sonContactos?: boolean
}
