import { EspesorLineaItem, ColorFondoLinea } from '@shared/diseno/enums/estilos-colores-general';
import { AnchoLineaItem } from "@shared/diseno/enums/ancho-linea-item.enum"

// Configuracion de la linea
export interface LineaCompartida {
    ancho:AnchoLineaItem,
    espesor:EspesorLineaItem,
    colorFondo:ColorFondoLinea,
    forzarAlFinal?:boolean, // Ubica la linea con posicion absoluta al final del elemento padre, tomar en cuenta que el padre debe tener position relative
    cajaGaze?:boolean,
} 