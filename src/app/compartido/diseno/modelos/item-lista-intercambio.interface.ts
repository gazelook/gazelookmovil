import { ConfiguracionItemIntercambio } from '@shared/diseno/modelos/item-intercambio.interface';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum'

export interface ConfiguracionItemListaIntercambio {
    codigoEntidad?: CodigosCatalogoEntidad,
    intercambio?: IntercambioModel
    configuracionRectangulo?: ConfiguracionItemIntercambio,
    eventoTap?: Function
}