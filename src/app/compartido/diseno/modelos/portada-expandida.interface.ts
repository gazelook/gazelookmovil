import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';

export enum TipoBloqueBortada {
    BOTON_PROYECTO = 'bloque-boton',
    INFO_PROYECTO = 'bloque-info',
    NO_ADDED_INFO = 'bloque-noadded',
    ACTUALUZADO_INFO = 'actualizado-info',
    BOTON_NOTICIA = 'bloque-boton-noticia',
}

export enum ColoresBloquePortada {
    AZUL_FUERTE = 'azul-fuerte',
    AZUL_FUERTE_CON_OPACIDAD = 'azul-fuerte-con-opacidad',
    AZUL_DEBIL = 'azul-debil',
    AZUL_DEBIL_CON_OPACIDAD = 'azul-debil-con-opacidad',
    NEGRO = 'negro',
    AMARILLO_BASE = 'amarillo-base',

}

export enum SombraBloque {
    SOMBRA_NEGRA = 'sombra-negra'
}

export interface BloquePortada {
    llaveTexto: string,
    tipo: TipoBloqueBortada,
    colorFondo: ColoresBloquePortada,
    conSombra?: SombraBloque,
    botones?: Array<BotonCompartido>,
    eventoTap?: Function,
    eventoDobleTap?: Function
    eventoPress?: Function
}

export interface EventoPortada {
    activarEvento: boolean,
    evento?: Function
}

export interface ConfiguracionPortadaExandida {
    id?: string,
    urlMedia: string,
    mostrarLoader: boolean,
    bloques: Array<BloquePortada>
    botones?: Array<BotonCompartido>
    eventoTapPortada?: EventoPortada,
    eventoDobleTapPortada?: EventoPortada,
}

