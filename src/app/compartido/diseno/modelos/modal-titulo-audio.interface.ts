
export interface ConfiguracionTextArea {
  valor: string,
  placeholder: string,
  contadorCaracteres: {
    mostrar: boolean,
    maximo: number,
    contador: number,
  },
}

export interface ConfiguracionModalTituloAudio {
  mostrar: boolean,
  desactivarCerrarClickAfuera: boolean,
  textoTitulo?: ConfiguracionTextArea,
  evento?: Function,
}
