export interface ConfiguracionDone{
    mostrarDone?: boolean,
    mostrarLoader?: boolean,
    intervalo?: number,
}