import { ComentarioFirebaseModel } from 'dominio/modelo/entidades/comentario.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ConfiguracionAudioReproductor } from '@shared/diseno/modelos/audio-reproductor.interface';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';

export interface ComentarioRespuesta {
  idPerfilRespuesta: PerfilModel,
  comentario: ComentarioFirebaseModel,
  itemCirculo: ItemCircularCompartido,
}
export interface ConfiguracionComentario {
  fecha?: Date,
  coautor?: PerfilModel,
  idInterno?: string,
  esPropietario?: boolean,
  puedeBorrar?: boolean,
  comentarios?: Array<ComentarioRespuesta>,
  itemCirculo?: ItemCircularCompartido,
  configuracionAdjuntos?: Array<{
    id: string,
    confAdjunto: ConfiguracionAudioReproductor
  }>,
  couatorSeleccionado?: boolean,
  inglesActivo?: boolean,
  textoComentario?: string,
  eventoTap?: Function,
  eventoTapEnPerfil?: Function,
  eventoDobleTap?: Function,
  eventoPress?: Function,
}
