import { CatalogoTipoAnuncioSistemaModel } from 'dominio/modelo/catalogos/catalogo-tipo-anuncio-sistema.model';
export interface AnuncioPublicitarioCompartido {
    titulo?: string,
    descripcion: string,
    fecha: Date,
    colorTexto?: string,
    colorFondo?: string,
    tamanioLetra?: string,
    formatoColor?: string,    
    degradado1?: string,
    degragado2?: string
    tipo: CatalogoTipoAnuncioSistemaModel,
    espacio?: string,
    intervaloVisualizacion?: number,
    tipoDegradado?: string
  }