import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';

export interface ConfiguracionListaContactoCompartido {
    listaContactos: DatosLista,
    botonAccion?: DialogoCompartido,
    contactoSeleccionado?: Function,
    cargarMas?: Function,
    mostrar?: boolean
    llaveSubtitulo?: string,
}
