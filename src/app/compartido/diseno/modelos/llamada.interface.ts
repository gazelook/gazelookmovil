import { ParticipanteAsociacionModel } from 'dominio/modelo/entidades/participante-asociacion.model';
import { CatalogoTipoMensaje } from '@core/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { TipoLlamada } from '@shared/diseno/enums/llamada-agora.enum';

export interface ConfiguracionLlamadaMensaje {
  id?: string,
  finalizar?: Function,
  mutear?: boolean,
  infoAsociacion?: string,
  tipoLlamada?: TipoLlamada,
  mostrar?: boolean
  error?: boolean
}

export interface ConfiguracionItemLlamadaMensaje {
  finalizar?: Function,
  aceptar?: Function,
  infoParticipante?: ParticipanteAsociacionModel,
  tipoLlamada?: CatalogoTipoMensaje,
  enviado?: boolean,
  mostrar?: boolean
  error?: boolean,
  mostrarBotonesLlamada?: boolean
}
