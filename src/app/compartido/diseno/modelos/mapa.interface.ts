export interface ConfiguracionMapa {
    latitud?: number
    longitud?: number
    latitudInicial?: number
    longitudInicial?: number
    mostrarMarker?: boolean,
    visitar: boolean
    nuevasPosiciones?: Function

}
