import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';

export enum ModoBusqueda {
  BUSQUEDA_LOCAL = 'busqueda-local',
  BUSQUEDA_COMPONENTE = 'busqueda-componente',
}
export interface ConfiguracionBuscador {
  disable: boolean,
  modoBusqueda?: ModoBusqueda,
  entidad?: CodigosCatalogoEntidad,
  placeholder?: string,
  valorBusqueda?: string,
  placeholderActivo?: boolean,
  focusEnInput?: boolean,
  buscar?: Function,
  cerrarBuscador?: Function,
  abrirBuscador?: Function,
}

