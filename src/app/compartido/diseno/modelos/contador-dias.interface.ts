export interface ConfiguracionContadorDias {
    mostrar?: boolean,
    mostrarLoader?: boolean,
    fechaInicial?: Date,
    fechaFinal?: Date,
    titulo?: string,
    dias?: number,
    horas?: number,
    minutos?: number,
    segundos?: number,
}