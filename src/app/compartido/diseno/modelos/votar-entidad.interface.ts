import {ColorDeFondo} from '@shared/diseno/enums/estilos-colores-general';
import {CodigosCatalogoEntidad} from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';

export interface ConfiguracionVotarEntidad {
  id?: string;
  entidad?: CodigosCatalogoEntidad;
  voto?: boolean;
  bloqueTitulo?: {
    llavesTexto: Array<string>,
    coloDeFondo: ColorDeFondo,
  };
  bloqueBoton?: {
    llaveTexto?: string,
    activarEventoTap?: boolean,
    eventoTap?: Function
  };
  apoyarProyecto?: boolean;
}
