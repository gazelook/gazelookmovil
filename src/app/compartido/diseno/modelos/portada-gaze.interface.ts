import { TamanoPortadaGaze } from "@shared/diseno/enums/estilos-tamano-general.enum"
export interface PortadaGazeCompartido {
    tamano:TamanoPortadaGaze,
    espacioDerecha?:boolean,
    mostrarLoader?: boolean,
    imagenAleatoriaRamdon?: boolean
}
