export enum OrigenFoto {
    SIN_DEFINIR = 0,
    TOMAR_FOTO = 1,
    IMAGEN_GALERIA = 2,
    GRABAR_VIDEO = 3,
}