import {AccionesItemCircularRectangular} from './acciones-general.enum';
import {AlturaResumenPerfil} from './altura-resumen-perfil.enum';
import {AnchoLineaItem} from './ancho-linea-item.enum';
import {DireccionDelReproductor} from './audio-reproductor.enum';
import {EstiloInput} from './estilo-input.enum';
import {ColorFondoLinea, EspesorLineaItem, EstiloErrorInput} from './estilos-colores-general';
import {PaddingIzqDerDelTexto, PaddingLineaVerdeContactos} from './estilos-padding-general';
import {TamanoDeTextoConInterlineado, TamanoLista, TamanoPortadaGaze} from './estilos-tamano-general.enum';
import {ColorCapaOpacidadItem} from './item-cir-rec-capa-opacidad.enum';
import {UsoItemListaContacto} from './item-lista-contacto.enum';
import {TipoDialogo} from './tipo-dialogo.enum';
import {TipoIconoBarraInferior} from './tipo-icono.enum';
import {EstiloItemPensamiento, TipoPensamiento} from './tipo-pensamiento.enum';
import {UsoAppBar} from './uso-appbar.enum';
import {UsoItemCircular} from './uso-item-cir-rec.enum';
import {UsoItemProyectoNoticia} from './uso-item-proyecto-noticia.enum';

export const compartidoDisenoEnums: any[] =
  [
    EstiloInput,
    EstiloErrorInput,
    TamanoDeTextoConInterlineado,
    TamanoLista,
    TamanoPortadaGaze,
    EstiloItemPensamiento,
    TipoPensamiento,
    AnchoLineaItem,
    ColorFondoLinea,
    EspesorLineaItem,
    UsoAppBar,
    DireccionDelReproductor,
    TipoIconoBarraInferior,
    ColorCapaOpacidadItem,
    UsoItemCircular,
    AccionesItemCircularRectangular,
    AlturaResumenPerfil,
    UsoItemProyectoNoticia,
    UsoItemListaContacto,
    PaddingLineaVerdeContactos,
    PaddingIzqDerDelTexto,
    TipoDialogo,
  ];

export * from './acciones-general.enum';
export * from './altura-resumen-perfil.enum';
export * from './ancho-linea-item.enum';
export * from './audio-reproductor.enum';
export * from './estilo-input.enum';
export * from './estilos-colores-general';
export * from './estilos-padding-general';
export * from './estilos-tamano-general.enum';
export * from './item-cir-rec-capa-opacidad.enum';
export * from './item-lista-contacto.enum';
export * from './tipo-dialogo.enum';
export * from './tipo-icono.enum';
export * from './tipo-pensamiento.enum';
export * from './uso-appbar.enum';
export * from './uso-item-cir-rec.enum';
export * from './uso-item-proyecto-noticia.enum';
export * from './array-positions-projects-items';

export {COD_MONEDAS_PAYMENTEZ} from '@shared/diseno/enums/cod_monedas_paymentez.enum';
