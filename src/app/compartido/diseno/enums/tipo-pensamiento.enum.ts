export enum TipoPensamiento {
    PENSAMIENTO_PERFIL ='divPensamientoAmarillo tamanioFijo', // ESTILOS PENSAMIENTO QUE SE PRESENTA EN EL PEFIL
    PENSAMIENTO_ALEATORIO = 'divPensamientoAmarillo tamanoPensamientoAleatorio regular', // ESTILOS PENSAMIENTO ALEATORIO
    PENSAMIENTO_PRIVADO_CREACION = 'divFormasPensamientoRojo tamanoPensamientoPrivado', // ESTILOS PENSAMIENTO PARA CREACION O MODIFICACION DE UN PENSAMIENTO PRIVADO
    PENSAMIENTO_PUBLICO_CREACION = 'divFormasPensamientoAmarillo tamanoPensamiento', // ESTILOS PENSAMIENTO PARA CREACION O MODIFICACION DE UN PENSAMIENTO PUBLICO
    PENSAMIENTO_SIN_SELECCIONAR='divFormasPensamiento tamanoPensamiento',
    PENSAMIENTO_RESUMEN_CON_TITULO='divPensamientoAmarillo tamanoPensamientoTituloResumen',
}
 
export enum EstiloItemPensamiento {
    ITEM_ALEATORIO='regular letra-3-con-interlineado-1 txt-negro textPensamiento', /// ESTILOS DE CSS ITEM PENSAMIENTO
    ITEM_CREAR_PENSAMIENTO='regular letra-3-con-interlineado-1 txt-negro textPensamientoLista'
} 