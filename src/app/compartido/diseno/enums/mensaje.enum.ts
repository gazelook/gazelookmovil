export enum TipoMensaje {
  DOCUMENTO,
  AUDIO,
  TEXTO,
  IMAGEN,
  GENERAL,
  PROYECTO
}

export enum TipoMensajeDibujar {
  LOCAL,
  REMOTO,
}

