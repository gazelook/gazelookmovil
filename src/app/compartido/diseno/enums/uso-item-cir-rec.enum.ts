export enum UsoItemCircular {
    CIRPERFIL = 0, // item circular en el perfil
    CIRALBUM = 1, // Item circular en el album
    CIRCONTACTO = 2, // Item circular en el contacto
    CIREXCLAMACION = 3, // Item circular en el contacto
    CIRCARITACONTACTODEFECTO = 4,
    CIRCARITAPERFIL= 5,
    CIRPERFILREGISTRO = 6, 
    CIRPERFILCHAT = 7,
    CIRPERFILCARITACHAT = 8, 
    CIR_DNI = 9
}

export enum UsoItemRectangular {
    RECPERFIL = 0, // Item rectiangular en el perfil
    RECALBUMMINI = 1, // Item rectangular en el album - Para la miniatura
    RECALBUMPREVIEW = 2, // Item rectangular en el album - Para la preview
    RECPROYECTO = 3,
    RECPERFILREGISTRO = 4,
}

