export enum ColorCapaOpacidadItem {
    CAPA_OPACIDAD_A = 'capa-opacidad-a',
    CAPA_OPACIDAD_B = 'capa-opacidad-b',
    CAPA_OPACIDAD_C = 'capa-opacidad-c',
}