// Ancho de la linea
export enum AnchoLineaItem {
    ANCHO6028 = 'ancho-6028',
    ANCHO6386 = 'ancho-6386',
    ANCHO6920 = 'ancho-6920',
    ANCHO100  = 'ancho-100',
    ANCHO6382 = 'ancho-6382',
    ANCHO6916 = 'ancho-6916',
    ANCHO6954 = 'ancho-6954',
    ANCHO7416 = 'ancho-7416'  
}