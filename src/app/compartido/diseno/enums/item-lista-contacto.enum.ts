export enum UsoItemListaContacto{
    USO_CONTACTO = 'usoContacto',
    USO_MENSAJE = 'usoMensaje',
    USO_EXCLAMACION = 'usoExclamacion',
    USO_DEFECTO = 'usoDefecto',
    USO_PERFIL = 'usoPerfil',
} 
