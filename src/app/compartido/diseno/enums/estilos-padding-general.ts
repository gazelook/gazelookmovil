export enum PaddingLineaVerdeContactos {
    PADDING_0 = 'padding-izq-der-0',
    PADDING_1542_267 = 'padding-der-1542_267',
    PADDING_391 = 'padding-der-391',
    
}


export enum PaddingIzqDerDelTexto {
    PADDING_0 = 'padding-izq-der-0',
    PADDING_1512 = 'padding-izq-der-1512',
    PADDING_0658 = 'padding-izq-der-0658', 
    PADDING_0658_RESTA_267 = 'padding-izq-der-0658-267'
}