export enum UsoAppBar {
  USO_DEMO_APPBAR = 'demo-appbar',
  USO_SEARCHBAR_APPBAR = 'searchbar-appbar',
  USO_SEARCHBAR_APPBAR_MINI = 'searchbar-appbar-mini',
  USO_CONFERENCIA_APPBAR_MINI = 'conferencia-appbar-mini',
  USO_GAZE_BAR = 'gaze-appbar',
  SOLO_TITULO = 'solo-titulo-appbar',
  USO_ELEGIR_PERFIL = 'elegir-perfil-appbar'
}
