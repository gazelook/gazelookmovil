// Interlineados
export enum Interlineado {
    INTERLINEADO_1 = 'interlineado-1',
    INTERLINEADO_2 = 'interlineado-2',
    INTERLINEADO_3 = 'interlineado-3',
    INTERLINEADO_4 = 'interlineado-4',
    INTERLINEADO_5 = 'interlineado-5',
    INTERLINEADO_6 = 'interlineado-6',
    INTERLINEADO_7 = 'interlineado-7',
    INTERLINEADO_8 = 'interlineado-8',
    INTERLINEADO_9 = 'interlineado-9',
    INTERLINEADO_10 = 'interlineado-10',
    INTERLINEADO_11 = 'interlineado-11',
    INTERLINEADO_12 = 'interlineado-12',
    INTERLINEADO_13 = 'interlineado-13',
    INTERLINEADO_14 = 'interlineado-14',
    INTERLINEADO_15 = 'interlineado-15',
}