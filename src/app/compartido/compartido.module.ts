import {CamaraDniComponent} from '@shared/componentes/camara-dni/camara.component';
import {MensajeImagenComponent} from '@shared/componentes/mensaje-gaze/mensaje-imagen/mensaje-imagen.component';
import {MensajeAudioComponent} from '@shared/componentes/mensaje-gaze/mensaje-audio/mensaje-audio.component';
import {MensajeArchivoComponent} from '@shared/componentes/mensaje-gaze/mensaje-archivo/mensaje-archivo.component';
import {MensajeTextoComponent} from '@shared/componentes/mensaje-gaze/mensaje-texto/mensaje-texto.component';
import {ItemMenuPerfilesComponent} from '@shared/componentes/item-menu-perfiles/item-menu-perfiles.component';
// import { AgoraComponent } from '@shared/componentes/agora/agora.component';
import {ListaNoticiasComponent} from '@shared/componentes/lista-noticias/lista-noticias.component';
import {ResumenPerfilComponent} from '@shared/componentes/resumen-perfil/resumen-perfil.component';
import {TituloRectanguloComponent} from '@shared/componentes/titulo-rectangulo/titulo-rectangulo.component';
import {GestorEventosDirective} from '@shared/directivas/gestor-eventos.directive';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CrearComponenteDirective} from './directivas/crear-componente.directive';
import {EstiloListaDirective} from './directivas/estilo-lista.directive';
import {ListaVerticalComponent} from '@shared/componentes/lista-vertical/lista-vertical.component';
import {CargandoComponent} from '@shared/componentes/cargando/cargando.component';
import {ButtonComponent} from '@shared/componentes/button/button.component';
import {InputComponent} from '@shared/componentes/input/input.component';
import {ItemCirculoComponent} from '@shared/componentes/item-circulo/item-circulo.component';
import {ItemRectanguloComponent} from '@shared/componentes/item-rectangulo/item-rectangulo.component';
import {ItemMenuComponent} from '@shared/componentes/item-menu/item-menu.component';
import {LineaComponent} from '@shared/componentes/linea/linea.component';
import {AppbarComponent} from '@shared/componentes/appbar/appbar.component';
import {DialogoComponent} from '@shared/componentes/dialogo/dialogo.component';
import {PortadaGazeComponent} from '@shared/componentes/portada-gaze/portada-gaze.component';
import {ItemPensamientoComponent} from '@shared/componentes/item-pensamiento/item-pensamiento.component';
import {SelectorComponent} from '@shared/componentes/selector/selector.component';
import {BuscadorModalComponent} from '@shared/componentes/buscador-modal/buscador-modal.component';
import {WebcamModule} from 'ngx-webcam';
import {CabeceraIdiomasComponent} from '@shared/componentes/cabecera-idiomas/cabecera-idiomas.component';
import {CamaraComponent} from '@shared/componentes/camara/camara.component';
import {CropperComponent} from '@shared/componentes/cropper/cropper.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {ToastComponent} from '@shared/componentes/toast/toast.component';
import {DialogoContenidoComponent} from '@shared/componentes/dialogo-contenido/dialogo-contenido.component';
import {ItemFechaComponent} from '@shared/componentes/item-fecha/item-fecha.component';
import {PensamientoCompartidoComponent} from '@shared/componentes/pensamiento/pensamiento-compartido.component';
import {ModalInferiorComponent} from '@shared/componentes/modal-inferior/modal-inferior.component';
import {DialogoInlineComponent} from '@shared/componentes/dialogo-inline/dialogo-inline.component';
import {ListoComponent} from '@shared/componentes/listo/listo.component';
import {BarraInferiorComponent} from '@shared/componentes/barra-inferior/barra-inferior.component';
import {BuscadorComponent} from '@shared/componentes/buscador/buscador.component';
import {ItemListaContactosComponent} from '@shared/componentes/item-lista-contactos/item-lista-contactos.component';
import {PortadaExpandidaComponent} from '@shared/componentes/portada-expandida/portada-expandida.component';
import {MonedaPickerComponent} from '@shared/componentes/moneda-picker/moneda-picker.component';
import {BarraInferiorInlineComponent} from '@shared/componentes/barra-inferior-inline/barra-inferior-inline.component';
import {ModalTituloAudioComponent} from '@shared/componentes/modal-titulo-audio/modal-titulo-audio.component';
import {AudioReproductorComponent} from '@shared/componentes/audio-reproductor/audio-reproductor.component';
import {ItemProyectosNoticiasComponent} from '@shared/componentes/item-proyectos-noticias/item-proyectos-noticias.component';
import {ListaContactosCircularComponent} from '@shared/componentes/lista-contactos-circular/lista-contactos-circular.component';
import {MensajeComponent} from '@shared/componentes/mensaje/mensaje.component';
import {TextoMensajeComponent} from '@shared/componentes/mensaje/texto-mensaje/texto-mensaje.component';
import {AudioMensajeComponent} from '@shared/componentes/mensaje/audio-mensaje/audio-mensaje.component';
import {ImagenMensajeComponent} from '@shared/componentes/mensaje/imagen-mensaje/imagen-mensaje.component';
import {DocumentoMensajeComponent} from '@shared/componentes/mensaje/documento-mensaje/documento-mensaje.component';
import {ItemLinkComponent} from '@shared/componentes/item-link/item-link.component';
import {ModalOrigenFotoComponent} from '@shared/componentes/modal-origen-foto/modal-origen-foto.component';
import {ComentarioComponent} from '@shared/componentes/comentario/comentario.component';
import {ItemLlamadaComponent} from '@shared/componentes/item-llamada/item-llamada.component';
import {ListaProyectosComponent} from '@shared/componentes/lista-proyectos/lista-proyectos.component';
import {ListaContactoComponent} from '@shared/componentes/lista-contacto/lista-contacto.component';
import {ImagenPatallaCompletaComponent} from '@shared/componentes/imagen-patanlla-completa/imagen-patanlla-completa.component';
import {VotarEntidadComponent} from '@shared/componentes/votar-entidad/votar-entidad.component';
import {ContadorDiasComponent} from '@shared/componentes/contador-dias/contador-dias.component';
import {
  ItemBuscadorProyectoNoticiasComponent
} from '@shared/componentes/item-buscador-proyecto-noticias/item-buscador-proyecto-noticias.component';
import {NgxCurrencyModule} from 'ngx-currency';
import {DoneComponent} from '@shared/componentes/done/done.component';
import {ListaSelectorComponent} from '@shared/componentes/lista-selector/lista-selector.component';
import {ListaBuscadorModalComponent} from '@shared/componentes/lista-buscador-modal/lista-buscador-modal.component';
import {ListaSelectorTipoMonedaComponent} from '@shared/componentes/lista-selector-tipo-moneda/lista-selector-tipo-moneda.component';
import {MensajeGazeComponent} from '@shared/componentes/mensaje-gaze/mensaje-gaze.component';
import {MensajeProyectoComponent} from '@shared/componentes/mensaje-gaze/mensaje-proyecto/mensaje-proyecto.component';
import {MensajeNoticiaComponent} from '@shared/componentes/mensaje-gaze/mensaje-noticia/mensaje-noticia.component';
import {MapaComponent} from '@shared/componentes/mapa/mapa.component';
import {ItemListaIntercambioComponent} from '@shared/componentes/item-lista-intercambio/item-lista-intercambio.component';
import {ItemIntercambioComponent} from '@shared/componentes/item-intercambio/item-intercambio.component';
import {AnuncioPublicitarioComponent} from '@shared/componentes/anuncio-publicitario/anuncio-publicitario.component';
import {AnuncioInformativoComponent} from '@shared/componentes/anuncio-informativo/anuncio-informativo.component';
import {AnuncioReflexivoComponent} from '@shared/componentes/anuncio-reflexivo/anuncio-reflexivo.component';
import {AnuncioFinanzasComponent} from '@shared/componentes/anuncio-finanzas/anuncio-finanzas.component';
import {CamaraVideoComponent} from '@shared/componentes/camara-video/camara-video.component';
import {ReproductorVideoComponent} from '@shared/componentes/reproductor-video/reproductor-video.component';
import {CamaraFotoComponent} from '@shared/componentes/camara-foto/camara-foto.component';
import {ConfirmacionPagoCriptoComponent} from './componentes/confirmacion-pago-cripto/confirmacion-pago-cripto.component';
import {CountdownModule} from '@env/node_modules/ngx-countdown';
import {ClipboardModule} from '@env/node_modules/ngx-clipboard';
import {ProyectoItemForoComponent} from './componentes/proyecto-item-foro/proyecto-item-foro.component';
import {ProyectoItemForoRecomendadoComponent} from './componentes/proyecto-item-foro-recomendado/proyecto-item-foro-recomendado.component';
import { ApoyarProyectoEsperaFondosComponent } from './componentes/apoyar-proyecto-espera-fondos/apoyar-proyecto-espera-fondos.component';


@NgModule({
  imports: [
    NgxCurrencyModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WebcamModule,
    ImageCropperModule,
    TranslateModule,
    CountdownModule,
    ClipboardModule,
  ],
  declarations: [
    CrearComponenteDirective,
    EstiloListaDirective,
    ListaVerticalComponent,
    CargandoComponent,
    ButtonComponent,
    InputComponent,
    ItemCirculoComponent,
    ItemRectanguloComponent,
    ItemMenuComponent,
    LineaComponent,
    AppbarComponent,
    DialogoComponent,
    PortadaGazeComponent,
    PensamientoCompartidoComponent,
    ItemPensamientoComponent,
    SelectorComponent,
    BuscadorModalComponent,
    CabeceraIdiomasComponent,
    CamaraComponent,
    CropperComponent,
    ToastComponent,
    ItemFechaComponent,
    DialogoContenidoComponent,
    ModalInferiorComponent,
    DialogoInlineComponent,
    ListoComponent,
    BarraInferiorComponent,
    GestorEventosDirective,
    BuscadorComponent,
    TituloRectanguloComponent,
    ItemListaContactosComponent,
    PortadaExpandidaComponent,
    MonedaPickerComponent,
    BarraInferiorInlineComponent,
    ModalTituloAudioComponent,
    AudioReproductorComponent,
    ItemProyectosNoticiasComponent,
    ResumenPerfilComponent,
    ListaContactosCircularComponent,
    MensajeComponent,
    TextoMensajeComponent,
    AudioMensajeComponent,
    ImagenMensajeComponent,
    DocumentoMensajeComponent,
    ListaNoticiasComponent,
    ItemLinkComponent,
    ModalOrigenFotoComponent,
    ComentarioComponent,
    // AgoraComponent,
    ItemLlamadaComponent,
    ListaProyectosComponent,
    ListaContactoComponent,
    ImagenPatallaCompletaComponent,
    VotarEntidadComponent,
    ItemMenuPerfilesComponent,
    ContadorDiasComponent,
    ItemBuscadorProyectoNoticiasComponent,
    DoneComponent,
    ListaSelectorComponent,
    ListaBuscadorModalComponent,
    ListaSelectorTipoMonedaComponent,
    // Gazing
    MensajeGazeComponent,
    MensajeTextoComponent,
    MensajeArchivoComponent,
    MensajeAudioComponent,
    MensajeImagenComponent,
    MensajeTextoComponent,
    MensajeProyectoComponent,
    MensajeNoticiaComponent,
    MapaComponent,
    ItemListaIntercambioComponent,
    ItemIntercambioComponent,
    AnuncioPublicitarioComponent,
    AnuncioInformativoComponent,
    AnuncioReflexivoComponent,
    AnuncioFinanzasComponent,
    CamaraDniComponent,
    ReproductorVideoComponent,
    CamaraVideoComponent,
    CamaraFotoComponent,
    ConfirmacionPagoCriptoComponent,
    ProyectoItemForoComponent,
    ProyectoItemForoRecomendadoComponent,
    ApoyarProyectoEsperaFondosComponent
  ],
  exports: [
    CrearComponenteDirective,
    EstiloListaDirective,
    ListaVerticalComponent,
    CargandoComponent,
    ButtonComponent,
    InputComponent,
    ItemCirculoComponent,
    ItemRectanguloComponent,
    ItemMenuComponent,
    LineaComponent,
    AppbarComponent,
    DialogoComponent,
    PortadaGazeComponent,
    PensamientoCompartidoComponent,
    ItemPensamientoComponent,
    SelectorComponent,
    BuscadorModalComponent,
    CabeceraIdiomasComponent,
    CamaraComponent,
    CropperComponent,
    ToastComponent,
    ItemFechaComponent,
    DialogoContenidoComponent,
    ModalInferiorComponent,
    DialogoInlineComponent,
    BarraInferiorComponent,
    GestorEventosDirective,
    BuscadorComponent,
    TituloRectanguloComponent,
    ItemListaContactosComponent,
    TranslateModule,
    PortadaExpandidaComponent,
    MonedaPickerComponent,
    BarraInferiorInlineComponent,
    ModalTituloAudioComponent,
    AudioReproductorComponent,
    ItemProyectosNoticiasComponent,
    ResumenPerfilComponent,
    ListaContactosCircularComponent,
    MensajeComponent,
    ListaNoticiasComponent,
    ItemLinkComponent,
    ModalOrigenFotoComponent,
    ComentarioComponent,
    // AgoraComponent,
    ItemLlamadaComponent,
    ListaProyectosComponent,
    ListaContactoComponent,
    ImagenPatallaCompletaComponent,
    VotarEntidadComponent,
    ItemMenuPerfilesComponent,
    ContadorDiasComponent,
    ItemBuscadorProyectoNoticiasComponent,
    NgxCurrencyModule,
    DoneComponent,
    ListaSelectorComponent,
    ListaBuscadorModalComponent,
    ListaSelectorTipoMonedaComponent,
    MensajeGazeComponent,
    MensajeTextoComponent,
    MensajeArchivoComponent,
    MensajeAudioComponent,
    MensajeImagenComponent,
    MensajeTextoComponent,
    MensajeProyectoComponent,
    MensajeNoticiaComponent,
    MapaComponent,
    ItemListaIntercambioComponent,
    ItemIntercambioComponent,
    AnuncioPublicitarioComponent,
    AnuncioInformativoComponent,
    AnuncioReflexivoComponent,
    AnuncioFinanzasComponent,
    CamaraDniComponent,
    CamaraVideoComponent,
    ReproductorVideoComponent,
    CamaraFotoComponent,
    ConfirmacionPagoCriptoComponent,
    ProyectoItemForoComponent,
    ProyectoItemForoRecomendadoComponent
  ]
})

export class CompartidoModule {
}
