import { UsoItemListaContacto } from '@shared/diseno/enums/item-lista-contacto.enum';

import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-item-lista-contactos',
	templateUrl: './item-lista-contactos.component.html',
	styleUrls: ['./item-lista-contactos.component.scss']
})
export class ItemListaContactosComponent implements OnInit {
	@Input() configuracion: ConfiguracionItemListaContactosCompartido

	public usoItemListaContacto = UsoItemListaContacto
	constructor(
		public estiloTexto: EstiloDelTextoServicio,

	) { }

	ngOnInit(): void {
		
	}

	eventoIconoXContacto() {
		if (this.configuracion.eventoIconoX) {
			this.configuracion.eventoIconoX(this.configuracion.id)
		}
	}

	configuracionLineaVerde() {
		const clases = {}
		clases['linea-verde-contacto '] = true
		clases[this.configuracion.configuracionLineaVerde.paddingLineaVerde.toString()] = true

		return clases
	}

	tap() {
		if (this.configuracion.eventoCirculoNombre) {
			this.configuracion.eventoCirculoNombre(this.configuracion.id, this.configuracion.contacto, this.configuracion.configCirculoFoto.urlMedia)
		}
	}

	dobleTap() {
		if (this.configuracion.eventoDobleTap) {
			this.configuracion.eventoDobleTap(
				this.configuracion.id,
				this.configuracion.contacto
			)
		}
	}
}
