import { ConfiguracionCamaraFoto } from '@shared/diseno/modelos/camara-foto.interface';
import { AfterViewInit, Component, Input, OnInit, ViewChild, ComponentRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { WebcamImage } from 'ngx-webcam';
import * as RecordRTC from 'recordrtc';
import { GeneradorId } from 'src/app/nucleo/servicios/generales/generador-id.service';
import { CodigosCatalogoTipoMedia } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';
import { ConvertidorArchivos } from 'src/app/nucleo/util/caster-archivo.service';
import { OrigenFoto } from '@shared/diseno/enums/origen-foto.enum';
import { TipoIconoBarraInferior } from '@shared/diseno/enums/tipo-icono.enum';
import { ConfiguracionCamaraVideo } from '@shared/diseno/modelos/camara-video.interface';
import { ConfiguracionCamara, ConfiguracionCropper } from '@shared/diseno/modelos/foto-editor.interface';
import { ConfiguracionModalOrigenFoto } from '@shared/diseno/modelos/modal-opciones-foto.interface';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CamaraService } from '@core/servicios/generales/camara.service';
import { TamanoColorDeFondo } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { CapaIconoContenido, ConfiguracionBarraInferiorInline } from './../../diseno/modelos/barra-inferior-inline.interface';
import { ConfiguracionModalTituloAudio } from './../../diseno/modelos/modal-titulo-audio.interface';

@Component({
    selector: 'app-barra-inferior-inline',
    templateUrl: './barra-inferior-inline.component.html',
    styleUrls: ['./barra-inferior-inline.component.scss']
})
export class BarraInferiorInlineComponent implements OnInit, AfterViewInit {
    @Input() configuracion: ConfiguracionBarraInferiorInline
    // Identificadores
    public idInputFileIconoArchivo: string
    public idInputFileIconoCamara: string

    // Utils
    public intervaloDeGrabacion: any
    public mediaStream: any
    public mediaRecorder: any
    public dataApiArchivo: SubirArchivoData
    public iconoTexto: boolean

    // Configuraciones
    public confModalTituloAudio: ConfiguracionModalTituloAudio
    public confModalOrigenFoto: ConfiguracionModalOrigenFoto
    public confCamara: ConfiguracionCamara
    public confCropper: ConfiguracionCropper
    public confCamaraVideo: ConfiguracionCamaraVideo
    public confCamaraFoto: ConfiguracionCamaraFoto
    // Opciones del recorder
    public isEdge: boolean
    public isSafari: boolean

    public mostrarVideo: boolean


    constructor(
        public estiloDelTextoServicio: EstiloDelTextoServicio,
        private generadorId: GeneradorId,
        private translateService: TranslateService,
        private convertidorArchivos: ConvertidorArchivos,
        private camaraService: CamaraService
        // private platform: Platform
    ) {
        this.idInputFileIconoArchivo = 'iconoArchivoInputFile' + this.generadorId.generarIdConSemilla()
        this.idInputFileIconoCamara = 'iconoCamaraInputFile' + this.generadorId.generarIdConSemilla()
        this.iconoTexto = true
        this.isEdge = false
        this.isSafari = false
        this.mostrarVideo = false
    }

    ngOnInit(): void {
        if (this.configuracion) {
            this.configurarModalTituloAudio()
            this.configurarModalOrigenFoto()
            this.configurarOpcionesDelNavegador()
            this.configurarEscuchaCamaraService()
            if (this.configuracion.iconoAudio && this.configuracion.iconoAudio.capa.grabadora) {

                this.configuracion.iconoAudio.capa.grabadora.mostrarLoader = false
            }
        }
        // Inicializar funcion de redondeo
        if (!Math['round10']) {
            Math['round10'] = (value: any, exp: any) => {
                return this.ajustarDecimales('round', value, exp)
            };
        }
    }

    public blobToFile = (theBlob: Blob, fileName: string): File => {
        let b: any = theBlob;
        //A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;

        //Cast to a File() type
        return <File>theBlob;
    }
    configurarCamaraVideo() {
        this.confCamaraVideo = {
            mostrarModal: true,
            videoRealizado: (video: any) => {
                if (video) {
                    const blob = new Blob([video.blob], { type: 'video/mp4' });
                    // let file = new File([video.blob], "name")
                    let a = this.blobToFile(blob, video.title)
                    this.dataApiArchivo = {
                        archivo: [a],
                        nombreDelArchivo: video.title
                    }


                    if (this.configuracion.iconoArchivo && this.configuracion.iconoArchivo.icono.eventoTap) {
                        this.configuracion.iconoArchivo.icono.eventoTap(this.dataApiArchivo)
                    }
                    // this.confCamaraVideo.mostrarModal = false
                    this.confCamaraVideo = undefined
                }

            },
            cerrarCamara: () => {
                // this.confCamaraVideo.mostrarModal = false
                this.confCamaraVideo = undefined
            }
        }
    }
    configurarCamaraFoto() {
        this.confCamaraFoto = {
            mostrarModal: true,
            fotoRealizada: (foto: any) => {
                if (foto) {
                    this.dataApiArchivo = {
                        archivo: this.convertidorArchivos.dataURItoBlob(foto[0]),
                        nombreDelArchivo: new Date().getTime() + '.jpeg'
                    }
                    if (this.configuracion.iconoCamara && this.configuracion.iconoCamara.icono.eventoTap) {
                        this.configuracion.iconoCamara.icono.eventoTap(this.dataApiArchivo)
                    }
                    this.confCamaraFoto.mostrarModal = false
                    this.confCamaraFoto = undefined
                }

            },
            cerrarCamara: () => {
                this.confCamaraFoto.mostrarModal = false
                this.confCamaraFoto = undefined
            }
        }
    }
    configurarEscuchaCamaraService() {
        this.camaraService.suscripcionFotoCapturada$ = this.camaraService.escuchaFotoCapturada$.subscribe(imagen => {
            this.fotoCapturada(imagen)
        })
    }


    fotoCapturada(imagen: WebcamImage) {
        if (imagen) {
            this.dataApiArchivo = {
                archivo: this.convertidorArchivos.dataURItoBlob(imagen.imageAsDataUrl),
                nombreDelArchivo: new Date().getTime() + '.jpeg'
            }
            if (this.configuracion.iconoCamara && this.configuracion.iconoCamara.icono.eventoTap) {
                this.configuracion.iconoCamara.icono.eventoTap(this.dataApiArchivo)
            }
        }
    }

    ngAfterViewInit(): void { }

    async configurarModalTituloAudio() {
        this.confModalTituloAudio = {
            mostrar: false,
            desactivarCerrarClickAfuera: true,
            textoTitulo: {
                valor: '',
                placeholder: '',
                contadorCaracteres: {
                    mostrar: false,
                    maximo: 75,
                    contador: 0
                },
            },
            evento: (titulo: string) => {
                this.confModalTituloAudio.mostrar = false
                if (this.dataApiArchivo) {
                    this.dataApiArchivo.descripcion = titulo
                    this.guardarGrabacion()
                }
            }
        }

        this.confModalTituloAudio.textoTitulo.placeholder = await this.translateService.get('m4v7texto6').toPromise()
    }
    configurarModalOrigenFoto() {
        this.confModalOrigenFoto = {
            mostrar: false,
            origenFoto: OrigenFoto.SIN_DEFINIR,
            eventoTap: (origen: OrigenFoto) => {
                this.confModalOrigenFoto.mostrar = false
                switch (origen) {
                    case OrigenFoto.TOMAR_FOTO:
                        // this.camaraService.reiniciarCamara()
                        // this.camaraService.cambiarEstadoCamara(true)
                        this.configurarCamaraFoto()

                        break
                    case OrigenFoto.IMAGEN_GALERIA:
                        this.clickEnInputTipoFile(this.idInputFileIconoCamara)
                        break
                    case OrigenFoto.GRABAR_VIDEO:
                        // this.clickEnInputTipoFile(this.idInputFileIconoCamara)
                        //   this.mostrarVideo = true
                        this.configurarCamaraVideo()
                        //   this.confCamaraVideo.mostrarModal = true

                        break
                    default: break;
                }
            }
        }
    }

    destruirDatosGrabadora() {
        if (this.mediaRecorder) {
            this.mediaRecorder.reset()
            this.mediaRecorder = null
        }

        if (this.mediaStream) {
            this.mediaStream.getAudioTracks().forEach((track: any) => track.stop())
            this.mediaStream = null
        }

        if (this.intervaloDeGrabacion) {
            clearInterval(this.intervaloDeGrabacion)
        }

    }

    obtenerClasesCapaColorDeFondo() {
        const clases = {}
        clases['capa'] = true
        clases['color-fondo'] = true
        clases['mostrar'] = this.configuracion.capaColorFondo.mostrar

        // Ancho color de fondo
        if (this.configuracion.capaColorFondo.anchoCapa) {
            clases[this.configuracion.capaColorFondo.anchoCapa.toString()] = true
        } else {
            clases[TamanoColorDeFondo.TAMANO100.toString()] = true
        }

        // Color de fondo
        if (this.configuracion.capaColorFondo.colorDeFondo) {
            clases[this.configuracion.capaColorFondo.colorDeFondo.toString()] = true
        }

        return clases
    }


    obtenerClaseTipoIcono(
        contenido: CapaIconoContenido
    ) {
        const clases = {}
        clases['imagen'] = true
        if (contenido) {
            clases[contenido.icono.tipo.toString()] = true
            clases['mostrar'] = contenido.icono.mostrar
            // Grabando
            if (contenido.icono.tipo === TipoIconoBarraInferior.ICONO_AUDIO) {
                clases['grabando'] = (contenido.capa && contenido.capa.grabadora && contenido.capa.grabadora.grabando)
            }
        }
        return clases
    }

    determinarOverflowIconoAudio() {
        if (
            (this.configuracion.iconoTexto && this.configuracion.iconoTexto.capa && (this.configuracion.iconoTexto.capa.mostrar || this.configuracion.iconoTexto.capa.siempreActiva))
        ) {
            return true
        }

        return false
    }

    determinarEstadozIndexDelIcono(icono: CapaIconoContenido) {
        if (icono && icono.capa && (icono.capa.mostrar || icono.capa.siempreActiva)) {
            return true
        }

        return false
    }

    obtenerTextoSegunValor(tiempo: number): string {
        if (tiempo < 10) {
            return '0' + tiempo
        }

        if (tiempo >= 10 && tiempo < 60) {
            return tiempo.toString()
        }
    }

    obtenerTiempoDeGrabacion(
        delTiempoMaximo: boolean = false
    ) {
        if (
            this.configuracion.iconoAudio &&
            this.configuracion.iconoAudio.capa &&
            this.configuracion.iconoAudio.capa.grabadora
        ) {
            const tiempo = (delTiempoMaximo) ? this.configuracion.iconoAudio.capa.grabadora.tiempoMaximo : this.configuracion.iconoAudio.capa.grabadora.duracionActual

            if (tiempo >= 60) {
                const minutosEnDecimal = Math['round10'](tiempo / 60, -2)
                const cuantosMinutosHay = parseInt(minutosEnDecimal.toString())
                const cuantosSegundosHay = tiempo - (cuantosMinutosHay * 60)
                return this.obtenerTextoSegunValor(cuantosMinutosHay) + ':' + this.obtenerTextoSegunValor(cuantosSegundosHay)
            }

            return '00:' + this.obtenerTextoSegunValor(tiempo)
        }
    }

    obtenerTiempoMaximo() { }

    obtenerEstilosLineaDeTiempo() {
        if (
            this.configuracion.iconoAudio &&
            this.configuracion.iconoAudio.capa &&
            this.configuracion.iconoAudio.capa.grabadora
        ) {
            const ancho = this.configuracion.iconoAudio.capa.grabadora.duracionActual * this.configuracion.iconoAudio.capa.grabadora.factorAumentoLinea
            return {
                'width': '' + ancho + '%'
            }
        }
        return {}
    }

    eventoEnterEnCapaTexto() {

        if (
            this.configuracion.iconoTexto &&
            this.configuracion.iconoTexto.capa &&
            this.configuracion.iconoTexto.capa.informacion &&
            this.configuracion.iconoTexto.capa.informacion.length > 0
        ) {
            this.dataApiArchivo = {
                descripcion: this.configuracion.iconoTexto.capa.informacion
            }

            if (this.configuracion.iconoTexto.icono.eventoTap) {
                this.configuracion.iconoTexto.icono.eventoTap(this.dataApiArchivo)
            }

            this.dataApiArchivo = null
            this.configuracion.iconoTexto.capa.informacion = ''

            if (!this.configuracion.iconoTexto.capa.siempreActiva) {
                this.configuracion.iconoTexto.capa.mostrar = false
            }
        }
    }

    cambiarValorDelLoaderDeLaGrabadora(estado: boolean) {
        if (this.configuracion.iconoAudio.capa.grabadora.usarLoader) {
            this.configuracion.iconoAudio.capa.grabadora.mostrarLoader = estado
        }
    }

    informarDeErrorEnGrabadora(error?: string) {
        this.dataApiArchivo = null
        this.configuracion.iconoAudio.capa.mostrar = false
        this.configuracion.iconoAudio.capa.grabadora.grabando = false
        this.configuracion.iconoAudio.capa.grabadora.duracionActual = 0
        this.destruirDatosGrabadora()
        if (this.configuracion.estatusError) {
            this.configuracion.estatusError((error) ? error : 'Lo sentimos, ocurrió un error al procesar la grabación')
        }
    }

    configurarOpcionesDelNavegador() {
        this.isEdge = window.navigator.userAgent.toLowerCase().includes('edge') || window.navigator.userAgent.toLowerCase().includes('edg')
        this.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
    }

    obtenerOpcionesParaRecordRTC() {
        let options: any = {
            type: 'audio',
            mimeType: 'audio/webm;codecs=pcm',
            numberOfAudioChannels: this.isEdge ? 1 : 2,
            disableLogs: false,
            checkForInactiveTracks: true,
            bufferSize: 16384
        }

        if (this.isSafari) {
            options.recorderType = RecordRTC.StereoAudioRecorder
        }

        if (navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1) {
            options.sampleRate = 48000
        }

        if (this.isSafari) {
            options.sampleRate = 44100
            options.bufferSize = 4096
            options.numberOfAudioChannels = 2
        }

        return options
    }


    async empezarGrabacion() {
        try {
            this.mediaStream = await navigator.mediaDevices.getUserMedia({ audio: true })

            const options = this.obtenerOpcionesParaRecordRTC()
            this.mediaRecorder = new RecordRTC['RecordRTCPromisesHandler'](this.mediaStream, options)

            if (this.mediaRecorder) {
                if (!this.configuracion.iconoAudio.capa.siempreActiva) {
                    this.configuracion.iconoAudio.capa.mostrar = true
                }
                this.configuracion.iconoAudio.capa.grabadora.grabando = true
                this.mediaRecorder.startRecording()
                this.iniciarContador()

                if (!this.configuracion.botonSend) {
                    this.configuracion.botonSend = {
                        mostrar: true
                    }
                } else {
                    this.configuracion.botonSend.mostrar = true
                }
            }
        } catch (error) {
            this.informarDeErrorEnGrabadora()
        }
    }

    async detenerGrabacion() {
        try {
            this.configuracion.iconoAudio.capa.grabadora.grabando = false
            this.cambiarValorDelLoaderDeLaGrabadora(true)
            await this.mediaRecorder.stopRecording();

            const audio: Blob = await this.mediaRecorder.getBlob()

            const file = new File([audio], new Date().toDateString() + '.mp3', {
                type: 'audio/mp3'
            })

            this.destruirDatosGrabadora()
            // Definir data del audio a guardar
            this.dataApiArchivo = {
                archivo: file,
                formato: 'audio/mp3',
                catalogoMedia: CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
                descripcion: '',
                duracion: this.configuracion.iconoAudio.capa.grabadora.duracionActual.toString()
            }
            this.validarTituloGrabacion()
            this.configuracion.iconoAudio.capa.grabadora.duracionActual = 0
        } catch (error) {
            this.informarDeErrorEnGrabadora()
            this.cambiarValorDelLoaderDeLaGrabadora(false)
        }
    }

    async validarTituloGrabacion() {
        // Validar si se debe solicitar un titulo
        if (!this.configuracion.iconoAudio.capa.grabadora.noPedirTituloGrabacion) {
            // mostrar modal del titulo
            this.confModalTituloAudio.mostrar = true
            return
        }

        this.guardarGrabacion()
    }

    async guardarGrabacion() {
        if (
            this.configuracion.iconoAudio &&
            this.configuracion.iconoAudio.icono.eventoTap
        ) {
            this.configuracion.iconoAudio.icono.eventoTap(this.dataApiArchivo)
        }

        if (!this.configuracion.iconoAudio.capa.siempreActiva && !this.configuracion.iconoAudio.capa.grabadora.usarLoader) {
            this.configuracion.iconoAudio.capa.mostrar = false
        }

        if (this.configuracion.iconoAudio.capa.siempreActiva && this.configuracion.botonSend) {
            this.configuracion.botonSend.mostrar = false
        }
    }

    iniciarContador() {
        if (this.intervaloDeGrabacion) {
            clearInterval(this.intervaloDeGrabacion)
        }
        this.intervaloDeGrabacion = setInterval(() => {
            if (this.configuracion.iconoAudio.capa.grabadora.duracionActual < this.configuracion.iconoAudio.capa.grabadora.tiempoMaximo) {
                this.configuracion.iconoAudio.capa.grabadora.duracionActual += 1
            } else {
                this.detenerGrabacion()
            }
        }, 1000)
    }

    ajustarDecimales(type: any, value: any, exp: any) {
        // Si el exp no está definido o es cero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Si el valor no es un número o el exp no es un entero...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    eventoIconoTexto() {
        if (
            this.configuracion.iconoTexto.capa &&
            !this.configuracion.iconoTexto.capa.siempreActiva
        ) {
            this.configuracion.iconoTexto.capa.mostrar = !this.configuracion.iconoTexto.capa.mostrar
        }
    }

    cambioEnInputFile(
        files: FileList,
        capaIconoActivo: CapaIconoContenido
    ) {
        if (files) {


            this.dataApiArchivo = {
                archivo: files
            }


            if (capaIconoActivo && capaIconoActivo.icono) {
                capaIconoActivo.icono.eventoTap(this.dataApiArchivo)
            }
        }
    }

    clickEnInputTipoFile(id: string) {
        const elemento: HTMLInputElement = document.getElementById(id) as HTMLInputElement
        elemento.value = ''
        elemento.click()
    }

    eventoIconoArchivo() {
        this.clickEnInputTipoFile(this.idInputFileIconoArchivo)
    }

    eventoIconoCamara() {
        this.confModalOrigenFoto.mostrar = true
    }

    eventoIconoLlamada() {
        this.configuracion.iconoLlamada.icono.eventoTap()
    }

    eventoIconoVideoLlamada() {
        this.configuracion.iconoVideoLlamada.icono.eventoTap()
    }

    eventoIconoAudio() {
        if (
            this.configuracion.iconoAudio &&
            this.configuracion.iconoAudio.capa &&
            this.configuracion.iconoAudio.capa.grabadora
        ) {
            // Si el loader se esta mostrando, debe evitar ejecucion de tareas debajo
            if (this.configuracion.iconoAudio.capa.grabadora.usarLoader) {
                if (this.configuracion.iconoAudio.capa.grabadora.mostrarLoader) {
                    return
                }
            }

            // Apertura o cierre de la capa
            if (
                !this.configuracion.iconoAudio.capa.siempreActiva &&
                !this.configuracion.iconoAudio.capa.grabadora.grabando
            ) {
                // this.configuracion.iconoAudio.capa.mostrar = !this.configuracion.iconoAudio.capa.mostrar
            }

            if (this.configuracion.iconoAudio.capa.grabadora.grabando) {
                this.detenerGrabacion()
            } else {
                this.empezarGrabacion()
            }

        }
    }

    eventoEnBotonSend() {

        if (
            this.configuracion.iconoAudio &&
            this.configuracion.iconoAudio.capa &&
            this.configuracion.iconoAudio.capa.grabadora &&
            this.configuracion.iconoAudio.capa.grabadora.grabando
        ) {
            this.destruirDatosGrabadora()
            this.configuracion.iconoAudio.capa.grabadora.grabando = false
            this.configuracion.iconoAudio.capa.grabadora.duracionActual = 0
            this.cambiarValorDelLoaderDeLaGrabadora(false)

            if (!this.configuracion.iconoAudio.capa.siempreActiva) {
                this.configuracion.iconoAudio.capa.mostrar = false
            }

            if (this.configuracion.iconoAudio.capa.siempreActiva && this.configuracion.botonSend) {
                this.configuracion.botonSend.mostrar = false
            }
        }

        if (
            this.configuracion &&
            this.configuracion.botonSend &&
            this.configuracion.iconoTexto &&
            this.configuracion.iconoTexto.capa.mostrar &&
            this.configuracion.iconoTexto.capa.informacion &&
            this.configuracion.botonSend
        ) {
            this.dataApiArchivo = {
                descripcion: this.configuracion.iconoTexto.capa.informacion
            }

            if (this.configuracion.botonSend.evento) {

                this.configuracion.botonSend.evento(this.dataApiArchivo)
            }

            this.dataApiArchivo = null

            this.configuracion.iconoTexto.capa.informacion = ''
            if (!this.configuracion.iconoTexto.capa.siempreActiva) {
                this.configuracion.iconoTexto.capa.mostrar = false
            }
        }
    }

    mostrarIconoDeTexto() {
        if (
            this.configuracion &&
            this.configuracion.iconoTexto &&
            this.configuracion.iconoTexto.capa &&
            !this.configuracion.iconoTexto.capa.siempreActiva
        ) {
            this.configuracion.iconoTexto.capa.mostrar = false
        }
    }

    obtenerTamanoDeCaracteres(): string {
        if (
            this.configuracion &&
            this.configuracion.iconoTexto &&
            this.configuracion.iconoTexto.capa &&
            this.configuracion.iconoTexto.capa.informacion &&
            this.configuracion.iconoTexto.capa.informacion !== null
        ) {
            const texto: string = this.configuracion.iconoTexto.capa.informacion
            if (texto.length > 9) {
                return '' + texto.length
            }

            return '0' + texto.length
        }

        return '00'
    }

    obtenerValorMaximo(): number {
        if (
            this.configuracion &&
            this.configuracion.iconoTexto &&
            this.configuracion.iconoTexto.capa &&
            this.configuracion.iconoTexto.capa.contador &&
            this.configuracion.iconoTexto.capa.contador.maximo
        ) {
            return this.configuracion.iconoTexto.capa.contador.maximo
        }

        return 2500
    }
}
