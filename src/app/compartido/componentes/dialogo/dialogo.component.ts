import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum';
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface';
@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.scss']
})
export class DialogoComponent implements OnInit {
  @ViewChild('descripcion', { static: false }) containerDescripcion: ElementRef
  @ViewChild('principal', { static: false }) containerPrincipal: ElementRef
  @ViewChild('fondo', { static: false }) fondo: ElementRef

  @Input() configuracion: DialogoCompartido

  public tipoDialogo = TipoDialogo
  public mostrarInfoVertical = false;
  public mostrarInfoDemo = false

  constructor(
    private render: Renderer2,
    public estiloTexto: EstiloDelTextoServicio

  ) { } 

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.configuracion) {
        this.definirEstilo();
      }
    })
  }

  ngOnDestroy(): void {
  }

  eventoModal(target: any) {
    if (this.configuracion.completo) {
      target.classList.forEach((clase: any) => {
        if (clase === 'modal') {
          this.configuracion.mostrarDialogo = false
          return
        }
      })
    }
  }

  definirEstilo() {
    this.mostrarInfoVertical = false;
    if (this.configuracion.completo) {
      this.render.addClass(this.fondo.nativeElement, "fondoModal");
    }
    switch (this.configuracion.tipo) {
      case TipoDialogo.CONFIRMACION:
        this.render.addClass(this.containerPrincipal.nativeElement, "dimensionesConfirmacion");
        this.render.addClass(this.containerDescripcion.nativeElement, "dimensionDescripcionConfirmacion");
        break;

      case TipoDialogo.MULTIPLE_ACCION:
        this.render.addClass(this.containerPrincipal.nativeElement, "dimensionesMultipleAccion");
        this.render.addClass(this.containerDescripcion.nativeElement, "dimensionDescripcionMultipleAccion");
        break;

      case TipoDialogo.MULTIPLE_ACCION_DEMO:
        this.mostrarInfoDemo = true
        this.render.addClass(this.containerPrincipal.nativeElement, "dimensionesMultipleAccionDemo");
        this.render.addClass(this.containerDescripcion.nativeElement, "dimensionDescripcionMultipleAccionDemo");
        break;

      case TipoDialogo.MULTIPLE_ACCION_HORIZONTAL:
        this.render.addClass(this.containerPrincipal.nativeElement, "dimensionesMultipleAccionHorizontal");
        break;

      case TipoDialogo.INFO_VERTICAL:
        this.mostrarInfoVertical = true;
        this.render.addClass(this.containerPrincipal.nativeElement, "info-vertical");
        this.render.addClass(this.containerDescripcion.nativeElement, "dimensionDescripcionVertical");
        break;
    }
  }

  definirTamanoLetra(elemento: any, tamano: TamanoTextoDialogo) {
    this.render.addClass(elemento, tamano.toString());
  }
}

export enum TamanoTextoDialogo {
  PEQUENA = "letra-024",
  MEDIANA = "letra-029",
  GRANDE = "letra-349",
  EXTGRANDE = "letra-423"
}


