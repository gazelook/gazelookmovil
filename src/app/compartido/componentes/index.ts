import { AppbarComponent } from './appbar/appbar.component';
import { BuscadorModalComponent } from './buscador-modal/buscador-modal.component';
import { ButtonComponent } from './button/button.component';
import { DiametroDelLoader } from './cargando/cargando.component';
import { ContadorDiasComponent } from './contador-dias/contador-dias.component';
import { CropperComponent } from './cropper/cropper.component';
import { DialogoContenidoComponent } from './dialogo-contenido/dialogo-contenido.component';
import { DialogoInlineComponent } from './dialogo-inline/dialogo-inline.component';
import { ItemCirculoComponent } from './item-circulo/item-circulo.component';
import { TipoMenu } from './item-menu/item-menu.component';
import { ItemRectanguloComponent } from './item-rectangulo/item-rectangulo.component';
import { ListaBuscadorModalComponent } from './lista-buscador-modal/lista-buscador-modal.component';
import { ListaSelectorComponent } from './lista-selector/lista-selector.component';
import { MensajeGazeComponent } from './mensaje-gaze/mensaje-gaze.component';
import { ModalInferiorComponent } from './modal-inferior/modal-inferior.component';
import { PensamientoCompartidoComponent } from './pensamiento/pensamiento-compartido.component';
import { PortadaExpandidaComponent } from './portada-expandida/portada-expandida.component';
import { PortadaGazeComponent } from './portada-gaze/portada-gaze.component';
import { SelectorComponent } from './selector/selector.component';
import { ToastComponent } from './toast/toast.component';

export const componentes: any[] =
  [
    AppbarComponent,
    ButtonComponent,
    PensamientoCompartidoComponent,
    PortadaGazeComponent,
    ToastComponent,
    // PortadaGifComponent,
    TipoMenu,
    DiametroDelLoader,
    CropperComponent,
    SelectorComponent,
    BuscadorModalComponent,
    MensajeGazeComponent,
    DialogoContenidoComponent,
    ModalInferiorComponent,
    PortadaExpandidaComponent,
    ContadorDiasComponent,
    ItemCirculoComponent,
    ItemRectanguloComponent,
    ListaSelectorComponent,
    ListaBuscadorModalComponent,
    DialogoInlineComponent
  ];

export * from './appbar/appbar.component';
export * from './buscador-modal/buscador-modal.component';
export * from './button/button.component';
export * from './cargando/cargando.component';
export * from './contador-dias/contador-dias.component';
export * from './cropper/cropper.component';
export * from './dialogo-contenido/dialogo-contenido.component';
export * from './dialogo-inline/dialogo-inline.component';
export * from './item-circulo/item-circulo.component';
export * from './item-menu/item-menu.component';
export * from './item-rectangulo/item-rectangulo.component';
export * from './lista-buscador-modal/lista-buscador-modal.component';
export * from './lista-selector/lista-selector.component';
export * from './mensaje-gaze/mensaje-gaze.component';
export * from './modal-inferior/modal-inferior.component';
export * from './pensamiento/pensamiento-compartido.component';
export * from './portada-expandida/portada-expandida.component';
export * from './portada-gaze/portada-gaze.component';
export * from './selector/selector.component';
export * from './toast/toast.component';

