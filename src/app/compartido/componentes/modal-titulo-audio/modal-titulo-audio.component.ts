import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionModalTituloAudio } from '@shared/diseno/modelos/modal-titulo-audio.interface';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-titulo-audio',
  templateUrl: './modal-titulo-audio.component.html',
  styleUrls: ['./modal-titulo-audio.component.scss']
})
export class ModalTituloAudioComponent implements OnInit {

  @Input() configuracion: ConfiguracionModalTituloAudio

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void { }

  obtenerTextoContadorCaracteres(): string {
    let contador = ''

    if (this.configuracion.textoTitulo.valor.length > 0) {
      this.configuracion.textoTitulo.contadorCaracteres.contador = this.configuracion.textoTitulo.valor.length
    }

    if (this.configuracion.textoTitulo.contadorCaracteres.contador > 9) {
      contador = '' + this.configuracion.textoTitulo.contadorCaracteres.contador
    } else {
      contador = '0' + this.configuracion.textoTitulo.contadorCaracteres.contador
    }

    return contador + '/' + this.configuracion.textoTitulo.contadorCaracteres.maximo
  }

  escribiendoEnInput() {
    if (this.configuracion.textoTitulo.contadorCaracteres.contador <= this.configuracion.textoTitulo.contadorCaracteres.maximo) {
      this.configuracion.textoTitulo.contadorCaracteres.contador = this.configuracion.textoTitulo.valor.length
    }
  }

  eventoEnter(evento: any) {
    evento.preventDefault()
    this.eventoTap()
  }

  eventoTap() {
    if (this.configuracion.evento) {
      this.configuracion.evento(this.configuracion.textoTitulo.valor)
      this.configuracion.textoTitulo.valor = ''
    }
  }

  eventoTapModal(evento: any) {
    evento.target.classList.forEach((clase: string) => {
      if (clase === 'titulo-audio') {
        if (this.configuracion.desactivarCerrarClickAfuera) {
          return
        }

        this.configuracion.mostrar = false
        this.configuracion.textoTitulo.valor = ''
        return
      }
    })
  }
}
