import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {fromEvent, Observable} from 'rxjs';
import {debounceTime, map} from 'rxjs/operators';
import {EstiloDelTextoServicio} from '@core/servicios/diseno/estilo-del-texto.service';
import {AccionesSelector} from '@shared/diseno/enums/acciones-general.enum';
import {ItemSelector} from '@shared/diseno/modelos/elegible.interface';
import {ConfiguracionSelector} from '@shared/diseno/modelos/selector.interface';

@Component({
  selector: 'app-lista-selector',
  templateUrl: './lista-selector.component.html',
  styleUrls: ['./lista-selector.component.scss']
})
export class ListaSelectorComponent implements OnInit, AfterViewInit {
  @Input() configuracion: ConfiguracionSelector;
  @ViewChild('inputBuscador', {static: false}) inputBuscador: ElementRef;

  public barraBusqueda$: Observable<string>;
  public buscar: string;
  public elegibles: ItemSelector[];
  public todosPaises: ItemSelector[];

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
  ) {
    this.buscar = '';
    this.elegibles = [];
    this.todosPaises = [];
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.configurarObservable();
  }

  configurarObservable(): void {

    this.barraBusqueda$ = fromEvent<any>(this.inputBuscador.nativeElement, 'keyup')
      .pipe(
        map(event =>
          event.target.value
        ),
        debounceTime(300),
      );
    this.barraBusqueda$.subscribe(query => {
      if (!query || query.length === 0) {
        this.configuracion.evento({
          accion: AccionesSelector.ABRIR_SELECTOR
        });
      }
      this.elegibles = this.configuracion.elegibles;
      if (this.todosPaises.length === 0) {
        this.todosPaises = this.configuracion.elegibles;
      }

      const coincidencias = this.encontrarCoincidencias(query);
      if (coincidencias.length > 0) {
        this.configuracion.elegibles = coincidencias;

      } else {
        this.configuracion.evento({
          accion: AccionesSelector.ABRIR_SELECTOR
        });
      }
    });
  }

  clickInputSelector(): void {
    if (this.configuracion.evento) {
      this.configuracion.evento({
        accion: AccionesSelector.ABRIR_SELECTOR
      });
    }
  }

  encontrarCoincidencias(buscar: string): ItemSelector[] {
    const query = buscar.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    try {
      return this.todosPaises.filter(place => {
        if (place.nombre.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').startsWith(query)) {
          return place && place.nombre;
        }
      });
    } catch (error) {
      return [];
    }
  }

  elegirPais(id: string, nombre: string, latitud: number, longitud: number): void {
    if (this.configuracion.evento) {
      this.configuracion.evento({
        accion: AccionesSelector.SELECCIONAR_ITEM,
        informacion: {
          codigo: id,
          nombre,
          latitud,
          longitud
        }
      });
    }
  }

  // Reiniciar selector
  reiniciarSelector(): void {
    this.configuracion.mostrarModal = false;
    this.configuracion.cargando.mostrar = false;
    this.configuracion.error.mostrarError = false;
    this.configuracion.error.contenido = '';
    this.configuracion.error.tamanoCompleto = false;
  }

  // Setear error
  mostrarError(error: string): void {
    this.configuracion.elegibles = [];
    this.configuracion.error.contenido = error;
    this.configuracion.error.mostrarError = true;
    this.configuracion.cargando.mostrar = false;
  }

  // Evento en modal (Borde negro)
  eventoModal(target: any): void {
    target.classList.forEach((clase: any) => {
      if (clase === 'modal') {
        this.reiniciarSelector();
        return;
      }
    });
  }
}

