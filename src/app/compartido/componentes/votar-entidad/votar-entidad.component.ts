import {EstiloDelTextoServicio} from '@core/servicios/diseno/estilo-del-texto.service';
import {ConfiguracionVotarEntidad} from '@shared/diseno/modelos/votar-entidad.interface';
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-votar-entidad',
  templateUrl: './votar-entidad.component.html',
  styleUrls: ['./votar-entidad.component.scss']
})
export class VotarEntidadComponent implements OnInit {

  @Input() configuracion: ConfiguracionVotarEntidad;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {
  }

  ngOnInit(): void {
  }

  obtenerClasesBloqueTitulo(): {} {
    const clases = {};
    clases['bloque-titulo'] = true;

    if (
      this.configuracion &&
      this.configuracion.bloqueTitulo &&
      this.configuracion.bloqueTitulo.coloDeFondo
    ) {
      clases[this.configuracion.bloqueTitulo.coloDeFondo.toString()] = true;
    }

    return clases;
  }

  eventoTap(): void {
    if (
      this.configuracion.bloqueBoton &&
      this.configuracion.bloqueBoton.activarEventoTap &&
      this.configuracion.bloqueBoton.eventoTap
    ) {
      this.configuracion.bloqueBoton.eventoTap();
    }
  }
}
