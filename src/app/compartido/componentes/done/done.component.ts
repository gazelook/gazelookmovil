import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionDone } from '@shared/diseno/modelos/done.interface';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-done',
	templateUrl: './done.component.html',
	styleUrls: ['./done.component.scss']
})
export class DoneComponent implements OnInit {
	@Input() configuracion: ConfiguracionDone

	public imagenesDefecto: Array<ArchivoModel>
	public timeOut: any
	public urlDone: string

	constructor(
		private mediaNegocio: MediaNegocio,
		public estiloTexto: EstiloDelTextoServicio
	) {
		this.imagenesDefecto = []
	}

	ngOnInit(): void {
		this.inicializarImagenesPorDefecto()
	}

	inicializarImagenesPorDefecto() {
		this.mediaNegocio.obtenerListaArchivosDefaultDemo(CodigosCatalogoArchivosPorDefecto.DONE).subscribe(data => {
			data.forEach(item => {
				this.imagenesDefecto.push(item)
			})
			this.seleccionarDoneAleatorio()
		}, error => {
			this.imagenesDefecto = []
		})
	}

	seleccionarDoneAleatorio() {
		let random = Math.floor(Math.random() * this.imagenesDefecto.length);
		this.urlDone = this.imagenesDefecto[random].url
	}

	abrirDone() {
		this.seleccionarDoneAleatorio()
		this.configuracion.mostrarDone = true;
		this.visibleHasta()
	}

	visibleHasta() {
		if (!this.configuracion.mostrarLoader) {
			if (!this.configuracion.intervalo) {
				this.configuracion.intervalo = 3;
			}

			if (this.timeOut) {
				clearTimeout(this.timeOut)
			}

			this.timeOut = setTimeout(() => {
				this.cerrarDone();
			}, this.configuracion.intervalo * 1000)
		}
	}

	cerrarDone() {
		this.configuracion.mostrarDone = false
		this.configuracion.mostrarLoader = false
	}

	siImagenEstaCargada(evt: any) {
		if (evt && evt.target) {
			const x = evt.srcElement.x
			const y = evt.srcElement.y
			if ((x === 0) && (y === 0)) {
				const width = evt.srcElement.width
				const height = evt.srcElement.height
				const portrait = height > width ? true : false
			}
			this.configuracion.mostrarLoader = false
		}
	}
}
