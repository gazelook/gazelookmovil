import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  Router
} from '@angular/router';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { PensamientoNegocio } from 'dominio/logica-negocio/pensamiento.negocio';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { EstiloItemPensamiento, TipoPensamiento } from '@shared/diseno/enums/tipo-pensamiento.enum';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { Configuracion, ItemPensamiento, PensamientoCompartido } from "@shared/diseno/modelos/pensamiento";
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';

@Component({
  selector: 'app-pensamiento-compartido',
  templateUrl: './pensamiento-compartido.component.html',
  styleUrls: ['./pensamiento-compartido.component.scss']
})
export class PensamientoCompartidoComponent implements OnInit {
  @Input() pensamientoCompartido: PensamientoCompartido
  @Input() divPensamiento: string
  @Input() tamanoSvg: string
  @Input() dataLista: DatosLista
  @Output() unClick: EventEmitter<ItemPensamiento>
  @Output() dobleClick: EventEmitter<ItemPensamiento>
  @Output() clickLargo: EventEmitter<ItemPensamiento>
  dataPensamiento: Configuracion
  error: string
  public tipoPensamientos = TipoPensamiento

  constructor(
    private pensamientoNegocio: PensamientoNegocio,
    public internacionalizacionNegocio: InternacionalizacionNegocio,
    private router: Router,
    public estiloTexto: EstiloDelTextoServicio,
  ) {
    this.unClick = new EventEmitter<ItemPensamiento>();
    this.dobleClick = new EventEmitter<ItemPensamiento>();
    this.clickLargo = new EventEmitter<ItemPensamiento>();
  }

  ngOnInit(): void {

    this.cargarPensamiento()
  }

  cargarPensamiento() {

    if (this.pensamientoCompartido.tipoPensamiento === TipoPensamiento.PENSAMIENTO_ALEATORIO) {
      this.tamanoSvg = 'forma-contenedor-pensamiento-login'
    }
    if (this.pensamientoCompartido.tipoPensamiento === TipoPensamiento.PENSAMIENTO_PERFIL) {
      this.tamanoSvg = 'forma-contenedor-pensamiento-perfil'
    }
    if (this.pensamientoCompartido.tipoPensamiento === TipoPensamiento.PENSAMIENTO_RESUMEN_CON_TITULO) {
      this.tamanoSvg = 'forma-contenedor-pensamiento-perfil'
    }

    this.error = ""

    if (this.pensamientoCompartido && (this.pensamientoCompartido.tipoPensamiento === TipoPensamiento.PENSAMIENTO_ALEATORIO)) {
      this.obtenerPensamientoAleatorio()
    } else {
      if (this.pensamientoCompartido && (this.pensamientoCompartido.tipoPensamiento === TipoPensamiento.PENSAMIENTO_PRIVADO_CREACION)) {
        this.divPensamiento = 'divPensamientoFormaRoja'
      } else {
        if (this.pensamientoCompartido && (this.pensamientoCompartido.tipoPensamiento === TipoPensamiento.PENSAMIENTO_PUBLICO_CREACION)) {
          this.divPensamiento = 'divPensamientoFormaAmarilla'
        }
      }
    }
  }

  obtenerPensamientoAleatorio() {
    this.dataPensamiento = { data: { texto: "" }, presentarX: false }
    this.error = ""
    this.divPensamiento = 'divPensamientoAleatorio' //CLASE PARA EL ESTILO
    this.pensamientoNegocio.obtenerPensamientoAleatorio()
      .subscribe((res: PensamientoModel) => {

        if (res) {
          this.dataPensamiento = { data: res, presentarX: false }
        }
      }, error => {

        this.error = error
      })
  }

  eventoClick(index: number, pensamientoModel: PensamientoModel) {
    this.unClick.emit({ indice: index, pensamiento: pensamientoModel })
  }

  eventoDobleClick(index: number, pensamientoModel: PensamientoModel) {
    this.dobleClick.emit({ indice: index, pensamiento: pensamientoModel })
  }

  eventoClickLargo(index: number, pensamientoModel: PensamientoModel) {
    this.clickLargo.emit({ indice: index, pensamiento: pensamientoModel })
  }

  configurarDatos(index: number, pensamientoModel: PensamientoModel): Configuracion {
    const data = {
      data: pensamientoModel,
      estilo: EstiloItemPensamiento.ITEM_ALEATORIO,
      dobleClick: () => this.eventoDobleClick(index, pensamientoModel),
      presentarX: this.pensamientoCompartido.configuracionItem.presentarX,
      onclick: () => this.eventoClick(index, pensamientoModel)
    }
    return data
  }
}
