import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import {
  ColorDelTexto, EstilosDelTexto
} from '@shared/diseno/enums/estilos-colores-general';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { BloquePortada, ConfiguracionPortadaExandida, TipoBloqueBortada } from '@shared/diseno/modelos/portada-expandida.interface';

@Component({
  selector: 'app-portada-expandida',
  templateUrl: './portada-expandida.component.html',
  styleUrls: ['./portada-expandida.component.scss']
})
export class PortadaExpandidaComponent implements OnInit {

  @Input() configuracion: ConfiguracionPortadaExandida
  public TipoBloquePortadaEnum = TipoBloqueBortada

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void { }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.mostrarLoader = false
    }
  }

  obtenerClasesBloque(bloque: BloquePortada, index: number) {
    const clases = {}
    clases['item'] = true
    clases[bloque.tipo.toString()] = true
    clases[bloque.colorFondo.toString()] = true
    if (bloque.conSombra) {
      clases[bloque.conSombra.toString()] = true
    }

    if (bloque.tipo === TipoBloqueBortada.BOTON_PROYECTO) {
      clases['padding139'] = true
    }

    if (bloque.botones && bloque.botones.length > 0) {
      clases['con-botones'] = true
    }

    return clases
  }

  obtenerEstiloDelTextoSegunBloque(bloque: BloquePortada): EstilosDelTexto {
    switch (bloque.tipo) {
      case TipoBloqueBortada.BOTON_PROYECTO:
        return EstilosDelTexto.BOLD
      case TipoBloqueBortada.BOTON_NOTICIA:
        return EstilosDelTexto.BOLD
      case TipoBloqueBortada.INFO_PROYECTO:
        return EstilosDelTexto.REGULAR
      case TipoBloqueBortada.NO_ADDED_INFO:
        return EstilosDelTexto.BOLD
      case TipoBloqueBortada.ACTUALUZADO_INFO:
        return EstilosDelTexto.BOLDCONSOMBRA
      default:
        return EstilosDelTexto.REGULAR
    }
  }

  obtenerColorDelTextoSegunBloque(bloque: BloquePortada): ColorDelTexto {
    switch (bloque.tipo) {
      case TipoBloqueBortada.BOTON_PROYECTO:
        return ColorDelTexto.TEXTOBLANCO
      case TipoBloqueBortada.BOTON_NOTICIA:
        return ColorDelTexto.TEXTOBLANCO
      case TipoBloqueBortada.INFO_PROYECTO:
        return ColorDelTexto.TEXTOBLANCO
      case TipoBloqueBortada.NO_ADDED_INFO:
        return ColorDelTexto.TEXTOBLANCO
      case TipoBloqueBortada.ACTUALUZADO_INFO:
        return ColorDelTexto.TEXTOROJOBASE
        break
      default:
        return ColorDelTexto.TEXTOBLANCO
    }
  }

  obtenerMayusculaDelTextoSegunBloque(bloque: BloquePortada): boolean {
    switch (bloque.tipo) {
      case TipoBloqueBortada.BOTON_PROYECTO:
        return true
      case TipoBloqueBortada.BOTON_NOTICIA:
        return true
      case TipoBloqueBortada.INFO_PROYECTO:
        return true
      case TipoBloqueBortada.NO_ADDED_INFO:
        return false
      case TipoBloqueBortada.ACTUALUZADO_INFO:
        return true
      default:
        return false
    }
  }

  obtenerTamanoLetraConInterlineadoDelTextoSegunBloque(bloque: BloquePortada): TamanoDeTextoConInterlineado {
    switch (bloque.tipo) {
      case TipoBloqueBortada.BOTON_PROYECTO:
        return TamanoDeTextoConInterlineado.L3_I6
      case TipoBloqueBortada.BOTON_NOTICIA:
        return TamanoDeTextoConInterlineado.L3_I6
      case TipoBloqueBortada.INFO_PROYECTO:
        return TamanoDeTextoConInterlineado.L3_I6
      case TipoBloqueBortada.NO_ADDED_INFO:
        return TamanoDeTextoConInterlineado.L2_I2
      case TipoBloqueBortada.ACTUALUZADO_INFO:
        return TamanoDeTextoConInterlineado.L7_IGUAL
      default:
        return TamanoDeTextoConInterlineado.L2_I2
    }
  }

  tapEnBloque(bloque: BloquePortada) {
    if (bloque.eventoTap) {
      bloque.eventoTap()
    }
  }

  eventoTapPortada() {

    if (!this.configuracion.eventoTapPortada || !this.configuracion.eventoTapPortada.activarEvento) {
      return
    }

    if (this.configuracion.eventoTapPortada.evento) {
      this.configuracion.eventoTapPortada.evento()
    }
  }

  eventoDobleTapPortada() {
    if (!this.configuracion.eventoDobleTapPortada || !this.configuracion.eventoDobleTapPortada.activarEvento) {
      return
    }

    if (this.configuracion.eventoDobleTapPortada.evento) {
      this.configuracion.eventoDobleTapPortada.evento()
    }
  }
}
