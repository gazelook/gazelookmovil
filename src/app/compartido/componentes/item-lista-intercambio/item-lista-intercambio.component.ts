import { Component, Input, OnInit } from '@angular/core';
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ColorFondoLinea, EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { ConfiguracionItemListaIntercambio } from '@shared/diseno/modelos/item-lista-intercambio.interface';

@Component({
  selector: 'app-item-lista-intercambio',
  templateUrl: './item-lista-intercambio.component.html',
  styleUrls: ['./item-lista-intercambio.component.scss']
})
export class ItemListaIntercambioComponent implements OnInit {

  @Input() configuracion: ConfiguracionItemListaIntercambio

  public confLinea: LineaCompartida

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {

  }

  ngOnInit(): void {
    this.configurarLinea()
  }

  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO6382,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR041,
    }
  }

  eventoTap() {
    if (this.configuracion.eventoTap) {

      if (this.configuracion.intercambio) {
        this.configuracion.eventoTap(this.configuracion.intercambio.id)
        return
      }
    }
  }
}
