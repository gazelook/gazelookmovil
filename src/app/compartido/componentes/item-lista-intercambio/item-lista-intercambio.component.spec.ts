import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemListaIntercambioComponent } from './item-lista-intercambio.component';

describe('ItemListaIntercambioComponent', () => {
  let component: ItemListaIntercambioComponent;
  let fixture: ComponentFixture<ItemListaIntercambioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemListaIntercambioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemListaIntercambioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
