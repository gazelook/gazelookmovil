import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { AnuncioSistemaModel } from 'dominio/modelo/entidades/anuncio-sistema.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { CodigosCatalogoTipoAnuncioSistema } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-estilo-anuncio-sistema.enum';
import { AnuncioPublicitarioCompartido } from '@shared/diseno/modelos/anuncio-publicitario.interface';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AnuncioSistemaNegocio } from 'dominio/logica-negocio/anuncio-sistema.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core'
import { PortadaGazeCompartido } from '@shared/diseno/modelos/portada-gaze.interface'
import { TamanoPortadaGaze } from '@shared/diseno/enums/estilos-tamano-general.enum'

@Component({
	selector: 'app-portada-gaze',
	templateUrl: './portada-gaze.component.html',
	styleUrls: ['./portada-gaze.component.scss']
})
export class PortadaGazeComponent implements OnInit, OnDestroy {

	@Input() configuracionPortada: PortadaGazeCompartido

	public TamanoPortadaGazeEnum = TamanoPortadaGaze
	public perfilSeleccionado: PerfilModel
	public codigosCatalogoTipoAnuncioSistema = CodigosCatalogoTipoAnuncioSistema
	public configuracionAnuncioPublicitario: Array<AnuncioPublicitarioCompartido>

	public intervaloDeCambio: any
	public urlImagenes: Array<ArchivoModel>
	public urlDefinida: string
	public esAnuncio: boolean = false;
	public mostrarAnuncio: boolean = false
	public mostrarImagenes: boolean = false
	public itemAnuncio: any;
	public listaAnuncio: Array<any>

	public listaAnuncioRemote = [
	
	];

	public listaConfig = [
		{ cantidad: 30, intervalo: 48 },
		{ cantidad: 60, intervalo: 24 },
		{ cantidad: 90, intervalo: 16 },
		{ cantidad: 120, intervalo: 12 },
		{ cantidad: 150, intervalo: 9.6 },
		{ cantidad: 180, intervalo: 8 },
		{ cantidad: 210, intervalo: 6.8 },
		{ cantidad: 240, intervalo: 6 },
	]
	public unidadMinima
	public total
	public config


	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		private anuncioSistemaNegocio: AnuncioSistemaNegocio,
		private perfilNegocio: PerfilNegocio,
		private mediaNegocio: MediaNegocio, 
	) {
		this.listaAnuncio = [];
		this.urlImagenes = []

		this.urlDefinida = 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/portada-gaze-completa.png'
	}

	async ngOnInit() {

		this.obtenerArchivos()
		this.configuracionPortada.mostrarLoader = true
		this.configurarPerfilSeleccionado()
		
		if (this.configuracionPortada.tamano !== this.TamanoPortadaGazeEnum.PORTADACORTADA) {
			await this.obtenerAnuncios()
			if (!this.mostrarAnuncio) {
				this.mostrarImagenes = true
				this.configurarAnuncios()
			}
		}
	}
	obtenerArchivos(){
		let archivos: ArchivoModel[] = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal()
		if (archivos) {
			this.urlImagenes = archivos.filter(element => element.catalogoArchivoDefault === CodigosCatalogoArchivosPorDefecto.ANUNCIOS_PORTADA)
		}
		
	}

	ngOnDestroy(): void {
		if (this.intervaloDeCambio) {
			clearInterval(this.intervaloDeCambio)
		}

		this.intervaloDeCambio = undefined
	}

	configurarPerfilSeleccionado() {
		this.perfilSeleccionado = this.perfilNegocio.obtenerPerfilSeleccionado()
	}

	obtenerClasesPortada() {
		const clases = {}
		clases['portada'] = true
		clases[this.configuracionPortada.tamano.toString()] = true
		if (this.configuracionPortada.espacioDerecha) {
			clases['espacio-derecha'] = this.configuracionPortada.espacioDerecha
		}

		return clases
	}

	reDibujar(a: PortadaGazeCompartido) {
		this.configuracionPortada = a
	}

	siImagenEstaCargada(evt: any) {
		if (evt && evt.target) {
			const x = evt.srcElement.x
			const y = evt.srcElement.y
			if ((x === 0) && (y === 0)) {
				const width = evt.srcElement.width
				const height = evt.srcElement.height
				const portrait = height > width ? true : false
			}
			this.configuracionPortada.mostrarLoader = false
		}
	}


	fotoAleatorio(): string {
		try {
			const item = this.urlImagenes[Math.floor(Math.random() * this.urlImagenes.length)]
			return item.url
		} catch (error) {
			return 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/portada-gaze-completa.png'
		}
	}

	async obtenerAnuncios() {
		if (this.perfilSeleccionado === undefined) {
			return 
		}
		this.listaAnuncio = await this.anuncioSistemaNegocio.obtenerAnunciosSistemaPublicitario(this.perfilSeleccionado._id).toPromise();

		for (let i = 0; i < this.listaAnuncio.length; i++) {
			const anuncio = this.listaAnuncio[i];

			this.listaAnuncioRemote.push(this.configurarAnuncioSistema(anuncio, '0vw'));
		}

		this.unidadMinima = this.obtenerUnidadMinima();
		this.total = this.obtenerTotal();
		this.config = this.listaConfig.find((e) => e.cantidad === this.total);

		if (this.config) {
			this.iniciarReloj()
			this.mostrarAnuncio = true
		} else {
		 
		}
	}

	configurarAnuncios() {
		if (
			!this.configuracionPortada ||
			!this.configuracionPortada.imagenAleatoriaRamdon ||
			this.configuracionPortada.tamano !== TamanoPortadaGaze.PORTADACOMPLETA
		) {
			return
		}
		if (this.urlDefinida === 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/fondos/portada-gaze-completa.png') {
			this.intervaloDeCambio = setInterval(() => {

				const item = this.fotoAleatorio()
				
				this.urlDefinida = item
			}, 5000)
		} else {
			this.intervaloDeCambio = setInterval(() => {
				const item = this.fotoAleatorio()
				this.urlDefinida = item
			}, 9000)
		}
	}

	configurarAnuncioSistema(anuncio: AnuncioSistemaModel, espacio: string): AnuncioPublicitarioCompartido {
		let colorFondo = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.FONDO).color;
		let colorTexto = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.LETRA).color;
		let tamanioTexto = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.TAMANIO).tamanioLetra;

		let formatoColor = colorFondo;

		if (anuncio.tipo.codigo === 'TIPANU_01') {
			let colorFondo2 = anuncio.configuraciones[0].estilos.find((e) => e.tipo.codigo == this.codigosCatalogoTipoAnuncioSistema.FONDO && e.color.toLowerCase() !== colorFondo.toLowerCase()).color
			formatoColor = 'linear-gradient(90deg, ' + formatoColor + ', ' + colorFondo2 + ')';
		}

		return {
			titulo: anuncio?.titulo,
			descripcion: anuncio.descripcion,
			fecha: anuncio.fechaInicio,
			tipo: anuncio.tipo,
			colorFondo: colorFondo ? colorFondo : (anuncio.tipo.codigo == 'TIPANU_01' ? '#ff8b8b' : anuncio.tipo.codigo == 'TIPANU_02' ? '#082434' : anuncio.tipo.codigo == 'TIPANU_03' ? '#082434' : 'black'),
			colorTexto: colorTexto ? colorTexto : (anuncio.tipo.codigo == 'TIPANU_01' ? 'white' : anuncio.tipo.codigo == 'TIPANU_02' ? '#9fc5ee' : anuncio.tipo.codigo == 'TIPANU_03' ? 'white' : '#bbb7b7'),
			tamanioLetra: tamanioTexto ? tamanioTexto : 'L4_I1',
			espacio: espacio,
			intervaloVisualizacion: anuncio.intervaloVisualizacion,
			formatoColor: formatoColor
		}
	}

	// NUEVO PROCESO

	obtenerUnidadMinima() {
		return Math.min(...this.listaAnuncioRemote.map((e) => e.intervaloVisualizacion));
	}

	obtenerTotal() {
		return this.listaAnuncioRemote.map((e) => e.intervaloVisualizacion).reduce((a, b) => a + b, 0);
	}

	sumarMinutoDate(fecha, minutos) {
		fecha.setMinutes(fecha.getMinutes() + minutos);
		return fecha;
	}

	generarListaImagen(length) {
		let lista = [{ url: this.urlDefinida, timeout: 5, esAnuncio: false, orden: 1 }];
		for (let i = 0; i < length; i++) {
			let timeout = 10;
			if (i === length - 1) {
				timeout = 5;
			}

			lista.push({ url: this.urlImagenes[Math.floor(Math.random() * this.urlImagenes.length)].url, timeout: timeout, esAnuncio: false, orden: i + 2 });
		}

		return lista;
	}

	generarListaImagenV1(length) {
		let lista = [{ url: this.urlDefinida, timeout: 5, esAnuncio: false, orden: 1 }];
		for (let i = 0; i < length; i++) {
			let timeout = 9;
			if (i === length - 1) {
				timeout = 1;
			}

			lista.push({ url: this.urlImagenes[Math.floor(Math.random() * this.urlImagenes.length)].url, timeout: timeout, esAnuncio: false, orden: i + 2 });
		}

		return lista;
	}

	diferenciaFechas(fechaMayor, fechaMenor) {
		return Math.floor((fechaMayor.getTime() - fechaMenor.getTime()) / 1000);
	}

	generarIntervaloTiempo(intervalo) {
		let lista = [];
		let fecha = new Date();
		let fechaHoraInicio = new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate());
		let timeout = 60 * 5;
		let orden = 1;

		while (fechaHoraInicio.getDate() === fecha.getDate()) {
			for (let anuncio of this.listaAnuncioRemote) {
				let cantRepite = anuncio.intervaloVisualizacion / this.unidadMinima;

				for (let i = 0; i < cantRepite; i++) {
					let fechaHoraInicioAux = new Date(fechaHoraInicio.toString());
					let fechaHoraFin = this.sumarMinutoDate(fechaHoraInicio, intervalo);
					let tiempoRestante = this.diferenciaFechas(fechaHoraFin, this.sumarMinutoDate(new Date(fechaHoraInicioAux.toString()), 5));
					let listaImagenes = tiempoRestante >= 10 ? this.generarListaImagenV1(tiempoRestante / 10) : [];

					let listaProceso = [{ timeout: timeout, esAnuncio: true, anuncio: anuncio, orden: orden }];
					listaProceso.push.apply(listaProceso, listaImagenes);

					lista.push({
						horaInicio: fechaHoraInicioAux.toString(),
						horaFin: fechaHoraFin.toString(),
						listaProceso: listaProceso
					});

					orden++;
				}
			}
		}

		return lista;
	}

	delay(ms) {
		return new Promise((resolve, reject) => setTimeout(resolve, ms));
	}

	async iniciarRelojAnuncio(element) {
		let lista = element.listaProceso;

		let minutosPasado = new Date().getMinutes() - new Date(element.horaInicio).getMinutes();
		let empiezaImagen = (minutosPasado - 5) * 6;

		for (let e of lista) {
			this.esAnuncio = e.esAnuncio;

			if (e.esAnuncio) {
				if (minutosPasado >= 0 && minutosPasado < 5) {
					this.itemAnuncio = e.anuncio;
					await this.delay((e.timeout - (minutosPasado * 60)) * 1000);
				}

				empiezaImagen = 0;
			} else {
				minutosPasado = 0; // Al terminar las imagenes permite continuar con el anuncio

				if (empiezaImagen === 0) {
					this.urlDefinida = e.url;
					await this.delay(e.timeout * 1000);
				} else {
					empiezaImagen--;
				}
			}
		}
	}

	async iniciarReloj() {
		let fechaActual = new Date();
		let lista = this.generarIntervaloTiempo(this.config.intervalo);
		let listaLimpia = lista.filter((e) => (fechaActual >= new Date(e.horaInicio) && fechaActual < new Date(e.horaFin)) || new Date(e.horaInicio) > fechaActual);

		for (let e of listaLimpia) {
			if (e.listaProceso) { // si tiene registros
				await this.iniciarRelojAnuncio(e);
			}
		}
	}

}
