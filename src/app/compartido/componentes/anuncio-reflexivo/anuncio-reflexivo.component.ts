import { PathSvgAnuncioReflexivo } from '@shared/componentes/anuncio-publicitario/path-svg';
import { AnuncioPublicitarioCompartido } from '@shared/diseno/modelos/anuncio-publicitario.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-anuncio-reflexivo',
  templateUrl: './anuncio-reflexivo.component.html',
  styleUrls: ['./anuncio-reflexivo.component.scss']
})
export class AnuncioReflexivoComponent implements OnInit {

  @Input() configuracion: AnuncioPublicitarioCompartido

  public pathSvgAnuncioReflexivo = PathSvgAnuncioReflexivo

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void {
    
  }

}
