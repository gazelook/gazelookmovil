import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnuncioPublicitarioComponent } from './anuncio-publicitario.component';

describe('AnuncioPublicitarioComponent', () => {
  let component: AnuncioPublicitarioComponent;
  let fixture: ComponentFixture<AnuncioPublicitarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnuncioPublicitarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnuncioPublicitarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
