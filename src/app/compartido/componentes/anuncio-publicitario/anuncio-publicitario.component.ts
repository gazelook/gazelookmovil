import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { PathSvgAnuncioPublicitario } from './path-svg';
import { AnuncioPublicitarioCompartido } from '@shared/diseno/modelos/anuncio-publicitario.interface';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-anuncio-publicitario',
  templateUrl: './anuncio-publicitario.component.html',
  styleUrls: ['./anuncio-publicitario.component.scss']
})
export class AnuncioPublicitarioComponent implements OnInit {

  @Input() configuracion: AnuncioPublicitarioCompartido
  public urlsvg: string
  public id = 'SVGID_1_'
  public gradiente  = 'linear-gradient(90deg, rgb(20%, 40%, 60%), rgb(60%, 20%, 40%))'
  public pathSvgAnuncioPublicitario = PathSvgAnuncioPublicitario

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  )   {   }

  ngOnInit(): void {
    this.urlsvg = this.configuracion.tipoDegradado
  }
}
