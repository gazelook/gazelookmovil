import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { TamanoLista } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { DatosLista } from '@shared/diseno/modelos/datos-lista.interface';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import {
  ColorDeBorde,
  ColorDeFondo
} from '@shared/diseno/enums/estilos-colores-general';
import { NoticiaEntity } from 'dominio/entidades/noticia.entity';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ConfiguracionListaNoticias } from '@shared/diseno/modelos/lista-noticias.interface';

@Component({
  selector: 'app-lista-noticias',
  templateUrl: './lista-noticias.component.html',
  styleUrls: ['./lista-noticias.component.scss'],
})
export class ListaNoticiasComponent implements OnInit, OnChanges {
  @Input() configuracion: ConfiguracionListaNoticias;
  public listaNoticias: DatosLista;
  public archivosPorDefecto: Array<ArchivoModel>;
  public archivoPorDefectoNoticia: ArchivoModel;
  public placeholderNoticia: CongifuracionItemProyectosNoticias;
  public util = FuncionesCompartidas;
  public mostrarListaNoticias = false;
  public paginacionNoticias: PaginacionModel<NoticiaEntity>;
  public error: string;
  public ultimaPagina = false;
  public puedeCargarMas = true;
  public paddinNoticiaActualizado = false;

  constructor(public estiloTexto: EstiloDelTextoServicio,
    public mediaNegocio: MediaNegocio,
    private noticiaNegocio: NoticiaNegocio) {
    this.archivosPorDefecto = [];
    this.paginacionNoticias = {
      paginaActual: 0,
      proximaPagina: true,
    };
  }

  ngOnInit(): void {
    this.obtenerArchivosPorDefecto();
    this.configurarPlaceholders();
    this.paddinNoticiaActualizado = this.configuracion?.noticias[0]?.actualizado;
  }
  ngOnChanges() {
  }

  cargarNoticias() {
    this.listaNoticias = {
      tamanoLista: TamanoLista.LISTA_CONTACTOS,
      lista: [],
      cargarMas: () => this.configuracionNoticias(),
      cargando: false,
    };
  }

  cargarMas() {
    let element = document.getElementById('scroll');
    if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {
      this.puedeCargarMas = false;
      if (this.configuracion.eventoScroll) {
        this.configuracion.eventoScroll();
      }
    }
  }

  configuracionNoticias(noticias?) {
    this.listaNoticias.lista = [];
    if (noticias) {
      this.configuracion.noticias = noticias;
    }

    for (const noticia of this.configuracion.noticias) {
      this.listaNoticias.lista.push(this.configNoticia(noticia));
    }
  }

  configNoticia(noticia): CongifuracionItemProyectosNoticias {
    return {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: noticia.traducciones[0].tituloCorto,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: noticia.fechaActualizacion,
          formato: 'dd/MM/yyyy',
        },
      },
      loader: true,
      urlMedia: noticia.adjuntos[0]?.portada.principal.url,
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (id: string) => {
        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: noticia._id,
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
    };
  }

  obtenerArchivosPorDefecto() {
    this.mediaNegocio.obtenerListaArchivosDefault().subscribe((archivos) => {
      for (const archivo of archivos) {
        if (
          archivo.catalogoArchivoDefault ===
          CodigosCatalogoArchivosPorDefecto.PROYECTOS
        ) {
          this.archivosPorDefecto.push(archivo);
        }
      }

      this.archivoPorDefectoNoticia = this.util.randomItem(
        this.archivosPorDefecto
      );

      if (this.placeholderNoticia) {
        this.placeholderNoticia.loader = true;
        this.placeholderNoticia.urlMedia = this.archivoPorDefectoNoticia.url;
      }

      (error) => {
        if (this.placeholderNoticia) {
          this.placeholderNoticia.loader = false;
        }

      };
    });
  }

  configurarPlaceholders() {
    this.placeholderNoticia = {
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: 'm3v1texto12',
          textoBoton2: 'm3v1texto13',
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        },
      },
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      etiqueta: { mostrar: false },
      fecha: {
        mostrar: false,
        configuracion: { fecha: new Date(), formato: 'dd/MM/yyyy' },
      },
      loader: true,
      urlMedia: '',
      colorDeFondo: ColorDeFondo.FONDO_AZUL_CLARO,
      eventoTap: {
        activo: true,
        evento: (id: string) => {

        },
      },
      eventoPress: { activo: false },
      eventoDobleTap: { activo: false },
      id: '',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
    };
  }
}
