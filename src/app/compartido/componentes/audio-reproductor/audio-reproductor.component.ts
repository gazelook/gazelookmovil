import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from 'src/app/nucleo/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from 'src/app/nucleo/servicios/generales/generador-id.service';
import { DireccionDelReproductor } from '@shared/diseno/enums/audio-reproductor.enum';
import { ConfiguracionAudioReproductor } from '@shared/diseno/modelos/audio-reproductor.interface';
import { DiametroDelLoader } from '@shared/componentes/cargando/cargando.component';

@Component({
  selector: 'app-audio-reproductor',
  templateUrl: './audio-reproductor.component.html',
  styleUrls: ['./audio-reproductor.component.scss']
})
export class AudioReproductorComponent implements OnInit, AfterViewInit {
  @Input() configuracion: ConfiguracionAudioReproductor

  public DiametroDelLoaderEnum = DiametroDelLoader
  public audioTag: HTMLAudioElement
  public intervaloReproduccion: any

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private generadorId: GeneradorId
  ) { }

  ngOnInit(): void {
    // Id interno
    if (
      this.configuracion &&
      !this.configuracion.idInterno
    ) {
      this.configuracion.idInterno = 'audio_' + this.generadorId.generarIdConSemilla()
    }

    // Tiempo de reproduccion
    if (
      this.configuracion &&
      !this.configuracion.tiempoReproduccion
    ) {
      this.configuracion.tiempoReproduccion = {
        actual: 0,
        total: parseInt(this.configuracion.media.principal.duracion),
        factor: 100 / parseInt(this.configuracion.media.principal.duracion)
      }
    }

    // Redondeo
    if (!Math['round10']) {
      Math['round10'] = (value: any, exp: any) => {
        return this.ajustarDecimales('round', value, exp)
      };
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (!this.audioTag) {
        this.audioTag = document.getElementById(this.configuracion.idInterno) as HTMLAudioElement
      }

      if (this.audioTag && this.audioTag !== null) {
        this.audioTag.load()
        this.cuandoFinaliceReproduccionDelAudio()
        this.cuandoSeEmpieceReproducirAudio()
      }
    })
  }

  definirDuracionDelAudio(evento: any) {
    this.configuracion.mostrarLoader = false
  }

  cuandoFinaliceReproduccionDelAudio() {
    this.audioTag.onended = () => {
      this.configuracion.reproduciendo = false
      this.configuracion.tiempoReproduccion.actual = 0
      if (this.intervaloReproduccion) {
        clearInterval(this.intervaloReproduccion)
      }
      this.intervaloReproduccion = null
    }
  }

  cuandoSeEmpieceReproducirAudio() {
    this.audioTag.onplay = () => {
      if (!this.intervaloReproduccion) {
        this.intervaloReproduccion = setInterval(() => {
          if (
            this.configuracion.reproduciendo &&
            this.configuracion.tiempoReproduccion.actual < this.configuracion.tiempoReproduccion.total
          ) {
            this.configuracion.tiempoReproduccion.actual += 1
          }
        }, 1000)
      }
    }
  }

  obtenerClasesParaReproductor() {
    const clases = {}
    clases['player'] = true
    if (this.configuracion.colorDeFondo) {
      clases[this.configuracion.colorDeFondo.toString()] = true
    }

    if (this.configuracion.direccion) {
      clases[this.configuracion.direccion.toString()] = true
    } else {
      clases[DireccionDelReproductor.HACIA_LA_DERECHA.toString()] = true
    }

    return clases
  }

  eventoTapIconoReproducir() {
    this.configuracion.reproduciendo = true
    this.audioTag.play()
  }

  eventoTapIconoPausar() {
    this.configuracion.reproduciendo = false
    this.audioTag.pause()
  }

  obtenerTiempoDeGrabacion(
    delTiempoMaximo: boolean = false
  ) {
    if (
      this.configuracion.tiempoReproduccion
    ) {
      const tiempo = (delTiempoMaximo) ? this.configuracion.tiempoReproduccion.total : this.configuracion.tiempoReproduccion.actual

      if (tiempo >= 60) {
        const minutosEnDecimal = Math['round10'](tiempo / 60, -2)
        const cuantosMinutosHay = parseInt(minutosEnDecimal.toString())
        const cuantosSegundosHay = tiempo - (cuantosMinutosHay * 60)
        return this.obtenerTextoSegunValor(cuantosMinutosHay) + ':' + this.obtenerTextoSegunValor(cuantosSegundosHay)
      }
      return '00:' + this.obtenerTextoSegunValor(tiempo)
    }

    return '00:00'
  }

  obtenerTextoSegunValor(tiempo: number): string {
    if (tiempo < 10) {
      return '0' + tiempo
    }

    if (tiempo >= 10 && tiempo < 60) {
      return tiempo.toString()
    }
  }

  ajustarDecimales(type: any, value: any, exp: any) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  obtenerEstiloLineaReproductor() {

    let ancho = 0
    if (
      this.configuracion.tiempoReproduccion
    ) {
      const { actual, factor } = this.configuracion.tiempoReproduccion
      ancho = actual * factor
    }

    return {
      'width': ancho + '%'
    }
  }

  eventoDobleTap() {
    if (this.configuracion.eventoDobleTap) {
      this.configuracion.eventoDobleTap(this.configuracion.media.id)
    }
  }

  eventoPress() {
    if (this.configuracion.eventoDobleTap) {
      this.configuracion.eventoPress(this.configuracion.media.id)
    }
  }

  obtenerEstilosParaContendorDelTexto() {
    const data = {}
    if (this.configuracion.colorDeFondoCustom) {
      if (this.configuracion.colorDeFondoCustom.indexOf('#') >= 0) {
        data['background-color'] = this.configuracion.colorDeFondoCustom
      } else {
        data['background-color'] = '#' + this.configuracion.colorDeFondoCustom
      }
    }
    return data
  }
}
