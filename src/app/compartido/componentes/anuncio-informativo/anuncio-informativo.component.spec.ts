import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnuncioInformativoComponent } from './anuncio-informativo.component';

describe('AnuncioInformativoComponent', () => {
  let component: AnuncioInformativoComponent;
  let fixture: ComponentFixture<AnuncioInformativoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnuncioInformativoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnuncioInformativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
