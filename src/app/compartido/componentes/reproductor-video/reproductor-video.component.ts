import { Component, Input, OnInit, Output } from '@angular/core';
import { ReproductorVideo } from '@shared/diseno/modelos/reproductor-video.interface';

@Component({
  selector: 'app-reproductor-video',
  templateUrl: './reproductor-video.component.html',
  styleUrls: ['./reproductor-video.component.scss']
})
export class ReproductorVideoComponent implements OnInit {

  @Input() configuracion: ReproductorVideo;

  urlVideo: string

  constructor() {
    this.urlVideo = '' 
   }

  ngOnInit(): void {
    this.urlVideo = this.configuracion.url
  }
}
