import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalOrigenFotoComponent } from './modal-origen-foto.component';

describe('ModalOrigenFotoComponent', () => {
  let component: ModalOrigenFotoComponent;
  let fixture: ComponentFixture<ModalOrigenFotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalOrigenFotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOrigenFotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
