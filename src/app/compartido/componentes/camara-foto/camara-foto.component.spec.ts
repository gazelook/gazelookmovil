import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CamaraFotoComponent } from './camara-foto.component';

describe('CamaraFotoComponent', () => {
  let component: CamaraFotoComponent;
  let fixture: ComponentFixture<CamaraFotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CamaraFotoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CamaraFotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
