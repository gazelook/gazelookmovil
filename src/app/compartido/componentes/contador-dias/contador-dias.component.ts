import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionContadorDias } from '@shared/diseno/modelos/contador-dias.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-contador-dias',
	templateUrl: './contador-dias.component.html',
	styleUrls: ['./contador-dias.component.scss']
})
export class ContadorDiasComponent implements OnInit {

	@Input() configuracion: ConfiguracionContadorDias

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio
	) {
	}

	ngOnInit(): void {
	}

	obtenerValorFormateado(numero: number): string {
		if (!numero) {
			return '00'
		}

		if (numero < 10) {
			return '0' + numero
		}

		return '' + numero
	}
}
