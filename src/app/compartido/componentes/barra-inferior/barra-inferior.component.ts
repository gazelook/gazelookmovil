import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BarraInferior } from "@shared/diseno/modelos/barra-inferior.interfce";
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { TipoBoton } from '@shared/componentes/button/button.component';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';

@Component({
  selector: 'app-barra-inferior',
  templateUrl: './barra-inferior.component.html',
  styleUrls: ['./barra-inferior.component.scss']
})
export class BarraInferiorComponent implements OnInit {
  @Input() barraInferior: BarraInferior
  botonCompartido: BotonCompartido
  dataForm: FormGroup
  txtContadorPalabras: string
  mostrarIconoTexto: boolean

  constructor(
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {
    this.iniciarDatos()
    this.txtContadorPalabras = '000/230'
    this.mostrarIconoTexto = true
  }

  ngOnInit(): void {

  }
  //Escucho el cambio del texto a actualizar de varible texto para presentar en el input
  ngOnChanges() {
    if (this.barraInferior) {
      this.dataForm.get('texto').patchValue(this.barraInferior.input.data.texto)
    }
  }

  iniciarDatos() {
    this.dataForm = this.formBuilder.group({
      texto: ['', [Validators.required, Validators.maxLength(230)]],
    });
    this.botonCompartido = { tipoBoton: TipoBoton.ICON, enProgreso: false, ejecutar: () => this.agregarTexto() }
  }
  //Detecta el click del boton de la barra inferior
  agregarTexto() {
    if (this.dataForm.valid) {
      this.barraInferior.input.data.texto = this.dataForm.controls.texto.value
      this.barraInferior.enviar()
      this.dataForm.reset()
      this.dataForm.value.texto = ''
      this.contadorPalabras()
    }
  }

  //condar de palabras input

  contadorPalabras() {

    let totalPalabras = this.dataForm.value.texto.length;

    if (totalPalabras <= 230) {
      if (this.dataForm.value.texto.length === 0) {

        this.txtContadorPalabras = '000/230';

      } else {
        if (totalPalabras <= 10) {
          this.txtContadorPalabras = `00${totalPalabras}/230`;

        } else if (totalPalabras > 10 && totalPalabras < 100) {

          this.txtContadorPalabras = `0${totalPalabras}/230`;
        }
        else if (totalPalabras >= 100) {

          this.txtContadorPalabras = `${totalPalabras}/230`;
        }
      }
    } else {
      this.dataForm.patchValue({ texto: this.dataForm.value.texto.slice(0, -1) })
    }
  }

  empezarEscribir() {
    this.mostrarIconoTexto = false
  }

  dejarDeEscribir() {
    this.mostrarIconoTexto = true
  }

  empezarAescribir() {
    const elemento = document.getElementById('input_barr_mdfd') as HTMLInputElement
    if (elemento) {
      elemento.focus()
    }
  }
}
