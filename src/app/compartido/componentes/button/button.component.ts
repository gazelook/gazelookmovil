import { DiametroDelLoader } from '@shared/componentes/cargando/cargando.component';
import { Component, Renderer2, ElementRef, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';

@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
	@Input() botonCompartido: BotonCompartido
	public tipoBoton = TipoBoton //La referencia esta siendo utilizado para un ngswitch
	texto: Promise<string>;

	public DiametroDelLoaderEnum = DiametroDelLoader

	constructor(
		private internacionalizacionNegocio: InternacionalizacionNegocio,
	) { }

	ngOnInit(): void {

	}

	obtenerTexto() {
		if (this.botonCompartido) {
			if (this.botonCompartido.text) {
				return this.internacionalizacionNegocio.obtenerTextoSincrono(this.botonCompartido.text);
			}
		}
		return "";
	}


	execute() {
		this.botonCompartido.ejecutar();
	}

	obtenerEstilosBoton() {
		let tamano: string = ''

		if (
			this.botonCompartido.tamanoTexto &&
			this.botonCompartido.tamanoTexto.indexOf('letra-7') >= 0
		) {
			if (this.botonCompartido.text.length <= 4) {
				tamano = 'boton_1127'
			} else {
				tamano = 'boton_0979'
			}
		}

		if (
			this.botonCompartido.tamanoTexto &&
			this.botonCompartido.tamanoTexto.indexOf('letra-4') >= 0
		) { tamano = 'boton_8634' }

		if (
			this.botonCompartido.tamanoTexto &&
			this.botonCompartido.tamanoTexto.indexOf('letra-2') >= 0
		) { tamano = 'boton_7575' }

		if (
			this.botonCompartido.tamanoTexto &&
			this.botonCompartido.tamanoTexto.indexOf('letra-6') >= 0
		) {
			tamano = 'boton_0952'
		}

		return {
			'buttonBase': true,
			'botonCorto': this.botonCompartido.text.length <= 4,
			'botonLargo': this.botonCompartido.text.length > 4,
			[tamano]: true
		}
	}

	obtenerClasesDelTexto() {
		const clases = {}
		clases[this.botonCompartido.colorTexto] = true
		clases[this.botonCompartido.tamanoTexto] = true

		return clases
	}

	static crearBotonAfirmativo(funcion: Function): BotonCompartido {
		return {
			colorTexto: ColorTextoBoton.ROJO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			text: "m2v11texto4",
			ejecutar: funcion,
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO
		}
	}

	static crearBotonNegativo(funcion: Function): BotonCompartido {
		return {
			colorTexto: ColorTextoBoton.AMARRILLO,
			tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
			text: "m2v11texto5",
			ejecutar: funcion,
			enProgreso: false,
			tipoBoton: TipoBoton.TEXTO
		}
	}
}

export enum ColorTextoBoton {
	AMARRILLO = "txt-amarillo-base",
	VERDE = "txt-verde-base",
	ROJO = "txt-rojo-base",
	CELESTE = "txt-btn-history",
	BLANCO = "txt-btn-blanco",
	AZUL = "txt-azul-base",
	NARANJA_INTERCAMBIO = 'boton-intercambio-menu',
	CELESTEMAIN = 'txt-celeste-texto-main'
}

export enum TipoBoton {
	TEXTO,
	TEXTO_ICON,
	ICON,
	ICON_COLOR
}

export enum ColorIconoBoton {
	AZUL = 'icono-boton-azul',
	ROJO = 'icono-boton-rojo',
	AMARILLO = 'icono-boton-amarillo'
}

