import { Component, Input, OnInit } from '@angular/core';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionListaContactoCompartido } from '@shared/diseno/modelos/lista-contacto.interface';
import { ConfiguracionItemListaContactosCompartido } from '@shared/diseno/modelos/lista-contactos.interface';

@Component({
  selector: 'app-lista-contacto',
  templateUrl: './lista-contacto.component.html',
  styleUrls: ['./lista-contacto.component.scss']
})
export class ListaContactoComponent implements OnInit {
  @Input() configuracion: ConfiguracionListaContactoCompartido

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
    public internacionalizacionNegocio: InternacionalizacionNegocio,
  ) { }

  ngOnInit(): void {
    if (
      this.configuracion &&
      !this.configuracion.llaveSubtitulo
    ) {
      this.configuracion.llaveSubtitulo = 'm3v8texto4'
    }
  }

  eventoTap(contacto: ConfiguracionItemListaContactosCompartido) {
    if (this.configuracion.contactoSeleccionado) {
      this.configuracion.contactoSeleccionado(contacto.id)
    }
  }
}
