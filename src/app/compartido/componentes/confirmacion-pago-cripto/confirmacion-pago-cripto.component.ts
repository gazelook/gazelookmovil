import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ClipboardService} from '@env/node_modules/ngx-clipboard';

import {Coinpayments} from 'dominio/entidades/catalogos/coinpayments-rates.entity';

import {ColorTextoBoton, TipoBoton} from '@shared/componentes';
import {TamanoDeTextoConInterlineado} from '@shared/diseno/enums';
import {BotonCompartido} from '@shared/diseno/modelos';

import {RutasLocales} from '@env/src/app/rutas-locales.enum';


@Component({
  selector: 'app-confirmacion-pago-cripto',
  templateUrl: './confirmacion-pago-cripto.component.html',
  styleUrls: ['./confirmacion-pago-cripto.component.scss']
})
export class ConfirmacionPagoCriptoComponent implements OnInit {
  public coinpayments: Coinpayments;
  public isCopied = false;
  public confBoton: BotonCompartido;

  constructor(
    private clipboardService: ClipboardService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.coinpayments = JSON.parse(localStorage.getItem('respCoiPaiments'));
    this.configurarBotones();
  }

  copyDynamicText(): void {
    this.clipboardService.copyFromContent('Hola');
  }

  copied(event): void {
    this.isCopied = event.isSuccess;
  }

  configurarBotones(): void {
    this.confBoton = {
      text: 'text39',
      tipoBoton: TipoBoton.TEXTO,
      tamanoTexto: TamanoDeTextoConInterlineado.L4_IGUAL,
      colorTexto: ColorTextoBoton.AMARRILLO,
      enProgreso: false,
      ejecutar: () => {
        localStorage.removeItem('respCoiPaiments');
        this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
      },
    };
  }

}
