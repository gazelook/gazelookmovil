// import { EstiloDelTextoServicio } from './../../../nucleo/servicios/diseno/estilo-del-texto.service';
// import { ConfiguracionLlamadaMensaje } from './../../diseno/modelos/llamada.interface';
// import { Component, OnInit, Input } from '@angular/core';
// import { NgxAgoraService, Stream, AgoraClient, ClientEvent, StreamEvent } from 'ngx-agora';
// import { TipoLlamada } from '../../diseno/enums/llamada-agora.enum';
// @Component({
//     selector: 'app-agora',
//     templateUrl: './agora.component.html',
//     styleUrls: ['./agora.component.scss']
// })
// export class AgoraComponent implements OnInit {
//     @Input() configuracion: ConfiguracionLlamadaMensaje

//     title = 'agorawrtc-demo';
//     localCallId = 'agora_local';
//     remoteCalls: string[] = [];

//     private client: AgoraClient;
//     private localStream: Stream;
//     private uid: number;
//     public ocultarCancelar: boolean
//     public ocultarCrono: boolean
//     public crono: any
//     public m: HTMLInputElement
//     public s: HTMLInputElement

//     public contador_s: number
//     public contador_m: number
//     public contador_h: number

//     constructor(
//         private ngxAgoraService: NgxAgoraService,
//         public estiloDelTextoServicio: EstiloDelTextoServicio,
//     ) {

//         this.uid = Math.floor(Math.random() * 10000)
//         this.ocultarCancelar = false
//         this.ocultarCrono = false
//         this.contador_m = 0
//         this.contador_s = 0
//         this.contador_h = 0
//     }

//     ngOnInit() {
//         this.iniciarllamada()
//     }

//     iniciarCronometro() {
//         this.crono = setInterval(() => {
//             if (this.contador_s === 60) {
//                 this.contador_s = 0
//                 this.contador_m++

//                 if (this.contador_m == 60) {
//                     this.contador_h++
//                     this.contador_m = 0
//                 }
//             }
//             this.contador_s++
//         }, 1000)
//     }

//     finalizarCronometro() {
//         clearInterval(this.crono)
//     }

//     iniciarllamada() {
//         let video: boolean
//         if (this.configuracion.tipoLlamada === TipoLlamada.AUDIO) {
//             video = false
//         }

//         if (this.configuracion.tipoLlamada === TipoLlamada.VIDEO) {
//             video = true
//         }

//         this.ngxAgoraService.AgoraRTC.getDevices(()=>{

//         })

//         this.client = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
//         this.assignClientHandlers();
//         this.localStream = this.ngxAgoraService.createStream({ streamID: this.uid, audio: true, video: video, screen: false });
//         this.assignLocalStreamHandlers();
//         // Join and publish methods added in this step
//         this.initLocalStream(() => this.join(uid => this.publish(), error =>{}));
//     }

//     private assignClientHandlers(): void {
//         this.client.on(ClientEvent.LocalStreamPublished, evt => {
//         });

//         this.client.on(ClientEvent.Error, error => {

//             if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
//                 this.client.renewChannelKey(
//                     '',
//                     () => {},
//                     renewError => {}
//                 );
//             }
//         });

//         this.client.on(ClientEvent.RemoteStreamAdded, evt => {
//             const stream = evt.stream as Stream;
//             this.client.subscribe(stream, { audio: true, video: true }, err => {

//             });
//         });

//         this.client.on(ClientEvent.RemoteStreamSubscribed, evt => {

//             const stream = evt.stream as Stream;
//             const id = this.getRemoteId(stream);
//             if (!this.remoteCalls.length) {

//                 this.remoteCalls.push(id);
//                 setTimeout(() => stream.play(id), 1000);
//             }

//             this.iniciarCronometro()

//         });

//         this.client.on(ClientEvent.RemoteStreamRemoved, evt => {
//             const stream = evt.stream as Stream;
//             if (stream) {
//                 stream.stop();
//                 stream.close();
//                 this.remoteCalls = [];

//             }
//         });

//         this.client.on(ClientEvent.PeerLeave, evt => {
//             const stream = evt.stream as Stream;
//             if (stream) {
//                 stream.stop();
//                 stream.close();
//                 this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
//             }
//         });
//     }

//     private getRemoteId(stream: Stream): string {
//         return `agora_remote-${stream.getId()}`;
//     }

//     private assignLocalStreamHandlers(): void {
//         this.localStream.on(StreamEvent.MediaAccessAllowed, () => {

//         });

//         // The user has denied access to the camera and mic.
//         this.localStream.on(StreamEvent.MediaAccessDenied, () => {

//         });
//     }

//     private initLocalStream(onSuccess?: () => any): void {
//         this.localStream.init(
//             () => {
//                 // The user has granted access to the camera and mic.
//                 this.localStream.play(this.localCallId);
//                 if (onSuccess) {
//                     onSuccess();
//                 }
//             },
//             err => {

//             }
//         );
//     }
//     join(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
//         this.client.join(null, this.configuracion.infoAsociacion, this.uid, onSuccess, onFailure);
//     }

//     publish(): void {
//         this.client.publish(this.localStream, err => {});
//     }

//     cancelarLlamada() {
//         this.ngxAgoraService.client.leave(() => {
//             this.ocultarCrono = true
//             this.localStream.stop()
//             this.localStream.close()

//             if (this.configuracion.finalizar) {
//                 this.configuracion.finalizar(this.contador_h, this.contador_m, this.contador_s)
//             }
//             this.finalizarCronometro()
//             this.ocultarCancelar = true
//         }, (err) => {
//         });
//     }
// }
