import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { AccionesSelector } from '@shared/diseno/enums/acciones-general.enum';
import { ConfiguracionSelector } from '@shared/diseno/modelos/selector.interface';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {
  @Input() configuracion: ConfiguracionSelector

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
  ) { }

  ngOnInit() { 


  }

  ngAfterViewInit(): void { }

  clickInputSelector() {
    if (this.configuracion.evento) {
      this.configuracion.evento({
        accion: AccionesSelector.ABRIR_SELECTOR
      })
    }
  }
  
}
