import { ConfiguracionItemLink } from '@shared/diseno/modelos/item-link.interface';
import { Component, OnInit, Input } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';

@Component({
	selector: 'app-item-link',
	templateUrl: './item-link.component.html',
	styleUrls: ['./item-link.component.scss']
})
export class ItemLinkComponent implements OnInit {

	@Input() configuracion: ConfiguracionItemLink

	public CodigosCatalogoEntidadEnum = CodigosCatalogoEntidad

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio
	) { }

	ngOnInit(): void { 
		
	}

	eventoTap() {
		if (this.configuracion.eventoTap) {
			this.configuracion.eventoTap(
				this.configuracion.media.id,
				this.configuracion.media.principal.url
			)
		}
	}

	eventoDobleTap() {
		if (this.configuracion.eventoDobleTap) {
			this.configuracion.eventoDobleTap(this.configuracion.media.id)
		}
	}

	eventoPress() {
		if (this.configuracion.eventoPress) {
			this.configuracion.eventoPress(this.configuracion.media.id)
		}
	}
}
