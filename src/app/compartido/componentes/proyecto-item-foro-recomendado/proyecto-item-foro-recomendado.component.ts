import {Component, OnInit, Input} from '@angular/core';
import {CongifuracionItemProyectos} from '@shared/diseno/modelos';
import {CodigosCatalogoTipoAlbum} from '@core/servicios/remotos/codigos-catalogos';
import {EstiloDelTextoServicio} from '@core/servicios/diseno';
import {UsoItemProyectoNoticia} from '@shared/diseno/enums';


@Component({
  selector: 'app-proyecto-item-foro-recomendado',
  templateUrl: './proyecto-item-foro-recomendado.component.html',
  styleUrls: ['./proyecto-item-foro-recomendado.component.scss']
})
export class ProyectoItemForoRecomendadoComponent implements OnInit {
  @Input() proyecto: CongifuracionItemProyectos;

  public bordesEsquinas: number[];
  public click = 'click';
  public tipoLink = CodigosCatalogoTipoAlbum.LINK;

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) {
    this.bordesEsquinas = [0, 1, 2, 3];
  }

  ngOnInit(): void {
  }

  /**
   * @description
   * Método que se ejecuta cuando se hace click en el componente.
   * @param proyecto
   * Recibe el proyecto para realizar la acción.
   */
  eventoTap(proyecto: CongifuracionItemProyectos): void {
    if (
      this.proyecto.eventoTap.activo &&
      this.proyecto.eventoTap.evento
    ) {
      this.proyecto.eventoTap.evento(proyecto);
    }
  }

  /**
   * @description
   * Método que define las clases para los bordes de las esquinas.
   * @param index
   * Recibe el index para realizar la acción.
   */
  obtenerClasesBordesEnLasEsquinas(index: number):
    {
      esquina: boolean;
      blanca: boolean;
      a: boolean;
      b: boolean;
      c: boolean; d: boolean;
      mostrar: boolean;
    } {
    return {
      esquina: true,
      blanca: (!this.proyecto.usarMarcosDeConfiguracion),
      a: index === 0,
      b: index === 1,
      c: index === 2,
      d: index === 3,
      mostrar: true,
    };
  }

  /**
   * @description
   * Método que define las clases que crean el rectangulo.
   * param args Objeto con las clases.
   * Recibe el index para realizar la acción.
   */
  obtenerClasesParaItemRectangular(): {
    'item-proyecto-noticia': boolean,
    'proyecto-detalle': boolean,
    'noticia': boolean,
    'item-solo-texto': boolean,
    'mini-rectangulo': boolean
  } {
    const clases = {
      'item-proyecto-noticia': this.proyecto.usoItem !== UsoItemProyectoNoticia.SOLO_TEXTO, // Clase por defecto
      'proyecto-detalle':
        this.proyecto.usoItem === UsoItemProyectoNoticia.RECPROYECTO
        && this.proyecto.resumen,
      noticia: this.proyecto.usoItem === UsoItemProyectoNoticia.RECNOTICIA,
      'item-solo-texto': this.proyecto.usoItem === UsoItemProyectoNoticia.SOLO_TEXTO,
      'mini-rectangulo': this.proyecto.usoVersionMini,
    };

    clases[this.proyecto.colorDeFondo.toString()] = true;
    return clases;
  }

  /**
   * @description
   * Método que se ejecuta para comprobar si la imagen esta cargada
   * @param event
   * Recibe el evento html.
   */
  imgenCargada(event): void {
    if (!event && !event.target) {
      return;
    }
    const x = event.target.x;
    const y = event.target.y;
    if ((x === 0) && (y === 0)) {
      const width = event.target.width;
      const height = event.target.height;
      const portrait = height > width;
    }
    this.proyecto.loader = false;
  }

}
