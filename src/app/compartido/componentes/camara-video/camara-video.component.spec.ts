import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CamaraVideoComponent } from './camara-video.component';

describe('CamaraVideoComponent', () => {
  let component: CamaraVideoComponent;
  let fixture: ComponentFixture<CamaraVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CamaraVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CamaraVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
