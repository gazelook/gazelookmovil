import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemIntercambioComponent } from './item-intercambio.component';

describe('ItemIntercambioComponent', () => {
  let component: ItemIntercambioComponent;
  let fixture: ComponentFixture<ItemIntercambioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemIntercambioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemIntercambioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
