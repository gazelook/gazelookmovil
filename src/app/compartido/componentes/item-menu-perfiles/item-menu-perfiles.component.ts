import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { ItemAccion } from 'dominio/modelo/entidades/item-menu.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ColorFondoItemMenu, EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { ItemMenuCompartido } from '@shared/diseno/modelos/item-menu.interface';
import { LineaDeTexto } from '@shared/diseno/modelos/linea-de-texto.interface';

@Component({
  selector: 'app-item-menu-perfiles',
  templateUrl: './item-menu-perfiles.component.html',
  styleUrls: ['./item-menu-perfiles.component.scss']
})
export class ItemMenuPerfilesComponent implements OnInit, AfterViewInit {
  @ViewChild('itemMenu', { static: false }) itemMenu: ElementRef
  @Input() configuracionItem: ItemMenuCompartido
  public tipoMenu = TipoMenu

  texto4:  string;
  texto3:  string;
  texto2:  string;
  texto1:  string;
  textos: Promise<string>[] = [];

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
    private internacionalizacionNegocio: InternacionalizacionNegocio,
  ) {
  }

  ngOnInit(): void {
    
    this.obtenerTraducciones()
    this.obtenerTraduccionesListaPalabras()
    this.estiloTexto1()
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
    })
  }

  tap() {

    if (this.configuracionItem.onclick) {
      this.configuracionItem.onclick()
    }
  }

  dobletap() {
    if (this.configuracionItem.dobleClick) {
      this.configuracionItem.dobleClick()
    }
  }

  press() {
    try {
      this.configuracionItem.clickSostenido()
    } catch (error) {

    }
  }

  estiloTexto1() {
    switch (this.configuracionItem.colorFondo) {
      case ColorFondoItemMenu.PREDETERMINADO:
        return this.estiloTexto.obtenerEstilosTexto({
          color: this.estiloTexto.colorDelTexto.TEXTOAMARILLOBASE,
          estiloTexto: this.estiloTexto.estilosDelTexto.BOLDCONSOMBRA,
          enMayusculas: true,
          tamanoConInterlineado: this.estiloTexto.tamanoDeTextoConInterlineado.L5_IGUAL
        })
      case ColorFondoItemMenu.PERFILCREADO:
        return this.estiloTexto.obtenerEstilosTexto({
          color: this.estiloTexto.colorDelTexto.TEXTOPERFILCREADO,
          estiloTexto: this.estiloTexto.estilosDelTexto.BOLDCONSOMBRA,
          enMayusculas: true,
          tamanoConInterlineado: this.estiloTexto.tamanoDeTextoConInterlineado.L5_IGUAL
        })
      case ColorFondoItemMenu.PERFILHIBERNADO:
        return this.estiloTexto.obtenerEstilosTexto({
          color: this.estiloTexto.colorDelTexto.AZUL_HIBERNATE,
          estiloTexto: this.estiloTexto.estilosDelTexto.BOLDCONSOMBRA,
          enMayusculas: true,
          tamanoConInterlineado: this.estiloTexto.tamanoDeTextoConInterlineado.L5_IGUAL
        })
      case ColorFondoItemMenu.TRANSPARENTE:
        return this.estiloTexto.obtenerEstilosTexto({
          color: this.estiloTexto.colorDelTexto.TEXTOAMARILLOBASE,
          estiloTexto: this.estiloTexto.estilosDelTexto.BOLDCONSOMBRA,
          enMayusculas: true,
          tamanoConInterlineado: this.estiloTexto.tamanoDeTextoConInterlineado.L5_IGUAL
        })
    }
  }

  async obtenerTraducciones() {
    if (this.configuracionItem.texto1) {
      this.texto1 = await this.internacionalizacionNegocio.obtenerTextoLlave(this.configuracionItem.texto1);
    }
    if (this.configuracionItem.texto2) {
      this.texto2 = await this.internacionalizacionNegocio.obtenerTextoLlave(this.configuracionItem.texto2);
    }
    if (this.configuracionItem.texto3) {
      this.texto3 = await this.internacionalizacionNegocio.obtenerTextoLlave(this.configuracionItem.texto3);
    }
    if (this.configuracionItem.texto4) {
      this.texto4 =  await this.internacionalizacionNegocio.obtenerTextoLlave(this.configuracionItem.texto4);
    }
  }

  obtenerTraduccionesListaPalabras() {
    if (this.configuracionItem.descripcion) {
      this.textos = []
      for (let palabra of this.configuracionItem.descripcion) {

      }
    }

    if (this.configuracionItem.acciones) {
      this.textos = []
      for (let accion of this.configuracionItem.acciones) {
        this.textos.push(this.internacionalizacionNegocio.obtenerTextoLlave(accion.nombre))
      }
    }
  }

  // Obtener las clases del item segun su configuracion
  obtenerItemClases() {
    const clases = {}
    clases['itemMenu'] = true
    clases[this.configuracionItem.tamano.toString()] = true
    clases[this.configuracionItem.colorFondo.toString()] = true
    clases['gazeAnuncios'] = (this.configuracionItem.tipoMenu === TipoMenu.ANUNCIOS)
    clases['paddig-button-linea1'] = EspesorLineaItem.ESPESOR041 == this.configuracionItem.linea.configuracion.espesor
    clases['paddig-button-linea2'] = EspesorLineaItem.ESPESOR071 == this.configuracionItem.linea.configuracion.espesor
    clases['paddig-button-linea3'] = EspesorLineaItem.ESPESOR089 == this.configuracionItem.linea.configuracion.espesor
    return clases
  }

  // Obtener clases caja item
  obtenerClasesItemCaja() {
    return {
      'caja': true,
    }
  }

  // Obtener clases lineas texto
  obtenerClasesLineasTexto(linea: LineaDeTexto) {
    const clases = {}
    clases[linea?.color.toString()] = true
    clases[linea?.estiloTexto.toString()] = true
    clases[linea?.tamanoConInterlineado.toString()] = true
    clases['enMayusculas'] = linea?.enMayusculas
    return clases
  }

  obtenerSeparacionItemInstrucciones() {
    const clases = {}
    clases["instruccion-separacion-min"] = (this.configuracionItem.texto1) ? true : false
    clases["instruccion-separacion-media"] = (this.configuracionItem.texto1) ? false : true
    return clases
  }

  clickItemSubMenu(item: ItemAccion) {
    if (item.accion) {
      item.accion();
    }
  }

  //temporal, considere la eliminacion en caso de no usuarlo
  estiloTextoMenus(item: ItemAccion) {
    if (item.codigo == "g") {
      return this.estiloTexto.obtenerEstilosTexto({
        color: this.estiloTexto.colorDelTexto.GRIS,
        estiloTexto: this.estiloTexto.estilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: this.estiloTexto.tamanoDeTextoConInterlineado.L2_I8
      })
    } else {
      return this.estiloTexto.obtenerEstilosTexto({
        color: this.estiloTexto.colorDelTexto.GRIS,
        estiloTexto: this.estiloTexto.estilosDelTexto.BOLD,
        enMayusculas: true,
        tamanoConInterlineado: this.estiloTexto.tamanoDeTextoConInterlineado.L1_I8
      })
    }
  }
}


