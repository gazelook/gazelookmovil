import { Component, Input, OnInit } from '@angular/core';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { AccionesBuscadorLocalidadModal } from '@shared/diseno/enums/acciones-general.enum';
import { ConfiguracionBuscadorModal } from '@shared/diseno/modelos/buscador-modal.interface';

@Component({
	selector: 'app-buscador-modal',
	templateUrl: './buscador-modal.component.html',
	styleUrls: ['./buscador-modal.component.scss']
})
export class BuscadorModalComponent implements OnInit {
	@Input() configuracion: ConfiguracionBuscadorModal

	constructor(
		public estiloDelTextoServicio: EstiloDelTextoServicio,
		public generadorId: GeneradorId,
	) {   }

	ngOnInit() {

	}

	// Abrir selector
	clickInputPreview() {
		if (this.configuracion.evento) {
			this.configuracion.evento({
				accion: AccionesBuscadorLocalidadModal.ABRIR_BUSCADOR
			})
		}
	}
}
