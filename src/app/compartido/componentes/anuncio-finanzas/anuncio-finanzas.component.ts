import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { AnuncioPublicitarioCompartido } from '@shared/diseno/modelos/anuncio-publicitario.interface';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-anuncio-finanzas',
  templateUrl: './anuncio-finanzas.component.html',
  styleUrls: ['./anuncio-finanzas.component.scss']
})
export class AnuncioFinanzasComponent implements OnInit {

  @Input() configuracion: AnuncioPublicitarioCompartido

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void {
  }

}
