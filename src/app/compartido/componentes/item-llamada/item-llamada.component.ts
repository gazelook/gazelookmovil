import { Component, Input, OnInit } from '@angular/core';
import { CatalogoTipoMensaje } from '@core/servicios/remotos/codigos-catalogos/catalogo-mensaje.enum';
import { ColorDeBorde } from '@shared/diseno/enums/estilos-colores-general';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { FuncionesCompartidas } from '@core/util/funciones-compartidas';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { ConfiguracionItemLlamadaMensaje } from '@shared/diseno/modelos/llamada.interface';

@Component({
  selector: 'app-item-llamada',
  templateUrl: './item-llamada.component.html',
  styleUrls: ['./item-llamada.component.scss']
})
export class ItemLlamadaComponent implements OnInit {
  @Input() configuracion: ConfiguracionItemLlamadaMensaje
  public configuracionItemCircular: ItemCircularCompartido
  util = FuncionesCompartidas;
  public textoInfoLlamada: string
  public mostrarItem: boolean
  constructor(
    public estiloTexto: EstiloDelTextoServicio,
  ) {
    this.textoInfoLlamada = ''
    this.mostrarItem = false
  }

  ngOnInit(): void {
    this.textoResumen()
    this.configurarItemCircular()
  }

  textoResumen() {
    if (this.configuracion.enviado) {
      if (this.configuracion.tipoLlamada === CatalogoTipoMensaje.VIDEOLLAMADA) {
        this.textoInfoLlamada = 'Conectando'
      }

      if (this.configuracion.tipoLlamada === CatalogoTipoMensaje.LLAMADA) {
        this.textoInfoLlamada = 'Conectando'
      }

    } else {
      if (this.configuracion.tipoLlamada === CatalogoTipoMensaje.VIDEOLLAMADA) {
        this.textoInfoLlamada = 'Videollamada Entrante'
      }
      if (this.configuracion.tipoLlamada === CatalogoTipoMensaje.LLAMADA) {
        this.textoInfoLlamada = 'Llamada Entrante'
      }
      this.mostrarItem = true
    }
  }

  aceptarLlamada() {
    if (this.configuracion.aceptar) {
      this.configuracion.aceptar(true)
    }
  }

  cancelarLlamada() {
    if (this.configuracion.finalizar) {
      this.configuracion.finalizar(true)
    }
  }

  configurarItemCircular() {
    let usoItem: UsoItemCircular
    if (this.configuracion.infoParticipante.perfil.album[0].portada.principal.fileDefault) {
      usoItem = UsoItemCircular.CIRCARITAPERFIL
    } else {
      usoItem = UsoItemCircular.CIRPERFIL
    }
    this.configuracionItemCircular = {
      id: '',
      idInterno: '',
      usoDelItem: usoItem,
      esVisitante: false,
      urlMedia: this.configuracion.infoParticipante.perfil.album[0].portada.principal.url,
      activarClick: false,
      activarDobleClick: false,
      activarLongPress: false,
      mostrarBoton: true,
      mostrarLoader: true,
      textoBoton: '',
      capaOpacidad: {
        mostrar: false
      },
      esBotonUpload: false,
      colorBorde: ColorDeBorde.BORDER_ROJO,
      colorDeFondo: this.util.obtenerColorFondoAleatorio(),
    }
  }
}
