import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CodigosCatalogoEntidad } from 'src/app/nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { RutasLocales } from 'src/app/rutas-locales.enum';
import { ConfiguracionBuscador, ModoBusqueda } from '@shared/diseno/modelos/buscador.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { RutasBuscadorGeneral } from 'src/app/presentacion/buscador-general/rutas-buscador-general.enum';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss']
})
export class BuscadorComponent implements OnInit, AfterViewInit {
  @Input() configuracion: ConfiguracionBuscador

  // Utils
  public ModoBusquedaEnum = ModoBusqueda

  constructor(
    public internacionalizacionNegocio: InternacionalizacionNegocio,
    public estiloTexto: EstiloDelTextoServicio,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.inicializarPlaceholder()
    this.inicializarModoBusqueda()
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.focusEnInput()
    })
  }

  focusEnInput() {
    if (!this.configuracion.focusEnInput) {
      return
    }

    const elemento: HTMLInputElement = document.getElementById('inputBarraBusqueda') as HTMLInputElement
    if (elemento) {
      elemento.focus()
    }
  }

  inicializarPlaceholder() {
    this.configuracion.placeholderActivo = true
  }

  inicializarModoBusqueda() {
    if (!this.configuracion.modoBusqueda) {
      this.configuracion.modoBusqueda = ModoBusqueda.BUSQUEDA_COMPONENTE
    }
  }

  validarSiEstaBuscando() {
    if (this.configuracion.modoBusqueda !== ModoBusqueda.BUSQUEDA_COMPONENTE) {
      this.configuracion.placeholderActivo = (
        this.configuracion.valorBusqueda &&
        this.configuracion.valorBusqueda.length === 0
      )

      if (
        (
          typeof this.configuracion.valorBusqueda === 'string' &&
          this.configuracion.valorBusqueda.length === 0
        )
        || this.configuracion.valorBusqueda === null
      ) {
        this.configuracion.buscar(true)
      }
    }
  }

  eventoTap() {
    if (
      this.configuracion &&
      !this.configuracion.disable &&
      this.configuracion.modoBusqueda === ModoBusqueda.BUSQUEDA_COMPONENTE
    ) {
      this.irAComponenteDeBusquedaSegunEntidad()
    }
  }

  irAComponenteDeBusquedaSegunEntidad() {
    if (!this.configuracion.entidad) {
      return
    }

    const ruta = RutasLocales.MODULO_BUSCADOR_GENERAL.toString()
    let buscar = ''
    switch (this.configuracion.entidad) {
      case CodigosCatalogoEntidad.PERFIL:
        buscar = RutasBuscadorGeneral.BUSCAR_PERFILES.toString()
        break
      case CodigosCatalogoEntidad.PROYECTO:
        buscar = RutasBuscadorGeneral.BUSCAR_PROYECTOS.toString()
        break
      case CodigosCatalogoEntidad.NOTICIA:
        buscar = RutasBuscadorGeneral.BUSCAR_NOTICIAS.toString()
        break
      case CodigosCatalogoEntidad.CONTACTO:
        buscar = RutasBuscadorGeneral.BUSCAR_CONTACTOS.toString()
        break
      default:
        break;
    }

    if (buscar.length === 0) {
      return
    }

    this.router.navigateByUrl(ruta + '/' + buscar)
  }

  focus() {
    if (
      this.configuracion &&
      !this.configuracion.disable
    ) {

    }
  }

  buscar() {
    
    if (this.configuracion.buscar) {
      this.configuracion.buscar()
    }
  }
}

