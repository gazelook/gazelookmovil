import { AccionesItemCircularRectangular } from '@shared/diseno/enums/acciones-general.enum';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionModalOrigenFoto } from '@shared/diseno/modelos/modal-opciones-foto.interface';
import { OrigenFoto } from '@shared/diseno/enums/origen-foto.enum';
import { UsoItemCircular } from '@shared/diseno/enums/uso-item-cir-rec.enum';
import { ItemCircularCompartido } from '@shared/diseno/modelos/item-cir-rec.interface';
import { Component, OnInit, Renderer2, Input, ViewChild, ElementRef, Output, EventEmitter, OnChanges, AfterViewInit } from '@angular/core'
import { GeneradorId } from '@core/servicios/generales/generador-id.service'
import { ItemCircularRectangularMetodosCompartidos } from '@core/servicios/diseno/item-cir-rec.service'
import { InfoAccionCirRec } from '@shared/diseno/modelos/info-accion-cir-rec.interface'

@Component({
	selector: 'app-item-circulo',
	templateUrl: './item-circulo.component.html',
	styleUrls: ['./item-circulo.component.scss']
})
export class ItemCirculoComponent implements OnInit, AfterViewInit {

	@ViewChild('inputFile', { static: false }) inputFile: ElementRef
	@Input() configuracion: ItemCircularCompartido // Configuracion del item, para dibujar el item los valores de configuracion.eventoEnItemtido deben ser establecidos

	public infoAccion: InfoAccionCirRec // Informacion de que accion se va a ejecutar en el padre cuando se dispare un evento
	public itemMetodos: ItemCircularRectangularMetodosCompartidos // Contiene metodos generales para el item
	public confModalOrigenFoto: ConfiguracionModalOrigenFoto // Modal origen foto

	constructor(
		private generadorId: GeneradorId,
		public estiloDelTextoServicio: EstiloDelTextoServicio
	) {
		this.itemMetodos = new ItemCircularRectangularMetodosCompartidos()
	}

	ngOnInit(): void {
		
		this.configurarOrigenFoto()
	}

	ngAfterViewInit(): void {
		setTimeout(() => {
		})
	}

	siImagenEstaCargada(evt: any) {

		if (evt && evt.target) {
			const x = evt.srcElement.x
			const y = evt.srcElement.y
			if ((x === 0) && (y === 0)) {
				const width = evt.srcElement.width
				const height = evt.srcElement.height
				const portrait = height > width ? true : false
			}

			this.configuracion.mostrarLoader = false
		}
	}

	tieneOtraImeagen() {

		this.configuracion.mostrarLoader = false
	}

	configurarOrigenFoto() {
		this.confModalOrigenFoto = {
			mostrar: false,
			origenFoto: OrigenFoto.SIN_DEFINIR
		}
	}

	tamanoTextoBoton(){
	}

	eventoEnModalOrigenFoto(evento: number) {
		this.confModalOrigenFoto.mostrar = false
		if (evento === 0) {
			// Tomar Foto
			this.infoAccion = {
				accion: AccionesItemCircularRectangular.TOMAR_FOTO
			}
			this.configuracion.eventoEnItem(this.infoAccion)
			return
		}

		if (evento === 1) {
			// Subir foto
			const selector = document.getElementById("itemCirculoInputFile" + this.configuracion.idInterno)
			selector.click()
			return
		}
	}


	// Se ejecuta cuando se produce un click en el item
	tap() {
		try {
			// Si el item no tiene eventos de click
			if (!this.configuracion.activarClick || this.configuracion.mostrarLoader) {
				return
			}

			if (this.configuracion.usoDelItem === UsoItemCircular.CIRPERFILREGISTRO) {
				if (!this.configuracion.esVisitante) {
					this.infoAccion = {
						accion: AccionesItemCircularRectangular.ABRIR_ADMIN_ALBUM_PERFIL
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}

				return
			}

			// Cuando el item es usado en el perfil
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRPERFIL || this.configuracion.usoDelItem === UsoItemCircular.CIRCARITAPERFIL ) {
				// Si esta en modo visita, se expande la foto del item
				if (this.configuracion.esVisitante) {
					this.infoAccion = {
						accion: AccionesItemCircularRectangular.EXPANDIR_FOTO_DEL_ITEM,
						informacion: {
							id: this.configuracion.id,
							urlMedia: this.configuracion.urlMedia
						}
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}

				if (!this.configuracion.esVisitante) {
					this.infoAccion = {
						informacion: {
							id: this.configuracion.id,

						},
						accion: AccionesItemCircularRectangular.ACTUALIZAR_PERFIL
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}
				return
			}

			// Cuando el item es usado en el album
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRALBUM) {
				// Accion por defecto
				// Mostrar modal definir opcion origen foto
				this.confModalOrigenFoto.mostrar = true
				return
			}

			// Cuando el item es usado en el contacto
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRCONTACTO || this.configuracion.usoDelItem === UsoItemCircular.CIRCARITACONTACTODEFECTO) {
				// No se implementa aun la parte de contactos


				if (this.configuracion.abrirResumenPerfil) {
					this.infoAccion = {
						accion: AccionesItemCircularRectangular.ABRIR_MODAL_RESUMEN_PERFIL,
						informacion: {
							id: this.configuracion.id,
						}
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}

				this.infoAccion = {
					accion: AccionesItemCircularRectangular.VER_RESUMEN_CONTACTOS,
					informacion: {
						id: this.configuracion.id,
					}
				}
				this.configuracion.eventoEnItem(this.infoAccion)

				return
			}
		} catch (error) {

		}
	}

	// Se ejecuta cuando se produce un doble click en el item
	dobletap() {
		// CIRCONTACTO, CIRCARITACONTACTO, CIRPERFILCONTACTO

		try {
			// Si el item no tiene eventos
			if (!this.configuracion.activarDobleClick || this.configuracion.mostrarLoader) {
				return
			}

			if (this.configuracion.usoDelItem === UsoItemCircular.CIRCONTACTO) {
				if (this.configuracion.esVisitante) {
					// TODO determinar accion
					return
				}

				if (!this.configuracion.esVisitante) {
					this.infoAccion = {
						accion: AccionesItemCircularRectangular.CANCELAR_INVITACION,
							informacion: {
						    id: this.configuracion.id,
						}
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}
				return
			}

			if (this.configuracion.usoDelItem === UsoItemCircular.CIRCARITACONTACTODEFECTO) {
				if (this.configuracion.esVisitante) {
					// TODO determinar accion
					return
				}

				if (!this.configuracion.esVisitante) {
					this.infoAccion = {
						accion: AccionesItemCircularRectangular.CANCELAR_INVITACION,
							informacion: {
						    id: this.configuracion.id,
						}
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}
				return
			}

			// Cuando es usado en el perfil
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRPERFIL || this.configuracion.usoDelItem === UsoItemCircular.CIRCARITAPERFIL) {

				if (this.configuracion.esVisitante) {

					this.infoAccion = {
						informacion: {
							id: this.configuracion.id,
						},
						accion: AccionesItemCircularRectangular.VISITAR_ALBUM_GENERAL
					}
					this.configuracion.eventoEnItem(this.infoAccion)
					return
				}
			}

			// Cuando es usado en el album
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRALBUM) {
				if (this.configuracion.fotoPredeterminadaRamdon) {
					// El item predeterminado se obtiene de forma aleatoria
					return
				}
				// Accion por defecto
				// Cuando el item predeterminado se obtiene con doble click
				this.infoAccion = {
					accion: AccionesItemCircularRectangular.ESTABLECER_ITEM_PREDETERMINADO,
					informacion: {
						id: this.configuracion.id,
						urlMedia: this.configuracion.urlMedia
					}
				}
				this.configuracion.eventoEnItem(this.infoAccion)
				return
			}
		} catch (error) {

		}
	}

	// Se ejecuta cuando se produce un long click en el item
	press() {
		try {
			// Si el item no tiene eventos
			if (!this.configuracion.activarLongPress || this.configuracion.mostrarLoader) {
				return
			}

			// Cuando es usado en el perfil
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRPERFIL) {
				// No hay evento definido aun
				return
			}

			// Cuando es usado en el album
			if (this.configuracion.usoDelItem === UsoItemCircular.CIRALBUM) {
				// Borrar imagen en click largo
				this.infoAccion = {
					accion: AccionesItemCircularRectangular.BORRAR_ITEM,
					informacion: {
						id: this.configuracion.id
					}
				}
				// Accion por defecto
				this.configuracion.eventoEnItem(this.infoAccion)
				return
			}
		} catch (error) {

		}
	}

	// Se ejecuta cuando cambia el valor del input tipo file del item
	cambioEnInputTipoFile(file: File) {
		try {
			if (file) {
				this.infoAccion = {
					accion: AccionesItemCircularRectangular.SUBIR_ARCHIVO,
					informacion: {
						id: this.configuracion.id,
						archivo: file
					}
				}
				this.configuracion.eventoEnItem(this.infoAccion)
				this.inputFile.nativeElement.value = ""
			}
		} catch (error) {

		}
	}

	cerrarModalOrigenFoto(event: any) {
		if (event.target.className.indexOf('modalOrigenFoto') >= 0) {
			this.confModalOrigenFoto.mostrar = false
			this.confModalOrigenFoto.origenFoto = OrigenFoto.SIN_DEFINIR
		}
	}
}
