import { MetodosSessionStorageService } from '@core/util/metodos-session-storage.service';
import { RutasDemo } from 'src/app/presentacion/demo/rutas-demo.enum';
import { LineaCompartida } from '@shared/diseno/modelos/linea.interface'
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface'
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio'
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service'
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core'
import { ConfiguracionAppbarCompartida } from '@shared/diseno/modelos/appbar.interface'
import { UsoAppBar } from '@shared/diseno/enums/uso-appbar.enum'
import { ColorTextoBoton, TipoBoton, ButtonComponent } from '@shared/componentes/button/button.component'
import { TamanoDeTextoConInterlineado } from '@shared/diseno/enums/estilos-tamano-general.enum'
import { AnchoLineaItem } from '@shared/diseno/enums/ancho-linea-item.enum'
import { ColorFondoLinea } from '@shared/diseno/enums/estilos-colores-general'
import { Location } from '@angular/common'
import { Router } from '@angular/router'
import { RutasLocales } from 'src/app/rutas-locales.enum'
import { DialogoCompartido } from '@shared/diseno/modelos/dialogo.interface'
import { TipoDialogo } from '@shared/diseno/enums/tipo-dialogo.enum'
import { CuentaNegocio } from 'src/app/dominio/logica-negocio/cuenta.negocio'
import { BuscadorComponent } from '@shared/componentes/buscador/buscador.component'
import { EspesorLineaItem } from '@shared/diseno/enums/estilos-colores-general';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-appbar',
  templateUrl: './appbar.component.html',
  styleUrls: ['./appbar.component.scss']
})
export class AppbarComponent implements OnInit, OnChanges {
  @ViewChild('buscador', { static: false }) public buscador: BuscadorComponent
  @ViewChild('toast', { static: false }) toast: ToastComponent
  @Input() configuracion: ConfiguracionAppbarCompartida

  public usoAppBar = UsoAppBar
  public textoNombrePerfil: string
  public textoTituloPrincipal: string
  public textoSubtitulo: string
  public textoSubtituloDemo: string
  public textoSubtituloNormal: string
  public textoBack: string
  public textoHome: string
  public textoElegirPerfil: string

  public confBoton: BotonCompartido
  public confLinea: LineaCompartida

  public datosDialogoCerrarSession: DialogoCompartido
  public confToast: ConfiguracionToast

  constructor(
    private servicioIdiomas: InternacionalizacionNegocio,
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    private cuentaNegocio: CuentaNegocio,
    private _location: Location,
    private router: Router,
    private auth: AngularFireAuth,
    private metodosSessionStorageService: MetodosSessionStorageService
  ) {
    this.textoNombrePerfil = ''
    this.textoTituloPrincipal = ''
    this.textoSubtitulo = ''
    this.textoSubtituloDemo = ''
    this.textoBack = ''
    this.textoHome = ''
    this.textoElegirPerfil = ''
  }

  ngOnInit(): void {
    if (this.configuracion) {
      this.configurarToast()
      this.inicializarTextos()
      if (this.configuracion.usoAppBar === UsoAppBar.USO_DEMO_APPBAR) {
        this.configurarBotonPrincipal()
      }
      this.configurarLinea()
    }
    this.prepararDialogoCerrarSession()
  }

  ngOnChanges() {
    if (this.configuracion) {
      this.inicializarTextos()
      if (this.configuracion.usoAppBar === UsoAppBar.USO_DEMO_APPBAR) {
        this.configurarBotonPrincipal()
      }
      this.configurarLinea()
    }
  }

  // Inicializar textos
  async inicializarTextos() {
    if (this.configuracion.demoAppbar) {
      const conf = this.configuracion.demoAppbar
      this.textoNombrePerfil = await this.servicioIdiomas.obtenerTextoLlave(conf.nombrePerfil.llaveTexto ? conf.nombrePerfil.llaveTexto : 'undefined')
      this.textoSubtitulo = await this.servicioIdiomas.obtenerTextoLlave(conf.subtitulo?.llaveTexto ? conf.subtitulo.llaveTexto : 'undefined')
    }

    if (this.configuracion.searchBarAppBar) {
      const conf = this.configuracion.searchBarAppBar
      this.textoNombrePerfil = await this.servicioIdiomas.obtenerTextoLlave(conf.nombrePerfil.llaveTexto ? conf.nombrePerfil.llaveTexto : 'undefined')
      this.textoSubtitulo = await this.servicioIdiomas.obtenerTextoLlave(conf.subtitulo.llaveTexto ? conf.subtitulo.llaveTexto : 'undefined')
      this.textoBack = await this.servicioIdiomas.obtenerTextoLlave('m2v13texto1')
      this.textoHome = await this.servicioIdiomas.obtenerTextoLlave('m2v13texto2')
    }

    if (this.configuracion.gazeAppBar) {
      const conf = this.configuracion.gazeAppBar
      this.textoSubtitulo = await this.servicioIdiomas.obtenerTextoLlave(conf.subtituloNormal?.llaveTexto ? conf.subtituloNormal.llaveTexto : 'undefined')
      this.textoSubtituloDemo = await this.servicioIdiomas.obtenerTextoLlave(conf.subtituloDemo?.llaveTexto ? conf.subtituloDemo.llaveTexto : 'undefined')
      this.textoTituloPrincipal = await this.servicioIdiomas.obtenerTextoLlave(conf.tituloPrincipal.llaveTexto ? conf.tituloPrincipal.llaveTexto : 'undefined')
      this.textoElegirPerfil = await this.servicioIdiomas.obtenerTextoLlave(conf.textoElegirPerfil.llaveTexto ? conf.textoElegirPerfil.llaveTexto : 'undefined')
    }

    if (this.configuracion.tituloAppbar) {
      this.textoTituloPrincipal = await this.servicioIdiomas.obtenerTextoLlave(this.configuracion.tituloAppbar.tituloPrincipal.llaveTexto)
    }
  }

  // Configurar boton principal
  async configurarBotonPrincipal() {
    if (this.configuracion.demoAppbar.boton) {
      //Se puede recibir un boton custom desde fuera
    } else {
      this.confBoton = {
        text: 'm1v4texto1',
        tipoBoton: TipoBoton.TEXTO,
        tamanoTexto: TamanoDeTextoConInterlineado.L7_IGUAL,
        colorTexto: ColorTextoBoton.AMARRILLO,
        enProgreso: false,
        ejecutar: () => {
          this.router.navigateByUrl(RutasLocales.MENU_PERFILES)
        }
      }
    }
  }

  // Configurar linea verde
  configurarLinea() {
    this.confLinea = {
      ancho: AnchoLineaItem.ANCHO100,
      colorFondo: ColorFondoLinea.FONDOLINEAVERDE,
      espesor: EspesorLineaItem.ESPESOR071,
      forzarAlFinal: false
    }
  }

  // Devuelve las clases que definen los estilos del appbar
  obtenerClasesAppBar() {
    const clases = {}
    clases['appbar'] = true
    clases[this.configuracion.usoAppBar.toString()] = true
    clases['color-fondo-principal'] = true
    return clases
  }

  // Devuelve las clases para el color de fondo
  obtenerClasesCapaColorFondo(configuracion: any) {
    const clases = {}
    clases['colorFondo'] = true
    clases[configuracion.toString()] = true
    clases['con-color'] = (this.configuracion.conColorSecundario)
    return clases
  }

  mostrarOriginal() {

    if (this.configuracion.searchBarAppBar.idiomaOriginal.clickMostrarOriginal) {
      this.configuracion.searchBarAppBar.idiomaOriginal.clickMostrarOriginal()
    }
  }
  // Eventos de click
  clickBotonAtras() {
    if (this.configuracion.accionAtras) {
      this.configuracion.accionAtras();
    } else {
      this._location.back();
    }
  }
  clickBotonXRoja() {
    this.datosDialogoCerrarSession.mostrarDialogo = true;
  }
  clickBotonPrincipal() {

  }

  clickBotonCasaHome() {
    this.metodosSessionStorageService.eliminarSessionStorage()
    this.router.navigateByUrl(RutasLocales.MODULO_DEMO + '/' + RutasDemo.MENU_PRINCIPAL)
  }

  clickHome() {
    if (this.configuracion.eventoHome) {
      this.configuracion.eventoHome()
      return
    }

    this.metodosSessionStorageService.eliminarSessionStorage()
    this.router.navigateByUrl(RutasLocales.MENU_PRINCIPAL)
  }

  prepararDialogoCerrarSession() {
    let textoDescripcion = ''

    if (this.cuentaNegocio.sesionIniciada()) {
      textoDescripcion = 'm2v11texto27'
    } else {
      textoDescripcion = 'm1v3texto4'
    }

    this.datosDialogoCerrarSession = {
      completo: true,
      mostrarDialogo: false,
      tipo: TipoDialogo.CONFIRMACION,
      descripcion: textoDescripcion,
      listaAcciones: [
        ButtonComponent.crearBotonAfirmativo(() => this.cerrarSession()),
        ButtonComponent.crearBotonNegativo(() => { this.datosDialogoCerrarSession.mostrarDialogo = false })
      ]
    }
  }

  async cerrarSession() {
    try {
      this.datosDialogoCerrarSession.mostrarDialogo = false
      this.toast.abrirToast('', true)
      const usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
      const respuesta = await this.cuentaNegocio.cerrarSessionEnElApi(usuario).toPromise()

      if (respuesta !== 200 && respuesta !== 201) {
        throw new Error('')
      }

      await this.auth.signOut()

      this.cuentaNegocio.cerrarSession()
      this._location.replaceState('/')
      this.router.navigateByUrl(RutasLocales.BASE, { replaceUrl: true })
    } catch (error) {
      this.toast.abrirToast('text37')
    }
  }

  clickTituloPrincipal() {
    switch (this.configuracion.usoAppBar) {
      case UsoAppBar.USO_DEMO_APPBAR: break
      case UsoAppBar.USO_SEARCHBAR_APPBAR:
        break
      case UsoAppBar.USO_GAZE_BAR:
        if (this.configuracion.gazeAppBar.clickTituloPrincipal) {
          this.configuracion.gazeAppBar.clickTituloPrincipal()
        }
        break
      case UsoAppBar.SOLO_TITULO: break
      case UsoAppBar.USO_ELEGIR_PERFIL:
        if (this.configuracion.gazeAppBar.clickElegirOtroPerfil) {
          this.configuracion.gazeAppBar.clickTituloPrincipal()
        }
        break
      default: break
    }
  }

  clickTituloElegPer() {
    if (this.configuracion.gazeAppBar.clickElegirOtroPerfil) {
      this.configuracion.gazeAppBar.clickElegirOtroPerfil()
    }
  }

  configurarToast() {
    this.confToast = {
      mostrarToast: false,
      mostrarLoader: false,
      cerrarClickOutside: false,
      texto: '',
      intervalo: 5,
      bloquearPantalla: false,
    }
  }
}
