import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionResumenPerfil } from '@shared/diseno/modelos/resumen-perfil.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-resumen-perfil',
  templateUrl: './resumen-perfil.component.html',
  styleUrls: ['./resumen-perfil.component.scss']
})
export class ResumenPerfilComponent implements OnInit {
  @Input() configuracion: ConfiguracionResumenPerfil// Configuracion del item, para dibujar el item los valores de configuracion.eventoEnItemtido deben ser establecidos

  constructor(
    public estiloDelTextoServicio: EstiloDelTextoServicio,
    public estiloTexto: EstiloDelTextoServicio
  ) { }

  ngOnInit(): void { }

  obtenerContenidoDefault() {
    const clases = {}
    clases['estilos-defecto-resumen'] = true
    clases[this.configuracion?.alturaModal.toString()] = true
    clases['mostrar'] = this.configuracion?.mostrar

    return clases
  }

  reintentar() { }
}
