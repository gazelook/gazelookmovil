import { Component, Input, OnInit } from '@angular/core';
import { ConfiguracionImagenPantallaCompleta } from '@shared/diseno/modelos/imagen-pantalla-completa.interface';
@Component({
  selector: 'app-imagen-patanlla-completa',
  templateUrl: './imagen-patanlla-completa.component.html',
  styleUrls: ['./imagen-patanlla-completa.component.scss']
})
export class ImagenPatallaCompletaComponent implements OnInit {

  @Input() configuracion: ConfiguracionImagenPantallaCompleta

  constructor() { }

  ngOnInit(): void { }

  eventoTapBotonCerrar() {
    if (this.configuracion.eventoBotonCerrar) {
      this.configuracion.eventoBotonCerrar()
    }
  }
  eventoTapBotonDescargar() {
    if (this.configuracion.eventoBotonDescargar) {
      this.configuracion.eventoBotonDescargar()
    }
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.mostrarLoader = false
    }
  }
}
