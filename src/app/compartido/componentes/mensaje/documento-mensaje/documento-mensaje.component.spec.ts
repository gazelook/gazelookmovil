import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoMensajeComponent } from './documento-mensaje.component';

describe('DocumentoMensajeComponent', () => {
  let component: DocumentoMensajeComponent;
  let fixture: ComponentFixture<DocumentoMensajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoMensajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoMensajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
