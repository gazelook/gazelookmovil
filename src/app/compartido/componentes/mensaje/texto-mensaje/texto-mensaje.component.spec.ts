import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextoMensajeComponent } from './texto-mensaje.component';

describe('TextoMensajeComponent', () => {
  let component: TextoMensajeComponent;
  let fixture: ComponentFixture<TextoMensajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextoMensajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextoMensajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
