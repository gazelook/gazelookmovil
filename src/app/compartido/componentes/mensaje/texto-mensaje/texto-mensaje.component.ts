import { Component, Input, OnInit } from '@angular/core';
import { TextoMensajeCompartido } from '@shared/diseno/modelos/mensaje.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { TipoMensaje } from '@shared/diseno/enums/mensaje.enum';
import { MensajeAccion } from '@shared/diseno/modelos/mensaje.interface';

@Component({
  selector: 'app-texto-mensaje',
  templateUrl: './texto-mensaje.component.html',
  styleUrls: ['./texto-mensaje.component.scss']
})
export class TextoMensajeComponent implements OnInit {
  @Input() configuracion: TextoMensajeCompartido

  constructor(
    public estiloTexto: EstiloDelTextoServicio,
  ) { }

  ngOnInit(): void { }

  eventoTap(id: string) {
    let mensajeAccion: MensajeAccion = {
      id: id,
      tipoMensaje: TipoMensaje.TEXTO
    }

    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(mensajeAccion)
    }
  }
}
