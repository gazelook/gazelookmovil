import { Component, Input, OnInit } from '@angular/core';
import { AudioMensajeCompartido } from '@shared/diseno/modelos/mensaje.interface';
import { ColorDeFondo } from '@shared/diseno/enums/estilos-colores-general';
import { TipoMensaje } from '@shared/diseno/enums/mensaje.enum';
import { MediaEntityMapperService } from 'dominio/entidades/media.entity';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { DireccionDelReproductor } from '@shared/diseno/enums/audio-reproductor.enum';
import { ConfiguracionAudioReproductor } from '@shared/diseno/modelos/audio-reproductor.interface';
import { MensajeAccion } from '@shared/diseno/modelos/mensaje.interface';

@Component({
  selector: 'app-audio-mensaje',
  templateUrl: './audio-mensaje.component.html',
  styleUrls: ['./audio-mensaje.component.scss']
})
export class AudioMensajeComponent implements OnInit {
  @Input() configuracion: AudioMensajeCompartido
  public configuracionAudio: ConfiguracionAudioReproductor

  constructor(
    private generadorId: GeneradorId,
    private mediaEntityMapperService: MediaEntityMapperService
  ) { }

  ngOnInit(): void {
    this.configurarAudioReproductor()
  }

  configurarAudioReproductor() {
    let direccionRepro: DireccionDelReproductor

    if (!this.configuracion.enviado) {
      direccionRepro = DireccionDelReproductor.HACIA_LA_DERECHA
    } else {
      direccionRepro = DireccionDelReproductor.HACIA_LA_IZQUIERDA
    }

    this.configuracionAudio = {
      idInterno: 'audio_' + this.generadorId.generarIdConSemilla(),
      mostrarTitulo: false,
      colorDeFondo: ColorDeFondo.FONDO_TRANSPARENTE,
      direccion: direccionRepro,
      reproduciendo: false,
      mostrarLoader: true,
      media: this.mediaEntityMapperService.transform(this.configuracion.adjunto),
      eventoDobleTap: () => { },
      eventoPress: () => { },
    }
  }

  eventoTap(id: string) {

    let mensajeAccion: MensajeAccion = {
      id: id,
      tipoMensaje: TipoMensaje.AUDIO
    }

    if (this.configuracion.eventoTap) {
      this.configuracion.eventoTap(mensajeAccion)
    }
  }
}
