import { Component, Input, OnInit } from '@angular/core';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { ColorDeBorde, ColorDeFondo } from '@shared/diseno/enums/estilos-colores-general';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ConfiguracionMensajeFirebase } from '@shared/componentes/mensaje-gaze/mensaje-gaze.component';

@Component({
  selector: 'app-mensaje-noticia',
  templateUrl: './mensaje-noticia.component.html',
  styleUrls: ['./mensaje-noticia.component.scss']
})
export class MensajeNoticiaComponent implements OnInit {

  @Input() configuracion: ConfiguracionMensajeFirebase

  public confItemProyectosNoticias: CongifuracionItemProyectosNoticias

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId,
    private albumNegocio: AlbumNegocio,
  ) {

  }

  ngOnInit(): void {
    if (this.configuracion) {
      this.configurarItemNoticia(
        this.configuracion.mensaje.referenciaEntidadCompartida
      )
    }
  }

  configurarItemNoticia(
    noticia: NoticiaModel
  ) {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumPredeterminadoDeLista(
      noticia.adjuntos
    )

    this.confItemProyectosNoticias = {
      id: noticia.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(noticia.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: noticia.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECNOTICIA,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: false
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    }
  }

  obtenerDateDeTimeStamp(fecha: any) {
    if (typeof fecha !== 'number') {
      return new Date()
    }
    return new Date(fecha)
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.mostrarCargando = false
    }
  }
}
