import { MensajeModelFirebase } from 'dominio/modelo/entidades/mensaje.model';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-mensaje-gaze',
	templateUrl: './mensaje-gaze.component.html',
	styleUrls: ['./mensaje-gaze.component.scss']
})
export class MensajeGazeComponent implements OnInit {

	constructor() { }

	ngOnInit(): void {	}

}
export interface ConfiguracionMensajeFirebase {
	mensaje: MensajeModelFirebase,
	esPropietario: boolean,
	seleccionado?: boolean,
	mostrarCargando?: boolean,
	contenidoError?: string
	capaEncima?: boolean,
	leido?: boolean
}
