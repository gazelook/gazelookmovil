import { DiametroDelLoader } from '@shared/componentes/cargando/cargando.component';
import { ColorDeFondo } from '@shared/diseno/enums/estilo-colores.enum';

import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { ConfiguracionAudioReproductor } from '@shared/diseno/modelos/audio-reproductor.interface';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { ConfiguracionMensajeFirebase } from '@shared/componentes/mensaje-gaze/mensaje-gaze.component';
import { Component, Input, OnInit } from '@angular/core';
import { DireccionDelReproductor } from '@shared/diseno/enums/audio-reproductor.enum';

@Component({
	selector: 'app-mensaje-audio',
	templateUrl: './mensaje-audio.component.html',
	styleUrls: ['./mensaje-audio.component.scss']
})
export class MensajeAudioComponent implements OnInit {

	@Input() configuracion: ConfiguracionMensajeFirebase

	public confAudio: ConfiguracionAudioReproductor
	public DiametroDelLoaderEnum = DiametroDelLoader

	constructor(
		public estilosDelTextoServicio: EstiloDelTextoServicio,
		private generadorId: GeneradorId
	) { }

	ngOnInit(): void {
		if (
			this.configuracion &&
			this.configuracion.mensaje.adjuntos &&
			this.configuracion.mensaje.adjuntos.length > 0 &&
			!this.confAudio
		) {
			this.configurarAudioReproductor()
		}
	}

	configurarAudioReproductor() {
		this.confAudio = {
			colorDeFondo:
				(!this.configuracion.esPropietario)
					? ColorDeFondo.FONDO_MENSAJE_ENVIADO
					: ColorDeFondo.FONDO_MENSAJE_RECIBIDO,
			direccion:
				(this.configuracion.esPropietario)
					? DireccionDelReproductor.HACIA_LA_IZQUIERDA
					: DireccionDelReproductor.HACIA_LA_DERECHA,
			idInterno: this.generadorId.generarIdConSemilla(),
			media: this.configuracion.mensaje.adjuntos[0],
		}
	}

	obtenerDateDeTimeStamp(fecha: any) {
		return new Date(fecha)
	}

	obtenerConfiguracionAudio() {
		if (!this.confAudio) {
			this.configurarAudioReproductor()
		}

		return this.confAudio
	}

}
