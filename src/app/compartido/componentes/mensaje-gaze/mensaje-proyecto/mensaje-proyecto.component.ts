import { Component, Input, OnInit } from '@angular/core';
import { ColorDeBorde, ColorDeFondo } from '@shared/diseno/enums/estilos-colores-general';
import { UsoItemProyectoNoticia } from '@shared/diseno/enums/uso-item-proyecto-noticia.enum';
import { AlbumNegocio } from 'dominio/logica-negocio/album.negocio';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { EstiloDelTextoServicio } from '@core/servicios/diseno/estilo-del-texto.service';
import { GeneradorId } from '@core/servicios/generales/generador-id.service';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { BotonCompartido } from '@shared/diseno/modelos/boton.interface';
import { CongifuracionItemProyectosNoticias } from '@shared/diseno/modelos/item-proyectos-noticias.interface';
import { ConfiguracionMensajeFirebase } from '@shared/componentes/mensaje-gaze/mensaje-gaze.component';

@Component({
  selector: 'app-mensaje-proyecto',
  templateUrl: './mensaje-proyecto.component.html',
  styleUrls: ['./mensaje-proyecto.component.scss']
})
export class MensajeProyectoComponent implements OnInit {

  @Input() configuracion: ConfiguracionMensajeFirebase

  public confItemProyectosNoticias: CongifuracionItemProyectosNoticias
  public confBotonAceptar: BotonCompartido
  public confBotonDeclinar: BotonCompartido

  constructor(
    public estilosDelTextoServicio: EstiloDelTextoServicio,
    public generadorId: GeneradorId,
    private albumNegocio: AlbumNegocio,
  ) {

  }

  ngOnInit(): void {
    if (this.configuracion) {
      this.configurarItemProyecto(
        this.configuracion.mensaje.referenciaEntidadCompartida
      )
    }
  }

  configurarItemProyecto(
    proyecto: ProyectoModel
  ) {
    const album: AlbumModel = this.albumNegocio.obtenerAlbumDeListaSegunTipo(
      CodigosCatalogoTipoAlbum.GENERAL,
      proyecto.adjuntos
    )

    this.confItemProyectosNoticias = {
      id: proyecto.id,
      colorDeBorde: ColorDeBorde.BORDER_SEMI_AMARILLO,
      colorDeFondo: ColorDeFondo.FONDO_BLANCO,
      fecha: {
        mostrar: true,
        configuracion: {
          fecha: new Date(proyecto.fechaCreacion),
          formato: 'dd/MM/yyyy'
        }
      },
      etiqueta: {
        mostrar: false,
      },
      titulo: {
        mostrar: true,
        configuracion: {
          textoBoton1: proyecto.tituloCorto,
          colorBorde: ColorDeBorde.BORDER_TRANSPARENTE,
          colorDeFondo: ColorDeFondo.FONDO_AZUL_CON_OPACIDAD,
        }
      },
      urlMedia: (album && album.portada) ? album.portada.principal.url || album.portada.miniatura.url || '' : '',
      usoItem: UsoItemProyectoNoticia.RECPROYECTO,
      loader: (
        album &&
        album.portada &&
        (
          (album.portada.principal && album.portada.principal.url) ||
          (album.portada.miniatura && album.portada.miniatura.url)
        )
      ) ? true : false,
      eventoTap: {
        activo: false
      },
      eventoDobleTap: {
        activo: false
      },
      eventoPress: {
        activo: false
      }
    }
  }

  obtenerDateDeTimeStamp(fecha: any) {
    if (typeof fecha !== 'number') {
      return new Date()
    }

    return new Date(fecha)
  }

  siImagenEstaCargada(evt: any) {
    if (evt && evt.target) {
      const x = evt.srcElement.x
      const y = evt.srcElement.y
      if ((x === 0) && (y === 0)) {
        const width = evt.srcElement.width
        const height = evt.srcElement.height
        const portrait = height > width ? true : false
      }
      this.configuracion.mostrarCargando = false
    }
  }
}
