import { map } from 'rxjs/operators';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { CamaraService } from '@core/servicios/generales/camara.service';
import { NotificacionesDeUsuario } from '@core/servicios/generales/notificaciones/notificaciones-usuario.service';
import { VariablesGlobales } from '@core/servicios/generales/variables-globales.service';
import { ToastComponent } from '@shared/componentes/toast/toast.component';
import { ConfiguracionToast } from '@shared/diseno/modelos/toast.interface';
import { InternacionalizacionNegocio } from 'dominio/logica-negocio/internacionalizacion.negocio';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { fromEvent, merge, of, Subscription } from 'rxjs';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
    @ViewChild('toast', { static: false }) toast: ToastComponent
    public title = 'Gazelook'
    public urlGif: string
    public confToast: ConfiguracionToast



    public networkStatus: boolean;
    private networkStatus$: Subscription = Subscription.EMPTY;
    constructor(
        public variablesGlobales: VariablesGlobales,
        private internacionalizacionNegocio: InternacionalizacionNegocio,
        private notificacionesDeUsuario: NotificacionesDeUsuario,
        public camaraService: CamaraService,
        private mediaNegocio: MediaNegocio
    ) {
        this.urlGif = 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/gif-en.gif'
    }

    ngOnInit(): void {
        this.configurarImagenesPorDefecto()
        this.camaraService.reiniciarServicio()
        this.internacionalizacionNegocio.guardarIdiomaDefecto()
        this.configurarEscuchasDeEstadoDelInternet()
        this.obtenerMundoClases()
        this.escucharCambioDeIdiomaParaElGif()
        this.configurarImagenesPorDefecto()
        this.configurarPermisosParaNotificaciones()
        this.configurarToast()
        this.checkNetworkStatus();

        window.onbeforeunload = () => {
            this.notificacionesDeUsuario.desconectarDeEscuchaNotificaciones()
        }
    }

    ngOnDestroy(): void {
        this.notificacionesDeUsuario.desconectarDeEscuchaNotificaciones()
    }

    desactivarContextMenu() {
        window.oncontextmenu = function (event: any) {
            event.preventDefault()
            event.stopPropagation()
            return false
        }
    }


    configurarToast() {
        this.confToast = {
            mostrarToast: false,
            mostrarLoader: false,
            cerrarClickOutside: false,
            texto: '',
            intervalo: 5,
            bloquearPantalla: false,
        }
    }

    configurarEscuchasDeEstadoDelInternet() {
        window.addEventListener('offline', (e) => {

        })

        window.addEventListener('online', (e) => {

        })
    }

    escucharCambioDeIdiomaParaElGif() {
        this.variablesGlobales.$cambioIdiomaGif.subscribe(codigo => {
            this.urlGif = 'https://d3ubht94yroq8c.cloudfront.net/recursos-sistema/gif/gif-' + codigo + '.gif'
        })
    }

    obtenerMundoClases() {
        return {
            'mundo': true,
            'mostrar': this.variablesGlobales.mostrarMundo
        }
    }

    async configurarImagenesPorDefecto() {
        try {
            let archivos: ArchivoModel[] = this.mediaNegocio.obtenerListaArchivosDefaultDelLocal()
            if (!archivos || archivos === null || (archivos && archivos.length === 0)) {
                archivos = await this.mediaNegocio.obtenerListaArchivosDefaultDelApi().toPromise()
            }

            this.mediaNegocio.guardarListaArchivosDefaultEnLocal(archivos)
        } catch (error) {
            this.mediaNegocio.guardarListaArchivosDefaultEnLocal([])
        }
    }

    configurarPermisosParaNotificaciones() {
        if (!("Notification" in window)) {
            return
        }

        if (Notification.permission === "granted") {
            return
        }

        if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                if (permission === "granted") {
                    var notification = new Notification("Hi there!");
                }
            })
            return
        }
    }


    checkNetworkStatus(): void {
        this.networkStatus = navigator.onLine;
        this.networkStatus$ = merge(
            of(null),
            fromEvent(window, 'online'),
            fromEvent(window, 'offline')
        )
            .pipe(map(() => navigator.onLine))
            .subscribe(status => this.networkStatus = status);
    }
}

export interface DataCerrarSesion {
    idUsuario: string,
    idDispositivo: string,
}
