import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { AnunciosSistemaComponent } from './presentacion/anuncios-sistema/anuncios-sistema.component';
import { AnuncioComponent } from './presentacion/anuncio/anuncio.component';
import { FincaComponent } from './presentacion/finca/finca.component';
import { LandingComponent } from './presentacion/landing/landing.component';
import { NuestrasMetasComponent } from './presentacion/nuestras-metas/nuestras-metas.component';
import { MenuPublicarProyectoNoticiaComponent } from './presentacion/menu-publicar-proyecto-noticia/menu-publicar-proyecto-noticia.component';
import { MetodoPagoComponent } from './presentacion/metodo-pago/metodo-pago.component';
import { LoginComponent } from './presentacion/login/login.component';
import { RegistroComponent } from './presentacion/registro/registro.component';
import { MenuPerfilesComponent } from './presentacion/menu-perfiles/menu-perfiles.component';
import { BienvenidaComponent } from './presentacion/bienvenida/bienvenida.component';
import { MenuPrincipalComponent } from './presentacion/menu-principal/menu-principal.component';
import { MenuSeleccionPerfilesComponent } from './presentacion/menu-seleccion-perfiles/menu-seleccion-perfiles.component';
import { RestriccionRutas } from '@core/servicios/generales/canActivate/resticcionRutas.service';
import { RutasInicioSession } from '@core/servicios/generales/canActivate/rutas-inicio-session.service';
import { ContactoComponent } from './presentacion/contacto/contacto.component';
import { MiCuentaComponent } from './presentacion/mi-cuenta/mi-cuenta.component';

import { RutasLocales } from './rutas-locales.enum';

import { DocLegalesComponent } from './presentacion/doc-legales/doc-legales.component';
import { CuotaExtraComponent } from './presentacion/cuota-extra/cuota-extra.component';
import { ReembolsosPagosComponent } from './presentacion/reembolsos-pagos/reembolsos-pagos.component';
import { MetodosPagoComponent } from '@env/src/app/presentacion/metodos-pago/metodos-pago.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    canActivate: [RutasInicioSession],
  },
  {
    path: RutasLocales.LANDING.toString(),
    component: LandingComponent,
  },
  {
    path: RutasLocales.DOC_LEGALES.toString(),
    component: DocLegalesComponent,
  },
  {
    path: RutasLocales.PAGO_CUOTA_EXTRA.toString(),
    component: CuotaExtraComponent,
  },
  {
    path: RutasLocales.FINCA.toString(),
    component: FincaComponent,
  },
  {
    path: RutasLocales.ANUNCIO.toString(),
    component: AnuncioComponent,
  },
  {
    path: RutasLocales.MODULO_DEMO.toString(),
    loadChildren: () =>
      import('./presentacion/demo/demo.module').then((p) => p.DemoModule),
  },
  // {
  //   path: RutasLocales.METODO_PAGO.toString(),
  //   component: MetodoPagoComponent,
  // },
  {
    path: RutasLocales.METODO_PAGO.toString(),
    component: MetodosPagoComponent,
  },
  {
    path: RutasLocales.REGISTRAR_PERFIL.toString(),
    component: RegistroComponent,
  },
  {
    path: RutasLocales.CREAR_PERFIL.toString(),
    component: RegistroComponent,
  },
  {
    path: RutasLocales.ACTUALIZAR_PERFIL.toString(),
    component: RegistroComponent,
  },
  {
    path: RutasLocales.MENU_PERFILES.toString(),
    component: MenuPerfilesComponent,
  },
  {
    path: RutasLocales.BIENVENIDO.toString(),
    component: BienvenidaComponent,
    canActivate: [RutasInicioSession],
  },
  {
    path: RutasLocales.MENU_PRINCIPAL.toString(),
    component: MenuPrincipalComponent,
  },

  {
    path: RutasLocales.CONTACTO.toString(),
    component: ContactoComponent,
  },
  {
    path: RutasLocales.MODULO_PENSAMIENTO.toString(),
    loadChildren: () =>
      import('./presentacion/pensamiento/pensamiento.module').then(
        (p) => p.PensamientoModule
      ),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.GAZING.toString(),
    loadChildren: () =>
      import('./presentacion/gazing/gazing.module').then((p) => p.GazingModule),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MENU_SELECCION_PERFILES.toString(),
    component: MenuSeleccionPerfilesComponent,
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MODULO_PROYECTOS.toString(),
    loadChildren: () =>
      import('./presentacion/proyectos/proyectos.module').then(
        (p) => p.ProyectosModule
      ),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MODULO_NOTICIAS.toString(),
    loadChildren: () =>
      import('./presentacion/noticias/noticias.module').then(
        (p) => p.NoticiasModule
      ),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MODULO_PERFILES.toString(),
    loadChildren: () =>
      import('./presentacion/perfiles/perfiles.module').then(
        (p) => p.PerfilesModule
      ),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MODULO_ALBUM.toString(),
    loadChildren: () =>
      import('./presentacion/album/album.module').then((p) => p.AlbumModule),
  },
  {
    path: RutasLocales.MENU_PUBLICAR_PROYECTO_NOTICIA.toString(),
    component: MenuPublicarProyectoNoticiaComponent,
  },
  {
    path: RutasLocales.COMPRAS_INTERCAMBIOS.toString(),
    loadChildren: () =>
      import(
        './presentacion/compra-intercambio/compra-intercambio.module'
      ).then((p) => p.CompraIntercambioModule),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.FINANZAS.toString(),
    loadChildren: () =>
      import('./presentacion/finanzas/finanzas.module').then(
        (p) => p.FinanzasModule
      ),
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MI_CUENTA.toString(),
    component: MiCuentaComponent,
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.REEMBOLSOS_PAGOS.toString(),
    component: ReembolsosPagosComponent,
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.NUETRAS_METAS.toString(),
    component: NuestrasMetasComponent,
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.ANUNCIOS_SISTEMA.toString(),
    component: AnunciosSistemaComponent,
    canActivate: [RestriccionRutas],
  },
  {
    path: RutasLocales.MODULO_BUSCADOR_GENERAL.toString(),
    loadChildren: () =>
      import('./presentacion/buscador-general/buscador-general.module').then(
        (p) => p.BuscadorGeneralModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
  constructor(private router: Router) {
    this.router.errorHandler = (error: any) => {
      this.router.navigateByUrl(RutasLocales.MENU_SELECCION_PERFILES);
    };
  }
}
