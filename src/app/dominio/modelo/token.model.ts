export interface TokenModel {
    tokenAccess: string,
    tokenRefresh: string
}
