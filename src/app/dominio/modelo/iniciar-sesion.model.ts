import { TokenModel } from 'dominio/modelo/token.model';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
export interface IniciarSesionModel extends TokenModel {
    usuario: UsuarioModel
    //perfiles?: PerfilModel[],
}