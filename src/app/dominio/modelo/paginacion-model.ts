export interface PaginacionModel<T> {
    totalDatos?:number
    totalPaginas?:number
    proximaPagina?:boolean
    anteriorPagina?:boolean
    lista?:T[]
    paginaActual?: number,
    cargando?: boolean
}
