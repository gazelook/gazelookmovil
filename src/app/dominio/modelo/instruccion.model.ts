export interface InstruccionModel {
    codigo?: string,
    titulo?: string,
    instruccion1?: string
    instruccion2?: string
}
