import { CodigosCatalogoArchivosPorDefecto } from '@core/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { CodigosCatalogoTipoMedia } from '@core/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';

export interface SubirArchivoData {
    archivo?: any,
    formato?: string, 
    relacionAspecto?: string,
    descripcion?: string,
    catalogoMedia?: CodigosCatalogoTipoMedia,
    duracion?: string,
    enlace?: string,
    fileDefault?: boolean,
    catalogoArchivoDefault?: CodigosCatalogoArchivosPorDefecto,
    nombreDelArchivo?: string,
    estado?: string
} 