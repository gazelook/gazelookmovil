import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';

export interface PerfilResumenModel {
    _id?: string,
    nombreContacto?: string,
    nombreContactoTraducido?: string
    nombre?: string,
    estado?: CatalogoEstadoEntity,
}
