import {Injectable} from '@angular/core';
import {MapedorService} from '@core/base/mapeador.interface';
import {UsuarioEntity} from 'dominio/entidades/usuario.entity';
import {CatalogoIdiomaModel, IdiomaMapperService} from 'dominio/modelo/catalogos/catalogo-idioma.model';
import {CatalogoEstadoModel} from 'dominio/modelo/catalogos/catalogo-estado.model';
import {
  CatalogoMetodoPagoModel,
  CatalogoMetodoPagoModelMapperService,
  PagoFacturacionModel,
  PagoFacturacionModelMapperService
} from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import {CatalogoTipoMonedaModel} from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import {DireccionModel, DireccionModelMapperService} from 'dominio/modelo/entidades/direccion.model';
import {DispositivoModel, DispositivoModelMapperService} from 'dominio/modelo/entidades/dispositivo.model';
import {DocumentosUsuarioModel, DocumentosUsuarioModelMapperBusqueda} from 'dominio/modelo/entidades/documentos-usuario.model';
import {PerfilModel, PerfilModelMapperService} from 'dominio/modelo/entidades/perfil.model';
import {RolSistemaModel} from 'dominio/modelo/entidades/rol-sistema.model';
import {SuscripcionModel} from 'dominio/modelo/entidades/suscripcion.model';
import {TransaccionModel, TransaccionModelMapperService} from 'dominio/modelo/entidades/transaccion.model';

interface UsuarioCripto {
  cantidad?: number;
  nombreCripto?: string;
  simboloCripto?: string;
}

export interface UsuarioModel {
  id?: string;
  email?: string;
  nombre?: string;
  fechaNacimiento?: Date;
  contrasena?: string;
  idioma?: CatalogoIdiomaModel;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  emailVerificado?: boolean;
  aceptoTerminosCondiciones?: boolean;
  estado?: CatalogoEstadoModel;
  perfilGrupo?: boolean;
  menorEdad?: boolean;
  perfiles?: Array<PerfilModel>;
  emailResponsable?: string;
  nombreResponsable?: string;
  responsableVerificado?: boolean;
  transacciones?: Array<TransaccionModel>;
  suscripciones?: Array<SuscripcionModel>;
  dispositivos?: Array<DispositivoModel>;
  rolSistema?: Array<RolSistemaModel>;
  metodoPago?: CatalogoMetodoPagoModel;
  datosFacturacion?: PagoFacturacionModel;
  direccion?: DireccionModel;
  nuevaContrasena?: string;
  anteriorContrasena?: string;
  idDispositivo?: string;
  documentosUsuario?: Array<DocumentosUsuarioModel>;
  monedaRegistro?: CatalogoTipoMonedaModel;
  direccionDomiciliaria?: string;
  documentoIdentidad?: string;
  pagoNoConfirmado?: boolean;
  autorizacionCodePaymentez?: string;
  pagoCripto?: UsuarioCripto;
}


@Injectable({providedIn: 'root'})
export class UsuarioModelMapperService extends MapedorService<UsuarioModel, UsuarioEntity> {

  constructor(
    private perfilModelMapperService: PerfilModelMapperService,
    private metodoPagoMapper: CatalogoMetodoPagoModelMapperService,
    private pagoMapper: PagoFacturacionModelMapperService,
    private idiomaMapper: IdiomaMapperService,
    private direccionModelMapperService: DireccionModelMapperService,
    private transaccionModelMapperService: TransaccionModelMapperService,
    private dispositivoModelMapperService: DispositivoModelMapperService,
    private documentosUsuarioModelMapperBusqueda: DocumentosUsuarioModelMapperBusqueda
  ) {
    super();
  }

  protected map(model: UsuarioModel): UsuarioEntity {

    if (model) {
      const entity: UsuarioEntity = {};


      if (model.id) {
        entity._id = model.id;
      }

      if (model.email) {
        entity.email = model.email;
      }

      if (model.contrasena) {
        entity.contrasena = model.contrasena;
      }

      if (model.nuevaContrasena) {
        entity.nuevaContrasena = model.nuevaContrasena;
      }

      entity.aceptoTerminosCondiciones = (model.aceptoTerminosCondiciones) ? true : false;

      entity.menorEdad = (model.menorEdad) ? true : false;

      if (model.idioma) {
        entity.idioma = this.idiomaMapper.transform(model.idioma);
      }

      entity.perfilGrupo = (model.perfilGrupo) ? true : false;

      if (model.emailResponsable) {
        entity.emailResponsable = model.emailResponsable;
      }

      if (model.nombreResponsable) {
        entity.nombreResponsable = model.nombreResponsable;
      }

      if (model.perfiles) {
        entity.perfiles = this.perfilModelMapperService.transform(model.perfiles);
      }

      if (model.metodoPago) {
        entity.metodoPago = this.metodoPagoMapper.transform(model.metodoPago);
      }

      if (model.datosFacturacion) {
        entity.datosFacturacion = this.pagoMapper.transform(model.datosFacturacion);
      }

      if (model.direccion) {
        entity.direccion = this.direccionModelMapperService.transform(model.direccion);
      }

      if (model.transacciones) {
        entity.transacciones = this.transaccionModelMapperService.transform(model.transacciones);
      }

      if (model.fechaNacimiento) {
        entity.fechaNacimiento = model.fechaNacimiento;
      }

      if (model.dispositivos) {
        entity.dispositivos = this.dispositivoModelMapperService.transform(model.dispositivos);
      }


      if (model.documentosUsuario) {
        entity.documentosUsuario = this.documentosUsuarioModelMapperBusqueda.transform(model.documentosUsuario);
      }

      if (model.monedaRegistro) {
        entity.monedaRegistro = model.monedaRegistro;
      }
      if (model.direccionDomiciliaria) {
        entity.direccionDomiciliaria = model.direccionDomiciliaria;
      }

      if (model.documentoIdentidad) {
        entity.documentoIdentidad = model.documentoIdentidad;
      }

      return entity;
    }

    return null;
  }
}


@Injectable({providedIn: 'root'})
export class ActualizarDatosUsuarioModelMapperService extends MapedorService<UsuarioModel, UsuarioEntity> {
  constructor(
    private dispositivoModelMapperService: DispositivoModelMapperService
  ) {
    super();
  }

  protected map(model: UsuarioModel): UsuarioEntity {

    if (model) {
      const entity: UsuarioEntity = {};

      if (model.email) {
        entity.email = model.email;
      }

      if (model.anteriorContrasena) {
        entity.contrasena = model.anteriorContrasena;
      }

      if (model.nuevaContrasena) {
        entity.nuevaContrasena = model.nuevaContrasena;
      }

      if (model.fechaNacimiento) {
        entity.fechaNacimiento = model.fechaNacimiento;
      }

      if (model.nombre) {
        entity.nombre = model.nombre;
      }

      if (model.idDispositivo) {
        entity.idDispositivo = model.idDispositivo;
      }


      return entity;
    }

    return null;
  }

}







