import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoRolModel } from "dominio/modelo/catalogos/catalogo-rol.model";
import { RolEntidadModel } from "dominio/modelo/entidades/rol-entidad.model";

export interface RolSistemaModel {
  id?: string
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date
  fechaActualizacion?: Date
  rolesEspecificos?: RolEntidadModel
  rol?: CatalogoRolModel
  nombre?: string
}
