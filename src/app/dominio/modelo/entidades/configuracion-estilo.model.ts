import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoEntidadModel } from "dominio/modelo/catalogos/catalogo-entidad.model";
import { EstiloModel } from "dominio/modelo/entidades/estilo.model";
import { CatalogoConfiguracionModel } from "dominio/modelo/catalogos/catalogo-configuracion.model";
import { CatalogoPaticipanteConfiguracionModel } from "dominio/modelo/catalogos/catalogo-participante-configuracion.model";

export interface ConfiguracionEstiloModel {
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?: string
    entidad?: CatalogoEntidadModel
    silenciada?: boolean
    tonoNotificacion?: MediaModel
    estilos?: Array<EstiloModel>
    tipo?: CatalogoConfiguracionModel
    catalogoPaticipante?:CatalogoPaticipanteConfiguracionModel
}