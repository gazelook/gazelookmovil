import { DocumentosUsuarioEntity } from 'dominio/entidades/documentos-usuario.entity';
import { ArchivoModel, ArchivoModelMapperService } from 'dominio/modelo/entidades/archivo.model';
import { CatalogoDocumentosUsuarioModel, CatalogoDocumentosUsuarioModelMapperService } from 'dominio/modelo/catalogos/catalogo-documentos-usuario.model';
import { MapedorService } from '@core/base/mapeador.interface';

import { Injectable } from '@angular/core';


export interface DocumentosUsuarioModel {
    tipo?: CatalogoDocumentosUsuarioModel,
    archivo?: ArchivoModel
}



@Injectable({ providedIn: 'root' })
export class DocumentosUsuarioModelMapperBusqueda extends MapedorService<DocumentosUsuarioModel, DocumentosUsuarioEntity> {
   
    constructor(
        private catalogoDocumentosUsuarioModelMapperService: CatalogoDocumentosUsuarioModelMapperService,
        private archivoModelMapperService: ArchivoModelMapperService
    ){
        super()
    }
    protected map(model: DocumentosUsuarioModel): DocumentosUsuarioEntity {
        return {
            
            tipo: this.catalogoDocumentosUsuarioModelMapperService.transform(model.tipo),
            archivo: this.archivoModelMapperService.transform(model.archivo)
        };
    }
}