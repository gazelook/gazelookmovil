import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { AsociacionEntity } from 'dominio/entidades/asociacion.entity';
import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { PerfilEntity } from 'dominio/entidades/perfil.entity';
import { TelefonoEntity } from 'dominio/entidades/telefono.entity';
import { CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { ItemResultadoBusqueda } from 'dominio/modelo/item-resultado-busqueda';
import { CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { ParticipanteAsociacionEntity } from 'dominio/entidades/participante-asociacion.entity';
import { CatalogoTipoPerfilModel, CatalogoTipoPerfilModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { AlbumModel, AlbumModelMapperService } from 'dominio/modelo/entidades/album.model';
import { DireccionModel, DireccionModelMapperService } from 'dominio/modelo/entidades/direccion.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { TelefonoModel } from 'dominio/modelo/entidades/telefono.model';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';

export interface PerfilModel {
    _id?: string,
    nombreContacto?: string,
    nombreContactoTraducido?: string,
    nombre?: string,
    tipoPerfil?: CatalogoTipoPerfilModel,
    usuario?: UsuarioModel,
    album?: Array<AlbumModel>,
    estado?: CatalogoEstadoEntity,
    direcciones?: Array<DireccionModel>,
    telefonos?: Array<TelefonoModel>,
    asociaciones?: Array<AsociacionEntity>,
    proyectos?: Array<ProyectoModel>,
    noticias?: Array<NoticiaModel>,
    pensamientos?: Array<PensamientoModel>,
    participanteAsociacion?: ParticipanteAsociacionEntity,
} 

@Injectable({ providedIn: 'root' })
export class PerfilModelMapperService extends MapedorService<PerfilModel, PerfilEntity> {
    constructor
        (
            private estadoMapper: CatalogoEstadoModelMapperService,
            private albumMaper: AlbumModelMapperService,
            private direccionMapper: DireccionModelMapperService,
            private tipoPerfilMapper: CatalogoTipoPerfilModelMapperService
        ) {
        super();
    }

    protected map(model: PerfilModel): PerfilEntity {
        
        if (model) {
            const entity: PerfilEntity = {}

            if (model._id) {
                entity._id = model._id
            }

            if (model.nombreContacto) {
                entity.nombreContacto = model.nombreContacto
            }
            if (model.nombreContactoTraducido) {
                entity.nombreContactoTraducido = model.nombreContactoTraducido
            }

            if (model.nombre) {
                entity.nombre = model.nombre
            }

            if (model.album) {
                if(model.album.length>0) entity.album = this.albumMaper.transform(model.album)
            }

            if (model.estado) {
                entity.estado = this.estadoMapper.transform(model.estado)
            }

            if (model.direcciones) {
                if(model.direcciones.length > 0) entity.direcciones = this.direccionMapper.transform(model.direcciones)
            }

            if (model.tipoPerfil) {
                entity.tipoPerfil = this.tipoPerfilMapper.transform(model.tipoPerfil)
            }

            if (model.telefonos) {
                if(model.telefonos.length > 0) entity.telefonos = model.telefonos as TelefonoEntity[]
            }
            return entity
            
        }
        return null
    }

}

@Injectable({ providedIn: 'root' })
export class PerfilModelEstadoMapperService extends MapedorService<PerfilModel, PerfilEntity> {
    constructor
        (
            private estadoMapper: CatalogoEstadoModelMapperService,
        ) {
        super();
    }

    protected map(model: PerfilModel): PerfilEntity {
        if (model) {
            const entity: PerfilEntity = {}
            if (model._id) {
                entity._id = model._id
            }

            if (model.estado) {
                entity.estado = this.estadoMapper.transform(model.estado)
            }

            return entity
        }
        return null
    }
}

@Injectable({ providedIn: 'root' })
export class PerfilModelMapperResultadoBusqueda extends MapedorService<PerfilModel, ItemResultadoBusqueda> {
    protected map(entity: PerfilModel): ItemResultadoBusqueda {
        return {
            titulo: entity.nombreContacto,
            subtitulo: entity.nombre,
            tipo: CodigosCatalogoEntidad.PERFIL
        };
    }
}
