import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { AlbumEntity } from 'dominio/entidades/album.entity';
import { CatalogoAlbumModel, CatalogoAlbumModelMapperService } from "dominio/modelo/catalogos/catalogo-album.model";
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { MediaModel, MediaModelMapperService } from 'dominio/modelo/entidades/media.model';
export interface AlbumModel {
  _id?: string
  nombre?: string,
  tipo?: CatalogoAlbumModel,
  media?: Array<MediaModel>,
  portada?: MediaModel,
  predeterminado?: boolean,
  estado?: {
    codigo: CodigosCatalogoEstadoAlbum
  }
}

@Injectable({ providedIn: 'root' })
export class AlbumModelMapperService extends MapedorService<AlbumModel, AlbumEntity> {
  constructor
    (
      //private estadoMapper: EstadoModelMapperService,
      private mediaModelMapper: MediaModelMapperService,
      private catalogoAlbumModelMapper: CatalogoAlbumModelMapperService
    ) {
    super();
  }

  protected map(model: AlbumModel): AlbumEntity {
    if (model) {
      const entity: AlbumEntity = {}

      if (model._id) {
        entity._id = model._id
      }

      if (model.portada) {
        entity.portada = this.mediaModelMapper.transform(model.portada)
      }

      if (model.media) {
        entity.media = this.mediaModelMapper.transform(model.media)
      }

      if (model.tipo) {
        entity.tipo = this.catalogoAlbumModelMapper.transform(model.tipo)
      }

      if (model.predeterminado) {
        entity.predeterminado = model.predeterminado
      }

      return entity
    }
    return null;
  }
}
