import { DispositivoEntity } from 'dominio/entidades/dispositivo.entity';
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface DispositivoModel {
    id?: string
    estado?: CatalogoEstadoModel //CatalogoEstado
    fechaCreacion?: Date
    fechaActualizacion?: Date
    tokenAcceso?:string
    tokenNotificacion?:string
    nombre?:string
    tipo?:string
}


@Injectable({ providedIn: 'root' })
export class DispositivoModelMapperService extends MapedorService<DispositivoModel, DispositivoEntity> {

  constructor() {  super(); }

    protected map(model: DispositivoModel): DispositivoEntity {
        if (model) {
            const entity: DispositivoEntity = {};

            if (model.id) {
                entity._id = model.id
            }

            return entity
        }
        return null
    }

}
