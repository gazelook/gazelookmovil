import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoComentarioModel, CatalogoTipoComentarioModelMapperService } from "dominio/modelo/catalogos/catalogo-tipo-comentario.model";
import { ComentarioEntity, ComentarioFirebaseEntity, TraduccionComentarioEntity } from 'dominio/entidades/comentario.entity';
import { MediaModel, MediaModelMapperService } from 'dominio/modelo/entidades/media.model';
import { ParticipanteProyectoModel, ParticipanteProyectoModelMapperService } from "dominio/modelo/entidades/participante-proyecto.model";
import { PerfilModel, PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';
import { ProyectoModel, ProyectoModelMapperServiceParaComentarios } from 'dominio/modelo/entidades/proyecto.model';

export interface ComentarioModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaCreacionFirebase?: number,
  fechaActualizacion?: Date,
  coautor?: ParticipanteProyectoModel,
  adjuntos?: Array<MediaModel>,
  importante?: boolean,
  tipo?: CatalogoTipoComentarioModel,
  proyecto?: ProyectoModel,
  idPerfilRespuesta?: PerfilModel,
  traducciones?: Array<TraduccionComentarioEntity>,
}

export interface ComentarioFirebaseModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacionFirebase?: number,
  coautor?: ParticipanteProyectoModel,
  adjuntos?: Array<MediaModel>,
  importante?: boolean,
  tipo?: CatalogoTipoComentarioModel,
  proyecto?: ProyectoModel,
  idPerfilRespuesta?: PerfilModel,
  traducciones?: Array<TraduccionComentarioEntity>,
  idProyecto?: string
}

@Injectable({ providedIn: 'root' })
export class ComentarioModelMapperService extends MapedorService<ComentarioModel, ComentarioEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    private participanteProyectoModelMapperService: ParticipanteProyectoModelMapperService,
    private mediaModelMapperService: MediaModelMapperService,
    private catalogoTipoComentarioModelMapperService: CatalogoTipoComentarioModelMapperService,
    private proyectoModelMapperServiceParaComentarios: ProyectoModelMapperServiceParaComentarios,
    private perfilModelMapperService: PerfilModelMapperService,
  ) {
    super()
  }

  protected map(model: ComentarioModel): ComentarioEntity {

    if (model) {

      const entity: ComentarioEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }

      if (model.coautor) {
        entity.coautor = this.participanteProyectoModelMapperService.transform(model.coautor)
      }

      if (model.adjuntos) {
        entity.adjuntos = this.mediaModelMapperService.transform(model.adjuntos)
      }

      entity.importante = (model.importante)

      if (model.traducciones) {
        entity.traducciones = model.traducciones
      }

      if (model.tipo) {
        entity.tipo = this.catalogoTipoComentarioModelMapperService.transform(model.tipo)
      }

      if (model.proyecto) {
        entity.proyecto = this.proyectoModelMapperServiceParaComentarios.transform(model.proyecto)
      }

      if (model.idPerfilRespuesta) {
        entity.idPerfilRespuesta = this.perfilModelMapperService.transform(model.idPerfilRespuesta)
      }
      return entity
    }
    return null
  }
}

@Injectable({ providedIn: 'root' })
export class ComentarioFirebaseModelMapperService extends MapedorService<ComentarioFirebaseModel, ComentarioFirebaseEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    private participanteProyectoModelMapperService: ParticipanteProyectoModelMapperService,
    private mediaModelMapperService: MediaModelMapperService,
    private catalogoTipoComentarioModelMapperService: CatalogoTipoComentarioModelMapperService,
    private proyectoModelMapperServiceParaComentarios: ProyectoModelMapperServiceParaComentarios,
    private perfilModelMapperService: PerfilModelMapperService,
  ) {
    super()
  }

  protected map(model: ComentarioFirebaseModel): ComentarioFirebaseEntity {

    if (model) {

      const entity: ComentarioFirebaseEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.fechaCreacionFirebase) {
        entity.fechaCreacionFirebase = model.fechaCreacionFirebase
      }

      if (model.coautor) {
        entity.coautor = this.participanteProyectoModelMapperService.transform(model.coautor)
      }

      if (model.adjuntos) {
        entity.adjuntos = this.mediaModelMapperService.transform(model.adjuntos)
      }

      entity.importante = (model.importante)

      if (model.traducciones) {
        entity.traducciones = model.traducciones
      }

      if (model.tipo) {
        entity.tipo = this.catalogoTipoComentarioModelMapperService.transform(model.tipo)
      }

      if (model.proyecto) {
        entity.proyecto = this.proyectoModelMapperServiceParaComentarios.transform(model.proyecto)
      }

      if (model.idPerfilRespuesta) {
        entity.idPerfilRespuesta = this.perfilModelMapperService.transform(model.idPerfilRespuesta)
      }

      return entity
    }
    return null
  }
}
