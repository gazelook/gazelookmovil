import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ArchivoEntity } from 'dominio/entidades/archivo.entity';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { CatalogoTipoMediaModel, CatalogoTipoMediaModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-media.model';
import { CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';

export interface ArchivoModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  url?: string,
  tipo?: CatalogoTipoMediaModel,
  peso?: number,
  relacionAspecto?: number,
  path?: string,
  catalogoArchivoDefault?: string,
  fileDefault?: boolean,
  duracion?: string,
  filename?: string
}

@Injectable({ providedIn: 'root' })
export class ArchivoModelMapperService extends MapedorService<ArchivoModel, ArchivoEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    private catalogoTipoMediaModelMapperService: CatalogoTipoMediaModelMapperService
  ) {
    super()
  }

  protected map(model: ArchivoModel): ArchivoEntity {
    if (model) {

      const entity: ArchivoEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.url) {
        entity.url = model.url
      }

      if (model.tipo) {
        entity.tipo = this.catalogoTipoMediaModelMapperService.transform(model.tipo)
      }

      if (model.peso) {
        entity.peso = model.peso
      }

      if (model.relacionAspecto) {
        entity.relacionAspecto = model.relacionAspecto
      }

      if (model.path) {
        entity.path = model.path
      }

      if (model.catalogoArchivoDefault) {
        entity.catalogoArchivoDefault = model.catalogoArchivoDefault
      }

      if (model.duracion) {
        entity.duracion = model.duracion
      }

      if (model.fileDefault) {
        entity.fileDefault = model.fileDefault
      }

      return entity
    }

    return null
  }
}
