import {Injectable} from '@angular/core';
import {MapedorService} from '@core/base/mapeador.interface';
import {CodigosCatalogoEntidad} from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import {ProyectoEntity, TraduccionProyectoEntity} from 'dominio/entidades/proyecto.entity';
import {CatalogoEstadoModel, CatalogoEstadoModelMapperService} from 'dominio/modelo/catalogos/catalogo-estado.model';
import {CatalogoTipoMonedaModel, CatalogoTipoMonedaModelMapperService} from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import {CatalogoTipoProyectoModel, CatalogoTipoProyectoModelMapperService} from 'dominio/modelo/catalogos/catalogo-tipo-proyecto.model';
import {AlbumModel} from 'dominio/modelo/entidades/album.model';
import {ComentarioModel, ComentarioModelMapperService} from 'dominio/modelo/entidades/comentario.model';
import {EstrategiaModel} from 'dominio/modelo/entidades/estrategia.model';
import {MediaModel} from 'dominio/modelo/entidades/media.model';
import {ParticipanteProyectoModel} from 'dominio/modelo/entidades/participante-proyecto.model';
import {PerfilModel, PerfilModelEstadoMapperService} from 'dominio/modelo/entidades/perfil.model';
import {VotoProyectoModel} from 'dominio/modelo/entidades/voto-proyecto.model';
import {ItemResultadoBusqueda} from 'dominio/modelo/item-resultado-busqueda';
import {AlbumModelMapperService} from 'dominio/modelo/entidades/album.model';
import {DireccionModel, DireccionModelMapperService} from 'dominio/modelo/entidades/direccion.model';
import {MediaModelMapperService} from 'dominio/modelo/entidades/media.model';
import {CatalogoMetodoPagoModel, PagoFacturacionModel, PagoFacturacionModelDonacionProyectos} from 'dominio/modelo/catalogos';
import {TransaccionModel} from 'dominio/modelo/entidades/transaccion.model';


export interface DonacionProyectosModel {
  idUsuario?: string;
  idProyecto?: string;
  metodoPago?: CatalogoMetodoPagoModel;
  transacciones?: TransaccionModel[];
  datosFacturacion?: PagoFacturacionModelDonacionProyectos;
  monedaRegistro?: CatalogoTipoMonedaModel;
  email?: string;
  autorizacionCodePaymentez?: string;
}


export interface ProyectoModel {
  id?: string;
  estado?: CatalogoEstadoModel;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  totalVotos?: number;
  actualizado?: boolean;
  participantes?: Array<ParticipanteProyectoModel>;
  recomendadoAdmin?: boolean;
  valorEstimado?: number;
  tipo?: CatalogoTipoProyectoModel;
  perfil?: PerfilModel;
  adjuntos?: Array<AlbumModel>;
  direccion?: DireccionModel;
  votos?: Array<VotoProyectoModel>;
  estrategia?: EstrategiaModel;
  comentarios?: Array<ComentarioModel>;
  moneda?: CatalogoTipoMonedaModel;
  titulo?: string;
  tituloCorto?: string;
  descripcion?: string;
  original?: boolean;
  tituloOriginal?: string;
  tituloCortoOriginal?: string;
  descripcionOriginal?: string;
  tags?: Array<string>;
  medias?: Array<MediaModel>;
  voto?: boolean;
  transferenciaActiva?: string;
  montoEntregado?: number;
  montoFaltante?: number;
}

@Injectable({providedIn: 'root'})
export class ProyectoModelMapperResultadoBusqueda extends MapedorService<ProyectoModel, ItemResultadoBusqueda> {
  protected map(entity: ProyectoModel): ItemResultadoBusqueda {
    return {
      titulo: entity.tituloCorto,
      subtitulo: entity.titulo,
      tipo: CodigosCatalogoEntidad.PROYECTO
    };
  }
}

@Injectable({providedIn: 'root'})
export class ProyectoModelMapperService extends MapedorService<ProyectoModel, ProyectoEntity> {

  constructor(
    private estadoMapperService: CatalogoEstadoModelMapperService,
    private catalogoTipoProyectoModelMapperService: CatalogoTipoProyectoModelMapperService,
    private perfilModelEstadoMapperService: PerfilModelEstadoMapperService,
    private albumModelMapperService: AlbumModelMapperService,
    private direccionModelMapperService: DireccionModelMapperService,
    private catalogoTipoMonedaModelMapperService: CatalogoTipoMonedaModelMapperService,
    private mediaModelMapperService: MediaModelMapperService,
    private comentarioModelMapperService: ComentarioModelMapperService
  ) {
    super();
  }

  protected map(model: ProyectoModel): ProyectoEntity {
    const entity: ProyectoEntity = {};
    if (model.id) {
      entity._id = model.id;
    }

    if (model.estado) {
      entity.estado = this.estadoMapperService.transform(model.estado);
    }

    if (model.fechaCreacion) {
      entity.fechaCreacion = model.fechaCreacion;
    }

    if (model.fechaActualizacion) {
      entity.fechaActualizacion = model.fechaActualizacion;
    }

    if (model.totalVotos) {
      entity.totalVotos = model.totalVotos;
    }

    entity.actualizado = model.actualizado;

    if (model.participantes) {
      // paticipantes?: Array<ParticipanteProyectoModel>,
    }

    entity.recomendadoAdmin = model.recomendadoAdmin;

    if (model.valorEstimado) {
      entity.valorEstimado = model.valorEstimado;
    }

    if (model.tipo) {
      entity.tipo = this.catalogoTipoProyectoModelMapperService.transform(model.tipo);
    }

    if (model.perfil) {
      entity.perfil = this.perfilModelEstadoMapperService.transform(model.perfil);
    }

    if (model.adjuntos) {
      entity.adjuntos = this.albumModelMapperService.transform(model.adjuntos);
    }

    if (model.direccion) {
      entity.direccion = this.direccionModelMapperService.transform(model.direccion);
    }

    if (model.votos) {
      // votos?: Array<VotoProyectoModel>,
    }

    if (model.estrategia) {
      // estrategia?: EstrategiaModel,
    }

    if (model.comentarios) {
      entity.comentarios = this.comentarioModelMapperService.transform(model.comentarios);
    }

    if (model.moneda) {
      entity.moneda = this.catalogoTipoMonedaModelMapperService.transform(model.moneda);
    }

    entity.traducciones = [];
    const traduccion: TraduccionProyectoEntity = {};
    if (model.titulo) {
      traduccion.titulo = model.titulo;
    }
    if (model.tituloCorto) {
      traduccion.tituloCorto = model.tituloCorto;
    }

    if (model.descripcion) {
      traduccion.descripcion = model.descripcion;
    }
    if (traduccion.titulo || traduccion.tituloCorto || traduccion.descripcion) {
      entity.traducciones.push(traduccion);
    }
    if (model.montoEntregado) {
      entity.montoEntregado = model.montoEntregado;
    }
    if (model.montoFaltante) {
      entity.montoFaltante = model.montoFaltante;
    }

    if (model.medias) {
      entity.medias = this.mediaModelMapperService.transform(model.medias);
    }


    entity.voto = model.voto;

    if (model.transferenciaActiva) {
      entity.transferenciaActiva = model.transferenciaActiva;
    }

    return entity;
  }

}

@Injectable({providedIn: 'root'})
export class ProyectoModelMapperServiceParaComentarios extends MapedorService<ProyectoModel, ProyectoEntity> {

  constructor() {
    super();
  }

  protected map(model: ProyectoModel): ProyectoEntity {
    if (model) {
      const entity: ProyectoEntity = {};

      if (model.id) {
        entity._id = model.id;
      }

      return entity;
    }

    return null;
  }
}
