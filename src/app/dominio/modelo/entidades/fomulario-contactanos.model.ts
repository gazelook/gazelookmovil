export interface FormularioContactanos {
    nombre?: string,
    email?:string,
    pais?:string, 
    direccion?:string,
    tema?: string,
    mensaje?: string
}