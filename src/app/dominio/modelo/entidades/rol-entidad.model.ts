import { CatalogoAccionModel } from "dominio/modelo/catalogos/catalogo-accion.model";
import { CatalogoEntidadModel } from "dominio/modelo/catalogos/catalogo-entidad.model";
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoRolModel } from 'dominio/modelo/catalogos/catalogo-rol.model';

export interface RolEntidadModel {
  id?: string
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date
  fechaActualizacion?: Date
  acciones?: Array<CatalogoAccionModel>
  entidad?: CatalogoEntidadModel
  rol?: CatalogoRolModel
  nombre?: string
}



