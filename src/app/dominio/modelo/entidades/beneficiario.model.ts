import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { CatalogoTipoBeneficiarioModel } from "dominio/modelo/catalogos/catalogo-tipo-beneficiario.model";
import { ProyectoModel } from "dominio/modelo/entidades/proyecto.model";

export interface BeneficiarioModel{
    id?:string,
    fechaCreacion?:Date,
    fechaActualizacion?:Date,
    tipo?:CatalogoTipoBeneficiarioModel,
    usuario?:UsuarioModel,
    proyecto?:ProyectoModel 
}