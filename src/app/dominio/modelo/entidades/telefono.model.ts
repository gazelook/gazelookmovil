import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { CatalogoPaisModel } from 'dominio/modelo/catalogos/catalogo-pais.model';

export interface TelefonoModel {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  numero?: string,
  pais?: CatalogoPaisModel
}
