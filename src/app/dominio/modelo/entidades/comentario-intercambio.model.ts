import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ComentarioIntercambioEntity, ComentarioIntercambioFirebaseEntity } from 'dominio/entidades/comentario-intercambio.entity';
import { TraduccionComentarioEntity } from 'dominio/entidades/comentario.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoComentarioModel, CatalogoTipoComentarioModelMapperService } from "dominio/modelo/catalogos/catalogo-tipo-comentario.model";
import { IntercambioModel, IntercambioModelMapperServiceParaComentarios } from 'dominio/modelo/entidades/intercambio.model';
import { MediaModel, MediaModelMapperService } from 'dominio/modelo/entidades/media.model';
import { ParticipanteIntercambioModel, ParticipanteIntercambioModelMapperService } from 'dominio/modelo/entidades/participante-intercambio.model';
import { PerfilModel, PerfilModelMapperService } from 'dominio/modelo/entidades/perfil.model';

export interface ComentarioIntercambioModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacion?: Date,
  fechaCreacionFirebase?: number,
  fechaActualizacion?: Date,
  coautor?: ParticipanteIntercambioModel,
  adjuntos?: Array<MediaModel>,
  importante?: boolean,
  tipo?: CatalogoTipoComentarioModel,
  intercambio?: IntercambioModel,
  idPerfilRespuesta?: PerfilModel,
  traducciones?: Array<TraduccionComentarioEntity>,
}

export interface ComentarioIntercambioFirebaseModel {
  id?: string,
  estado?: CatalogoEstadoModel,
  fechaCreacionFirebase?: number,
  coautor?: ParticipanteIntercambioModel,
  adjuntos?: Array<MediaModel>,
  importante?: boolean,
  tipo?: CatalogoTipoComentarioModel,
  intercambio?: IntercambioModel,
  idPerfilRespuesta?: PerfilModel,
  traducciones?: Array<TraduccionComentarioEntity>,
}

@Injectable({ providedIn: 'root' })
export class ComentarioIntercambioModelMapperService extends MapedorService<ComentarioIntercambioModel, ComentarioIntercambioEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    private participanteIntercambioModelMapperService: ParticipanteIntercambioModelMapperService,
    private mediaModelMapperService: MediaModelMapperService,
    private catalogoTipoComentarioModelMapperService: CatalogoTipoComentarioModelMapperService,
    private intercambioModelMapperServiceParaComentarios: IntercambioModelMapperServiceParaComentarios,
    private perfilModelMapperService: PerfilModelMapperService,
  ) {
    super()
  }

  protected map(model: ComentarioIntercambioModel): ComentarioIntercambioEntity {

    if (model) {

      const entity: ComentarioIntercambioEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }

      if (model.coautor) {
        entity.coautor = this.participanteIntercambioModelMapperService.transform(model.coautor)
      }

      if (model.adjuntos) {
        entity.adjuntos = this.mediaModelMapperService.transform(model.adjuntos)
      }

      entity.importante = (model.importante)

      if (model.traducciones) {
        entity.traducciones = model.traducciones
      }

      if (model.tipo) {
        entity.tipo = this.catalogoTipoComentarioModelMapperService.transform(model.tipo)
      }

      if (model.intercambio) {
        entity.intercambio = this.intercambioModelMapperServiceParaComentarios.transform(model.intercambio)
      }

      if (model.idPerfilRespuesta) {
        entity.idPerfilRespuesta = this.perfilModelMapperService.transform(model.idPerfilRespuesta)
      }


      return entity
    }

    return null
  }
}

@Injectable({ providedIn: 'root' })
export class ComentarioIntercambioFirebaseModelMapperService extends MapedorService<ComentarioIntercambioFirebaseModel, ComentarioIntercambioFirebaseEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    private participanteIntercambioModelMapperService: ParticipanteIntercambioModelMapperService,
    private mediaModelMapperService: MediaModelMapperService,
    private catalogoTipoComentarioModelMapperService: CatalogoTipoComentarioModelMapperService,
    private intercambioModelMapperServiceParaComentarios: IntercambioModelMapperServiceParaComentarios,
    private perfilModelMapperService: PerfilModelMapperService,
  ) {
    super()
  }

  protected map(model: ComentarioIntercambioFirebaseModel): ComentarioIntercambioFirebaseEntity {

    if (model) {

      const entity: ComentarioIntercambioFirebaseEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.fechaCreacionFirebase) {
        entity.fechaCreacionFirebase = model.fechaCreacionFirebase
      }

      if (model.coautor) {
        entity.coautor = this.participanteIntercambioModelMapperService.transform(model.coautor)
      }

      if (model.adjuntos) {
        entity.adjuntos = this.mediaModelMapperService.transform(model.adjuntos)
      }

      entity.importante = (model.importante)

      if (model.traducciones) {
        entity.traducciones = model.traducciones
      }

      if (model.tipo) {
        entity.tipo = this.catalogoTipoComentarioModelMapperService.transform(model.tipo)
      }

      if (model.intercambio) {
        entity.intercambio = this.intercambioModelMapperServiceParaComentarios.transform(model.intercambio)
      }

      if (model.idPerfilRespuesta) {
        entity.idPerfilRespuesta = this.perfilModelMapperService.transform(model.idPerfilRespuesta)
      }

      return entity
    }

    return null
  }
}
