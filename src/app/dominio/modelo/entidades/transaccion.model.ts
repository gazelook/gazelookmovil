import { CatalogoOrigenModelMapperService } from 'dominio/modelo/catalogos/catalogo-origen.model';
import { CatalogoTipoMonedaModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { MapedorService } from '@core/base/mapeador.interface';
import { TransaccionEntity } from 'dominio/entidades/transaccion.entity';
import { Injectable } from '@angular/core';
import { CatalogoMetodoPagoModel } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoOrigenModel } from "dominio/modelo/catalogos/catalogo-origen.model";
import { CatalogoTipoMonedaModel } from "dominio/modelo/catalogos/catalogo-tipo-moneda.model";
import { BalanceModel } from "dominio/modelo/entidades/balance.model";
import { BeneficiarioModel } from "dominio/modelo/entidades/beneficiario.model";


export interface TransaccionModel {
    id?: string,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    estado?: CatalogoEstadoModel,
    monto?: number,
    moneda?: CatalogoTipoMonedaModel,
    descripcion?: string,
    origen?: CatalogoOrigenModel,
    destino?: CatalogoOrigenModel,
    balance?: Array<BalanceModel>,
    beneficiario?: BeneficiarioModel,
    metodoPago?: CatalogoMetodoPagoModel
}

@Injectable({ providedIn: 'root' })
export class TransaccionModelMapperService extends MapedorService<TransaccionModel, TransaccionEntity> {

    constructor(
        private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
        private catalogoTipoMonedaModelMapperService: CatalogoTipoMonedaModelMapperService,
        private catalogoOrigenModelMapperService: CatalogoOrigenModelMapperService,
    ) {
        super()
    }

    protected map(model: TransaccionModel): TransaccionEntity {

        if (model) {

            const entity: TransaccionEntity = {}

            if (model.id) {
                entity._id = model.id
            }

            if (model.fechaCreacion) {
                entity.fechaCreacion = model.fechaCreacion
            }

            if (model.fechaActualizacion) {
                entity.fechaActualizacion = model.fechaActualizacion
            }

            if (model.estado) {
                entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
            }

            if (model.monto) {
                entity.monto = model.monto
            }

            if (model.moneda) {
                entity.moneda = this.catalogoTipoMonedaModelMapperService.transform(model.moneda)
            }

            if (model.descripcion) {
                entity.descripcion = model.descripcion
            }

            if (model.origen) {
                entity.origen = this.catalogoOrigenModelMapperService.transform(model.origen)
            }
            if (model.destino) {
                entity.destino = this.catalogoOrigenModelMapperService.transform(model.destino)
            }

            if (model.balance) {
            }

            if (model.beneficiario) {
            }

            if (model.metodoPago) {
            }

            return entity
        }

        return null
    }
}
