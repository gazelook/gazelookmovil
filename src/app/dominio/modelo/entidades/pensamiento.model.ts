import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface PensamientoModel {
    id?:string,
    perfil?:PerfilModel,
    texto?:string,
    estado?:CatalogoEstadoModel,
    fechaActualizacion?:Date,
    publico?:boolean
}
