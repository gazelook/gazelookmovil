import { TipoMenu } from '@shared/componentes/item-menu/item-menu.component';

export interface ItemMenuModel {
    id: any,
    titulo: string[] | ItemAccion[],
    subTitulo?: string[],
    action?: Function,
    ruta?: any, // Lo puse para poder redireccionar por el momento a la vista de proyectos
    tipo?: TipoMenu,
    mostrarCorazon?: boolean
}

export interface ItemSubMenu {
  id: any,
  titulo: string,
  menusInternos?: ItemMenuModel[],
  mostrarDescripcion: boolean,
}

export interface ItemAccion {
  nombre: string,
  accion: Function,
  codigo: "g" | "m" | "p"
}

