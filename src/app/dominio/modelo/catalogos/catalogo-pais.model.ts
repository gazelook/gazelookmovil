import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoPaisEntity } from 'dominio/entidades/catalogos/catalogo-pais.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';

export interface CatalogoPaisModel {
  id?: string
  codigo?: string
  estado?: CatalogoEstadoModel
  nombre?: string
  codigoTelefono?: any
  codigoNombre?: string,
  latitud?: number
  longitud?: number
}

@Injectable({ providedIn: 'root' })
export class CatalogoPaisModelMapperService extends MapedorService<CatalogoPaisModel, CatalogoPaisEntity> {

  constructor(
    private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService
  ) {
    super()
  }

  protected map(model: CatalogoPaisModel): CatalogoPaisEntity {

    if (model) {
      const entity: CatalogoPaisEntity = {}

      if (model.codigo) {
        entity.codigo = model.codigo
      }

      if (model.codigoNombre) {
        entity.codigoNombre = model.codigoNombre
      }

      if (model.codigoTelefono) {
        entity.codigoTelefono = model.codigoTelefono
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
      }

      if (model.latitud) {
        entity.latitud = model.latitud
      }
      if (model.longitud) {
        entity.longitud - model.longitud
      }
      return entity

    }
    return null
  }
}
