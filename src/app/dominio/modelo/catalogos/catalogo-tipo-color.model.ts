import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface CatalogoTipoColorModel {
    id?:string,
    codigo?:string,
    estado?:CatalogoEstadoModel, // CatalogoEstado
    nombre?:string,
    formula?:string,
    fechaCreacion?:Date,
    fechaActualizacion?:Date
}
