import { CatalogoMensajeEntity } from 'dominio/entidades/catalogos/catalogo-mensaje.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
export interface CatalogoMensajeModel{
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?:string
    nombre?:string
}

@Injectable({ providedIn: 'root' })
export class CatalogoMensajeModelMapperService extends MapedorService<CatalogoMensajeModel, CatalogoMensajeEntity> {

    constructor(
        private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService
    ) {
        super()
    }

    protected map(model: CatalogoMensajeModel): CatalogoMensajeEntity {

        if (model) {
            const entity: CatalogoMensajeEntity = {}

            if (model.codigo) {
                entity.codigo = model.codigo
            }

            if (model.estado) {
                entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
            }

            if (model.nombre) {
                entity.nombre = model.nombre
            }

            return entity

        }

        return null
    }
}
