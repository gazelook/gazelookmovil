import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';

export interface CatalogoIdiomaModel {
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?: string
    nombre?: string
    codNombre?: string,
    idiomaSistema?: boolean,
}

@Injectable({ providedIn: 'root' })
export class IdiomaMapperService extends MapedorService<CatalogoIdiomaModel, CatalogoIdiomaEntity> {

    protected map(model: CatalogoIdiomaModel): CatalogoIdiomaEntity {
        if (model) {
            return {
                codigo: model.codigo,
            };
        }
        return null;
    }

}
