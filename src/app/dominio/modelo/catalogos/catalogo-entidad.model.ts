import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { CatalogoEntidadEntity } from 'dominio/entidades/catalogos/catalogo-entidad.entity';
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
export interface CatalogoEntidadModel {
    id?: string,
    estado?: CatalogoEstadoModel, // CatalogoEstado
    codigo?: string,
    nombre?: string,
    fechaCreacion?: Date,
    fechaActualizacion?: Date
}
@Injectable({ providedIn: 'root' })
export class CatalogoEntidadModelMapperService extends MapedorService<CatalogoEntidadModel, CatalogoEntidadEntity> {
    constructor(
        private catalogoEstadoModelMapperService: CatalogoEstadoModelMapperService,
    ) {
        super()
    }
    protected map(model: CatalogoEntidadModel): CatalogoEntidadEntity {
        if (model) {
            const entity: CatalogoEntidadEntity = {}

            if (model.id) {
                entity._id = model.id
            }

            if (model.codigo) {
                entity.codigo = model.codigo
            }

            if (model.nombre) {
                entity.nombre = model.nombre
            }

            if (model.estado) {
                entity.estado = this.catalogoEstadoModelMapperService.transform(model.estado)
            }
            return entity
        }
        return null;
    }
}
