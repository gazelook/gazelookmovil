import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoDocumentosUsuarioEntity } from "dominio/entidades/catalogos/catalogo-documentos-usuario.entity";

export interface CatalogoDocumentosUsuarioModel {
    id?: string
    codigo?: string
}


@Injectable({ providedIn: 'root' })
export class CatalogoDocumentosUsuarioModelMapperService extends MapedorService<CatalogoDocumentosUsuarioModel, CatalogoDocumentosUsuarioEntity> {

    protected map(model: CatalogoDocumentosUsuarioModel): CatalogoDocumentosUsuarioEntity {
        if (model) {
            return {
                codigo: model.codigo,
            };
        }
        return null;
    }

}
