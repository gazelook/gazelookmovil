import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoEstiloAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-estilo-anuncio-sistema.entity';

export interface CatalogoTipoEstiloAnuncioSistemaModel {
    id?: string,
    codigo?: string,
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoEstiloAnuncioSistemaModelMapperService extends MapedorService<CatalogoTipoEstiloAnuncioSistemaModel, CatalogoTipoEstiloAnuncioSistemaEntity> {

    protected map(model: CatalogoTipoEstiloAnuncioSistemaModel): CatalogoTipoEstiloAnuncioSistemaEntity {
        return model;
    }
}