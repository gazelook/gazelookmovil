import { CatalogoPaisModel, CatalogoPaisModelMapperService } from 'dominio/modelo/catalogos/catalogo-pais.model'
import { Injectable } from '@angular/core'
import { MapedorService } from '@core/base/mapeador.interface'
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface'
import { CatalogoLocalidadEntity } from 'dominio/entidades/catalogos/catalogo-localidad.entity';

export interface CatalogoLocalidadModel {
    id?: string
    codigo?: string
    nombre?: string
    codigoPostal?: string
    pais?: CatalogoPaisModel,
}

@Injectable({ providedIn: 'root' })
export class CatalogoLocalidadMapperAItemSelectorService extends MapedorService<CatalogoLocalidadModel, ItemSelector> {

    protected map(entity: CatalogoLocalidadModel): ItemSelector {
        return {
            codigo: entity.codigo,
            nombre: entity.nombre,
            auxiliar: entity.codigoPostal
        };
    }

}

@Injectable({ providedIn: 'root' })
export class CatalogoLocalidadMapperService extends MapedorService<CatalogoLocalidadModel, CatalogoLocalidadEntity> {

    constructor(
        private catalogoPaisModelMapper: CatalogoPaisModelMapperService
    ) {
        super()
    }

    protected map(model: CatalogoLocalidadModel): CatalogoLocalidadEntity {
        if (model) {
            const entity: CatalogoLocalidadEntity = {}

            if (model.id) {
                entity.id = model.id
            }

            if (model.codigo) {
                entity.codigo = model.codigo
            }

            if (model.codigoPostal) {
                entity.codigoPostal = model.codigoPostal
            }

            if (model.nombre) {
                entity.nombre = model.nombre
            }

            if (model.pais) {
                entity.catalogoPais = this.catalogoPaisModelMapper.transform(model.pais)
            }

            return entity
        }

        return null
    }
}
