import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoPerfilEntity } from 'dominio/entidades/catalogos/catalogo-tipo-perfil.entity';
import { PerfilResumenModel } from 'dominio/modelo/entidades/perfil-resumen.model';
import { PerfilModel } from "dominio/modelo/entidades/perfil.model";

export interface CatalogoTipoPerfilModel {
  codigo?: string,
  nombre?: string,
  descripcion?: string
  mostrarDescripcion?: boolean
  perfil?: PerfilResumenModel | PerfilModel
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoPerfilModelMapperService extends MapedorService<CatalogoTipoPerfilModel, CatalogoTipoPerfilEntity> {

  constructor(
  ) {
    super()
  }

  protected map(model: CatalogoTipoPerfilModel,): CatalogoTipoPerfilEntity {
    return {
      codigo: model.codigo,
    };
  }
}
