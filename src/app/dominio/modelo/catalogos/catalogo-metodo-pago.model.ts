import {UsuarioEntity} from 'dominio/entidades';
import {TransaccionEntity} from 'dominio/entidades';
import {CatalogoTipoMonedaModel} from './catalogo-tipo-moneda.model';

import {Injectable} from '@angular/core';
import {MapedorService} from '@core/base/mapeador.interface';
import {CatalogoEstadoEntity} from 'dominio/entidades/catalogos/catalogo-estado.entity';
import {CatalogoMetodoPagoEntity, PagoFacturacionEntity} from 'dominio/entidades/catalogos/catalogo-metodo-pago.entity';

export interface CatalogoMetodoPagoModel {
  id?: string;
  estado?: CatalogoEstadoEntity;
  codigo?: string;
  nombre?: string;
  icono?: string;
  descripcion?: string;
}

export interface PagoStripeModel {
  nombre: string;
  email: string;
}


export interface PagoFacturacionModel {
  nombres: string;
  telefono?: string;
  direccion?: string;
  email?: string;
  idPago?: string;
}

export interface PagoFacturacionModelDonacionProyectos extends PagoFacturacionModel {
  monto?: number;
}

export interface CatalogoMetodoPagoExtraModel {
  monedaRegistro?: CatalogoTipoMonedaModel;
  metodoPago?: CatalogoMetodoPagoEntity;
  transaccion?: TransaccionEntity;
  datosFacturacion?: PagoFacturacionModel;
  usuario?: UsuarioEntity;

}


@Injectable({providedIn: 'root'})
export class CatalogoMetodoPagoModelMapperService extends MapedorService<CatalogoMetodoPagoModel, CatalogoMetodoPagoEntity> {

  constructor() {
    super();
  }

  protected map(model: CatalogoMetodoPagoModel): CatalogoMetodoPagoEntity {
    return (model) ? {
      codigo: model.codigo,
    } : null;
  }

}

@Injectable({providedIn: 'root'})
export class PagoFacturacionModelMapperService extends MapedorService<PagoFacturacionModel, PagoFacturacionEntity> {

  constructor() {
    super();
  }

  protected map(model: PagoFacturacionModel): PagoFacturacionEntity {
    return (model) ? {
      nombres: model.nombres,
      direccion: model.direccion,
      email: model.email,
      telefono: model.telefono
    } : null;
  }

}


export interface PaymentezTransaccion {
  transaction?: Transaccion;
}

export interface Transaccion {
  status?: string;
  authorization_code?: string;
  status_detail?: number;
  message?: string;
  id?: string;
  payment_date?: Date;
  dev_reference?: string;
  carrier_code?: string;
  current_status?: string;
  amount?: number;
  carrier?: string;
  installments?: number;
}
