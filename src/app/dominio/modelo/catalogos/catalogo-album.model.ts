import { CatalogoAlbumEntity } from "dominio/entidades/catalogos/catalogo-album.entity";
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
export interface CatalogoAlbumModel {
    id?: string,
    codigo?: string,
    estado?: any //Catalogo estados
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    nombre?: string,
    descripcion?: string
}

@Injectable({ providedIn: 'root' })
export class CatalogoAlbumModelMapperService extends MapedorService<CatalogoAlbumModel, CatalogoAlbumEntity> {
    constructor() {
        super();
    }

    protected map(model: CatalogoAlbumModel): CatalogoAlbumEntity {
        if (model) {
            return {
                codigo: model.codigo
            };
        }
        return null;
    }
}
