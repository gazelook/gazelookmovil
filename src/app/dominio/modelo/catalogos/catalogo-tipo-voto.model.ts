import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoVotoEntity } from 'dominio/entidades/catalogos/catalogo-tipo-voto.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface CatalogoTipoVotoModel {
  id?: string,
  codigo?: string,
  nombre?: string,
  estado?: CatalogoEstadoModel
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  votoAdmin?: boolean,
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoVotoModelMapperService extends MapedorService<CatalogoTipoVotoModel, CatalogoTipoVotoEntity> {

  constructor(
    private catalogoEstadoModelMapper: CatalogoEstadoModelMapperService,
  ) {
    super()
  }

  protected map(model: CatalogoTipoVotoModel): CatalogoTipoVotoEntity {

    if (model) {

      const entity: CatalogoTipoVotoEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.estado) {
        entity.estado = this.catalogoEstadoModelMapper.transform(model.estado)
      }

      if (model.fechaCreacion) {
        entity.fechaCreacion = model.fechaCreacion
      }

      if (model.fechaActualizacion) {
        entity.fechaActualizacion = model.fechaActualizacion
      }

      if (model.codigo) {
        entity.codigo = model.codigo
      }

      if (model.nombre) {
        entity.nombre = model.nombre
      }

      entity.votoAdmin = model.votoAdmin

      return entity;
    }

    return null;
  }

}
