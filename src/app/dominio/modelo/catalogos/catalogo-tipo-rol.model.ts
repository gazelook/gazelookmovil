import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface CatalogoTipoRolModel {
  id?: string,
  codigo?: string,
  estado?: CatalogoEstadoModel
  nombre?: string,
  fechaCreacion?: Date,
}
