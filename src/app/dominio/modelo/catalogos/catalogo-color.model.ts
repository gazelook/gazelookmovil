import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoTipoColorModel } from "dominio/modelo/catalogos/catalogo-tipo-color.model";

export interface CatalogoColorModel {
    id?: string,
    codigo?: string,
    estado?: CatalogoEstadoModel,
    tipo?: CatalogoTipoColorModel,
    fechaCreacion?: Date,
    fechaActualizacion?: Date
}