import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { FormulaEventoModel } from "dominio/modelo/catalogos/formula-evento.model";
import { ConfiguracionEventoModel } from "dominio/modelo/catalogos/configuracion-evento.model";

export interface CatalogoEventoModel{
    _id?:string,
    codigo?:string,
    estado?:CatalogoEstadoModel //Catalogo estados
    fechaCreacion?:Date,
    fechaActualizacion?:Date,
    nombre?:string,
    configuraciones?:Array<ConfiguracionEventoModel>,
    formulas?:Array<FormulaEventoModel>
}
