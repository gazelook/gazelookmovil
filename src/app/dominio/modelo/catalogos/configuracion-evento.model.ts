import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";
import { CatalogoEventoModel } from "dominio/modelo/catalogos/catalogo-evento.model";
import { FormulaEventoModel } from "dominio/modelo/catalogos/formula-evento.model";
export interface ConfiguracionEventoModel {
  id?: string,
  codigo?: string,
  estado?: CatalogoEstadoModel, // CatalogoEstado
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  intervalo?: number,
  duracion?: number,
  ciclico?: boolean
  catalogoEvento?: CatalogoEventoModel,
  formulas?: Array<FormulaEventoModel>
}
