import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoMonedaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-moneda.entity';
import { CatalogoEstadoModel } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface CatalogoTipoMonedaModel {
    id?: string,
    codigo?: string,
    codNombre?: string,
    nombre?: string,
    predeterminado?: boolean,
    estado?: CatalogoEstadoModel //Catalogo estados
    fechaCreacion?: Date,
    fechaActualizacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoMonedaModelMapperService extends MapedorService<CatalogoTipoMonedaModel, CatalogoTipoMonedaEntity> {

    protected map(model: CatalogoTipoMonedaModel): CatalogoTipoMonedaEntity {
        if (model) {
            const entity: CatalogoTipoMonedaEntity = {}

            if (model.id) {
                entity._id = model.id
            }

            if (model.codigo) {
                entity.codigo = model.codigo
            }

            if (model.codNombre) {
                entity.codNombre = model.codNombre
            }

            if (model.codigo) {
                entity.codigo = model.codigo
            }

            if (model.predeterminado) {
                entity.predeterminado = model.predeterminado
            }

            return entity
        }
        return null
    }
}
