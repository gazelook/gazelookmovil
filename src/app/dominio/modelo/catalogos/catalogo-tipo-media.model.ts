import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoMediaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-media.entity';
import { CatalogoEstadoModel, CatalogoEstadoModelMapperService } from "dominio/modelo/catalogos/catalogo-estado.model";

export interface CatalogoTipoMediaModel{
    id?: string
    estado?: CatalogoEstadoModel
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?:string
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoMediaModelMapperService extends MapedorService<CatalogoTipoMediaModel, CatalogoTipoMediaEntity> {

    constructor(
        private catalogoEstadoModelMapper: CatalogoEstadoModelMapperService
    ) {
        super()
    }

    protected map(model: CatalogoTipoMediaModel): CatalogoTipoMediaEntity {
        const entity: CatalogoTipoMediaEntity = {}

        if (model.id) {
            entity._id = model.id
        }

        if (model.estado) {
            entity.estado = this.catalogoEstadoModelMapper.transform(model.estado)
        }

        if (model.codigo) {
            entity.codigo = model.codigo
        }

        return entity
    }
}
