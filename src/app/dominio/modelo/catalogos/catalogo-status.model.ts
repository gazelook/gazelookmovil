import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoStatusEntity } from "dominio/entidades/catalogos/catalogo-status.entity";

export interface CatalogoStatusModel {
  id?: String
  codigo?: string
  nombre?: string
}

@Injectable({ providedIn: 'root' })
export class CatalogoEstatusModelMapperService extends MapedorService<CatalogoStatusModel, CatalogoStatusEntity> {

  protected map(model: CatalogoStatusModel): CatalogoStatusEntity {
    if (model) {
      const entity: CatalogoStatusEntity = {}

      if (model.id) {
        entity._id = model.id
      }

      if (model.codigo) {
        entity.codigo = model.codigo
      }

      if (model.nombre) {
        entity.nombre = model.nombre
      }

      return entity
    }
    return null;
  }
}
