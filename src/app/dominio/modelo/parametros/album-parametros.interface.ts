import { AccionEntidad, CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum'

export interface AlbumParams {
    estado?: boolean, // Indica el estado de los params en el componente
    entidad?: CodigosCatalogoEntidad, // Entidad principal del album
    titulo?: string, // Titulo del album
    codigo?: string, // Codigo del tipo perfil en caso de que la url sea registro
    accionEntidad?: AccionEntidad, // Accion del album, se determina internamente por la url
}