import { CatalogoTipoPerfilModel } from 'dominio/modelo/catalogos/catalogo-tipo-perfil.model';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';

export interface RegistroParams {
    estado?: boolean,
    id?: string,
    tipoPerfil?: CatalogoTipoPerfilModel,
    accionEntidad?: AccionEntidad
}