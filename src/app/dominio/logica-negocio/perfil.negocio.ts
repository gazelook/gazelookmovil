import { AccionEntidad } from './../../nucleo/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoPerfil } from './../../nucleo/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { CatalogoTipoPerfilModel } from './../modelo/catalogos/catalogo-tipo-perfil.model';
import { RegistroParams } from './../modelo/parametros/registro-parametros.interface';
import { PerfilModel } from './../modelo/entidades/perfil.model';
import { PerfilEntity } from './../entidades/perfil.entity';
import { ItemSelector } from './../../compartido/diseno/modelos/elegible.interface';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { CodigosCatalogosEstadoPerfiles } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-perfiles.enun';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';
import { UsuarioModel } from 'dominio/modelo/entidades/usuario.model';
import { Injectable } from "@angular/core";
import { Observable, throwError, of } from 'rxjs';
import { catchError, map, } from 'rxjs/operators'
import { PerfilRepository } from "dominio/repositorio/perfil.repository";
import { FormGroup } from '@angular/forms';
import { DireccionModel } from 'dominio/modelo/entidades/direccion.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { TelefonoModel } from 'dominio/modelo/entidades/telefono.model';

@Injectable({
    providedIn: 'root'
})
export class PerfilNegocio {

    data: CatalogoTipoPerfilModel[]
    constructor(
        private perfilRepository: PerfilRepository,
        private cuentaNegocio: CuentaNegocio,
        private router: Router,
        private _location: Location,
    ) {

    }

    crearObjetoPerfilVacio(tipoPerfilACrear: CodigosCatalogoTipoPerfil): PerfilModel {
        return {
            _id: '',
            nombre: '',
            nombreContacto: '',
            direcciones: [],
            telefonos: [],
            tipoPerfil: this.obtenerTipoPerfilSegunCodigo(tipoPerfilACrear),
            estado: {
                codigo: CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR
            },
            album: []
        }
    }

    obtenerCatalogoTipoPerfil(): Observable<CatalogoTipoPerfilModel[]> {
        const data = this.perfilRepository.obtenerCatalogoTipoPerfilLocal()
        if (data) {
            return of(data)
        } else {
            return this.perfilRepository.obtenerCatalogoTipoPerfil()
                .pipe(
                    map((data: CatalogoTipoPerfilModel[]) => {

                        data.sort(function (x, y) {
                            if (x.codigo > y.codigo) {
                                return 1;
                            }
                            if (x.codigo < y.codigo) {
                                return -1;
                            }
                            return 0;
                        });
                        
                        this.almacenarCatalogoPerfiles(data);
                        return data;
                    }),
                    catchError(err => {
                        return throwError(err)
                    })
                )
        }
        //Se debe llenar el perfil con los datos de cuenta. 
    }


 
    buscarPerfiles(palabra: string, perfil: string, pagina: number ): Observable<PaginacionModel<PerfilModel>> {
        let limite = 16
        return this.perfilRepository.buscarPerfiles(palabra, limite, pagina, perfil).pipe(
            map((data: PaginacionModel<PerfilModel>) => {
                return data;
            }),
            catchError(err => {
                return throwError(err)
            })
        )

    }

    obtenerCatalogoTipoPerfilConPerfil(session: boolean = true): Observable<CatalogoTipoPerfilModel[]> {
        return this.obtenerCatalogoTipoPerfil().pipe(
            map((data: CatalogoTipoPerfilModel[]) => {
                let usuario = (session) ? this.cuentaNegocio.obtenerUsuarioDelSessionStorage() : this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
                if (usuario) {
                    for (let perfil of usuario.perfiles) {
                        for (let tipo of data) {
                            if (tipo.codigo == perfil.tipoPerfil.codigo) {
                                tipo.perfil = perfil
                                break;
                            }
                        }
                    }
                }
                return data;
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

    almacenarCatalogoPerfiles(tipoPerfiles: CatalogoTipoPerfilModel[]) {
        
        return this.perfilRepository.almacenarCatalogoPerfiles(tipoPerfiles);
    }

    obtenerCatalogoTipoPerfilLocal(): CatalogoTipoPerfilModel[] {
        return this.perfilRepository.obtenerCatalogoTipoPerfilLocal();
    }

    conflictoCrearPerfil(tipoPerfilCrear: CatalogoTipoPerfilModel, tipoPerfiles: CatalogoTipoPerfilModel[]) {
        if (tipoPerfilCrear.codigo == CodigosCatalogoTipoPerfil.GROUP) {
            const index = tipoPerfiles.findIndex(e => 
                e.codigo !== CodigosCatalogoTipoPerfil.GROUP && 
                e.perfil !== null &&
                e.perfil.estado.codigo !== CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR &&
                e.perfil.estado.codigo !== CodigosCatalogosEstadoPerfiles.PERFIL_CREADO
            )
            return (index >= 0)
        } else {
            const index = tipoPerfiles.findIndex(e => 
                e.codigo === CodigosCatalogoTipoPerfil.GROUP && 
                e.perfil !== null && 
                e.perfil.estado.codigo !== CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR &&
                e.perfil.estado.codigo !== CodigosCatalogosEstadoPerfiles.PERFIL_CREADO
            )
            return (index >= 0)
        }
    }

    limpiarPerfiles(tipoPerfiles: CatalogoTipoPerfilModel[]) {
        tipoPerfiles.forEach(tipoPerfil => tipoPerfil.perfil = null)
        this.almacenarCatalogoPerfiles(tipoPerfiles);
    }

    obtenerTipoPerfilSegunCodigo(codigoPerfil: string): CatalogoTipoPerfilModel {
        let tipoPerfil: CatalogoTipoPerfilModel
        const tipoPerfiles = this.obtenerCatalogoTipoPerfilLocal()
        tipoPerfiles.forEach(perfil => {
            if (perfil.codigo === codigoPerfil) {
                tipoPerfil = perfil
            }
        })
        return tipoPerfil
    }

    validarPerfilModelDelSessionStorage(codigoPerfil: string): PerfilModel {
        const usuario: UsuarioModel = this.cuentaNegocio.validarUsuarioDelSesionStorage(codigoPerfil)
        let perfil: PerfilModel
        if (usuario) {
            usuario.perfiles.forEach(item => {
                if (item.tipoPerfil.codigo === codigoPerfil) {
                    perfil = item
                }
            })

            // Si el perfil no existe, se crea y se actualiza el usuario
            if (!perfil) {
                perfil = {
                    _id: '',
                    nombre: '',
                    nombreContacto: '',
                    direcciones: [],
                    telefonos: [],
                    tipoPerfil: this.obtenerTipoPerfilSegunCodigo(codigoPerfil),
                    estado: {
                        codigo: CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR
                    },
                    album: []
                }
                usuario.perfiles.push(perfil)
                this.cuentaNegocio.guardarUsuarioEnSessionStorage(usuario)
            }
        }
        return perfil
    }

    obtenerIdDelPerfilSeleccionado(): string {
        const perfil: PerfilModel = this.perfilRepository.obtenerPerfilSeleccionado()
        let id = ''
        if (perfil) {
            id = perfil._id
        }
        return id
    }

    obtenerDatosDelPerfil(id: string): Observable<PerfilModel> {
        return this.perfilRepository.obtenerDatosDelPerfil(id)
    }

    actualizarPerfilEnUsuarioDelSessionStorage(perfil: PerfilModel) {
        const usuario: UsuarioModel = this.cuentaNegocio.validarUsuarioDelSesionStorage(perfil.tipoPerfil.codigo)
        let pos = -1
        usuario.perfiles.forEach((item, i) => {
            if (item.tipoPerfil.codigo === perfil.tipoPerfil.codigo) {
                pos = i
            }
        })
        if (pos >= 0) {
            usuario.perfiles[pos] = perfil
        }
        this.cuentaNegocio.guardarUsuarioEnSessionStorage(usuario)
    }
    

    almacenarPerfilSeleccionado(tipoPerfil: CatalogoTipoPerfilModel) {
        if (tipoPerfil.perfil) {
            (tipoPerfil.perfil as PerfilModel).tipoPerfil.nombre = tipoPerfil.nombre
        }

        this.perfilRepository.almacenarPerfilSeleccionado(tipoPerfil.perfil);
    }

    removerPerfilSeleccionado() {
        this.perfilRepository.removerPerfilSeleccionado()
    }

    actualizarPerfilSeleccionado(perfil: PerfilModel){
        this.perfilRepository.almacenarPerfilSeleccionado(perfil);
    }

    obtenerPerfilSeleccionado(): PerfilModel {
        const perfil = this.perfilRepository.obtenerPerfilSeleccionado();
        if (
            perfil &&
            perfil.estado &&
            perfil.estado.codigo !== CodigosCatalogosEstadoPerfiles.PERFIL_HIBERNADO &&
            perfil.estado.codigo !== CodigosCatalogosEstadoPerfiles.PERFIL_ELIMINADO
        ) {
            return perfil
        }

        return undefined
    }

    validarNombreDeContactoUnico(nombreContacto: string, traducirContactName: boolean): Observable<object> {
        return this.perfilRepository.validarNombreDeContactoUnico(nombreContacto, traducirContactName)
    }

    guardarPerfilActivoEnSessionStorage(perfil: PerfilModel) {
        this.perfilRepository.guardarPerfilActivoEnSessionStorage(perfil)
    }

    obtenerPerfilActivoDelSessionStorage(): PerfilModel {
        return this.perfilRepository.obtenerPerfilActivoDelSessionStorage()
    }

    removerPerfilActivoDelSessionStorage() {
        this.perfilRepository.removerPerfilActivoDelSessionStorage()
    }

    // Devuelve la llave de la traduccion del texto segun el tipo de perfil
    obtenerLlaveSegunCodigoPerfil(
        codigoPerfil: CodigosCatalogoTipoPerfil
    ) {
        switch (codigoPerfil) {
            case CodigosCatalogoTipoPerfil.CLASSIC:
                return 'm2v7texto8'
            case CodigosCatalogoTipoPerfil.PLAYFUL:
                return 'm2v7texto7'
            case CodigosCatalogoTipoPerfil.SUBSTITUTE:
                return 'm2v7texto6'
            case CodigosCatalogoTipoPerfil.GROUP:
                return 'm2v8texto1'
            default:
                return ''
        }
    }

    validarOrigenPerfilActivo(id: string) : Observable<PerfilModel> {
        let perfil: PerfilModel = this.obtenerPerfilActivoDelSessionStorage()
        if (perfil) {
            return of(perfil)
        } else {
            return this.obtenerDatosDelPerfil(id)
        }
    }

    validarFormularioDelPerfil(
        form: FormGroup
    ): boolean {
        const { contrasena, direccion, email, nombre, nombreContacto } = form.value
        if (
            contrasena && contrasena.length > 0 &&
            email && email.length > 0 &&
            nombre && nombre.length > 0 &&
            nombreContacto && nombreContacto.length > 0
        ) {
            return true
        }

        return false
    }

    // Actualizar perfil antes de pasar a albunes
    guardarInformacionPerfilSegunAccionRegistro(
        perfil: PerfilModel,
        estadoPerfil: CodigosCatalogosEstadoPerfiles,
        codigoPerfil: string,
        registroForm: FormGroup,
        paisSeleccionado: ItemSelector
    ) {
        
        const usuario: UsuarioModel = this.cuentaNegocio.validarUsuarioDelSesionStorage(codigoPerfil)
        const { contrasena, direccion, email, nombre, apellido, nombreContacto, nombreContactoTraducido, telefono, fechaNacimiento, direccionDomiciliaria, documentoIdentidad } = registroForm.value
        const itemPais = paisSeleccionado

        // Asignar data
        perfil.nombreContacto = nombreContacto
        perfil.nombreContactoTraducido = nombreContactoTraducido
 
       
        perfil.nombre = nombre + '-' + apellido
        perfil.direcciones[0] = {
            descripcion: direccion,
            pais: {
                codigo: (itemPais) ? itemPais.codigo : '',
                nombre: (itemPais) ? itemPais.nombre : ''
            }
        }


        if (registroForm.value.telefono) {
            perfil.telefonos[0] = {
                numero: telefono,
                pais: {
                    codigo: (itemPais) ? itemPais.codigo : '',
                    nombre: (itemPais) ? itemPais.nombre : ''
                }
            }
        } else {
            perfil.telefonos.pop()
        }
        // Se cambia el estado a creado
        perfil.estado.codigo = estadoPerfil

        const pos = usuario.perfiles.findIndex(e => e.tipoPerfil.codigo === perfil.tipoPerfil.codigo)

        if (pos >= 0) {
            usuario.perfiles[pos] = perfil
        } else {
            usuario.perfiles.push(perfil)
        }
        
        // Actualizar el perfil activo con la data nueva
        this.guardarPerfilActivoEnSessionStorage(perfil)

        // Actualizar email y contrasena
        usuario.documentoIdentidad = documentoIdentidad
        usuario.direccionDomiciliaria = direccionDomiciliaria
        usuario.email = email
        usuario.contrasena = contrasena
        usuario.fechaNacimiento = fechaNacimiento
        this.cuentaNegocio.guardarUsuarioEnSessionStorage(usuario)

        // Actualizar los demas perfiles
        this.actualizarOtrosPerfilesAlCambiarInformacionDelPerfilActivo(perfil)
    }

    // Actualizar perfil antes de pasar a albunes
    guardarInformacionPerfilSegunAccionActualizar(
        perfil: PerfilModel,
        registroForm: FormGroup,
        paisSeleccionado: ItemSelector
    ) {
        const { direccion, nombre, nombreContacto, nombreContactoTraducido, apellido, telefono } = registroForm.value
        const itemPais = paisSeleccionado

        // Asignar data
        if (nombreContactoTraducido) {
            perfil.nombreContactoTraducido = nombreContactoTraducido
        }
        perfil.nombreContacto = nombreContacto
        perfil.nombre = nombre + '-' + apellido
        // Direcciones
        perfil.direcciones[0] = {
            id: perfil.direcciones[0]?.id,
            descripcion: direccion,
            pais: {
                codigo: (itemPais) ? itemPais.codigo : '',
                nombre: (itemPais) ? itemPais.nombre : ''
            }
        }

        if (telefono) {
            if ((telefono as string).length > 0) {
                if (perfil.telefonos.length > 0) {
                    perfil.telefonos[0].numero = telefono
                    perfil.telefonos[0].pais = {
                        codigo: (itemPais) ? itemPais.codigo : '',
                        nombre: (itemPais) ? itemPais.nombre : ''
                    }
                } else {
                    perfil.telefonos[0] = {
                        numero: telefono,
                        pais: {
                            codigo: (itemPais) ? itemPais.codigo : '',
                            nombre: (itemPais) ? itemPais.nombre : ''
                        }
                    }
                }
            } else {
                perfil.telefonos = []
            }
        }
        
        this.guardarPerfilActivoEnSessionStorage(perfil)
    }

    guardarInformacionPerfilSegunAccionCrear(
        perfil: PerfilModel,
        registroForm: FormGroup,
        paisSeleccionado: ItemSelector,
        estadoPerfil: CodigosCatalogosEstadoPerfiles
    ) {
        const { direccion, nombre, nombreContacto, nombreContactoTraducido, apellido, telefono } = registroForm.value
        const itemPais = paisSeleccionado
        // Asignar data
        if (nombreContactoTraducido) {
            perfil.nombreContactoTraducido = nombreContactoTraducido
        }
        perfil.estado.codigo = estadoPerfil
        perfil.nombreContacto = nombreContacto
        perfil.nombre = nombre + '-' + apellido
        // Direcciones
        perfil.direcciones[0] = {
            id: perfil.direcciones[0]?.id,
            descripcion: direccion,
            pais: {
                codigo: (itemPais) ? itemPais.codigo : '',
                nombre: (itemPais) ? itemPais.nombre : ''
            }
        }

        if (telefono && (telefono as string).length > 0) {
            perfil.telefonos[0] = {
                numero: telefono,
                pais: {
                    codigo: (itemPais) ? itemPais.codigo : '',
                    nombre: (itemPais) ? itemPais.nombre : ''
                }
            }
        }
        
        this.guardarPerfilActivoEnSessionStorage(perfil)
    }

    actualizarOtrosPerfilesAlCambiarInformacionDelPerfilActivo(
        perfilActivo: PerfilModel
    ) {
        try {
            const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelSessionStorage()

            if (!usuario || !perfilActivo) {
                throw new Error('Error no existen los datos necesarios')
            }

            usuario.perfiles.forEach(e => {
                e.nombre = perfilActivo.nombre
                e.direcciones = perfilActivo.direcciones
                e.telefonos = perfilActivo.telefonos
            })

            this.cuentaNegocio.guardarUsuarioEnSessionStorage(usuario)
        } catch (error) {
        }
    }

    removerAlbumsConIdDelPerfil(perfil: PerfilModel): PerfilModel {
        const album: AlbumModel[] = []
        if (
            perfil &&
            perfil.album &&
            perfil.album.length > 0
        ) {
            perfil.album.forEach(item => {
                if (item && !item._id) {
                    album.push(item)
                }
            })
        }
        perfil.album = album
        return perfil
    }

    actualizarPerfil(perfil: PerfilModel): Observable<PerfilModel> {
        const perfilSinAlbums: PerfilModel = this.removerAlbumsConIdDelPerfil(perfil)
        return this.perfilRepository.actualizarPerfil(perfilSinAlbums)
    }

    actualizarDatosUsuario(usuario: UsuarioModel, idUsuario: string): Observable<string> {
        return this.perfilRepository.actualizarDatosUsuario(usuario, idUsuario)
    }

    actualizarDataDelPerfilEnElUsuarioDelLocalStorage(
        perfil: PerfilModel
    ) {
        const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
        if (usuario) {
            usuario.perfiles.forEach(item => {
                if (item && item.tipoPerfil.codigo === perfil.tipoPerfil.codigo) {
                    item.estado.codigo = perfil.estado.codigo
                    item.nombre = perfil.nombre
                    item.nombreContacto = perfil.nombreContacto
                }
            })
            this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuario)
        }
    }


    eliminarDataDelPerfilEnElUsuarioDelLocalStorage(
        perfil: PerfilModel
    ) {
        const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
        if (usuario) {
            let pos = -1
            usuario.perfiles.forEach((item, i) => {
                if (item.tipoPerfil.codigo === perfil.tipoPerfil.codigo) {
                    pos = i
                }
            })
            if (pos >= 0) {
                usuario.perfiles.splice(pos, 1)
            }
            this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuario)
        }
    }

    eliminarHibernarElPerfil(perfil: PerfilModel) : Observable<object> {
        return this.perfilRepository.eliminarHibernarElPerfil(perfil)
    }

    inicializarPerfilActivoParaAccionCrear(tipoPerfil: CatalogoTipoPerfilModel): PerfilModel {
        let perfil : PerfilModel = this.obtenerPerfilActivoDelSessionStorage()
        if (!perfil) {
            perfil = {
                _id: '',
                nombre: '',
                nombreContacto: '',
                direcciones: [],
                telefonos: [],
                tipoPerfil: tipoPerfil,
                estado: {
                    codigo: CodigosCatalogosEstadoPerfiles.PERFIL_SIN_CREAR
                },
                album: []
            }
            this.guardarPerfilActivoEnSessionStorage(perfil)
        }
        return perfil
    }

    insertarPerfilEnElUsuario(perfil: PerfilModel) {
        const usuario: UsuarioModel = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
        if (usuario) {
            usuario.perfiles.push(perfil)
            this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuario)
        }
    }

    crearPerfilEnElUsuario(perfil: PerfilModel, usuario: UsuarioModel): Observable<PerfilModel> {
        return this.perfilRepository.crearPerfilEnElUsuario(perfil, usuario)
    }

    activarPerfil(perfil: PerfilModel): Observable<object> {
        return this.perfilRepository.activarPerfil(perfil)
    } 

    obtenerPerfilGeneral(idPerfil: string, idPerfilOtros?: string): Observable<PerfilEntity> {
        return this.perfilRepository.obtenerPerfilGeneral(idPerfil, idPerfilOtros);
    }

    obtenerPerfilBasico(
        idPerfil: string,
        perfilVisitante: string = ''
    ): Observable<PerfilModel> {
        return this.perfilRepository.obtenerPerfilBasico(idPerfil, perfilVisitante)
    }

    obtenerPerfilDelUsuarioSegunTipo(
        tipoPerfil: CodigosCatalogoTipoPerfil,
        sacarDelSession: boolean = true
    ): PerfilModel {
        const usuario = (sacarDelSession) 
            ? this.cuentaNegocio.obtenerUsuarioDelSessionStorage()
            : this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
        
        if (!usuario) {
            return null
        }

        let pos = -1
        usuario.perfiles.forEach((perfil, i) => {
            if (perfil && perfil.tipoPerfil.codigo === tipoPerfil) {
                pos = i
            }
        })
        if (pos < 0) {
            const perfilNuevo: PerfilModel = this.crearObjetoPerfilVacio(tipoPerfil)
            if (usuario.perfiles.length > 0) {
                const otroPerfil: PerfilModel = usuario.perfiles[0]
                perfilNuevo.nombre = otroPerfil.nombre
                perfilNuevo.direcciones = otroPerfil.direcciones
                perfilNuevo.telefonos = otroPerfil.telefonos
            }

            return perfilNuevo
        }

        return usuario.perfiles[pos]
    }

    validarInformacionDelPerfil(
        params: RegistroParams,
        perfil: PerfilModel
    ) {
        if (params.accionEntidad === AccionEntidad.REGISTRO) {
            // Validar estado para destruir o guardar en el usuario
            this.cuentaNegocio.validarEstadoPerfilParaDestruir(params.tipoPerfil.codigo, perfil.estado.codigo as CodigosCatalogosEstadoPerfiles)
        }

        this.removerPerfilActivoDelSessionStorage()
        this.removerTipoPerfilActivoDelSessionStorage()
    }

    
    guardarTipoPerfilActivo(tipoPerfil: CatalogoTipoPerfilModel) {
        this.perfilRepository.guardarTipoPerfilActivo(tipoPerfil)
    }

    obtenerTipoPerfilActivo(): CatalogoTipoPerfilModel {
        return this.perfilRepository.obtenerTipoPerfilActivo()
    }

    removerTipoPerfilActivoDelSessionStorage() {
        this.perfilRepository.removerTipoPerfilActivoDelSessionStorage()
    }

    cambiarTipoPerfilActivoSegunCodigo(codigo: CodigosCatalogoTipoPerfil) {
        const tipoPerfil: CatalogoTipoPerfilModel[] = this.obtenerCatalogoTipoPerfilLocal()
        if (tipoPerfil) {
            tipoPerfil.forEach(tipo => {
                if (tipo.codigo === codigo) {
                    this.guardarTipoPerfilActivo(tipo)
                }
            })
        }
    }

    buscarPerfilesPorNombre(
        query: string,
        idPerfil: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<PerfilModel>> {
        return this.perfilRepository.buscarPerfilesPorNombre(
            query, 
            idPerfil,
            limite,
            pagina
        ).pipe(
            map((data: PaginacionModel<PerfilModel>) => {
                return data
            }),
            catchError(err => {
                return throwError(err)
            })
        )

    }

    buscarContactosPorNombre(
        query: string,
        idPerfil: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<PerfilModel>> {
        return this.perfilRepository.buscarContactosPorNombre(
            query, 
            idPerfil,
            limite,
            pagina
        ).pipe(
            map((data: PaginacionModel<PerfilModel>) => {
                return data
            }),
            catchError(err => {
                return throwError(err)
            })
        )

    }

    validarSiHayMasDeUnPerfilCreado(
        sacarDelSession: boolean = true
    ): boolean {
        const usuario = (sacarDelSession) 
            ? this.cuentaNegocio.obtenerUsuarioDelSessionStorage()
            : this.cuentaNegocio.obtenerUsuarioDelLocalStorage()

        if (!usuario || usuario === null) {
            return false
        }

        if (
            usuario &&
            usuario.perfiles &&
            usuario.perfiles.length >= 1
        ) {
            let estado = false
            usuario.perfiles.forEach(perfil => {
                if (perfil.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_CREADO) {
                    estado = true
                }
            })
            return estado
        }

        return false
    }

    obtenerInformacionDelPerfil(
        idPerfil: string, 
        idPerfilOtros?: string
    ): Observable<PerfilModel> {
        return this.perfilRepository.obtenerInformacionDelPerfil(idPerfil, idPerfilOtros)
    }

    validarSiExistenCambiosEnElPerfil(
        perfilAValidar: PerfilModel
    ): boolean {
        // let return false
        try {
            const perfilOriginal = this.obtenerPerfilActivoDelSessionStorage()

            if (!perfilOriginal || !perfilAValidar) {
                return false
            }

            if (perfilAValidar.nombreContacto.trim() !== perfilOriginal.nombreContacto.trim()) {
                return true
            }

            if (perfilAValidar.direcciones.length === 0 && perfilOriginal.direcciones.length === 0) {
                return false
            }

            const direccionOriginal = perfilOriginal.direcciones[0]
            const direccionModificada = perfilAValidar.direcciones[0]

            if (
                !direccionOriginal &&
                !direccionModificada
            ) {
                return false
            }

            if (
                direccionOriginal &&
                !direccionModificada
            ) {
                return true
            }

            if (
                !direccionOriginal &&
                direccionModificada
            ) {
                return true
            }


            if (
                direccionOriginal && direccionModificada &&
                (direccionOriginal.pais.codigo.trim() !== direccionModificada.pais.codigo.trim())
            ) {

                return true
            }

            if (
                direccionOriginal && direccionModificada &&
                !direccionOriginal.descripcion &&
                !direccionModificada.descripcion
            ) {

                return false
            }

            if (
                direccionOriginal && direccionModificada &&
                direccionOriginal.descripcion &&
                !direccionModificada.descripcion
            ) {

                return true
            }

            if (
                direccionOriginal && direccionModificada &&
                !direccionOriginal.descripcion &&
                direccionModificada.descripcion
            ) {
                return false
            }

            if (
                direccionOriginal && direccionModificada &&
                direccionOriginal.descripcion.trim() !== direccionModificada.descripcion.trim()
            ) {
                return true
            }

            const telefonosOriginal = perfilOriginal.telefonos[0]
            const telefonosModificado = perfilAValidar.telefonos[0]

            if (
                !telefonosOriginal &&
                !telefonosModificado
            ) {
                return false
            }

            if (
                telefonosOriginal &&
                !telefonosModificado
            ) {

                return true
            }

            if (
                !telefonosOriginal &&
                telefonosModificado
            ) {

                return true
            }

            if (
                telefonosOriginal && telefonosModificado &&
                (telefonosOriginal.numero.trim() !== telefonosModificado.numero.trim())
            ) {

                return true
            }

        } catch (error) {
            return false
        }
    }

    guardarInformacionPerfilSegunAccionActualizarSinSessionStorage(
        perfil: PerfilModel,
        registroForm: FormGroup,
        paisSeleccionado: ItemSelector
    ): PerfilModel {
        const { direccion, nombre, nombreContacto, nombreContactoTraducido, apellido, telefono } = registroForm.value
        const itemPais = paisSeleccionado

        // Asignar data
        if (nombreContactoTraducido) {
            perfil.nombreContactoTraducido = nombreContactoTraducido
        }
        perfil.nombreContacto = nombreContacto
        perfil.nombre = nombre + '-' + apellido
        // Direcciones
        perfil.direcciones[0] = {
            id: perfil.direcciones[0]?.id,
            descripcion: direccion,
            pais: {
                codigo: (itemPais) ? itemPais.codigo : '',
                nombre: (itemPais) ? itemPais.nombre : ''
            }
        }

        if (telefono && (telefono as string).length > 0) {
            if (perfil.telefonos.length > 0) {
                perfil.telefonos[0].numero = telefono
                perfil.telefonos[0].pais = {
                    codigo: (itemPais) ? itemPais.codigo : '',
                    nombre: (itemPais) ? itemPais.nombre : ''
                }
            } else {
                perfil.telefonos[0] = {
                    numero: telefono,
                    pais: {
                        codigo: (itemPais) ? itemPais.codigo : '',
                        nombre: (itemPais) ? itemPais.nombre : ''
                    }
                }
            }
        }

        return perfil
    }

    obtenerResumenPerfilParaCoautorProyecto(
		idPerfil: string
	): Observable<PerfilModel> {
        return this.perfilRepository.obtenerResumenPerfilParaCoautorProyecto(idPerfil)
    }

    validarEstadoDelPerfil(
        perfilSeleccionado: PerfilModel,
        estadoParams: boolean = true
    ) {
      
        
        // Si params es valido y hay perfil activo
        if (
            estadoParams &&
            perfilSeleccionado &&
            perfilSeleccionado.estado &&
            perfilSeleccionado.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
        ) {
            return
        }

        // Si params no es valido y hay perfil seleccionado
        if (
            !estadoParams &&
            perfilSeleccionado &&
            perfilSeleccionado.estado &&
            perfilSeleccionado.estado.codigo === CodigosCatalogosEstadoPerfiles.PERFIL_ACTIVO
        ) {
            this._location.back()
            return
        }

        // Si no hay perfil seleccionado (sin immportar el valor de estadoParams)
        if (!perfilSeleccionado) {
            this._location.replaceState('')
            this.router.navigateByUrl('')
            return
        }
    }

    actualizarCamposEnLosDemasPerfiles(
        direcciones: Array<DireccionModel>,
        telefonos: Array<TelefonoModel>,
        session: boolean = false
    ) {
        try {
            const usuario = (session) ?
                this.cuentaNegocio.obtenerUsuarioDelSessionStorage() :
                this.cuentaNegocio.obtenerUsuarioDelLocalStorage()

            if (!usuario) {
                throw new Error('No hay usuario')
            }

            usuario.perfiles.forEach(e => {
                if (e.direcciones.length > 0) {
                    e.direcciones[0].pais = direcciones[0].pais
                    e.direcciones[0].descripcion = direcciones[0].descripcion
                    e.direcciones[0].descripcionOriginal = direcciones[0].descripcionOriginal
                }

                if (e.telefonos.length > 0) {
                    e.telefonos[0].numero = telefonos[0].numero
                    e.telefonos[0].pais = telefonos[0].pais
                }
            })

            if (session) {
                this.cuentaNegocio.guardarUsuarioEnSessionStorage(usuario)
                return
            }

            this.cuentaNegocio.guardarUsuarioEnLocalStorage(usuario)
        } catch (error) {
        }

    }
}
