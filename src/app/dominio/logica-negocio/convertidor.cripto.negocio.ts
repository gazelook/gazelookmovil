import {Injectable} from '@angular/core';
import {ConvertidorCriptoRepository} from 'dominio/repositorio/convertidor.cripto.repository';
import {Observable, throwError} from '@env/node_modules/rxjs';
import {catchError, map} from '@env/node_modules/rxjs/operators';
import {RespuestaRemota} from '@core/util/respuesta';


@Injectable({providedIn: 'root'})
export class ConvertidorCriptoNegocio {
  constructor(
    private convertidorCriptoRepository: ConvertidorCriptoRepository
  ) {
  }

  public convertirMonedaACripto(amount: number, convertID: number, symbol: string): Observable<number> {
    return this.convertidorCriptoRepository.convertirMonedaACripto(amount, convertID, symbol)
      .pipe(map(data => data), catchError(err => throwError(err)));
  }
}
