import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { ReporteFinanzasUsuarioModel } from "dominio/modelo/entidades/reporte-finanzas-usuario.model";
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { FinanzasRepository } from 'dominio/repositorio/finanzas.repository';

@Injectable({ providedIn: 'root' })
export class FinanzasNegocio {

  constructor(
    private finanzasRepository: FinanzasRepository
  ) { }

  obtenerAportaciones(idUsuario: string, fechaInicial: string, fechaFinal: string): Observable<any> {
    return this.finanzasRepository.obtenerAportaciones(idUsuario, fechaInicial, fechaFinal);
  }

  obtenerDocumentosLegales(origen: string, fechaInicial: string, limite: number, pagina: number): Observable<PaginacionModel<any>> {
    return this.finanzasRepository.obtenerDocumentosLegales(origen, fechaInicial, limite, pagina);
  }

  obtenerAportacionesGenerales(filtro: string, codigo: string, fechaInicial: string, fechaFinal: string): Observable<any> {
    return this.finanzasRepository.obtenerAportacionesGenerales(filtro, codigo, fechaInicial, fechaFinal);
  }

   obtenerDatosInformeActualFinanzas(idUsuario: string, anio: string, mes?:string): Observable<Array<ReporteFinanzasUsuarioModel>> {
    return this.finanzasRepository.obtenerDatosInformeActualFinanzas(idUsuario, anio, mes)
  } 

  obtenerTarifasTotalesUsuario(fecha?: string, codigoPais?: string, pais?: string): Observable<Array<ReporteFinanzasUsuarioModel>> {    
    return this.finanzasRepository.obtenerTarifasTotalesUsuario(fecha, codigoPais)
  }
}
