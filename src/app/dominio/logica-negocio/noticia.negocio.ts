import { Injectable } from "@angular/core";
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { NoticiaParams } from 'dominio/modelo/parametros/noticia-params.interface';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoEstadoNoticia } from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-estado-noticia.enum';
import { NoticiaEntity } from 'dominio/entidades/noticia.entity';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { VotoNoticiaModel } from 'dominio/modelo/entidades/voto-noticia.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { NoticiaRepository } from 'dominio/repositorio/noticia.repository';
import { CuentaNegocio } from 'dominio/logica-negocio/cuenta.negocio';


@Injectable({
  providedIn: 'root'
})
export class NoticiaNegocio {

  constructor(
    private noticiaRepository: NoticiaRepository,
    private cuentaNegocio: CuentaNegocio,
  ) {   }

  obtenerNoticiasPerfil(idPerfil: string, limite: number, pagina: number): Observable<PaginacionModel<NoticiaEntity>> {
    return this.noticiaRepository.obtenerNoticiasPerfil(idPerfil, limite, pagina);
  }

  obtenerNoticiasMapeadasDelPerfil(idPerfil: string, limite: number, pagina: number, traducir: boolean): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.obtenerNoticiasMapeadasDelPerfil(idPerfil, limite, pagina, traducir);
  }

  guardarNoticiaActivaEnSessionStorage(noticia: NoticiaModel) {
    this.noticiaRepository.guardarNoticiaActivaEnSessionStorage(noticia)
  }

  obtenerNoticiaActiviaDelSessionStorage(): NoticiaModel {
    return this.noticiaRepository.obtenerNoticiaActiviaDelSessionStorage()
  }

  removerNoticiaActivaDelSessionStorage() {
    this.noticiaRepository.removerNoticiaActivaDelSessionStorage()
  }

  obtenerNoticiaParaDemo(): NoticiaModel {
    return {
      id: '',
      estado: {
        codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR
      },
      fechaCreacion: new Date(),
      direccion: {
        descripcion: ''
      },
      autor: '',
      adjuntos: [],
      perfil: {
        nombre: 'm4v15texto9',
        nombreContacto: 'm4v15texto9',
        tipoPerfil: {
          codigo: CodigosCatalogoTipoPerfil.CLASSIC
        }
      },
      votos: [],
      totalVotos: 0,
      tituloCorto: '',
      titulo: '',
      descripcion: '',
      medias: []
    }
  }

  validarNoticiaActivaSegunAccionCrear(
    perfil: PerfilModel,
  ): NoticiaModel {
    let noticia: NoticiaModel = this.obtenerNoticiaActiviaDelSessionStorage()

    if (!noticia) {
      noticia = {
        id: '',
        estado: {
          codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR
        },
        fechaCreacion: new Date(),
        direccion: {
          descripcion: ''
        },
        autor: '',
        adjuntos: [],
        perfil: perfil,
        votos: [],
        totalVotos: 0,
        tituloCorto: '',
        titulo: '',
        descripcion: '',
        medias: []
      }

      this.guardarNoticiaActivaEnSessionStorage(noticia)
    }

    return noticia
  }

  crearObjetoDeNoticiaVacio(
    perfil: PerfilModel,
  ) {
    const noticia: NoticiaModel = {
      id: '',
      estado: {
        codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR
      },
      fechaCreacion: new Date(),
      direccion: {
        descripcion: ''
      },
      autor: '',
      adjuntos: [],
      perfil: perfil,
      votos: [],
      totalVotos: 0,
      tituloCorto: '',
      titulo: '',
      descripcion: '',
      medias: []
    }

    this.guardarNoticiaActivaEnSessionStorage(noticia)
  }

  crearObjetoDeNoticiaVacioParaRetornar(
    perfil: PerfilModel,
  ): NoticiaModel {
    return {
      id: '',
      estado: {
        codigo: CodigosCatalogoEstadoNoticia.SIN_CREAR
      },
      fechaCreacion: new Date(),
      direccion: {
        descripcion: ''
      },
      autor: '',
      adjuntos: [],
      perfil: perfil,
      votos: [],
      totalVotos: 0,
      tituloCorto: '',
      titulo: '',
      descripcion: '',
      medias: []
    }
  }

  asignarValoresDeLosCamposALaNoticia(
    params: NoticiaParams,
    noticia: NoticiaModel,
    noticiaForm: FormGroup,
    eliminarData: boolean,
  ): NoticiaModel {
    switch (params.accionEntidad) {
      case AccionEntidad.CREAR:
        noticia.tituloCorto = noticiaForm.value.tituloCorto
        noticia.titulo = noticiaForm.value.titulo
        noticia.autor = noticiaForm.value.autor
        noticia.direccion.descripcion = noticiaForm.value.ubicacion
        this.guardarNoticiaActivaEnSessionStorage(noticia)
        break
      case AccionEntidad.ACTUALIZAR:
        noticia.tituloCorto = noticiaForm.value.tituloCorto
        noticia.titulo = noticiaForm.value.titulo
        noticia.autor = noticiaForm.value.autor
        noticia.direccion.descripcion = noticiaForm.value.ubicacion
        break
      default: break;
    }

    if (eliminarData) {
      this.removerNoticiaActivaDelSessionStorage()
    }

    return noticia
  }

  crearNoticia(noticia: NoticiaModel): Observable<NoticiaModel> {
    return this.noticiaRepository.crearNoticia(noticia)
  }

  obtenerInformacionDeLaNoticia(idNoticia: string, idPerfil: string): Observable<NoticiaModel> {
    return this.noticiaRepository.obtenerInformacionDeLaNoticia(idNoticia, idPerfil)
  }

  actualizarNoticia(noticia: NoticiaModel): Observable<NoticiaModel> {
    noticia.adjuntos = []
    return this.noticiaRepository.actualizarNoticia(noticia)
  }

  excluirAlbumsYaRegistradosDeLaNoticia(noticia: NoticiaModel) {
    const adjuntosParaEnviar: Array<AlbumModel> = []
    noticia.adjuntos.forEach((item, pos) => {
      if (!(item && item._id)) {
        adjuntosParaEnviar.push(item)
      }
    })

    noticia.adjuntos = adjuntosParaEnviar
    return noticia
  }

  eliminarNoticia(idPerfil: string, idNoticia: string): Observable<string> {
    return this.noticiaRepository.eliminarNoticia(idPerfil, idNoticia)
  }

  apoyarNoticia(voto: VotoNoticiaModel): Observable<string> {
    return this.noticiaRepository.apoyarNoticia(voto)
  }

  buscarNoticiasSinfiltroFechas(
    limite: number,
    pagina: number,
    perfil: string
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.buscarNoticiasSinfiltroFechas(
      limite,
      pagina,
      perfil
    )
  }

  buscarNoticiasPorFechas(
    limite: number,
    pagina: number,
    fechaInicial: string,
    fechaFinal: string,
    filtro: string,
    perfil: string
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.buscarNoticiasPorFechas(
      limite,
      pagina,
      fechaInicial,
      fechaFinal,
      filtro,
      perfil
    )
  }

  buscarNoticiasPorTitulo(
    titulo: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<NoticiaModel>> {
    return this.noticiaRepository.buscarNoticiasPorTitulo(
      titulo,
      limite,
      pagina
    )
  }

  asignarValoresDeLosCamposALaNoticiaParaValidarCambios(
    noticia: NoticiaModel,
    noticiaForm: FormGroup
  ): NoticiaModel {
    noticia.tituloCorto = noticiaForm.value.tituloCorto
    noticia.titulo = noticiaForm.value.titulo
    noticia.autor = noticiaForm.value.autor
    noticia.direccion.descripcion = noticiaForm.value.ubicacion

    return noticia
  }

  validarSiExistenCambiosEnLaNoticia(
    noticiaAValidar: NoticiaModel,
    accionEntidad: AccionEntidad
  ): boolean {
    try {
      const noticiaOriginal = (accionEntidad === AccionEntidad.CREAR) ?
        this.crearObjetoDeNoticiaVacioParaRetornar(noticiaAValidar.perfil) :
        this.obtenerNoticiaActiviaDelSessionStorage()

      if (!noticiaOriginal || !noticiaAValidar) {
        return false
      }

      if (noticiaAValidar.tituloCorto.trim() !== noticiaOriginal.tituloCorto.trim()) {
        return true
      }

      if (noticiaAValidar.titulo.trim() !== noticiaOriginal.titulo.trim()) {
        return true
      }

      const direccionOriginal = noticiaOriginal.direccion
      const direccionModificada = noticiaAValidar.direccion

      if (direccionOriginal.descripcion.trim() !== direccionModificada.descripcion.trim()) {
        return true
      }

      if (noticiaAValidar.descripcion !== noticiaOriginal.descripcion) {
        return true
      }

      if (noticiaAValidar.autor.trim() !== noticiaOriginal.autor.trim()) {
        return true
      }

      if (noticiaAValidar.medias.length !== noticiaOriginal.medias.length) {
        return true
      }

      const mediaAValidar = noticiaAValidar.medias[0]
      const mediaOriginal = noticiaOriginal.medias[0]

      if (
        mediaAValidar &&
        mediaOriginal &&
        mediaAValidar.id !== mediaOriginal.id
      ) {
        return true
      }

      return false
    } catch (error) {
      return false
    }
  }
}
