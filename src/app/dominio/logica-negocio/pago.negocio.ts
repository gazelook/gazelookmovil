import { PagoEntity } from 'dominio/entidades/pago.entity';
import { CatalogoMetodoPagoExtraModel } from './../modelo/catalogos/catalogo-metodo-pago.model';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { CatalogoMetodoPagoModel } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';

import { MetodoPagoStripeEntity } from 'dominio/entidades/catalogos/catalogo-metodo-pago.entity';

import { IdiomaRepository } from 'dominio/repositorio/idioma.repository';
import { PagoRepository } from 'dominio/repositorio/pago.repository';
import { PerfilRepository } from 'dominio/repositorio/perfil.repository';

@Injectable({
  providedIn: 'root',
})
export class PagoNegocio {
  constructor(
    private metodoPagoRepository: PagoRepository,
    private perfilRepository: PerfilRepository,
    private idiomaRepository: IdiomaRepository
  ) {}

  obtenerCatalogoMetodoPago(): Observable<CatalogoMetodoPagoModel[]> {
    // const data: CatalogoMetodoPagoModel[] = this.metodoPagoRepository.obtenerLocalMetodosPago();
    // if (data) {
    //   return of(data);
    // } else {
    //   return this.metodoPagoRepository.obtenerCatalogoMetodoPago()
    //     .pipe(
    //       map((data: CatalogoMetodoPagoModel[]) => {
    //         this.metodoPagoRepository.almacenarLocalmenteMetodosPago(data);
    //         return data;
    //       }),
    //       catchError(err => {
    //         return throwError(err);
    //       })
    //     );
    // }

    return this.metodoPagoRepository.obtenerCatalogoMetodoPago()
    .pipe(
      map((data: CatalogoMetodoPagoModel[]) => data), catchError((err => throwError(err)))
    );
  }

  crearOrdenPagoExtra(data: CatalogoMetodoPagoExtraModel): Observable<PagoEntity>{
    return this.metodoPagoRepository.crearOrdenPagoExtra(data).pipe(
      map(resp => resp),
      catchError(err => throwError(err))
    );
  }
  validarPagoExtra(data: string): Observable<boolean>{
    return this.metodoPagoRepository.validarPagoExtra(data).pipe(
      map(resp => resp),
      catchError(err => throwError(err))
    );
  }

  // Metodo pendiente, considererar la actualizaicon para integrar las donaciones o pagos.
  prepararPagoStripe(data: any): Observable<MetodoPagoStripeEntity> {
    return this.metodoPagoRepository.prepararPagoStripe(data).pipe(
      map((data: MetodoPagoStripeEntity) => {
        return data;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }

  // Metodo pendiente, considererar la actualizaicon para integrar las donaciones o pagos.
  prepararPagoPaypal(data: any): Observable<string> {
    return this.metodoPagoRepository.prepararPagoPaypal(data).pipe(
      map((data: string) => {
        return data;
      }),
      catchError((err) => {
        return throwError(err);
      })
    );
  }
}
