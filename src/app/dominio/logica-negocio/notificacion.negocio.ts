import { Injectable } from "@angular/core";
import { AngularFireDatabase } from '@angular/fire/database';
import { CuentaNegocio } from "dominio/logica-negocio/cuenta.negocio";
import { UsuarioModel } from "dominio/modelo/entidades/usuario.model";
@Injectable({ providedIn: 'root' })
export class NoticiaServiceRemoto {

  private notificacionObs: any
  private usuario: UsuarioModel
  private sesionActiva: boolean


  constructor(
    private cuentaNegocio: CuentaNegocio,
    private db: AngularFireDatabase
  ) {
    this.sesionActiva = this.cuentaNegocio.sesionIniciada()
  }

  obtenerUsuario() {
    this.usuario = this.cuentaNegocio.obtenerUsuarioDelLocalStorage()
  }

  obtenerNotificacionesDelUsuario() {
    if (!this.sesionActiva) {
      return
    }
  }
}
