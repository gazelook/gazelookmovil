import { AnuncioSistemaModel } from 'dominio/modelo/entidades/anuncio-sistema.model';
import { AnuncioSistemaRepository } from 'dominio/repositorio/anuncio-sistema.repository';
import { Observable } from 'rxjs';
import { Injectable } from "@angular/core"

@Injectable({ providedIn: 'root' })
export class AnuncioSistemaNegocio {

    constructor(
        private anuncioRepository: AnuncioSistemaRepository
    ) {

    }

    obtenerAnunciosSistema(
        perfil: string,
    ): Observable<AnuncioSistemaModel[]> {
        return this.anuncioRepository.obtenerAnunciosSistema(perfil)
    }

    obtenerAnunciosSistemaPublicitario(
        perfil: string,
    ): Observable<AnuncioSistemaModel[]> {
        return this.anuncioRepository.obtenerAnunciosSistemaPublicitario(perfil)
    }

}