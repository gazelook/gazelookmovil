import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ComentarioIntercambioModel } from 'dominio/modelo/entidades/comentario-intercambio.model';
import { ComentarioIntercambioRepository } from 'dominio/repositorio/comentario-intercambio.repository';

@Injectable({ providedIn: 'root' })
export class ComentarioIntercambioNegocio {

  constructor(
    private comentarioIntercambioRepository: ComentarioIntercambioRepository
  ) { }

  crearComentario(comentario: ComentarioIntercambioModel): Observable<ComentarioIntercambioModel> {
    return this.comentarioIntercambioRepository.crearComentario(comentario)
  }

  eliminarComentario(
    idIntercambio: string,
    coautor?: string,
    idComentario?: string
  ): Observable<string> {
    return this.comentarioIntercambioRepository.eliminarComentario(idIntercambio, coautor, idComentario)
  }

  eliminarMiComentario(
    idIntercambio?: string,
    idCoautor?: string,
  ): Observable<string> {
    return this.comentarioIntercambioRepository.eliminarMiComentario(idIntercambio, idCoautor)
  }

}
