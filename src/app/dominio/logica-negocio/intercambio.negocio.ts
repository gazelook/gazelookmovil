import { CatalogoTipoIntercambioModel } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { Observable } from 'rxjs';
import { AccionEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { FormGroup } from '@angular/forms';
import { IntercambioParams } from 'dominio/modelo/parametros/intercambio-params.interface';
import { IntercambioRepository } from 'dominio/repositorio/intercambio.repository';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { Injectable } from "@angular/core";
@Injectable({ providedIn: 'root' })
export class IntercambioNegocio {

    constructor(
        private intercambioRepository: IntercambioRepository
    ) {

    }

    guardarintercambioActivoEnSessionStorage(intercambio: IntercambioModel) {
        this.intercambioRepository.guardarIntercambioActivoEnSessionStorage(intercambio)
    }

    obtenerIntercambioActivoDelSessionStorage(): IntercambioModel {
        return this.intercambioRepository.obtenerIntercambioActivoDelSessionStorage()
    }

    removerIntercambioActivoDelSessionStorage() {
        this.intercambioRepository.removerintercambioActivoDelSessionStorage()
    }

    crearIntercambio(intercambio: IntercambioModel): Observable<IntercambioModel> {
        return this.intercambioRepository.crearIntercambio(intercambio)
    }

    obtenerInformacionDelIntercambio(idIntercambio: string, idPerfil: string): Observable<IntercambioModel> {
        return this.intercambioRepository.obtenerInformacionDelIntercambio(idIntercambio, idPerfil)
    }

    actualizarIntercambio(intercambio: IntercambioModel): Observable<IntercambioModel> {

        const intercambioSinAlbums: IntercambioModel = this.excluirAlbumsYaRegistradosDelIntercambio(intercambio)
        return this.intercambioRepository.actualizarIntercambio(intercambioSinAlbums)
    }

    eliminarIntercambio(idIntercambio: string, idPerfil: string): Observable<string> {
        return this.intercambioRepository.eliminarIntercambio(idIntercambio, idPerfil)
    }

    cambiarStatusIntercambio(intercambio: IntercambioModel): Observable<string> {
        return this.intercambioRepository.cambiarStatusIntercambio(intercambio)
    }

    tiposIntercambio(): Observable<Array<CatalogoTipoIntercambioModel>> {
        return this.intercambioRepository.tiposIntercambio()
    }


    excluirAlbumsYaRegistradosDelIntercambio(intercambio: IntercambioModel) {
        const adjuntosParaEnviar: Array<AlbumModel> = []
        intercambio.adjuntos.forEach((item, pos) => {
            if (!(item && item._id)) {
                adjuntosParaEnviar.push(item)
            }
        })

        intercambio.adjuntos = adjuntosParaEnviar

        return intercambio
    }



    validarIntercambioActivoSegunAccionCrear(
        codigoTipoIntercambio: CodigosCatalogoTipoIntercambio,
        perfil: PerfilModel,
    ): IntercambioModel {

        let intercambio: IntercambioModel = this.obtenerIntercambioActivoDelSessionStorage()
        if (!intercambio) {


            intercambio = this.crearObjetoVacioDeintercambioParaRetornar(codigoTipoIntercambio, perfil)
            this.guardarintercambioActivoEnSessionStorage(intercambio)
        }

        return intercambio
    }

    crearObjetoVacioDeintercambioParaRetornar(
        codigoTipoIntercambio: CodigosCatalogoTipoIntercambio,
        perfil: PerfilModel,
    ) {


        const intercambio: IntercambioModel = {
            id: '',
            tituloCorto: '',
            titulo: '',
            direccion: {
                descripcion: '',
                pais: {

                },
                localidad: {

                }
            },
            adjuntos: [],
            email: '',
            tipo: {
                codigo: codigoTipoIntercambio
            },
            tipoIntercambiar: {
                codigo: CodigosCatalogoTipoIntercambio.INTERCAMBIO_CUALQUIERA
            },
            perfil: perfil,
            descripcion: '',
            medias: [],
            fechaCreacion: new Date(),

        }



        return intercambio
    }

    asignarValoresDeLosCamposAlIntercambio(
        params: IntercambioParams,
        intercambio: IntercambioModel,
        pais: ItemSelector,
        localidad: ItemSelector,
        intercambioForm: FormGroup,
        eliminarData: boolean,
        codigoAIntercambiar: string
    ): IntercambioModel {

        switch (params.accionEntidad) {
            case AccionEntidad.CREAR:

                intercambio.tituloCorto = intercambioForm.value.tituloCorto
                intercambio.titulo = intercambioForm.value.titulo
                intercambio.direccion.descripcion = intercambioForm.value.ubicacion
                intercambio.direccion.pais.codigo = pais.codigo
                intercambio.direccion.pais.nombre = pais.nombre
                intercambio.direccion.localidad.codigo = localidad.codigo
                intercambio.direccion.localidad.nombre = localidad.nombre
                intercambio.email = intercambioForm.value.email
                intercambio.tipoIntercambiar.codigo = codigoAIntercambiar
                this.guardarintercambioActivoEnSessionStorage(intercambio)
                break
            case AccionEntidad.ACTUALIZAR:
                intercambio.tituloCorto = intercambioForm.value.tituloCorto
                intercambio.titulo = intercambioForm.value.titulo
                intercambio.direccion.descripcion = intercambioForm.value.ubicacion
                intercambio.direccion.pais.codigo = pais.codigo
                intercambio.direccion.pais.nombre = pais.nombre
                intercambio.direccion.localidad.codigo = localidad.codigo
                intercambio.direccion.localidad.nombre = localidad.nombre
                intercambio.email = intercambioForm.value.email
                intercambio.tipoIntercambiar.codigo = codigoAIntercambiar

                break
            default: break;
        }

        if (eliminarData) {
            this.removerIntercambioActivoDelSessionStorage()
        }

        return intercambio
    }

    asignarValoresDeLosCamposAlintercambioParaValidarCambios(
        intercambio: IntercambioModel,
        pais: ItemSelector,
        localidad: ItemSelector,
        intercambioForm: FormGroup
    ): IntercambioModel {
        intercambio.tituloCorto = intercambioForm.value.tituloCorto
        intercambio.titulo = intercambioForm.value.titulo
        intercambio.email = intercambioForm.value.email
        intercambio.direccion.descripcion = intercambioForm.value.ubicacion



        if (!intercambio.direccion.pais) {
            intercambio.direccion.pais = {
                codigo: '',
                nombre: ''
            }
        }

        if (!intercambio.direccion.localidad) {
            intercambio.direccion.localidad = {
                codigo: '',
                nombre: ''
            }
        }

        intercambio.direccion.pais.codigo = pais.codigo
        intercambio.direccion.pais.nombre = pais.nombre

        intercambio.direccion.localidad.codigo = localidad.codigo
        intercambio.direccion.localidad.nombre = localidad.nombre

        return intercambio
    }

    validarSiExistenCambiosEnElIntercambio(
        intercambioAValidar: IntercambioModel,
        accionEntidad: AccionEntidad
    ): boolean {
        try {




            const intercambioOriginal = (accionEntidad === AccionEntidad.CREAR) ?
                this.crearObjetoVacioDeintercambioParaRetornar(
                    intercambioAValidar.tipo.codigo as CodigosCatalogoTipoIntercambio,
                    intercambioAValidar.perfil
                ) :
                this.obtenerIntercambioActivoDelSessionStorage()

       
            
            if (!intercambioOriginal || !intercambioAValidar) {
                return false
            }

            if (intercambioAValidar.tituloCorto.trim() !== intercambioOriginal.tituloCorto.trim()) {
                return true
            }

            if (intercambioAValidar.titulo.trim() !== intercambioOriginal.titulo.trim()) {
                return true
            }

            const direccionOriginal = intercambioOriginal.direccion
            const direccionModificada = intercambioAValidar.direccion


            if (
                !direccionOriginal.pais ||
                !direccionModificada.pais
            ) {
                return false
            }

            if (direccionOriginal.localidad.nombre.trim() !== direccionModificada.localidad.nombre.trim()) {
                return true
            }
            if (direccionOriginal.latitud !== direccionModificada.latitud) {
                return true
            }
            if (direccionOriginal.longitud !== direccionModificada.longitud) {
                return true
            }

            if (direccionOriginal.pais.codigo.trim() !== direccionModificada.pais.codigo.trim()) {
                return true
            }


            if (direccionOriginal.descripcion.trim() !== direccionModificada.descripcion.trim()) {
                return true
            }


            if (intercambioAValidar.descripcion !== intercambioOriginal.descripcion) {
                return true
            }

            if (intercambioAValidar.medias.length !== intercambioOriginal.medias.length) {
                return true
            }

            if (intercambioAValidar.tipoIntercambiar.codigo !== intercambioOriginal.tipoIntercambiar.codigo) {
                return true
            }

            const mediaAValidar = intercambioAValidar.medias[0]
            const mediaOriginal = intercambioOriginal.medias[0]

            if (
                mediaAValidar &&
                mediaOriginal &&
                mediaAValidar.id !== mediaOriginal.id
            ) {
                return true
            }

            return false
        } catch (error) {
            return false
        }
    }

    crearObjetoVacioDeintercambio(
        codigoTipoIntercambio: CodigosCatalogoTipoIntercambio,
        perfil: PerfilModel,
    ) {
        const intercambio: IntercambioModel = this.crearObjetoVacioDeintercambioParaRetornar(codigoTipoIntercambio, perfil)


        this.guardarintercambioActivoEnSessionStorage(intercambio)
    }


    buscarMisIntercambiosTipo(
        tipoIntercambio: CodigosCatalogoTipoIntercambio,
        perfil: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<IntercambioModel>> {
        return this.intercambioRepository.buscarMisIntercambiosTipo(
            tipoIntercambio,
            perfil,
            limite,
            pagina
        )
    }


    buscarIntercambioPorFiltro(
        limite: number,
        pagina: number,
        tipo: CodigosCatalogoTipoIntercambio,
        perfil: string,
        fechaInicial: string,
        fechaFinal: string,
        pais: string,
        titulo: string,
        localidad: string,
        codigoAIntercambiarA: string,
        codigoAIntercambiarB: string,
    ): Observable<PaginacionModel<IntercambioModel>> {

        return this.intercambioRepository.buscarintercambiosPorFiltro(
            limite,
            pagina,
            tipo,
            perfil,
            fechaInicial,
            fechaFinal,
            pais,
            titulo,
            localidad,
            codigoAIntercambiarA,
            codigoAIntercambiarB,
        )
    }


}
