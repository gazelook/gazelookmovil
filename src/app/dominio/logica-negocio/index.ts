import { LandingNegocio } from './landing.negocio';
import { ComentarioNegocio } from './comentario.negocio';
import { PensamientoNegocio } from './pensamiento.negocio';
import { PagoNegocio } from './pago.negocio';
import { ParticipanteAsociacionNegocio } from './participante-asociacion.negocio';
import { UbicacionNegocio } from './ubicacion.negocio';
import { FinanzasNegocio } from './finanzas.negocio';
import { TipoMonedaNegocio } from './moneda.negocio';
import { NoticiaNegocio } from './noticia.negocio';
import { ProyectoNegocio } from './proyecto.negocio';
import { MediaNegocio } from './media.negocio';
import { AlbumNegocio } from './album.negocio';
import { PerfilNegocio } from './perfil.negocio';
import { InternacionalizacionNegocio } from './internacionalizacion.negocio';
import { IdiomaNegocio } from './idioma.negocio';
import { CuentaNegocio } from './cuenta.negocio';


export const logicaNegocio: any[] = [
    InternacionalizacionNegocio,
    CuentaNegocio,
    IdiomaNegocio,
    PerfilNegocio,
    AlbumNegocio,
    MediaNegocio,
    ProyectoNegocio,
    NoticiaNegocio,
    TipoMonedaNegocio,
    FinanzasNegocio,
    UbicacionNegocio,
    ParticipanteAsociacionNegocio,
    PagoNegocio,
    PensamientoNegocio,
    ComentarioNegocio,
    LandingNegocio
];

export * from './cuenta.negocio';
export * from './idioma.negocio';
export * from './internacionalizacion.negocio';
export * from './perfil.negocio';
export * from './media.negocio';
export * from './album.negocio';
export * from './noticia.negocio';
export * from './proyecto.negocio';
export * from './finanzas.negocio';
export * from './moneda.negocio';
export * from './ubicacion.negocio';
export * from './participante-asociacion.negocio';
export * from './pago.negocio';
export * from './pensamiento.negocio';
export * from './comentario.negocio';
export * from './landing.negocio';
