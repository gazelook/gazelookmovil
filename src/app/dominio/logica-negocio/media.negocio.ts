import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { ArchivoModel } from 'dominio/modelo/entidades/archivo.model';
import { CodigosCatalogoArchivosPorDefecto } from 'dominio/../nucleo/servicios/remotos/codigos-catalogos/catalogo-archivos-defeto.enum';
import { CodigosCatalogoTipoMedia } from '@core/servicios/remotos/codigos-catalogos/catalago-tipo-media.enum';
import { Injectable } from "@angular/core"
import { Observable } from 'rxjs'
import { MediaRepository } from 'dominio/repositorio/media.repository'
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';

@Injectable({ providedIn: 'root' })
export class MediaNegocio { 

    constructor(
        private mediaRepository: MediaRepository
    ) {

    }

    obtenerListaArchivosDefaultDelLocal(): ArchivoModel[] {
        return this.mediaRepository.obtenerListaArchivosDefaultDelLocal()
    }

    guardarListaArchivosDefaultEnLocal(archivos: ArchivoModel[]) {
        this.mediaRepository.guardarListaArchivosDefaultEnLocal(archivos)
    }

    obtenerListaArchivosDefault(): Observable<ArchivoModel[]> {
        return this.mediaRepository.obtenerListaArchivosDefault()
    } 

    obtenerListaArchivosDefaultDelApi(): Observable<ArchivoModel[]> {
        return this.mediaRepository.obtenerListaArchivosDefaultDelApi()
    }

    obtenerListaArchivosDefaultDemo(filtro: string): Observable<ArchivoModel[]> {
        return this.mediaRepository.obtenerListaArchivosDefaultDemo(filtro)
    } 

    subirMediaAlServidor(
        imagen: any,
        relacionAspecto: string,
        descripcion: string,
        catalogoTipoMedia: CodigosCatalogoTipoMedia = CodigosCatalogoTipoMedia.TIPO_MEDIA_SIMPLE,
        fileDefault?: boolean,
        catalogoArchivoDefault?: CodigosCatalogoArchivosPorDefecto,
    ): Observable<MediaModel> {
        const body: FormData = new FormData()
        body.append('archivo', imagen.archivo, imagen.nombre)
        body.append('relacionAspecto', relacionAspecto)
        body.append('catalogoMedia', catalogoTipoMedia)
        body.append('descripcion', descripcion)

        if (fileDefault) {
            body.append('fileDefault', (fileDefault) ? 'true' : 'false')
        }
        if (catalogoArchivoDefault) {
            body.append('catalogoArchivoDefault', catalogoArchivoDefault.toString())
        }
        return this.mediaRepository.subirArchivoAlservidor(body)
    }

    subirMediaDNI(data: SubirArchivoData): Observable<MediaModel> {
        const body: FormData = new FormData()
        let formato: string = ''
        if (data.formato) {
            formato = data.formato.split('/')[0] + '.' + data.formato.split('/')[1]
        }

        if (data.archivo) {
            body.append('archivo', data.archivo, ( formato.length > 0 && data.formato)? formato : data.nombreDelArchivo   )
        }

        if (data.catalogoMedia) {
            body.append('catalogoMedia', data.catalogoMedia.toString())
        }

        if (data.descripcion) {
            body.append('descripcion', data.descripcion)   
        }

        if (data.relacionAspecto) {
            body.append('relacionAspecto', data.relacionAspecto)
        }

        if (data.duracion) {
            body.append('duracion', data.duracion)
        }

        if (data.enlace) {
            body.append('enlace', data.enlace)
        }

        if (data.fileDefault) {
            body.append('fileDefault', (data.fileDefault) ? 'true' : 'false')
        }
        
        if (data.catalogoArchivoDefault) {
            body.append('catalogoArchivoDefault', data.catalogoArchivoDefault.toString())
        }

        if (data.estado) {
            body.append('estado', data.estado)
        }
       
        return this.mediaRepository.subirMediaDNI(body)
    }

    subirMedia(data: SubirArchivoData): Observable<MediaModel> {
        const body: FormData = new FormData()
        let formato: string = ''
        if (data.formato) {
            formato = data.formato.split('/')[0] + '.' + data.formato.split('/')[1]
        }

        if (data.archivo) {
            body.append('archivo', data.archivo, ( formato.length > 0 && data.formato)? formato : data.nombreDelArchivo   )
        }

        if (data.catalogoMedia) {
            body.append('catalogoMedia', data.catalogoMedia.toString())
        }

        if (data.descripcion) {
            body.append('descripcion', data.descripcion)   
        }

        if (data.relacionAspecto) {
            body.append('relacionAspecto', data.relacionAspecto)
        }

        if (data.duracion) {
            body.append('duracion', data.duracion)
        }

        if (data.enlace) {
            body.append('enlace', data.enlace)
        }

        if (data.fileDefault) {
            body.append('fileDefault', (data.fileDefault) ? 'true' : 'false')
        }
        
        if (data.catalogoArchivoDefault) {
            body.append('catalogoArchivoDefault', data.catalogoArchivoDefault.toString())
        }

        if (data.estado) {
            body.append('estado', data.estado)
        }
       
        return this.mediaRepository.subirMedia(body)
    }

    subirVariasMedia(medias: any): Observable<MediaModel[]> {
        return this.mediaRepository.subirVariasMedia(medias) 
    }

    obtenerArchivosDefaultPorTipo(
        codigo: CodigosCatalogoArchivosPorDefecto
    ): ArchivoModel[] {
        let archivos = this.obtenerListaArchivosDefaultDelLocal()

        if (!archivos || archivos === null) {
            archivos = []
        }

        return archivos.filter(e => e && e.catalogoArchivoDefault && e.catalogoArchivoDefault === codigo)
    }
}
