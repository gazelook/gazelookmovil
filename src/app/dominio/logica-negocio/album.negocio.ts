import { IntercambioModel } from 'dominio/modelo/entidades/intercambio.model';
import { IntercambioNegocio } from 'dominio/logica-negocio/intercambio.negocio';
import { CodigosCatalogoEstadoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-estado-album.enum';
import { NoticiaNegocio } from 'dominio/logica-negocio/noticia.negocio';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { MediaNegocio } from 'dominio/logica-negocio/media.negocio';
import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { SubirArchivoData } from 'dominio/modelo/subir-archivo.interface';
import { AlbumEntity } from 'dominio/entidades/album.entity';
import { Observable, of } from 'rxjs';
import { CodigosCatalogoTipoPerfil } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-perfiles.enum';
import { AccionEntidad, CodigosCatalogoEntidad } from '@core/servicios/remotos/codigos-catalogos/catalogo-entidad.enum';
import { CodigosCatalogoTipoAlbum } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-album.enum';
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { PerfilNegocio } from 'dominio/logica-negocio/perfil.negocio';
import { AlbumRepository } from 'dominio/repositorio/album.repository'
import { Injectable } from "@angular/core"
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { ProyectoNegocio } from 'dominio/logica-negocio/proyecto.negocio';
import { AlbumParams } from 'dominio/modelo/parametros/album-parametros.interface';
import { PerfilEntity } from 'dominio/entidades/perfil.entity';

@Injectable({ providedIn: 'root' })
export class AlbumNegocio {

	constructor(
		private albumRepository: AlbumRepository,
		private perfilNegocio: PerfilNegocio,
		private proyectoNegocio: ProyectoNegocio,
		private noticiaNegocio: NoticiaNegocio,
		private mediaNegocio: MediaNegocio,
		private intercambioNegocio: IntercambioNegocio
	) {

	}

	// Album del perfil
	guardarAlbumActivoEnSessionStorage(album: AlbumModel) {
		this.albumRepository.guardarAlbumActivoEnSessionStorage(album)
	}

	obtenerAlbumActivoDelSessionStorage(): AlbumModel {
		return this.albumRepository.obtenerAlbumActivoDelSessionStorage()
	}

	removerAlbumActivoDelSessionStorage() {
		this.albumRepository.removerAlbumActivoDelSessionStorage()
	}

	crearObjetoDeAlbumVacio(
		tipoAlbum: CodigosCatalogoTipoAlbum,
	): AlbumModel {
		return {
			portada: {},
			tipo: {
				codigo: tipoAlbum,
			},
			media: [],
			estado: {
				codigo: CodigosCatalogoEstadoAlbum.SIN_CREAR
			}
		}
	}

	obtenerAlbumDelPerfil(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		perfil: PerfilModel
	): AlbumModel {
		let album: AlbumModel
		if (perfil) {
			perfil.album.forEach(item => {
				if (item && item.tipo && item.tipo.codigo === tipoAlbum) {
					album = item
				}
			})
		}
		return album
	}

	obtenerPosDelAlbumEnElPerfil(
		album: AlbumModel,
		perfil: PerfilModel
	): number {
		let pos = -1
		if (perfil) {
			perfil.album.forEach((item, i) => {

				if (item && item.tipo && item.tipo.codigo === album.tipo.codigo) {
					pos = i
				}
			})
		}
		return pos
	}

	insertarAlbumEnPerfilDelSessionStorage(
		codigoPerfil: string,
		album: AlbumModel
	) {
		const perfil: PerfilModel = this.perfilNegocio.validarPerfilModelDelSessionStorage(codigoPerfil)
		if (perfil) {
			let pos = this.obtenerPosDelAlbumEnElPerfil(album, perfil)
			if (pos >= 0) {
				perfil.album[pos] = album
			} else {
				perfil.album.push(album)
			}
			this.perfilNegocio.actualizarPerfilEnUsuarioDelSessionStorage(perfil)
			this.removerAlbumActivoDelSessionStorage()
		}
	}

	insertarAlbumEnPerfilActivoDelSessionStorage(
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		const perfil: PerfilModel = this.perfilNegocio.obtenerPerfilActivoDelSessionStorage()
		if (perfil) {
			let pos = this.obtenerPosDelAlbumEnElPerfil(album, perfil)
			// Si el album existe, se actualizar, caso contrario se inserta
			if (pos >= 0) {
				if (album && album.media && album.media.length > 0) {
					perfil.album[pos] = album
				} else {
					perfil.album.splice(pos, 1)
				}
			} else {
				perfil.album.push(album)
			}
			// actualizar usuario
			this.perfilNegocio.guardarPerfilActivoEnSessionStorage(perfil)
			// Validar si se debe destruir el album
			if (destruirAlbum) {
				this.removerAlbumActivoDelSessionStorage()
			} else {
				this.guardarAlbumActivoEnSessionStorage(album)
			}
		}
	}

	validarAlbumSegunTipoEnSessionStorage(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		perfil: PerfilModel
	) {
		let album: AlbumModel = this.obtenerAlbumDelPerfil(tipoAlbum, perfil)
		// Si el album no existe, se crea y se actualiza el perfil en el usuario
		if (!album) {
			album = this.crearObjetoDeAlbumVacio(tipoAlbum)
			perfil.album.push(album)
			this.perfilNegocio.actualizarPerfilEnUsuarioDelSessionStorage(perfil)
		}
		// Definir album activo
		this.guardarAlbumActivoEnSessionStorage(album)
	}

	validarAlbumEnPerfilActivo(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		perfil: PerfilModel
	): AlbumModel {
		let album: AlbumModel = this.obtenerAlbumDelPerfil(tipoAlbum, perfil)
		// Si el album no existe, se crea y se actualiza el perfil en el usuario
		if (!album) {
			album = this.crearObjetoDeAlbumVacio(tipoAlbum)
			perfil.album.push(album)
			this.perfilNegocio.guardarPerfilActivoEnSessionStorage(perfil)
		}
		// Definir album activo
		this.guardarAlbumActivoEnSessionStorage(album)
		return album
	}

	actualizarAlbum(album: AlbumModel) {
		return this.albumRepository.actualizarAlbum(album)
	}

	// Metodo en revision (Eliminar)
	validarUpdateAlbumSegunEntidadJuntoAccionEntidad(
		entidad: CodigosCatalogoEntidad,
		accionEntidad: AccionEntidad,
		album: AlbumModel,
		codigo: string, // Codigo de la entidad
	) {
		switch (entidad) {
			case CodigosCatalogoEntidad.PERFIL:
				break
			case CodigosCatalogoEntidad.PROYECTO:
				break
			case CodigosCatalogoEntidad.NOTICIA:
				break
			default: break;
		}
	}

	insertarAlbumEnProyectoActivoDelSessionStorage(
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		const proyecto: ProyectoModel = this.proyectoNegocio.obtenerProyectoActivoDelSessionStorage()

		if (proyecto) {
			let pos = -1
			proyecto.adjuntos.forEach((item, i) => {
				if (item && item.tipo.codigo === album.tipo.codigo) {
					pos = i
				}
			})
			// Si el album existe, se actualizar, caso contrario se inserta
			if (pos >= 0) {
				if (album && album.media && album.media.length > 0) {
					proyecto.adjuntos[pos] = album
				} else {
					proyecto.adjuntos.splice(pos, 1)
				}
			} else {
				if (album && album.media && album.media.length > 0) {
					proyecto.adjuntos.push(album)
				}
			}

			// actualizar album
			this.proyectoNegocio.guardarProyectoActivoEnSessionStorage(proyecto)
			// Validar si se debe destruir el album
			if (destruirAlbum) {
				
				this.removerAlbumActivoDelSessionStorage()
			} else {
				
				this.guardarAlbumActivoEnSessionStorage(album)
			}
		}
	}

	insertarAlbumEnNoticiaActivoDelSessionStorage(
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		const noticia: NoticiaModel = this.noticiaNegocio.obtenerNoticiaActiviaDelSessionStorage()

		if (noticia) {
			let pos = -1
			noticia.adjuntos.forEach((item, i) => {
				if (item && item.tipo.codigo === album.tipo.codigo) {
					pos = i
				}
			})
			// Si el album existe, se actualizar, caso contrario se inserta
			if (pos >= 0) {
				if (album && album.media && album.media.length > 0) {
					noticia.adjuntos[pos] = album
				} else {
					noticia.adjuntos.splice(pos, 1)
				}
			} else {
				if (album && album.media && album.media.length > 0) {
					noticia.adjuntos.push(album)
				}
			}

			// actualizar album
			this.noticiaNegocio.guardarNoticiaActivaEnSessionStorage(noticia)
			// Validar si se debe destruir el album
			if (destruirAlbum) {
				this.removerAlbumActivoDelSessionStorage()
			} else {
				this.guardarAlbumActivoEnSessionStorage(album)
			}
		}
	}

	insertarAlbumEnIntercambioActivoDelSessionStorage(
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		const intercambio: IntercambioModel = this.intercambioNegocio.obtenerIntercambioActivoDelSessionStorage()
		if (intercambio) {
			let pos = -1
			intercambio.adjuntos.forEach((item, i) => {
				if (item && item.tipo.codigo === album.tipo.codigo) {
					pos = i
				}
			})
			// Si el album existe, se actualizar, caso contrario se inserta
			if (pos >= 0) {
				if (album && album.media && album.media.length > 0) {
					intercambio.adjuntos[pos] = album
				} else {
					intercambio.adjuntos.splice(pos, 1)
				}
			} else {
				if (album && album.media && album.media.length > 0) {
					intercambio.adjuntos.push(album)
				}
			}
			// actualizar album
			this.intercambioNegocio.guardarintercambioActivoEnSessionStorage(intercambio)
			// Validar si se debe destruir el album
			if (destruirAlbum) {
				this.removerAlbumActivoDelSessionStorage()
			} else {
				this.guardarAlbumActivoEnSessionStorage(album)
			}
		}
	}

	obtenerAlbumDelPerfilSegunTipo(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		perfil: PerfilModel
	): AlbumModel {
		let album: AlbumModel
		if (perfil) {
			perfil.album.forEach(item => {
				if (item.tipo.codigo === tipoAlbum) {
					album = item
				}
			})
		}
		return album
	}

	validarAlbumEnProyectoActivo(
		tipoAlbum: CodigosCatalogoTipoAlbum
	): AlbumModel {
		let album: AlbumModel
		const proyecto = this.proyectoNegocio.obtenerProyectoActivoDelSessionStorage()

		if (proyecto) {
			proyecto.adjuntos.forEach(item => {
				if (item && item.tipo.codigo === tipoAlbum) {
					album = item
				}
			})

			if (!album) {
				album = this.crearObjetoDeAlbumVacio(tipoAlbum)
				proyecto.adjuntos.push(album)
				this.proyectoNegocio.guardarProyectoActivoEnSessionStorage(proyecto)
			}

			this.guardarAlbumActivoEnSessionStorage(album)
		}

		return album
	}


	validarAlbumEnIntercambioActivo(
		tipoAlbum: CodigosCatalogoTipoAlbum
	): AlbumModel {
		let album: AlbumModel
		const intercambio = this.intercambioNegocio.obtenerIntercambioActivoDelSessionStorage()

		if (intercambio) {
			intercambio.adjuntos.forEach(item => {
				if (item && item.tipo.codigo === tipoAlbum) {
					album = item
				}
			})

			if (!album) {
				album = this.crearObjetoDeAlbumVacio(tipoAlbum)
				intercambio.adjuntos.push(album)
				this.intercambioNegocio.guardarintercambioActivoEnSessionStorage(intercambio)
			}

			this.guardarAlbumActivoEnSessionStorage(album)
		}

		return album
	}

	determinarTextosAppBarSegunEntidad(
		entidad: CodigosCatalogoEntidad,
		perfilSeleccionado: PerfilModel,
		codigoTipoAlbum: CodigosCatalogoTipoAlbum
	): {
		nombrePerfil: string,
		subtitulo: string,
		mostrarTextoHome: boolean,
	} {
		const data = {
			nombrePerfil: this.perfilNegocio.obtenerLlaveSegunCodigoPerfil(perfilSeleccionado.tipoPerfil.codigo as CodigosCatalogoTipoPerfil),
			subtitulo: this.obtenerSubTituloDelAppBarSegunTipoDelAlbum(codigoTipoAlbum),
			mostrarTextoHome: false,
		}
		switch (entidad) {
			case CodigosCatalogoEntidad.PERFIL:
				break
			case CodigosCatalogoEntidad.PROYECTO:
				data.mostrarTextoHome = true
				break
			case CodigosCatalogoEntidad.NOTICIA:
				data.mostrarTextoHome = true
				break
			default: break
		}

		return data
	}

	obtenerSubTituloDelAppBarSegunTipoDelAlbum(
		codigoTipoAlbum: CodigosCatalogoTipoAlbum
	) {
		switch (codigoTipoAlbum) {
			case CodigosCatalogoTipoAlbum.GENERAL:
				return 'm2v14texto1'
			case CodigosCatalogoTipoAlbum.PERFIL:
				return 'm2v14texto1'
			case CodigosCatalogoTipoAlbum.LINK:
				return 'm4v8texto1'
			case CodigosCatalogoTipoAlbum.AUDIOS:
				return 'm4v7texto1'
			default: break;
		}
	}

	guardarAlbumEnProyectoActivo(
		album: AlbumModel
	) {
		const proyecto = this.proyectoNegocio.obtenerProyectoActivoDelSessionStorage()
		if (proyecto) {
			let pos = -1
			proyecto.adjuntos.forEach((item, i) => {
				if (album.tipo.codigo === item.tipo.codigo) {
					pos = i
				}
			})

			if (pos >= 0) {
				proyecto.adjuntos[pos] = album
			}

			this.guardarAlbumActivoEnSessionStorage(album)
			this.proyectoNegocio.guardarProyectoActivoEnSessionStorage(proyecto)
		}
	}

	validarActualizacionDelAlbumSegunParams(
		album: AlbumModel,
		params: AlbumParams,
		destruirAlbum: boolean
	) {
		if (album) {
			switch (params.entidad) {
				case CodigosCatalogoEntidad.PERFIL:
					this.validarAccionEntidadPerfil(params.accionEntidad, album, destruirAlbum)
					break
				case CodigosCatalogoEntidad.PROYECTO:
					this.validarAccionEntidadProyecto(params.accionEntidad, album, destruirAlbum)
					break
				case CodigosCatalogoEntidad.NOTICIA:
					this.validarAccionEntidadNoticia(params.accionEntidad, album, destruirAlbum)
					break
				case CodigosCatalogoEntidad.INTERCAMBIO:
					this.validarAccionEntidadIntercambio(params.accionEntidad, album, destruirAlbum)
					break
				default: break;
			}
		}
	}

	validarAccionEntidadPerfil(
		accionEntidad: AccionEntidad,
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		switch (accionEntidad) {
			case AccionEntidad.VISITAR:
				if (destruirAlbum) {
					this.removerAlbumActivoDelSessionStorage()
				}
				break
			case AccionEntidad.CREAR:
				this.insertarAlbumEnPerfilActivoDelSessionStorage(album, destruirAlbum)
				break
			case AccionEntidad.ACTUALIZAR:
				this.insertarAlbumEnPerfilActivoDelSessionStorage(album, destruirAlbum)
			default:
				break
		}
	}

	validarAccionEntidadProyecto(
		accionEntidad: AccionEntidad,
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		switch (accionEntidad) {
			case AccionEntidad.VISITAR:
				if (destruirAlbum) {
					this.removerAlbumActivoDelSessionStorage()
				}
				break
			case AccionEntidad.CREAR:
				this.insertarAlbumEnProyectoActivoDelSessionStorage(album, destruirAlbum)
				break
			case AccionEntidad.ACTUALIZAR:
				this.insertarAlbumEnProyectoActivoDelSessionStorage(album, destruirAlbum)
				break
			default: break
		}
	}

	validarAccionEntidadIntercambio(
		accionEntidad: AccionEntidad,
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		switch (accionEntidad) {
			case AccionEntidad.VISITAR:
				if (destruirAlbum) {
					this.removerAlbumActivoDelSessionStorage()
				}
				break
			case AccionEntidad.CREAR:
				this.insertarAlbumEnIntercambioActivoDelSessionStorage(album, destruirAlbum)
				break
			case AccionEntidad.ACTUALIZAR:
				this.insertarAlbumEnIntercambioActivoDelSessionStorage(album, destruirAlbum)
				break
			default: break
		}
	}

	validarAccionEntidadNoticia(
		accionEntidad: AccionEntidad,
		album: AlbumModel,
		destruirAlbum: boolean
	) {
		switch (accionEntidad) {
			case AccionEntidad.VISITAR:
				if (destruirAlbum) {
					this.removerAlbumActivoDelSessionStorage()
				}
				break
			case AccionEntidad.CREAR:
				this.insertarAlbumEnNoticiaActivoDelSessionStorage(album, destruirAlbum)
				break
			case AccionEntidad.ACTUALIZAR:
				this.insertarAlbumEnNoticiaActivoDelSessionStorage(album, destruirAlbum)
				break
			default: break
		}
	}

	obtenerAlbumDelPerfilEntitySegunTipo(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		perfil: PerfilEntity
	): AlbumEntity {
		let album: AlbumEntity;
		if (perfil) {
			perfil.album.forEach((item) => {
				if (item.tipo.codigo === tipoAlbum) {
					album = item;
				}
			});
		}
		return album;
	}

	// Definir data segun portada
	definirDataItemCircularSegunPortadaAlbum(
		album: AlbumEntity
	): { urlMedia: string; mostrarBoton: boolean; mostrarLoader: boolean } {
		let data = {
			urlMedia: '',
			mostrarBoton: true,
			mostrarLoader: false,
		};
		if (
			album &&
			album.portada &&
			album.portada.principal &&
			album.portada.principal.url.length > 0
		) {
			data.urlMedia = album.portada.principal.url;
			data.mostrarBoton = false;
			data.mostrarLoader = true;
		}

		return data;
	}

	agragarMediaAlAlbum(
		idEntidad: string,
		codigoEntidad: string,
		album: AlbumModel
	): Observable<number> {
		
		return this.albumRepository.agragarMediaAlAlbum(idEntidad, codigoEntidad, album)
	}

	eliminarMediaDelAlbum(
		idEntidad: string,
		codigoEntidad: string,
		album: AlbumModel
	): Observable<number> {
		return this.albumRepository.eliminarMediaDelAlbum(idEntidad, codigoEntidad, album)
	}

	actualizarMediaDelAlbum(
		idEntidad: string,
		codigoEntidad: string,
		album: AlbumModel
	): Observable<number> {
		return this.albumRepository.actualizarMediaDelAlbum(idEntidad, codigoEntidad, album)
	}

	obtenerIdDeLaEntidadSegunCodigo(codigoEntidad: CodigosCatalogoEntidad) {

		
		switch (codigoEntidad) {
			case CodigosCatalogoEntidad.PERFIL:
				const perfil: PerfilModel = this.perfilNegocio.obtenerPerfilActivoDelSessionStorage()
				return perfil._id
			case CodigosCatalogoEntidad.PROYECTO:
				const proyecto: ProyectoModel = this.proyectoNegocio.obtenerProyectoActivoDelSessionStorage()
				return proyecto.id
			case CodigosCatalogoEntidad.NOTICIA:
				const noticia: NoticiaModel = this.noticiaNegocio.obtenerNoticiaActiviaDelSessionStorage()
				return noticia.id
			case CodigosCatalogoEntidad.INTERCAMBIO:
				const intercambio: IntercambioModel = this.intercambioNegocio.obtenerIntercambioActivoDelSessionStorage()
				return intercambio.id
			default: break;
		}
	}

	async subirMedia(
		data: SubirArchivoData,
		params: AlbumParams,
		album: AlbumModel
	): Promise<MediaModel> {
		try {
			
			const media: MediaModel = await this.mediaNegocio.subirMedia(data).toPromise()
	
			
			// Validar si hacer update del album o no
			if (params.accionEntidad === AccionEntidad.ACTUALIZAR) {
				const idEntidad = this.obtenerIdDeLaEntidadSegunCodigo(params.entidad)
				const status: number = await this.agragarMediaAlAlbum(
					idEntidad,
					params.entidad,
					{
						_id: album._id,
						media: [
							media
						]
					}
				).toPromise()
				

				if (status !== 201 && status !== 200) {
					throw new Error('')
				}
			}

			return media
		} catch (error) {
			return null
		}
	}

	async subirMediaDNI(
		data: SubirArchivoData
	): Promise<MediaModel> {
		try {
			
			const media: MediaModel = await this.mediaNegocio.subirMediaDNI(data).toPromise()
			

			return media
		} catch (error) {
			return null
		}
	}
	

	async borrarMedia(
		idMedia: string,
		params: AlbumParams,
		album: AlbumModel
	): Promise<boolean> {
		try {
			const idEntidad = this.obtenerIdDeLaEntidadSegunCodigo(params.entidad)

			const status: number = await this.eliminarMediaDelAlbum(
				idEntidad,
				params.entidad,
				{
					_id: album._id,
					media: [
						{
							id: idMedia
						}
					]
				}
			).toPromise()



			if (status !== 200 && status !== 201) {
				throw new Error('')
			}

			return true
		} catch (error) {
			return false
		}
	}

	async asignarPortada(
		idMedia: string,
		params: AlbumParams,
		album: AlbumModel
	): Promise<boolean> {
		try {
			const idEntidad = this.obtenerIdDeLaEntidadSegunCodigo(params.entidad)
			const status: number = await this.agragarMediaAlAlbum(
				idEntidad,
				params.entidad,
				{
					_id: album._id,
					portada: {
						id: idMedia
					}
				}
			).toPromise()

			if (status !== 200 && status !== 201) {
				throw new Error('')
			}

			return true
		} catch (error) {
			return false
		}
	}

	async removerPortada(
		idMedia: string,
		params: AlbumParams,
		album: AlbumModel
	): Promise<boolean> {
		try {
			const idEntidad = this.obtenerIdDeLaEntidadSegunCodigo(params.entidad)
			const status: number = await this.eliminarMediaDelAlbum(
				idEntidad,
				params.entidad,
				{
					_id: album._id,
					portada: {
						id: idMedia
					}
				}
			).toPromise()


			if (status !== 200) {
				throw new Error('')
			}

			return true
		} catch (error) {
			return false
		}
	}

	async actualizarDescripcionDeLaMedia(
		idMedia: string,
		descripcion: string,
		params: AlbumParams,
		album: AlbumModel
	): Promise<boolean> {
		try {
			const idEntidad = this.obtenerIdDeLaEntidadSegunCodigo(params.entidad)
			const status: number = await this.actualizarMediaDelAlbum(
				idEntidad,
				params.entidad,
				{
					_id: album._id,
					media: [
						{
							id: idMedia,
							descripcion: descripcion
						}
					]
				}
			).toPromise()

			if (status !== 200) {
				throw new Error('')
			}

			return true
		} catch (error) {
			return false
		}
	}

	obtenerAlbumDeListaSegunTipo(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		albums: Array<AlbumModel>
	): AlbumModel {
		let album: AlbumModel
		if (albums) {
			albums.forEach(item => {
				if (item && item.tipo.codigo === tipoAlbum) {
					album = item
				}
			})
		}
		
	
		
		return album
	}

	obtenerAlbumPredeterminadoDeLista(
		albums: Array<AlbumModel>
	): AlbumModel {
		let album: AlbumModel
		if (albums) {
			albums.forEach(item => {
				
				if (item && item.predeterminado) {
					album = item
				}
			})
		}
		return album
	}

	validarAlbumEnNoticiaActiva(
		tipoAlbum: CodigosCatalogoTipoAlbum,
		guardarAlbumActivo: boolean,
		predeterminado: boolean = false
	): AlbumModel {
		let album: AlbumModel
		const noticia = this.noticiaNegocio.obtenerNoticiaActiviaDelSessionStorage()

		try {
			const indexUno = noticia.adjuntos.findIndex(e => e.tipo.codigo === tipoAlbum)
			if (indexUno < 0) {
				album = this.crearObjetoDeAlbumVacio(tipoAlbum)
				noticia.adjuntos.push(album)
			}

			const indexDos = noticia.adjuntos.findIndex(e => e.tipo.codigo === tipoAlbum)
			album = noticia.adjuntos[indexDos]
			album.predeterminado = predeterminado
			noticia.adjuntos[indexDos] = album

			this.noticiaNegocio.guardarNoticiaActivaEnSessionStorage(noticia)

			if (guardarAlbumActivo) {
				this.guardarAlbumActivoEnSessionStorage(album)
			}

			return album
		} catch (error) {
			return album
		}
	}

	actualizarParametrosDelAlbum(
		idEntidad: string,
		codigoEntidad: string,
		album: AlbumModel
	): Observable<AlbumModel> {
		return this.albumRepository.actualizarParametrosDelAlbum(idEntidad, codigoEntidad, album)
	}

	agregarAlbumEnEntidad(
		idEntidad: string,
		codigoEntidad: string,
		album: AlbumModel
	): Observable<AlbumModel> {
		return this.albumRepository.agregarAlbumEnEntidad(idEntidad, codigoEntidad, album)
	}

	removerAlbumSegunTipoDeLaEntidad(
		entidad: CodigosCatalogoEntidad,
		tipoAlbum: CodigosCatalogoTipoAlbum
	) {
		switch (entidad) {
			case CodigosCatalogoEntidad.PERFIL:
				const perfil = this.perfilNegocio.obtenerPerfilActivoDelSessionStorage()
				if (perfil && perfil.album) {
					const index = perfil.album.findIndex(e => e.tipo.codigo === tipoAlbum)
					if (index >= 0) {
						perfil.album.splice(index, 1)
						this.perfilNegocio.guardarPerfilActivoEnSessionStorage(perfil)
					}
				}
				break
			case CodigosCatalogoEntidad.PROYECTO:
				const proyecto = this.proyectoNegocio.obtenerProyectoActivoDelSessionStorage()
				if (proyecto && proyecto.adjuntos) {
					const indexDos = proyecto.adjuntos.findIndex(e => e.tipo.codigo === tipoAlbum)
					if (indexDos >= 0) {
						proyecto.adjuntos.splice(indexDos, 1)
						this.proyectoNegocio.guardarProyectoActivoEnSessionStorage(proyecto)
					}
				}
				break
			case CodigosCatalogoEntidad.NOTICIA:
				const noticia = this.noticiaNegocio.obtenerNoticiaActiviaDelSessionStorage()
				if (noticia && noticia.adjuntos) {
					const indexTres = noticia.adjuntos.findIndex(e => e.tipo.codigo === tipoAlbum)
					if (indexTres >= 0) {
						noticia.adjuntos.splice(indexTres, 1)
						this.noticiaNegocio.guardarNoticiaActivaEnSessionStorage(noticia)
					}
				}
				break
			case CodigosCatalogoEntidad.INTERCAMBIO:
				const intercambio = this.intercambioNegocio.obtenerIntercambioActivoDelSessionStorage()
				if (intercambio && intercambio.adjuntos) {
					const indexTres = intercambio.adjuntos.findIndex(e => e.tipo.codigo === tipoAlbum)
					if (indexTres >= 0) {
						intercambio.adjuntos.splice(indexTres, 1)
						this.intercambioNegocio.guardarintercambioActivoEnSessionStorage(intercambio)
					}
				}
				break
			default: break;
		}
	}

	removerAlbumSegunElEstado(
		params: AlbumParams,
		album: AlbumModel,
		estado: CodigosCatalogoEstadoAlbum
	) {
		if (
			params.accionEntidad === AccionEntidad.CREAR &&
			album.estado &&
			album.estado.codigo &&
			album.estado.codigo === estado
		) {
			this.removerAlbumSegunTipoDeLaEntidad(
				params.entidad,
				album.tipo.codigo as CodigosCatalogoTipoAlbum
			)
		}
	}

	actualizarEstadoDelAlbum(
		album: AlbumModel,
		estado: CodigosCatalogoEstadoAlbum
	): AlbumModel {
		if (album.estado) {
			album.estado.codigo = estado
		}

		return album
	}

	obtenerUrlMedaDeLaPortada(
		album: AlbumModel
	): string {
		try {
			if (!album || !album.portada) {
				throw new Error('')
			}

			if (album.portada.miniatura) {
				return album.portada.miniatura.url
			}

			if (
				album.portada.principal &&
				album.tipo.codigo !== CodigosCatalogoTipoAlbum.LINK
			) {
				return album.portada.principal.url
			}

			return ''
		} catch (error) {
			return ''
		}
	}

}
