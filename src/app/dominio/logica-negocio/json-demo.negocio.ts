import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { Observable } from 'rxjs';
import { JsonDemoRepository } from 'dominio/repositorio/json-demo.repository';
import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
})
export class JsonDemoNegocio {

    constructor(
        private jsonDemoRepository: JsonDemoRepository
    ) { }

    obtenerContactosDemo(): Observable<any> {
        return this.jsonDemoRepository.obtenerContactosDemo()
    }

    obtenerPensamientoDemo(): Observable<PensamientoModel[]> {
        return this.jsonDemoRepository.obtenerPensamientoDemo()
    }

    obtenerProyectosDemo(): Observable<any> {
        return this.jsonDemoRepository.obtenerProyectosDemo()
    }
    obtenerPerfilGeneralDemo(): Observable<any> {
        return this.jsonDemoRepository.obtenerPerfilGeneralDemo()
    }

    obtenerNoticiasDemo(): Observable<any> {
        return this.jsonDemoRepository.obtenerNoticiasDemo()
    }

    obtenerProyectoDemo(): Observable<ProyectoModel> {
        return this.jsonDemoRepository.obtenerProyectoDemo()
    }

    obtenerNoticiaDemo(): Observable<NoticiaModel> {
        return this.jsonDemoRepository.obtenerNoticiaDemo()
    }

    obtenerComentarios(): Observable<ComentarioModel[]> {
        return this.jsonDemoRepository.obtenerComentarios()
    }
}
