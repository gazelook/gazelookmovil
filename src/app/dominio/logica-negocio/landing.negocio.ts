import { Injectable } from '@angular/core';
import { LandingRepository } from 'dominio/repositorio/landing.repository';
import { Observable, throwError } from '@env/node_modules/rxjs';
import { catchError, map } from '@env/node_modules/rxjs/operators';
import { RespuestaRemota } from '@core/util/respuesta';

@Injectable({ providedIn: 'root' })
export class LandingNegocio {
  constructor(private landingRepository: LandingRepository) {}

  public mostrarTextoLandingSegundaPagina(): Observable<number> {
    return this.landingRepository.mostrarTextoLandingSegundaPagina().pipe(
      map((data) =>  data),
      catchError((err) => throwError(err))
    );
  }
}
