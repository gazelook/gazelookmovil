import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { DispositivoModel } from 'dominio/modelo/entidades/dispositivo.model';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";

export interface DispositivoEntity {
  _id?: string
  estado?: CatalogoEstadoEntity //CatalogoEstado
  fechaCreacion?: Date
  fechaActualizacion?: Date
  tokenAcceso?: string
  tokenNotificacion?: string
  nombre?: string
  tipo?: string
}

@Injectable({ providedIn: 'root' })
export class DispositivoEntityMapperService extends MapedorService<DispositivoEntity, DispositivoModel> {
  constructor(

  ) {
    super();
  }

  protected map(entity: DispositivoEntity): DispositivoModel {
    if (entity) {
      const model: DispositivoModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      return model
    }

    return null
  }
}
