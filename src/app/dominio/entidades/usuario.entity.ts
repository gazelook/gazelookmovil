import {DocumentosUsuarioEntity} from 'dominio/entidades/documentos-usuario.entity';
import {CatalogoEstadoEntityMapperService} from 'dominio/entidades/catalogos/catalogo-evento-notificacion';
import {DireccionEntity, DireccionEntityMapperService} from 'dominio/entidades/direccion.entity';
import {PerfilEntity, PerfilEntityMapperServicePerfil} from 'dominio/entidades/perfil.entity';
import {TransaccionEntity} from 'dominio/entidades/transaccion.entity';
import {SuscripcionEntity} from 'dominio/entidades/suscripcion.entity';
import {DispositivoEntity, DispositivoEntityMapperService} from 'dominio/entidades/dispositivo.entity';
import {CatalogoIdiomaEntity} from 'dominio/entidades/catalogos//catalogo-idioma.entity';
import {CatalogoEstadoEntity} from 'dominio/entidades/catalogos//catalogo-estado.entity';
import {PagoFacturacionEntity, CatalogoMetodoPagoEntity} from 'dominio/entidades/catalogos//catalogo-metodo-pago.entity';
import {RolSistemaEntity} from 'dominio/entidades/rol-sistema.entity';
import {Injectable} from '@angular/core';
import {MapedorService} from '@core/base/mapeador.interface';
import {UsuarioModel} from 'dominio/modelo/entidades/usuario.model';
import {CatalogoTipoMonedaModel} from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';

export interface UsuarioEntity {
  _id?: string;
  email?: string,
  nombre?: string
  fechaNacimiento?: Date,
  contrasena?: string,
  idioma?: CatalogoIdiomaEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date;
  emailVerificado?: boolean,
  aceptoTerminosCondiciones?: boolean,
  estado?: CatalogoEstadoEntity,
  perfilGrupo?: boolean,
  menorEdad?: boolean,
  perfiles?: Array<PerfilEntity>,
  emailResponsable?: string,
  nombreResponsable?: string
  responsableVerificado?: boolean,
  transacciones?: Array<TransaccionEntity>,
  suscripciones?: Array<SuscripcionEntity>,
  dispositivos?: Array<DispositivoEntity>,
  rolSistema?: Array<RolSistemaEntity>
  metodoPago?: CatalogoMetodoPagoEntity,
  datosFacturacion?: PagoFacturacionEntity,
  direccion?: DireccionEntity,
  nuevaContrasena?: string,
  idDispositivo?: string
  documentosUsuario?: Array<DocumentosUsuarioEntity>
  monedaRegistro?: CatalogoTipoMonedaModel
  direccionDomiciliaria?: string
  documentoIdentidad?: string
  pagoNoConfirmado?: boolean
  autorizacionCodePaymentez?: string
}

@Injectable({providedIn: 'root'})
export class UsuarioEntityMapperService extends MapedorService<UsuarioEntity, UsuarioModel> {

  constructor(
    private perfilMapper: PerfilEntityMapperServicePerfil,
    private direccionEntityMapperService: DireccionEntityMapperService,
    private dispositivoEntityMapperService: DispositivoEntityMapperService,
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService
  ) {
    super();
  }

  protected map(entity: UsuarioEntity): UsuarioModel {
    if (entity) {
      return {
        id: entity._id,
        email: entity.email,
        perfilGrupo: entity.perfilGrupo,
        perfiles: this.perfilMapper.transform(entity.perfiles),
        direccion: this.direccionEntityMapperService.transform(entity.direccion),
        direccionDomiciliaria: entity.direccionDomiciliaria,
        documentoIdentidad: entity.documentoIdentidad,
        fechaNacimiento: entity.fechaNacimiento,
        dispositivos: this.dispositivoEntityMapperService.transform(entity.dispositivos),
        estado: this.catalogoEstadoEntityMapperService.transform(entity.estado),
        fechaCreacion: entity.fechaCreacion

      };
    }
    return null;
  }
}

