import { UsoDelEstiloEntity, UsoDelEstiloEntityMapperService } from 'dominio/entidades/uso-del-estilo.entity';
import { EstiloModel } from 'dominio/modelo/entidades/estilo.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { MediaEntity, MediaEntityMapperService } from "dominio/entidades/media.entity";
import { CatalogoColorEntity, CatalogoColorMapperService } from "dominio/entidades/catalogos/catalogo-color.entity";
import { CatalogoEstiloEntity, CatalogoEstiloMapperService } from "dominio/entidades/catalogos/catalogo-estilo.entity";
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';

export interface EstiloEntity {
    _id?: string
    estado?: CatalogoEstadoEntity //CatalogoEstado
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?: string
    media?: MediaEntity
    color?: CatalogoColorEntity
    tipo?: CatalogoEstiloEntity
    usoDelEstilo?: UsoDelEstiloEntity
}

@Injectable({ providedIn: 'root' })
export class EstiloEntityMapperService extends MapedorService<EstiloEntity, EstiloModel> {

    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
        private mediaEntityMapperService: MediaEntityMapperService,
        private catalogoColorMapperService: CatalogoColorMapperService,
        private catalogoEstiloMapperService: CatalogoEstiloMapperService,
        private usoDelEstiloEntityMapperService: UsoDelEstiloEntityMapperService,
    ) {
        super()
    }

    protected map(entity: EstiloEntity): EstiloModel {

        if (entity) {
            const model: EstiloModel = {}

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }

            if (entity.codigo) {
                model.codigo = entity.codigo
            }

            if (entity.media) {
                model.media = this.mediaEntityMapperService.transform(entity.media)
            }

            if (entity.color) {
                model.color = this.catalogoColorMapperService.transform(entity.color)
            }

            if (entity.tipo) {
                model.tipo = this.catalogoEstiloMapperService.transform(entity.tipo)
            }

            if (entity.usoDelEstilo) {
                model.usoDelEstilo = this.usoDelEstiloEntityMapperService.transform(entity.usoDelEstilo)
            }
            return model
        }
        return null
    }
}
