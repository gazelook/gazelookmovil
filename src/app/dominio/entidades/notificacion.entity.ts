import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEntidadModelMapperService } from 'dominio/modelo/catalogos/catalogo-entidad.model';
import { NotificacionModel } from 'dominio/modelo/entidades/notificacion.model';
import { CatalogoAccionEntity, CatalogoAccionEntityMapperService } from 'dominio/entidades/catalogos/catalogo-accion.entity';
import { CatalogoEntidadEntity } from 'dominio/entidades/catalogos/catalogo-entidad.entity';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { CatalogoEventoNotificacionEntity } from 'dominio/entidades/catalogos/catalogo-evento-notificacion';
import { PerfilEntity, PerfilEntityMapperService } from 'dominio/entidades/perfil.entity';


export interface NotificacionEntity {
  fechaCreacion?: Date
  fechaActualizacion?: Date
  id?: string
  entidad?: CatalogoEntidadEntity
  evento?: CatalogoEventoNotificacionEntity
  data?: any
  listaPerfiles?: Array<PerfilEntity>
  estado?: CatalogoEstadoEntity
  accion?: CatalogoAccionEntity
}

@Injectable({ providedIn: 'root' })
export class NotificacionEntityMapperService extends MapedorService<NotificacionEntity, NotificacionModel> {

  constructor(
    private catalogoEntidadMapperService: CatalogoEntidadModelMapperService,
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private perfilEntityMapperService: PerfilEntityMapperService,
    private catalogoAccionEntityMapperService: CatalogoAccionEntityMapperService

  ) {
    super()
  }

  protected map(entity: NotificacionEntity): NotificacionModel {


    if (entity) {
      const model: NotificacionModel = {}

      if (entity.id) {
        model.id = entity.id
      }

      if (entity.entidad) {
        model.entidad = this.catalogoEntidadMapperService.transform(entity.entidad)
      }

      if (entity.evento) {
        model.evento = this.catalogoEstadoEntityMapperService.transform(entity.evento)
      }

      if (entity.data) {
        model.data = entity.data
      }

      if (entity.listaPerfiles) {
        model.listaPerfiles = this.perfilEntityMapperService.transform(entity.listaPerfiles)
      }

      if (entity.estado) {

        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.accion) {

        model.accion = this.catalogoAccionEntityMapperService.transform(entity.accion)
      }

      return model
    }

    return null
  }

}
