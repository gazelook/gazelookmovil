import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ConfiguracionEstiloModel } from 'dominio/modelo/entidades/configuracion-estilo.model';
import { CatalogoConfiguracionEntity } from "dominio/entidades/catalogos/catalogo-configuracion.entity";
import { CatalogoEntidadEntity, CatalogoEntidadMapperService } from "dominio/entidades/catalogos/catalogo-entidad.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoPaticipanteConfiguracionEntity, CatalogoPaticipanteConfiguracionEntityMapperService } from "dominio/entidades/catalogos/catalogo-participante-configuracion.entity";
import { EstiloEntity, EstiloEntityMapperService } from "dominio/entidades/estilo.entity";
import { MediaEntity, MediaEntityMapperService } from 'dominio/entidades/media.entity';

export interface ConfiguracionEstiloEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  entidad?: CatalogoEntidadEntity
  silenciada?: boolean
  tonoNotificacion?: MediaEntity
  estilos?: Array<EstiloEntity>
  tipo?: CatalogoConfiguracionEntity
  catalogoPaticipante?: CatalogoPaticipanteConfiguracionEntity
}

@Injectable({ providedIn: 'root' })
export class ConfiguracionEstiloEntityMapperService extends MapedorService<ConfiguracionEstiloEntity, ConfiguracionEstiloModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private catalogoEntidadMapperService: CatalogoEntidadMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
    private estiloEntityMapperService: EstiloEntityMapperService,
    private catalogoPaticipanteConfiguracionEntityMapperService: CatalogoPaticipanteConfiguracionEntityMapperService,
  ) {
    super()
  }

  protected map(entity: ConfiguracionEstiloEntity): ConfiguracionEstiloModel {
    if (entity) {
      const model: ConfiguracionEstiloModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.entidad) {
        model.entidad = this.catalogoEntidadMapperService.transform(entity.entidad)
      }

      if (entity.silenciada) {
        model.silenciada = entity.silenciada
      }

      if (entity.tonoNotificacion) {
        model.tonoNotificacion = this.mediaEntityMapperService.transform(entity.tonoNotificacion)
      }
      if (entity.estilos) {
        model.estilos = this.estiloEntityMapperService.transform(entity.estilos)
      }

      if (entity.catalogoPaticipante) {
        model.catalogoPaticipante = this.catalogoPaticipanteConfiguracionEntityMapperService.transform(entity.catalogoPaticipante)
      }

      return model
    }

    return null
  }
}
