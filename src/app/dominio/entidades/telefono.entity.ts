import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoPaisEntity } from "dominio/entidades/catalogos/catalogo-pais.entity";

export interface TelefonoEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity, // CatogoEstado
    numero?: string,
    pais?: CatalogoPaisEntity, // CatalogoPais
}