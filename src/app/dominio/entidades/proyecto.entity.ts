import {Injectable} from '@angular/core';
import {MapedorService} from '@core/base/mapeador.interface';
import {ProyectoModel} from 'dominio/modelo/entidades/proyecto.model';
import {AlbumEntity, AlbumEntityMapperService} from 'dominio/entidades/album.entity';
import {
  CatalogoEstadoEntity,
  CatalogoEstadoEntityMapperService
} from 'dominio/entidades/catalogos/catalogo-estado.entity';
import {CatalogoIdiomaEntity} from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import {CatalogoTipoMonedaEntity, CatalogoTipoMonedaEntityMapperService} from 'dominio/entidades/catalogos/catalogo-tipo-moneda.entity';
import {
  CatalogoTipoProyectoEntity,
  CatalogoTipoProyectoEntityMapperService
} from 'dominio/entidades/catalogos/catalogo-tipo-proyecto.entity';
import {ComentarioEntity, ComentarioEntityMapperService} from 'dominio/entidades/comentario.entity';
import {DireccionEntity, DireccionEntityMapperService} from 'dominio/entidades/direccion.entity';
import {EstrategiaEntity} from 'dominio/entidades/estrategia.entity';
import {MediaEntity, MediaEntityMapperService} from 'dominio/entidades/media.entity';
import {
  ParticipanteProyectoEntity,
  ParticipanteProyectoEntityMapperService
} from 'dominio/entidades/participante-proyecto.entity';
import {PerfilEntity, PerfilEntityMapperService} from 'dominio/entidades/perfil.entity';
import {VotoProyectoEntity} from 'dominio/entidades/votoProyecto.entity';

export interface ProyectoEntity {
  _id?: string;
  estado?: CatalogoEstadoEntity;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  totalVotos?: number;
  actualizado?: boolean;
  participantes?: Array<ParticipanteProyectoEntity>;
  recomendadoAdmin?: boolean;
  valorEstimado?: number;
  tipo?: CatalogoTipoProyectoEntity;
  perfil?: PerfilEntity;
  adjuntos?: Array<AlbumEntity>;
  direccion?: DireccionEntity;
  votos?: Array<VotoProyectoEntity>;
  traducciones?: Array<TraduccionProyectoEntity>;
  estrategia?: EstrategiaEntity;
  comentarios?: Array<ComentarioEntity>;
  moneda?: CatalogoTipoMonedaEntity;
  medias?: Array<MediaEntity>;
  voto?: boolean;
  transferenciaActiva?: string;
  montoEntregado?: number;
  montoFaltante?: number;
}

export interface TraduccionProyectoEntity {
  _id?: string;
  titulo?: string;
  tituloCorto?: string;
  descripcion?: string;
  tituloOriginal?: string;
  tituloCortoOriginal?: string;
  descripcionOriginal?: string;
  tags?: Array<string>;
  idioma?: CatalogoIdiomaEntity;
  original?: boolean;
}

@Injectable({providedIn: 'root'})
export class ProyectoEntityMapperService extends MapedorService<ProyectoEntity, ProyectoModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
    private catalogoTipoProyectoEntityMapperService: CatalogoTipoProyectoEntityMapperService,
    private perfilEntityMapperService: PerfilEntityMapperService,
    private catalogoTipoMonedaEntityMapperService: CatalogoTipoMonedaEntityMapperService,
    private albumEntityMapperService: AlbumEntityMapperService,
    private direccionEntityMapperService: DireccionEntityMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
    private participanteProyectoEntityMapperService: ParticipanteProyectoEntityMapperService,
    private comentarioEntityMapperService: ComentarioEntityMapperService
  ) {
    super();
  }

  protected map(entity: ProyectoEntity): ProyectoModel {
    if (entity) {
      const model: ProyectoModel = {};
 
      if (entity._id) {
        model.id = entity._id;
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado);
      }

      model.actualizado = entity.actualizado;

      if (entity.tipo) {
        model.tipo = this.catalogoTipoProyectoEntityMapperService.transform(entity.tipo);
      }

      if (entity.totalVotos) {
        model.totalVotos = entity.totalVotos;
      }

      model.recomendadoAdmin = (entity.recomendadoAdmin);

      if (entity.valorEstimado) {
        model.valorEstimado = entity.valorEstimado;
      }

      if (entity.perfil) {
        model.perfil = this.perfilEntityMapperService.transform(entity.perfil);
      }

      if (entity.moneda) {
        model.moneda = this.catalogoTipoMonedaEntityMapperService.transform(entity.moneda);
      }


      if (entity.traducciones && entity.traducciones.length > 0) {
        for (const traduccion of entity.traducciones) {
          if (traduccion.original && entity.traducciones.length > 1) {
            model.descripcionOriginal = traduccion.descripcion;
            model.tituloOriginal = traduccion.titulo;
            model.tituloCortoOriginal = traduccion.tituloCorto;

          }
          if (!traduccion.original || entity.traducciones.length === 1) {
            model.descripcion = traduccion.descripcion;
            model.titulo = traduccion.titulo;
            model.tituloCorto = traduccion.tituloCorto;
          }
        }
      }


      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion;
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion;
      }

      if (entity.participantes) {
        model.participantes = this.participanteProyectoEntityMapperService.transform(entity.participantes);
      }

      if (entity.adjuntos) {
        model.adjuntos = this.albumEntityMapperService.transform(entity.adjuntos);
      }

      if (entity.direccion) {
        model.direccion = this.direccionEntityMapperService.transform(entity.direccion);
      }

      if (entity.votos) {
        // votos ?: Array<VotoProyectoEntity>;
      }

      if (entity.estrategia) {
        // estrategia ?: EstrategiaEntity;
      }

      if (entity.comentarios) {
        // comentarios ?: Array<ComentarioEntity>;
        model.comentarios = this.comentarioEntityMapperService.transform(entity.comentarios);
      }

      if (entity.medias) {
        model.medias = this.mediaEntityMapperService.transform(entity.medias);
      }

      model.voto = entity.voto;

      if (entity.transferenciaActiva) {
        model.transferenciaActiva = entity.transferenciaActiva;
      }
      if (entity.montoEntregado) {
        model.montoEntregado = entity.montoEntregado;
      }
      if (entity.montoFaltante) {
        model.montoFaltante = entity.montoFaltante;
      }

      return model;
    }

    return null;
  }

}

@Injectable({providedIn: 'root'})
export class ProyectoEntityMapperServiceParaComentarios extends MapedorService<ProyectoEntity, ProyectoModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
    private catalogoTipoProyectoEntityMapperService: CatalogoTipoProyectoEntityMapperService,
  ) {
    super();
  }

  protected map(entity: ProyectoEntity): ProyectoModel {
    if (entity) {
      const model: ProyectoModel = {};

      if (entity._id) {
        model.id = entity._id;
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado);
      }

      if (entity.tipo) {
        model.tipo = this.catalogoTipoProyectoEntityMapperService.transform(entity.tipo);
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        model.descripcion = entity.traducciones[0].descripcion;
        model.titulo = entity.traducciones[0].titulo;
        model.tituloCorto = entity.traducciones[0].tituloCorto;
      }

      return model;
    }

    return null;
  }
}
