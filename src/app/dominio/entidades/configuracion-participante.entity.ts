import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ConfiguracionEstiloParticipanteModel } from 'dominio/modelo/entidades/configuracion-participante.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { ConfiguracionEstiloEntity } from 'dominio/entidades/configuracion-estilo.entity';
import { ParticipanteAsociacionEntity, ParticipanteAsociacionMapperService } from "dominio/entidades/participante-asociacion.entity";

export interface ConfiguracionEstiloParticipanteEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  propietario?: ParticipanteAsociacionEntity
  asignado?: ParticipanteAsociacionEntity
  configuracionEstilo?: ConfiguracionEstiloEntity
}

@Injectable({ providedIn: 'root' })
export class ConfiguracionEstiloParticipanteEntityMapperService extends MapedorService<ConfiguracionEstiloParticipanteEntity, ConfiguracionEstiloParticipanteModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private participanteAsociacionMapperService: ParticipanteAsociacionMapperService
  ) {
    super()
  }

  protected map(entity: ConfiguracionEstiloParticipanteEntity): ConfiguracionEstiloParticipanteModel {
    if (entity) {
      const configuracionesEstiloParti: ConfiguracionEstiloParticipanteModel = {}

      if (entity._id) {
        configuracionesEstiloParti.id = entity._id
      }

      if (entity.estado) {
        configuracionesEstiloParti.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.propietario) {
        configuracionesEstiloParti.estado = this.participanteAsociacionMapperService.transform(entity.propietario)
      }

      if (entity.asignado) {
        configuracionesEstiloParti.asignado = this.participanteAsociacionMapperService.transform(entity.asignado)
      }

      if (entity.estado) {
        configuracionesEstiloParti.estado = this.estadoEntityMapper.transform(entity.estado)
      }
      return configuracionesEstiloParti
    }
    return null
  }
}
