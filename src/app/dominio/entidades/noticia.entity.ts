import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';
import { AlbumEntity, AlbumEntityMapperService } from "dominio/entidades/album.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { DireccionEntity, DireccionEntityMapperService } from 'dominio/entidades/direccion.entity';
import { MediaEntity, MediaEntityMapperService } from 'dominio/entidades/media.entity';
import { PerfilEntity, PerfilEntityMapperService } from "dominio/entidades/perfil.entity";
import { VotoNoticiaEntity } from "dominio/entidades/voto-noticia.entity";

export interface NoticiaEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  direccion?: DireccionEntity
  autor?: string
  adjuntos?: Array<AlbumEntity>
  perfil?: PerfilEntity
  actualizado?: boolean
  votos?: Array<VotoNoticiaEntity>
  totalVotos?: number
  traducciones?: Array<TraduccionNoticiaEntity>
  medias?: Array<MediaEntity>,
  voto?: boolean
}

export interface TraduccionNoticiaEntity {
  _id?: string,
  tituloCorto?: string,
  titulo?: string,
  descripcion?: string,
  tituloOriginal?: string;
  tituloCortoOriginal?: string;
  descripcionOriginal?: string;
  tags?: Array<string>,
  idioma?: CatalogoIdiomaEntity,
  original?: boolean
}

@Injectable({ providedIn: 'root' })
export class NoticiaEntityMapperService extends MapedorService<NoticiaEntity, NoticiaModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private direccionEntityMapperService: DireccionEntityMapperService,
    private albumEntityMapperService: AlbumEntityMapperService,
    private perfilEntityMapperService: PerfilEntityMapperService,
    private mediaEntityMapperService: MediaEntityMapperService,
  ) {
    super()
  }

  protected map(entity: NoticiaEntity): NoticiaModel {
    if (entity) {
      const model: NoticiaModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      if (entity.direccion) {
        model.direccion = this.direccionEntityMapperService.transform(entity.direccion)
      }

      if (entity.autor) {
        model.autor = entity.autor
      }

      if (entity.adjuntos) {
        model.adjuntos = this.albumEntityMapperService.transform(entity.adjuntos)
      }

      if (entity.perfil) {
        model.perfil = this.perfilEntityMapperService.transform(entity.perfil)
      }

      if (entity.votos) {
      }

      if (entity.totalVotos) {
        model.totalVotos = entity.totalVotos
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        for (const traduccion of entity.traducciones) {
          if (traduccion.original && entity.traducciones.length > 1) {
            model.descripcionOriginal = traduccion?.descripcion;
            model.tituloOriginal = traduccion.titulo;
            model.tituloCortoOriginal = traduccion.tituloCorto;

          }
          if (!traduccion.original || entity.traducciones.length === 1) {
            model.descripcion = traduccion?.descripcion;
            model.titulo = traduccion.titulo;
            model.tituloCorto = traduccion.tituloCorto;

          }
        }
      }

      if (entity.medias) {
        model.medias = this.mediaEntityMapperService.transform(entity.medias)
      }

      model.voto = entity.voto
      model.actualizado = entity.actualizado

      return model
    }

    return null
  }
}
