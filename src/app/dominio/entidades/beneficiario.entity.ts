import { ProyectoEntity } from "dominio/entidades/proyecto.entity";
import { CatalogoTipoBeneficiarioEntity } from "dominio/entidades/catalogos/catalogo-tipo-beneficiario.entity";
import { UsuarioEntity } from 'dominio/entidades/usuario.entity'

export interface BeneficiarioEntity{
    id:string,
    fechaCreacion:Date,
    fechaActualizacion:Date,
    tipo:CatalogoTipoBeneficiarioEntity,
    usuario:UsuarioEntity,
    proyecto:ProyectoEntity 
}