import { BalanceEntity } from "dominio/entidades/balance.entity";
import { BeneficiarioEntity } from "dominio/entidades/beneficiario.entity"
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoMonedaEntity } from "dominio/entidades/catalogos/catalogo-tipo-moneda.entity";
import { CatalogoOrigenEntity } from "dominio/entidades/catalogos/catalogo-origen.entity";
import { CatalogoMetodoPagoEntity } from "dominio/entidades/catalogos/catalogo-metodo-pago.entity";

export interface TransaccionEntity {
    _id?: string,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    estado?: CatalogoEstadoEntity,
    monto?: number,
    moneda?: CatalogoTipoMonedaEntity,
    descripcion?: string,
    origen?: CatalogoOrigenEntity,
    destino?: CatalogoOrigenEntity,
    balance?: Array<BalanceEntity>,
    beneficiario?: BeneficiarioEntity,
    metodoPago?: CatalogoMetodoPagoEntity
}
