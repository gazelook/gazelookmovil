import { AlbumModel } from 'dominio/modelo/entidades/album.model';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoAlbumEntity, CatalogoAlbumEntityMapperService } from "dominio/entidades/catalogos/catalogo-album.entity";
import { MediaEntityMapperService, MediaEntity } from "dominio/entidades/media.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { Injectable } from '@angular/core';

export interface AlbumEntity {
    _id?:string
    fechaCreacion?:Date
    fechaActualizacion?:Date,
    traduccion?:Array<TraduccionAlbumEntity>,
    tipo?:CatalogoAlbumEntity,
    media?:Array<MediaEntity>, //MEDIA
    portada?:MediaEntity, //MEDIA
    predeterminado?: boolean,
}

export interface TraduccionAlbumEntity {
    id?: string,
    nombre?: string,
    idioma?: CatalogoIdiomaEntity,
    original?: boolean
}

@Injectable({ providedIn: 'root' })
export class AlbumEntityMapperService extends MapedorService<AlbumEntity, AlbumModel> {

    constructor(
        private mediaEntityMapper: MediaEntityMapperService,
        private catalogoAlbumEntityMapper: CatalogoAlbumEntityMapperService
    ) {
        super()
    }

    protected map(entity: AlbumEntity): AlbumModel {
        if (entity) {
            const model: AlbumModel = {}

            if (entity._id) {
                model._id = entity._id
            }

            if (entity.media) {
                model.media = this.mediaEntityMapper.transform(entity.media)
            }

            if (entity.portada) {
                model.portada = this.mediaEntityMapper.transform(entity.portada)
            }

            if (entity.tipo) {
                model.tipo = this.catalogoAlbumEntityMapper.transform(entity.tipo)
            }

            model.predeterminado = false

            if (entity.predeterminado) {
                model.predeterminado = entity.predeterminado
            }

            return model
        }
        return null;
    }
}
