import { Injectable } from '@angular/core';
import { MapedorService } from "@core/base/mapeador.interface";
import { CatalogoTipoPerfilModel } from "dominio/modelo/catalogos/catalogo-tipo-perfil.model";
import { PerfilEntity } from 'dominio/entidades/perfil.entity';
import { PerfilEntityMapperServicePerfil, PerfilEntityMapperServicePerfilresumenModelo } from 'dominio/entidades/perfil.entity';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";

export interface CatalogoTipoPerfilEntity {
  id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  perfil?: PerfilEntity
  traducciones?: Array<TraduccionCatalogoTipoPerfilEntity>
}

export interface TraduccionCatalogoTipoPerfilEntity {
  id?: string,
  nombre: string,
  idioma?: CatalogoIdiomaEntity, //CATATLOGO IDIOMA
  original?: boolean,
  descripcion?: string
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoPerfilEntitytMapperServiceSinPerfil extends MapedorService<CatalogoTipoPerfilEntity, CatalogoTipoPerfilModel> {

  constructor(
  ) {
    super()
  }

  protected map(entity: CatalogoTipoPerfilEntity,): CatalogoTipoPerfilModel {
    if (entity) {
      const model: CatalogoTipoPerfilModel = {}

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        model.nombre = entity.traducciones[0].nombre
        model.descripcion = entity.traducciones[0].descripcion
      }

      return model
    }

    return null
  }
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoPerfilMapperService extends MapedorService<CatalogoTipoPerfilEntity, CatalogoTipoPerfilModel> {

  constructor(
    private mapearPerfilEntityAResumen: PerfilEntityMapperServicePerfilresumenModelo
  ) {
    super()
  }

  protected map(entity: CatalogoTipoPerfilEntity,): CatalogoTipoPerfilModel {
    return (entity) ?
      {
        codigo: entity.codigo,
        nombre: (entity.traducciones) ? entity.traducciones[0].nombre : null,
        descripcion: (entity.traducciones) ? entity.traducciones[0].descripcion : null,
        perfil: this.mapearPerfilEntityAResumen.transform(entity.perfil)
      } : null;
  }
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoPerfilMapperService2 extends MapedorService<CatalogoTipoPerfilEntity, CatalogoTipoPerfilModel> {

  constructor(
    private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil
  ) {
    super()
  }

  protected map(entity: CatalogoTipoPerfilEntity): CatalogoTipoPerfilModel {
    return (entity) ? {
      codigo: entity.codigo,
      nombre: (entity.traducciones) ? entity.traducciones[0].nombre : null,
      descripcion: (entity.traducciones) ? entity.traducciones[0].descripcion : null,
      perfil: this.perfilEntityMapperServicePerfil.transform(entity.perfil)
    } : null;
  }
}

enum MapperCatalogoTipoPerfil {
  PERFIL_COMPLETO,
  PERFIL_RESUME
}






