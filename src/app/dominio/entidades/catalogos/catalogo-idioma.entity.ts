import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoIdiomaEntity{
    id?: string
    estado?: CatalogoEstadoEntity
    fechaCreacion?: Date
    fechaActualizacion?: Date
    codigo?:string
    nombre?:string
    codNombre?:string
    idiomaSistema?: boolean
}
