import {MediaEntityMapperService} from 'dominio/entidades/media.entity';
import {CatalogoEstadoEntity} from 'dominio/entidades/catalogos/catalogo-estado.entity';
import {MediaEntity} from 'dominio/entidades/media.entity';
import {Injectable} from '@angular/core';
import {MapedorService} from '@core/base/mapeador.interface';
import {CatalogoIdiomaEntity} from 'dominio/entidades/catalogos/catalogo-idioma.entity';
import {CatalogoMetodoPagoModel} from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';

export interface CatalogoMetodoPagoEntity {
  _id?: string;
  estado?: CatalogoEstadoEntity;
  fechaCreacion?: Date;
  fechaActualizacion?: Date;
  codigo?: string;
  nombre?: string;
  icono?: MediaEntity;
  descripcion?: string;
  traducciones?: Array<TraduccionCatalogoMetodoPagoEntity>;
}

export interface TraduccionCatalogoMetodoPagoEntity {
  _id?: string;
  nombre: string;
  idioma?: CatalogoIdiomaEntity; // CATATLOGO IDIOMA
  original?: boolean;
  descripcion?: string;
}

export interface PagoFacturacionEntity {
  nombres: string;
  telefono?: string;
  direccion?: string;
  email?: string;
  idPago?: string;

}

export interface MetodoPagoStripeEntity {
  clientSecret: string;
  customer: any;
}

@Injectable({providedIn: 'root'})
export class CatalogoMetodoPagoMapperService extends MapedorService<CatalogoMetodoPagoEntity, CatalogoMetodoPagoModel> {
  constructor(
    private mediaEntityMapperService: MediaEntityMapperService
  ) {
    super();
  }

  protected map(entity: CatalogoMetodoPagoEntity): CatalogoMetodoPagoModel {

    if (entity) {
      const model: CatalogoMetodoPagoModel = {};
      if (entity._id) {
        model.id = entity._id;
      }
      if (entity.estado) {
        model.estado = entity.estado;
      }
      if (entity.codigo) {
        model.codigo = entity.codigo;
      }
      if (entity.traducciones[0].nombre) {
        model.nombre = entity.traducciones[0].nombre;
      }
      if (entity.traducciones[0].descripcion) {
        model.descripcion = entity.traducciones[0].descripcion;
      }
      if (entity.icono) {
        model.icono = entity.icono.principal.url;
      }
      return model;
    }
    return null;
  }
}
