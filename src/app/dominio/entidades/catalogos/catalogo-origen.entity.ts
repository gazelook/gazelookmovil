import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
export interface CatalogoOrigenEntity{
    _id?: string,
    codigo?: string,
    estado?: CatalogoEstadoEntity
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    nombre?: string,
    descripcion?: string,
    ingreso?: boolean
}
