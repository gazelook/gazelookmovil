import { CatalogoEntidadEntity } from "dominio/entidades/catalogos/catalogo-entidad.entity";
import { CatalogoAccionEntity } from "dominio/entidades/catalogos/catalogo-accion.entity";
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';

export interface CatalogoEstadoEntity {
    _id?: String
    codigo?: string
    nombre?: string
    entidad?: CatalogoEntidadEntity
    accion?: CatalogoAccionEntity
    descripcion?: string
    fechaCreacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoEstadoEntityMapperService extends MapedorService<CatalogoEstadoEntity, CatalogoEstadoModel> {

    protected map(entity: CatalogoEstadoEntity): CatalogoEstadoModel {
        if (entity) {
            return {
                codigo: entity.codigo,
                nombre: entity.nombre,
                descripcion: entity.descripcion
            };
        }
        return null;
    }
}
