import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoModel } from 'dominio/modelo/catalogos/catalogo-estado.model';
import { ParticipanteProyectoEntityMapperService } from 'dominio/entidades/participante-proyecto.entity';
import { CatalogoTipoComentarioModel } from 'dominio/modelo/catalogos/catalogo-tipo-comentario.model';
import { CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";

export interface CatalogoTipoComentarioEntity {
  _id?: string,
  codigo?: string,
  nombre?: string,
  estado?: CatalogoEstadoModel //Catalogo estados
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoComentarioEntityMapperService extends MapedorService<CatalogoTipoComentarioEntity, CatalogoTipoComentarioModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private participanteProyectoMapperService: ParticipanteProyectoEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoTipoComentarioEntity): CatalogoTipoComentarioModel {
    if (entity) {

      const model: CatalogoTipoComentarioModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      return model
    }

    return null
  }

}
