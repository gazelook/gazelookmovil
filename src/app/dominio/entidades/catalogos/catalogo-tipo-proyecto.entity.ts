import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoProyectoModel } from 'dominio/modelo/catalogos/catalogo-tipo-proyecto.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";

export interface CatalogoTipoProyectoEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  traducciones?: Array<TraduccionCatalogoTipoProyectoEntity> // TraduccionCatalogoTipoProyecto
}
export interface TraduccionCatalogoTipoProyectoEntity {
  _id?: string,
  nombre?: string,
  idioma?: CatalogoIdiomaEntity,
  original?: boolean,
  descripcion?: string
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoProyectoEntityMapperService extends MapedorService<CatalogoTipoProyectoEntity, CatalogoTipoProyectoModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoTipoProyectoEntity): CatalogoTipoProyectoModel {
    const model: CatalogoTipoProyectoModel = {}

    if (entity._id) {
      model.id = entity._id
    }

    if (entity.codigo) {
      model.codigo = entity.codigo
    }

    if (entity.estado) {
      model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
    }

    if (entity.fechaCreacion) {
      model.fechaCreacion = entity.fechaCreacion
    }

    if (entity.fechaActualizacion) {
      model.fechaActualizacion = entity.fechaActualizacion
    }

    if (entity.traducciones && entity.traducciones.length > 0) {
      model.nombre = entity.traducciones[0].nombre
      model.descripcion = entity.traducciones[0].descripcion
      model.nombre = entity.traducciones[0].nombre
    }

    return model
  }
}
