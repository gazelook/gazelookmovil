import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoPaisModel } from 'dominio/modelo/catalogos/catalogo-pais.model';
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";

export interface CatalogoPaisEntity {
  id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
  codigoTelefono?: Array<any>
  codigoNombre?: string
  traducciones?: Array<TraduccionCatalogoPaisEntity>
  latitud?: number
  longitud?: number
}

export interface TraduccionCatalogoPaisEntity {
  id?: string,
  nombre?: string,
  idioma?: CatalogoIdiomaEntity,
  original?: boolean
}

@Injectable({ providedIn: 'root' })
export class CatalogoPaisEntityMapperService extends MapedorService<CatalogoPaisEntity, CatalogoPaisModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoPaisEntity): CatalogoPaisModel {
    if (entity) {
      const model: CatalogoPaisModel = {}

      if (entity.id) {
        model.id = entity.id
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.codigoNombre) {
        model.codigoNombre = entity.codigoNombre
      }

      if (entity.codigoTelefono) {
        model.codigoTelefono = entity.codigoTelefono
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.codigoNombre) {
        model.codigoNombre = entity.codigoNombre
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        model.nombre = entity.traducciones[0].nombre
      }

      if (entity.latitud) {
        model.latitud = entity.latitud
      }
      if (entity.longitud) {
        model.longitud = entity.longitud
      }
      return model
    }

    return null
  }

}

@Injectable({ providedIn: 'root' })
export class CatalogoPaisMapperAItemSelectorService extends MapedorService<CatalogoPaisModel, ItemSelector> {

  protected map(entity: CatalogoPaisModel): ItemSelector {
    return {
      codigo: entity.codigo,
      nombre: entity.nombre,
      latitud: entity.latitud,
      longitud: entity.longitud,
    };
  }
}
