import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoVotoModel } from 'dominio/modelo/catalogos/catalogo-tipo-voto.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";

export interface CatalogoTipoVotoEntity {
  _id?: string,
  codigo?: string,
  nombre?: string,
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  votoAdmin?: boolean,
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoVotoEntityMapperService extends MapedorService<CatalogoTipoVotoEntity, CatalogoTipoVotoModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoTipoVotoEntity): CatalogoTipoVotoModel {

    if (entity) {

      const model: CatalogoTipoVotoModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      if (entity.nombre) {
        model.nombre = entity.nombre
      }

      model.votoAdmin = entity.votoAdmin

      return model;
    }

    return null;
  }

}
