import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEventoNotificacionModel } from "dominio/modelo/catalogos/catalogo-evento-notificacion";

export interface CatalogoEventoNotificacionEntity {
  _id?: String
  codigo?: string
  nombre?: string
  descripcion?: string
  fechaCreacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoEstadoEntityMapperService extends MapedorService<CatalogoEventoNotificacionEntity, CatalogoEventoNotificacionModel> {

  protected map(entity: CatalogoEventoNotificacionEntity): CatalogoEventoNotificacionModel {
    if (entity) {
      return {
        codigo: entity.codigo,
        nombre: entity.nombre,
        descripcion: entity.descripcion
      };
    }
    return null;
  }
}
