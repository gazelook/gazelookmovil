import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoAnuncioSistemaModel } from 'dominio/modelo/catalogos/catalogo-tipo-anuncio-sistema.model';

export interface CatalogoTipoAnuncioSistemaEntity {
    _id?: string,
    codigo?: string,
    nombre?: string,
    estado?: CatalogoEstadoEntity
    fechaCreacion?: Date,
    fechaActualizacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoAnuncioSistemaElegibleMapperService extends MapedorService<CatalogoTipoAnuncioSistemaEntity, CatalogoTipoAnuncioSistemaModel> {

    protected map(model: CatalogoTipoMonedaModel): CatalogoTipoAnuncioSistemaModel {
        return model;
    }
}