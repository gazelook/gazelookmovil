import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { CatalogoTipoMonedaModel } from 'dominio/modelo/catalogos/catalogo-tipo-moneda.model';
import { CatalogoEstadoEntity } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { ItemSelector } from '@shared/diseno/modelos/elegible.interface';

export interface CatalogoTipoMonedaEntity {
    _id?: string,
    codigo?: string,
    codNombre?: string,
    nombre?: string,
    predeterminado?: boolean,
    estado?: CatalogoEstadoEntity //Catalogo estados
    fechaCreacion?: Date,
    fechaActualizacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoMonedaEntityMapperService extends MapedorService<CatalogoTipoMonedaEntity, CatalogoTipoMonedaModel> {

    protected map(entity: CatalogoTipoMonedaEntity): CatalogoTipoMonedaModel {
        if (entity) {
            const model: CatalogoTipoMonedaModel = {}

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.codigo) {
                model.codigo = entity.codigo
            }

            if (entity.codNombre) {
                model.codNombre = entity.codNombre
            }

            if (entity.nombre) {
                model.nombre = entity.nombre
            }

            if (entity.predeterminado) {
                model.predeterminado = entity.predeterminado
            }

            return model
        }
        return null
    }
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoMonedaElegibleMapperService extends MapedorService<CatalogoTipoMonedaModel, ItemSelector> {
    protected map(model: CatalogoTipoMonedaModel): ItemSelector {
        if (model) {
            return {
                codigo: model.codigo,
                nombre: model.nombre,
                auxiliar: model.codNombre
            }
        }
        return null
    }
}
