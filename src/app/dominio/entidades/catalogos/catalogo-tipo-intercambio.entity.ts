import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoIntercambioModel } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import { CatalogoEstadoEntity } from 'dominio/entidades/catalogos/catalogo-estado.entity';
import { CatalogoEstadoEntityMapperService } from 'dominio/entidades/catalogos/catalogo-evento-notificacion';
import { CatalogoIdiomaEntity } from 'dominio/entidades/catalogos/catalogo-idioma.entity';

export interface CatalogoTipoIntercambioEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  traducciones?: Array<TraduccionCatalogoTipoIntercambioEntity> // TraduccionCatalogoTipoProyecto
}
export interface TraduccionCatalogoTipoIntercambioEntity {
  _id?: string,
  nombre?: string,
  idioma?: CatalogoIdiomaEntity,
  original?: boolean,
  descripcion?: string
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoIntercambioEntityMapperService extends MapedorService<CatalogoTipoIntercambioEntity, CatalogoTipoIntercambioModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoTipoIntercambioEntity): CatalogoTipoIntercambioModel {
    const model: CatalogoTipoIntercambioModel = {}

    if (entity._id) {
      model.id = entity._id
    }

    if (entity.codigo) {
      model.codigo = entity.codigo
    }

    if (entity.estado) {
      model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
    }

    if (entity.fechaCreacion) {
      model.fechaCreacion = entity.fechaCreacion
    }

    if (entity.fechaActualizacion) {
      model.fechaActualizacion = entity.fechaActualizacion
    }

    if (entity.traducciones && entity.traducciones.length > 0) {
      model.nombre = entity.traducciones[0].nombre
      model.descripcion = entity.traducciones[0].descripcion
      model.nombre = entity.traducciones[0].nombre
    }

    return model
  }
}
