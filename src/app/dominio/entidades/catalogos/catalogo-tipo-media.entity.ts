import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoTipoMediaModel } from 'dominio/modelo/catalogos/catalogo-tipo-media.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";

export interface CatalogoTipoMediaEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  codigo?: string
}

@Injectable({ providedIn: 'root' })
export class CatalogoTipoMediaEntityMapperService extends MapedorService<CatalogoTipoMediaEntity, CatalogoTipoMediaModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService
  ) {
    super()
  }

  protected map(entity: CatalogoTipoMediaEntity): CatalogoTipoMediaModel {
    if (entity) {

      const model: CatalogoTipoMediaModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
      }

      if (entity.codigo) {
        model.codigo = entity.codigo
      }

      return entity

    }
    return null
  }
}
