import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoAccionModel } from 'dominio/modelo/catalogos/catalogo-accion.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";

export interface CatalogoAccionEntity {
  _id?: string,
  codigo?: string,
  estado?: CatalogoEstadoEntity, // CatalogoEstado
  nombre?: string,
  fechaCreacion?: Date,
  fechaActualizacion?: Date
}

@Injectable({ providedIn: 'root' })
export class CatalogoAccionEntityMapperService extends MapedorService<CatalogoAccionEntity, CatalogoAccionModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: CatalogoAccionEntity): CatalogoAccionModel {
    if (entity) {
      const catalogoAccionModel: CatalogoAccionModel = {}

      if (entity._id) {
        catalogoAccionModel.id = entity._id
      }

      if (entity.codigo) {
        catalogoAccionModel.codigo = entity.codigo
      }

      if (entity.estado) {
        catalogoAccionModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }


      if (entity.nombre) {
        catalogoAccionModel.nombre = entity.nombre
      }

      return catalogoAccionModel
    }

    return null
  }
}
