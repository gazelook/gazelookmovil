import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstiloAnuncioSistemaModel } from "dominio/modelo/catalogos/catalogo-estilo-anuncio-sistema.model";
import { CatalogoTipoEstiloAnuncioSistemaModelMapperService } from 'dominio/modelo/catalogos/catalogo-tipo-estilo-anuncio-sistema.model';
import { CatalogoTipoEstiloAnuncioSistemaEntity } from 'dominio/entidades/catalogos/catalogo-tipo-estilo-anuncio-sistema.entity';
export interface CatalogoEstiloAnuncioSistemaEntity {
    _id?: string,
    color?: string,
    tamanioLetra?: string,
    tipo?: CatalogoTipoEstiloAnuncioSistemaEntity
}

@Injectable({ providedIn: 'root' })
export class CatalogoEstiloAnuncioSistemaMapperService extends MapedorService<CatalogoEstiloAnuncioSistemaEntity, CatalogoEstiloAnuncioSistemaModel> {

    constructor(
        private catalogoTipoEstiloAnuncioSistemaModelMapperService: CatalogoTipoEstiloAnuncioSistemaModelMapperService,
    ) {
        super()
    }

    protected map(entity: CatalogoEstiloAnuncioSistemaEntity): CatalogoEstiloAnuncioSistemaModel {
        if (entity) {
            const catalogoEstiloModel: CatalogoEstiloAnuncioSistemaModel = {}

            if (entity._id) {
                catalogoEstiloModel._id = entity._id
            }

            if (entity.color) {
                catalogoEstiloModel.color = entity.color
            }

            if (entity.tamanioLetra) {
                catalogoEstiloModel.tamanioLetra = entity.tamanioLetra
            }
          
            if (entity.tipo) {
                catalogoEstiloModel.tipo = this.catalogoTipoEstiloAnuncioSistemaModelMapperService.transform(entity.tipo) 
            }

            return catalogoEstiloModel
        }

        return null
    }

}