import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ReporteFinanzasUsuarioModel } from 'dominio/modelo/entidades/reporte-finanzas-usuario.model';
import {
  CatalogoTipoMonedaEntity,
  CatalogoTipoMonedaEntityMapperService,
} from 'dominio/entidades/catalogos/catalogo-tipo-moneda.entity';

export interface valores {
  monto?: number;
  fechaCreacion?: Date;
}
export interface ReporteFinanzasUsuarioEntity {
  aportacionesSuscripcion?: number;
  aportacionesValorExtra?: Array<valores>;
  aportacionesDonacionProyectos?: Array<valores>;
  moneda?: CatalogoTipoMonedaEntity;
  aportaciones?: number;
}

@Injectable({ providedIn: 'root' })
export class ReporteFinanzasUsuarioEntityMapperService extends MapedorService<
  ReporteFinanzasUsuarioEntity,
  ReporteFinanzasUsuarioModel
> {
  constructor(
    private catalogoTipoMonedaEntityMapperService: CatalogoTipoMonedaEntityMapperService
  ) {
    super();
  }

  protected map(
    entity: ReporteFinanzasUsuarioEntity
  ): ReporteFinanzasUsuarioModel {
    if (entity) {
      const model: ReporteFinanzasUsuarioModel = {};

      if (entity.aportacionesSuscripcion && entity.aportacionesSuscripcion >= 0) {
        model.aportacionesSuscripcion = entity.aportacionesSuscripcion;
      }

      if (entity.aportacionesValorExtra && entity.aportacionesValorExtra.length >= 0) {
        model.aportacionesValorExtra = entity.aportacionesValorExtra;
      }

      if (entity.aportacionesDonacionProyectos && entity.aportacionesDonacionProyectos.length >= 0) {
        model.aportacionesDonacionProyectos = entity.aportacionesDonacionProyectos;
      }

      if (entity.moneda) {
        model.moneda = this.catalogoTipoMonedaEntityMapperService.transform(
          entity.moneda
        );
      }

      if (entity.aportaciones) {
        model.aportaciones = entity.aportaciones;
      }
      return model;
    }

    return null;
  }
}
