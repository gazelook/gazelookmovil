import { TelefonoModel } from 'dominio/modelo/entidades/telefono.model';
import { PerfilResumenModel } from 'dominio/modelo/entidades/perfil-resumen.model';
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { UsuarioEntity, UsuarioEntityMapperService } from "dominio/entidades/usuario.entity";
import { AlbumEntity, AlbumEntityMapperService } from "dominio/entidades/album.entity"
import { TelefonoEntity } from "dominio/entidades/telefono.entity"
import { DireccionEntity, DireccionEntityMapperService } from "dominio/entidades/direccion.entity";
import { ProyectoEntity, ProyectoEntityMapperService } from "dominio/entidades/proyecto.entity";
import { PensamientoEntity, PensamientoMapperService } from "dominio/entidades/pensamiento.entity";
import { AsociacionEntity, AsociacionEntityMapperService } from "dominio/entidades/asociacion.entity";
import { NoticiaEntity, NoticiaEntityMapperService } from "dominio/entidades/noticia.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoPerfilEntity, CatalogoTipoPerfilEntitytMapperServiceSinPerfil, CatalogoTipoPerfilMapperService } from "dominio/entidades/catalogos/catalogo-tipo-perfil.entity";
import { PerfilModel } from 'dominio/modelo/entidades/perfil.model';
import { ParticipanteAsociacionEntity } from 'dominio/entidades/participante-asociacion.entity';

export interface PerfilEntity {
    _id?: string,
    nombreContacto?: string,
    nombreContactoTraducido?: string,
    nombre?: string,
    tipoPerfil?: CatalogoTipoPerfilEntity,
    usuario?: UsuarioEntity,
    album?: Array<AlbumEntity>,
    estado?: CatalogoEstadoEntity,
    direcciones?: Array<DireccionEntity>,
    telefonos?: Array<TelefonoEntity>,
    proyectos?: Array<ProyectoEntity>,
    pensamientos?: Array<PensamientoEntity>,
    noticias?: Array<NoticiaEntity>,
    asociaciones?: Array<AsociacionEntity>,
    participanteAsociacion?: ParticipanteAsociacionEntity,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
}

export interface PerfilCrearCuentaEntity {
    nombreContacto: string,
    nombre: string,
    tipoPerfil: string,
    albums: Array<AlbumEntity>[]
}

@Injectable({ providedIn: 'root' })
export class PerfilEntityMapperService extends MapedorService<PerfilEntity, PerfilModel> {
    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
        private catalogoTipoPerfilEntitytMapperServiceSinPerfil: CatalogoTipoPerfilEntitytMapperServiceSinPerfil,
        private albumEntityMapperService: AlbumEntityMapperService
    ) {
        super()
    }

    protected map(entity: PerfilEntity): PerfilModel {
        
        if (entity) {
            const model: PerfilModel = {}

            if (entity._id) {
                model._id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }
            if (entity.album) {
                model.album = this.albumEntityMapperService.transform(entity.album)
            }

            if (entity.nombre) {
                model.nombre = entity.nombre
            }

            if (entity.nombreContacto) {
                model.nombreContacto = entity.nombreContacto
            }
            
            if (entity.nombreContactoTraducido) {
                model.nombreContactoTraducido = entity.nombreContactoTraducido
            }

            if (entity.tipoPerfil) {
                model.tipoPerfil = this.catalogoTipoPerfilEntitytMapperServiceSinPerfil.transform(entity.tipoPerfil)
            }

            return model
        }
        return null;
    }
}

@Injectable({ providedIn: 'root' })
export class PerfilEntityMapperServicePerfilresumenModelo extends MapedorService<PerfilEntity, PerfilResumenModel> {

    protected map(entity: PerfilEntity): PerfilResumenModel {
        return (entity) ? {
            _id: entity._id,
            nombreContacto: entity.nombreContacto,
            nombreContactoTraducido: entity.nombreContactoTraducido,
            nombre: entity.nombre,
            estado: entity.estado,
        } : null
    }

}

@Injectable({ providedIn: 'root' })
export class PerfilEntityMapperServicePerfil extends MapedorService<PerfilEntity, PerfilModel> {

    constructor(
        private tipoPerfilMapper: CatalogoTipoPerfilMapperService,
        private direccionEntityMapperService: DireccionEntityMapperService,
        private albumEntityMapperService: AlbumEntityMapperService
    ) {
        super();
    } 

    protected map(entity: PerfilEntity): PerfilModel {
        
        if (entity) {
            const perfil: PerfilModel = {
                _id: entity._id,
                nombreContacto: entity.nombreContacto,
                nombreContactoTraducido: entity.nombreContactoTraducido,
                nombre: entity.nombre,
                estado: entity.estado,
                tipoPerfil: this.tipoPerfilMapper.transform(entity.tipoPerfil),
                album: this.albumEntityMapperService.transform(entity?.album),
                asociaciones: entity?.asociaciones,
                telefonos: entity.telefonos as TelefonoModel[]
            }

            if (entity.direcciones) {
                perfil.direcciones = this.direccionEntityMapperService.transform(entity.direcciones)
            }

            return perfil
        }
        return null;
    }
}

@Injectable({ providedIn: 'root' })
export class PerfilResumenEntityMapper extends MapedorService<PerfilEntity, PerfilModel> {

    constructor(
        private tipoPerfilMapper: CatalogoTipoPerfilMapperService,
        private pensamientoMapperService: PensamientoMapperService
    ) {
        super();
    }

    protected map(entity: PerfilEntity): PerfilModel {
        
        if (entity) {
            const perfil: PerfilModel = {
                _id: entity._id,
                nombreContacto: entity.nombreContacto,
                nombreContactoTraducido: entity.nombreContactoTraducido,
                nombre: entity.nombre,
                estado: entity.estado,
                tipoPerfil: this.tipoPerfilMapper.transform(entity.tipoPerfil),
                album: entity?.album,
               
            }

            if (entity.pensamientos) {
                perfil.pensamientos = this.pensamientoMapperService.transform(entity.pensamientos)
            }

            
            return perfil
        }
        return null;
    }
}

@Injectable({ providedIn: 'root' })
export class PerfilEntityGeneralMapperService extends MapedorService<PerfilEntity, PerfilModel> {
    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
        private catalogoTipoPerfilEntitytMapperServiceSinPerfil: CatalogoTipoPerfilEntitytMapperServiceSinPerfil,
        private albumEntityMapperService: AlbumEntityMapperService,
        private proyectoEntityMapperService: ProyectoEntityMapperService,
        private noticiaEntityMapperService: NoticiaEntityMapperService,
        private pensamientoMapperService: PensamientoMapperService
    ) {
        super()
    }

    protected map(entity: PerfilEntity): PerfilModel {
        if (entity) {
            const model: PerfilModel = {}

            if (entity._id) {
                model._id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }

            if (entity.nombre) {
                model.nombre = entity.nombre
            }

            if (entity.nombreContacto) {
                model.nombreContacto = entity.nombreContacto
            }

            if (entity.nombreContactoTraducido) {
                model.nombreContactoTraducido = entity.nombreContactoTraducido
            }

            if (entity.tipoPerfil) {
                model.tipoPerfil = this.catalogoTipoPerfilEntitytMapperServiceSinPerfil.transform(entity.tipoPerfil)
            }

            if (entity.album) {
                model.album = this.albumEntityMapperService.transform(entity.album)
            }

            if (entity.proyectos) {
                model.proyectos = this.proyectoEntityMapperService.transform(entity.proyectos)
            }

            if (entity.noticias) {
                model.noticias = this.noticiaEntityMapperService.transform(entity.noticias)
            }

            if (entity.asociaciones) {
                model.asociaciones = entity.asociaciones
            }
            
            if(entity.pensamientos){
                model.pensamientos = this.pensamientoMapperService.transform(entity.pensamientos)
            }

            if (entity.participanteAsociacion) {
                model.participanteAsociacion = entity.participanteAsociacion
            }


            return model
        }

        return null
    }
}

