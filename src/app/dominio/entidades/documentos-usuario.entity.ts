import { ArchivoEntity } from 'dominio/entidades/archivo.entity';
import { CatalogoDocumentosUsuarioEntity } from 'dominio/entidades/catalogos/catalogo-documentos-usuario.entity';
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { DocumentosUsuarioModel } from 'dominio/modelo/entidades/documentos-usuario.model';
export interface DocumentosUsuarioEntity {
    _id?: string
    tipo?: CatalogoDocumentosUsuarioEntity,
    archivo?: ArchivoEntity
}

@Injectable({ providedIn: 'root' })
//llega entidad envio PensamientoModel
export class ParticipanteAsociacionMapperService extends MapedorService<DocumentosUsuarioEntity, DocumentosUsuarioModel> {

    constructor(

    ) {
        super(); 
    }

    protected map(entity: DocumentosUsuarioEntity): DocumentosUsuarioModel {

       
        

        if (entity) {
            const participanteAsoModel: DocumentosUsuarioModel = {}
            
            return participanteAsoModel
        }

        return null
    }

} 