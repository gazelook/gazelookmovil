import { PerfilEntity, PerfilEntityMapperServicePerfil } from "dominio/entidades/perfil.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { PensamientoModel } from "dominio/modelo/entidades/pensamiento.model"

export interface PensamientoEntity {
    _id?: string,
    perfil?: PerfilEntity,
    traducciones?: Array<TraduccionPensamientoEntity>, // TraduccionPensamiento
    estado?: CatalogoEstadoEntity, // CatalogoEstado
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    publico?: boolean
}

export interface TraduccionPensamientoEntity {
    _id?: string,
    texto?: string,
    idioma?: CatalogoIdiomaEntity,
    original?: string
}

@Injectable({ providedIn: 'root' })
//llega entidad envio PensamientoModel
export class PensamientoMapperService extends MapedorService<PensamientoEntity, PensamientoModel> {

    constructor(
        private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil,
        private estadoMapperService: CatalogoEstadoEntityMapperService,
    ) {
        super();
    }

    protected map(entity: PensamientoEntity): PensamientoModel {
        const model: PensamientoModel = {}
        if (entity) {
            if (entity.estado) {
                model.estado = this.estadoMapperService.transform(entity.estado)
            }

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.perfil) {
                model.perfil = this.perfilEntityMapperServicePerfil.transform(entity.perfil)
            }

            model.publico = entity.publico

            if (entity.traducciones && entity.traducciones.length > 0) {
                model.texto = entity.traducciones[0].texto
            }

            if (entity.fechaActualizacion) {
                model.fechaActualizacion = entity.fechaActualizacion
            }

            return model
        }

        return null
    }
}
