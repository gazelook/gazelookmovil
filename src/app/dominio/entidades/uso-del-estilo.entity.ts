import { UsoDelEstiloModel } from 'dominio/modelo/entidades/uso-del-estilo.model';
import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from 'dominio/entidades/catalogos/catalogo-estado.entity';

export interface UsoDelEstiloEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    codigo?: string,
    nombre?: string,
    descripcion?: string,
}

@Injectable({ providedIn: 'root' })
export class UsoDelEstiloEntityMapperService extends MapedorService<UsoDelEstiloEntity, UsoDelEstiloModel> {

    constructor(
        private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    ) {
        super()
    }

    protected map(entity: UsoDelEstiloEntity): UsoDelEstiloModel {

        if (entity) {
            const model: UsoDelEstiloModel = {}

            if (entity._id) {
                model.id = entity._id
            }

            if (entity.estado) {
                model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
            }

            if (entity.codigo) {
                model.codigo = entity.codigo
            }

            if (entity.nombre) {
                model.nombre = entity.nombre
            }

            if (entity.descripcion) {
                model.descripcion = entity.descripcion
            }

            return model
        }

        return null
    }

}
