import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { AsociacionModel } from 'dominio/modelo/entidades/asociacion.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoTipoAsociacionEntity, CatalogoTipoAsociacionEntityMapperService } from "dominio/entidades/catalogos/catalogo-tipo-asociacion.entity";
import { ConversacionEntity } from "dominio/entidades/conversacion.entity";
import { MediaEntity } from "dominio/entidades/media.entity";
import { ParticipanteAsociacionEntity, ParticipanteAsociacionMapperService } from "dominio/entidades/participante-asociacion.entity";

export interface AsociacionEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity, // CatalogoEstado
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  tipo?: CatalogoTipoAsociacionEntity, // CatalogoTipoAsociacion
  nombre?: string,
  foto?: MediaEntity,
  participantes?: Array<ParticipanteAsociacionEntity>, // Participante
  conversacion?: ConversacionEntity, // Conversacion
  privado?: boolean
}

@Injectable({ providedIn: 'root' })
export class AsociacionEntityMapperService extends MapedorService<AsociacionEntity, AsociacionModel> {

  constructor(
    private estadoEntityMapper: CatalogoEstadoEntityMapperService,
    private catalogoTipoAsociacionEntityMapperService: CatalogoTipoAsociacionEntityMapperService,
    private participanteAsociacionMapperService: ParticipanteAsociacionMapperService
  ) {
    super()
  }

  protected map(entity: AsociacionEntity): AsociacionModel {
    if (entity) {
      const asociacionModel: AsociacionModel = {}

      if (entity._id) {
        asociacionModel.id = entity._id
      }

      if (entity.estado) {
        asociacionModel.estado = this.estadoEntityMapper.transform(entity.estado)
      }

      if (entity.tipo) {
        asociacionModel.tipo = this.catalogoTipoAsociacionEntityMapperService.transform(entity.tipo)
      }

      if (entity.foto) {
      }

      if (entity.privado) {
        asociacionModel.privado = entity.privado
      }

      if (entity.nombre) {
        asociacionModel.nombre = entity.nombre
      }

      if (entity.participantes) {
        asociacionModel.participantes = this.participanteAsociacionMapperService.transform(entity.participantes)
      }
      return asociacionModel
    }
    return null
  }
}

@Injectable({ providedIn: 'root' })
export class AsociacionResumenEntityMapperService extends MapedorService<AsociacionEntity, AsociacionModel> {

  protected map(entity: AsociacionEntity): AsociacionModel {
    return {
      id: entity._id,
      nombre: entity.nombre,
    };
  }
}
