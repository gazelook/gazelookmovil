import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { ParticipanteProyectoModel } from 'dominio/modelo/entidades/participante-proyecto.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { ComentarioEntity } from "dominio/entidades/comentario.entity";
import { ConfiguracionEstiloEntity, ConfiguracionEstiloEntityMapperService } from "dominio/entidades/configuracion-estilo.entity";
import { PerfilEntity, PerfilEntityMapperServicePerfil } from "dominio/entidades/perfil.entity";
import { ProyectoEntity } from "dominio/entidades/proyecto.entity";
import { RolEntidadEntity, RolEntidadEntityMapperService } from 'dominio/entidades/rol-entidad.entity';

export interface ParticipanteProyectoEntity {
  _id?: string,
  estado?: CatalogoEstadoEntity,
  fechaCreacion?: Date,
  fechaActualizacion?: Date,
  roles?: Array<RolEntidadEntity>,
  configuraciones?: Array<ConfiguracionEstiloEntity>,
  comentarios?: Array<ComentarioEntity>,
  proyecto?: ProyectoEntity,
  coautor?: PerfilEntity,
  totalComentarios?: number
}

@Injectable({ providedIn: 'root' })
export class ParticipanteProyectoEntityMapperService extends MapedorService<ParticipanteProyectoEntity, ParticipanteProyectoModel> {

  constructor(
    private catalogoEstadoEntityMapperService: CatalogoEstadoEntityMapperService,
    private configuracionEstiloEntityMapperService: ConfiguracionEstiloEntityMapperService,
    private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil,
    private rolEntidadEntityMapperService: RolEntidadEntityMapperService
  ) {
    super()
  }

  protected map(entity: ParticipanteProyectoEntity): ParticipanteProyectoModel {

    if (entity) {
      const model: ParticipanteProyectoModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapperService.transform(entity.estado)
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      if (entity.roles) {
        model.roles = this.rolEntidadEntityMapperService.transform(entity.roles)
      }

      if (entity.configuraciones) {
        model.configuraciones = this.configuracionEstiloEntityMapperService.transform(entity.configuraciones)
      }

      if (entity.comentarios) {
      }

      if (entity.proyecto) {
      }

      if (entity.coautor) {
        model.coautor = this.perfilEntityMapperServicePerfil.transform(entity.coautor)
      }

      if (entity.totalComentarios) {
        model.totalComentarios = entity.totalComentarios
      }

      return model
    }

    return null
  }
}


@Injectable({ providedIn: 'root' })
export class ParticipanteProyectoResumenEntityMapperService extends MapedorService<ParticipanteProyectoEntity, ParticipanteProyectoModel> {
  constructor(
    private perfilEntityMapperServicePerfil: PerfilEntityMapperServicePerfil

  ) {
    super();
  }
  protected map(entity: ParticipanteProyectoEntity): ParticipanteProyectoModel {
    return {
      id: entity._id,
      coautor: this.perfilEntityMapperServicePerfil.transform(entity.coautor),
    };
  }
}


