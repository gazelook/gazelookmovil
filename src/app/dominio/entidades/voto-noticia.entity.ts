import { Injectable } from '@angular/core';
import { MapedorService } from '@core/base/mapeador.interface';
import { VotoNoticiaModel } from 'dominio/modelo/entidades/voto-noticia.model';
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos//catalogo-idioma.entity";
import { NoticiaEntity } from "dominio/entidades/noticia.entity";
import { PerfilEntity } from "dominio/entidades/perfil.entity";


export interface VotoNoticiaEntity {
  _id?: string
  estado?: CatalogoEstadoEntity
  fechaCreacion?: Date
  fechaActualizacion?: Date
  perfil?: PerfilEntity
  noticia?: NoticiaEntity
  traducciones?: Array<TraduccionVotoNoticiaEntity>
}

export interface TraduccionVotoNoticiaEntity {
  _id?: string
  descripcion?: string
  idioma?: CatalogoIdiomaEntity
  original?: boolean
}

@Injectable({ providedIn: 'root' })
export class VotoNoticiaEntityMapperService extends MapedorService<VotoNoticiaEntity, VotoNoticiaModel> {

  constructor(
    private catalogoEstadoEntityMapper: CatalogoEstadoEntityMapperService,
  ) {
    super()
  }

  protected map(entity: VotoNoticiaEntity): VotoNoticiaModel {
    if (entity) {
      const model: VotoNoticiaModel = {}

      if (entity._id) {
        model.id = entity._id
      }

      if (entity.estado) {
        model.estado = this.catalogoEstadoEntityMapper.transform(entity.estado)
      }

      if (entity.fechaCreacion) {
        model.fechaCreacion = entity.fechaCreacion
      }

      if (entity.fechaActualizacion) {
        model.fechaActualizacion = entity.fechaActualizacion
      }

      if (entity.noticia) {
        // proyecto?: ProyectoEntity,
      }

      if (entity.perfil) {
        // perfil?: PerfilEntity,
      }

      if (entity.traducciones && entity.traducciones.length > 0) {
        model.descripcion = entity.traducciones[0].descripcion
      }

      return model;
    }

    return null;
  }
}
