export interface ReembolsoEntity {
  _id?: string;
  metodoPago?: MetodoPago;
  informacionPago?: InformacionPago;
  monto?: number;
  moneda?: Moneda;
  conversionTransaccion?: ConversionTransaccion[];
  fechaCreacion?: Date;
}

export interface ConversionTransaccion {
  _id?: string;
  principal?: boolean;
  moneda?: Moneda;
  monto?: number;
}

export interface Moneda {
  codNombre?: string;
}

export interface InformacionPago {
  _id?: string;
  datos?: Datos;
  idPago?: string;
}

export interface Datos {
  nombres?: string;
  telefono?: string;
  direcccion?: string;
  email?: string;
}

export interface MetodoPago {
  codigo?: string;
}
