import { MediaModel } from 'dominio/modelo/entidades/media.model';
import { MapedorService } from '@core/base/mapeador.interface';
import { Injectable } from '@angular/core';
import { ArchivoEntity, ArchivoEntityMapperService } from "dominio/entidades/archivo.entity";
import { CatalogoEstadoEntity, CatalogoEstadoEntityMapperService } from "dominio/entidades/catalogos/catalogo-estado.entity";
import { CatalogoMediaEntity, CatalogoMediaEntityMapperService } from "dominio/entidades/catalogos/catalogo-media.entity";
import { CatalogoIdiomaEntity } from "dominio/entidades/catalogos/catalogo-idioma.entity";

export interface MediaEntity {
    _id?: string,
    estado?: CatalogoEstadoEntity,
    fechaCreacion?: Date,
    fechaActualizacion?: Date,
    catalogoMedia?: CatalogoMediaEntity,
    principal?: ArchivoEntity,
    miniatura?: ArchivoEntity,
    enlace?: string,
    traducciones?: Array<TraduccionMediaEntity>
}

export interface TraduccionMediaEntity {
    _id?: string,
    descripcion?: string,
    idioma?: CatalogoIdiomaEntity,
    original?: boolean
}

@Injectable({ providedIn: 'root' })
export class MediaEntityMapperService extends MapedorService<MediaEntity, MediaModel> {

    constructor(
        private estadoEntityMapper: CatalogoEstadoEntityMapperService,
        private archivoEntityMapper: ArchivoEntityMapperService,
        private catalogoMediaEntityMapper: CatalogoMediaEntityMapperService,
    ) {
        super()
    }

    protected map(entity: MediaEntity): MediaModel {
        if (entity) {
            const mediaModel: MediaModel = {}
            if (entity._id) {
                mediaModel.id = entity._id
            }

            if (entity.estado) {
                mediaModel.estado = this.estadoEntityMapper.transform(entity.estado)
            }

            if (entity.catalogoMedia) {
                mediaModel.catalogoMedia = this.catalogoMediaEntityMapper.transform(entity.catalogoMedia)
            }

            if (entity.principal) {
                mediaModel.principal = this.archivoEntityMapper.transform(entity.principal)
            }

            if (entity.miniatura) {
                mediaModel.miniatura = this.archivoEntityMapper.transform(entity.miniatura)
            }

            if (entity.enlace) {
                mediaModel.enlace = entity.enlace
            }

            mediaModel.descripcion = ''
            if (entity.traducciones && entity.traducciones.length > 0) {
                mediaModel.descripcion = entity.traducciones[0].descripcion
            }

            if (entity.fechaActualizacion) {
                mediaModel.fechaActualizacion = entity.fechaActualizacion
            }

            if (entity.fechaCreacion) {
                mediaModel.fechaActualizacion = entity.fechaCreacion
            }

            return mediaModel
        }
        return null
    }
}
