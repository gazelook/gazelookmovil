import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {throwError} from '@env/node_modules/rxjs';
import {Observable} from 'rxjs';

import {ConvertidorCriptoService} from '@core/servicios/remotos/convertidor.cripto.service';
import {RespuestaRemota} from '@core/util/respuesta';


@Injectable({providedIn: 'root'})
export class ConvertidorCriptoRepository {
  constructor(
    private convertidorCriptoService: ConvertidorCriptoService
  ) {
  }

  public convertirMonedaACripto(amount: number, convertID: number, symbol: string): Observable<number> {
    return this.convertidorCriptoService.convertirMonedaACripto(amount, convertID, symbol)
      .pipe(map(data => data.respuesta.datos), catchError(err => throwError(err)));
  }
}
