import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { PensamientoModel } from 'dominio/modelo/entidades/pensamiento.model';
import { ProyectoModel } from 'dominio/modelo/entidades/proyecto.model';
import { JsonDemoServiceLocal } from '@core/servicios/locales/json-demo.service';
import { ComentarioEntityMapperService } from 'dominio/entidades/comentario.entity';
import { NoticiaEntityMapperService } from 'dominio/entidades/noticia.entity';
import { ProyectoEntityMapperService } from 'dominio/entidades/proyecto.entity';
import { NoticiaModel } from 'dominio/modelo/entidades/noticia.model';


@Injectable({ providedIn: 'root' })
export class JsonDemoRepository {

  constructor(
    private jsonDemoServiceLocal: JsonDemoServiceLocal,
    private proyectoEntityMapperService: ProyectoEntityMapperService,
    private noticiaEntityMapperService: NoticiaEntityMapperService,
    private comentarioEntityMapperService: ComentarioEntityMapperService,
  ) {

  }

  obtenerContactosDemo(): Observable<any> {
    return this.jsonDemoServiceLocal.obtenerContactosDemo().pipe(
      map(data => {
        return data.respuesta.datos
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  obtenerPensamientoDemo(): Observable<PensamientoModel[]> {
    return this.jsonDemoServiceLocal.obtenerPensamientoDemo().pipe(
      map(data => {
        return data.respuesta.datos
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }


  obtenerProyectosDemo(): Observable<any> {
    return this.jsonDemoServiceLocal.obtenerProyectosDemo().pipe(
      map(data => {
        return data.respuesta.datos
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }


  obtenerNoticiasDemo(): Observable<any> {
    return this.jsonDemoServiceLocal.obtenerNoticiasDemo().pipe(
      map(data => {
        return data.respuesta.datos
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  obtenerPerfilGeneralDemo(): Observable<any> {
    return this.jsonDemoServiceLocal.obtenerPerfilGeneralDemo().pipe(
      map(data => {
        return data.respuesta.datos
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  obtenerProyectoDemo(): Observable<ProyectoModel> {
    return this.jsonDemoServiceLocal.obtenerProyectoDemo()
      .pipe(
        map(data => {
          return this.proyectoEntityMapperService.transform(data.respuesta.datos)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  obtenerNoticiaDemo(): Observable<NoticiaModel> {
    return this.jsonDemoServiceLocal.obtenerNoticiaDemo()
      .pipe(
        map(data => {
          return this.noticiaEntityMapperService.transform(data.respuesta.datos)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  obtenerComentarios(): Observable<ComentarioModel[]> {
    return this.jsonDemoServiceLocal.obtenerComentarios()
      .pipe(
        map(data => {
          return this.comentarioEntityMapperService.transform(data.respuesta.datos)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }
}
