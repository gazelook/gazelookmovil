import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AlbumModel, AlbumModelMapperService } from 'dominio/modelo/entidades/album.model';
import { AlbumServiceLocal } from '@core/servicios/locales/album.service';
import { AlbumServiceRemoto } from '@core/servicios/remotos/album.service';
import { AlbumEntity, AlbumEntityMapperService } from 'dominio/entidades/album.entity';
import { MediaEntityMapperService } from 'dominio/entidades/media.entity';

@Injectable({ providedIn: 'root' })
export class AlbumRepository {

  constructor(
    private albumServiceLocal: AlbumServiceLocal,
    private albumServiceRemoto: AlbumServiceRemoto,
    private albumMapperAEntidad: AlbumModelMapperService,
    private albumEntityMapperService: AlbumEntityMapperService,
    private albumModelMapperService: AlbumModelMapperService,
    private mediaEntityMapperService: MediaEntityMapperService
  ) {

  }

  guardarAlbumActivoEnSessionStorage(album: AlbumModel) {
    this.albumServiceLocal.guardarAlbumEnSessionStorage(album)
  }

  obtenerAlbumActivoDelSessionStorage(): AlbumModel {
    return this.albumServiceLocal.obtenerAlbumEnSessionStorage()
  }

  removerAlbumActivoDelSessionStorage() {
    this.albumServiceLocal.removerAlbumActivoDelSessionStorage()
  }

  actualizarAlbum(album: AlbumModel): Observable<string> {
    const albumEntity = this.albumMapperAEntidad.transform(album)
    return this.albumServiceRemoto.actualizarAlbum(albumEntity)
      .pipe(
        map(data => {
          return data.respuesta.datos
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  agragarMediaAlAlbum(
    idEntidad: string,
    codigoEntidad: string,
    album: AlbumModel
  ): Observable<number> {
    const albumEntity: AlbumEntity = this.albumModelMapperService.transform(album)
    return this.albumServiceRemoto.agragarMediaAlAlbum(idEntidad, codigoEntidad, albumEntity)
      .pipe(
        map(data => {
          return data.codigoEstado
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  eliminarMediaDelAlbum(
    idEntidad: string,
    codigoEntidad: string,
    album: AlbumModel
  ): Observable<number> {
    const albumEntity: AlbumEntity = this.albumModelMapperService.transform(album)
    return this.albumServiceRemoto.eliminarMediaDelAlbum(idEntidad, codigoEntidad, albumEntity)
      .pipe(
        map(data => {
          return data.codigoEstado
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  actualizarMediaDelAlbum(
    idEntidad: string,
    codigoEntidad: string,
    album: AlbumModel
  ): Observable<number> {
    const albumEntity: AlbumEntity = this.albumModelMapperService.transform(album)
    return this.albumServiceRemoto.actualizarMediaDelAlbum(idEntidad, codigoEntidad, albumEntity)
      .pipe(
        map(data => {
          return data.codigoEstado
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  agregarAlbumEnEntidad(
    idEntidad: string,
    codigoEntidad: string,
    album: AlbumModel
  ): Observable<AlbumModel> {
    const albumEntity: AlbumEntity = this.albumModelMapperService.transform(album)
    return this.albumServiceRemoto.agregarAlbumEnEntidad(idEntidad, codigoEntidad, albumEntity)
      .pipe(
        map(data => {
          return this.albumEntityMapperService.transform(data.respuesta.datos)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  actualizarParametrosDelAlbum(
    idEntidad: string,
    codigoEntidad: string,
    album: AlbumModel
  ): Observable<AlbumModel> {
    const albumEntity: AlbumEntity = this.albumModelMapperService.transform(album)
    return this.albumServiceRemoto.actualizarParametrosDelAlbum(idEntidad, codigoEntidad, albumEntity)
      .pipe(
        map(data => {
          return this.albumEntityMapperService.transform(data.respuesta.datos)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }
}
