import { RespuestaRemota } from '@core/util/respuesta';
import { AnuncioSistemaEntity, AnuncioSistemaMapperService } from 'dominio/entidades/anuncio-sistema.entity';
import { AnuncioSistemaModel } from 'dominio/modelo/entidades/anuncio-sistema.model';
import { HttpResponse } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AnuncioSistemaServiceRemoto } from '@core/servicios/remotos/anuncio-sistema.service';

@Injectable({ providedIn: 'root'})
export class AnuncioSistemaRepository {


    constructor(
        private anuncioSistemaServiceRemoto: AnuncioSistemaServiceRemoto,
        private anuncioSistemaMapperService: AnuncioSistemaMapperService
    ) {

    }

    obtenerAnunciosSistema(idPerfil: string): Observable<AnuncioSistemaModel[]>{
        return this.anuncioSistemaServiceRemoto.obtenerAnunciosSistema(idPerfil).pipe( 
            map((data: HttpResponse<RespuestaRemota<AnuncioSistemaEntity[]>>) => {
                return this.anuncioSistemaMapperService.transform(data.body.respuesta.datos)
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

    obtenerAnunciosSistemaPublicitario(idPerfil: string): Observable<AnuncioSistemaModel[]>{
        return this.anuncioSistemaServiceRemoto.obtenerAnunciosSistemaPublicitario(idPerfil).pipe( 
            map((data: HttpResponse<RespuestaRemota<AnuncioSistemaEntity[]>>) => {
                return this.anuncioSistemaMapperService.transform(data.body.respuesta.datos)
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

}