import { ComentarioIntercambioModel, ComentarioIntercambioModelMapperService } from 'dominio/modelo/entidades/comentario-intercambio.model';

import { HttpResponse } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ComentarioIntercambioEntity } from 'dominio/entidades/comentario-intercambio.entity';
import { ComentarioEntity } from 'dominio/entidades/comentario.entity';
import { ComentarioModel } from 'dominio/modelo/entidades/comentario.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ComentarioIntercambioServiceRemoto } from '@core/servicios/remotos/comentario.intercambio.service';
import { RespuestaRemota } from '@core/util/respuesta';
import { ComentarioIntercambioEntityMapperService } from 'dominio/entidades/comentario-intercambio.entity';

@Injectable({ providedIn: 'root'})
export class ComentarioIntercambioRepository {

    constructor(
        private comentarioModelMapperService: ComentarioIntercambioModelMapperService,
        private comentarioIntercambioEntityMapperService: ComentarioIntercambioEntityMapperService,
        private comentarioIntercambioServiceRemoto: ComentarioIntercambioServiceRemoto
    ) {

    }

    crearComentario(comentario: ComentarioIntercambioModel): Observable<ComentarioIntercambioModel> {
        const comentarioEntity: ComentarioIntercambioEntity = this.comentarioModelMapperService.transform(comentario)
        return this.comentarioIntercambioServiceRemoto.crearComentario(comentarioEntity)
            .pipe(
                map(data => {
                    return this.comentarioIntercambioEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    obtenerComentarios(
        idProyecto: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<ComentarioModel>> {
		return this.comentarioIntercambioServiceRemoto.obtenerComentarios(idProyecto, limite, pagina)
			.pipe(
				map((data: HttpResponse<RespuestaRemota<ComentarioEntity[]>>) => {
					const paginas: PaginacionModel<ComentarioModel> = {
                        proximaPagina: data.headers.get("proximaPagina") == "true",
                        totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
						lista: this.comentarioIntercambioEntityMapperService.transform(data.body.respuesta.datos)
					}
					return paginas
				}),
				catchError(error => {
					return throwError(error)
				})
			)
    }

    eliminarComentario(
        idIntercambio: string,
        coautor?: string,
        idComentario?: string
    ): Observable<string> {
        return this.comentarioIntercambioServiceRemoto.eliminarComentario(idIntercambio, coautor, idComentario)
            .pipe(
                map(data => {
                    return data.respuesta.mensaje
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    eliminarMiComentario(
        idIntercambio?: string,
        idCoautor?: string,
    ): Observable<string> {
        return this.comentarioIntercambioServiceRemoto.eliminarMiComentario(idIntercambio, idCoautor)
            .pipe(
                map(data => {
                    return data.respuesta.mensaje
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    obtenerContenidoComentario(
        comentarios: Array<ComentarioModel>
    ): Observable<ComentarioModel[]> {
        const comentariosEntity = this.comentarioModelMapperService.transform(comentarios)
        return this.comentarioIntercambioServiceRemoto.obtenerContenidoComentario(comentariosEntity)
            .pipe(
                map(data => {
                    return this.comentarioIntercambioEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }
}
