import { HttpResponse } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { RespuestaRemota } from '@core/util/respuesta';
import { IntercambioEntity } from 'dominio/entidades/intercambio.entity';
import { ProyectoModel } from "dominio/modelo/entidades/proyecto.model";
import { IntercambioServiceLocal } from '@core/servicios/locales/intercambio.service';
import { CodigosCatalogoTipoIntercambio } from '@core/servicios/remotos/codigos-catalogos/catalogo-tipo-intercambio.enum';
import { IntercambioServiceRemoto } from '@core/servicios/remotos/intercambio.service';
import { CatalogoTipoIntercambioEntityMapperService } from 'dominio/entidades/catalogos/catalogo-tipo-intercambio.entity';
import { IntercambioEntityMapperService } from 'dominio/entidades/intercambio.entity';
import { CatalogoTipoIntercambioModel } from 'dominio/modelo/catalogos/catalogo-tipo-intercambio.model';
import { IntercambioModel, IntercambioModelMapperService, IntercambioStatusModelMapperService } from 'dominio/modelo/entidades/intercambio.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';

@Injectable({ providedIn: 'root' })
export class IntercambioRepository {

  constructor(

    private proyectoServiceLocal: IntercambioServiceLocal,
    private intercambioServiceRemoto: IntercambioServiceRemoto,
    private intercambioModelMapperService: IntercambioModelMapperService,
    private intercambioEntityMapperService: IntercambioEntityMapperService,
    private intercambioStatusModelMapperService: IntercambioStatusModelMapperService,
    private catalogoTipoIntercambioEntityMapperService: CatalogoTipoIntercambioEntityMapperService
  ) {   }

  guardarIntercambioActivoEnSessionStorage(intercambio: IntercambioModel) {
    this.proyectoServiceLocal.guardarIntercambioActivoEnSessionStorage(intercambio)
  }

  obtenerIntercambioActivoDelSessionStorage(): IntercambioModel {
    return this.proyectoServiceLocal.obtenerIntercambioActivoDelSessionStorage()
  }

  removerintercambioActivoDelSessionStorage() {
    this.proyectoServiceLocal.removerIntercambioActivoDelSessionStorage()
  }

  crearIntercambio(intercambio: IntercambioModel): Observable<IntercambioModel> {
    const intercambioEntity: IntercambioEntity = this.intercambioModelMapperService.transform(intercambio)
    return this.intercambioServiceRemoto.crearintercambio(intercambioEntity).pipe(
      map(data => {
        return this.intercambioEntityMapperService.transform(data.respuesta.datos)
      }),
      catchError(err => {
        return throwError(err)
      })
    )
  }

  obtenerInformacionDelIntercambio(idintercambio: string, idPerfil: string): Observable<IntercambioModel> {
    return this.intercambioServiceRemoto.obtenerInformacionDelIntercambio(idintercambio, idPerfil).pipe(
      map(data => {
        const test = this.intercambioEntityMapperService.transform(data.respuesta.datos)
        return test
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  actualizarIntercambio(intercambio: IntercambioModel): Observable<IntercambioModel> {
    const intercambioEntity: IntercambioEntity = this.intercambioModelMapperService.transform(intercambio)
    return this.intercambioServiceRemoto.actualizarIntercambio(intercambioEntity).pipe(
      map(data => {
        return this.intercambioEntityMapperService.transform(data.respuesta.datos)
      }),
      catchError(err => {
        return throwError(err)
      })
    )
  }

  eliminarIntercambio(idIntercambio: string, idPerfil: string): Observable<string> {
    return this.intercambioServiceRemoto.eliminarIntercambio(idIntercambio, idPerfil)
      .pipe(
        map(data => {
          return data.respuesta.mensaje
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  cambiarStatusIntercambio(intercambio: IntercambioModel): Observable<string> {
    const intercambioEntity: IntercambioEntity = this.intercambioStatusModelMapperService.transform(intercambio)
    return this.intercambioServiceRemoto.cambiarStatusIntercambio(intercambioEntity)
      .pipe(
        map(data => {
          return data.respuesta.mensaje
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  tiposIntercambio(): Observable<Array<CatalogoTipoIntercambioModel>> {
    return this.intercambioServiceRemoto.tiposIntercambio()
      .pipe(
        map(data => {

          return this.catalogoTipoIntercambioEntityMapperService.transform(data.respuesta.datos)


        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  buscarintercambiosPorFiltro(
    limite: number,
    pagina: number,
    tipo: CodigosCatalogoTipoIntercambio,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
    pais: string,
    titulo: string,
    localidad: string,
    codigoAIntercambiarA: string,
    codigoAIntercambiarB: string,
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.intercambioServiceRemoto.buscarintercambiosPorFiltro(
      limite,
      pagina,
      tipo,
      perfil,
      fechaInicial,
      fechaFinal,
      pais,
      titulo,
      localidad,
      codigoAIntercambiarA,
      codigoAIntercambiarB,
    ).pipe(
      map((data: HttpResponse<RespuestaRemota<Array<IntercambioEntity>>>) => {

        const dataPagina: PaginacionModel<IntercambioModel> = {
          proximaPagina: data.headers.get("proximaPagina") == "true",
          totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
          lista: this.intercambioEntityMapperService.transform(data.body.respuesta.datos)
        }
        return dataPagina
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }



  buscarMisIntercambiosTipo(
    tipoIntercambio: CodigosCatalogoTipoIntercambio,
    perfil: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<IntercambioModel>> {
    return this.intercambioServiceRemoto.buscarMisIntercambiosTipo(
      tipoIntercambio,
      perfil,
      limite,
      pagina
    ).pipe(
      map((data: HttpResponse<RespuestaRemota<Array<IntercambioEntity>>>) => {
        const dataPagina: PaginacionModel<ProyectoModel> = {
          proximaPagina: data.headers.get("proximaPagina") == "true",
          totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
          lista: this.intercambioEntityMapperService.transform(data.body.respuesta.datos)
        }
        return dataPagina
      }),
      catchError(error => {
        return throwError(error)
      })
    )
  }



}
