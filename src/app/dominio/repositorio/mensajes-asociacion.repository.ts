import { ConversacionModel } from 'dominio/modelo/entidades/conversacion.model';
import { MensajeMapperService, MensajeEntity } from 'dominio/entidades/mensaje.entity';
import { MensajeModel } from 'dominio/modelo/entidades/mensaje.model';
import { RespuestaRemota } from '@core/util/respuesta';
import { PaginacionModel } from "dominio/modelo/paginacion-model"
import { Injectable } from '@angular/core'
import { Observable, throwError } from "rxjs"
import { catchError, map } from 'rxjs/operators'
import { HttpResponse } from '@angular/common/http'
import { MensajesAsociacionServiceRemoto } from '@core/servicios/remotos/mensajes-asociacion.service';
import { ConversacionEntity, ConversacionEntityMapperService } from 'dominio/entidades/conversacion.entity';
@Injectable({ providedIn: 'root'})
export class MensajesAsociacionRepository {

	constructor(
		private mensajesAsociacionServiceRemoto: MensajesAsociacionServiceRemoto,
		private mensajeMapperService: MensajeMapperService,
		private conversacionEntityMapperService: ConversacionEntityMapperService
	) { 	}

	obtenerConversacion(idPerfil: string, idAsociacion: string, limite: number, pagina: number, filtro: string): Observable<PaginacionModel<MensajeModel>> {
		return this.mensajesAsociacionServiceRemoto.obtenerConversacion(idPerfil, idAsociacion, limite, pagina, filtro)
			.pipe(
				map((data: HttpResponse<RespuestaRemota<MensajeEntity[]>>) => {
					let cargarMas = data.headers.get("proximaPagina") == "true"
					let paginas: PaginacionModel<MensajeModel> = {
						proximaPagina: cargarMas,
						lista: this.mensajeMapperService.transform(data.body.respuesta.datos)
					}
					return paginas
				}),
				catchError(err => {
					return throwError(err)
				})
			)
	}


	obtenerConversacionFecha(idPerfil: string, idAsociacion: string, limite: number, pagina: number, fecha: string): Observable<PaginacionModel<MensajeModel>> {
		return this.mensajesAsociacionServiceRemoto.obtenerConversacionFecha(idPerfil, idAsociacion, limite, pagina, fecha)
			.pipe(
				map((data: HttpResponse<RespuestaRemota<MensajeEntity[]>>) => {
					let cargarMas = data.headers.get("proximaPagina") == "true"
					let paginas: PaginacionModel<MensajeModel> = {
						proximaPagina: cargarMas,
						lista: this.mensajeMapperService.transform(data.body.respuesta.datos)
					}
					return paginas
				}),
				catchError(err => {
					return throwError(err)
				})
			)
	}

	obtenerMsjNoLeidos(idPerfil: string, limite: number, pagina: number): Observable<PaginacionModel<ConversacionModel>> {
        return this.mensajesAsociacionServiceRemoto.obtenerMsjNoLeidos(idPerfil, limite, pagina)
            .pipe(
                map((data: HttpResponse<RespuestaRemota<ConversacionEntity[]>>) => {

                    let cargarMas = data.headers.get("proximaPagina") == "true"
                    let paginas: PaginacionModel<ConversacionModel> = {
                        proximaPagina: cargarMas,
                        lista: this.conversacionEntityMapperService.transform(data.body.respuesta.datos)
                    }
                    return paginas
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
    }
}
