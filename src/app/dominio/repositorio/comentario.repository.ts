import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { ComentarioModel, ComentarioModelMapperService } from 'dominio/modelo/entidades/comentario.model';
import { ComentarioEntity, ComentarioEntityMapperService } from 'dominio/entidades/comentario.entity';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Injectable } from "@angular/core"
import { ComentarioServiceRemoto } from '@core/servicios/remotos/comentario.service';
import { RespuestaRemota } from '@core/util/respuesta';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root'})
export class ComentarioRepository {

    constructor(
        private comentarioModelMapperService: ComentarioModelMapperService,
        private comentarioEntityMapperService: ComentarioEntityMapperService,
        private comentarioServiceRemoto: ComentarioServiceRemoto,
    ) {

    }

    crearComentario(comentario: ComentarioModel): Observable<ComentarioModel> {
        const comentarioEntity: ComentarioEntity = this.comentarioModelMapperService.transform(comentario)
        return this.comentarioServiceRemoto.crearComentario(comentarioEntity)
            .pipe(
                map(data => {
                    return this.comentarioEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    obtenerComentarios(
        idProyecto: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<ComentarioModel>> {
		return this.comentarioServiceRemoto.obtenerComentarios(idProyecto, limite, pagina)
			.pipe(
				map((data: HttpResponse<RespuestaRemota<ComentarioEntity[]>>) => {
					const paginas: PaginacionModel<ComentarioModel> = {
                        proximaPagina: data.headers.get("proximaPagina") == "true",
                        totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
						lista: this.comentarioEntityMapperService.transform(data.body.respuesta.datos)
					}
					return paginas
				}),
				catchError(error => {
					return throwError(error)
				})
			)
    }

    eliminarComentario(
        idProyecto: string,
        coautor?: string,
        idComentario?: string
    ): Observable<string> {
        return this.comentarioServiceRemoto.eliminarComentario(idProyecto, coautor, idComentario)
            .pipe(
                map(data => {
                    return data.respuesta.mensaje
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    eliminarMiComentario(
        idComentario?: string,
        idCoautor?: string,
    ): Observable<string> {
        return this.comentarioServiceRemoto.eliminarMiComentario(idComentario, idCoautor)
            .pipe(
                map(data => {
                    return data.respuesta.mensaje
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    obtenerContenidoComentario(
        comentarios: Array<ComentarioModel>
    ): Observable<ComentarioModel[]> {
        const comentariosEntity = this.comentarioModelMapperService.transform(comentarios)
        return this.comentarioServiceRemoto.obtenerContenidoComentario(comentariosEntity)
            .pipe(
                map(data => {
                    return this.comentarioEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }
}
