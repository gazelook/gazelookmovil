import { LandingService } from '@core/servicios/remotos/landing.service';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {throwError} from '@env/node_modules/rxjs';
import {Observable} from 'rxjs';


@Injectable({providedIn: 'root'})
export class LandingRepository {
  constructor(
    private landingService: LandingService
  ) {
  }

  public mostrarTextoLandingSegundaPagina(): Observable<number> {
    return this.landingService.mostrarTextoLandingSegundaPagina()
      .pipe(map(data => data.respuesta.datos), catchError(err => throwError(err)));
  }
}
