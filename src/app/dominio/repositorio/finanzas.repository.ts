import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FinanzasServiceRemoto } from '@core/servicios/remotos/finanzas.service';
import { RespuestaRemota } from '@core/util/respuesta';
import {
  ReporteFinanzasUsuarioEntity,
  ReporteFinanzasUsuarioEntityMapperService,
} from 'dominio/entidades/reporte-finanzas-usuario.entity';
import { ReporteFinanzasUsuarioModel } from 'dominio/modelo/entidades/reporte-finanzas-usuario.model';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';

@Injectable({
  providedIn: 'root',
})
export class FinanzasRepository {
  constructor(
    private finanzasServiceRemoto: FinanzasServiceRemoto,
    private reporteFinanzasUsuarioEntityMapperService: ReporteFinanzasUsuarioEntityMapperService
  ) {}

  obtenerAportaciones(
    idUsuario: string,
    fechaInicial: string,
    fechaFinal: string
  ): Observable<any> {
    return this.finanzasServiceRemoto
      .obtenerAportaciones(idUsuario, fechaInicial, fechaFinal)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError((error) => {
          return throwError(error);
        })
      );
  }

  obtenerDocumentosLegales(
    origen: string,
    fechaInicial: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<any>> {
    return this.finanzasServiceRemoto
      .obtenerDocumentosLegales(origen, fechaInicial, limite, pagina)
      .pipe(
        map((data: HttpResponse<RespuestaRemota<any>>) => {
          let cargarMas = data.headers.get('proximaPagina') == 'true';
          let paginas: PaginacionModel<any> = {
            proximaPagina: cargarMas,
            lista: data.body.respuesta.datos,
          };
          return paginas;
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  obtenerAportacionesGenerales(
    filtro: string,
    codigo: string,
    fechaInicial: string,
    fechaFinal: string
  ): Observable<any> {
    return this.finanzasServiceRemoto
      .obtenerAportacionesGenerales(filtro, codigo, fechaInicial, fechaFinal)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError((error) => {
          return throwError(error);
        })
      );
  }

  obtenerDatosInformeActualFinanzas(
    idUsuario: string,
    anio: string,
    mes?
  ): Observable<ReporteFinanzasUsuarioModel[]> {
    return this.finanzasServiceRemoto
      .obtenerDatosInformeActualFinanzas(idUsuario, anio, mes)
      .pipe(
        map(
          (
            data: HttpResponse<
              RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>
            >
          ) => {
            return this.reporteFinanzasUsuarioEntityMapperService.transform(
              data.body.respuesta.datos
            );
          }
        ),
        catchError((error) => {
          return throwError(error);
        })
      );
  }

  obtenerTarifasTotalesUsuario(
    fecha?: string, codigoPais?: string, pais?: string
  ): Observable<ReporteFinanzasUsuarioModel[]> {
    return this.finanzasServiceRemoto.obtenerTarifasTotalesUsuario(fecha, codigoPais).pipe(
      map(
        (
          data: HttpResponse<
            RespuestaRemota<Array<ReporteFinanzasUsuarioEntity>>
          >
        ) => {
          return this.reporteFinanzasUsuarioEntityMapperService.transform(
            data.body.respuesta.datos
          );
        }
      ),
      catchError((error) => {
        return throwError(error);
      })
    );
  }
}
