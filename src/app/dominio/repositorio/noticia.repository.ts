import { VotoNoticiaEntity, VotoNoticiaEntityMapperService } from 'dominio/entidades/voto-noticia.entity';
import { VotoNoticiaModel, VotoNoticiaModelMapperService } from 'dominio/modelo/entidades/voto-noticia.model';
import { NoticiaModel, NoticiaModelMapperService } from 'dominio/modelo/entidades/noticia.model';
import { NoticiaServiceLocal } from '@core/servicios/locales/noticia.service';
import { PaginacionModel } from 'dominio/modelo/paginacion-model';
import { RespuestaRemota } from '@core/util/respuesta';
import { HttpResponse } from '@angular/common/http';
import { map, catchError} from 'rxjs/operators';
import { NoticiaServiceRemoto } from '@core/servicios/remotos/noticia.service';
import { NoticiaEntity, NoticiaEntityMapperService } from 'dominio/entidades/noticia.entity';
import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';

@Injectable({ providedIn: 'root'})
export class NoticiaRepository {


    constructor(
        private noticiaServiceLocal: NoticiaServiceLocal,
        private noticiaServiceRemoto: NoticiaServiceRemoto,
        private noticiaModelMapperService: NoticiaModelMapperService,
        private noticiaEntityMapperService: NoticiaEntityMapperService,
        private votoNoticiaEntityMapperService: VotoNoticiaEntityMapperService,
        private votoNoticiaModelMapperService: VotoNoticiaModelMapperService,
    ) {   }
    obtenerNoticiasPerfil(idPerfil: string, limite: number, pagina: number): Observable<PaginacionModel<NoticiaEntity>>{
        return this.noticiaServiceRemoto.obtenerNoticiasPerfil(idPerfil, limite, pagina, false)
        .pipe(
            map((data: HttpResponse<RespuestaRemota<NoticiaEntity[]>>) => {
                let cargarMas = data.headers.get("proximaPagina") == "true"
                let paginas: PaginacionModel<NoticiaEntity> = {
                    proximaPagina: cargarMas,
                    lista: data.body.respuesta.datos
                }
                return paginas
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

    obtenerNoticiasMapeadasDelPerfil(idPerfil: string, limite: number, pagina: number, traducir: boolean): Observable<PaginacionModel<NoticiaModel>>{
        return this.noticiaServiceRemoto.obtenerNoticiasPerfil(idPerfil, limite, pagina, traducir)
        .pipe(
            map((data: HttpResponse<RespuestaRemota<NoticiaEntity[]>>) => {
                let cargarMas = data.headers.get("proximaPagina") == "true"
                let paginas: PaginacionModel<NoticiaModel> = {
                    proximaPagina: cargarMas,
                    lista: this.noticiaEntityMapperService.transform(data.body.respuesta.datos)
                }
                return paginas
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

    guardarNoticiaActivaEnSessionStorage(noticia: NoticiaModel) {
        this.noticiaServiceLocal.guardarNoticiaActivaEnSessionStorage(noticia)
    }

    obtenerNoticiaActiviaDelSessionStorage(): NoticiaModel  {
        return this.noticiaServiceLocal.obtenerNoticiaActiviaDelSessionStorage()
    }

    removerNoticiaActivaDelSessionStorage() {
        this.noticiaServiceLocal.removerNoticiaActivaDelSessionStorage()
    }

    crearNoticia(noticia: NoticiaModel): Observable<NoticiaModel> {
        const noticiaEntity: NoticiaEntity = this.noticiaModelMapperService.transform(noticia)
        return this.noticiaServiceRemoto.crearNoticia(noticiaEntity)
            .pipe(
                map(data => {
                    return this.noticiaEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    obtenerInformacionDeLaNoticia(idNoticia: string, idPerfil: string): Observable<NoticiaModel> {
        return this.noticiaServiceRemoto.obtenerInformacionDeLaNoticia(idNoticia, idPerfil)
            .pipe(
                map(data => {
                    return this.noticiaEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    actualizarNoticia(noticia: NoticiaModel): Observable<NoticiaModel> {
        const noticiaEntity: NoticiaEntity = this.noticiaModelMapperService.transform(noticia)
        return this.noticiaServiceRemoto.actualizarNoticia(noticiaEntity)
            .pipe(
                map(data => {
                    return this.noticiaEntityMapperService.transform(data.respuesta.datos)
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    eliminarNoticia(idPerfil: string, idNoticia: string): Observable<string> {
        return this.noticiaServiceRemoto.eliminarNoticia(idPerfil, idNoticia)
            .pipe(
                map(data => {
                    return data.respuesta.mensaje
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    apoyarNoticia(voto: VotoNoticiaModel): Observable<string> {
        const votoNoticiaEntity: VotoNoticiaEntity = this.votoNoticiaModelMapperService.transform(voto)
        return this.noticiaServiceRemoto.apoyarNoticia(votoNoticiaEntity)
            .pipe(
                map(data => {
                    return data.respuesta.mensaje
                }),
                catchError(error => {
                    return throwError(error)
                })
            )
    }

    buscarNoticiasSinfiltroFechas(
        limite: number,
        pagina: number,

        perfil: string
    ): Observable<PaginacionModel<NoticiaModel>> {
        return this.noticiaServiceRemoto.buscarNoticiasSinfiltroFechas(
            limite,
            pagina,
            perfil
        ).pipe(
            map((data: HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>) => {
                const dataPagina: PaginacionModel<NoticiaModel> = {
                    proximaPagina: data.headers.get("proximaPagina") == "true",
                    totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
                    lista: this.noticiaEntityMapperService.transform(data.body.respuesta.datos)
                }
                return dataPagina
            }),
            catchError(error => {
                return throwError(error)
            })
        )
    }

    buscarNoticiasPorFechas(
        limite: number,
        pagina: number,
        fechaInicial: string,
        fechaFinal: string,
        filtro: string,
        perfil: string
    ): Observable<PaginacionModel<NoticiaModel>> {
        return this.noticiaServiceRemoto.buscarNoticiasPorFechas(
            limite,
            pagina,
            fechaInicial,
            fechaFinal,
            filtro,
            perfil
        ).pipe(
            map((data: HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>) => {
                const dataPagina: PaginacionModel<NoticiaModel> = {
                    proximaPagina: data.headers.get("proximaPagina") == "true",
                    totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
                    lista: this.noticiaEntityMapperService.transform(data.body.respuesta.datos)
                }
                return dataPagina
            }),
            catchError(error => {
                return throwError(error)
            })
        )
    }

    buscarNoticiasPorTitulo(
        titulo: string,
        limite: number,
        pagina: number
    ): Observable<PaginacionModel<NoticiaModel>> {
        return this.noticiaServiceRemoto.buscarNoticiasPorTitulo(
            titulo,
            limite,
            pagina
        ).pipe(
            map((data: HttpResponse<RespuestaRemota<Array<NoticiaEntity>>>) => {
                const dataPagina: PaginacionModel<NoticiaModel> = {
                    proximaPagina: data.headers.get("proximaPagina") == "true",
                    totalDatos: (data.headers.get("totalDatos")) ? parseInt(data.headers.get("totalDatos")) : 0,
                    lista: this.noticiaEntityMapperService.transform(data.body.respuesta.datos)
                }
                return dataPagina
            }),
            catchError(error => {
                return throwError(error)
            })
        )
    }
}
