import {HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {RespuestaRemota} from '@core/util/respuesta';
import {ProyectoServiceLocal} from '@core/servicios/locales/proyectos.service';
import {ProyectoServiceRemoto} from '@core/servicios/remotos/proyectos.service';
import {ProyectoEntity, ProyectoEntityMapperService} from 'dominio/entidades/proyecto.entity';
import {VotoProyectoEntity} from 'dominio/entidades/votoProyecto.entity';
import {DonacionProyectosModel, ProyectoModel, ProyectoModelMapperService} from 'dominio/modelo/entidades/proyecto.model';
import {VotoProyectoModel, VotoProyectoModelMapperService} from 'dominio/modelo/entidades/voto-proyecto.model';
import {PaginacionModel} from 'dominio/modelo/paginacion-model';
import {CodigosCatalogoTipoProyecto} from '@core/servicios/remotos/codigos-catalogos/codigos-catalogo-tipo-proyecto.enum';
import {FiltroBusquedaProyectos} from '@core/servicios/remotos/filtro-busqueda/filtro-busqueda.enum';
import {PagoEntity} from 'dominio/entidades';

@Injectable({providedIn: 'root'})
export class ProyectoRepository {


  constructor(
    private proyectoServiceRemoto: ProyectoServiceRemoto,
    private proyectoServiceLocal: ProyectoServiceLocal,
    private proyectoModelMapperService: ProyectoModelMapperService,
    private proyectoEntityMapperService: ProyectoEntityMapperService,
    private votoProyectoModelMapperService: VotoProyectoModelMapperService
  ) {
  }

  guardarProyectoActivoEnSessionStorage(proyecto: ProyectoModel): void {
    this.proyectoServiceLocal.guardarProyectoActivoEnSessionStorage(proyecto);
  }

  obtenerProyectoActivoDelSessionStorage(): ProyectoModel {
    return this.proyectoServiceLocal.obtenerProyectoActivoDelSessionStorage();
  }

  crearProyecto(proyecto: ProyectoModel): Observable<ProyectoModel> {
    const proyectoEntity: ProyectoEntity = this.proyectoModelMapperService.transform(proyecto);
    return this.proyectoServiceRemoto.crearProyecto(proyectoEntity).pipe(
      map(data => {
        return this.proyectoEntityMapperService.transform(data.respuesta.datos);
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  donacionesProyectos(donacionProyecto: DonacionProyectosModel): Observable<PagoEntity> {
    return this.proyectoServiceRemoto.donacionesProyectos(donacionProyecto).pipe(
      map(resp => resp.respuesta.datos),
      catchError(err => throwError(err))
    );
  }

  validarDonacionProyectoStripe(idTransaccion: string): Observable<boolean> {
    return this.proyectoServiceRemoto.validarDonacionProyectoStripe(idTransaccion).pipe(
      map(resp => resp.respuesta.datos),
      catchError(err => throwError(err))
    );
  }


  actualizarProyecto(proyecto: ProyectoModel): Observable<ProyectoModel> {
    const proyectoEntity: ProyectoEntity = this.proyectoModelMapperService.transform(proyecto);
    return this.proyectoServiceRemoto.actualizarProyecto(proyectoEntity).pipe(
      map(data => {
        return this.proyectoEntityMapperService.transform(data.respuesta.datos);
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  removerProyectoActivoDelSessionStorage(): void {
    this.proyectoServiceLocal.removerProyectoActivoDelSessionStorage();
  }

  obtenerInformacionDelProyecto(idProyecto: string, idPerfil: string, estado?: string): Observable<ProyectoModel> {
    return this.proyectoServiceRemoto.obtenerInformacionDelProyecto(idProyecto, idPerfil, estado).pipe(
      map(data => this.proyectoEntityMapperService.transform(data.respuesta.datos)),
      catchError(error => throwError(error))
    );
  }


  obtenerProyectosPerfil(idPerfil: string, limite: number, pagina: number): Observable<PaginacionModel<ProyectoEntity>> {
    return this.proyectoServiceRemoto.obtenerProyectosPerfil(idPerfil, limite, pagina, false).pipe(
      map((data: HttpResponse<RespuestaRemota<ProyectoEntity[]>>) => {
        const cargarMas = data.headers.get('proximaPagina') === 'true';
        const paginas: PaginacionModel<ProyectoEntity> = {
          proximaPagina: cargarMas,
          lista: data.body.respuesta.datos
        };
        return paginas;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }


  obtenerProyectosMapeadosDelPerfil(idPerfil: string, limite: number, pagina: number, traducir: boolean)
    : Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.obtenerProyectosPerfil(idPerfil, limite, pagina, traducir).pipe(
      map((data: HttpResponse<RespuestaRemota<ProyectoEntity[]>>) => {
        const cargarMas = data.headers.get('proximaPagina') === 'true';
        const paginas: PaginacionModel<ProyectoModel> = {
          proximaPagina: cargarMas,
          lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
        };
        return paginas;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }


  apoyarProyecto(voto: VotoProyectoModel): Observable<string> {
    const votoEntidad: VotoProyectoEntity = this.votoProyectoModelMapperService.transform(voto);
    return this.proyectoServiceRemoto.apoyarProyecto(votoEntidad)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  obtenerFechaMaximaParaActualizarValorEstimado(): Observable<{ fecha: Date }> {
    return this.proyectoServiceRemoto.obtenerFechaMaximaParaActualizarValorEstimado()
      .pipe(
        map(data => {
          return data.respuesta.datos;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  eliminarProyecto(idProyecto: string, idPerfil: string): Observable<string> {
    return this.proyectoServiceRemoto.eliminarProyecto(idProyecto, idPerfil)
      .pipe(
        map(data => {
          return data.respuesta.mensaje;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  obtenerFechaForo(
    inicio: boolean
  ): Observable<any> {
    return this.proyectoServiceRemoto.obtenerFechaForo(inicio)
      .pipe(
        map(data => {
          return data.respuesta.datos;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  obtenerProyectosSinFriltroFechas(
    limite: number,
    pagina: number,
    perfil: string,
    tipo: CodigosCatalogoTipoProyecto,
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.obtenerProyectosSinFriltroFechas(
      limite,
      pagina,
      perfil,
      tipo
    ).pipe(
      map((data: HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>) => {
        const dataPagina: PaginacionModel<ProyectoModel> = {
          proximaPagina: data.headers.get('proximaPagina') === 'true',
          totalDatos: (data.headers.get('totalDatos')) ? parseInt(data.headers.get('totalDatos'), 0) : 0,
          lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
        };
        return dataPagina;
      }),
      catchError(error => {
        return throwError(error);
      })
    );
  }

  obtenerProyectosRecomendados(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.obtenerProyectosRecomendados(idPerfil, limite, pagina, tipo)
      .pipe(
        map((data: HttpResponse<RespuestaRemota<ProyectoEntity[]>>) => {
          const cargarMas = data.headers.get('proximaPagina') === 'true';
          const paginas: PaginacionModel<ProyectoModel> = {
            proximaPagina: cargarMas,
            lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
          };
          return paginas;
        }),
        catchError(err => throwError(err))
      );
  }

  obtenerProyectosForos(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.obtenerProyectosForos(idPerfil, limite, pagina, tipo)
      .pipe(
        map((data: HttpResponse<RespuestaRemota<ProyectoEntity[]>>) => {
          const cargarMas = data.headers.get('proximaPagina') === 'true';
          const paginas: PaginacionModel<ProyectoModel> = {
            proximaPagina: cargarMas,
            lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
          };
          return paginas;
        }),
        catchError(err => throwError(err))
      );
  }

  obtenerProyectosEsperaFinanciamiento(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.obtenerProyectosEsperaFinanciamiento(idPerfil, limite, pagina, tipo)
      .pipe(
        map((data: HttpResponse<RespuestaRemota<ProyectoEntity[]>>) => {
          const cargarMas = data.headers.get('proximaPagina') === 'true';
          const paginas: PaginacionModel<ProyectoModel> = {
            proximaPagina: cargarMas,
            lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
          };
          return paginas;
        }),
        catchError(err => throwError(err))
      );
  }

  obtenerProyectosSeleccionados(
    idPerfil: string,
    limite: number,
    pagina: number,
    tipo: string,
    fecha?: string
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.obtnerProyectosSeleccionados(idPerfil, limite, pagina, tipo, fecha)
      .pipe(
        map((data: HttpResponse<RespuestaRemota<ProyectoEntity[]>>) => {
          const cargarMas = data.headers.get('proximaPagina') === 'true';
          const paginas: PaginacionModel<ProyectoModel> = {
            proximaPagina: cargarMas,
            lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
          };
          return paginas;
        }),
        catchError(err => throwError(err))
      );
  }

  buscarProyectosPorFiltro(
    limite: number,
    pagina: number,
    filtro: FiltroBusquedaProyectos,
    tipo: CodigosCatalogoTipoProyecto,
    perfil: string,
    fechaInicial: string,
    fechaFinal: string,
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.buscarProyectosPorFiltro(
      limite,
      pagina,
      filtro,
      tipo,
      perfil,
      fechaInicial,
      fechaFinal,
    ).pipe(
      map((data: HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>) => {
        const dataPagina: PaginacionModel<ProyectoModel> = {
          proximaPagina: data.headers.get('proximaPagina') === 'true',
          totalDatos: (data.headers.get('totalDatos')) ? parseInt(data.headers.get('totalDatos'), 0) : 0,
          lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
        };
        return dataPagina;
      }),
      catchError(error => {
        return throwError(error);
      })
    );
  }

  buscarProyectosPorTitulo(
    titulo: string,
    limite: number,
    pagina: number
  ): Observable<PaginacionModel<ProyectoModel>> {
    return this.proyectoServiceRemoto.buscarProyectosPorTitulo(
      titulo,
      limite,
      pagina
    ).pipe(
      map((data: HttpResponse<RespuestaRemota<Array<ProyectoEntity>>>) => {
        const dataPagina: PaginacionModel<ProyectoModel> = {
          proximaPagina: data.headers.get('proximaPagina') === 'true',
          totalDatos: (data.headers.get('totalDatos')) ? parseInt(data.headers.get('totalDatos'), 0) : 0,
          lista: this.proyectoEntityMapperService.transform(data.body.respuesta.datos)
        };
        return dataPagina;
      }),
      catchError(error => {
        return throwError(error);
      })
    );
  }

  confirmarTransferenciaDeProyecto(
    idProyecto: string,
    idPerfilNuevo: string,
    idPerfilPropietario: string
  ): Observable<number> {
    return this.proyectoServiceRemoto.confirmarTransferenciaDeProyecto(
      idProyecto,
      idPerfilNuevo,
      idPerfilPropietario
    ).pipe(
      map(data => {
        return data.codigoEstado;
      }),
      catchError(error => {
        return throwError(error);
      })
    );
  }
}
