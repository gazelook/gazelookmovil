import { PagoEntity } from 'dominio/entidades/pago.entity';
import { CatalogoMetodoPagoExtraModel } from './../modelo/catalogos/catalogo-metodo-pago.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { LocalStorage } from '@core/servicios/locales/local-storage.service';
import { PagoService } from '@core/servicios/remotos/pago.service';
import { CatalogoMetodoPagoMapperService, MetodoPagoStripeEntity } from 'dominio/entidades/catalogos/catalogo-metodo-pago.entity';
import { CatalogoMetodoPagoModel, PagoStripeModel } from 'dominio/modelo/catalogos/catalogo-metodo-pago.model';

@Injectable({
  providedIn: 'root'
})
export class PagoRepository {

  prepararPagoPaypal(data: any): Observable<string> {
    return this.pagoServicie.prepararPagoPaypal(data)
      .pipe(
        map(data => {
          return data.respuesta.datos.orderID;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  constructor(
    protected http: HttpClient,
    private pagoServicie: PagoService,
    private mapper: CatalogoMetodoPagoMapperService,
    private local: LocalStorage
  ) {

  }

  obtenerCatalogoMetodoPago(): Observable<CatalogoMetodoPagoModel[]> {
    return this.pagoServicie.obtenerCatalogoMetodosPago()
      .pipe(
        map(data => this.mapper.transform(data.respuesta.datos)),
        catchError(err => throwError(err)));
  }

  almacenarLocalmenteMetodosPago(metodos: CatalogoMetodoPagoModel[]): void {
    this.local.almacenarMetodosPago(metodos);
  }

  obtenerLocalMetodosPago(): CatalogoMetodoPagoModel[] {
    return this.local.obtenerMetodosPago();
  }

  eliminarVariableStorage(llave: string): void {
    this.local.eliminarVariableStorage(llave);
  }

  crearOrdenPagoExtra(data: CatalogoMetodoPagoExtraModel): Observable<PagoEntity>{
    return this.pagoServicie.crearOrdenPagoExtra(data).pipe(
      map(resp => resp.respuesta.datos),
      catchError(err => throwError(err))
    );
  }

  validarPagoExtra(data: string): Observable<boolean>{
    return this.pagoServicie.validarPagoExtra(data).pipe(
      map(resp => resp.respuesta.datos),
      catchError(err => throwError(err))
    );
  }

  prepararPagoStripe(data: PagoStripeModel): Observable<MetodoPagoStripeEntity> {
    return this.pagoServicie.prepararPagoStripe(data).pipe(
      map(data => {
        return data.respuesta.datos;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
