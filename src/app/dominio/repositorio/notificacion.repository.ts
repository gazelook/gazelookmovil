import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NotificacionGeneralServiceRemoto } from '@core/servicios/remotos/notificacion.service';
import { RespuestaRemota } from '@core/util/respuesta';
import { ConversacionEntityMapperService } from 'dominio/entidades/conversacion.entity';
import { MensajeMapperService } from 'dominio/entidades/mensaje.entity';
import { ConversacionModel } from 'dominio/modelo/entidades/conversacion.model';
import { NotificacionEntity, NotificacionEntityMapperService } from 'dominio/entidades/notificacion.entity';
import { MensajeModel } from 'dominio/modelo/entidades/mensaje.model';


@Injectable({
  providedIn: 'root'
})
export class NotificacionRepository {

  constructor(
    private notificacionGeneralServiceRemoto: NotificacionGeneralServiceRemoto,
    private conversacionEntityMapperService: ConversacionEntityMapperService,
    private mensajeMapperService: MensajeMapperService,
    private notificacionEntityMapperService: NotificacionEntityMapperService
  ) { }
  notiLLamada(): Observable<MensajeModel> {
    return this.notificacionGeneralServiceRemoto.notiLLamada()
      .pipe(
        map((data: RespuestaRemota<NotificacionEntity>) => {

          let dataMsj = this.mensajeMapperService.transform(data.respuesta.datos.data)
          let notiModel = this.notificacionEntityMapperService.transform(data.respuesta.datos)
          notiModel.data = dataMsj
          return notiModel
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }

  notiMensajeNoLeido(): Observable<ConversacionModel> {
    return this.notificacionGeneralServiceRemoto.notiMensajeNoLeido()
      .pipe(
        map((data: RespuestaRemota<NotificacionEntity>) => {
          return this.conversacionEntityMapperService.transform(data.respuesta.datos.data)
        }),
        catchError(error => {
          return throwError(error)
        })
      )
  }


  notiDataMensaje(): Observable<MensajeModel> {
    return this.notificacionGeneralServiceRemoto.notiDataMensaje()
  }

}
