import { Injectable } from '@angular/core';
import { ParamsServiceLocal } from '@core/servicios/locales/params.service';

@Injectable({ providedIn: 'root' })
export class ParamsRepository {

    constructor(
        private paramsServiceLocal: ParamsServiceLocal
    ) {   }

    guardarParamsEnSessionStorage(params: any) {
        this.paramsServiceLocal.guardarParams(params)
    }

    obtenerParamsDelSessionStorage() : any {
        this.paramsServiceLocal.obtenerParams()
    }

}
