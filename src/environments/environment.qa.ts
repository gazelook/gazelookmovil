// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://apiqa.gazelook.com/api/',
  apiMantenimiento: 'https://supportqa.gazelook.com/api/',
  apiKey: '5a2ead641b254e3bba911d08b0adf2f6',
  agora: {
    appId: '5db4afad7da0472999cfb765cbb7952b'
  },
  firebase: {
    apiKey: 'AIzaSyAnRIromT8qyhZYjZUb7kdwEcKnFVPNfAU',
    authDomain: 'gazelook-qa.firebaseapp.com',
    databaseURL: 'https://gazelook-qa-default-rtdb.firebaseio.com',
    projectId: 'gazelook-qa',
    storageBucket: 'gazelook-qa.appspot.com',
    messagingSenderId: '1037523701616',
    appId: '1:1037523701616:web:43fdcc909b4e5567f6c9b5'
  },
  mapBoxToken: 'pk.eyJ1IjoiZ2F6ZWxvb2siLCJhIjoiY2tvdTdqeWpoMGN1bDJ4bzNqMnVnaWF1aSJ9.EeoWuUTzEGcu0XwGShytTA',
  clientIdPaypal: 'AfoZpR_cdNqCpFhZ9i2SdDd8by6G35z0CZGLZcGXrbRB_RfgVYgEUXy3ouWrl4drS-fd9hOv2DM2l8aq',
  pkStripe: 'pk_test_51IqgS8BwtZzPGjbYedBedJ5SLS8m3UBTBgi5OohH58uvtrB3HRP0fgt8i0WzckdfuY4KI1yg9GDCCVZpm87zCyiD00UlXHzeej',
  clientAppCodePaymentez: 'TPP3-EC-CLIENT',
  clientAppKeyPaymentez: 'ZfapAKOk4QFXheRNvndVib9XU3szzg',
  serverAppCodePaymentez: 'TPP3-EC-SERVER',
  serverAppKeyPaymentez: 'JdXTDl2d0o0B8ANZ1heJOq7tf62PC6',
  envModePaymentez: 'stg',
  apiPaymentez: 'https://ccapi-stg.paymentez.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
