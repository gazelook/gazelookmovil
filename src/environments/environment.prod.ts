export const environment = {
  production: true,
  apiUrl: 'https://api.gazelook.com/api/',
  apiSocket: 'https://api.gazelook.com/',
  apiMantenimiento: 'https://support.gazelook.com/api/',
  apiKey: 'ca03na188ame03u1d78620de67282882a84',
  agora: {
    appId: '5db4afad7da0472999cfb765cbb7952b'
  },
  firebase: {
    apiKey: 'AIzaSyAksNtzLnoUQWy_cNnc4pXBUMaAMsjECpc',
    authDomain: 'gazelook-production.firebaseapp.com',
    databaseURL: 'https://gazelook-production-default-rtdb.firebaseio.com',
    projectId: 'gazelook-production',
    storageBucket: 'gazelook-production.appspot.com',
    messagingSenderId: '89348822340',
    appId: '1:89348822340:web:0e5bb30aa5300eed073311'
  },
  mapBoxToken: 'pk.eyJ1IjoiZ2F6ZWxvb2siLCJhIjoiY2tvdTdqeWpoMGN1bDJ4bzNqMnVnaWF1aSJ9.EeoWuUTzEGcu0XwGShytTA',
  clientIdPaypal: 'AfoZpR_cdNqCpFhZ9i2SdDd8by6G35z0CZGLZcGXrbRB_RfgVYgEUXy3ouWrl4drS-fd9hOv2DM2l8aq',
  // pkStripe: 'pk_test_51IqgS8BwtZzPGjbYedBedJ5SLS8m3UBTBgi5OohH58uvtrB3HRP0fgt8i0WzckdfuY4KI1yg9GDCCVZpm87zCyiD00UlXHzeej'
  pkStripe: 'pk_live_51IqgS8BwtZzPGjbYFMNVxWBs9Y35HpEb1qXz6XniTUcVAXurLHaYbMV5Gqi0SfMKfNYLsbFukMiwqFgrqiKUsYeT00r3MaYFve',
  clientAppCodePaymentez: 'GAZELOOKCOM-EC-CLIENT',
  clientAppKeyPaymentez: 'MXzMNGWBvBqxmxCBHRymj7lXDnML8H',
  serverAppCodePaymentez: 'GAZELOOKCOM-EC-SERVER',
  serverAppKeyPaymentez: 'yA0qvxk3LK1H1ltDLfJSQWMNmzulkp',
  envModePaymentez: 'prod',
  apiPaymentez: 'https://ccapi.paymentez.com'
};
